﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using winMoz.Markets.Bids;
using winMoz.Markets;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Data.Access.Repositories;
using System.Collections.Concurrent;
using winMoz.Data.Access.Repositories.Prediction;

namespace winMoz.Markets.ControlReserve
{
    public class ControlReserve_Activation : SimEvent
    {
        #region Attributes
        public ActivationAttributes Attributes { get; private set; }
        private Market MarketCoupledTo;
        private DateTime ActivationEventTime { get; set; }
        private BidRepository Bid_Repo { get; set; }
        public ActivationResultHandler Results { get; set; }

        protected BlockingCollection<Bid> Activations => Bid_Repo.Bids;

        protected List<Bid_Segment> SegmentsActivation;

        public delegate void SimEventHandler(ControlReserve_Activation sender, ActivationArgs eventArgs);

        /// <summary>
        /// <seealso cref="EventHandler"/> zur Auslösung des Events
        /// </summary>
        public event SimEventHandler Activation;
        #endregion

        #region Initialisierung
        private ControlReserve_Activation() { }
        public ControlReserve_Activation(ActivationAttributes attributes, int scenarioID = 0) : base(attributes.EventAttributes, scenarioID)
        {
            this.Attributes = attributes;
            Initialize();
        }

        public void Initialize(bool isComplete = true)
        {
            Log.Info($" Start initialization of {this.GetType()}");
            InitializeEventTimes(this.Attributes.EventAttributes);
            Bid_Repo = new BidRepository(-2); //TODO: Zeit in Attributes speichern
            if (isComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public virtual void SetSettings()
        {
            Log.Info($"Set Settings of {this.GetType()}");
            InitializeEventTimes(this.Attributes.EventAttributes);
        }

        public void CoupleToMarket(Market marketCoupledTo)
        {
            MarketCoupledTo = marketCoupledTo;
            EventTimeframe = (EventTimeframe.TimeStamp, EventTimeframe.Duration, EventTimeframe.Step);
        }
        #endregion


        public void SubmitActivation(Market calling, Bid_ControlReserve_Activation activation)
        {
            if (calling.Id == MarketCoupledTo.Id)
            {
                Activations.Add(activation);
            }
        }

        public void SubmitActivation(ControlReserveActivationVolumeHandler calling, Bid_ControlReserve_Activation activation)
            => Activations.Add(activation);

        /// <summary>
        /// <see langword="abstract"/> Methode zum Abruf von Auktions-Eventargumenten
        /// </summary>
        /// <returns>Auktionsargumente <seealso cref="AuctionArgs"/></returns>
        protected virtual ActivationArgs GetActivationArgs() => new ActivationArgs(this.EventTimeframe, this.Attributes.AuctionStep);



        /// <summary>
        /// Methode zur Auslösung des Auktionsevents - überprüft Übergabezeitpunkt und löst Auktionsevent aus
        /// </summary>
        /// <param name="time">Simulationszeit als <see cref="DateTime"/></param>
        public virtual void HandleActivation(DateTime time = default)
        {
            if (IsEventTime(time))
            {
                Log.Info("Automatic invocation of Activation - Market: " + this.GetType().Name + $" @ {this.EventTimeframe.TimeStamp}");

                InvokeActivationEvent(GetActivationArgs());

                // Activierung durchführen
                ExecuteActivation(out List<MarketResult> results);

                UpdateResults(results);

                // Clearing der Gebote
                ClearActivations();

                UpdateEventTiming();

                SaveBidsToDb();
            }
        }

        protected void InvokeActivationEvent(ActivationArgs activationArgs)
        {
            var delegates = Activation?.GetInvocationList();

            if (delegates != null) Parallel.ForEach(delegates, d => d.DynamicInvoke(this, activationArgs));
        }

        private void ExecuteActivation(out List<MarketResult> results)
        {
            //DateTime timeStart = DateTime.Now;
            //Log.Info("Execution started at: " + timeStart.ToLongTimeString());
            results = new List<MarketResult>();

            foreach (int controlReserveType in ControlReserveShortCode.GetAuctionTypes())
            {
                SegmentsActivation = GetSegmentsForActivation((ControlReserveShortCode.ShortCode)controlReserveType).ToList();

                var Results = CalcResults((ControlReserveShortCode.ShortCode)controlReserveType);
                //var Results = ActivationProcessHandler.ProcessMarketResult(GetActivationProcessArgs(SegmentsActivation, (ControlReserveShortCode.ShortCode)controlReserveType));

                results.AddRange(Results);
            }
            //var timeExecutionEnd = DateTime.Now;
            //Log.Info("Execution ended at: " + timeExecutionEnd.ToLongTimeString());
            //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
        }

        protected virtual List<ControlReserveResult> CalcResults(ControlReserveShortCode.ShortCode controlReserveType)
        {
            DateTime DurationEnd = EventTimeframe.TimeStamp.Add(EventTimeframe.Duration);
            List<ControlReserveResult> Results = new List<ControlReserveResult>();
            var IsNegative = controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_aFRR || controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_mFRR;
            for (DateTime time = EventTimeframe.TimeStamp; time < DurationEnd; time = time.Add(EventTimeframe.Step))
            {
                var segmentsTime = SegmentsActivation.Where(seg => seg.TimeStamp == time).ToList();
                
                var result = ControlReserve_Extensions.CalculateMarketResultsPayAsBidOneBuyer(segmentsTime, isEnergy: true, isNegative: IsNegative, Lower_Bound_Price: this.Attributes.Lower_Price_Bound, Upper_Price_Bound: this.Attributes.Upper_Price_Bound);
                if (result != null) Results.Add(result);
            }
            return Results;
        }


        private ActivationVolumeArgs GetActivationVolumeArgs(ControlReserveShortCode.ShortCode controlReserveType)
        {
            ActivationVolumeArgs args = new ActivationVolumeArgs(EventTimeframe.TimeStamp, EventTimeframe.Duration, EventTimeframe.Step, this.Attributes.AuctionStep, controlReserveType);
            return args;
        }

        protected IEnumerable<Bid_Segment> GetSegmentsForActivation(ControlReserveShortCode.ShortCode controlReserveType)
        {
            if (Activations != null)
                return Activations.Where(bid => bid.IsControlReserve_Activation()).Where(bid => bid.IsValid(EventTimeframe.TimeStamp, EventTimeframe.Duration, EventTimeframe.Step))
                                  .SelectMany(bid => ((Bid_ControlReserve_Activation)bid).GetValidSegments(EventTimeframe.TimeStamp, EventTimeframe.Duration, controlReserveType))
                                  .ToList();
            else
                return null;
        }

        private void ClearActivations()
        {
            var ActivationsGroupedByAssets = this.Activations.GroupBy(item => item.Assets, (el1, el2) => el2).ToList();
            //ActivationsGroupedByAssets.ToList().ForEach(bids => bids.Where(bid => bid.IsControlReserve()).Where(bid => bid.IsControlReserve_Activation()).ToList().ForEach(bid => ((Bid_ControlReserve_Activation)bid).Activate(this)));
            Parallel.ForEach(ActivationsGroupedByAssets.ToList(), bids => bids.Where(bid => bid.IsControlReserve()).Where(bid => bid.IsControlReserve_Activation()).ToList().ForEach(bid => ((Bid_ControlReserve_Activation)bid).Activate(this)));
            Parallel.ForEach(ActivationsGroupedByAssets.ToList(), bids => bids.Where(bid => bid.IsControlReserve()).Where(bid => bid.IsControlReserve_Activation()).ToList().ForEach(bid => ((Bid_ControlReserve_Activation)bid).UpdateAssetsCallProbability()));
        }

        private ProcessArgs GetActivationProcessArgs(IEnumerable<Bid_Segment> segments, ControlReserveShortCode.ShortCode controlReserveType)
        {
            ProcessArgs args = new ProcessArgs(EventTimeframe.TimeStamp, EventTimeframe.Duration, EventTimeframe.Step, segments, controlReserveType);
            return args;
        }

        protected virtual void UpdateResults(List<MarketResult> marketResults)
        {
            this.Results = new ActivationResultHandler() { _results = marketResults.ToList() };
            //this.MarketResultRepo.Add(marketResults);
            MarketResultRepository.Add(marketResults);
            using (var repo = new PredictionDataRepository())
            {
                repo.AddMarketResults(marketResults);
            }
        }

        public void SaveBidsToDb() => Bid_Repo.SaveToDb();

    }

    public class ActivationResultHandler
    {
        internal List<MarketResult> _results;
        public MarketResult Get(ShortCode shortCode, DateTime timeStamp) => _results.SingleOrDefault(result => result.ShortCode == shortCode && result.TimeStamp == timeStamp);
    }
}
