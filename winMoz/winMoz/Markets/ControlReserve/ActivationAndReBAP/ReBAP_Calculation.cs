﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Results;
using winMoz.Helper;
using winMoz.Markets.Bids;
using winMoz.Simulation;
using winMoz.Markets.Elements;
using winMoz.Data.Access.Repositories.Prediction;

namespace winMoz.Markets.ControlReserve
{
    /// <summary>
    /// Klasse zur Bestimmung des Ausgleichsenergiepreises
    /// </summary>
    public class ReBAP_Calculation : SimEvent
    {
        public SimEventAttributes Attributes { get; private set; }
        public IGCC_Calculation IGCC { get; set; }
        private reBAP_ResultHandler Results { get; set; }
        private DateTime reBAPEventTime { get; set; }

        bool WithIGCC;

        private ILookup<ShortCode,  ControlReserveResult> ControlReserveResults;

        public int IGCCId { get; private set; }

        public delegate void SimEventHandler(SimEvent sender, SimEventArgs eventArgs);

        /// <summary>
        /// <seealso cref="EventHandler"/> zur Auslösung des Events
        /// </summary>
        public event SimEventHandler StartCalculation;

        private ReBAP_Calculation() { }
        public ReBAP_Calculation(SimEventAttributes attributes, IGCC_Calculation igcc, bool isComplete = true, int scenarioID = 0) : base(attributes, scenarioID)
        {
            Log.Info($" Start initialization of {this.GetType()}");
            this.Attributes = attributes;
            IGCC = igcc;
            IGCCId = IGCC.Id;
            WithIGCC = IGCC == null ? false : true;
            Attributes = attributes;
            if (isComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public void Initialize()
        {
            InitializeEventTimes(this.Attributes);
        }

        public virtual void SetSettings()
        {
            Log.Info($"Set Settings of {this.GetType()}");
            InitializeEventTimes(this.Attributes);
        }

        public void SetIGCC(List<IGCC_Calculation> igccs)
        {
            if (IGCC == null)
            {
                IGCC = igccs.FirstOrDefault(item => item.Id == IGCCId);
                WithIGCC = true;
            }
        }

        public void Handle_reBAP_Calculation(DateTime time)
        {
            if (IsEventTime(time))
            {
                Log.Info("Automatic invocation of Calculation: " + this.GetType().Name + $" @ {this.EventTimeframe.TimeStamp}");
                var results = Calculate_reBAPs();
                UpdateResults(results);
                UpdateEventTiming();
            }
        }

        /// <summary>
        /// Berechnung des Ausgleichsenergiepreis vorläufig
        /// </summary>
        /// <param name="timeStamp">Zeitpunkt, zu dem der Ausgleichsenergiepreis wirkt</param>
        protected virtual List<MarketResult> Calculate_reBAPs()
        {
            List<MarketResult> results = new List<MarketResult>();
            DateTime DurationEnd = EventTimeframe.TimeStamp.Add(EventTimeframe.Duration);
            CacheControlReserveResults(EventTimeframe.TimeStamp, EventTimeframe.Duration);
            for (DateTime time = EventTimeframe.TimeStamp; time < DurationEnd; time = time.Add(EventTimeframe.Step))
            {
                var CostRevenuesSum = GetCostRevenueSum(time, EventTimeframe.Step);
                var VolumeSum = GetVolumeSum(time, EventTimeframe.Step);
                var AP_Max_List = Get_AP_Max_List(time, EventTimeframe.Step);
                var LimitList = GetLimitList(time, EventTimeframe.Step);
                //var MarketResultIntraday = MarketResultRepo.IntradayResultsRepo.Get().Where(el => el.ShortCode == ShortCode.IntraDay.QuarterHour(time) && el.TimeStamp == time).FirstOrDefault();
                var MarketResultIntraday = MarketResultRepository.IntradayResultsRepo.Get().Where(el => el.ShortCode == ShortCode.IntraDay.QuarterHour(time) && el.TimeStamp == time).FirstOrDefault();
                var P_id = MarketResultIntraday != default ? MarketResultIntraday.Price : 0;
                //Berechnet den Ausgleichsenergiepreis (Börsenpreiskopplung erfordert Argumente)
                float AEP1H = AEP1(VolumeSum, CostRevenuesSum);
                float AEP2H = AEP2(AEP1H, AP_Max_List);
                float AEP20H = AEP20(AEP2H, VolumeSum, P_id);
                float AEP3H = AEP3(AEP20H, VolumeSum, P_id);
                float AEP4H = AEP4(AEP3H, VolumeSum, LimitList);
                if (float.IsNaN(AEP4H)) AEP4H = 9999999999999;
                bool Limit80PercentExceeded = AEP4H != AEP3H;
                results.Add(new ControlReserveResult(time, ShortCode.ControlReserve.reBAP(time, EventTimeframe.Step), AEP4H, 0, price_Min: 0, price_Max: 0, proRataVolumePurchase: 0, proRataVolumeSell: 0));
            }
            return results;
        }

        private float GetVolumeSum(DateTime time, TimeSpan step)
        {
            var VolumeSum = GetControlReserveResultVolume(ControlReserveShortCode.ShortCode.p_aFRR, time, step)
                + GetControlReserveResultVolume(ControlReserveShortCode.ShortCode.p_mFRR, time, step)
                + GetControlReserveResultVolume(ControlReserveShortCode.ShortCode.n_aFRR, time, step)
                + GetControlReserveResultVolume(ControlReserveShortCode.ShortCode.n_mFRR, time, step);
            return VolumeSum;
        }

        private void CacheControlReserveResults(DateTime time, TimeSpan duration)
        {
            DateTime EndOfDuration = time.Add(duration);
            ControlReserveResults = MarketResultRepository.ControlReserveResultsRepo.Get().Where(item => item.TimeStamp >= time && item.TimeStamp < EndOfDuration).ToLookup(item => item.ShortCode, item => item);
        }

        private float GetCostRevenueSum(DateTime time, TimeSpan step)
        {
            var CostRevenuesSum = GetControlReserveResultCostsRevenues(ControlReserveShortCode.ShortCode.p_aFRR, time, step)
                + GetControlReserveResultCostsRevenues(ControlReserveShortCode.ShortCode.p_mFRR, time, step)
                + GetControlReserveResultCostsRevenues(ControlReserveShortCode.ShortCode.n_aFRR, time, step)
                + GetControlReserveResultCostsRevenues(ControlReserveShortCode.ShortCode.n_mFRR, time, step);
            if (WithIGCC) CostRevenuesSum += GetCostRevenuesIGCC(time);
            return CostRevenuesSum;
        }

        private float GetControlReserveResultCostsRevenues(ControlReserveShortCode.ShortCode shortCode, DateTime timeStamp, TimeSpan step)
        {
            //var result = MarketResultRepo.ControlReserveRepo.Get().Where(el => el.ShortCode == ShortCode.ControlReserve.ActivationFromEnum(shortCode, timeStamp, step) && el.TimeStamp == timeStamp).FirstOrDefault();
            var result = ControlReserveResults[ShortCode.ControlReserve.ActivationFromEnum(shortCode, timeStamp, step)].Where(el => el.TimeStamp.Ticks == timeStamp.Ticks).FirstOrDefault();
            return result != null ? result.Price * result.Volume : 0;
        }

        private float GetControlReserveResultVolume(ControlReserveShortCode.ShortCode shortCode, DateTime timeStamp, TimeSpan step)
        {
            //var result = MarketResultRepo.ControlReserveRepo.Get().Where(el => el.ShortCode == ShortCode.ControlReserve.ActivationFromEnum(shortCode, timeStamp, step) && el.TimeStamp == timeStamp).FirstOrDefault();
            var result = ControlReserveResults[ShortCode.ControlReserve.ActivationFromEnum(shortCode, timeStamp, step)].Where(el => el.TimeStamp.Ticks == timeStamp.Ticks).FirstOrDefault();
            return result != null ? result.Volume : 0;
        }

        private float GetControlReserveResultAPMax(ControlReserveShortCode.ShortCode shortCode, DateTime timeStamp, TimeSpan step)
        {
            //var result = MarketResultRepo.ControlReserveRepo.Get().Where(el => el.ShortCode == ShortCode.ControlReserve.ActivationFromEnum(shortCode, timeStamp, step) && el.TimeStamp == timeStamp).FirstOrDefault();
            var result = ControlReserveResults[ShortCode.ControlReserve.ActivationFromEnum(shortCode, timeStamp, step)].Where(el => el.TimeStamp.Ticks == timeStamp.Ticks).FirstOrDefault();
            return result != null ? result.Price_Max : 0;
        }

        private float GetCostRevenuesIGCC(DateTime time)
            => IGCC.GetIGCC_Price(time) * IGCC.GetIGCC_Volume(time);

        private List<float> Get_AP_Max_List(DateTime time, TimeSpan step)
        {
            var AP_Max_List = new List<float>
            {
                GetControlReserveResultAPMax(ControlReserveShortCode.ShortCode.p_aFRR, time, step),
                GetControlReserveResultAPMax(ControlReserveShortCode.ShortCode.p_mFRR, time, step),
                GetControlReserveResultAPMax(ControlReserveShortCode.ShortCode.n_aFRR, time, step),
                GetControlReserveResultAPMax(ControlReserveShortCode.ShortCode.n_mFRR, time, step)
            };
            return AP_Max_List;
        }

        private Dictionary<ControlReserveShortCode.ShortCode, int> GetLimitList(DateTime time, TimeSpan duration)
        {
            var LimitList = new Dictionary<ControlReserveShortCode.ShortCode, int>
            {
                { ControlReserveShortCode.ShortCode.p_aFRR, GetTenderVolume(new DemandArgs(time, EventTimeframe.Step, EventTimeframe.Step, ControlReserveShortCode.ShortCode.p_aFRR))},
                {ControlReserveShortCode.ShortCode.p_mFRR, GetTenderVolume(new DemandArgs(time, EventTimeframe.Step, EventTimeframe.Step, ControlReserveShortCode.ShortCode.p_mFRR)) },
                {ControlReserveShortCode.ShortCode.n_aFRR, GetTenderVolume(new DemandArgs(time, EventTimeframe.Step, EventTimeframe.Step, ControlReserveShortCode.ShortCode.n_aFRR)) },
                {ControlReserveShortCode.ShortCode.n_mFRR, GetTenderVolume(new DemandArgs(time, EventTimeframe.Step, EventTimeframe.Step, ControlReserveShortCode.ShortCode.n_mFRR)) }
            };
            return LimitList;
        }

        private int GetTenderVolume(DemandArgs args)
        {
            var TenderVolumes = ControlReserveDemandRepository.GetTenderVolumeActivation(args);
            return TenderVolumes != default ? TenderVolumes.Demand : 0; 
        }


        #region AEPs
        /// <summary>
        /// Berechnung des AEP1
        /// Berechnet sich aus Kosten minus Erlöse durch Saldo
        /// </summary>
        private static float AEP1(float volumeSum, float costRevenuesSum) => volumeSum != 0 ? costRevenuesSum * (float)Math.Pow(volumeSum, -1) : 9999999;

        /// <summary>
        /// Berechnung des AEP2 -> Bergrenzung durch AP_Max (höchstes bezuschlagtes Arbeitspreisgebot) der Viertelstunde
        /// Bei kleinen Saldi wird so ein sehr hoher Arbeitspreis verhindert
        /// </summary>
        private static float AEP2(float AEP1, IEnumerable<float> AP_Max_List)
        {
            float APMax = AP_Max_List.Count() > 0 ? AP_Max_List.Max() : 9999999;
            return AEP1 >= 0 ? (float)Math.Min(AEP1, APMax) : (float)-Math.Min(-AEP1, APMax);
        }

        /// <summary>
        /// Berechnung des AEP20
        /// </summary>
        private static float AEP20(float AEP2, float volumeSum, float P_id)
        {
            float Balance = volumeSum;
            if (AEP2 < 0 & Balance >= -125 & Balance <= 125) return -Math.Min(Math.Abs(AEP2), Math.Abs(P_id - 100 - 150 * Math.Abs(Balance / 125)));
            else if (AEP2 >= 0 & Balance >= -125 & Balance <= 125) return Math.Min(Math.Abs(AEP2), Math.Abs(P_id + 100 + 150 * Math.Abs(Balance / 125)));
            else return AEP2;
        }

        /// <summary>
        /// Berechnung des AEP3 -> Intradaymarktbindung (Stundentarife)
        /// Begrenzung des Ausgeleichsenergiepreises durch gewichtetes Mittel der Intraday-Stundengebote
        /// </summary>
        private static float AEP3(float AEP20, float volumeSum, float P_id)
           => volumeSum < 0 ? Math.Min(P_id, AEP20) : Math.Max(P_id, AEP20);

        /// <summary>
        /// Berechnung des AEP4 -> Intradaymarktbindung (Viertelstundetarife)
        /// Begrenzung des Ausgeleichsenergiepreises durch gewichtetes Mittel der Intraday-Viertelstundengebote
        /// </summary>
        private static float AEP4(float AEP3, float volumeSum, Dictionary<ControlReserveShortCode.ShortCode, int> demands)
        {
            float Balance = volumeSum;
            float RL_pos = demands[ControlReserveShortCode.ShortCode.p_aFRR] + demands[ControlReserveShortCode.ShortCode.p_mFRR];
            float RL_neg = demands[ControlReserveShortCode.ShortCode.n_aFRR] + demands[ControlReserveShortCode.ShortCode.n_mFRR];
            if (Balance > 0.8 * RL_pos) return AEP3 + (float)Math.Max(100, 0.5 * Math.Abs(AEP3));
            else if (Balance < -0.8 * RL_neg) return AEP3 - (float)Math.Max(100, 0.5 * Math.Abs(AEP3));
            else return AEP3;
        }
        #endregion

        protected virtual void UpdateResults(List<MarketResult> marketResults)
        {
            this.Results = new reBAP_ResultHandler() { _results = marketResults.ToList() };
            //this.MarketResultRepo.Add(marketResults);
            MarketResultRepository.Add(marketResults);
            using (var repo = new PredictionDataRepository())
            {
                repo.AddMarketResults(marketResults);
            }
        }

    }

    public class reBAP_ResultHandler 
    {
        internal List<MarketResult> _results;
        public MarketResult Get(ShortCode shortCode, DateTime timeStamp) => _results.SingleOrDefault(result => result.ShortCode == shortCode && result.TimeStamp == timeStamp);
    }
}
