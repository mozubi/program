﻿using System;

namespace winMoz.Markets.Elements
{
    public class ActivationArgs : EventArgs
    {
        public DateTime TimeStamp { get; set; }
        public TimeSpan Duration { get; set; }

        public TimeSpan Step { get; set; }
        public TimeSpan AuctionStep { get; set; }

        public ActivationArgs((DateTime TimeStamp, TimeSpan Duration, TimeSpan Step) activationTime, TimeSpan auctionStep)
        {
            this.TimeStamp = activationTime.TimeStamp;
            this.Duration = activationTime.Duration;
            this.Step = activationTime.Step;
            this.AuctionStep = auctionStep;
        }
    }

}

