﻿using System;
using winMoz.Markets;

namespace winMoz.Markets.ControlReserve
{
    public class DemandArgs
    {
        public DateTime DeterminationDate { get; set; }
        public TimeSpan Duration { get; set; }
        public TimeSpan StepDuration { get; set; }
        public ControlReserveShortCode.ShortCode ControlReserveType { get; set; }

        public DemandArgs(DateTime determinationDate, TimeSpan duration, TimeSpan stepDuration, ControlReserveShortCode.ShortCode controlReserveType)
        {
            DeterminationDate = determinationDate;
            Duration = duration;
            StepDuration = stepDuration;
            ControlReserveType = controlReserveType;
        }
    }
}
