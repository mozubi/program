﻿using System;
using System.Collections.Generic;
using winMoz.Markets;
using winMoz.Markets.Results;

namespace winMoz.Markets.ControlReserve
{
    public class ActivationVolumeArgs
    {
        public DateTime DeterminationDate { get; set; }
        public TimeSpan Duration { get; set; }
        public TimeSpan StepDuration { get; set; } 

        public TimeSpan LimitStepDuration { get; set; }
        public ControlReserveShortCode.ShortCode ControlReserveType { get; set; }

        public ActivationVolumeArgs(DateTime determinationDate, TimeSpan duration, TimeSpan stepDuration, TimeSpan limitStepDuration, ControlReserveShortCode.ShortCode controlReserveType)
        {
            DeterminationDate = determinationDate;
            Duration = duration;
            StepDuration = stepDuration;
            LimitStepDuration = limitStepDuration;
            ControlReserveType = controlReserveType;
        }
    }
}
