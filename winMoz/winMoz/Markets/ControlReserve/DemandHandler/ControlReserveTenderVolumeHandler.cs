﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Data.Access.Contexts;
using winMoz.Helper;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;
using winMoz.Simulation;
using winMoz.Markets.Elements;

namespace winMoz.Markets.ControlReserve
{
    /// <summary>
    /// Klasse zur Bestimmung des Regelleistungsbedarfs nach k-mean-Clustering
    /// </summary>
    public class ControlReserveTenderVolumeHandler : SimObject
    {
        #region Basic
        //protected ControlReserveDemandRepository TenderVolume_Repo;
        public List<Market> Markets { get; private set; }

        public List<int> MarketIDs { get; private set; }

        public void CoupleToMarketAuction(ControlReserveMarket market)
        {
            market.Auction += PutDemandBidToAuction;
        }

        public void PutDemandBidToAuction(Market calling, AuctionArgs args)
        {
            if (calling.IsControlReserveMarket())
            {
                if (calling.IsControlReserve_Capacity())
                {
                    foreach (int type_number in ControlReserveShortCode.GetAuctionTypes())
                    {
                        var Demands = GetDemand(new DemandArgs(args.TimeStamp, args.Duration, args.Step, (ControlReserveShortCode.ShortCode)type_number));
                        Bid_ControlReserve_Capacity bid_Cap = new Bid_ControlReserve_Capacity(calling);
                        var DurationEnd = args.TimeStamp.Add(args.Duration);
                        for (DateTime time = args.TimeStamp; time < DurationEnd; time = time.Add(args.Step))
                        {
                            bid_Cap.AddSegment(calling, ShortCode.ControlReserve.EnergyFromEnum((ControlReserveShortCode.ShortCode)type_number, time, args.Step), time, -Demands[time], 99999);
                            bid_Cap.AddSegment(calling, ShortCode.ControlReserve.CapacityFromEnum((ControlReserveShortCode.ShortCode)type_number, time, args.Step), time, -Demands[time], 99999);
                        }
                        calling.SubmitBid(bid_Cap);
                    }
                }
                else if (calling.IsControlReserve_Energy())
                {
                    foreach (int type_number in ControlReserveShortCode.GetAuctionTypes())
                    {
                        var Demands = GetDemand(new DemandArgs(args.TimeStamp, args.Duration, args.Step, (ControlReserveShortCode.ShortCode)type_number));
                        Bid_ControlReserve_Energy bid_Cap = new Bid_ControlReserve_Energy(calling);
                        var DurationEnd = args.TimeStamp.Add(args.Duration);
                        for (DateTime time = args.TimeStamp; time < DurationEnd; time = time.Add(args.Step))
                        {
                            bid_Cap.AddSegment(ShortCode.ControlReserve.EnergyFromEnum((ControlReserveShortCode.ShortCode)type_number, time, args.Step), time, -Demands[time], 99999);
                        }
                        calling.SubmitBid(bid_Cap);
                    }
                }
            }
        }

        public Dictionary<DateTime, int> GetDemand(DemandArgs args)
        {
            if (!ControlReserveDemandRepository.IsTenderVolumeTimeFrameCached(args))
                Determine_ControlReserve_Demand(args.DeterminationDate, args.Duration, args.StepDuration);
            return ControlReserveDemandRepository.GetTenderVolume(args).ToDictionary(item => item.TimeStamp, item => item.Demand);
        }
        #endregion

        #region Attributes
        public ControlReserveTenderVolumeAttributes Attributes { get; private set; }
        ///// <summary>
        ///// Defizit-/Überschusswahrscheinlichkeit für GRL = SRL + MRL
        ///// </summary>
        //public float ErrorLevel_FRR { get; private set; }// = 0.03 / 100;
        ///// <summary>
        ///// Defizit-/Überschusswahrscheinlichkeit für SRL
        ///// </summary>
        //public float ErrorLevel_aFRR { get; private set; }// = 0.2 / 100;
        /// <summary>
        /// Sammlung der Bedarfe für einen Zeitstempel
        /// </summary>
        protected Dictionary<ControlReserveShortCode.ShortCode, int> TenderVolumes { get; set; }
        /// <summary>
        /// k-Mean-Cluster für Regelleistung
        /// </summary>
        protected Dictionary<ControlReserveShortCode.ShortCode, Dictionary<int, Dictionary<DateTime, float>>> Balances_Cluster_ControlReserve { get; set; }
        /// <summary>
        /// Sammlung der Wahrscheinlichkeitsdichteverteilungen der k-mean-Cluster für die Regelleistung
        /// </summary>
        protected Dictionary<ControlReserveShortCode.ShortCode, Dictionary<int, Dictionary<int, float>>> Hist_Cluster_ControlReserve { get; set; }
        /// <summary>
        /// Mittelwerte der k-Mean-CLuster Regelleistung
        /// </summary>
        protected Dictionary<ControlReserveShortCode.ShortCode, float[]> MeanValuesCluster_ControlReserve { get; set; }
        /// <summary>
        /// Letzter Bestimmungszeitpunkt für die Regelleistung
        /// </summary>
        protected Dictionary<ControlReserveShortCode.ShortCode, DateTime> LastDetermination_ControlReserve { get; set; }
        /// <summary>
        /// Gewichtetetes Mittel der Wahrscheinlichkeitsverteilungen der k-mean-Cluster der Gesamtregelleistung. Gewichtung nach Referenzzeitpunkten.
        /// </summary>
        protected Dictionary<ControlReserveShortCode.ShortCode, Dictionary<int, float>> LastHistWeighted_ControlReserve { get; set; }



        public DateTime HistoricStart { get; protected set; }

        #endregion

        #region Initialisierung

        private ControlReserveTenderVolumeHandler() { }

        /// <summary>
        /// Konstruktor für Bedarfsklasse mit Berechnung nach k-Mean Clustering
        /// </summary>
        public ControlReserveTenderVolumeHandler(ControlReserveTenderVolumeAttributes attributes, List<Market> markets, int scenarioID = 0) : base(scenarioID)
        {
            Attributes = attributes;
            HistoricStart = Simulator.Times.Start.AddYears(-10);
            Markets = markets;

            MarketIDs = Markets.Select(item => item.Id).ToList();
            Initialize();
        }


        public virtual void Initialize(bool isComplete = true)
        {
            Log.Info($" Start initialization of {this.GetType()}");
            DateTime HistoricEnd = Simulator.Times.Start;
            TimeSpan HistoricDuration = HistoricEnd - HistoricStart;
            using (var repo = new ScenarioData_Repository())
            {
                repo.MarketRepository.SetScenarioID(this.ScenarioID);
            }
            ControlReserveDemandRepository.CacheHistoric(HistoricStart, HistoricDuration);
            foreach (ControlReserveMarket market in Markets.Select(item => (ControlReserveMarket)item).ToList()) CoupleToMarketAuction(market);
            InitializeDemands();
            Balances_Cluster_ControlReserve = new Dictionary<ControlReserveShortCode.ShortCode, Dictionary<int, Dictionary<DateTime, float>>> { { ControlReserveShortCode.ShortCode.aFRR, new Dictionary<int, Dictionary<DateTime, float>>() }, { ControlReserveShortCode.ShortCode.FRR, new Dictionary<int, Dictionary<DateTime, float>>() } };
            Hist_Cluster_ControlReserve = new Dictionary<ControlReserveShortCode.ShortCode, Dictionary<int, Dictionary<int, float>>> { { ControlReserveShortCode.ShortCode.aFRR, new Dictionary<int, Dictionary<int, float>>() }, { ControlReserveShortCode.ShortCode.FRR, new Dictionary<int, Dictionary<int, float>>() } };
            MeanValuesCluster_ControlReserve = new Dictionary<ControlReserveShortCode.ShortCode, float[]> { { ControlReserveShortCode.ShortCode.aFRR, new float[4] { 0, 0, 0, 0 } }, { ControlReserveShortCode.ShortCode.FRR, new float[4] { 0, 0, 0, 0 } } };
            LastDetermination_ControlReserve = new Dictionary<ControlReserveShortCode.ShortCode, DateTime> { { ControlReserveShortCode.ShortCode.aFRR, new DateTime(1900, 1, 1) }, { ControlReserveShortCode.ShortCode.FRR, new DateTime(1900, 1, 1) } };
            LastHistWeighted_ControlReserve = new Dictionary<ControlReserveShortCode.ShortCode, Dictionary<int, float>> { { ControlReserveShortCode.ShortCode.aFRR, new Dictionary<int, float>() }, { ControlReserveShortCode.ShortCode.FRR, new Dictionary<int, float>() } };
            if (isComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public virtual void SetSettings()
        {
            Log.Info($"Set Settings of {this.GetType()}");
        }

        public void SetMarkets(List<Market> markets)
        {
            if (Markets == null || Markets.Count() == 0)
            {
                Markets = markets.Where(market => MarketIDs.Contains(market.Id)).ToList();
                foreach (ControlReserveMarket market in Markets.Select(item => (ControlReserveMarket)item).ToList()) CoupleToMarketAuction(market);
            }
        }

        /// <summary>
        /// Initialisieren der Bedarfssammlung
        /// </summary>
        public void InitializeDemands()
        {
            TenderVolumes = new Dictionary<ControlReserveShortCode.ShortCode, int>();
            for (int i = 0; i < 4; i++) TenderVolumes.Add((ControlReserveShortCode.ShortCode)i, 0);
        }
        #endregion

        #region Bestimmung der Regelleistungsbedarfe

        protected virtual void Determine_ControlReserve_Demand(DateTime determinationDate, TimeSpan duration, TimeSpan stepDuration)
        {
            DateTime EndDate = determinationDate.Add(duration);

            //Läuft einen Tag lang für sechs Vierstundenzeiträume
            for (DateTime DateSwitch = determinationDate; DateSwitch < EndDate; DateSwitch = DateSwitch.Add(stepDuration))
            {
                if (DateSwitch == determinationDate)
                {
                    //Regelleistungs-Bedarf berechnen
                    DateTime HistStart = Simulator.Times.Start.AddYears(-10);
                    DateTime EndOfDuration = determinationDate.Add(duration);
                    var HistBalances = ControlReserveDemandRepository.GetBalancesHistoricAllTypes(EndOfDuration);
                    Determine_ProbabilityDensityDistribution_Of_Demands(DateSwitch, stepDuration, HistBalances);
                }
                else
                {
                    //Regelleistungs-Bedarf berechnen (für selbe Auktion ändern sich die Werte in den CLustern nicht)
                    Determine_ProbabilityDensityDistribution_Of_Demands_For_Same_Auction(DateSwitch, stepDuration);
                }
                //SRL-Bedarf berechnen
                TenderVolumes[ControlReserveShortCode.ShortCode.n_aFRR] = FindValueForErrorlevel(LastHistWeighted_ControlReserve[ControlReserveShortCode.ShortCode.aFRR], Attributes.ErrorLevel_aFRR / 2);
                TenderVolumes[ControlReserveShortCode.ShortCode.p_aFRR] = FindValueForErrorlevel(LastHistWeighted_ControlReserve[ControlReserveShortCode.ShortCode.aFRR], 1 - Attributes.ErrorLevel_aFRR / 2);
                //MRL-Bedarf berechnen
                TenderVolumes[ControlReserveShortCode.ShortCode.n_mFRR] = FindValueForErrorlevel(LastHistWeighted_ControlReserve[ControlReserveShortCode.ShortCode.FRR], Attributes.ErrorLevel_FRR / 2) - TenderVolumes[ControlReserveShortCode.ShortCode.n_aFRR];
                TenderVolumes[ControlReserveShortCode.ShortCode.p_mFRR] = FindValueForErrorlevel(LastHistWeighted_ControlReserve[ControlReserveShortCode.ShortCode.FRR], 1 - Attributes.ErrorLevel_FRR / 2) - TenderVolumes[ControlReserveShortCode.ShortCode.p_aFRR];

                //Hinzufügen der Bedarfe zur Bedarfssammlung
                ControlReserveDemandRepository.AddTenderVolume(DateSwitch, new Dictionary<ControlReserveShortCode.ShortCode, int>(TenderVolumes));
            }
        }

        public int FindValueForErrorlevel(Dictionary<int, float> distribution, float ErrorLevel)
        {
            float Sum = 0;
            foreach (var item in distribution)
            {
                Sum += item.Value;
                if (Sum > ErrorLevel) return item.Key;
            }
            return distribution.Last().Key;
        }

        /// <summary>
        /// Bestimme die Wahrscheinlichkeitsdichteverteilung für den Gesamtregelleistungsbedarf
        /// </summary>
        /// <param name="determinationDate"></param>
        /// <param name="balances"></param>
        public virtual void Determine_ProbabilityDensityDistribution_Of_Demands(DateTime determinationDate, TimeSpan duration, Dictionary<ControlReserveShortCode.ShortCode, Dictionary<DateTime, float>> balances)
        {
            foreach (int typeNumber in ControlReserveShortCode.GetSyntethicDemandTypes())
            {
                Dictionary<int, float> HistWeighted = new Dictionary<int, float>();
                if (Helper_Time_Translation.GetStartTime(determinationDate, duration).CompareTo(Helper_Time_Translation.GetStartTime(LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], duration)) != 0)
                {
                    DateTime EndDate = new DateTime(determinationDate.Year, determinationDate.Month, determinationDate.Day, 0, 0, 0);
                    DateTime StartDate = EndDate.AddYears(-3).AddDays(-30);
                    Dictionary<DateTime, float> BalancesPart = balances[(ControlReserveShortCode.ShortCode)typeNumber].Where(item => item.Key >= StartDate & item.Key < EndDate).ToDictionary(item2 => item2.Key, item2 => item2.Value);
                    Dictionary<int, Dictionary<DateTime, float>> BalancesCluster = new Dictionary<int, Dictionary<DateTime, float>>();

                    float Mean = BalancesPart.Select(item => item.Value).Average();

                    //Neue Cluster bilden
                    (MeanValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], BalancesCluster) = CalculateCluster(MeanValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], BalancesPart);

                    //Veränderungen zu vorherigen Cluster suchen
                    Dictionary<int, Dictionary<DateTime, float>> BalancesClusterAdd = new Dictionary<int, Dictionary<DateTime, float>>();
                    Dictionary<int, Dictionary<DateTime, float>> BalancesClusterSubtract = new Dictionary<int, Dictionary<DateTime, float>>();
                    (BalancesClusterAdd, BalancesClusterSubtract) = GetClusterDiff(Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], BalancesCluster);

                    //Wahrscheinlichektisdichteverteilung für die Cluster bilden
                    Dictionary<int, Dictionary<int, float>> HistCluster = new Dictionary<int, Dictionary<int, float>>();
                    bool HistoricClusterCreated = false;
                    bool HistoricDataCreated = false;
                    if (Hist_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber].Count() > 0)
                    {
                        HistoricClusterCreated = true;
                        HistoricDataCreated = Hist_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber].All(item => item.Value.Count() > 0);
                    }
                    HistCluster = GetProbDensClusterFromDiff(BalancesClusterAdd, BalancesClusterSubtract, BalancesCluster, HistoricClusterCreated, HistoricDataCreated, (ControlReserveShortCode.ShortCode)typeNumber);

                    //Speichern der Cluster für die Werte und die Histogramme
                    Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = new Dictionary<int, Dictionary<DateTime, float>>(BalancesCluster);
                    Hist_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = new Dictionary<int, Dictionary<int, float>>(HistCluster);

                    //Gewichtung nach Referenzzeitpunkten durchführen.
                    float[] Weighting = WeightingForRef(StartDate, determinationDate, EndDate, duration, BalancesCluster);

                    //Gewichtetes Mittel der Wahrscheinlichkeitsverteilungen der einzelnen Cluster  (nach oben berechneter Gewichtung) bilden.
                    HistWeighted = HistFromWeighting(Weighting, HistCluster);
                }
                else
                {
                    //Fall, wenn der Bestimmungszeitraum genau derselbe ist, wie vorher.
                    HistWeighted = LastHistWeighted_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber];
                }
                LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = determinationDate;
                LastHistWeighted_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = HistWeighted;
            }
        }

        /// <summary>
        /// Bestimme die Wahrscheinlichkeitsdichteverteilung für den Gesamtregelleistungsbedarf -> Werte für Cluster werden nur für jede Auktion geändert.
        /// Deshalb hier die Kurzfassung für nicht geänderte Werte
        /// </summary>
        /// <param name="determinationDate"></param>
        public virtual void Determine_ProbabilityDensityDistribution_Of_Demands_For_Same_Auction(DateTime determinationDate, TimeSpan duration)
        {
            foreach (int typeNumber in ControlReserveShortCode.GetSyntethicDemandTypes())
            {
                Dictionary<int, float> HistWeighted = new Dictionary<int, float>();
                if (Helper_Time_Translation.GetStartTime(determinationDate, duration).CompareTo(Helper_Time_Translation.GetStartTime(LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], duration)) != 0)
                {
                    DateTime EndDate = new DateTime(determinationDate.Year, determinationDate.Month, determinationDate.Day, 0, 0, 0);
                    DateTime StartDate = EndDate.AddYears(-3).AddDays(-30);

                    //Gewichtung nach Referenzzeitpunkten durchführen.
                    float[] Weighting = WeightingForRef(StartDate, determinationDate, EndDate, duration, Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber]);

                    //Gewichtetes Mittel der Wahrscheinlichkeitsverteilungen der einzelnen Cluster  (nach oben berechneter Gewichtung) bilden.
                    HistWeighted = HistFromWeighting(Weighting, Hist_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber]);
                }
                else
                {
                    //Fall, wenn der Bestimmungszeitraum genau derselbe ist, wie vorher.
                    HistWeighted = LastHistWeighted_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber];
                }
                LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = determinationDate;
                LastHistWeighted_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = HistWeighted;
            }
        }

        /// <summary>
        /// Bildung der Cluster
        /// Zuerst werden die Startwerte als Mittelwerte der CLuster definiert.
        /// Dann werden die Regelleistungswerte dem Cluster zugeordnet, 
        /// dessen Mittelwert die geringste quadratische Abweichung zum Wert besitzt.
        /// Anschließend wird für jeden Cluster der Mittelwert bestimmt und als neuer Mittelwert des Clusters bestimmt.
        /// Entsprechen diese Werte abgesehen von einer zulässigen Abweichung nicht den vorherigen Mittelwerten, werden die
        /// Regelleistungswerte erneut den CLustern zugeordnet.
        /// Ende des Algorythmus ist, wenn die Veränderung der Mittelwerte nicht über die erlaubte ABweichung hinausgeht.
        /// </summary>
        /// <param name="historicMean"></param>
        /// <param name="balances"></param>
        /// <returns></returns>
        private (float[] Mean, Dictionary<int, Dictionary<DateTime, float>> Balances) CalculateCluster(float[] historicMean, Dictionary<DateTime, float> balances)
        {
            Dictionary<int, Dictionary<DateTime, float>> BalancesCluster = new Dictionary<int, Dictionary<DateTime, float>>();
            Dictionary<int, HashSet<float>> BalancesClusterHashList = new Dictionary<int, HashSet<float>>();
            int i;
            int Clusternumber = 4;
            float[] MeanList = new float[4];

            //Startwerte ermitteln
            MeanList = MeanValues(historicMean, balances.Select(item => item.Value));

            // k-Means CLustern
            for (i = 0; i <= Clusternumber - 1; i++)
                BalancesCluster.Add(i, new Dictionary<DateTime, float>());
            bool isStart = true;
            while (isStart || BalancesClusterHashList.All(item => Math.Pow(item.Value.Select(item2 => item2).Average() - MeanList[item.Key], 2) <= 0.25) == false)
            {
                if (!isStart) //Schalter -> Startwerte bereits bestimmt
                              //Bestimmen der Mittelwerte
                    MeanList = BalancesClusterHashList.Select(item3 => item3.Value.Select(item4 => item4).Average()).ToArray();
                //Leeren der Cluster
                foreach (var item2 in BalancesCluster)
                    item2.Value.Clear();
                //Bestimmen der gerinsten Abweichung des Mittelwertes 
                //des Clusters zum einzusetzenden Regelleistungwert
                //und Einfügen des Wertes in diesen Cluster.
                foreach (var item in balances)
                {
                    var ClusterDivergence = BalancesClusterHashList.Select(item2 => (item2.Key, Math.Pow(item.Value - MeanList[item2.Key], 2)));
                    ClusterDivergence.ToList().ForEach(item2 => { if (item2.Item2 == ClusterDivergence.Select(item3 => item3.Item2).Min()) BalancesClusterHashList[item2.Key].Add(item.Value); });
                }
                isStart = false;
            }
            //Bestimmen der gerinsten Abweichung des Mittelwertes 
            //des Clusters zum einzusetzenden Regelleistungwert
            //und Einfügen des Wertes in diesen Cluster.
            foreach (var item in balances)
            {
                var ClusterDivergence = BalancesCluster.Select(item2 => (item2.Key, Math.Pow(item.Value - MeanList[item2.Key], 2)));
                ClusterDivergence.ToList().ForEach(item2 => { if (item2.Item2 == ClusterDivergence.Select(item3 => item3.Item2).Min()) BalancesCluster[item2.Key].Add(item.Key, item.Value); });
            }
            MeanList = BalancesCluster.Select(item3 => item3.Value.Select(item4 => item4.Value).Average()).ToArray();

            return (MeanList, BalancesCluster);
        }

        /// <summary>
        /// Berechnet den Unterschied zwischen Clustern der jetzigen und vorhergehenden Auktion
        /// Add -> Werte, die zu den alten Clustern hinzugefügt werden müssen.
        /// Subtract -> Werte, die den alten Clustern entnommen werden müssen
        /// </summary>
        /// <param name="historicValues"></param>
        /// <param name="newValues"></param>
        /// <returns></returns>
        public virtual (Dictionary<int, Dictionary<DateTime, float>> Add, Dictionary<int, Dictionary<DateTime, float>> Subtract) GetClusterDiff(Dictionary<int, Dictionary<DateTime, float>> historicValues, Dictionary<int, Dictionary<DateTime, float>> newValues)
        {
            Dictionary<int, Dictionary<DateTime, float>> BalancesAdd = new Dictionary<int, Dictionary<DateTime, float>>();
            Dictionary<int, Dictionary<DateTime, float>> BalancesSubtract = new Dictionary<int, Dictionary<DateTime, float>>();

            if (historicValues.Count() != 0)
                if (historicValues.All(item => item.Value.Count() != 0))
                    newValues.ToList().ForEach(item =>
                    {
                    BalancesAdd.Add(item.Key, historicValues[item.Key].Except(item.Value.AsEnumerable())
                        .ToDictionary(item2 => item2.Key, item2 => item2.Value));
                    BalancesSubtract.Add(item.Key, item.Value.Except(historicValues[item.Key].AsEnumerable())
                        .ToDictionary(item2 => item2.Key, item2 => item2.Value));
                    });

            return (BalancesAdd, BalancesSubtract);
        }

        /// <summary>
        /// Bestimmung der Gewichtung für die Cluster.
        /// Dabei fließen alle historischen Werte ein, die in den letzten 60 Tagen vor dem Bestimmungstag liegen
        /// und der Zeiträume jeweils 30 Tage vor und nach dem Bestimmungstag in den zwei Jahren vor dem Bestimmungtag.
        /// Zusätzlich wird nach dem Wochentag gefiltert (Mo, Di, Mi, Do, Fr, Sa, So + Feiertag).
        /// Zum Schluss wird nochmals nach dem Tageszeitraum gefiltert, sodass diese dem Bestimmungszeitraum am Bestimmungstag entsprechen.
        /// Für 0-4 Uhr werden somit nur Werte von 0:00 Uhr bis 3:45 Uhr gewählt.
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="BalancesCluster"></param>
        /// <returns></returns>
        private float[] WeightingForRef(DateTime StartDate, DateTime DeterminationDate, DateTime EndDate, TimeSpan duration, Dictionary<int, Dictionary<DateTime, float>> BalancesCluster)
        {
            DateTime LastStart = EndDate.AddDays(-60);
            var Durations = new List<(DateTime StartDate, DateTime EndDate)>();
            for(DateTime time = StartDate.Add(-duration); time < EndDate; time = time.AddYears(1))
            {
                if (time > LastStart) Durations.Add((LastStart, EndDate));
                else
                {
                    DateTime DurationStart = time;
                    DateTime DurationEnd = time.AddDays(60).Add(-duration);
                    Durations.Add((DurationStart, DurationEnd));
                }
            }
            Dictionary<int, DayValues> H_DV_List = new Dictionary<int, DayValues>();
            List<int> Years = Helper_Time_Translation.GetYearsFromDuration(StartDate, EndDate);
            Years.ForEach(year => H_DV_List.Add(year, new DayValues(year)));

            int NumberTimeSpan = Helper_Time_Translation.GetNumberTimeSpanDay(DeterminationDate, duration);
            int DayInt = H_DV_List[EndDate.Year].getDay("DE", DeterminationDate);
            var cluster1 = BalancesCluster[0].Where(item => H_DV_List[item.Key.Year].getDay("DE", item.Key) == DayInt);
            //Summieren des Vorkommens der Referenzzeitpunkte in den Clustern
            int[] Numbers = BalancesCluster
                .Select(cluster => cluster.Value
                .Where(item => Helper_Time_Translation.GetNumberTimeSpanDay(item.Key, duration) == NumberTimeSpan &&
                H_DV_List[item.Key.Year].getDay("DE", item.Key) == DayInt && Durations.Any(dur => item.Key >= dur.StartDate && item.Key < dur.EndDate)).Count()).ToArray();

            //Gewichtung der Cluster nach Häufigkeit des Vorkommens der Refenzzeitpunkte
            int Sum = Numbers.Sum();
            return Numbers.Select(item => (float)item / Sum).ToArray();
        }


        /// <summary>
        /// Wenn es bereits Cluster und eine Wahrscheinlichkeitsdichteverteilung gibt,
        /// berechne nur die Wahrscheinlichkeitsverteilung für die unterschiedlichen Werte
        /// (ergänzte und reduzierte Werte und bilde das gewichtete Mittel der beiden, 
        /// nach Anzahl der Elemente (reduzierte Elemente werden als negative Elemente gewertet
        /// mit einer ebenfalls negativen Wahrscheinlichketisverteilung) 
        /// </summary>
        /// <param name="balancesAdd"></param>
        /// <param name="balancesSubtract"></param>
        /// <param name="balancesCluster"></param>
        /// <param name="historicCLusterCreated"></param>
        /// <param name="historicDataCreated"></param>
        /// <returns></returns>
        private Dictionary<int, Dictionary<int, float>> GetProbDensClusterFromDiff(Dictionary<int, Dictionary<DateTime, float>> balancesAdd, Dictionary<int, Dictionary<DateTime, float>> balancesSubtract, Dictionary<int, Dictionary<DateTime, float>> balancesCluster, bool historicClusterCreated, bool historicDataCreated, ControlReserveShortCode.ShortCode type)
        {
            Dictionary<int, Dictionary<int, float>> HistCluster = new Dictionary<int, Dictionary<int, float>>();

            if (historicClusterCreated)
            {
                if (historicDataCreated)
                    HistCluster = DensityDistributions.PerformKernelDensityEstimationWithGaussianKernelAddSubtract(Hist_Cluster_ControlReserve[type],
                        balancesAdd.Select(item => (item.Key, item.Value.Select(item2 => item2.Value))), balancesSubtract.Select(item => (item.Key, item.Value.Select(item2 => item2.Value))), Balances_Cluster_ControlReserve[type].Select(item => item.Value.Count), 3000, 1); //Wahrscheinlichkeiten (nur Subtraction) schätzen
            }
            else
            {
                foreach (KeyValuePair<int, Dictionary<DateTime, float>> item in balancesCluster)
                {
                    var ListBalances = item.Value.Select(item2 => item2.Value).ToList();
                    HistCluster.Add(item.Key, DensityDistributions.PerformKernelDensityEstimationWithGaussianKernel(ListBalances, 3000, 1)); //Wahrscheinlichkeiten schätzen
                }
            }
            return HistCluster;
        }

        /// <summary>
        /// Bestimmung der Anfangswerte für das K-Mean-Clustern
        /// Es werden die 25 % und 75 % Percentile sowie die oberen und unteren Whisker der Verteilung genommen.
        /// </summary>
        /// <param name="historic"></param>
        /// <param name="balances"></param>
        /// <returns></returns>
        private float[] MeanValues(float[] historic, IEnumerable<float> balances)
        {
            float[] MeanList = new float[4];
            if (historic.All(item => item == 0))
            {
                //Bestimmung der Anfangswerte nach Percentilen und Whiskern der Verteilung
                float q25 = MathNet.Numerics.Statistics.Statistics.Quantile(balances, 0.25);
                float q75 = MathNet.Numerics.Statistics.Statistics.Quantile(balances, 0.75);
                float icr = q75 - q25;
                float upperWhisker = (float)Math.Min(q75 + 1.5 * icr, balances.Max());
                float lowerWhisker = (float)Math.Max(q25 - 1.5 * icr, balances.Min());
                MeanList = new float[4] { lowerWhisker, q25, q75, upperWhisker };
            }
            else
            {
                MeanList = historic;
            }
            return MeanList;
        }

        /// <summary>
        /// Gewichtetes Mittel der Wahrscheinlichkeitsdichteverteilungen der Cluster bilden.
        /// Gewichtung ist mit Weighting angegeben.
        /// </summary>
        /// <param name="weighting"></param>
        /// <param name="histCluster"></param>
        /// <returns></returns>
        private Dictionary<int, float> HistFromWeighting(float[] weighting, Dictionary<int, Dictionary<int, float>> histCluster)
        {
            Dictionary<int, float> HistWeighted = new Dictionary<int, float>();
            var histClusterWeighting = histCluster.Zip(weighting, (element1, element2) => (element1.Key, element1.Value.Select(item => (item.Key, item.Value * element2)).ToDictionary(item => item.Key, item => item.Item2))).ToDictionary(item => item.Key, item => item.Item2);
            HistWeighted = histClusterWeighting.Values.SelectMany(dict => dict)
                .GroupBy(item => item.Key, (item1, item2) => (item1, item2.Select(item => item.Value).Sum())).ToDictionary(item => item.item1, item => item.Item2);
            float Sum1 = HistWeighted.Select(item3 => item3.Value).Sum();
            HistWeighted = HistWeighted
                        .Select(item2 => new { Key1 = item2.Key, Value1 = item2.Value * (1 / Sum1) })
                        .ToDictionary(item3 => item3.Key1, item3 => item3.Value1)
                        .OrderBy(item => item.Key)
                        .ToDictionary(item2 => item2.Key, item2 => item2.Value);
            return HistWeighted;
        }
        #endregion
    }
}
