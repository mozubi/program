﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Data.Access.Repositories;
using winMoz.Simulation;

namespace winMoz.Markets.ControlReserve
{
    public class IGCC_Calculation : SimObject
    {
        private MathNet.Numerics.Random.MersenneTwister Random;

        private MathNet.Numerics.Distributions.Normal Normal_Volume;

        private MathNet.Numerics.Distributions.Normal Normal_Price;

        private float Average_Volume;

        private float StandardDeviation_Volume;

        private float Average_Price_Start;

        private float StandardDeviation_Price;

        private Dictionary<DateTime, float> LastVolumes;

        private Dictionary<DateTime, float> LastPrices;

        private Dictionary<MarketAreaEnum, (float slope, float priceAxisShift)> Shift_Average_Price_DayAhead;

        private IGCC_Calculation() {  }
        public IGCC_Calculation(int scenarioID = 0) : base(scenarioID)
        {
            Initialize();
        }

        public void Initialize(bool isComplete = true)
        {
            Log.Info($" Start initialization of {this.GetType()}");
            Random = new MathNet.Numerics.Random.MersenneTwister(seed: 10000, threadSafe: true);
            Average_Volume = -35.52F;
            StandardDeviation_Volume = 109.61F;
            Average_Price_Start = 39.51F;
            StandardDeviation_Price = 109.61F;
            Normal_Volume = new MathNet.Numerics.Distributions.Normal(Average_Volume, StandardDeviation_Volume, Random);
            Normal_Price = new MathNet.Numerics.Distributions.Normal(Average_Price_Start, StandardDeviation_Price, Random);
            Initialise_Average_Price_Shift();
            LastVolumes = new Dictionary<DateTime, float>();
            LastPrices = new Dictionary<DateTime, float>();
            //DA_Results = new ExternalMarketResultRepository();
            if (isComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public virtual void SetSettings()
        {
            Log.Info($"Set Settings of {this.GetType()}");
        }

        private void Initialise_Average_Price_Shift()
        {
            //Werte wurden aus Mittelwert für das Jahr 2018 aus den Werten des DayAhead und 
            //IGCC -Settlement-Preises mit Ausklammern seltener vorkommender Werte berechnet.
            Shift_Average_Price_DayAhead = new Dictionary<MarketAreaEnum, (float, float)>() {
                { MarketAreaEnum.CH, (0.282761647671292F, -14.6118776707792F) },
                { MarketAreaEnum.DK, (0.418291960697637F, -19.1012110306099F) },
                { MarketAreaEnum.FR, (0.371335121474057F, -18.4849687518683F) },
                { MarketAreaEnum.NL, (0.460961544731193F, -23.9229258375112F) },
                { MarketAreaEnum.DE, (0.318435263672833F, -15.2682046286108F) } };
        }

        public float GetIGCC_Volume(DateTime timeStamp)
        {
            if (LastVolumes.ContainsKey(timeStamp))
                return LastVolumes[timeStamp];
            else
            {
                var volume = (float)Normal_Volume.Sample();
                LastVolumes.Add(timeStamp, volume);
                return volume;
            }
        }

        public void CorrectVolume(DateTime timeStamp, float volume) => LastVolumes[timeStamp] = volume;

        public float GetIGCC_Price(DateTime timeStamp, Dictionary<MarketAreaEnum, decimal> dayAheadPrices = null)
        {
            if (LastPrices.ContainsKey(timeStamp))
                return LastPrices[timeStamp];
            else
            {
                float Average_Price = Average_Price_Start;
                foreach (var shift in Shift_Average_Price_DayAhead)
                {
                    (float slope, float priceAxisShift) = shift.Value;
                    Average_Price += GetPriceShift(slope, priceAxisShift, ExternalMarketResultRepository.Get(timeStamp.AddMinutes(-timeStamp.Minute)).SingleOrDefault(result => result.MarketArea == shift.Key).Price);
                }
                Normal_Price = new MathNet.Numerics.Distributions.Normal(Average_Price_Start, StandardDeviation_Price, Random);
                var price = (float)Normal_Price.Sample();
                LastPrices.Add(timeStamp, price);
                return price;
            }
        }

        public float GetPriceShift(float slope, float priceAxisShift, float price) => slope * price + priceAxisShift;
    }
}
