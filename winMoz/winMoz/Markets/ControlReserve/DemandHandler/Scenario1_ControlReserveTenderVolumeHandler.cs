﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Data.Access.Contexts;
using winMoz.Helper;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;
using winMoz.Simulation;

namespace winMoz.Markets.ControlReserve
{
    /// <summary>
    /// Klasse zur Bestimmung des Regelleistungsbedarfs nach k-mean-Clustering
    /// </summary>
    public class Scenario1_ControlReserveTenderVolumeHandler : ControlReserveTenderVolumeHandler
    {

        #region Attributes
        /// <summary>
        /// Standardabweichung der k-Mean-Cluster Regelleistung
        /// </summary>
        public Dictionary<ControlReserveShortCode.ShortCode, float[]> StandardDeviationValuesCluster_ControlReserve { get; private set; }
        /// <summary>
        /// Gewichtung der k-Mean-Cluster Regelleistung
        /// </summary>
        public Dictionary<ControlReserveShortCode.ShortCode, float[]> Weighting_ControlReserve { get; private set; }

        //Eigenschaften Sekundär- und Minutenregelleistungsabruf

        #endregion

        #region Initialisierung

        /// <summary>
        /// Konstruktor für Bedarfsklasse mit Berechnung nach k-Mean Clustering
        /// </summary>
        public Scenario1_ControlReserveTenderVolumeHandler(ControlReserveTenderVolumeAttributes attributes, List<Market> markets, int scenarioID = 0)
            : base(attributes, markets, scenarioID) 
        {
            //ControlReserve_Historic = demand_Repo.GetHistoricBalances();

        }

        public override void Initialize(bool isComplete = true)
        {
            base.Initialize(isComplete: false);
            StandardDeviationValuesCluster_ControlReserve = new Dictionary<ControlReserveShortCode.ShortCode, float[]> { { ControlReserveShortCode.ShortCode.aFRR, new float[4] { 0, 0, 0, 0 } }, { ControlReserveShortCode.ShortCode.FRR, new float[4] { 0, 0, 0, 0 } } };
            Weighting_ControlReserve = new Dictionary<ControlReserveShortCode.ShortCode, float[]> { { ControlReserveShortCode.ShortCode.aFRR, new float[4] { 0, 0, 0, 0 } }, { ControlReserveShortCode.ShortCode.FRR, new float[4] { 0, 0, 0, 0 } } };
            if (isComplete)
                Log.Info($"{this.GetType()} initialized.");
        }
        #endregion

        #region Bestimmung der Regelleistungsbedarfe

        protected override void Determine_ControlReserve_Demand(DateTime determinationDate, TimeSpan duration, TimeSpan stepDuration)
        {
            DateTime EndDate = determinationDate.Add(duration);

            //Läuft einen Tag lang für sechs Vierstundenzeiträume
            for (DateTime DateSwitch = determinationDate; DateSwitch < EndDate; DateSwitch = DateSwitch.Add(stepDuration))
            {
                if (DateSwitch == determinationDate)
                {
                    //Regelleistungs-Bedarf berechnen
                    DateTime HistStart = Simulator.Times.Start.AddYears(-10);
                    DateTime EndOfDuration = determinationDate.Add(duration);
                    var HistBalances = ControlReserveDemandRepository.GetBalancesHistoricAllTypes(EndOfDuration);
                    Determine_ProbabilityDensityDistribution_Of_Demands(DateSwitch, stepDuration, HistBalances);
                }
                else
                {
                    //Regelleistungs-Bedarf berechnen (für selbe Auktion ändern sich die Werte in den CLustern nicht)
                    Determine_ProbabilityDensityDistribution_Of_Demands_For_Same_Auction(DateSwitch, stepDuration);
                }
                //SRL-Bedarf berechnen
                //TODO: Nicht PDF sonder CDF
                TenderVolumes[ControlReserveShortCode.ShortCode.n_aFRR] = FindValueForErrorlevel((int)ControlReserveShortCode.ShortCode.aFRR, Attributes.ErrorLevel_aFRR / 2);
                TenderVolumes[ControlReserveShortCode.ShortCode.p_aFRR] = FindValueForErrorlevel((int)ControlReserveShortCode.ShortCode.aFRR, 1 - Attributes.ErrorLevel_aFRR / 2);
                //MRL-Bedarf berechnen
                TenderVolumes[ControlReserveShortCode.ShortCode.n_mFRR] = FindValueForErrorlevel((int)ControlReserveShortCode.ShortCode.FRR, Attributes.ErrorLevel_FRR / 2) - TenderVolumes[ControlReserveShortCode.ShortCode.n_aFRR];
                TenderVolumes[ControlReserveShortCode.ShortCode.p_mFRR] = FindValueForErrorlevel((int)ControlReserveShortCode.ShortCode.FRR, 1 - Attributes.ErrorLevel_FRR / 2) - TenderVolumes[ControlReserveShortCode.ShortCode.p_aFRR];

                //Hinzufügen der Bedarfe zur Bedarfssammlung
                ControlReserveDemandRepository.AddTenderVolume(DateSwitch, new Dictionary<ControlReserveShortCode.ShortCode, int>(TenderVolumes));
            }
        }


        /// <summary>
        /// Bestimme die Wahrscheinlichkeitsdichteverteilung für den Gesamtregelleistungsbedarf
        /// </summary>
        /// <param name="determinationDate"></param>
        /// <param name="balances"></param>
        public override void Determine_ProbabilityDensityDistribution_Of_Demands(DateTime determinationDate, TimeSpan duration, Dictionary<ControlReserveShortCode.ShortCode, Dictionary<DateTime, float>> balances)
        {
            foreach (int typeNumber in ControlReserveShortCode.GetSyntethicDemandTypes())
            {
                Dictionary<int, float> HistWeighted = new Dictionary<int, float>();
                if (Helper_Time_Translation.GetStartTime(determinationDate, duration).CompareTo(Helper_Time_Translation.GetStartTime(LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], duration)) != 0)
                {
                    DateTime EndDate = new DateTime(determinationDate.Year, determinationDate.Month, determinationDate.Day, 0, 0, 0);
                    DateTime StartDate = EndDate.AddYears(-3).AddDays(-30);
                    Dictionary<DateTime, float> BalancesPart = balances[(ControlReserveShortCode.ShortCode)typeNumber].Where(item => item.Key >= StartDate & item.Key < EndDate).ToDictionary(item2 => item2.Key, item2 => item2.Value);

                    float Mean = BalancesPart.Select(item => item.Value).Average();

                    //Neue Cluster bilden
                    Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = CalculateCluster(MeanValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], BalancesPart);
                    MeanValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = GetMeanOfCluster(Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber]);
                    StandardDeviationValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = GetStandardDeviationOfCluster(Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber]);

                    //Gewichtung nach Referenzzeitpunkten durchführen.
                    Weighting_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = WeightingForRef(StartDate, determinationDate, EndDate, duration, Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber]);
                }
                LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = determinationDate;
            }
        }

        /// <summary>
        /// Bestimme die Wahrscheinlichkeitsdichteverteilung für den Gesamtregelleistungsbedarf -> Werte für Cluster werden nur für jede Auktion geändert.
        /// Deshalb hier die Kurzfassung für nicht geänderte Werte
        /// </summary>
        /// <param name="determinationDate"></param>
        public override void Determine_ProbabilityDensityDistribution_Of_Demands_For_Same_Auction(DateTime determinationDate, TimeSpan duration)
        {
            foreach (int typeNumber in ControlReserveShortCode.GetSyntethicDemandTypes())
            {
                Dictionary<int, float> HistWeighted = new Dictionary<int, float>();
                if (Helper_Time_Translation.GetStartTime(determinationDate, duration).CompareTo(Helper_Time_Translation.GetStartTime(LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber], duration)) != 0)
                {
                    DateTime EndDate = new DateTime(determinationDate.Year, determinationDate.Month, determinationDate.Day, 0, 0, 0);
                    DateTime StartDate = EndDate.AddYears(-3).AddDays(-30);

                    //Gewichtung nach Referenzzeitpunkten durchführen.
                    Weighting_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = WeightingForRef(StartDate, determinationDate, EndDate, duration, Balances_Cluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber]);
                }
                LastDetermination_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber] = determinationDate;
            }
        }

        /// <summary>
        /// Bildung der Cluster
        /// Zuerst werden die Startwerte als Mittelwerte der CLuster definiert.
        /// Dann werden die Regelleistungswerte dem Cluster zugeordnet, 
        /// dessen Mittelwert die geringste quadratische Abweichung zum Wert besitzt.
        /// Anschließend wird für jeden Cluster der Mittelwert bestimmt und als neuer Mittelwert des Clusters bestimmt.
        /// Entsprechen diese Werte abgesehen von einer zulässigen Abweichung nicht den vorherigen Mittelwerten, werden die
        /// Regelleistungswerte erneut den CLustern zugeordnet.
        /// Ende des Algorythmus ist, wenn die Veränderung der Mittelwerte nicht über die erlaubte ABweichung hinausgeht.
        /// </summary>
        /// <param name="historicMean"></param>
        /// <param name="balances"></param>
        /// <returns></returns>
        private Dictionary<int, Dictionary<DateTime, float>> CalculateCluster(float[] historicMean, Dictionary<DateTime, float> balances)
        {
            Dictionary<int, Dictionary<DateTime, float>> BalancesCluster = new Dictionary<int, Dictionary<DateTime, float>>();
            Dictionary<int, HashSet<float>> BalancesCluster2 = new Dictionary<int, HashSet<float>>();
            int i;
            int Clusternumber = 4;
            float[] MeanList = new float[4];

            //Startwerte ermitteln
            MeanList = MeanValues(historicMean, balances.Select(item => (float)item.Value));

            // k-Means CLustern
            for (i = 0; i <= Clusternumber - 1; i++)
                BalancesCluster.Add(i, new Dictionary<DateTime, float>());
            bool isStart = true;
            while (isStart || BalancesCluster2.All(item => Math.Pow(item.Value.Average() - MeanList[item.Key], 2) <= 0.25) == false)
            {
                if (!isStart) //Schalter -> Startwerte bereits bestimmt
                              //Bestimmen der Mittelwerte
                    MeanList = BalancesCluster2.Select(item3 => item3.Value.Average()).ToArray();
                //Leeren der Cluster
                foreach (var item2 in BalancesCluster)
                    item2.Value.Clear();
                //Bestimmen der gerinsten Abweichung des Mittelwertes 
                //des Clusters zum einzusetzenden Regelleistungwert
                //und Einfügen des Wertes in diesen Cluster.
                foreach (var item in balances)
                {
                    var ClusterDivergence = BalancesCluster2.Select(item2 => (item2.Key, Math.Pow(item.Value - MeanList[item2.Key], 2)));
                    ClusterDivergence.ToList().ForEach(item2 => { if (item2.Item2 == ClusterDivergence.Select(item3 => item3.Item2).Min()) BalancesCluster2[item2.Key].Add(item.Value); });
                }
                isStart = false;
            }
            //Bestimmen der gerinsten Abweichung des Mittelwertes 
            //des Clusters zum einzusetzenden Regelleistungwert
            //und Einfügen des Wertes in diesen Cluster.
            foreach (var item in balances)
            {
                var ClusterDivergence = BalancesCluster.Select(item2 => (item2.Key, Math.Pow(item.Value - MeanList[item2.Key], 2)));
                ClusterDivergence.ToList().ForEach(item2 => { if (item2.Item2 == ClusterDivergence.Select(item3 => item3.Item2).Min()) BalancesCluster[item2.Key].Add(item.Key, item.Value); });
            }
            return BalancesCluster;
        }

        private float[] GetMeanOfCluster(Dictionary<int, Dictionary<DateTime, float>> balancesCluster)
            => balancesCluster.Select(item3 => item3.Value.Select(item4 => item4.Value).Average()).ToArray();

        private float[] GetStandardDeviationOfCluster(Dictionary<int, Dictionary<DateTime, float>> balancesCluster)
            => balancesCluster.Select(item3 => (float)MathNet.Numerics.Statistics.Statistics.StandardDeviation(item3.Value.Select(item4 => item4.Value))).ToArray();

        /// <summary>
        /// Bestimmung der Gewichtung für die Cluster.
        /// Dabei fließen alle historischen Werte ein, die in den letzten 60 Tagen vor dem Bestimmungstag liegen
        /// und der Zeiträume jeweils 30 Tage vor und nach dem Bestimmungstag in den zwei Jahren vor dem Bestimmungtag.
        /// Zusätzlich wird nach dem Wochentag gefiltert (Mo, Di, Mi, Do, Fr, Sa, So + Feiertag).
        /// Zum Schluss wird nochmals nach dem Tageszeitraum gefiltert, sodass diese dem Bestimmungszeitraum am Bestimmungstag entsprechen.
        /// Für 0-4 Uhr werden somit nur Werte von 0:00 Uhr bis 3:45 Uhr gewählt.
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="BalancesCluster"></param>
        /// <returns></returns>
        private float[] WeightingForRef(DateTime StartDate, DateTime DeterminationDate, DateTime EndDate, TimeSpan duration, Dictionary<int, Dictionary<DateTime, float>> BalancesCluster)
        {
            DateTime LastStart = EndDate.AddDays(-60);
            var Durations = new List<(DateTime StartDate, DateTime EndDate)>();
            for(DateTime time = StartDate.Add(-duration); time < EndDate; time = time.AddYears(1))
            {
                if (time > LastStart) Durations.Add((LastStart, EndDate));
                else
                {
                    DateTime DurationStart = time;
                    DateTime DurationEnd = time.AddDays(60).Add(-duration);
                    Durations.Add((DurationStart, DurationEnd));
                }
            }
            Dictionary<int, DayValues> H_DV_List = new Dictionary<int, DayValues>();
            List<int> Years = Helper_Time_Translation.GetYearsFromDuration(StartDate, EndDate);
            Years.ForEach(year => H_DV_List.Add(year, new DayValues(year)));

            int NumberTimeSpan = Helper_Time_Translation.GetNumberTimeSpanDay(DeterminationDate, duration);
            int DayInt = H_DV_List[EndDate.Year].getDay("DE", DeterminationDate);
            //Summieren des Vorkommens der Referenzzeitpunkte in den Clustern
            int[] Numbers = BalancesCluster
                .Select(cluster => cluster.Value
                .Where(item => Helper_Time_Translation.GetNumberTimeSpanDay(item.Key, duration) == NumberTimeSpan &&
                H_DV_List[item.Key.Year].getDay("DE", item.Key) == DayInt && Durations.Any(dur => item.Key >= dur.StartDate && item.Key < dur.EndDate)).Count()).ToArray();

            //Gewichtung der Cluster nach Häufigkeit des Vorkommens der Refenzzeitpunkte
            int Sum = Numbers.Sum();
            return Numbers.Select(item => (float)item / Sum).ToArray();
        }


        /// <summary>
        /// Bestimmung der Anfangswerte für das K-Mean-Clustern
        /// Es werden die 25 % und 75 % Percentile sowie die oberen und unteren Whisker der Verteilung genommen.
        /// </summary>
        /// <param name="historic"></param>
        /// <param name="balances"></param>
        /// <returns></returns>
        private float[] MeanValues(float[] historic, IEnumerable<float> balances)
        {
            float[] MeanList = new float[4];
            if (historic.All(item => item == 0))
            {
                //Bestimmung der Anfangswerte nach Percentilen und Whiskern der Verteilung
                float q25 = MathNet.Numerics.Statistics.Statistics.Quantile(balances, 0.25);
                float q75 = MathNet.Numerics.Statistics.Statistics.Quantile(balances, 0.75);
                float icr = q75 - q25;
                float upperWhisker = Math.Min(q75 + 1.5F * icr, balances.Max());
                float lowerWhisker = Math.Max(q25 - 1.5F * icr, balances.Min());
                MeanList = new float[4] { lowerWhisker, q25, q75, upperWhisker };
            }
            else
            {
                MeanList = historic;
            }
            return MeanList;
        }

        /// <summary>
        /// Gewichtetes Mittel der Wahrscheinlichkeitsdichteverteilungen der Cluster bilden.
        /// Gewichtung ist mit Weighting angegeben.
        /// </summary>
        /// <param name="weighting"></param>
        /// <param name="histCluster"></param>
        /// <returns></returns>
        private float PartFromWeighting(int position, int typeNumber)
        {
            var result = Enumerable.Range(0, MeanValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber].Count()).Select(item2 => Weighting_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber][item2] * CDF_NormalDistribution(position, MeanValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber][item2], StandardDeviationValuesCluster_ControlReserve[(ControlReserveShortCode.ShortCode)typeNumber][item2])).Sum();
            return result;
        }

        private float CDF_NormalDistribution(int x, float mean, float standardDeviation)
            => 0.5F * (float)(1 + MathNet.Numerics.SpecialFunctions.Erf((x - mean) / Math.Sqrt(2 * Math.Pow(standardDeviation, 2))));


        private int FindValueForErrorlevel(int typeNumber, float errorLevel)
        {
            (int Key, float Value) a, b, c;
            int range = 3000;

            a = GetTupel(-range, typeNumber);
            c = GetTupel(range, typeNumber);

            if (a.Value > errorLevel) return a.Key;
            if (c.Value < errorLevel) return c.Key;

            for (int i = 0; 2 + (1 - Math.Pow(2, i + 1)) / (1 - 2) <= 2 * range; i++) //Ausfallbedingung durch geometrische Summenformel.
            {
                b = GetTupel((a.Key + c.Key) / 2, typeNumber);
                if (b.Value > errorLevel)
                {
                    if (GetTupel(b.Key - 1, typeNumber).Value > errorLevel)
                        //Menge ist zu hoch, setzte c = b
                        c = b;
                    else
                        //Preis liegt zwischen b und b-1
                        return b.Key - 1;
                }
                else
                {
                    if (GetTupel(b.Key + 1, typeNumber).Value < errorLevel)
                        //Menge ist zu niedrig, setzte a = b
                        a = b;
                    else
                        //Preis liegt zwischen b und b +1
                        return b.Key;
                }
            }
            return a.Key;
        }

        public (int Key, float Value) GetTupel(int position, int typeNumber)
            => (position, PartFromWeighting(position, typeNumber));
        #endregion
    }
}
