﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using winMoz.Helper;
using winMoz.Helper;
using winMoz.Data.Access.DirectAcess;
using winMoz.Simulation;

namespace winMoz.Markets.ControlReserve
{
    /// <summary>
    /// Klasse zur Berechnung der Abweichungen der Sekundärregelleistungsabrufen vom Mittel der Viertelstunde
    /// nach Regression
    /// Beachtet wird die Last und die Viertelstunde der Stunde.
    /// </summary>
    public class DeviationFromRZSaldo_Handler : SimObject
    {
        #region Attributes

        /// <summary>
        /// Differenziert zwischen den verschiedenen Parametern 
        /// für die Berechnung der Sekundärregelleistungsabweichung innerhalb einer Viertelstunde
        /// </summary>
        public enum ParamaFRRDeviation
        {
            MIN, MAX, QUARTER_MIN, QUARTER_MAX, DEMAND_MIN, DEMAND_MAX
        }

        /// <summary>
        /// Sammlung der Parameter zur Berechnung der Abweichung der Regelleistung nach Regression
        /// </summary>
        private Dictionary<ParamaFRRDeviation, float[][]> DeviationParameters { get; set; }

        private MathNet.Numerics.Random.MersenneTwister Random { get; set; }
        #endregion

        private DeviationFromRZSaldo_Handler() { }
        /// <summary>
        /// Konstruktur für die Klasse zur Berechnung der Abweichung der Sekundärregelleistung nach Regression
        /// </summary>
        public DeviationFromRZSaldo_Handler(int scenarioID = 0) : base(scenarioID)
        {
            Initialize();
        }

        public void Initialize(bool isComplete = true)
        {
            Log.Info($" Start initialization of {this.GetType()}");
            Random = new MathNet.Numerics.Random.MersenneTwister(seed: 10000, threadSafe: true);
            DeviationParameters = RZ_Saldo_Deviation_DirectAcess.GetParameters();
            if (isComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public virtual void SetSettings()
        {
            Log.Info($"Set Settings of {this.GetType()}");
        }

        private float[][] SumArray { get; set; }

        /// <summary>
        /// Berechnung der Abweichung zu höheren Werten (Max) und geringeren Werten (Min) der Sekundärregelleistung, die sich innerhalb einer Viertelstunde ergeben
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="EDemand"></param>
        /// <returns></returns>
        public virtual (float Min, float Max) GetDeviationFromRZSaldo(DateTime timestamp, float EDemand)
        {
            //Setzen der lokalen Variablen
            float QuarterHourOfDay = Extensions.GetQuarterHourOfDay(timestamp);
            DateTime DeterminationTime = new DateTime(timestamp.Year, timestamp.Month, timestamp.Day).AddMinutes(15 * QuarterHourOfDay - 15);
            float QuarterHourOfHour = Extensions.GetQuarterHourOfHour(DeterminationTime);
            float HourOfDay = DeterminationTime.Hour;

            //Berechnen der Parameter für die Verteilung der Min und Max Werte der Abweichung (siehe Erläuterung oben)
            List<float> ArrayMin = new List<float>();
            for (int i = 0; i < 3; i++)
                 ArrayMin.Add((float) (DeviationParameters[ParamaFRRDeviation.MIN][i].First() + MathNet.Numerics.Polynomial.Evaluate(EDemand / 1000, DeviationParameters[ParamaFRRDeviation.DEMAND_MIN][i].ToList().ConvertAll(el => (double)el).ToArray()) + MathNet.Numerics.Polynomial.Evaluate(QuarterHourOfHour, DeviationParameters[ParamaFRRDeviation.QUARTER_MIN][i].ToList().ConvertAll(el => (double)el).ToArray())));
            List<float> ArrayMax = new List<float>();
            for (int i = 0; i < 3; i++)
                ArrayMax.Add((float) (DeviationParameters[ParamaFRRDeviation.MAX][i].First() + MathNet.Numerics.Polynomial.Evaluate(EDemand / 1000, DeviationParameters[ParamaFRRDeviation.DEMAND_MAX][i].ToList().ConvertAll(el => (double)el).ToArray()) + MathNet.Numerics.Polynomial.Evaluate(QuarterHourOfHour, DeviationParameters[ParamaFRRDeviation.QUARTER_MAX][i].ToList().ConvertAll(el => (double)el).ToArray())));
            SumArray = new float[2][] { ArrayMin.ToArray(), ArrayMax.ToArray() };

            float rndNew = (float)Random.NextDouble();

            //Bestimmung der Min und Max Werte als Zufallszahl der oben berechneten Verteilung
            //Dafür wird die kummulierte Verteilung bestimmt.
            float[] MinMax= new float[2] { 0, 0 };
            for (int i = 0; i < 2; i++)
                MinMax[i] = FindValueForRnd(i, rndNew);
            return (MinMax[0], MinMax[1]);
        }

        public (int Key, float Value) GetTupel(int position, int typeNumber)
            => (position, GetValue(position, typeNumber));

        public float GetValue(int position, int typeNumber)
            => DensityDistributions.DistributionFit(position, SumArray[typeNumber][0], SumArray[typeNumber][1], SumArray[typeNumber][2]);

        public float GetValue(int position, int typeNumber, float[][] sumArray)
            => DensityDistributions.DistributionFit(position, sumArray[typeNumber][0], sumArray[typeNumber][1], sumArray[typeNumber][2]);

        public int FindValueForRnd(int typeNumber, float rndNew)
        {
            (int Key, float Value) a, b, c;
            int range = 2000;

            a = GetTupel(-range, typeNumber);
            c = GetTupel(range, typeNumber);

            if (a.Value > rndNew) return a.Key;
            if (c.Value < rndNew) return c.Key;

            for (int i = 0; 2 + (1 - Math.Pow(2, i + 1)) / (1 - 2) <= 2 * range; i++) //Ausfallbedingung durch geometrische Summenformel.
            {
                b = GetTupel((a.Key + c.Key) / 2, typeNumber);
                if (b.Value > rndNew)
                {
                    if (GetTupel(b.Key - 1, typeNumber).Value > rndNew)
                        //Menge ist zu hoch, setzte c = b
                        c = b;
                    else
                        //Preis liegt zwischen b und b-1
                        return b.Key - 1;
                }
                else
                {
                    if (GetTupel(b.Key + 1, typeNumber).Value < rndNew)
                        //Menge ist zu niedrig, setzte a = b
                        a = b;
                    else
                        //Preis liegt zwischen b und b +1
                        return b.Key;
                }
            }
            return a.Key;
        }
    }
}
