﻿using System.Collections.Generic;
using System;
using System.Linq;
using winMoz.Markets;
using winMoz.Helper;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.Bids;
using winMoz.Helper;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;
using winMoz.Simulation;
using winMoz.Markets.Elements;
using winMoz.Tools.Prediction.GridLoad;

namespace winMoz.Markets.ControlReserve
{
    public class ControlReserveActivationVolumeHandler : SimObject
    {
        #region Basic
        public List<ControlReserve_Activation> Activations { get; private set; }
        public List<int> ActivationIDs { get; private set; }
        public DateTime HistoricStart { get; protected set; }

        private PredictorGridLoad predictorLoad = new PredictorGridLoad();


        public void CoupleToActivation(ControlReserve_Activation activation)
        {
            activation.Activation += PutDemandBidToActivation;
        }

        public void PutDemandBidToActivation(ControlReserve_Activation calling, ActivationArgs args)
        {
            foreach (int type_number in ControlReserveShortCode.GetAuctionTypes())
            {
                var ActivationVolumes = GetActivationVolume(new ActivationVolumeArgs(args.TimeStamp, args.Duration, args.Step, args.AuctionStep, (ControlReserveShortCode.ShortCode)type_number));
                Bid_ControlReserve_Activation bid_Cap = new Bid_ControlReserve_Activation();
                var DurationEnd = args.TimeStamp.Add(args.Duration);
                for (DateTime time = args.TimeStamp; time < DurationEnd; time = time.Add(args.Step))
                    bid_Cap.AddSegment(ShortCode.ControlReserve.ActivationFromEnum((ControlReserveShortCode.ShortCode)type_number, time, args.Step), time, -ActivationVolumes[time], 99999);
                calling.SubmitActivation(this, bid_Cap);
            }
        }

        public Dictionary<DateTime, int> GetActivationVolume(ActivationVolumeArgs activationVolumeArgs)
        {
            if (!ControlReserveDemandRepository.IsActivationVolumeTimeFrameCached(activationVolumeArgs))
                DetermineActivationVolume(activationVolumeArgs);
            return ControlReserveDemandRepository.GetActivationVolumes(activationVolumeArgs).ToDictionary(item => item.TimeStamp, item => item.Volume);
        }
        #endregion

        public DeviationFromRZSaldo_Handler DeviationFromRZSaldo { get; private set; }

        public IGCC_Calculation IGCC { get; private set; }

        private ControlReserveActivationVolumeHandler() { Log.Info($" Start initialization of {this.GetType()}"); }
        public ControlReserveActivationVolumeHandler(List<ControlReserve_Activation> activations, IGCC_Calculation igcc, DeviationFromRZSaldo_Handler deviationFromRZSaldo, int scenarioID = 0) : base(scenarioID)
        {
            Log.Info($" Start initialization of {this.GetType()}");
            HistoricStart = Simulator.Times.Start.AddYears(-10);
            Activations = activations ?? throw new ArgumentNullException(activations.GetType().ToString());
            ActivationIDs = Activations.Select(item => item.Id).ToList();
            DeviationFromRZSaldo = deviationFromRZSaldo;
            IGCC = igcc;
            Initialize();
        }

        public virtual void Initialize(bool isComplete = true)
        {
            DateTime HistoricEnd = Simulator.Times.Start;
            TimeSpan HistoricDuration = HistoricEnd - HistoricStart;
            using (var repo = new ScenarioData_Repository())
            {
                repo.MarketRepository.SetScenarioID(this.ScenarioID);
            }
            foreach (ControlReserve_Activation activation in Activations) CoupleToActivation(activation);
            ControlReserveDemandRepository.CacheHistoric(HistoricStart, HistoricDuration);
            if (isComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public virtual void SetSettings()
        {
            Log.Info($"Set Settings of {this.GetType()}");
        }

        private List<(DateTime TimeStamp, float Load)> PredictLoad(ActivationVolumeArgs activationVolumeArgs)
        {
            var forecast = new List<ForecastGridLoad>();
            for (int qh = 0; qh < activationVolumeArgs.Duration.TotalHours * 4; qh++)
                forecast.Add(new ForecastGridLoad(activationVolumeArgs.DeterminationDate.AddMinutes(15 * qh)));
            List<(DateTime TimeStamp, float Load)> Load = predictorLoad.Predict(forecast).Select(item => (item.TimeStamp, item.GridLoadPrediction / 1000.0F)).ToList();
            return Load;
        }



        public void SetActivations(List<ControlReserve_Activation> activations)
        {
            if (Activations == null || Activations.Count() == 0)
            {
                Activations = activations.Where(activation => ActivationIDs.Contains(activation.Id)).ToList();
                foreach (ControlReserve_Activation activation in Activations) CoupleToActivation(activation);
            }
        }

        /// <summary>
        /// Berechne den Bedarf für die Arbeitspreisauktion (Aktivierung)
        /// Zuerst
        /// </summary>
        /// <param name="activationVolumeArgs"></param>
        /// <returns></returns>
        protected virtual void DetermineActivationVolume(ActivationVolumeArgs activationVolumeArgs)
        {
            Dictionary<DateTime, Dictionary<ControlReserveShortCode.ShortCode, int>> demands = new Dictionary<DateTime, Dictionary<ControlReserveShortCode.ShortCode, int>>();
            Dictionary<DateTime, Dictionary<ControlReserveShortCode.ShortCode, bool>> limits = new Dictionary<DateTime, Dictionary<ControlReserveShortCode.ShortCode, bool>>();
            var balances = ControlReserveDemandRepository.GetBalances(activationVolumeArgs.DeterminationDate, activationVolumeArgs.Duration);
            Dictionary<DateTime, float> Balances = ControlReserveDemandRepository.GetBalances(activationVolumeArgs.DeterminationDate, activationVolumeArgs.Duration).ToDictionary(item => item.TimeStamp, item => item.Balance);
            DateTime LimitStepStartTime = activationVolumeArgs.DeterminationDate;
            List<(DateTime TimeStamp, float Load)> Load = PredictLoad(activationVolumeArgs);
            if (Balances.Count() > 0)
            {
                for (DateTime time = activationVolumeArgs.DeterminationDate; time < activationVolumeArgs.DeterminationDate.Add(activationVolumeArgs.Duration); time = time.Add(activationVolumeArgs.StepDuration))
                {
                    if ((time - activationVolumeArgs.DeterminationDate).TotalMinutes % activationVolumeArgs.LimitStepDuration.TotalMinutes == 0)
                        LimitStepStartTime = time;
                    int Limit_p_mFRR = ControlReserveDemandRepository.GetTenderVolume(new DemandArgs(LimitStepStartTime, activationVolumeArgs.LimitStepDuration, activationVolumeArgs.LimitStepDuration, ControlReserveShortCode.ShortCode.p_mFRR)).First().Demand;
                    int Limit_n_mFRR = ControlReserveDemandRepository.GetTenderVolume(new DemandArgs(LimitStepStartTime, activationVolumeArgs.LimitStepDuration, activationVolumeArgs.LimitStepDuration, ControlReserveShortCode.ShortCode.n_mFRR)).First().Demand;
                    Dictionary<ControlReserveShortCode.ShortCode, int> demand = new Dictionary<ControlReserveShortCode.ShortCode, int>();
                    Dictionary<ControlReserveShortCode.ShortCode, bool> limit = new Dictionary<ControlReserveShortCode.ShortCode, bool>();
                    //Abschätzung der Minutenreserve aus der Vergangenheit (zwei Viertelstunden zuvor)
                    int preactivate_mFRR = PreActivate_mFRR(Balances[time], Limit_p_mFRR, Limit_n_mFRR, (float)ControlReserveDemandRepository.mFRR_Attributes.mFRR_Threshold, (float)ControlReserveDemandRepository.mFRR_Attributes.mFRR_Factor);
                    //Aktivierung der Sekundärregelleistung auf Basis dieser Vorhersage
                    //Berechne Abweichungen während der Viertelstunde -> Ausgabe: Positive Sekundärregleleistung, negative Sekundärregelleistung und neues Saldo

                    
                    int Limit_p_aFRR = ControlReserveDemandRepository.GetTenderVolume(new DemandArgs(LimitStepStartTime, activationVolumeArgs.LimitStepDuration, activationVolumeArgs.LimitStepDuration, ControlReserveShortCode.ShortCode.p_aFRR)).First().Demand;
                    int Limit_n_aFRR = ControlReserveDemandRepository.GetTenderVolume(new DemandArgs(LimitStepStartTime, activationVolumeArgs.LimitStepDuration, activationVolumeArgs.LimitStepDuration, ControlReserveShortCode.ShortCode.n_aFRR)).First().Demand;
                    var DeviationMinMax = DeviationFromRZSaldo.GetDeviationFromRZSaldo(time, Load.Single(item => item.TimeStamp == time).Load);
                    (float aFRR_Plus, bool limit_aFRR_Plus, float aFRR_Minus, bool limit_aFRR_Minus, float aFRR) = Activate_aFRR(Balances[time], preactivate_mFRR, Limit_p_aFRR, Limit_n_aFRR, DeviationMinMax);
                    //Austausch mit IGCC
                    var igcc_volume = IGCC.GetIGCC_Volume(time);
                    float aFRR_Plus_after = 0;
                    float aFRR_Minus_after = 0;
                    if (igcc_volume > 0)
                    {
                        aFRR_Plus_after = Math.Max(aFRR_Plus - igcc_volume, 0);
                        aFRR_Minus_after = Math.Min(aFRR_Minus, 0);
                        if (aFRR_Plus - igcc_volume != aFRR_Plus_after)
                            IGCC.CorrectVolume(time, aFRR_Plus);
                    }
                    else
                    {
                        aFRR_Minus_after = Math.Max(aFRR_Minus + igcc_volume, 0);
                        aFRR_Minus_after = Math.Min(aFRR_Minus + igcc_volume, 0);
                        aFRR_Plus_after = Math.Max(aFRR_Plus, 0);
                        if (aFRR_Minus + igcc_volume != aFRR_Minus_after)
                            IGCC.CorrectVolume(time, -aFRR_Minus);
                    }
                    demand.Add(ControlReserveShortCode.ShortCode.p_aFRR, (int)aFRR_Plus_after);
                    demand.Add(ControlReserveShortCode.ShortCode.n_aFRR, (int)aFRR_Minus_after);
                    limit.Add(ControlReserveShortCode.ShortCode.p_aFRR, limit_aFRR_Plus);
                    limit.Add(ControlReserveShortCode.ShortCode.n_aFRR, limit_aFRR_Minus);
                    //Aktivierung der Minutenreserve als Rest nach Abzug des Sekundärreglleistungssaldos
                    (float mFRR, bool limit_mFRR) = Activate_mFRR(Balances[time], aFRR, Limit_p_mFRR, Limit_n_mFRR);
                    demand.Add(ControlReserveShortCode.ShortCode.p_mFRR, mFRR > 0 ? (int)mFRR : 0);
                    demand.Add(ControlReserveShortCode.ShortCode.n_mFRR, mFRR < 0 ? (int)mFRR : 0);
                    limit.Add(ControlReserveShortCode.ShortCode.p_mFRR, mFRR > 0 ? limit_mFRR : false);
                    limit.Add(ControlReserveShortCode.ShortCode.n_mFRR, mFRR < 0 ? limit_mFRR : false);
                    ControlReserveDemandRepository.AddActivationVolumes(time, demand);
                    ControlReserveDemandRepository.AddActivationVolumeIsLimitList(time, limit);
                }
            }
        }
        /// <summary>
        /// Geplante Aktivierung von Minutenreserveleistung basierend auf einer vorhergehenden Vietelstunde (T-2)
        /// </summary>
        /// <param name="controlAreaBalanceTminus2">Viertelstunde T-2</param>
        /// <param name="timeStamp">Auktionszeitpunkt</param>
        /// <returns>geplanter Minutenreserveeinsatz in MW </returns>
        private static int PreActivate_mFRR(float controlAreaBalanceTminus2, int Limit_p_mFRR, int Limit_n_mFRR, float mFRR_Threshold, float mFRR_Factor)
        {
            //Bei größerem Bedarf als ausgeschrieben -> Begrenzung auf ausgeschriebene Menge
            if (controlAreaBalanceTminus2 > mFRR_Threshold) return Math.Min((int)((controlAreaBalanceTminus2 - mFRR_Threshold) * mFRR_Factor), Limit_p_mFRR); //Positive Regelleistung
            else if (controlAreaBalanceTminus2 < -mFRR_Threshold) return Math.Max((int)((controlAreaBalanceTminus2 + mFRR_Threshold) * mFRR_Factor), Limit_n_mFRR); //Negative Regelleistung
            else return 0; //Unterhalb Grenzen
        }

        /// <summary>
        /// Sekundarregelleistungseinsatz unter Annahme des geplanten Minutenreserveeinsatzes
        /// Bei überschreitung der ausgeschribenen Menge -> Begrenzung auf diese und Setzen des Schalters "Overlimited"
        /// </summary>
        /// <param name="controlAreaBalanceT0">Regelzonenabweichung zum Betrachtungszeitpunkt</param>
        /// <param name="mFRR_preActivation">geplanter Minutenreserveeinsatz in MW</param>
        /// <param name="timeStamp">Auktionszeitpunkt</param>
        /// <returns>Sekundarregeleistungseinsatz in MW</returns>
        private static (float Value_Positive, bool Overlimited_Positive, float Value_Negative, bool Overlimited_Negative, float Value_Saldo) Activate_aFRR(float controlAreaBalanceT0, int mFRR_preActivation, int limit_p_aFRR, int limit_n_aFRR, (float Minimum, float Maximum) aFRR_deviation)
        {
            float aFRR;

            aFRR = controlAreaBalanceT0 - mFRR_preActivation;

            //Abweichungen berechnen
            //(float Minimum, float Maximum) aFRR_deviation = aFRR_Deviation_1.MinMaxDeviation(timeStamp, Load);
            float aFRR_Plus = aFRR > 0 ? aFRR + aFRR_deviation.Maximum : aFRR_deviation.Maximum;
            float aFRR_Minus = aFRR > 0 ? aFRR_deviation.Minimum : aFRR + aFRR_deviation.Minimum;

            bool limited_Positive = false;
            bool limited_Negative = false;

            //Auf Limitierung prüfen
            if (aFRR_Plus > limit_p_aFRR)  //Positive Sekundärregelleistung
            {
                aFRR_Plus = limit_p_aFRR;
                limited_Positive = true;
            }

            if (aFRR_Minus < limit_n_aFRR) //Negative Sekundärregelleistung
            {
                aFRR_Minus = limit_n_aFRR;
                limited_Negative = true;
            }

            return (aFRR_Plus, limited_Positive, aFRR_Minus, limited_Negative, aFRR_Plus + aFRR_Minus);

        }

        /// <summary>
        /// Entgültiger Minutenreserveeinsatz
        /// Bei überschreitung der ausgeschribenen Menge -> Begrenzung auf diese und Setzen des Schalters "Overlimited"
        /// </summary>
        /// <param name="controlAreaBalanceT0">Regelzonenabweichung zum Betrachtungszeitpunkt</param>
        /// <param name="aFRR_Activation">Sekundarregeleistungseinsatz in MW</param>
        /// <returns></returns>
        private static (float Value, bool Overlimited) Activate_mFRR(float controlAreaBalanceT0, float aFRR_Activation, int limit_p_mFRR, int limit_n_mFRR)
        {
            float mFRR;
            int mFRR_Positive = limit_p_mFRR;
            int mFRR_Negative = limit_n_mFRR;
            mFRR = controlAreaBalanceT0 - aFRR_Activation;

            if (mFRR >= 0 && mFRR > mFRR_Positive) return (mFRR_Positive, true);
            else if (mFRR < 0 && mFRR < mFRR_Negative) return (mFRR_Negative, true);
            else return (mFRR, false);
        }
    }
}
