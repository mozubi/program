﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets.Bids;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;
using winMoz.Assets;
using winMoz.Simulation;
using winMoz.Markets.EoMMarkets;

namespace winMoz.Markets.ControlReserve
{
    public class ControlReserve_Energy : ControlReserveMarket
    {
        public IntradayMarket Intraday { get; set; }

        public int IntradayId { get; private set; }

        public ControlReserve_Activation Activation { get; set; }

        private ControlReserve_Energy() { }
        public ControlReserve_Energy(ControlReserve_Energy_Attributes attributes, IntradayMarket intraday, ControlReserve_Activation activation, int scenarioID = 0) :
            base(attributes, scenarioID)
        {
            Intraday = intraday;
            if (Intraday != null) Intraday.Auction += IntradayMarket_Auction;
            IntradayId = intraday.Id;
            Activation = activation ?? throw new ArgumentNullException(activation.GetType().ToString());
            if (Activation != null) Activation.CoupleToMarket(this);
            if (Activation != null) Activation.Activation += ActivationProcess;
        }

        public override void Initialize(bool IsComplete = true)
        {
            base.Initialize(IsComplete: false);
            if (Intraday != null) Intraday.Auction += IntradayMarket_Auction;
            if (Activation != null) Activation.CoupleToMarket(this);
            if (Activation != null) Activation.Activation += ActivationProcess;
            if (IsComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public void SetIntraday(IntradayMarket intraday)
        {
            if (Intraday == null && intraday.Id == IntradayId)
            {
                Intraday = intraday;
                if (Intraday != null) Intraday.Auction += IntradayMarket_Auction;
            }
        }

        //protected override void GenerateDebugBids(AuctionArgs args)
        //    => this.Bids.AddRange(DebugMarketObjectsGenerator.DebugGenerateBidsControlReserveEnergyMarket(args));

        protected override IEnumerable<Bid_Segment> GetSegmentsForAuction(ControlReserveShortCode.ShortCode controlReserveType)
        {
            DateTime DurationEnd = EventTimeframe.TimeStamp.Add(EventTimeframe.Duration);
            var test = BidsAuction.Where(item => item.Assets == null || item.Assets.Count() == 0).ToList();
            var Segments = BidsAuction.SelectMany(bid => ((Bid_ControlReserve_Energy)bid).GetValidSegments(EventTimeframe.TimeStamp, EventTimeframe.Duration, controlReserveType))
                                 .ToList();
            if (((ControlReserve_Energy_Attributes)this.Attributes).OnlyBidsFromCapacityAllowed)
                Segments = Segments.Where(seg => seg.FirstEvent is ControlReserve_Capacity).ToList();
            return Segments;
        }

        private void IntradayMarket_Auction(Market callingMarket, AuctionArgs args)
        {
            this.Bids
                .Where(bid => ((Bid_ControlReserve_Energy)bid).IsControlReserve_Energy()
                    && ((Bid_ControlReserve_Energy)bid).HasSegmentsToTransferToSimEvent(args.TimeStamp, args.Duration, callingMarket))
                .ToList()
                .ForEach(bid =>
                {
                    var segmentsToTransfer = ((Bid_ControlReserve_Energy)bid).GetSegmentsToTransferToSimEvent(args.TimeStamp, args.Duration, callingMarket);
                    if (segmentsToTransfer.Count() != 0)
                    {
                        foreach (var seg in segmentsToTransfer)
                        {
                            for (DateTime time = args.TimeStamp; time <= args.TimeStamp.Add(args.Duration); time = time.AddHours(1))
                            {
                                List<(DateTime TimeStamp, float Volume, float Price)> schedule = GetSchedule(time, seg.VolumeMW, seg.Price);
                                var BidTransfer = new Bid_Intraday(bid.Assets, callingMarket).QuarterHour(schedule);
                                BidTransfer.Clearing += (clearedBid) => clearedBid.UpdateSchedule();
                                Intraday.SubmitBid(BidTransfer);
                            }
                        }
                    }
                });
        }

        protected override List<ControlReserveResult> CalcResults(ControlReserveShortCode.ShortCode controlReserveType)
        {
            DateTime DurationEnd = EventTimeframe.TimeStamp.Add(EventTimeframe.Duration);
            List<ControlReserveResult> Results = new List<ControlReserveResult>();
            var IsNegative = controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_aFRR || controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_mFRR;
            for (DateTime time = EventTimeframe.TimeStamp; time < DurationEnd; time = time.Add(EventTimeframe.Step))
            {
                var segmentsTimeEnergy = SegmentsAuction.Where(seg => seg.TimeStamp == time && ((Bid_Segment_ControlReserve)seg).FirstEvent is ControlReserve_Energy).ToList();
                var segmentsTimeCapacity = SegmentsAuction.Where(seg => seg.TimeStamp == time && ((Bid_Segment_ControlReserve)seg).FirstEvent is ControlReserve_Capacity).ToList();
                var result = ControlReserve_Extensions.CalculateMarketResultsPayAsBidOneBuyer(segmentsTimeEnergy, isEnergy: false, Lower_Bound_Price: this.Attributes.Lower_Price_Bound, Upper_Price_Bound: this.Attributes.Upper_Price_Bound, isNegative: IsNegative, lastOfSameAuction: false);
                if (result != null)
                {
                    PlotAndOutputMeritOrder(segmentsTimeEnergy, time, controlReserveType);
                    Results.Add(result);
                    //Nicht berücksichtige Gebote des Regelleistungsmarktes werden in den Intraday-Markt verschoben.
                    segmentsTimeCapacity.ForEach(seg => ((Bid_Segment_ControlReserve)seg).TransferEvent = Intraday);
                }
                else
                {
                    PlotAndOutputMeritOrder(segmentsTimeCapacity, time, controlReserveType);
                    result = ControlReserve_Extensions.CalculateMarketResultsPayAsBidOneBuyer(segmentsTimeCapacity, isEnergy: false, Lower_Bound_Price: this.Attributes.Lower_Price_Bound, Upper_Price_Bound: this.Attributes.Upper_Price_Bound, isNegative: IsNegative);
                }
                if (result != null) Results.Add(result);
            }
            return Results;
        }


        #region Activation
        private void ActivationProcess(ControlReserve_Activation calling_Activation, ActivationArgs args)
        {
            this.Bids
                .Where(bid => ((Bid_ControlReserve_Energy)bid).IsControlReserve_Energy() &&
                ((Bid_ControlReserve_Energy)bid).HasSegmentsToTransferToSimEvent(args.TimeStamp, args.Duration, calling_Activation))
                .ToList()
                .ForEach(bid =>
                {
                    var segmentsToTransfer = ((Bid_ControlReserve_Energy)bid).GetSegmentsToTransferToSimEvent(args.TimeStamp, args.Duration, calling_Activation);
                    if (segmentsToTransfer.Count() != 0)
                    {
                        Bid_ControlReserve_Activation BidTransfer = bid.Assets != null ? new Bid_ControlReserve_Activation(new List<IAsset>(bid.Assets), calling_Activation) : new Bid_ControlReserve_Activation();
                        foreach (var seg in segmentsToTransfer)
                        {
                            DateTime EndDuration = seg.TimeStamp.Add(seg.Duration);
                            for (DateTime time = seg.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                                BidTransfer.AddSegment(seg.ShortCode.ToActivation(time, args.Step), time, seg.VolumeMW, seg.Price);
                        }
                        BidTransfer.Clearing += (clearedBid) => clearedBid.UpdateSchedule();
                        Activation.SubmitActivation(this, BidTransfer);
                    }
                });
        }
        #endregion
    }
}
