﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets.Bids;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Assets;
using winMoz.Simulation;
using winMoz.Markets.Results;
using winMoz.Tools.Viz;

namespace winMoz.Markets.ControlReserve
{
    public class ControlReserve_Capacity : ControlReserveMarket
    {
        public ControlReserve_Energy EnergyMarket { get; set; }

        private ControlReserve_Capacity() { }
        public ControlReserve_Capacity(ControlReserve_Capacity_Attributes attributes, ControlReserve_Energy energyMarket, int scenarioID = 0) :
            base(attributes, scenarioID)
        {
            EnergyMarket = energyMarket ?? throw new ArgumentNullException(energyMarket.GetType().ToString());
            if (EnergyMarket != null) EnergyMarket.Auction += EnergyMarket_Auction;
        }

        public override void Initialize(bool IsComplete = true)
        {
            base.Initialize(IsComplete);
            if (EnergyMarket != null) EnergyMarket.Auction += EnergyMarket_Auction;
            if (IsComplete)
                Log.Info($"{this.GetType()} initialized.");
        }
        private void EnergyMarket_Auction(Market callingMarket, AuctionArgs args)
        {
           this.Bids
                .Where(bid => ((Bid_ControlReserve_Capacity)bid).HasValidSegments(args.TimeStamp, args.Duration, args.Step) &&
                    (bid is Bid_ControlReserve_Capacity) &&
                    ((Bid_ControlReserve_Capacity)bid).HasSegmentsToTransferToSimEvent(args.TimeStamp, args.Duration, callingMarket))
                .ToList()
                .ForEach(bid =>
                {
                    var segmentsToTransfer = ((Bid_ControlReserve_Capacity)bid).GetSegmentsToTransferToSimEvent(args.TimeStamp, args.Duration, callingMarket);
                    if (segmentsToTransfer.Count() != 0)
                    {
                        Bid_ControlReserve_Energy BidTransfer = bid.Assets != null ? new Bid_ControlReserve_Energy(new List<IAsset>(bid.Assets), callingMarket) : new Bid_ControlReserve_Energy(callingMarket);
                        foreach (var seg in segmentsToTransfer)
                        {
                            BidTransfer.AddSegment(seg);
                        }
                        BidTransfer.Clearing += (clearedBid) => clearedBid.UpdateSchedule();
                        EnergyMarket.SubmitBid(BidTransfer);
                    }
                });
        }

        protected override List<ControlReserveResult> CalcResults(ControlReserveShortCode.ShortCode controlReserveType)
        {
            List<ControlReserveResult> Results = new List<ControlReserveResult>();
            DateTime DurationEnd = EventTimeframe.TimeStamp.Add(EventTimeframe.Duration);
            var IsNegative = controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_aFRR || controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_mFRR;
            for (DateTime time = EventTimeframe.TimeStamp; time < DurationEnd; time = time.Add(EventTimeframe.Step))
            {
                var segmentsTime = SegmentsAuction.Where(seg => seg.TimeStamp == time).ToList();
                PlotAndOutputMeritOrder(segmentsTime, time, controlReserveType);
                var result = ControlReserve_Extensions.CalculateMarketResultsPayAsBidOneBuyer(segmentsTime, isEnergy: false, Lower_Bound_Price: this.Attributes.Lower_Price_Bound, Upper_Price_Bound: this.Attributes.Upper_Price_Bound, isNegative: IsNegative);
                if (result != null) Results.Add(result);
            }
            return Results;
        }

        protected override IEnumerable<Bid_Segment> GetSegmentsForAuction(ControlReserveShortCode.ShortCode controlReserveType)
        {
            return  BidsAuction.SelectMany(bid => ((Bid_ControlReserve_Capacity)bid).GetValidSegments(EventTimeframe.TimeStamp, EventTimeframe.Duration, controlReserveType))
                        .ToList();
        }

    }
}
