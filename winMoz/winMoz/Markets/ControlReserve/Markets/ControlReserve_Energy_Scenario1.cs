﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets.Bids;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;
using winMoz.Markets.EoMMarkets;

namespace winMoz.Markets.ControlReserve
{
    public class ControlReserve_Energy_Scenario1 : ControlReserve_Energy
    {

        public ControlReserve_Energy_Scenario1(ControlReserve_Energy_Attributes attributes, IntradayMarket intraday, ControlReserve_Activation activation, int scenarioID = 0) :
            base(attributes, intraday, activation, scenarioID)
        {
        }

        protected override List<ControlReserveResult> CalcResults(ControlReserveShortCode.ShortCode controlReserveType)
        {
            DateTime DurationEnd = EventTimeframe.TimeStamp.Add(EventTimeframe.Duration);
            List<ControlReserveResult> Results = new List<ControlReserveResult>();
            var IsNegative = controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_aFRR || controlReserveType == winMoz.Markets.ControlReserveShortCode.ShortCode.n_mFRR;
            for (DateTime time = EventTimeframe.TimeStamp; time < DurationEnd; time = time.Add(EventTimeframe.Step))
            {
                var segmentsTime = SegmentsAuction.Where(seg => seg.TimeStamp == time).ToList();
                var result = ControlReserve_Extensions.CalculateMarketResultsPayAsBidOneBuyer(segmentsTime, isEnergy: false, Lower_Bound_Price: this.Attributes.Lower_Price_Bound, Upper_Price_Bound: this.Attributes.Upper_Price_Bound);
                if (result != null)
                {
                    Results.Add(result);
                    //Nicht berücksichtige Gebote des Regelleistungsmarktes werden in den Intraday-Markt verschoben.
                    segmentsTime.Where(item => item.Price > result.Price_Max).Where(seg => ((Bid_Segment_ControlReserve)seg).TransferEvent is ControlReserve_Capacity).ToList().ForEach(seg => ((Bid_Segment_ControlReserve)seg).TransferEvent = Intraday);
                }
            }
            return Results;
        }

    }
}
