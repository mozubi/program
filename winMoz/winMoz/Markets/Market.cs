﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Helper;
using System.Diagnostics;
using winMoz.Tools.Viz;
using winMoz.Data.Access.Repositories.Prediction;

namespace winMoz.Markets
{
    public abstract class Market : SimEvent, IMarket
    {
        public MarketAttributes Attributes { get; private set; }
        private BidRepository bidRepo;
        public MarketResultHandler Results { get; private set; }
        protected BlockingCollection<Bid> Bids => bidRepo.Bids;

        protected List<Bid> BidsAuction { get; private set; }


        protected Market() { }
        protected Market(MarketAttributes attributes, int scenarioID = 0) : base(attributes.EventAttributes, scenarioID)
        {
            this.Attributes = attributes;
            Initialize();
        }

        public virtual void Initialize(bool IsComplete = true)
        {
            Log.Info($" Start initialization of {this.GetType()}");
            this.bidRepo = bidRepo ?? new BidRepository(this.Attributes.NumberOfDaysToLetBidsInBidList);
            //this.resultRepository = resultRepository ?? new MarketResultRepository();
            this.AcceptedBidType = Type.GetType(Attributes.AllowedBidType);
            //InitializeAttributes();
            InitializeEventTimes(this.Attributes.EventAttributes);
            if (IsComplete)
                Log.Info($"{this.GetType()} initialized.");
        }

        public virtual void SetSettings()
        {
            Log.Info($"Set Settings of {this.GetType()}");
            this.AcceptedBidType = Type.GetType(Attributes.AllowedBidType);
            //InitializeAttributes();
            InitializeEventTimes(this.Attributes.EventAttributes);
        }

        public void SaveBidsToDb() => bidRepo.SaveToDb();

        #region Markteigenschaften
        public delegate void AuctionEventHandler(Market sender, AuctionArgs eventArgs);

        /// <summary>
        /// <seealso cref="EventHandler"/> zur Auslösung der Auktion
        /// </summary>
        public event AuctionEventHandler Auction;

        protected Type AcceptedBidType { get; private set; }
        #endregion


        #region Marktabwicklung
        /// <summary>
        /// Methode zur Auslösung des Auktionsevents - überprüft Übergabezeitpunkt und löst Auktionsevent aus
        /// </summary>
        /// <param name="time">Simulationszeit als <see cref="DateTime"/></param>
        public virtual void HandleAuction(DateTime time = default)
        {
            if (IsEventTime(time) || time == default)
            {
                Log.Info("Automatic invocation of Auction - Market: " + this.GetType().Name + $" @ {this.EventTimeframe.TimeStamp}");
                // Gebote einsammeln
                //DateTime timeStart = DateTime.Now;
                InvokeAuctionEvent(GetAuctionArgs());
                //var timeExecutionEnd = DateTime.Now;
                //Log.Info("InvokeAuctionEvent started at: " + timeStart.ToLongTimeString());
                //Log.Info("InvokeAuctionEvent ended at: " + timeExecutionEnd.ToLongTimeString());
                //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
                //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 200) Debugger.Break();

                // Preise unlimitierter Gebote setzen
                //timeStart = DateTime.Now;
                SetUnlimitedBids();
                //timeExecutionEnd = DateTime.Now;
                //Log.Info("SetUnlimitedBids started at: " + timeStart.ToLongTimeString());
                //Log.Info("SetUnlimitedBids ended at: " + timeExecutionEnd.ToLongTimeString());
                //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
                //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 1000) Debugger.Break();

                //Filtern der Gebote für die Aktuelle Auktion
                //timeStart = DateTime.Now;
                SetBidsAuction();

                //Für eventuelle Plots und Outputs der Bids 
                PlotAndOutputBids();
                //timeExecutionEnd = DateTime.Now;
                //Log.Info("SetBidsAuction started at: " + timeStart.ToLongTimeString());
                //Log.Info("SetBidsAuction ended at: " + timeExecutionEnd.ToLongTimeString());
                //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
                //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 1000) Debugger.Break();

                // Auction durchführen
                //var timeStart = DateTime.Now;
                ExecuteAuction(out List<MarketResult> results);
                //var timeExecutionEnd = DateTime.Now;
                //Log.Info("ExecuteAuction started at: " + timeStart.ToLongTimeString());
                //Log.Info("ExecuteAuction ended at: " + timeExecutionEnd.ToLongTimeString());
                //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
                //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 1000) Debugger.Break();

                //var timeStart = DateTime.Now;
                UpdateResults(results);
                //var timeExecutionEnd = DateTime.Now;
                //Log.Info("UpdateResults started at: " + timeStart.ToLongTimeString());
                //Log.Info("UpdateResults ended at: " + timeExecutionEnd.ToLongTimeString());
                //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
                //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 1000) Debugger.Break();

                // Clearing der Gebote
                //var timeStart = DateTime.Now;
                ClearBids();
                //var timeExecutionEnd = DateTime.Now;
                //Log.Info("ClearBids started at: " + timeStart.ToLongTimeString());
                //Log.Info("ClearBids ended at: " + timeExecutionEnd.ToLongTimeString());
                //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
                //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 1000) Debugger.Break();

                UpdateEventTiming();

                //var timeStart = DateTime.Now;
                SaveBidsToDb();
                //var timeExecutionEnd = DateTime.Now;
                //Log.Info("SaveBidsToDb started at: " + timeStart.ToLongTimeString());
                //Log.Info("SaveBidsToDb ended at: " + timeExecutionEnd.ToLongTimeString());
                //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
                //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 1000) Debugger.Break();
            }
        }

        private void PlotAndOutputBids()
        {
            PlotAndOutput.Plot__Bids(this, BidsAuction);
            PlotAndOutput.OutputConsole__Bids(this, BidsAuction);
            PlotAndOutput.OutputCSV__Bids(this, BidsAuction);
        }

        protected virtual void UpdateResults(List<MarketResult> marketResults)
        {
            var results = new BlockingCollection<MarketResult>();
            foreach (var item in marketResults.ToList()) results.Add(item);
            this.Results = new MarketResultHandler() { _results = results };
            MarketResultRepository.Add(marketResults);
            using (var repo = new PredictionDataRepository())
            {
                repo.AddMarketResults(marketResults);
            }
        }

        private void SetUnlimitedBids()
        {
            Bids.SelectManyOrEmptyList(bid => bid.Segments).WhereOrEmptyList(seg => seg.IsUnlimited).ToList().ForEachOrNothing(seg => seg.SetUnlimited(this));
        }

        protected void SetBidsAuction()
            => BidsAuction = Bids.Where(bid => bid.IsClearingMarket(this) && bid.IsValid(this.EventTimeframe.TimeStamp, this.EventTimeframe.Duration, this.EventTimeframe.Step) && bid.Segments.All(seg => !float.IsNaN(seg.Price))).ToList();

        protected void InvokeAuctionEvent(AuctionArgs auctionArgs)
        {
            var delegates = Auction?.GetInvocationList();

            if (delegates != null) Parallel.ForEach(delegates, d => d.DynamicInvoke(this, auctionArgs));
            //if (delegates != null) foreach(var d in delegates) d.DynamicInvoke(this, auctionArgs);
        }

        /// <summary>
        /// <see langword="abstract"/> Methode zur Ausführung der Auktionierung nach Sammlung aller Gebote
        /// /// </summary>
        protected virtual void ExecuteAuction(out List<MarketResult> results) { results = new List<MarketResult>(); }

        //private void ClearBids() => Parallel.ForEach(this.Bids, bid => bid.Clear(this));

        private void ClearBids()
        {
            var BidsGroupedByAssets = this.BidsAuction.GroupBy(item => item.Assets, (el1, el2) => el2).ToList();
            //Parallel.ForEach(BidsGroupedByAssets, bids => bids.ToList().ForEach(bid => bid.Clear(this)));
            BidsGroupedByAssets.ForEach(bids => bids.ToList().ForEach(bid => bid.Clear(this)));
        }

        protected virtual bool IsValid(Bid bid)
            => bid.GetType() == this.AcceptedBidType && bid.IsValid(this.EventTimeframe.TimeStamp, this.EventTimeframe.Duration, this.EventTimeframe.Step);

        /// <summary>
        /// Methode zur Abgabe eines Gebots am abgeleiteten Markt
        /// </summary>
        /// <param name="bid">Gebot als BaseType <see cref="Bid"/></param>
        public virtual void SubmitBid(Bid bid)
        {
            if (IsValid(bid))
                this.Bids.Add(bid);
        }

        /// <summary>
        /// Methode zur Abgabe mehrerer Gebote am abgeleiteten Markt
        /// </summary>
        /// <param name="bids">Gebote als <see cref="IEnumerable"/> des Basetypes <see cref="Bid"/></param>
        public void SubmitBid(IEnumerable<Bid> bids) => bids.ToList().ForEach(bid => SubmitBid(bid));

        /// <summary>
        /// <see langword="abstract"/> Methode zum Abruf von Auktions-Eventargumenten
        /// </summary>
        /// <returns>Auktionsargumente <seealso cref="AuctionArgs"/></returns>
        protected virtual AuctionArgs GetAuctionArgs() => new AuctionArgs(this.EventTimeframe);

        public void SaveResultsToDB() => MarketResultRepository.CacheToDB();


        #endregion
    }

    public class MarketResultHandler
    {
        internal BlockingCollection<MarketResult> _results;
        public MarketResult Get(ShortCode shortCode, DateTime timeStamp) => _results.SingleOrDefault(result => result.ShortCode == shortCode && result.TimeStamp.Ticks == timeStamp.Ticks);
    }
}
