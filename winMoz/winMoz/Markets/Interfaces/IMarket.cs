﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Helper;
using System.Diagnostics;
using winMoz.Tools.Viz;

namespace winMoz.Markets
{
    public interface IMarket : ISimEvent
    {
        MarketAttributes Attributes { get; }

        void SaveBidsToDb();

        #region Markteigenschaften
        #endregion


        #region Marktabwicklung
        /// <summary>
        /// Methode zur Auslösung des Auktionsevents - überprüft Übergabezeitpunkt und löst Auktionsevent aus
        /// </summary>
        /// <param name="time">Simulationszeit als <see cref="DateTime"/></param>
        void HandleAuction(DateTime time = default);

        /// <summary>
        /// Methode zur Abgabe eines Gebots am abgeleiteten Markt
        /// </summary>
        /// <param name="bid">Gebot als BaseType <see cref="Bid"/></param>
        void SubmitBid(Bid bid);

        /// <summary>
        /// Methode zur Abgabe mehrerer Gebote am abgeleiteten Markt
        /// </summary>
        /// <param name="bids">Gebote als <see cref="IEnumerable"/> des Basetypes <see cref="Bid"/></param>
        void SubmitBid(IEnumerable<Bid> bids);

        void SaveResultsToDB();

        #endregion
    }

    public interface IMarketResultHandler
    {
        MarketResult Get(ShortCode shortCode, DateTime timeStamp);
    }
}
