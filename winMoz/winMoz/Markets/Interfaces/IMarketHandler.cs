﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;

namespace winMoz.Markets
{
    /// <summary>
    /// <see cref="MarketHandler"/>-Klasse wickelt alle Aktionen von und für Märkte über die Simulationszeit ab
    /// </summary>
    public interface IMarketHandler
    {
        List<Market> Markets { get;  }
        MarketResultRepository Results { get; }
        void AddMarket(Market market);
        void AddMarket(IEnumerable<Market> markets);

        Market GetMarket<TMarket>() where TMarket : Market;

        void RunMarkets(DateTime timeStamp);
    }
}
