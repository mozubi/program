﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Markets.Bids.EoMMarkets
{
    /// <summary>
    /// Gebotsklasse für die Marktkoppelung
    /// </summary>
    public class BidTupel : IBidTupel
    {
        /// <summary>
        /// Preis des Gebots
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Menge des Gebots
        /// </summary>
        public decimal Volume { get; set; }
        /// <summary>
        /// Position in Angebotskurve, Nachfragekurve oder MeritOrder 
        /// </summary>
        public decimal AggVol { get; set; }
        /// <summary>
        /// Ist jedenfalls zu Teilen Nachfragegebot
        /// </summary>
        public bool IsPurchase { get; set; }
        /// <summary>
        /// Anteil, der Menge, die der Nachfrage zugeordnet ist.
        /// </summary>
        public decimal VolumePurchase { get; set; }

        /// <summary>
        /// Gebotsklasse für die Marktkoppelung
        /// </summary>
        /// <param name="Price"></param>
        /// <param name="Volume"></param>
        public BidTupel(decimal price, decimal volume, bool isPurchase, decimal volumePurchase, decimal aggVol = 0)
        {
            this.Price = price;
            this.Volume = volume;
            this.AggVol = aggVol;
            this.IsPurchase = isPurchase;
            this.VolumePurchase = volumePurchase;
        }

        public class PayAsBid : BidTupel
        {
            public decimal CostRevenues {get; set;}
            public PayAsBid(decimal price, decimal volume, bool isPurchase, decimal volumePurchase, decimal aggVol = 0, decimal costRevenues = 0) 
            : base(price, volume, isPurchase, volumePurchase, aggVol)
            {
            }
        }
    }
}
