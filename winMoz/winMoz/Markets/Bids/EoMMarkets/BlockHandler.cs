﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Helper;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;
using winMoz.Simulation;

namespace winMoz.Markets.Bids.EoMMarkets
{
    public class BlockHandler : IBlockHandler
    {
        public Dictionary<ShortCode, Block> Blocks { get; set; }
        public Dictionary<int, MA_Auction> LocalAuctions { get; set; }
        public Dictionary<int, MarketCoupling> MarketCoupling { get; set; }
        public Dictionary<int, Dictionary<int, decimal>> BlockShift { get; set; }
        bool WithMarketCoupling;
        decimal UBPrice;
        decimal LBPrice;
        public BlockHandler(Dictionary<int, MA_Auction> localAuctions, Dictionary<int, MarketCoupling> marketCoupling, bool withMarketCoupling, List<ShortCode> blocks, decimal ubPrice, decimal lbPrice)
        {
            ShortCode.ShortCodeEqualityComparer eqShortCode = new ShortCode.ShortCodeEqualityComparer();
            Blocks = new Dictionary<ShortCode, Block>(eqShortCode);
            LocalAuctions = localAuctions;
            MarketCoupling = marketCoupling;
            BlockShift = new Dictionary<int, Dictionary<int, decimal>>();
            WithMarketCoupling = withMarketCoupling;
            UBPrice = ubPrice;
            LBPrice = lbPrice;
            AddBlocks(blocks);
            InitialiseBlockShift();
        }

        #region Initial

        public void AddBlocks(List<ShortCode> blocks)
        {
            blocks.ForEach(block => Blocks.Add(block, new Block(block, this.UBPrice, this.LBPrice)));
        }

        private void InitialiseBlockShift()
        {
            int End = Blocks.Select(item => item.Value.BlockType.GetLevel()).Max();
            for (int hour = 0; hour < 24; hour++)
            {
                Dictionary<int, decimal> BlockShiftHour = new Dictionary<int, decimal>();
                for (int level = 1; level <= End; level++)
                    BlockShiftHour.Add(level, 0);
                BlockShift.Add(hour, BlockShiftHour);
            }
        }

        #endregion

        #region Je Auktionsereignis

        public void PrepareBlocks()
        {
            foreach (var Block in Blocks)
                Block.Value.PrepareBlocks();
        }

        public void Reset()
        {
            foreach (var blockList in Blocks) blockList.Value.Reset();
        }

        public void AddBlockBidToBlock(ShortCode code, decimal price, decimal volume, bool isSell)
        {
            if (code == ShortCode.DayAhead.OffPeak)
            {
                var code1 = ShortCode.DayAhead.OffPeak1;
                Blocks[code1].AddBlockBid(new BlockBid(code1, price, volume, isSell));
                var code2 = ShortCode.DayAhead.OffPeak2;
                Blocks[code2].AddBlockBid(new BlockBid(code2, price, volume, isSell));
            }
            else
                Blocks[code].AddBlockBid(new BlockBid(code, price, volume, isSell));
        }

        public Dictionary<ShortCode, float> CalculateMarketResult()
        {
            int level = 1;
            int count = 0;
            int maxLevel = Blocks.Select(block => block.Value.GetLevel()).Max();
            if (WithMarketCoupling)
                foreach(var tmpMCC in MarketCoupling)
                    tmpMCC.Value.CalculateExportImport();
            if (Blocks.Count() > 0 && Blocks.Any(block => !block.Value.IsEmpty()))
            {
                while (!((Blocks.Where(Block => !Block.Value.IsEmpty()).All(Block => Block.Value.StartIndexEqualsResultIndex()) && count > maxLevel) || count > maxLevel * 10))
                {
                    var BlocksLevel = Blocks.Where(block => block.Value.GetLevel() == level);
                    foreach (var block in BlocksLevel)
                    {
                        if (!block.Value.IsEmpty())
                        {
                            block.Value.IndexStart = block.Value.IndexResult;
                            var blockResult = FindBlockResult(block.Value.BlockType, level);
                            SetBlockShift(blockResult, block.Value.BlockType);
                        }
                    }
                    if (level != maxLevel)
                        level++;
                    else
                        level = 1;
                    count++;
                }
            }
            Dictionary<ShortCode, float> result = new Dictionary<ShortCode, float>(
                Blocks.Where(item => !item.Value.IsEmpty()).Select(item => (item.Value.BlockType, item.Value.GetPrice(item.Value.IndexResult))).ToDictionary(block => block.BlockType, block => (float)block.Item2));
            return result;
        }

        private void SetBlockShift(BlockAlgResult result, ShortCode blockType)
        {
            foreach(int hour in blockType.GetBlockHours())
                BlockShift[hour][blockType.GetLevel()] = result.VolumeBlock;            
        }

        /// <summary>
        /// Methode liefert das Optimale (Zwischen)Ergebnis des jeweiligen Blockgebots unter Berücksichtigung:
        /// a) der übergeordneten Blockgebote
        /// b) Marktkopplung
        /// 
        /// Ist eine Teilmethode im Rahmen der Bestimmung des Marktergebnisses unter Berücksichtigung von (Zweistufigen) Blockgeboten 
        /// 
        /// ALG:
        /// Suche die Maximale Differenz Dp zwischen dem Blockausfürhungspreis Pb und dem (daraus) resultierenden,
        /// mittleren Marktpreis Pm über die Blockstunden, mit Dp = Pb - Pm und Max(Dp) kleinergleich 0;           
        /// Notwendig für die Weiterberechnung ist nur der Index. Die Anderen Werte dienen primär der Fehleridentifikation. 
        /// </summary>
        /// <param name="Block"></param>
        /// <param name="BolWithMarketCoupling"></param>
        /// <returns></returns>
        private BlockAlgResult FindBlockResult(ShortCode blockType, int level)
        {
            int[] BlockHours;
            BlockAlgResult Result;

            int borderFirst, borderLast, first, last; //Parameter für Binäre Suche

            //Initialisiere Werte
            BlockHours = blockType.GetBlockHours();

            //Grenzen des Lösungsraums bestimmen
            (borderFirst, borderLast) = GetStartIndeces(blockType);

            //Suche zwei Indeces, zu denen die Lösung gehört.
            (first, last) = GetResultIndeces(blockType, borderFirst, borderLast, level);

            //Bestimme aus diesen zwei Indeces die Lösung
            Result = GetResultFromResultIndeces(blockType, first, last, level);
            Blocks[blockType].IndexResult = Result.IndexResultBlock;
            return Result;
        }

        private (int first, int last) GetStartIndeces(ShortCode blockType)
        {
            decimal Price_WithoutBlock, Price_WithBlock;
            int Index_WithoutBlock, Index_WithBlock;

            Price_WithoutBlock = GetPriceWithoutLevel(blockType.GetBlockHours(), blockType.GetLevel());
            Index_WithoutBlock = GetIndexFromPrice(blockType, Price_WithoutBlock);
            Price_WithBlock = GetPriceWithLevel(blockType.GetBlockHours(), blockType.GetLevel(), Blocks[blockType].GetShift(Index_WithoutBlock));
            Index_WithBlock = GetIndexFromPrice(blockType, Price_WithBlock);
            return (Math.Min(Index_WithoutBlock, Index_WithBlock), Math.Max(Index_WithoutBlock, Index_WithBlock));
        }

        private (int a, int c) GetResultIndeces(ShortCode blockType, int first, int last, int level)
        {
            int a, b, c;
            decimal P_Average, DeltaP, BlockPrice, BlockShift;
            a = first;
            c = last;
            if (a == c || a + 1 == c) return (a, c);
            b = (a + c) / 2;
            int Count = c - a + 1;

            BlockPrice = GetBlockPrice(blockType, b);
            BlockShift = GetBlockShift(blockType, b);

            for (int i = 0; 2 + (1 - Math.Pow(2, i + 1)) / (1 - 2) <= Count; i++) //Ausfallbedingung durch geometrische Summenformel.
            {
                (P_Average, DeltaP) = GetHourAverageAndDelta(blockType.GetBlockHours(), BlockPrice, BlockShift, level);

                //Bestimme a,b,c neu
                if (IsAnyVolumeAboveUpperBorder(blockType.GetBlockHours()) || DeltaP > 0) //DeltaP zu hoch, suche bei niedrigeren Preisen (Springe nach unten in der Liste) 
                    if (b - a == 1)
                    {
                        c = b;
                        b = a;
                    }
                    else
                    {
                        c = b;
                        b = (a + c) / 2;
                    }
                else if (IsAnyVolumeBelowLowerBorder(blockType.GetBlockHours()) || DeltaP <= 0)
                {
                    a = b;
                    b = (a + c) / 2;
                }
                BlockPrice = GetBlockPrice(blockType, b);
                BlockShift = GetBlockShift(blockType, b);
            }
            return (a, c);
        }

        private BlockAlgResult GetResultFromResultIndeces(ShortCode blockType, int first, int last, int level)
        {
            decimal P_Average, DeltaP, BlockPrice, BlockShift;
            int index = first;
            BlockPrice = GetBlockPrice(blockType, first);
            BlockShift = GetBlockShift(blockType, first);
            (P_Average, DeltaP) = GetHourAverageAndDelta(blockType.GetBlockHours(), BlockPrice, BlockShift, level);
            if (IsAnyVolumeAboveUpperBorder(blockType.GetBlockHours()) || DeltaP > 0)
            {
                if (IsAnyVolumeAboveUpperBorder(blockType.GetBlockHours()))
                     Log.Warning("One auction step price with upper bound price after block shift");
            }
            else if (!IsAnyVolumeBelowLowerBorder(blockType.GetBlockHours()))
            {
                index = last;
                BlockPrice = GetBlockPrice(blockType, last);
                BlockShift = GetBlockShift(blockType, last);
                (P_Average, DeltaP) = GetHourAverageAndDelta(blockType.GetBlockHours(), BlockPrice, BlockShift, level);
                if (IsAnyVolumeBelowLowerBorder(blockType.GetBlockHours()))
                    Log.Warning("One auction step price with lower bound price after block shift");
            }
            return new BlockAlgResult(P_Average, index, BlockPrice, BlockShift);
        }

        private (decimal Average, decimal Delta) GetHourAverageAndDelta(int[] blockHours, decimal blockPrice, decimal BlockVolumeShift, int level)
        {
            //Berechne temporären, mittleren Preis über Blockstunden
            CalculateMarketResultWithBlockshift(blockHours, GetBlockShiftsWithLevel(blockHours, level, BlockVolumeShift));
            decimal P_Average = GetAveragePriceLocalAuction(blockHours);

            //Bereche DeltaP
            decimal DeltaP = blockPrice - P_Average;

            return (P_Average, DeltaP);
        }

        /// <summary>
        /// Funktion prüft, ob eine lokale Auktion im Bereich eines Angebotsüberschusses liegt.
        /// </summary>
        /// <param name="BlockHours"></param>
        /// <returns></returns>
        private bool IsAnyVolumeBelowLowerBorder(int[] BlockHours) => LocalAuctions.Where(item => BlockHours.Contains(item.Key)).Any(item => item.Value.P_Result == item.Value.LB_Price);

        /// <summary>
        /// Funktion prüft, ob eine lokale Auktion im Bereich eines Nachfrageüberschusses liegt.
        /// </summary>
        /// <param name="BlockHours"></param>
        /// <returns></returns>
        private bool IsAnyVolumeAboveUpperBorder(int[] BlockHours) => LocalAuctions.Where(item => BlockHours.Contains(item.Key)).Any(item => item.Value.P_Result == item.Value.UB_Price);


        private decimal GetPriceWithoutLevel(int[] blockHours, int level)
        {
            var BlockShiftPartial = GetBlockShiftsWithoutLevel(blockHours, level); 
            CalculateMarketResultWithBlockshift(blockHours, BlockShiftPartial);
            return GetAveragePriceLocalAuction(blockHours);
        }

        private decimal GetPriceWithLevel(int[] blockHours, int level, decimal blockShift)
        {
            var BlockShiftPartial = GetBlockShiftsWithLevel(blockHours, level, blockShift);
            CalculateMarketResultWithBlockshift(blockHours, BlockShiftPartial);
            return GetAveragePriceLocalAuction(blockHours);
        }

        private decimal GetBlockPrice(ShortCode code, int index) => Blocks[code].GetPrice(index);

        private decimal GetBlockShift(ShortCode code, int index) => Blocks[code].GetShift(index);

        /// <summary>
        /// Gibt die durch das Einbeziehen der Blockgebote 
        /// der nicht angegebenen Ebenen hervorgerufenen Verschiebung 
        /// der Meritorder für die Blockstunden zurück.
        /// </summary>
        /// <param name="blockHours"></param>
        /// <param name="level"></param>
        /// <param name="blockShiftLevel"></param>
        /// <returns></returns>
        private Dictionary<int, decimal> GetBlockShiftsWithoutLevel(int[] blockHours, int level)
            => BlockShift
                .Where(shift => blockHours.ToList().Contains(shift.Key))
                .Select(shift => (shift.Key, shift.Value.Where(shift2 => shift2.Key != level)
                    .Select(shift2 => shift2.Value).Sum()))
                .ToDictionary(shift => shift.Key, shift => shift.Item2);

        /// <summary>
        /// Gibt die durch das Einbeziehen der Blockgebote 
        /// der nicht angegebenen Ebenen hervorgerufenen Verschiebung 
        /// der Meritorder plus die angegebene Verschiebung der angegebenen Ebene 
        /// für die Blockstunden zurück.
        /// </summary>
        /// <param name="blockHours"></param>
        /// <param name="level"></param>
        /// <param name="blockShiftLevel"></param>
        /// <returns></returns>
        private Dictionary<int, decimal> GetBlockShiftsWithLevel(int[] BlockHours, int level, decimal blockShift)
        {
            return BlockShift
                .Where(shift => BlockHours.ToList().Contains(shift.Key))
                .Select(shift => (shift.Key, shift.Value.Where(shift2 => shift2.Key != level)
                    .Select(shift2 => shift2.Value).Sum() + blockShift))
                .ToDictionary(shift => shift.Key, shift => shift.Item2);
        }

        private int GetIndexFromPrice(ShortCode code, decimal Price) => Blocks[code].GetIndexOfPriceMin(Price);

        private void CalculateMarketResultWithBlockshift(int[] BlockHours, Dictionary<int, decimal> shiftFromStart)
        {
            MA_Auction LocalAuction;
            MarketCoupling tmpMCC;
            foreach (int Hour in BlockHours)
            {
                //Für jede Blockstunde setze Referenz auf Objekte
                LocalAuction = LocalAuctions[Hour];

                //if (!LocalAuction.AuctionFailed() || !LocalAuction.IsMeritOrderEmpty())
                //{
                    //Neues Volumen und Preis mit Blockgeboten einstellen
                    LocalAuction = EoM_Extensions.ShiftMeritOrderFromStart(LocalAuction, shiftFromStart[Hour]);

                    //Starte Marktkopplung
                    if (WithMarketCoupling)
                    {
                        tmpMCC = MarketCoupling[Hour];
                        tmpMCC.CalculateExportImport();
                    }
                //}
            }
        }

        private decimal GetAveragePriceLocalAuction(int[] BlockHours)
            => LocalAuctions.Where(item => BlockHours.Contains(item.Key)).Select(auctions => auctions.Value.P_Result).Sum() / BlockHours.Count();

        #endregion
        
    }


    public class BlockAlgResult : IBlockAlgResult
    {
        /// <summary>
        /// Mittlerer Marktpreis nach (temporärer) Optimierung Blockgebote und MCC
        /// </summary>
        public decimal AverageMarketPrice { get; set; }
        /// <summary>
        /// Index des Blocks beim Ergebnis
        /// </summary>
        public int IndexResultBlock { get; set; }
        /// <summary>
        /// (notwendiger) Preis der Blockgebote beim Index
        /// </summary>
        public decimal PriceBlockAtIndex { get; set; }
        /// <summary>
        /// Menge der bei dem Index ausgeführten Blockgebote
        /// </summary>
        public decimal VolumeBlock { get; set; }
        public BlockAlgResult(decimal averageMarketPrice, int indexResultBlock, decimal priceBlockAtIndex, decimal volumeBlock)
        {
            AverageMarketPrice = averageMarketPrice;
            IndexResultBlock = indexResultBlock;
            PriceBlockAtIndex = priceBlockAtIndex;
            VolumeBlock = volumeBlock;
        }
    }
}
