﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;

namespace winMoz.Markets.Bids.EoMMarkets
{
    public interface IBlockBid
    {
        int Index { get; set; }
        ShortCode Type { get; set; }
        bool IsSell { get; set; }
        decimal BlockPrice { get; set; }
        decimal BlockVolume { get; set; }
        decimal AggBlockVolumeSell { get; set; }
        decimal AggBlockVolumePurchase { get; set; }
        decimal Shift { get; set; }

        void CalculateShift();
    }
}
