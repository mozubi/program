﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;

namespace winMoz.Markets.Bids.EoMMarkets
{
    public interface IBlock
    {
        ShortCode BlockType { get; set; }
        int IndexStart { get; set; }
        int IndexResult { get; set; }

        decimal UBPrice { get; set; }
        decimal LBPrice { get; set; }

        void Reset();

        bool IsEmpty();

        bool StartIndexEqualsResultIndex();

        int GetLevel();

        void AddBlockBid(BlockBid blockBid);

        void OrderBlocksByDescendingPrice();

        void PrepareBlocks();

        void Aggregate();

        void CoupleDoupletts();

        void CalculateShift();

        void SetIndeces();

        int GetIndexOfPriceMin(decimal PriceMin);

        decimal GetPrice(int index);

        decimal GetShift(int index);

    }
}
