﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Helper;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;

namespace winMoz.Markets.Bids.EoMMarkets
{
    public interface IBlockHandler
    {
        Dictionary<ShortCode, Block> Blocks { get; set; }
        Dictionary<int, MA_Auction> LocalAuctions { get; set; }
        Dictionary<int, MarketCoupling> MarketCoupling { get; set; }
        Dictionary<int, Dictionary<int, decimal>> BlockShift { get; set; }

        #region Initial

        void AddBlocks(List<ShortCode> blocks);

        #endregion

        #region Je Auktionsereignis

        void PrepareBlocks();

        void Reset();

        void AddBlockBidToBlock(ShortCode code, decimal price, decimal volume, bool isSell);

        Dictionary<ShortCode, float> CalculateMarketResult();
        #endregion

    }

    public interface IBlockAlgResult
    {
        /// <summary>
        /// Mittlerer Marktpreis nach (temporärer) Optimierung Blockgebote und MCC
        /// </summary>
        decimal AverageMarketPrice { get; set; }
        /// <summary>
        /// Index des Blocks beim Ergebnis
        /// </summary>
        int IndexResultBlock { get; set; }
        /// <summary>
        /// (notwendiger) Preis der Blockgebote beim Index
        /// </summary>
        decimal PriceBlockAtIndex { get; set; }
        /// <summary>
        /// Menge der bei dem Index ausgeführten Blockgebote
        /// </summary>
        decimal VolumeBlock { get; set; }
    }
}
