﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Markets.Bids.EoMMarkets
{
    /// <summary>
    /// Gebotsklasse für die Marktkoppelung
    /// </summary>
    public interface IBidTupel
    {
        /// <summary>
        /// Preis des Gebots
        /// </summary>
        decimal Price { get; set; }
        /// <summary>
        /// Menge des Gebots
        /// </summary>
        decimal Volume { get; set; }
        /// <summary>
        /// Position in Angebotskurve, Nachfragekurve oder MeritOrder 
        /// </summary>
        decimal AggVol { get; set; }
        /// <summary>
        /// Ist jedenfalls zu Teilen Nachfragegebot
        /// </summary>
        bool IsPurchase { get; set; }
        /// <summary>
        /// Anteil, der Menge, die der Nachfrage zugeordnet ist.
        /// </summary>
        decimal VolumePurchase { get; set; }

    }

    public interface IPayAsBid : IBidTupel
    {
        decimal CostRevenues { get; set; }
    }
}
