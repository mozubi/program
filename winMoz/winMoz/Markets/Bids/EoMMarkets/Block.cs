﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;

namespace winMoz.Markets.Bids.EoMMarkets
{
    public class Block : IBlock
    {
        public ShortCode BlockType { get; set; }
        List<BlockBid> BlockBids;
        public int IndexStart { get; set; }
        public int IndexResult { get; set; }

        public decimal UBPrice { get; set; }
        public decimal LBPrice { get; set; }


        public Block(ShortCode block, decimal ubPrice, decimal lbPrice)
        {
            BlockBids = new List<BlockBid>();
            BlockType = block;
            IndexStart = -1;
            IndexResult = -1;
            UBPrice = ubPrice;
            LBPrice = lbPrice;
        }

        public void Reset()
        {
            BlockBids.Clear();
            ResetStartAndResultIndex();
            BlockBids.Add(new BlockBid(BlockType, blockPrice: LBPrice, blockVolume: 0, isSell: true));
            BlockBids.Add(new BlockBid(BlockType, blockPrice: UBPrice, blockVolume: 0, isSell: false));
        }

        public bool IsEmpty() => BlockBids.Select(item => item.BlockVolume).Sum() == 0;

        public bool StartIndexEqualsResultIndex() => IndexStart == IndexResult;

        public int GetLevel() => BlockType.GetLevel();

        public void AddBlockBid(BlockBid blockBid) => BlockBids.Add(blockBid);

        public void OrderBlocksByDescendingPrice() => BlockBids = BlockBids.OrderByDescending(item => item.BlockPrice).ToList();

        public void PrepareBlocks()
        {
            if (BlockBids.Count > 0)
            {
                Aggregate();
                CoupleDoupletts();
                CalculateShift();
                SetIndeces();
                ResetStartAndResultIndex();
            }
        }

        private void ResetStartAndResultIndex()
        {
            IndexStart = -1;
            IndexResult = -1;
        }

        public void Aggregate()
        {
            OrderBlocksByDescendingPrice();
            decimal AggVolSell = 0;
            decimal AggVolPurchase = 0;
            foreach (BlockBid block in BlockBids)
            {
                if (block.IsSell)
                {
                    AggVolSell += block.BlockVolume;
                    block.AggBlockVolumeSell = AggVolSell;
                    block.AggBlockVolumePurchase = AggVolPurchase;
                }
                else
                {
                    AggVolPurchase += block.BlockVolume;
                    block.AggBlockVolumePurchase = AggVolPurchase;
                    block.AggBlockVolumeSell = AggVolSell;
                }  
            }
            decimal MaxSell = BlockBids.Select(item => item.AggBlockVolumeSell).Max();
            foreach (BlockBid block in BlockBids)
                block.AggBlockVolumeSell = MaxSell - block.AggBlockVolumeSell;
        }

        public void CoupleDoupletts()
        {
            decimal PriceTmp = BlockBids[BlockBids.Count - 1].BlockPrice;
            decimal VolTmp, VolSellTmp, VolPurchaseTmp;
            VolTmp = 0;
            VolSellTmp = 0;
            VolPurchaseTmp = 0;
            for (int i = BlockBids.Count - 2; i >= 0; i--)
            {
                if (BlockBids[i].BlockPrice == PriceTmp)
                {
                    VolTmp += BlockBids[i].BlockVolume;
                    VolSellTmp += BlockBids[i].AggBlockVolumeSell;
                    VolPurchaseTmp += BlockBids[i].AggBlockVolumePurchase;
                    BlockBids[i].BlockVolume = VolTmp;
                    BlockBids[i].AggBlockVolumeSell = VolSellTmp;
                    BlockBids[i].AggBlockVolumePurchase = VolPurchaseTmp;
                    BlockBids.RemoveAt(i + 1);
                }
                else
                {
                    VolTmp = BlockBids[i].BlockVolume;
                    VolSellTmp += BlockBids[i].AggBlockVolumeSell;
                    VolPurchaseTmp = BlockBids[i].AggBlockVolumePurchase;
                    PriceTmp = BlockBids[i].BlockPrice;
                }
            }
        }

        public void CalculateShift() => BlockBids.ForEach(Block => Block.CalculateShift());

        public void SetIndeces()
        {
            OrderBlocksByDescendingPrice();
            for(int idx = 0; idx < BlockBids.Count(); idx++)
                BlockBids[idx].Index = idx;
        }

        public int GetIndexOfPriceMin(decimal PriceMin)
        {
            var blocks = BlockBids.Where(item => item.BlockPrice <= PriceMin);
            if (blocks.Count() > 0)
                return blocks.OrderByDescending(item => item.BlockPrice).FirstOrDefault().Index;
            else
            {
                Console.WriteLine("BlockList-Fehler: Preis zu klein");
                return BlockBids.OrderBy(item => item.BlockPrice).FirstOrDefault().Index;
            }
        }

        public decimal GetPrice(int index) => BlockBids[index].BlockPrice;

        public decimal GetShift(int index) => BlockBids[index].Shift;

    }
}
