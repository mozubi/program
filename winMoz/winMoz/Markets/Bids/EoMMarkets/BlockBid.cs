﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;

namespace winMoz.Markets.Bids.EoMMarkets
{
    public class BlockBid : IBlockBid
    {
        public int Index { get; set; }
        public ShortCode Type { get; set; }
        public bool IsSell { get; set; }
        public decimal BlockPrice { get; set; }
        public decimal BlockVolume { get; set; }
        public decimal AggBlockVolumeSell { get; set; }
        public decimal AggBlockVolumePurchase { get; set; }
        public decimal Shift { get; set; }

        public BlockBid(ShortCode type, decimal blockPrice, decimal blockVolume, bool isSell)
        {
            Type = type;
            BlockPrice = blockPrice;
            BlockVolume = blockVolume;
            IsSell = isSell;
        }

        public void CalculateShift() => Shift = AggBlockVolumePurchase - AggBlockVolumeSell;
    }
}
