﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Markets
{
    public static class ControlReserveShortCode
    {
        public enum ShortCode
        {
            FRR, //0
            aFRR, //1
            mFRR, //2
            p_aFRR, //3
            n_aFRR, //4
            p_mFRR, //5
            n_mFRR //6
        }

        public static int[] GetAuctionTypes()
            => new int[] { 3, 4, 5, 6 };

        public static int[] GetSyntethicDemandTypes()
            => new int[] { 0, 1 };
    }
}
