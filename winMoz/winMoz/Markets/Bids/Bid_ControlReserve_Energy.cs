﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Assets;
using winMoz.Markets.ControlReserve;
using winMoz.Assets.Plants;
using winMoz.Simulation;

namespace winMoz.Markets.Bids
{
    /// <summary>
    /// Abgeleitete Klasse der Klasse Bid_ControlReserve für die Regelarbeit
    /// </summary>
    public class Bid_ControlReserve_Energy : Bid_ControlReserve
    {
        /// <summary>
        /// Base-Konstruktor (wird für EntityFramework benötigt)
        /// </summary>
        public Bid_ControlReserve_Energy() : base() { }
        /// <summary>
        /// Base-Konstruktor (wird für EntityFramework benötigt)
        /// </summary>
        public Bid_ControlReserve_Energy(Market calling) : base(calling) { }
        /// <summary>
        /// Base-Konstruktor
        /// </summary>
        /// <param name="assets">Kraftwerke, die an Gebot beteiligt sind.</param>
        /// <param name="calling">ID des Events auf dem dieses Gebot abgegeben wird</param>
        public Bid_ControlReserve_Energy(IEnumerable<IAsset> assets, Market calling) : base(assets, calling) { }


        protected override void TransferBidSegments(SimEvent calling) => Segments.Where(item => item.IsCleared).ToList().ForEach(seg => ((Bid_Segment_ControlReserve)seg).TransferEvent = ((ControlReserve_Energy)calling).Activation);

        /// <summary>
        /// Erzeugt Sekundärregelarbeitsgebot aus Zeitreihe
        /// </summary>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        /// <returns></returns>
        public Bid_ControlReserve aFRR(TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float PriceEnergy)> timeSeries)
        {
            if (timeSeries.Count > 1)
                timeSeries = timeSeries.Resample(Resampling.GetTimeStep(stepDuration));

            for (int i = 0; i < timeSeries.Count; i++)
                if (Math.Round(timeSeries[i].Item2, 1) != 0)
                {
                    ShortCode codeEnergy = timeSeries[i].Item2 > 0 ? ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_aFRR, timeSeries[i].TimeStamp, stepDuration) : ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_aFRR, timeSeries[i].TimeStamp, stepDuration);
                    AddSegment(codeEnergy, timeSeries[i].TimeStamp, timeSeries[i].PowerMW, timeSeries[i].PriceEnergy);
                }
            return this;
        }

        /// <summary>
        /// Erzeugt Minute-Regelarbeitsgebot aus Zeitreihe
        /// </summary>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        public Bid_ControlReserve mFRR(TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float PriceEnergy)> timeSeries)
        {
            if (timeSeries.Count > 1)
                timeSeries = timeSeries.Resample(Resampling.GetTimeStep(stepDuration));

            for (int i = 0; i < timeSeries.Count; i++)
                if (Math.Round(timeSeries[i].Item2, 1) != 0)
                {
                    ShortCode codeEnergy = timeSeries[i].Item2 > 0 ? ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_aFRR, timeSeries[i].TimeStamp, stepDuration) : ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_aFRR, timeSeries[i].TimeStamp, stepDuration);
                    AddSegment(codeEnergy, timeSeries[i].TimeStamp, timeSeries[i].PowerMW, timeSeries[i].PriceEnergy);
                }
            return this;
        }

        /// <summary>
        /// Überprüft, ob es für den angegebenen Bestimmungszeitraum valide Gebotssegmente gibt
        /// </summary>
        /// <param name="time2check">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="step">Abschnitt des Bestimmungszeitraums</param>
        /// <returns></returns>
        public override bool HasValidSegments(DateTime time2check, TimeSpan duration, TimeSpan step)
        {
            DateTime DurationEnd = time2check.Add(duration);
            return Segments.Count() != 0 ?
                (Segments.Any(seg => seg.TimeStamp.IsStartTimeOfDuration(step) && seg.TimeStamp >= time2check && seg.TimeStamp < DurationEnd && seg.IsEnergy)) : false;
        }

        /// <summary>
        /// Gibt valide Gebotssegmente für Bestimmungszeitraum zurück
        /// </summary>
        /// <param name="timeStamp">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="type">Regellleistungstyp</param>
        /// <returns></returns>
        public override List<Bid_Segment_ControlReserve> GetValidSegments(DateTime timeStamp, TimeSpan duration, ControlReserveShortCode.ShortCode type)
            => base.GetValidSegments(timeStamp, duration, type).Where(seg => seg.IsEnergy).ToList();

        /// <summary>
        /// Füge Gebot hinzu
        /// </summary>
        /// <param name="code">Shortcode des Gebotssegments</param>
        /// <param name="timeStamp">Start des Bestimmungszeitraums des Gebots</param>
        /// <param name="volume">Menge des Gebots</param>
        /// <param name="price">Preis des Gebots</param>
        public void AddSegment(ShortCode code, DateTime timeStamp, float volume, float price)
            => base.AddSegment(code, timeStamp, volume, price);

        protected override List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> GetFreeReserveSchedule() => this.GetControlReserveTimeSeries();

        protected override void UpdateControlReserveSchedule(IAsset asset, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> toAdd, float assetShareOfVolume, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> free = null)
        {
            if (free != null)
            {
                var FreePositiveReservationSchedule = free.Where(item => item.Values.Any(el => el.Key.ToString().Contains("m_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.volumeMW).Sum())).ToList();
                var FreeNegativeReservationSchedule = free.Where(item => item.Values.Any(el => el.Key.ToString().Contains("n_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.volumeMW).Sum())).ToList();
                asset.ControlReserveSchedule.FreePositiveReservation(FreePositiveReservationSchedule);
                asset.ControlReserveSchedule.FreeNegativeReservation(FreeNegativeReservationSchedule);
            }
            var ReservePositiveSchedule = toAdd.Where(item => item.Values.Any(el => el.Key.ToString().Contains("m_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.volumeMW).Sum())).ToList();
            var ReserveNegativeSchedule = toAdd.Where(item => item.Values.Any(el => el.Key.ToString().Contains("n_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.volumeMW).Sum())).ToList();
            asset.ControlReserveSchedule.ReservePositiveEnergy(ReservePositiveSchedule);
            asset.ControlReserveSchedule.ReserveNegativeEnergy(ReserveNegativeSchedule);
        }

        /// <summary>
        /// Erzeugt Sekundärregelarbeitsgebot aus Zeitreihe für den angegebenen Kraftwerkspark und das SimEvent
        /// </summary>
        /// <param name="assets">Kraftwerke, die zu dem Gebot beitragen</param>
        /// <param name="calling">Market, auf dem das Gebot abgegeben werden soll.</param>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        public static Bid_ControlReserve aFRR(List<IAsset> assets, Market calling, TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float PriceEnergy)> timeSeries)
            => new Bid_ControlReserve_Energy(assets, calling).aFRR(stepDuration, timeSeries);

        /// <summary>
        /// Erzeugt Minuten-Regelarbeitsgebot aus Zeitreihe für den angegebenen Kraftwerkspark und das SimEvent
        /// </summary>
        /// <param name="assets">Kraftwerke, die zu dem Gebot beitragen</param>
        /// <param name="calling">Market, auf dem das Gebot abgegeben werden soll.</param>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        /// <returns></returns>
        public static Bid_ControlReserve mFRR(List<IAsset> assets, Market calling, TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float PriceEnergy)> timeSeries)
            => new Bid_ControlReserve_Energy(assets, calling).aFRR(stepDuration, timeSeries);
    }
}
