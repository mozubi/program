﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Helper;
using winMoz.Markets.EoMMarkets;
using winMoz.Simulation;

namespace winMoz.Markets.Bids
{
    public class Bid_Intraday : Bid
    {
        public bool IsBlockBid => this.Segments.Any(seg => seg.ShortCode.IsBlock);

        #region Konstruktoren
        private Bid_Intraday() : base() { }  //{ IntradayId = Id; }
        public Bid_Intraday(IAsset asset, Market calling) : base(asset, calling) { }
        public Bid_Intraday(IEnumerable<IAsset> assets, Market calling) : base(assets, calling) { }
        #endregion

        public override TimeSpan Duration => TimeSpan.FromTicks(Math.Min(this.Segments.Sum(segment => segment.Duration.Ticks), TimeSpan.FromHours(1).Ticks));
        #region Products
        public Bid_Intraday QuarterHour(DateTime timeStamp, float powerMW, float price)
        {
            if (powerMW != 0) AddSegment(new Bid_Segment(ShortCode.IntraDay.QuarterHour(timeStamp), timeStamp, powerMW, price));
            return this;
        }
        public Bid_Intraday QuarterHour(List<(DateTime TimeStamp, float PowerMW, float Price)> ts)
        {
            if (ts.Duration().TotalHours > 1) Log.Warning("More than one hour in schedule. Truncated after one hour.");
            ts = ts.TakeWhile(el => el.TimeStamp.Hour == ts.First().TimeStamp.Hour && el.TimeStamp.Minute % 15 == 0).ToList();

            for (int i = 0; i < ts.Count; i++)
                if (Math.Round(ts[i].PowerMW, 1) != 0)
                    AddSegment(new Bid_Segment(ShortCode.IntraDay.QuarterHour(ts[i].TimeStamp), ts[i].TimeStamp, ts[i].PowerMW, ts[i].Price));

            return this;
        }
        #endregion


        #region Static Construcotrs
        public static Bid_Intraday QuarterHour(IAsset asset, Market calling, IEnumerable<(DateTime TimeStamp, float Power, float Price)> schedule)
            => new Bid_Intraday(asset, calling).QuarterHour(schedule.ToList());
        public static Bid_Intraday QuarterHour(IEnumerable<IAsset> assets, Market calling, IEnumerable<(DateTime TimeStamp, float Power, float Price)> schedule)
            => new Bid_Intraday(assets, calling).QuarterHour(schedule.ToList());
        #endregion
    }
}

