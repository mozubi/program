﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using winMoz.Assets;
using winMoz.Helper;
using winMoz.Markets.EoMMarkets;

namespace winMoz.Markets.Bids
{
    public class Bid_DayAhead : Bid, IStandardBlockBid
    {
        public bool IsUnderlying => this.Derivative != null;

        public Bid_Future Derivative { get; private set; }

        #region Konstruktoren
        private Bid_DayAhead() { }// : base() { }
        public Bid_DayAhead(IAsset asset, Market calling) : base(asset, calling) { }
        public Bid_DayAhead(IEnumerable<IAsset> assets, Market calling) : base(assets, calling) { }
        #endregion

        #region Overrides
        public override TimeSpan Duration => TimeSpan.FromTicks(Math.Min(this.Segments.Sum(segment => segment.Duration.Ticks), TimeSpan.FromDays(1).Ticks));

        #endregion

        #region Standard/Block Products
        public Bid PeakOffPeak(DateTime timeStamp, Market calling, float peakPowerMW, float peakPrice, float offPeakPowerMW, float offPeakPrice)
        {
            if (Math.Round(peakPowerMW, 1) != 0) AddSegment(new Bid_Segment(ShortCode.DayAhead.Peak, timeStamp, peakPowerMW, peakPrice));
            if (Math.Round(offPeakPowerMW, 1) != 0) AddSegment(new Bid_Segment(ShortCode.DayAhead.OffPeak, timeStamp, offPeakPowerMW, offPeakPrice));
            return this;
        }

        public Bid Peak(DateTime timeStamp, Market calling, float powerMW, float price)
        {
            if (Math.Round(powerMW, 1) != 0) AddSegment(new Bid_Segment(ShortCode.DayAhead.Peak, timeStamp, powerMW, price));
            return this;
        }

        public Bid OffPeak(DateTime timeStamp, Market calling, float powerMW, float price)
        {
            if (Math.Round(powerMW, 1) != 0) AddSegment(new Bid_Segment(ShortCode.DayAhead.OffPeak, timeStamp, powerMW, price));
            return this;
        }

        public Bid Base(DateTime timeStamp, Market calling, float powerMW, float price)
        {
            if (powerMW != 0) AddSegment(new Bid_Segment(ShortCode.DayAhead.Base, timeStamp, powerMW, price));
            return this;
        }

        public Bid_DayAhead Hour(DateTime timeStamp, float powerMW, float price)
        {
            if (Math.Round(powerMW, 1) != 0 && timeStamp.IsFullHour()) AddSegment(new Bid_Segment(ShortCode.DayAhead.Hour(timeStamp), timeStamp, powerMW, price));
            return this;
        }
        public Bid_DayAhead Hour(List<(DateTime TimeStamp, float PowerMW, float Price)> timeSeries)
        {
            if (timeSeries.Count > 1)
                //TODO: Resample löscht doppelte Gebote für einen Zeitpunkt raus: Ist das gewollt?
                timeSeries = timeSeries.Resample(Resampling.TimeStep.Hour);

            for (int i = 0; i < timeSeries.Count; i++)
                if (Math.Round(timeSeries[i].Item2, 1) != 0)
                    AddSegment(new Bid_Segment(ShortCode.DayAhead.Hour(timeSeries[i].TimeStamp), timeSeries[i].TimeStamp, timeSeries[i].Item2, timeSeries[i].Item3));
            return this;
        }
        #endregion

        #region Static Constructors
        public static List<Bid_DayAhead> FromFuture(Bid_Future future, Market calling)
        {
            var bids = new List<Bid_DayAhead>();
            for (int day = 0; day < future.Duration.TotalDays; day++)
            {
                var bid = new Bid_DayAhead(future.Assets, calling);
                foreach (var Segment in future.Segments)
                {
                    var eqShortCode = ShortCode.GetDayAheadEquivalent(Segment.ShortCode);

                    if (eqShortCode == ShortCode.DayAhead.Peak)
                        if (!future.TimeStamp.AddDays(day).IsWeekDay())
                            continue;

                    bid.AddSegment(new Bid_Segment(eqShortCode, future.TimeStamp.AddDays(day), Segment.VolumeMW));  // unlimitiert
                    //bid.Derivative = future;
                }
                bids.Add(bid);
            }
            return bids;
        }

        public static Bid_DayAhead Hour(IAsset asset, Market calling, List<(DateTime TimeStamp, float PowerMW, float Price)> timeSeries) => Hour(new[] { asset }, calling, timeSeries);
        public static Bid_DayAhead Base(IAsset asset, Market calling, DateTime timeStamp, float basePower, float basePrice) => Base(new[] { asset }, calling, timeStamp, basePower, basePrice);
        public static Bid_DayAhead BasePeak(IAsset asset, Market calling, DateTime timeStamp, float basePower, float basePrice, float peakPower, float peakPrice) => BasePeak(new[] { asset }, calling, timeStamp, basePower, basePrice, peakPower, peakPrice);
        public static Bid_DayAhead PeakOffPeak(IAsset asset, Market calling, DateTime timeStamp, float peakPower, float peakPrice, float offPeakPower, float offPeakPrice)
            => PeakOffPeak(new[] { asset }, calling, timeStamp, peakPower, peakPrice, offPeakPower, offPeakPrice);
        public static Bid_DayAhead BasePeak(IEnumerable<IAsset> assets, Market calling, DateTime timeStamp, float basePower, float basePrice, float peakPower, float peakPrice)
            => new Bid_DayAhead(assets, calling).Base(timeStamp, calling, basePower, basePrice).Cast<Bid_DayAhead>().Peak(timeStamp, calling, peakPower, peakPrice).Cast<Bid_DayAhead>();
        public static Bid_DayAhead PeakOffPeak(IEnumerable<IAsset> assets, Market calling, DateTime timeStamp, float peakPower, float peakPrice, float offPeakPower, float offPeakPrice)
         => new Bid_DayAhead(assets, calling).PeakOffPeak(timeStamp, calling, peakPower, peakPrice, offPeakPower, offPeakPrice).Cast<Bid_DayAhead>();
        public static Bid_DayAhead Base(IEnumerable<IAsset> assets, Market calling, DateTime timeStamp, float basePower, float basePrice) => new Bid_DayAhead(assets, calling).Base(timeStamp, calling, basePower, basePrice).Cast<Bid_DayAhead>();
        public static Bid_DayAhead Hour(IEnumerable<IAsset> assets, Market calling, List<(DateTime TimeStamp, float PowerMW, float Price)> timeSeries) => new Bid_DayAhead(assets, calling).Hour(timeSeries);
        #endregion
    }
}
