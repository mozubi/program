﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Assets;
using winMoz.Assets.Plants;
using winMoz.Markets.ControlReserve;
using winMoz.Simulation;

namespace winMoz.Markets.Bids
{
    /// <summary>
    /// Abgeleitete Klasse der Klasse Bid_ControlReserve für die Regelleistung
    /// </summary>
    public class Bid_ControlReserve_Capacity : Bid_ControlReserve
    {
        /// <summary>
        /// Base-Konstruktor (wird für EntityFramework benötigt)
        /// </summary>
        public Bid_ControlReserve_Capacity() { }
        /// <summary>
        /// Base-Konstruktor
        /// </summary>
        /// <param name="market">Markt</param>
        public Bid_ControlReserve_Capacity(Market market) : base(market) { }
        /// <summary>
        /// Base-Konstruktor
        /// </summary>
        /// <param name="assets">Kraftwerke, die am Gebot beteiligt sind</param>
        /// <param name="market">Markt</param>
        public Bid_ControlReserve_Capacity(IEnumerable<IAsset> assets, Market market) : base(assets, market) { }


        /// <summary>
        /// Erzeugt Sekundärregelleistungsgebot aus Zeitreihe
        /// </summary>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        /// <returns></returns>
        public Bid_ControlReserve_Capacity aFRR(Market calling, TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float Price, float PriceEnergy)> timeSeries)
        {
            //if (timeSeries.Count > 1)
            //    timeSeries = timeSeries.Resample(Resampling.GetTimeStep(stepDuration));

            for (int i = 0; i < timeSeries.Count; i++)
                if (Math.Round(timeSeries[i].Item2, 1) != 0)
                {
                    ShortCode codeEnergy = timeSeries[i].Item2 > 0 ? ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_aFRR, timeSeries[i].TimeStamp, stepDuration) : ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_aFRR, timeSeries[i].TimeStamp, stepDuration);
                    AddSegment(firstEvent: calling, codeEnergy, timeSeries[i].TimeStamp, timeSeries[i].PowerMW, timeSeries[i].PriceEnergy);
                    ShortCode codeCapacity = timeSeries[i].Item2 > 0 ? ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.p_aFRR, timeSeries[i].TimeStamp, stepDuration) : ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.n_aFRR, timeSeries[i].TimeStamp, stepDuration);
                    AddSegment(firstEvent: calling, codeCapacity, timeSeries[i].TimeStamp, timeSeries[i].PowerMW, timeSeries[i].Price);
                }
            return this;
        }

        /// <summary>
        /// Erzeugt Minute-Regelelleistungsgebot aus Zeitreihe
        /// </summary>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        public Bid_ControlReserve_Capacity mFRR(Market calling, TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float Price, float PriceEnergy)> timeSeries)
        {
            List<(DateTime, float, float, float)> timeSeries2 = null;
            for (int i = 0; i < timeSeries.Count; i++)
                if (Math.Round(timeSeries[i].Item2, 1) != 0)
                {
                    ShortCode codeEnergy = timeSeries[i].Item2 > 0 ? ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_mFRR, timeSeries[i].TimeStamp, stepDuration) : ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_mFRR, timeSeries[i].TimeStamp, stepDuration);
                    AddSegment(firstEvent: calling, codeEnergy, timeSeries[i].TimeStamp, timeSeries[i].PowerMW, timeSeries[i].PriceEnergy);
                    ShortCode codeCapacity = timeSeries[i].Item2 > 0 ? ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.p_mFRR, timeSeries[i].TimeStamp, stepDuration) : ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.n_mFRR, timeSeries[i].TimeStamp, stepDuration);
                    AddSegment(firstEvent: calling, codeCapacity, timeSeries[i].TimeStamp, timeSeries[i].PowerMW, timeSeries[i].Price);
                }
            return this;
        }


        protected override void TransferBidSegments(SimEvent calling) => Segments.Where(item => item.IsCleared || (item is Bid_Segment_ControlReserve && ((Bid_Segment_ControlReserve)item).IsTenderOrActivationVolume())).ToList().ForEach(seg => ((Bid_Segment_ControlReserve)seg).TransferCoupledSegmentTo(((ControlReserve_Capacity)calling).EnergyMarket));

        /// <summary>
        /// Prüft, ob alle Gebotssegmente in das mitgelieferte Bestimmungszeitfenster passen.
        /// </summary>
        /// <param name="timeStamp">Bestimmungszeitraumanfang</param>
        /// <param name="duration">Bestimmungszeitraumslänge</param>
        /// <param name="step">Bestimmungszeitraumsabschnittslänge</param>
        /// <returns></returns>
        public override bool IsValid(DateTime timeStamp, TimeSpan duration, TimeSpan step)
        {
            var EndOfDuration = Extensions.GetEndOfDuration(timeStamp, duration);
            return this.Segments.Count() <= 2 * (int)(duration.TotalHours / step.TotalHours) &&
            this.Segments.All(seg => seg.TimeStamp >= timeStamp && seg.TimeStamp < EndOfDuration) && Segments.All(seg => seg.IsEnergy || seg.IsCapacity)
                && Segments.Where(seg => seg.IsCapacity).Count() == Segments.Where(seg => seg.IsEnergy).Count();
        }

        /// <summary>
        /// Überprüft, ob es für den angegebenen Bestimmungszeitraum valide Gebotssegmente gibt
        /// </summary>
        /// <param name="time2check">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="step">Abschnitt des Bestimmungszeitraums</param>
        /// <returns></returns>
        public override bool HasValidSegments(DateTime time2check, TimeSpan duration, TimeSpan step)
        {
            DateTime DurationEnd = time2check.Add(duration);
            return Segments.Count() != 0 ?
                (Segments.Any(seg => seg.TimeStamp.IsStartTimeOfDuration(step) && seg.TimeStamp >= time2check && seg.TimeStamp < DurationEnd && (seg.IsCapacity || seg.IsEnergy))) : false;
        }

        /// <summary>
        /// Gibt valide Gebotssegmente für Bestimmungszeitraum zurück
        /// </summary>
        /// <param name="timeStamp">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="type">Regellleistungstyp</param>
        /// <returns></returns>
        public override List<Bid_Segment_ControlReserve> GetValidSegments(DateTime timeStamp, TimeSpan duration, ControlReserveShortCode.ShortCode type)
            => base.GetValidSegments(timeStamp, duration, type).Where(seg => seg.IsCapacity).ToList();

        /// <summary>
        /// Füge Gebot hinzu
        /// </summary>
        /// <param name="code">Shortcode des Gebotssegments</param>
        /// <param name="timeStamp">Start des Bestimmungszeitraums des Gebots</param>
        /// <param name="volume">Menge des Gebots</param>
        /// <param name="price">Preis des Gebots</param>
        public void AddSegment(SimEvent firstEvent, ShortCode code, DateTime timeStamp, float volume, float price, SimEvent transferEvent = null)
        {
            if (code.Is_Capacity && Segments.Where(seg => seg.IsEnergy).Any(sc => sc.TimeStamp == timeStamp))
            {
                var coupledSeg = Segments.Single(seg => seg.IsEnergy && seg.TimeStamp.Ticks == timeStamp.Ticks && seg.ShortCode.GetEnum() == code.GetEnum());
                AddSegment(code, timeStamp, volume, price, coupledSegment: coupledSeg);
            }
            else if (code.Is_Energy)
                AddSegment(code, timeStamp, volume, price, firstEvent: firstEvent, transferEvent: transferEvent, canBeAuctioned: false);
        }

        /// <summary>
        /// Bestimmt die neue Menge im Fahrplan der Kraftwerke nach der Markträumung für ein Kraftwerk
        /// </summary>
        /// <param name="asset">Kraftwerk</param>
        /// <param name="toAdd">Menge zum Hinzufügen</param>
        /// <param name="assetShareOfVolume">Anteil, der auf das angegebene Kraftwerk fällt</param>
        /// /// <param name="free">Menge zur Freigabe</param>
        protected override void UpdateControlReserveSchedule(IAsset asset, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> toAdd, float assetShareOfVolume, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> free)
        {
            var ReservePositiveSchedule = toAdd.Where(item => item.Values.Any(el => el.Key.ToString().Contains("m_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.volumeMW).Sum(), Extensions.GetWeightedValue(item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.priceCleared).ToList(), item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.volumeMW).ToList()))).ToList();
            var ReserveNegativeSchedule = toAdd.Where(item => item.Values.Any(el => el.Key.ToString().Contains("n_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.volumeMW).Sum(), -Extensions.GetWeightedValue(item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.priceCleared).ToList(), item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.volumeMW).ToList()))).ToList();
            asset.ControlReserveSchedule.ReservePositiveCapacity(ReservePositiveSchedule);
            asset.ControlReserveSchedule.ReserveNegativeCapacity(ReserveNegativeSchedule);
        }

        protected override List<(DateTime TimeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float VolumeMW, float PriceCleared)>)> GetControlReserveTimeSeries(bool ignoreClearing)
        {
            var schedule = new List<(DateTime TimeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float VolumeMW, float PriceCleared)>)>();

            //Alle Nicht-Blöcke
            if (ignoreClearing)
                schedule = this.Segments.Where(item => item.ShortCode.Is_Capacity)
                    //.Where(item => !item.ShortCode.IsBlock).ToList()
                    .SelectMany(item => ((Bid_Segment_ControlReserve)item).GetControlReserveTimeSeries())
                    .GroupBy(item => item.TimeStamp, (time, schType) => (time, schType.GroupBy(item => item.type, (type, sch) => new { type, VolumeMW = sch.Select(item => item.VolumeMW).Sum(), Price = sch.Select(schItem => schItem.VolumeMW).Sum() != 0 ? sch.Select(schItem => schItem.Price * schItem.VolumeMW).Sum() / sch.Select(schItem => schItem.VolumeMW).Sum() : 0 }).ToDictionary(el => el.type, el => (el.VolumeMW, el.Price)))).ToList();
            else
                schedule = this.Segments.Where(item => item.ShortCode.Is_Capacity)
                    //.Where(item => !item.ShortCode.IsBlock).ToList()
                    .SelectMany(item => ((Bid_Segment_ControlReserve)item).GetClearedControlReserveTimeSeries())
                    .GroupBy(item => item.TimeStamp, (time, schType) => (time, schType.GroupBy(item => item.type, (type, sch) => new { type, VolumeMW = sch.Select(item => item.VolumeMW).Sum(), Price = sch.Select(schItem => schItem.VolumeMW).Sum() != 0 ? sch.Select(schItem => schItem.Price * schItem.VolumeMW).Sum() / sch.Select(schItem => schItem.VolumeMW).Sum() : 0 }).ToDictionary(el => el.type, el => (el.VolumeMW, el.Price)))).ToList();

            return schedule;
        }

        /// <summary>
        /// Erzeugt Sekundärregelleistungsgebot aus Zeitreihe für den angegebenen Kraftwerkspark und das SimEvent
        /// </summary>
        /// <param name="assets">Kraftwerke, die zu dem Gebot beitragen</param>
        /// <param name="calling">Markt, auf dem das Gebot abgegeben werden soll.</param>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        /// <returns></returns>
        public static Bid_ControlReserve_Capacity aFRR(List<IAsset> assets, Market calling, TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float Price, float PriceEnergy)> timeSeries)
           => new Bid_ControlReserve_Capacity(assets, calling).aFRR(calling, stepDuration, timeSeries);

        /// <summary>
        /// Erzeugt Minuten-Regelleistungsgebot aus Zeitreihe für den angegebenen Kraftwerkspark und das SimEvent
        /// </summary>
        /// <param name="assets">Kraftwerke, die zu dem Gebot beitragen</param>
        /// <param name="calling">Markt, auf dem das Gebot abgegeben werden soll.</param>
        /// <param name="stepDuration">Dauer eines Gebotsegments</param>
        /// <param name="timeSeries">Zeitreihe aus der das Gebot bestimmt werden soll</param>
        /// <returns></returns>
        public static Bid_ControlReserve_Capacity mFRR(List<IAsset> assets, Market calling, TimeSpan stepDuration, List<(DateTime TimeStamp, float PowerMW, float Price, float PriceEnergy)> timeSeries)
           => new Bid_ControlReserve_Capacity(assets, calling).mFRR(calling, stepDuration, timeSeries);

    }
}
