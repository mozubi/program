﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;

namespace winMoz.Markets.Bids
{
    public class ShortCode
    {
        private string Code = "";
        private static IEnumerable<string> allCodes = Futures.GetCodes().Concat(DayAhead.GetCodes()).Concat(IntraDay.GetCodes());
        private static IEnumerable<string> dayaheadCodes = DayAhead.GetCodes();
        private static IEnumerable<string> intradayCodes = IntraDay.GetCodes();
        private static IEnumerable<string> futuresCodes = Futures.GetCodes();

        public bool IsHour => Code.StartsWith("H") && !Code.Contains("Q");
        public bool IsFutures => futuresCodes.Any(cd => cd.Contains(Code)) || Code.StartsWith("HF");
        public bool IsDayAhead => dayaheadCodes.Any(cd => cd.Contains(Code)) || Code.StartsWith("Hd");
        public bool IsIntraDay => intradayCodes.Any(cd => cd.Contains(Code));
        public bool IsBlock => allCodes.Where(cd => cd.Contains("N") || cd.Contains("D") || cd.Contains("B")).Any(cd => cd.Contains(Code));
        public bool IsDayAheadBlock() => allCodes.Where(cd => cd.Contains("D")).Any(cd => cd.Contains(Code));
        public bool Is_aFRR => Code.Contains("aFRR");
        public bool Is_mFRR => Code.Contains("mFRR");
        public bool Is_positive_FRR => Code.Contains("p_");
        public bool Is_negative_FRR => Code.Contains("n_");
        public bool Is_positive_a_FRR => Code.Contains("p_aFRR");
        public bool Is_negative_a_FRR => Code.Contains("n_aFRR");
        public bool Is_positive_m_FRR => Code.Contains("p_mFRR");
        public bool Is_negative_m_FRR => Code.Contains("n_mFRR");
        /// <summary>
        /// Wenn Leistungspreis
        /// </summary>
        public bool Is_Capacity => Code.Contains("_C");
        /// <summary>
        /// Wenn Arbeitspreis
        /// </summary>
        public bool Is_Energy => Code.Contains("_E");
        /// <summary>
        /// Wenn Aktivierung
        /// </summary>
        public bool Is_Activation => Code.Contains("_A");
        public bool Is_reBAP => Code.Contains("reBAP");
        public bool IsControlReserve => Code.Contains("FRR");
        public ShortCode(string code)
        {
            Code = code;
        }

        public ShortCode() { }

        public int CompareTo(ShortCode other)
            => Code.CompareTo(other.Code);

        #region Overrides
        public override bool Equals(object obj)
        {
            if (!(obj is ShortCode)) return false;

            return String.CompareOrdinal(this.ToString(), ((ShortCode)obj).ToString()) == 0;
        }

        /// <summary>
        /// Klasse zur Überprüfung der Schlüssel von Wörterbüchern mit ShortCode als Schlüssel.
        /// </summary>
        public class ShortCodeEqualityComparer : IEqualityComparer<ShortCode>
        {
            public bool Equals(ShortCode b1, ShortCode b2) => b1 == b2;

            public int GetHashCode(ShortCode bx) =>  bx.Code.GetHashCode();
        }
        public static bool operator ==(ShortCode scA, ShortCode scB) => scA.Equals(scB);
        public static bool operator !=(ShortCode scA, ShortCode scB) => !scA.Equals(scB);
        public override string ToString() => Code;
        #endregion

        #region Shortcodes
        public class Futures
        {
            public static ShortCode BaseMonth => new ShortCode("N2BM");
            public static ShortCode PeakMonth => new ShortCode("N2PM");
            public static ShortCode OffPeakMonth => new ShortCode("N2OPM");
            public static ShortCode Hour(int hour) => new ShortCode($"FH{hour}");
            public static ShortCode Hour(DateTime timeStamp) => new ShortCode($"HF{timeStamp.Hour}");
            internal static List<string> GetCodes() => ShortCode.GetCodes<Futures>();
            public static List<string> GetBlockCodes() => ShortCode.GetBlockCodes<Futures>();
        }

        public class DayAhead
        {
            public static ShortCode Base => new ShortCode("DB");
            public static ShortCode Peak => new ShortCode("DP");
            public static ShortCode OffPeak => new ShortCode("DOP");
            public static ShortCode OffPeak1 => new ShortCode("DOP1");
            public static ShortCode OffPeak2 => new ShortCode("DOP2");
            public static ShortCode Hour(int hour) => new ShortCode($"Hd{hour}");
            public static ShortCode Hour(DateTime timeStamp) => new ShortCode($"Hd{timeStamp.Hour}");

            internal static List<string> GetCodes() => ShortCode.GetCodes<DayAhead>().Concat(Enumerable.Range(0,24).Select(el => DayAhead.Hour(el).ToString())).ToList();
            public static List<string> GetBlockCodes() => ShortCode.GetBlockCodes<DayAhead>();
        }

        public class IntraDay
        {
            private static ShortCode QuarterHour(int h, int qh) => new ShortCode($"H{h}Q{qh}");
            public static ShortCode QuarterHour(DateTime timeStamp) => new ShortCode($"H{timeStamp.Hour}Q{timeStamp.Minute / 15}");
            public static ShortCode Hour(int h) => new ShortCode($"H{h}");
            internal static List<string> GetCodes() 
                => ShortCode.GetCodes<IntraDay>().Concat(Enumerable.Range(0, 24).Select(el => IntraDay.Hour(el).ToString()))
                .Concat(Enumerable.Range(0, 24).Select(h => Enumerable.Range(0,4).Select(qh => IntraDay.QuarterHour(h,qh).ToString())).SelectMany(el => el))
                .ToList();
            public static List<string> GetBlockCodes() => ShortCode.GetBlockCodes<IntraDay>();
        }

        public class ControlReserve
        {
            public static ShortCode CapacityFromEnum(ControlReserveShortCode.ShortCode type, DateTime time, TimeSpan duration)
                => new ShortCode(type.ToString() + $"_{GetTimeSpanChar(duration)}{time.GetNumberOfTimeSpanDay(duration)}_C");
            public static ShortCode EnergyFromEnum(ControlReserveShortCode.ShortCode type, DateTime time, TimeSpan duration)
                => new ShortCode(type.ToString() + $"_{GetTimeSpanChar(duration)}{time.GetNumberOfTimeSpanDay(duration)}_E");
            public static ShortCode ActivationFromEnum(ControlReserveShortCode.ShortCode type, DateTime time, TimeSpan duration)
                => new ShortCode(type.ToString() + $"_{GetTimeSpanChar(duration)}{time.GetNumberOfTimeSpanDay(duration)}_A");
            public static ShortCode reBAP(DateTime time, TimeSpan duration) => new ShortCode($"reBAP_V{time.GetNumberOfTimeSpanDay(duration)}");
            
            internal static IEnumerable<string> GetCodes() => typeof(ControlReserve).GetProperties().Select(el => el.ToString());
            public static List<string> GetBlockCodes() => ShortCode.GetBlockCodes<ControlReserve>();
            private static char GetTimeSpanChar(TimeSpan duration)
            {
                if (duration == TimeSpan.FromHours(0.25) || duration == default) return 'Q';
                if (duration == TimeSpan.FromHours(1)) return 'H';
                if (duration == TimeSpan.FromHours(4)) return 'S';
                if (duration == TimeSpan.FromHours(24)) return 'D';
                if (duration == TimeSpan.FromDays(7)) return 'W';

                throw new NotImplementedException();
            }
        }
        #endregion

        public static ShortCode FromString(string shortCode)
        {
            // TODO: Implement validation of ShortCode
            return new ShortCode(shortCode);
        }

        public static ShortCode GetDayAheadEquivalent(ShortCode shortCode)
        {
            if (shortCode == Futures.BaseMonth) return DayAhead.Base;

            if (shortCode == Futures.PeakMonth) return DayAhead.Peak;

            if (shortCode == Futures.OffPeakMonth) return DayAhead.OffPeak;

            return shortCode;
        }


        /// <summary>
        /// Umwandlung in Aktivierung
        /// </summary>
        /// <returns>Umgewandelten ShortCode</returns>
        public ShortCode ToActivation(DateTime time, TimeSpan duration)
            => ShortCode.ControlReserve.ActivationFromEnum(GetEnum(), time, duration);

        /// <summary>
        /// Umwandlung in reinen Arbeitspreis
        /// </summary>
        /// <returns>Umgewandelten ShortCode</returns>
        public ShortCode ToEnergy(DateTime time, TimeSpan duration)
            => this.IsControlReserve ? ShortCode.ControlReserve.EnergyFromEnum(GetEnum(), time, duration) : this;


        /// <summary>
        /// Gibt zugehöriges enum zurück für Regelenergie
        /// </summary>
        public ControlReserveShortCode.ShortCode GetEnum()
        {
            if (this.Is_positive_FRR)
            {
                if (this.Is_aFRR) return ControlReserveShortCode.ShortCode.p_aFRR;
                else return ControlReserveShortCode.ShortCode.p_mFRR;
            }
            else if (this.Is_negative_FRR)
            {
                if (this.Is_aFRR) return ControlReserveShortCode.ShortCode.n_aFRR;
                else return ControlReserveShortCode.ShortCode.n_mFRR;
            }
            throw new NotImplementedException();
        }

        private static List<string> GetCodes<TCode>()
        {
            object code = "";
            return typeof(TCode).GetProperties().Select(el => el.GetValue(code).ToString()).ToList();
        }

        private static List<string> GetBlockCodes<TCode>()
        {
            object code = "";
            return typeof(TCode).GetProperties().Select(el => el.GetValue(code).ToString()).Where(item => ShortCode.FromString(item).IsBlock).ToList();
        }
        
        public int GetLevel()
        {
            if (this == DayAhead.Base) return 1;
            else if (this == DayAhead.OffPeak1 || this == DayAhead.OffPeak2 || this == DayAhead.Peak) return 2;
            else return 0;
        }

        public int[] GetBlockHours()
        {
            if (this == DayAhead.Base) return Enumerable.Range(0, 24).ToArray();
            else if (this == DayAhead.Peak) return Enumerable.Range(8, 12).ToArray();
            else if (this == DayAhead.OffPeak1) return Enumerable.Range(0, 8).ToArray();
            else if (this == DayAhead.OffPeak2) return Enumerable.Range(20, 4).ToArray();
            else return new int[] { };
        }

        public bool IsFRRType(ControlReserveShortCode.ShortCode type)
        {
            switch (type)
            {
                case ControlReserveShortCode.ShortCode.p_aFRR: return Is_positive_a_FRR;
                case ControlReserveShortCode.ShortCode.n_aFRR: return Is_negative_a_FRR;
                case ControlReserveShortCode.ShortCode.p_mFRR: return Is_positive_m_FRR;
                case ControlReserveShortCode.ShortCode.n_mFRR: return Is_negative_m_FRR;
                default: return false;
            }
        }
    }
}
