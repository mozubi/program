﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using winMoz.Helper;
using winMoz.Markets.Elements;

namespace winMoz.Markets.Bids
{
    //public abstract partial class Bid
    //{
    public class Bid_Segment
    {
        /// <summary>
        /// ID des Gebotssegments (wird automatisch inkrementiert)
        /// </summary>
        public int BidId { get; internal set; }
        public int SegmentId { get; private set; }
        [NotMapped]
        private static int _id = 1;
        public ShortCode ShortCode { get; private set; }
        public DateTime TimeStamp { get; private set; }
        public TimeSpan Duration { get; private set; }
        public float VolumeMW { get; private set; }
        public float Price { get; private set; }
        //Damit nicht vorher und nachher vom Preis gleich sind 
        //und man die Veränderungen durch den Handel sehen kann.
        public float ClearedVolumeMW { get; protected set; }
        public float ClearedPrice { get; protected set; }
        public bool IsBuy { get; private set; }
        public bool IsSell => !IsBuy;
        public bool IsCleared { get; protected set; } = false;
        public bool IsUnlimited { get; private set; }

        public bool IsHour => ShortCode.IsHour;
        public bool IsCapacity => ShortCode.Is_Capacity;
        public bool IsEnergy => ShortCode.Is_Energy;
        public bool IsActivation => ShortCode.Is_Activation;


        #region Konstruktoren
        protected Bid_Segment() { }//{ this.SegmentId = System.Threading.Interlocked.Increment(ref _id); }

        public void SetBidId(int bidId) => BidId = bidId;

        public Bid_Segment(ShortCode shortCode, DateTime timeStamp, float volumeMW, float price) : this()
        {
            this.ShortCode = shortCode;
            this.TimeStamp = timeStamp;
            this.VolumeMW = (float)Math.Round(volumeMW, 1);
            this.Price = (float)Math.Round(price, 2);
            this.IsBuy = volumeMW < 0; // "True" = Buy ; "False" = Sell
            this.Duration = GetDuration(shortCode);
            this.ClearedVolumeMW = 0;
            this.ClearedPrice = 0;
        }

        internal Bid_Segment(ShortCode shortCode, DateTime timeStamp, float volumeMW) : this()
        {
            this.IsUnlimited = true;
            this.ShortCode = shortCode;
            this.TimeStamp = timeStamp;
            this.VolumeMW = (float)Math.Round(volumeMW, 1);
            this.IsBuy = volumeMW < 0; // "True" = Buy ; "False" = Sell
            this.Duration = GetDuration(shortCode);
            this.ClearedVolumeMW = 0;
            this.ClearedPrice = 0;
        }

        private TimeSpan GetDuration(ShortCode shortCode)
        {
            if (shortCode.IsDayAhead)
                if (shortCode.IsHour) return TimeSpan.FromHours(1);
                else return TimeSpan.FromHours(24);

            if (shortCode.IsIntraDay)
                if (shortCode.IsBlock) return TimeSpan.FromHours(1);
                else return TimeSpan.FromHours(0.25);

            if (shortCode.IsControlReserve)
            {
                if (shortCode.Is_Capacity) return TimeSpan.FromHours(4);
                else if (shortCode.Is_Energy) return TimeSpan.FromHours(4);
                else return TimeSpan.FromMinutes(15);
            }

            if (shortCode.IsFutures && shortCode.IsHour)
                return TimeSpan.FromHours(1);

            return this.TimeStamp.GetDurationOfMonth();
        }
        #endregion

        public void ChangeVolumeMWTo(float volumeMW) => VolumeMW = volumeMW;
        public void SetUnlimited() => this.IsUnlimited = true;

        public void SetUnlimited(Market market)
        {
            if (this.IsSell) this.Price = market.Attributes.Lower_Price_Bound;
            else this.Price = market.Attributes.Upper_Price_Bound;
            this.IsUnlimited = true;
        }

        public void SetUnlimitedIfTooHigh(Market market)
        {
            if (this.Price > market.Attributes.Upper_Price_Bound)
                this.Price = market.Attributes.Upper_Price_Bound;

            else if (this.Price < market.Attributes.Lower_Price_Bound)
                this.Price = market.Attributes.Lower_Price_Bound;


        }

        #region Clearing
        internal virtual void Clear(Market market)
        {
            var result = market.Results.Get(this.ShortCode, this.TimeStamp);
            Results.MarketResult result2 = null;
            if (this.ShortCode.Equals(ShortCode.DayAhead.OffPeak))
            {
                result = market.Results.Get(ShortCode.DayAhead.OffPeak1, this.TimeStamp);
                result2 = market.Results.Get(ShortCode.DayAhead.OffPeak2, this.TimeStamp);
                if (result != default && (result.IsBlock || result.Volume != 0))
                {
                    if (this.IsBuy)
                    {
                        if (this.Price.CompareTo(result.Price) >= 0 && this.Price.CompareTo(result2.Price) >= 0 && result.ProRataVolumePurchase == 1 && result2.ProRataVolumePurchase == 1)
                        {
                            this.IsCleared = true;
                            this.ClearedVolumeMW = this.VolumeMW;
                            this.ClearedPrice = (result.Price * ShortCode.DayAhead.OffPeak1.GetBlockHours().Count() +
                                result2.Price * ShortCode.DayAhead.OffPeak2.GetBlockHours().Count()) / 12;
                        }
                    }
                    else
                    {
                        if (this.Price.CompareTo(result.Price) <= 0 && this.Price.CompareTo(result2.Price) <= 0 && result.ProRataVolumeSell == 1 && result2.ProRataVolumeSell == 1)
                        {
                            this.IsCleared = true;
                            this.ClearedVolumeMW = this.VolumeMW;
                            this.ClearedPrice = (result.Price * ShortCode.DayAhead.OffPeak1.GetBlockHours().Count() +
                                result2.Price * ShortCode.DayAhead.OffPeak2.GetBlockHours().Count()) / 12;
                        }
                    }
                }
            }
            else if (ShortCode.IsHour && ShortCode.IsFutures) //PayAsBid
            {
                if (result != default)
                {
                    if (this.IsBuy)
                    {
                        if (this.Price.CompareTo(result.Price) >= 0 || this.Price >= result.Price)
                        {
                            this.IsCleared = true;
                            if (this.Price.CompareTo(result.Price) == 0 || this.Price == result.Price)
                                this.ClearedVolumeMW = this.VolumeMW * result.ProRataVolumePurchase;
                            else
                                this.ClearedVolumeMW = this.VolumeMW;
                            this.ClearedPrice = this.Price;
                        }
                    }
                    else
                    {
                        if (this.Price.CompareTo(result.Price) <= 0 || this.Price <= result.Price)
                        {
                            this.IsCleared = true;
                            if (this.Price.CompareTo(result.Price) == 0 || this.Price == result.Price)
                                this.ClearedVolumeMW = this.VolumeMW * result.ProRataVolumeSell;
                            else
                                this.ClearedVolumeMW = this.VolumeMW;
                            this.ClearedPrice = this.Price;
                        }
                    }
                }
            }
            else //MarginalPrice
            {
                if (result != default && (result.IsBlock || result.Volume != 0))
                {
                    if (this.IsBuy)
                    {
                        if (this.Price.CompareTo(result.Price) >= 0 || this.Price >= result.Price)
                        {
                            this.IsCleared = true;
                            if (this.Price.CompareTo(result.Price) == 0 || this.Price == result.Price)
                                this.ClearedVolumeMW = this.VolumeMW * result.ProRataVolumePurchase;
                            else
                                this.ClearedVolumeMW = this.VolumeMW;
                            this.ClearedPrice = result.Price;
                        }
                    }
                    else
                    {
                        if (this.Price.CompareTo(result.Price) <= 0 || this.Price <= result.Price)
                        {
                            this.IsCleared = true;
                            if (this.Price.CompareTo(result.Price) == 0 || this.Price == result.Price)
                                this.ClearedVolumeMW = this.VolumeMW * result.ProRataVolumeSell;
                            else
                                this.ClearedVolumeMW = this.VolumeMW;
                            this.ClearedPrice = result.Price;
                        }
                    }
                }
            }
        }

        public List<(DateTime TimeStamp, float VolumeMW, float Price)> GetTimeSeries() => GetTimes().Select(el => (el, this.VolumeMW, this.Price)).ToList();

        public List<(DateTime TimeStamp, float VolumeMW, float Price)> GetClearedTimeSeries() => GetTimes().Select(el => (el, this.ClearedVolumeMW, this.ClearedPrice)).ToList();

        internal List<DateTime> GetTimes()
        {
            if (this.ShortCode == ShortCode.DayAhead.OffPeak || this.ShortCode == ShortCode.Futures.OffPeakMonth) return this.Duration.GetOffPeakTimes(this.TimeStamp);
            else if (this.ShortCode == ShortCode.DayAhead.OffPeak1) return this.Duration.GetOffPeakTimes(this.TimeStamp).Where(item => item.Hour < 9).ToList();
            else if (this.ShortCode == ShortCode.DayAhead.OffPeak2) return this.Duration.GetOffPeakTimes(this.TimeStamp).Where(item => item.Hour > 19).ToList();
            else if (this.ShortCode == ShortCode.DayAhead.Peak || this.ShortCode == ShortCode.Futures.PeakMonth) return this.Duration.GetPeakTimes(this.TimeStamp);
            else return this.Duration.GetTimeVectorWithoutPredicate(this.TimeStamp);
        }
        #endregion
    }
}
//}