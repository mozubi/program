﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Assets;
using winMoz.Assets.Plants;
using winMoz.Markets.Elements;
using System.Diagnostics;
using winMoz.Simulation;
using winMoz.Markets.ControlReserve;

namespace winMoz.Markets.Bids
{
    /// <summary>
    /// Abgeleitete Gebotsklasse für Regelleistung
    /// </summary>
    public class Bid_ControlReserve : Bid
    {
        /// <summary>
        /// Base-Konstruktor (wird für EntityFramework benötigt)
        /// </summary>
        protected Bid_ControlReserve() { }// : base() { }

        /// <summary>
        /// Base-Konstruktor
        /// </summary>
        /// <param name="calling">ID des Events auf dem dieses Gebot abgegeben wird</param>
        public Bid_ControlReserve(SimEvent calling) : base(calling) { }
        /// <summary>
        /// Base-Konstruktor
        /// </summary>
        /// <param name="assets"></param>
        public Bid_ControlReserve(IEnumerable<IAsset> assets, SimEvent calling) : base(assets, calling) { }

        //protected override abstract bool IsValid();
        /// <summary>
        /// Überprüft, ob es für den angegebenen Bestimmungszeitraum valide Gebotssegmente gibt
        /// </summary>
        /// <param name="time2check">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="step">Abschnitt des Bestimmungszeitraums</param>
        /// <returns></returns>
        public virtual bool HasValidSegments(DateTime time2check, TimeSpan duration, TimeSpan step) => throw new NotImplementedException();

        /// <summary>
        /// Gibt valide Gebotssegmente für Bestimmungszeitraum zurück
        /// </summary>
        /// <param name="timeStamp">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="type">Regellleistungstyp</param>
        /// <returns></returns>
        public virtual List<Bid_Segment_ControlReserve> GetValidSegments(DateTime timeStamp, TimeSpan duration, ControlReserveShortCode.ShortCode type)
        {
            DateTime EndDuration = timeStamp.Add(duration);
            var segments = Segments.Where(seg => seg.TimeStamp >= timeStamp
                             && seg.TimeStamp.Add(seg.Duration) <= EndDuration
                             && (((Bid_Segment_ControlReserve)seg).CanBeAuctioned
                             || ((Bid_Segment_ControlReserve)seg).IsTenderOrActivationVolume())
                             && seg.ShortCode.IsFRRType(type))
                             .Select(seg => (Bid_Segment_ControlReserve)seg)
                             .ToList();
            return segments;
        }

        /// <summary>
        /// Füge Gebot hinzu
        /// </summary>
        /// <param name="code">Shortcode des Gebotssegments</param>
        /// <param name="timeStamp">Start des Bestimmungszeitraums des Gebots</param>
        /// <param name="volume">Menge des Gebots</param>
        /// <param name="price">Preis des Gebots</param>
        /// <param name="isCapacityMarketBid">Wurde oder wird das Gebot auf dem Regelleistungsmarkt angeboten</param>
        /// <param name="canBeAuctioned">Ist das Gebot ausführbar?</param>
        /// <param name="coupledSegment">Gekoppeltes Gebotssegement bei Regelleistungsmarktgeboten</param>
        protected void AddSegment(ShortCode code, DateTime timeStamp, float volume, float price, SimEvent firstEvent = null, SimEvent transferEvent = null, bool canBeAuctioned = true, Bid_Segment coupledSegment = null)
        => this.Segments.Add(new Bid_Segment_ControlReserve(code, timeStamp, volume, price, firstEvent, transferEvent, canBeAuctioned, coupledSegment: coupledSegment));



        /// <summary>
        /// Clearing des Gebotes
        /// </summary>
        /// <param name="calling">Aufrufender Markt</param>
        internal override protected void ClearBid(Market calling)
        {
            base.ClearBid(calling);
            TransferBidSegments(calling);
        }

        protected virtual void TransferBidSegments(SimEvent calling) { }

        /// <summary>
        /// Gibt Gebotssegmente zurück, die den betreffenden Zeitraum überschneiden.
        /// </summary>
        /// <param name="segments">Gebotssegmente, die zu prüfen sind</param>
        /// <param name="time2check">Beginn des zu prüfenden Zeitraums</param>
        /// <param name="duration">Dauer des zu prüfenden Zeitraums</param>
        /// <returns></returns>
        protected List<Bid_Segment> GetOverlapedSegments(List<Bid_Segment> segments, DateTime time2check, TimeSpan duration)
        {
            if (segments.First().Duration >= duration)
                return segments.Where(seg => ((Bid_Segment_ControlReserve)seg).DurationIsInSegment(time2check, duration)).ToList();
            else
                return segments.Where(seg => ((Bid_Segment_ControlReserve)seg).IsInDuration(time2check, duration)).ToList();
        }

        /// <summary>
        /// Überprüft, ob es Gebotssegmente gibt, die den betreffenden Zeitraum überschneiden.
        /// </summary>
        /// <param name="segments">Gebotssegmente, die zu prüfen sind</param>
        /// <param name="time2check">Beginn des zu prüfenden Zeitraums</param>
        /// <param name="duration">Dauer des zu prüfenden Zeitraums</param>
        /// <returns></returns>
        protected bool HasOverlappedSegments(List<Bid_Segment> segments, DateTime time2check, TimeSpan duration)
        {
            if (segments.First().Duration >= duration)
                return segments.Any(seg => ((Bid_Segment_ControlReserve)seg).DurationIsInSegment(time2check, duration));
            else
                return segments.Any(seg => ((Bid_Segment_ControlReserve)seg).IsInDuration(time2check, duration));
        }

        /// <summary>
        /// Überprüft, ob es Gebotssegmente gibt, die zum angegebene Markt transferiert werden sollen.
        /// </summary>
        /// <param name="time2check">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="transferEvent">Transferevent</param>
        /// <returns></returns>
        public virtual bool HasSegmentsToTransferToSimEvent(DateTime time2check, TimeSpan duration, SimEvent transferEvent) => Segments.Any(seg => ((Bid_Segment_ControlReserve)seg).IsTransferEvent(transferEvent)) && HasOverlappedSegments(Segments.Where(seg => ((Bid_Segment_ControlReserve)seg).IsTransferEvent(transferEvent)).ToList(), time2check, duration);

        /// <summary>
        /// Gibt Gebotssegmente zurück, die zum angegebene Markt transferiert werden sollen.
        /// </summary>
        /// <param name="time2check">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="transferEvent">Transfermarkt</param>
        /// <returns></returns>
        public virtual List<Bid_Segment> GetSegmentsToTransferToSimEvent(DateTime time2check, TimeSpan duration, SimEvent transferEvent)
        {
            DateTime EndDuration = time2check.Add(duration);
            return GetOverlapedSegments(Segments.Where(seg => ((Bid_Segment_ControlReserve)seg).IsTransferEvent(transferEvent)).ToList(), time2check, duration);
        }

        protected virtual List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> GetFreeReserveSchedule() => null;

        public override void UpdateSchedule()
        {
            if (this.IsCleared && !this.IsScheduled && this.Assets != null)
            {
                List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> before = GetFreeReserveSchedule();

                var update = this.GetClearedControlReserveTimeSeries();

                var t1 = update.First().TimeStamp;
                var duration = update.Duration();

                var totalAssetVolume = this.Assets.SelectMany(el => el.Schedule.Get(t1, duration)).Sum(el => Math.Abs(el.PowerDiff));

                // TODO: If assets -not only- contain thermals... factor in mixed plants!
                //if (this.Assets.All(asset => asset.IsThermal))
                //totalPower = this.Assets.Select(plant => ((Plant_Thermal)plant).Units.Select(unit => unit.P_th_Kwk).Sum()).Sum();

                foreach (var asset in this.Assets)
                {
                    if (update.SelectMany(item => item.Item2).Any(item => float.IsNaN(item.Value.PriceCleared))) Debugger.Break();
                    float assetShareOfVolume = totalAssetVolume == 0 ? 1 : asset.Schedule.Get(t1, duration).Sum(el => Math.Abs(el.PowerDiff)) / totalAssetVolume;
                    //if (asset.IsThermal)
                    //plantPower = ((Plant_Thermal)asset).Units.Select(unit => unit.P_th_Kwk).Sum();
                    UpdateControlReserveSchedule(asset, update, assetShareOfVolume, before);
                }
                this.IsScheduled = true;
            }
        }

        protected virtual void UpdateControlReserveSchedule(IAsset asset, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> toAdd, float assetShareOfVolume, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> free = null) => throw new NotImplementedException();

        protected virtual List<(DateTime TimeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float VolumeMW, float PriceCleared)>)> GetControlReserveTimeSeries(bool ignoreClearing)
        {
            var schedule = new List<(DateTime TimeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float VolumeMW, float PriceCleared)>)>();

            //Alle Nicht-Blöcke
            if (ignoreClearing)
                schedule = this.Segments
                    //.Where(item => !item.ShortCode.IsBlock).ToList()
                    .SelectMany(item => ((Bid_Segment_ControlReserve)item).GetControlReserveTimeSeries())
                    .GroupBy(item => item.TimeStamp, (time, schType) => (time, schType.GroupBy(item => item.type, (type, sch) => new { type, VolumeMW = sch.Select(item => item.VolumeMW).Sum(), Price = sch.Select(schItem => schItem.VolumeMW).Sum() != 0 ? sch.Select(schItem => schItem.Price * schItem.VolumeMW).Sum() / sch.Select(schItem => schItem.VolumeMW).Sum() : 0 }).ToDictionary(el => el.type, el => (el.VolumeMW, el.Price)))).ToList();
            else
                schedule = this.Segments
                    //.Where(item => !item.ShortCode.IsBlock).ToList()
                    .SelectMany(item => ((Bid_Segment_ControlReserve)item).GetClearedControlReserveTimeSeries())
                    .GroupBy(item => item.TimeStamp, (time, schType) => (time, schType.GroupBy(item => item.type, (type, sch) => new { type, VolumeMW = sch.Select(item => item.VolumeMW).Sum(), Price = sch.Select(schItem => schItem.VolumeMW).Sum() != 0 ? sch.Select(schItem => schItem.Price * schItem.VolumeMW).Sum() / sch.Select(schItem => schItem.VolumeMW).Sum() : 0 }).ToDictionary(el => el.type, el => (el.VolumeMW, el.Price)))).ToList();

            return schedule;
        }

        public List<(DateTime TimeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float VolumeMW, float PriceCleared)> VolumesAndPrices)> GetControlReserveTimeSeries() => GetControlReserveTimeSeries(true);
        public List<(DateTime TimeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float VolumeMW, float PriceCleared)> VolumesAndPrices)> GetClearedControlReserveTimeSeries() => GetControlReserveTimeSeries(false);


        //TODO: Überprüfen, ob noch gebraucht.
        /// <summary>
        /// Methode zum Übergeben des gebotspezifischen Kraftwerksfahrplans
        /// </summary>
        /// <param name="ignoreClearing">Clearing wird ignoriert</param>
        private List<(DateTime TimeStamp, float VolumeMW, float PriceCleared)> _getTimeSeries(bool ignoreClearing = false)
        {
            var schedule = new List<(DateTime TimeStamp, float Power, float PriceCleared)>();
            var assetCount = Assets.Count();

            this.Segments.ToList().ForEach(seg => { if (seg.IsCleared || ignoreClearing) schedule.Add((seg.TimeStamp, -seg.VolumeMW / assetCount, seg.Price)); });

            return schedule;
        }
    }
}
