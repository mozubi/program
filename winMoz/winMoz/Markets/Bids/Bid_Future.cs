﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Markets.EoMMarkets;

namespace winMoz.Markets.Bids
{
    /// <summary>
    /// Blockgebots-Klasse zum Handel an Termin- und Spotmarkt - Erbt von der abstrakten Gebotsklasse
    /// </summary>
    public class Bid_Future : Bid, IStandardBlockBid
    {
        public delegate void EventHandler(Bid_DayAhead bid);
        public event EventHandler<Bid_DayAhead> UnderlyingCleared;
        public List<Bid_DayAhead> Underlyings { get; private set; } = new List<Bid_DayAhead>();

        #region Constructors
        private Bid_Future() { }// : base() { }
        public Bid_Future(IEnumerable<IAsset> assets, Market calling) : base(assets, calling) { }
        #endregion

        #region Products
        public Bid PeakOffPeak(DateTime timeStamp, Market calling, float peakPowerMW, float peakPrice, float offPeakPowerMW, float offPeakPrice)
        {
            if (peakPowerMW != 0) AddSegment(new Bid_Segment(ShortCode.Futures.PeakMonth, timeStamp.ToStartOfMonth(), peakPowerMW, peakPrice));
            if (offPeakPowerMW != 0) AddSegment(new Bid_Segment(ShortCode.Futures.OffPeakMonth, timeStamp.ToStartOfMonth(), offPeakPowerMW, offPeakPrice));
            CreateUnderlyings(calling);
            return this;
        }

        public Bid Peak(DateTime timeStamp, Market calling, float powerMW, float price)
        {
            AddSegment(new Bid_Segment(ShortCode.Futures.PeakMonth, timeStamp.ToStartOfMonth(), powerMW, price));
            CreateUnderlyings(calling);
            return this;
        }

        public Bid OffPeak(DateTime timeStamp, Market calling, float powerMW, float price)
        {
            AddSegment(new Bid_Segment(ShortCode.Futures.OffPeakMonth, timeStamp.ToStartOfMonth(), powerMW, price));
            CreateUnderlyings(calling);
            return this;
        }

        public Bid Base(DateTime timeStamp, Market calling, float powerMW, float price)
        {
            AddSegment(new Bid_Segment(ShortCode.Futures.BaseMonth, timeStamp.ToStartOfMonth(), powerMW, price));
            CreateUnderlyings(calling);
            return this;
        }

        public Bid_Future Hour(DateTime timeStamp, float powerMW, float price)
        {
            if (powerMW != 0 && timeStamp.IsFullHour()) AddSegment(new Bid_Segment(ShortCode.Futures.Hour(timeStamp), timeStamp, powerMW, price));
            return this;
        }
        public Bid_Future Hour(List<(DateTime TimeStamp, float PowerMW, float Price)> timeSeries)
        {
            if (timeSeries.Count > 1)
                //TODO: Resample löscht doppelte Gebote für einen Zeitpunkt raus: Ist das gewollt?
                timeSeries = timeSeries.Resample(Resampling.TimeStep.Hour);

            for (int i = 0; i < timeSeries.Count; i++)
                if (Math.Round(timeSeries[i].Item2, 1) != 0)
                    AddSegment(new Bid_Segment(ShortCode.Futures.Hour(timeSeries[i].TimeStamp), timeSeries[i].TimeStamp, timeSeries[i].Item2, timeSeries[i].Item3));
            return this;
        }
        #endregion

        #region Static Constructors
        public static Bid_Future Peak(IAsset asset, Market calling, DateTime timeStamp, float powerMW, float price) => Peak(new[] { asset }, calling, timeStamp, powerMW, price);
        public static Bid_Future OffPeak(IAsset asset, Market calling, DateTime timeStamp, float powerMW, float price) => OffPeak(new[] { asset }, calling, timeStamp, powerMW, price);
        public static Bid_Future PeakOffPeak(IAsset asset, Market calling, DateTime timeStamp, float peakPowerMW, float peakPrice, float offPeakPowerMW, float offPeakPrice)
            => new Bid_Future(new[] { asset }, calling).PeakOffPeak(timeStamp, calling, peakPowerMW, peakPrice, offPeakPowerMW, offPeakPrice).Cast<Bid_Future>();
        public static Bid_Future Base(IEnumerable<IAsset> assets, Market calling, DateTime timeStamp, float powerMW, float price) => new Bid_Future(assets, calling).Base(timeStamp, calling, powerMW, price).Cast<Bid_Future>();
        public static Bid_Future Peak(IEnumerable<IAsset> assets, Market calling, DateTime timeStamp, float powerMW, float price) => new Bid_Future(assets, calling).Peak(timeStamp, calling, powerMW, price).Cast<Bid_Future>();
        public static Bid_Future OffPeak(IEnumerable<IAsset> assets, Market calling, DateTime timeStamp, float powerMW, float price) => new Bid_Future(assets, calling).OffPeak(timeStamp, calling, powerMW, price).Cast<Bid_Future>();
        public static Bid_Future PeakOffPeak(IEnumerable<IAsset> assets, Market calling, DateTime timeStamp, float peakPowerMW, float peakPrice, float offPeakPowerMW, float offPeakPrice)
            => new Bid_Future(assets, calling).PeakOffPeak(timeStamp, calling, peakPowerMW, peakPrice, offPeakPowerMW, offPeakPrice).Cast<Bid_Future>();
        public static Bid_Future Hour(IEnumerable<IAsset> assets, Market calling, List<(DateTime TimeStamp, float PowerMW, float Price)> timeSeries) => new Bid_Future(assets, calling).Hour(timeSeries);
        public static Bid_Future Hour(IAsset asset, Market calling, List<(DateTime TimeStamp, float PowerMW, float Price)> timeSeries) => Hour(new[] { asset }, calling, timeSeries);

        private void CreateUnderlyings(Market calling)
        {
            this.Underlyings = Bid_DayAhead.FromFuture(this, calling);
            this.Underlyings.ToList().ForEach(underlying => underlying.Clearing += (bid) => this.UnderlyingCleared?.Invoke(this, bid.Cast<Bid_DayAhead>()));
        }
        #endregion

        #region Private Bid-Methods
        // TODO: Implement validity check
        //protected override bool IsValid() => true;

        public override TimeSpan Duration => this.TimeStamp.GetDurationOfMonth();
        #endregion
    }
}
