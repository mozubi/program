﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Assets;
using winMoz.Assets.Plants;
using winMoz.Markets.Elements;
using System.Diagnostics;
using winMoz.Markets.ControlReserve;
using winMoz.Simulation;

namespace winMoz.Markets.Bids
{
    /// <summary>
    /// Abgeleitete Klasse der Klasse Bid_ControlReserve für die Regelleistungsaktivierung
    /// </summary>
    public class Bid_ControlReserve_Activation : Bid_ControlReserve
    {
        /// <summary>
        /// Base-Konstruktor (wird für EntityFramework benötigt)
        /// </summary>
        public Bid_ControlReserve_Activation() { } //: base() { }
        /// <summary>
        /// Base-Konstruktor
        /// </summary>
        /// <param name="assets">Kraftwerke, die an Gebot beteiligt sind.</param>
        /// <param name="simEvent">ID des Events auf dem dieses Gebot abgegeben wird</param>
        public Bid_ControlReserve_Activation(IEnumerable<IAsset> assets, SimEvent simEvent) : base(assets, simEvent) { }

        /// <summary>
        /// Aktivierung des Gebots
        /// </summary>
        /// <param name="calling"></param>
        public void Activate(ControlReserve_Activation calling)
        {
            ActivateBid(calling);
            this.IsCleared = this.Segments.Any(segment => segment.IsCleared);
        }

        /// <summary>
        /// Prüft, ob die angegebene Aktivierung das Gebot aktivieren darf
        /// </summary>
        /// <param name="calling">Aktivierung, die geprüft werden soll</param>
        /// <returns></returns>
        public bool IsActivation(ControlReserve_Activation calling) => calling == this.Event;

        /// <summary>
        /// Abstrakte Methode zum Clearing eines Gebots durch einen Markt
        /// </summary>
        /// <param name="calling">Aufrufendes Objekt - Methode wirkt nur, wenn <see cref="object"/> ein <see cref="Market"/> ist </param>
        /// <param name="clearingPrice">Preis, zu dem das Gebot ausgeführt wird</param>
        internal protected virtual void ActivateBid(ControlReserve_Activation calling)
        {
            foreach (var Bid_Segment in this.Segments)
                ((Bid_Segment_ControlReserve)Bid_Segment).Activate(calling);
        }

        //protected override void TransferBidSegments(Market calling) { }


        /// <summary>
        /// Überprüft, ob es für den angegebenen Bestimmungszeitraum valide Gebotssegmente gibt
        /// </summary>
        /// <param name="time2check">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="step">Abschnitt des Bestimmungszeitraums</param>
        /// <returns></returns>
        public override bool HasValidSegments(DateTime time2check, TimeSpan duration, TimeSpan step)
        {
            DateTime DurationEnd = time2check.Add(duration);
            return Segments.Count() != 0 ?
                (Segments.Any(seg => seg.TimeStamp.IsStartTimeOfDuration(step) && seg.TimeStamp >= time2check && seg.TimeStamp < DurationEnd && seg.IsActivation)) : false;
        }

        /// <summary>
        /// Gibt valide Gebotssegmente für Bestimmungszeitraum zurück
        /// </summary>
        /// <param name="timeStamp">Beginn des Bestimmungszeitraums</param>
        /// <param name="duration">Dauer des Bestimmungszeitraums</param>
        /// <param name="type">Regellleistungstyp</param>
        /// <returns></returns>
        public override List<Bid_Segment_ControlReserve> GetValidSegments(DateTime timeStamp, TimeSpan duration, ControlReserveShortCode.ShortCode type)
            => base.GetValidSegments(timeStamp, duration, type).Where(seg => seg.IsActivation).ToList();

        /// <summary>
        /// Füge Gebot hinzu
        /// </summary>
        /// <param name="code">Shortcode des Gebotssegments</param>
        /// <param name="timeStamp">Start des Bestimmungszeitraums des Gebots</param>
        /// <param name="volume">Menge des Gebots</param>
        /// <param name="price">Preis des Gebots</param>
        public void AddSegment(ShortCode code, DateTime timeStamp, float volume, float price)
            => base.AddSegment(code, timeStamp, volume, price);

        /// <summary>
        /// Bestimmt die neue Menge im Fahrplan der Kraftwerke nach der Markträumung für ein Kraftwerk
        /// </summary>
        /// <param name="asset">Kraftwerk</param>
        /// <param name="update">Geräumte Menge nach Zeit</param>
        /// <param name="assetShareOfVolume">Anteil, der auf das angegebene Kraftwerk fällt</param>
        protected override void UpdateSchedule(IAsset asset, List<(DateTime timeStamp, float volumeMW, float priceCleared)> update, float assetShareOfVolume)
        {
            asset.Schedule.AddPower(update.Select(el => (el.timeStamp, el.volumeMW * assetShareOfVolume)).ToList());

        }

        /// <summary>
        /// Erneuert die Abrufwahrscheinlichkeit für die Regelleistung
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="update"></param>
        /// <param name="before"></param>
        protected void UpdateAssetsCallProbability(IAsset asset, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> update, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> before)
        {
            List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, float> probabilities)> probabilities = before.Join(update, item => item.timeStamp, item => item.timeStamp, (be, up) => (be.timeStamp, be.Values.Join(up.Values, el => el.Key, el => el.Key, (bef, upd) => new { bef.Key, Value = Math.Abs(upd.Value.volumeMW / bef.Value.volumeMW) }).ToDictionary(el => el.Key, el => el.Value))).ToList();
            var ShortCodes = probabilities.SelectMany(item => item.probabilities.Keys).GroupBy(item => item, (group, elements) => group).ToList();
            ShortCodes.ForEach(item => asset.SetNewAssumedCallProbability(item, probabilities.SelectMany(el => el.probabilities).Where(el => el.Key == item).Select(el => el.Value).Average()));
        }

        /// <summary>
        /// Erneuert den Regelreserve-Fahrplan nach der Markträumung
        /// </summary>
        /// <param name="asset">Kraftwerke, die am Gebot beteiligt sind</param>
        /// <param name="toAdd">Regelreserve zum Hinzufügen</param>
        /// <param name="assetShareOfVolume">Anteil des Kraftwerks am Gebot</param>
        /// <param name="free">Regelreserve zum Freigeben</param>
        protected override void UpdateControlReserveSchedule(IAsset asset, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> toAdd, float assetShareOfVolume, List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> free = null)
        {
            var FreePositiveReservationSchedule = free.Where(item => item.Values.Any(el => el.Key.ToString().Contains("m_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.volumeMW).Sum())).ToList();
            var FreeNegativeReservationSchedule = free.Where(item => item.Values.Any(el => el.Key.ToString().Contains("n_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.volumeMW).Sum())).ToList();
            asset.ControlReserveSchedule.FreePositiveReservation(FreePositiveReservationSchedule);
            asset.ControlReserveSchedule.FreeNegativeReservation(FreeNegativeReservationSchedule);
            var updatePositiveSchedule = toAdd.Where(item => item.Values.Any(el => el.Key.ToString().Contains("m_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.volumeMW).Sum(), Extensions.GetWeightedValue(item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.priceCleared).ToList(), item.Values.Where(el => el.Key.ToString().Contains("m_")).Select(el => el.Value.volumeMW).ToList()))).ToList();
            var updateNegativeSchedule = toAdd.Where(item => item.Values.Any(el => el.Key.ToString().Contains("n_") && el.Value.volumeMW != 0)).Select(item => (item.timeStamp, item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.volumeMW).Sum(), -Extensions.GetWeightedValue(item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.priceCleared).ToList(), item.Values.Where(el => el.Key.ToString().Contains("n_")).Select(el => el.Value.volumeMW).ToList()))).ToList();
            asset.ControlReserveSchedule.ActivatePositive(updatePositiveSchedule);
            asset.ControlReserveSchedule.ActivateNegative(updateNegativeSchedule);
            asset.Schedule.Procure(updatePositiveSchedule);
            asset.Schedule.Procure(updateNegativeSchedule);
        }

        protected override List<(DateTime timeStamp, Dictionary<ControlReserveShortCode.ShortCode, (float volumeMW, float priceCleared)> Values)> GetFreeReserveSchedule() => this.GetControlReserveTimeSeries();

        /// <summary>
        /// Erneuert den Fahrplan nach der Markträumung
        /// </summary>
        public override void UpdateSchedule()
        {
            if (this.IsCleared && !this.IsScheduled && this.Assets != null)
            {
                var before = GetFreeReserveSchedule();
                var update = this.GetClearedControlReserveTimeSeries();

                var t1 = update.First().TimeStamp;
                var duration = update.Duration();

                var totalAssetVolume = this.Assets.SelectMany(el => el.Schedule.Get(t1, duration)).Sum(el => Math.Abs(el.PowerDiff));

                // TODO: If assets -not only- contain thermals... factor in mixed plants!
                //if (this.Assets.All(asset => asset.IsThermal))
                //totalPower = this.Assets.Select(plant => ((Plant_Thermal)plant).Units.Select(unit => unit.P_th_Kwk).Sum()).Sum();

                foreach (var asset in this.Assets)
                {
                    if (update.SelectMany(item => item.Item2).Any(item => float.IsNaN(item.Value.PriceCleared))) Debugger.Break();
                    float assetShareOfVolume = totalAssetVolume == 0 ? 1 : asset.Schedule.Get(t1, duration).Sum(el => Math.Abs(el.PowerDiff)) / totalAssetVolume;
                    //if (asset.IsThermal)
                    //plantPower = ((Plant_Thermal)asset).Units.Select(unit => unit.P_th_Kwk).Sum();
                    UpdateControlReserveSchedule(asset, update, assetShareOfVolume, before);
                }
                this.IsScheduled = true;
            }
        }

        /// <summary>
        /// Erneuert den Fahrplan nach der Markträumung
        /// </summary>
        public void UpdateAssetsCallProbability()
        {
            if (this.Assets != null && this.Assets.Count() > 0)
            {
                var before = GetFreeReserveSchedule();
                var update = this.GetClearedControlReserveTimeSeries();

                foreach (var asset in this.Assets)
                {
                    UpdateAssetsCallProbability(asset, update, before);
                }
                this.IsScheduled = true;
            }
        }

    }
}
