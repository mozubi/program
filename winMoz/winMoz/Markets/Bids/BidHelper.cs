﻿namespace winMoz.Markets.Bids
{
    /// <summary>
    /// <see langword="Static"/>-Extensionklasse für Gebote
    /// </summary>
    public static class BidHelper
    {
        /// <summary>
        /// Methode überprüft Typ eines BaseType-Gebots
        /// </summary>
        /// <param name="bid">Interfacetype Gebot <see cref="IBid>"/></param>
        /// <returns><see langword="true"/>, wenn Gebot ein Termingebot ist</returns>

        public static bool IsFuture(this IBid bid) => bid.GetType() == typeof(Bid_Future);

        /// <summary>
        /// Methode überprüft Typ eines BaseType-Gebots
        /// </summary>
        /// <param name="bid">BaseType Gebot <see cref="Bid>"/></param>
        /// <returns><see langword="true"/>, wenn Gebot ein DayAheadgebot ist</returns>
        public static bool IsDayAhead(this IBid bid) => bid.GetType() == typeof(Bid_DayAhead);

        /// <summary>
        /// Methode überprüft Typ eines BaseType-Gebots
        /// </summary>
        /// <param name="bid">BaseType Gebot <see cref="Bid>"/></param>
        /// <returns><see langword="true"/>, wenn Gebot ein DayAheadgebot ist</returns>
        public static bool IsIntraday(this IBid bid) => bid.GetType() == typeof(Bid_Intraday);

        /// <summary>
        /// Methode überprüft Typ eines BaseType-Gebots
        /// </summary>
        /// <param name="bid">BaseType Gebot <see cref="Bid>"/></param>
        /// <returns><see langword="true"/>, wenn Gebot ein Regelleistungsgebot ist</returns>
        public static bool IsControlReserve(this Bid bid) => typeof(Bid_ControlReserve_Capacity).IsAssignableFrom(bid.GetType())  || typeof(Bid_ControlReserve_Energy).IsAssignableFrom(bid.GetType()) || typeof(Bid_ControlReserve_Activation).IsAssignableFrom(bid.GetType());

    }
}
