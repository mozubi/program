﻿using System;
using System.Collections.Generic;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Simulation;

namespace winMoz.Markets.Bids
{
    public interface IBid
    {
        //int BidId { get; }
        DateTime TimeStamp { get; }
        TimeSpan Duration { get; }
        bool IsCleared { get; }
        List<Bid_Segment> Segments { get; }

        List<(DateTime TimeStamp, float VolumeMW, float PriceCleared)> GetClearedTimeSeries();
        List<(DateTime TimeStamp, float VolumeMW, float PriceCleared)> GetTimeSeries();

        #region Attributes
        IAgent Agent { get; }
        IEnumerable<IAsset> Assets { get;}

        string AssetIds { get;}

        string AssetTypes { get;}
        SimEvent Event { get;  }

        /// <summary>
        /// ID des Gebotes 
        /// </summary>
        int BidId { get;  }
        #endregion

        #region Booleans
        bool IsScheduled { get;  }
        #endregion

        bool IsValid(DateTime timeStamp, TimeSpan duration, TimeSpan step);

        #region Bid_Segment-Methods
        void AddSegment(Bid_Segment segment);

        void AddSegments(IEnumerable<Bid_Segment> Bid_Segments);
        #endregion

        #region Clearing & Procurement

        void UpdateSchedule();

        #endregion
    }
}