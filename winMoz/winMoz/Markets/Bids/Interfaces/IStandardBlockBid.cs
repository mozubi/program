﻿using System;

namespace winMoz.Markets.Bids
{
    public interface IStandardBlockBid
    {
        Bid Base(DateTime timeStamp, Market calling, float powerMW, float price);
        Bid Peak(DateTime timeStamp, Market calling, float powerMW, float price);
        Bid OffPeak(DateTime timeStamp, Market calling, float powerMW, float price);
        Bid PeakOffPeak(DateTime timeStamp, Market calling, float peakPowerMW, float peakPrice, float offPeakPowerMW, float offPeakPrice);
    }
}