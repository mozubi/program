﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Results;
using winMoz.Markets.ControlReserve;
using winMoz.Simulation;

namespace winMoz.Markets.Bids
{
    public class Bid_Segment_ControlReserve : Bid_Segment
    {
        /// <summary>
        /// SimEvent, auf dem das Gebot zuerst verwendet wurde.
        /// </summary>
        public SimEvent FirstEvent { get; set; }


        /// <summary>
        /// Zeigt an, ob dieses Gebot auf dem Regelleistungsmarkt angeboten werden soll oder bereits dort angeboten wurde.
        /// </summary>
        public bool CanBeAuctioned { get; private set; } = true;

        /// <summary>
        /// Setzt das Gebot ausführbar mit der angegebenen Menge: 
        /// Ist die Menge nicht angegeben, wird die Menge des Gebotssegments nicht verändert 
        /// </summary>
        /// <param name="volumeMW">Neu zu setzende Menge</param>
        public void SetAuctionAllowed(float volumeMW = default)
        {
            this.CanBeAuctioned = true;
            if (volumeMW != default)
                this.ChangeVolumeMWTo(volumeMW);
        }

        //public void SetAuctionAllowed() => this.CanBeAuctioned = true;

        /// <summary>
        /// SimEvent, zu dem die Gebote transferiert werden soll.
        /// </summary>
        public SimEvent TransferEvent { get; set; }

        private Bid_Segment CoupledSegment { get; set; }

        public void TransferCoupledSegmentTo(SimEvent transferEvent)
        {
            if (CoupledSegment != null && CoupledSegment is Bid_Segment_ControlReserve)
                ((Bid_Segment_ControlReserve)CoupledSegment).TransferEvent = transferEvent;
        }

        private Bid_Segment_ControlReserve() : base() { }

        internal Bid_Segment_ControlReserve(ShortCode shortCode, DateTime timeStamp, float volumeMW, float price, SimEvent firstEvent, SimEvent transferEvent, bool canBeAuctioned = true, Bid_Segment coupledSegment = null) : base(shortCode, timeStamp, volumeMW, price)
        {
            CanBeAuctioned = canBeAuctioned;
            CoupledSegment = coupledSegment;
            FirstEvent = firstEvent;
            TransferEvent = transferEvent;
        }

        public bool IsInDuration(DateTime time2check, TimeSpan duration)
        {
            var EndOfDuration = Helper.Extensions.GetEndOfDuration(time2check, duration);
            var result = TimeStamp >= time2check && TimeStamp.Add(Duration) <= EndOfDuration;
            return result;
        }

        public bool DurationIsInSegment(DateTime time2check, TimeSpan duration)
        {
            var EndOfDuration = Helper.Extensions.GetEndOfDuration(time2check, duration);
            var result = time2check >= TimeStamp && EndOfDuration <= TimeStamp.Add(Duration);
            return result;
        }
        public List<(DateTime TimeStamp, ControlReserveShortCode.ShortCode type, float VolumeMW, float Price)> GetControlReserveTimeSeries() => GetTimes().Select(el => (el, this.ShortCode.GetEnum(), this.VolumeMW, this.Price)).ToList();

        public List<(DateTime TimeStamp, ControlReserveShortCode.ShortCode type, float VolumeMW, float Price)> GetClearedControlReserveTimeSeries() => GetTimes().Select(el => (el, this.ShortCode.GetEnum(), this.ClearedVolumeMW, this.ClearedPrice)).ToList();

        /// <summary>
        /// Setzt den markt, zu dem das Gebot transferiert werden soll
        /// </summary>
        /// <param name="simEvent">TransferEvent</param>
        public bool IsTransferEvent(SimEvent simEvent) => simEvent == TransferEvent;

        public bool IsTenderOrActivationVolume() => ShortCode.Is_positive_FRR ? this.VolumeMW < 0 : this.VolumeMW > 0;

        /// <summary>
        /// Clearing-Funktion des Gebotssegments
        /// </summary>
        /// <param name="market">Ausführender Markt</param>
        internal override void Clear(Market market)
        {
            var result = market.Results.Get(this.ShortCode, this.TimeStamp);

            if (result != default && result.IsControlReserve)
            {
                if (((ControlReserveResult)result).Price_Max >= this.Price)
                {
                    this.IsCleared = true;
                    this.ClearedPrice = this.Price;
                    if (this.IsCapacity) ((Bid_Segment_ControlReserve)CoupledSegment).SetAuctionAllowed(this.ClearedVolumeMW);
                    if (((ControlReserveResult)result).Price_Max == this.Price)
                        this.ClearedVolumeMW = this.ShortCode.Is_negative_FRR ? this.VolumeMW * result.ProRataVolumePurchase : this.VolumeMW * result.ProRataVolumeSell;
                    else
                        this.ClearedVolumeMW = this.VolumeMW;
                }
            }
        }

        /// <summary>
        /// Clearing-Funktion des Gebotssegments
        /// </summary>
        /// <param name="market">Ausführender Markt</param>
        internal void Activate(ControlReserve_Activation calling)
        {
            var result = calling.Results.Get(this.ShortCode, this.TimeStamp);

            if (result != default && result.IsControlReserve)
            {
                if (((ControlReserveResult)result).Price_Max >= this.Price)
                {
                    this.IsCleared = true;
                    this.ClearedPrice = this.Price;
                    if (((ControlReserveResult)result).Price_Max == this.Price)
                        this.ClearedVolumeMW = this.ShortCode.Is_negative_FRR ? this.VolumeMW * result.ProRataVolumePurchase : this.VolumeMW * result.ProRataVolumeSell;
                    else
                        this.ClearedVolumeMW = this.VolumeMW;
                }
            }
        }
    }
}
