﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Markets
{
    /// <summary>
    /// <see langword="Static"/>-Extensionklasse für Gebote
    /// </summary>
    public static class BidHelper
    {
        public static List<Bid_DayAhead> ToDayAhead(this Bid_Future future, DateTime auctionTime)
        {
            var bids = new List<Bid_DayAhead>();

            foreach (var segment in future.Segments)
            {
                bids.Add(new Bid_DayAhead(future.Assets,
                    (ShortCode: GetDAEqShortCode(segment),
                     Timestamp: auctionTime,
                     segment.VolumeMW,
                     Price: GetUnlimitedPriceDA(segment))
                    ).SetUnderlyingFuture(future));
            }
            return bids;
        }

        private static double GetUnlimitedPriceDA(Bid_Segment segment)
        {
            switch (segment.Side)
            {
                case "B": // Kauf
                    return Market_Attributions.Upper_Price_bound_DA;

                case "S": // Verkauf
                    return Market_Attributions.Lower_Price_bound_DA;

                default:
                    return -999999.9999;
            }
        }

        private static string GetDAEqShortCode(Bid_Segment segment)
        {
            switch (segment.ShortCode)
            {
                case "N2BM":
                    return "DB";

                case "N2PM":
                    return "DP";

                case "N2OPM":
                    return "DOP";

                default:
                    return "";
            }
        }

        /// <summary>
        /// Methode überprüft Typ eines BaseType-Gebots
        /// </summary>
        /// <param name="bid">BaseType Gebot <see cref="Bid>"/></param>
        /// <returns><see langword="true"/>, wenn Gebot ein Termingebot ist</returns>
        public static bool IsFuture(this Bid bid)
        {
            var type = bid.GetType();

            if (type == typeof(Bid_Future))
                return true;

            return false;
        }

        /// <summary>
        /// Methode überprüft Typ eines BaseType-Gebots
        /// </summary>
        /// <param name="bid">BaseType Gebot <see cref="Bid>"/></param>
        /// <returns><see langword="true"/>, wenn Gebot ein DayAheadgebot ist</returns>
        public static bool IsDayAhead(this Bid bid)
        {
            var type = bid.GetType();

            if (type == typeof(Bid_DayAhead))
                return true;

            return false;
        }

        /// <summary>
        /// Methode überprüft Shortcode eines Gebots und gibt die zugrundeliegende Gebotsdauer in vollen Stunden zurück
        /// </summary>
        /// <param name="bid">Zugrundeliegendes Blockgebot <see cref="Bid_Future"/></param>
        /// <param name="seg">Gebotssegment</param>
        /// <returns>Gebotsdauer in vollen Stunden <see cref="int"/></returns>
        internal static double GetHourDuration(this Bid_Future bid, Bid_Segment seg)
        {
            switch (seg.ShortCode)
            {
                case "N2BM":
                    return bid.Product.Duration.TotalHours;

                case "N2PM":
                case "N2OPM":
                    return bid.Product.Duration.TotalHours / 2;

                default:
                    return 0;
            }
        }

        public static void ScheduleProcuredBid(this Bid bid)
        {
            if (bid.IsCleared && bid.IsDayAhead() || bid.IsFuture())
            {
                var procured = bid.GetProcuredTimeSeries();

                foreach (var asset in bid.Assets)
                {
                    asset.ScheduleProcuredTS(procured);
                }
            }
        }

    }
}
