﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Simulation;

namespace winMoz.Markets.Bids
{
    /// <summary>
    /// <see langword="Abstract"/> Gebotsklasse als Basistyp für weitere Gebotsarten
    /// </summary>
    public class Bid : IBid //  abstract
    {
        #region Attributes
        /// <summary>
        /// Agent
        /// </summary>
        public IAgent Agent => Assets.FirstOrDefault().BalancingGroup;
        /// <summary>
        /// Kraftwerke, die zum Gebot beitragen
        /// </summary>
        public IEnumerable<IAsset> Assets { get; private set; }
        [NotMapped]
        private int[] assetIds { get; set; }
        /// <summary>
        /// IDs der zum Bebot beitragenden Kraftwerke als String
        /// </summary>
        public string AssetIds { get; private set; }
        /// <summary>
        /// Typen der zum Bebot beitragenden Kraftwerke als String
        /// </summary>
        public string AssetTypes { get; private set; }
        /// <summary>
        /// Gebotssegmente (bündelt Gebote des selben Kraftwerksparks
        /// </summary>
        public List<Bid_Segment> Segments { get; private set; } = new List<Bid_Segment>();

        /// <summary>
        /// Event, auf dem dieses Gebot abgegeben werden soll
        /// </summary>
        public SimEvent Event { get; private set; }
        /// <summary>
        /// ID des Gebotes 
        /// </summary>
        public int BidId { get; private set; }
        private static int _id = 1;
        #endregion

        #region Events
        /// <summary>
        /// Handler der Funktionen, die mit dem Clearing in zusammenhang sethen auslöst
        /// </summary>
        /// <typeparam name="Bid">Gebot</typeparam>
        /// <param name="bid">Gebot</param>
        public delegate void ClearingEventHandler<Bid>(Bid bid) where Bid : winMoz.Markets.Bids.Bid;
        /// <summary>
        /// Handler der Funktionen, die mit dem Clearing in zusammenhang sethen auslöst
        /// </summary>
        public event ClearingEventHandler<Bid> Clearing;
        #endregion

        #region Booleans
        /// <summary>
        /// Clearing - <see langword="true"/>, wenn Gebot ganz oder teilweise erfüllt wurde
        /// </summary>
        private bool _isCleared { get; set; }
        /// <summary>
        /// Clearing - <see langword="true"/>, wenn Gebot ganz oder teilweise erfüllt wurde
        /// </summary>
        public bool IsCleared
        {
            get { return _isCleared; }
            set
            {
                _isCleared = value;
                if (_isCleared) Clearing?.Invoke(this);
            }
        }
        /// <summary>
        /// Zeigt an, ob diese Gebot bereits zur Neuaufstellung des Fahrplans verwendet wurde
        /// </summary>
        public bool IsScheduled { get; protected set; } = false;
        #endregion

        #region Konstruktoren
        /// <summary>
        /// Basis-Konstruktor (wird für das EntityFramework gebraucht)
        /// </summary>
        protected Bid()// : base()
        {
            this.BidId = System.Threading.Interlocked.Increment(ref _id);
            this.IsCleared = false;
        }
        /// <summary>
        /// Konstruktor für Gebote
        /// </summary>
        /// <param name="simEvent">ID des Events auf dem dieses Gebot abgegeben wird</param>
        protected Bid(SimEvent simEvent) : this()
        {
            Event = simEvent;
        }

        /// <summary>
        /// Konstruktor für Gebote
        /// </summary>
        /// <param name="assets">Kraftwerke, die zu diesem Gebot beitragen</param>
        /// <param name="simEvent">ID des Events auf dem dieses Gebot abgegeben wird</param>
        public Bid(IEnumerable<IAsset> assets, SimEvent simEvent) : this(simEvent)
        {
            this.Assets = assets;
            this.assetIds = assets.Select(asset => asset.Id).ToArray();
            this.AssetIds = String.Join(",", assetIds);
            this.AssetTypes = String.Join(",", assets.GroupBy(asset => asset.GetType().FullName, (el1, el2) => el1).ToList());
        }

        /// <summary>
        /// Konstruktor für Gebote
        /// </summary>
        /// <param name="asset">Kraftwerk, das dieses Gebot stützt</param>
        /// <param name="simEvent">ID des Events auf dem dieses Gebot abgegeben wird</param>
        public Bid(IAsset asset, SimEvent simEvent) : this(new List<IAsset>() { asset }, simEvent) { }

        //public Bid(IAsset asset, Type marketType) : this(new List<IAsset>() { asset }, marketType) { }
        #endregion

        #region Bid-Times
        /// <summary>
        /// Zeitstempel des Gebots - Definiert über den ersten Zeitstempel der Bidsegmente
        /// </summary>
        public DateTime TimeStamp => Segments.Select(seg => seg.TimeStamp).FirstOrDefault().ToFullHour();
        /// <summary>
        /// Dauer des Gebots - Definiert über die Summe der Gebotssegmente
        /// </summary>
        public virtual TimeSpan Duration => TimeSpan.FromTicks(this.Segments.Sum(el => el.Duration.Ticks));
        #endregion

        /// <summary>
        /// Prüft, ob alle Gebotssegmente in das mitgelieferte Bestimmungszeitfenster passen.
        /// </summary>
        /// <param name="timeStamp">Bestimmungszeitraumanfang</param>
        /// <param name="duration">Bestimmungszeitraumslänge</param>
        /// <param name="step">Bestimmungszeitraumsabschnittslänge</param>
        /// <returns></returns>
        public virtual bool IsValid(DateTime timeStamp, TimeSpan duration, TimeSpan step)
        {
            var EndOfDuration = Extensions.GetEndOfDuration(timeStamp, duration);
            return this.Segments.All(seg => seg.TimeStamp >= timeStamp && seg.TimeStamp < EndOfDuration);
        }

        #region Bid_Segment-Methods
        /// <summary>
        /// Hinzufügen eines Gebotssegment
        /// </summary>
        /// <param name="segment">Gebotssegment</param>
        public void AddSegment(Bid_Segment segment)
        {
            //segment.BidId = this.Id;
            this.Segments.Add(segment);
            //if (!this.IsValid()) throw new InvalidOperationException("Specific Bid_Segment not valid for this kind of bid.");
        }

        /// <summary>
        /// Hinzufügen mehrerer Gebotssegmente
        /// </summary>
        /// <param name="segments">Gebotssegmente</param>
        public void AddSegments(IEnumerable<Bid_Segment> segments) => segments.ToList().ForEach(segment => AddSegment(segment));

        //public void AddSegments(IEnumerable<Bid_Segment> Bid_Segments) => Bid_Segments.ToList().ForEach(segment => AddSegment(segment));
        #endregion

        #region Clearing & Procurement
        /// <summary>
        /// Clearing des Gebots ausgelöst durch Markt
        /// </summary>
        /// <param name="calling">Auslösender Markt</param>
        internal void Clear(Market calling)
        {
            ClearBid(calling);
            this.IsCleared = this.Segments.Any(segment => segment.IsCleared);
        }

        /// <summary>
        /// Abstrakte Methode zum Clearing eines Gebots durch einen Markt
        /// </summary>
        /// <param name="calling">Aufrufendes Objekt - Methode wirkt nur, wenn <see cref="object"/> ein <see cref="Market"/> ist </param>
        internal protected virtual void ClearBid(Market calling)
        {
            if (IsClearingMarket(calling))
                foreach (var Bid_Segment in this.Segments)
                    Bid_Segment.Clear(calling);
        }

        /// <summary>
        /// Fragt ab, ob die Id des auslösende Marktes mit der im Gebot hinterlegten übereinstimmt.
        /// </summary>
        /// <param name="calling"></param>
        /// <returns></returns>
        public virtual bool IsClearingMarket(Market calling) => calling == Event;

        //protected virtual bool IsClearingMarket(Market calling) => throw new NotImplementedException();

        /// <summary>
        /// Bestimmt die neue Menge im Fahrplan der Kraftwerke nach der Markträumung
        /// </summary>
        public virtual void UpdateSchedule()
        {
            if (this.IsCleared && !this.IsScheduled)
            {
                var update = this.GetClearedTimeSeries();

                var t1 = update.First().TimeStamp;
                var duration = update.Count() == 1 ? TimeSpan.FromMinutes(15) : update.Duration(); //TODO: Hier sollte minimaler Zeitabschnitt hin, falls der mal geändert werden soll.

                var totalAssetVolume = this.Assets.SelectMany(el => el.Schedule.Get(t1, duration)).Sum(el => Math.Abs(el.PowerDiff));

                // TODO: If assets -not only- contain thermals... factor in mixed plants!
                //if (this.Assets.All(asset => asset.IsThermal))
                //totalPower = this.Assets.Select(plant => ((Plant_Thermal)plant).Units.Select(unit => unit.P_th_Kwk).Sum()).Sum();

                foreach (var asset in this.Assets)
                {
                    if (update.Select(item => item.PriceCleared).Any(item => float.IsNaN(item))) Debugger.Break();
                    float assetShareOfVolume = totalAssetVolume == 0 ? 1 : asset.Schedule.Get(t1, duration).Sum(el => Math.Abs(el.PowerDiff)) / totalAssetVolume;
                    //if (asset.IsThermal)
                    //plantPower = ((Plant_Thermal)asset).Units.Select(unit => unit.P_th_Kwk).Sum();
                    UpdateSchedule(asset, update, assetShareOfVolume);
                }
                this.IsScheduled = true;
            }
        }

        /// <summary>
        /// Bestimmt die neue Menge im Fahrplan der Kraftwerke nach der Markträumung für ein Kraftwerk
        /// </summary>
        /// <param name="asset">Kraftwerk</param>
        /// <param name="update">Geräumte Menge nach Zeit</param>
        /// <param name="assetShareOfVolume">Anteil, der auf das angegebene Kraftwerk fällt</param>
        protected virtual void UpdateSchedule(IAsset asset, List<(DateTime timeStamp, float volumeMW, float priceCleared)> update, float assetShareOfVolume)
            => asset.Schedule.Procure(update.Select(el => (el.timeStamp, el.volumeMW * assetShareOfVolume, el.priceCleared)).ToList());


        #endregion

        #region TimeSeries
        /// <summary>
        /// Bestimme die im Gebot veranschlagte Menge und den Preis nach Zeit
        /// </summary>
        /// <param name="ignoreClearing">Soll die gesamte Menge genommen werden</param>
        /// <returns></returns>
        private List<(DateTime TimeStamp, float VolumeMW, float PriceCleared)> GetTimeSeries(bool ignoreClearing)
        {
            var schedule = new List<(DateTime TimeStamp, float Power, float PriceCleared)>();

            //Alle Nicht-Blöcke
            if (ignoreClearing)
                schedule = this.Segments
                    //.Where(item => !item.ShortCode.IsBlock).ToList()
                    .SelectMany(item => item.GetTimeSeries())
                    .GroupBy(item => item.TimeStamp, (el1, el2) => (el1, el2.Select(item => item.VolumeMW).Sum(), el2.Select(item => item.VolumeMW).Sum() != 0 ? el2.Select(item => item.Price * item.VolumeMW).Sum() / el2.Select(item => item.VolumeMW).Sum() : 0)).ToList();
            else
            {
                schedule = this.Segments
                    //.Where(item => !item.ShortCode.IsBlock)
                    .SelectMany(item => item.GetClearedTimeSeries())
                    .GroupBy(item => item.TimeStamp, (el1, el2) => (el1, el2.Select(item => item.VolumeMW).Sum(), el2.Select(item => item.VolumeMW).Sum() != 0 ? el2.Select(item => item.Price * item.VolumeMW).Sum() / el2.Select(item => item.VolumeMW).Sum() : 0)).ToList();
            }

            return schedule;
        }

        //static float weightedAverage(IEnumerable<float> values, IEnumerable<float> weights)
        //    => weights.Sum(el => Math.Abs(el)) != 0 ? values.Zip(weights, (val, wgh) =>  Math.Abs(val) * Math.Abs(wgh)).Sum() / weights.Sum(el => Math.Abs(el)) : 0.0F;

        /// <summary>
        /// Bestimme die gesamte im Gebot veranschlagte Menge und den Preis nach Zeit
        /// </summary>
        /// <returns></returns>
        public List<(DateTime TimeStamp, float VolumeMW, float PriceCleared)> GetTimeSeries() => GetTimeSeries(true);

        /// <summary>
        /// Bestimme die geräumte im Gebot veranschlagte Menge und den Preis nach Zeit
        /// </summary>
        /// <returns></returns>
        public List<(DateTime TimeStamp, float VolumeMW, float PriceCleared)> GetClearedTimeSeries() => GetTimeSeries(false);
        #endregion

        /// <summary>
        /// Wandelt Gebotstyp in Typ um
        /// </summary>
        /// <typeparam name="TBid"></typeparam>
        /// <returns></returns>
        public TBid Cast<TBid>() where TBid : Bid => (TBid)this;

        ///// <summary>
        ///// Setzen der Id des Events, auf dem das Gebot abgegeben werden soll.
        ///// </summary>
        ///// <param name="simEvent">Zugehöriges Event</param>
        //protected virtual void SetEventId(SimEvent simEvent) => this.EventId = simEvent.Id;

        ///// <summary>
        ///// Setzen des Eventtyps, auf dem das Gebot abgegeben werden soll.
        ///// </summary>
        ///// <param name="simEvent">Zugehöriges Event</param>
        //protected virtual void SetEventType(SimEvent simEvent) => this.EventType = simEvent.GetType();
    }
}
