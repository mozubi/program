﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;

namespace winMoz.Markets.Results
{
    public interface IExternalMarketResult
    {
        DateTime TimeStamp { get; }

        MarketAreaEnum MarketArea { get;  }

        float Price { get; }

        float Export { get;}

        float Import { get;}

        float Saldo { get; }

        float Verschiebevolumen { get; }
    }
}
