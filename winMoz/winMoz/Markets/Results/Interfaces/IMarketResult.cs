﻿using System;
using winMoz.Markets.Bids;

namespace winMoz.Markets.Results
{
    public interface IMarketResult
    {
        int Id { get; }
        float Price { get; }
        ShortCode ShortCode { get; }
        DateTime TimeStamp { get; }
        float Volume { get; }

        bool IsIntraDay { get;}
        bool IsFutures { get; }
        bool IsDayAhead { get; }
        bool IsControlReserve { get;}
        bool IsBlock { get; }
        /// <summary>
        /// Anteil der Verkaufsvolumina des marktsetzenden Preises, der geräumt wird.
        /// </summary>
        float ProRataVolumePurchase { get;  }
        /// <summary>
        /// Anteil der Kaufsvolumina des marktsetzenden Preises, der geräumt wird.
        /// </summary>
        float ProRataVolumeSell { get;}
    }
}