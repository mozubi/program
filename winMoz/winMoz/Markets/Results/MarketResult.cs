﻿using System;
using winMoz.Markets.Bids;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access;

namespace winMoz.Markets.Results
{
    public abstract class MarketResult : IMarketResult, IComparable<MarketResult>
    {

        public MarketResult(DateTime timeStamp, ShortCode shortCode, float price, float volume)
        {
            this.TimeStamp = timeStamp;
            this.ShortCode = shortCode;
            this.IsIntraDay = shortCode.IsIntraDay;
            this.IsControlReserve = ShortCode.IsControlReserve;
            this.Price = (float)Math.Round(price, 2);
            this.Volume = (float)Math.Round(volume, 2);
            this.ProRataVolumePurchase = 1;
            this.ProRataVolumeSell = 1;
        }

        public MarketResult(DateTime timeStamp, ShortCode shortCode, float price, float volume, float proRataVolumePurchase = 1, float proRataVolumeSell = 1)
        {
            this.TimeStamp = timeStamp;
            this.ShortCode = shortCode;
            this.IsIntraDay = shortCode.IsIntraDay;
            this.IsControlReserve = ShortCode.IsControlReserve;
            this.Price = (float)Math.Round(price, 2);
            this.Volume = (float)Math.Round(volume, 2);
            this.ProRataVolumePurchase = proRataVolumePurchase;
            this.ProRataVolumeSell = proRataVolumeSell;
        }

        public int Id { get; private set; }
        public DateTime TimeStamp { get; private set; }
        public ShortCode ShortCode { get; private set; }
        public float Price { get; private set; }
        public float Volume { get; private set; }

        public bool IsIntraDay { get; private set; }
        public bool IsFutures { get => !IsIntraDay && !IsDayAhead; }
        public bool IsDayAhead { get => !IsIntraDay; }
        public bool IsControlReserve { get; private set; }
        public bool IsBlock { get; protected set; } = false;
        /// <summary>
        /// Anteil der Verkaufsvolumina des marktsetzenden Preises, der geräumt wird.
        /// </summary>
        public float ProRataVolumePurchase { get; private set; }
        /// <summary>
        /// Anteil der Kaufsvolumina des marktsetzenden Preises, der geräumt wird.
        /// </summary>
        public float ProRataVolumeSell { get; private set; }

        public int CompareTo(MarketResult other)
        {
            var TimeStampCompareTo = this.TimeStamp.Ticks.CompareTo(other.TimeStamp.Ticks);
            var ShortCodeCompareTo = this.ShortCode.CompareTo(other.ShortCode);
            if (TimeStampCompareTo == -ShortCodeCompareTo && TimeStampCompareTo != 0) return TimeStampCompareTo;
            else return TimeStampCompareTo + ShortCodeCompareTo;
        }
    }

    public class DayAheadResult : MarketResult
    {

        public DayAheadResult(DateTime timeStamp, ShortCode shortCode, float price, float volume) : base(timeStamp, shortCode, price, volume)
        {
            this.IsBlock = !shortCode.IsHour;
        }

        public DayAheadResult(DateTime timeStamp, ShortCode shortCode, float price, float volume, float proRataVolumePurchase = 1, float proRataVolumeSell = 1) : base(timeStamp, shortCode, price, volume, proRataVolumePurchase, proRataVolumeSell)
        {
            this.IsBlock = !shortCode.IsHour;
        }
    }

    public class IntraDayResult : MarketResult
    {
        public IntraDayResult(DateTime timeStamp, ShortCode shortCode, float price, float volume) : base(timeStamp, shortCode, price, volume)
        {
        }

        public IntraDayResult(DateTime timeStamp, ShortCode shortCode, float price, float volume, float proRataVolumePurchase = 1, float proRataVolumeSell = 1) : base(timeStamp, shortCode, price, volume, proRataVolumePurchase, proRataVolumeSell)
        {
        }

    }
    public class FuturesResult : MarketResult
    {
        public FuturesResult(DateTime timeStamp, ShortCode shortCode, float price, float volume) : base(timeStamp, shortCode, price, volume)
        {
        }

        public FuturesResult(DateTime timeStamp, ShortCode shortCode, float price, float volume, float proRataVolumePurchase, float proRataVolumeSell) : base(timeStamp, shortCode, price, volume, proRataVolumePurchase, proRataVolumeSell)
        {
        }
    }

    public class ControlReserveResult : MarketResult
    {
        public float Price_Min { get; private set; }
        public float Price_Max { get; private set; }
        public ControlReserveResult(DateTime timeStamp, ShortCode shortCode, float price, float volume) : base(timeStamp, shortCode, price, volume)
        {
        }

        public ControlReserveResult(DateTime timeStamp, ShortCode shortCode, float price, float volume, float price_Min, float price_Max, float proRataVolumePurchase, float proRataVolumeSell) : base(timeStamp, shortCode, price, volume, proRataVolumePurchase, proRataVolumeSell)
        {
            Price_Min = price_Min;
            Price_Max = price_Max;
        }
    }

}