﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Data.Access;

namespace winMoz.Markets.Results
{
    public class ExternalMarketResult : IComparable<ExternalMarketResult>
    {
        public DateTime TimeStamp { get; private set; }
        public MarketAreaEnum MarketArea { get; private set; }

        public float Price { get; private set; }

        public float Export { get; private set; }

        public float Import { get; private set; }

        public float Saldo { get; private set; }

        public float Verschiebevolumen { get; private set; }

        private ExternalMarketResult() { }

        public ExternalMarketResult(DateTime timeStamp, MarketAreaEnum marketArea, float price, float export, float import, float verschiebevolumen)
        {
            this.TimeStamp = timeStamp;
            this.MarketArea = marketArea;
            this.Price = price;
            this.Export = export;
            this.Import = import;
            this.Saldo = this.Export - this.Import;
            this.Verschiebevolumen = verschiebevolumen;
        }

        #region IComparable
        public int CompareTo(ExternalMarketResult other)
        {
            var value = TimeStamp.CompareTo(other.TimeStamp);
            if (value == 0)
            {
                return MarketArea.Id - other.MarketArea.Id;
            }
            else
            {
                return value;
            }
        }
        #endregion
    }
}
