﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets.Elements;
using winMoz.Helper;
using winMoz.Helper;

namespace winMoz.Markets.EoMMarkets
{
    /// <summary>
    /// Klasse zur Erzeugung der Merit Order für den lokalen Markt
    /// </summary>
    class LocalMarketArea : MarketAreaUP
    {
        private Dictionary<int, List<(decimal Price, decimal Volume)>> BidsSortedAuctionTime { get; set; }

        /// <summary>
        /// Klasse zur Erzeugung der Merit Order für den lokalen Markt
        /// </summary>
        /// <param name="priceUpperBound"></param>
        /// <param name="priceLowerBound"></param>
        /// <param name="country"></param>
        /// <param name="ts"></param>
        public LocalMarketArea(MarketAreaEnum country, Market coupledMarket, DefaultPrices default_Prices) :
            base(country, coupledMarket, default_Prices)
        {
            int NumberOfAuctions = Extensions.GetCountOfTimeIntervals(this.AuctionTimeframe.Duration, this.AuctionTimeframe.Step);
            InitialiseBidsSortedAuctionTime(NumberOfAuctions);
        }

        public void ClearBidsSortedAuctionTime()
        {
            foreach (var bidList in BidsSortedAuctionTime) bidList.Value.Clear();
        }

        public void InitialiseBidsSortedAuctionTime(int numberOfAuctions)
        {
            BidsSortedAuctionTime = new Dictionary<int, List<(decimal, decimal)>>();
            Enumerable.Range(0, numberOfAuctions).ToList().ForEach(hour =>
            {
                BidsSortedAuctionTime.Add(hour, new List<(decimal, decimal)>());
            });
        }

        public void AddBidToListSortedAuction(decimal Price, decimal Volume, int Position)
            => BidsSortedAuctionTime[Position].Add((Price, Volume));

        /// <summary>
        /// Merit Order erzeugen
        /// </summary>
        /// <param name="AuctionDate"></param>
        public override MA_Auction CreateMeritOrder(MA_Auction coupledMeritOrder, DateTime AuctionDate)
        {
            coupledMeritOrder.TS = AuctionDate;
            MarketResultUP result = GetMarketResult();
            return InitMeritOrder(coupledMeritOrder, result);
        }

        public override void UpdateMeritOrder(MA_Auction coupledMeritOrder, DateTime time)
        {
            TS = time;
            DeleteBidsPurchaseSell();
            AddBids(BidsSortedAuctionTime[Extensions.GetPositionOfTimeInterval(time, this.AuctionTimeframe.TimeStamp, this.AuctionTimeframe.Step)]);
            CreateMeritOrder(coupledMeritOrder, time);
            SetWindProgDE(coupledMeritOrder);
        }
    }
}
