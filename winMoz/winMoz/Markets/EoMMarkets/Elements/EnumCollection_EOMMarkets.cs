﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace winMoz.Markets.EoMMarkets.Elements
{
    public class EnumCollection_EOMMarkets
    {
        public enum ThermalEnum
        {
            [EnumMember(Value = "Nuclear")]
            Nuclear,
            [EnumMember(Value = "Hard_coal")]
            HardCoal,
            [EnumMember(Value = "Lignite")]
            Lignite,
            [EnumMember(Value = "Gas")]
            Gas,
            [EnumMember(Value = "Oil")]
            Oil
        }

        public enum GenerationEnum
        {
            [EnumMember(Value = "Biomass")]
            Biomass,
            [EnumMember(Value = "Fossil_coal_derived_gas")]
            Fossil_Coal_Derived_Gas,
            [EnumMember(Value = "Fossil_gas")]
            Fossil_Gas,
            [EnumMember(Value = "Fossil_hard_coal")]
            Fossil_Hard_Coal,
            [EnumMember(Value = "Fossil_oil")]
            Fossil_Oil,
            [EnumMember(Value = "Fossil_oil_shale")]
            Fossil_Oil_Shale,
            [EnumMember(Value = "Fossil_peat")]
            Fossil_Peat,
            [EnumMember(Value = "Geothermal")]
            Geothermal,
            [EnumMember(Value = "Hydro_pumped_storage")]
            Hydro_Pumped_Storage,
            [EnumMember(Value = "Hydro_Run_of_river_and_poundage")]
            Hydro_Run_Of_River_And_Poundage,
            [EnumMember(Value = "Hydro_water_reservoir")]
            Hydro_Water_Reservoir,
            [EnumMember(Value = "Marine")]
            Marine,
            [EnumMember(Value = "Nuclear")]
            Nuclear,
            [EnumMember(Value = "Other")]
            Other,
            [EnumMember(Value = "Other_renewable")]
            Other_Renewable,
            [EnumMember(Value = "Solar")]
            Solar,
            [EnumMember(Value = "Waste")]
            Waste,
            [EnumMember(Value = "Wind_Offshore")]
            Wind_Offshore,
            [EnumMember(Value = "Wind_Onshore")]
            Wind_Onshore
        }

        public enum ConstantEnum
        {
            [EnumMember(Value = "Biomass")]
            Biomass,
            [EnumMember(Value = "Waste")]
            Waste,
            [EnumMember(Value = "Marine")]
            Marine,
            [EnumMember(Value = "Geothermal")]
            Geothermal,
            [EnumMember(Value = "Hydro")]
            Hydro
        }

        public enum UnitType
        {
            SOLAR, WIND_OFFSHORE, WIND_ONSHORE, CONSTANT, THERMAL
        }

        public enum TYNDP_Scenario_Type
        {
            DistributedEnergy, GlobalAmbition, NationalTrends 
        }

        public enum VREType
        {
            Solar, WindOnshore, WindOffshore
        }

    }
}
