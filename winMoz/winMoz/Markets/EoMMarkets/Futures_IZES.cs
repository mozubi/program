﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;
using winMoz.Markets;

namespace winMoz.Markets.EoMMarkets
{
    /// <summary>
    /// Futures-Marktklasse zur Durchführung von monatlichen Termingeschäften erbt von der <see langword="abstract"/> <see cref="Market"/>-Klasse
    /// </summary>
    public class Futures_IZES : FuturesMarket
    {
        private List<Bid_DayAhead> underlyings = new List<Bid_DayAhead>();
        //private DayAheadMarket dayAhead;

        //public Futures_IZES(DayAheadMarket dayAheadMarket, DateTime startDate) : this(new MarketResultRepository(), new BidRepository(), dayAheadMarket, startDate) { }
        protected Futures_IZES() : base() { }
        public Futures_IZES(MarketAttributes attributes, int scenarioID = 0) 
            : base(attributes, scenarioID)
        {
            //this.dayAhead = dayAheadMarket;
            //this.dayAhead.Auction += DayAheadMarket_Auction;
        }

        #region Marktaktionen
        /// <summary>
        /// Implementierung der <see langword="abstract"/> <see cref="Market.ExecuteAuction"/>-Methode zur Ausführung der Auktionierung nach Sammlung aller Gebote 
        /// </summary>
        protected override void ExecuteAuction(out List<MarketResult> results)
        {
            results = new List<MarketResult>();
            DateTime EndOfDuration = EventTimeframe.TimeStamp.Add(EventTimeframe.Duration);
            var SegmentsAuction = this.BidsAuction.SelectMany(item => item.Segments).ToList().GroupBy(item => item.TimeStamp.Ticks, (el1, el2) => (el1, el2)).ToDictionary(item => item.el1, item => item.el2);
            for (DateTime time = EventTimeframe.TimeStamp; time < EndOfDuration; time = time.Add(EventTimeframe.Step))
            {
                //var BidSegmentsAuctionTime = this.BidsAuction.SelectMany(item => item.Segments).Where(item => item.TimeStamp.Ticks == time.Ticks).ToList();
                //var BidSegmentsAuctionTime = SegmentsAuction.SkipWhile(item => item.TimeStamp.Ticks < time.Ticks).ToList().TakeWhile(item => item.TimeStamp.Ticks == time.Ticks).ToList();
                var BidSegmentsAuctionTime = SegmentsAuction[time.Ticks];
                var BidSegmentsSell = BidSegmentsAuctionTime.Where(item => item.IsSell).ToList();
                var BidSegmentsPurchase = BidSegmentsAuctionTime.Where(item => item.IsBuy).ToList();
                var VolumePurchase = BidSegmentsPurchase.Select(item => item.VolumeMW).Sum();
                float VolumeSell = BidSegmentsSell.Select(item => item.VolumeMW).Sum();
                var OrderedBidListSell = BidSegmentsSell.OrderBy(item => item.Price).ToList();
                float AggVol = 0;
                float AggVolHighestPrice = 0;
                float HighestClearedPrice = 0;
                foreach (var item in OrderedBidListSell)
                {
                    var MaxVolumeToAdd = Math.Min(Math.Abs(VolumePurchase) - Math.Abs(AggVol), Math.Abs(item.VolumeMW));
                    if (item.Price == HighestClearedPrice)
                        AggVolHighestPrice += MaxVolumeToAdd;
                    else
                        AggVolHighestPrice = MaxVolumeToAdd;
                    HighestClearedPrice = item.Price;
                    AggVol += MaxVolumeToAdd;
                    if (AggVol == Math.Abs(VolumePurchase))
                        break;
                }
                float VolumeHighestClearedPrice = BidSegmentsSell.Where(seg => seg.Price == HighestClearedPrice).Select(seg => seg.VolumeMW).Sum();
                float ProRataVolumeSell = VolumeHighestClearedPrice != 0 ? AggVolHighestPrice / VolumeHighestClearedPrice : 0;
                results.Add(new FuturesResult(time, ShortCode.Futures.Hour(time), HighestClearedPrice, AggVol, proRataVolumePurchase: 1, ProRataVolumeSell));
            }
        }

        #endregion

        public override void SubmitBid(Bid bid)
        {
            base.SubmitBid(bid);
            //this.underlyings.AddRange(this.Bids.Cast<Bid_Future>().SelectMany(el => el.Underlyings));
        }

    }
}
