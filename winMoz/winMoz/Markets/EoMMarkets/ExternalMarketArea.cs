﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Information;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Data.Access.DTO;
using winMoz.Assets.MCC;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Bids.EoMMarkets;
using winMoz.Markets.Elements;
using winMoz.Helper;

namespace winMoz.Markets.EoMMarkets
{
    /// <summary>
    /// Klasse zur Erzeugung der Merit Order der externen Märkte
    /// </summary>
    class ExternalMarketArea : MarketAreaUP
    {
        #region Variablen
        /// <summary>
        /// Liste der Kraftwerke
        /// </summary>
        List<_MCC_GenerationUnit> Units { get; set; }
        /// <summary>
        /// Netzlast für die externen Märkte
        /// </summary>
        MCC_Load_Repository Load { get; set; }
        /// <summary>
        /// Lastverschiebung und Verschiebung 
        /// durch im Vergleich zur Kapazität reduzierten Produktion
        /// der variablen Erneuerbaren
        /// </summary>
        decimal LoadDiff { get; set; }
        /// <summary>
        /// Liste der Lastverschiebung und Verschiebung 
        /// durch im Vergleich zur Kapazität reduzierten Produktion
        /// der variablen Erneuerbaren
        /// </summary>
        Dictionary<DateTime, decimal> LoadDiffList { get; set; }
        /// <summary>
        /// Repository für die Parameter der Kraftwerke.
        /// </summary>
        Unit_Parameters_Repository Unit_Parameters { get; set; }
        /// <summary>
        /// Basisliste für Nachfrage (ohne Gebote der thermischen Kraftwerke, da sich diese mit den Commodities ändern)
        /// </summary>
        protected List<BidTupel> LstCurveSellBase { get; set; }
        /// <summary>
        /// Basisliste für Angebot (ohne Gebote der thermischen Kraftwerke, da sich diese mit den Commodities ändern)
        /// </summary>
        protected List<BidTupel> LstCurvePurchaseBase { get; set; }
        #endregion

        #region Initialisierung
        /// <summary>
        /// Klasse zur Erzeugung der Merit Order der externen Märkte
        /// </summary>
        /// <param name="thermalUnits"></param>
        /// <param name="vreSolar"></param>
        /// <param name="vreWindOffshore"></param>
        /// <param name="vreWindOnshore"></param>
        /// <param name="constantUnit"></param>
        /// <param name="priceUpperBound"></param>
        /// <param name="priceLowerBound"></param>
        /// <param name="MA_Code"></param>
        /// <param name="scenario"></param>
        /// <param name="ts"></param>
        public ExternalMarketArea(
            MarketAreaEnum ma_Code,
            (Int64 LoadScaleID, Int64 LoadValID, Int64 LoadValYear, Int64 GenParaID, Int64 CapaID, Int64 tyndp_Forecast_Year, Int64 tyndp_Year, EnumCollection_EOMMarkets.TYNDP_Scenario_Type tyndp_Scenario, Int64 tyndp_Climate_Year) scenario, Market coupledMarket, DefaultPrices default_Prices) : base(ma_Code, coupledMarket, default_Prices) 
        {
            Units = new List<_MCC_GenerationUnit>();
            MA_Code = ma_Code;
            Load = new MCC_Load_Repository(scenario.LoadScaleID, ma_Code,  scenario.LoadValID, scenario.LoadValYear);
            TS = new DateTime(this.AuctionTimeframe.TimeStamp.Year, this.AuctionTimeframe.TimeStamp.Month, this.AuctionTimeframe.TimeStamp.Day);
            LoadDiffList = new Dictionary<DateTime, decimal>();
            Unit_Parameters = new Unit_Parameters_Repository(scenario.GenParaID, scenario.CapaID, MA_Code, scenario.tyndp_Forecast_Year, scenario.tyndp_Year, scenario.tyndp_Scenario, scenario.tyndp_Climate_Year);
            Capacities_Dto Capacities = Unit_Parameters.GetCapacities();
            //TODO: Herausfinden, warum ich diese hier brauche.
            CreateUnits(Capacities, (int)scenario.tyndp_Forecast_Year);
            LstCurvePurchaseBase = new List<BidTupel>();
            LstCurveSellBase = new List<BidTupel>();
        }
        #endregion

        #region Angebots- und Nachfragekurve
        /// <summary>
        /// Erstellt Angebots- und Nachfragekurve
        /// </summary>
        /// <param name="AuctionTime"></param>
        public void CreateBidsPurchaseSell(DateTime AuctionTime)
        {
            //Leeren der Liste Angebots- und Nachfrageliste
            DeleteBidsPurchaseSell();
            //Wenn Commodity sich geändert hat, nur thermische Kraftwerke ergänzen
            if (LstCurvePurchaseBase.Count() > 0 && LstCurveSellBase.Count() > 0)
            {
                //Basislisten nutzen
                LstCurvePurchase = new List<BidTupel>(LstCurvePurchaseBase);
                LstCurveSell = new List<BidTupel>(LstCurveSellBase);
            }
            else
            {
                //Durchschnittslast
                AddBid(UB_Price, -(decimal)Load.GetAvgLoad());
                //Gebot der nicht thermischen Kraftwerke hinzufügen
                Units.Where(item => item.Type != EnumCollection_EOMMarkets.UnitType.THERMAL).ToList().ForEach(unit => AddBids(unit.GetBids(AuctionTime)));
                //Rand Gebote
                AddBid(UB_Price, (decimal)Load.GetMaxLoad());
                AddBid(LB_Price, -(decimal)Load.GetMaxLoad());
                //Basislisten ohne thermische Kraftwerke erzeugen
                LstCurvePurchaseBase = new List<BidTupel>(LstCurvePurchase);
                LstCurveSellBase = new List<BidTupel>(LstCurveSell);             
            }
            //Gebote der thermischen Kraftwerke hinzufügen
            Units.Where(item => item.Type == EnumCollection_EOMMarkets.UnitType.THERMAL).ToList().ForEach(unit => AddBids(unit.GetBids(AuctionTime)));
        }
        #endregion

        #region Lastverschiebung
        /// <summary>
        /// Lastverschiebung und Anpassung der Erneuerbaren-Stromproduktion für einen Tag berechnen
        /// </summary>
        /// <param name="Day"></param>
        public void UpdateLoadAndVREDiff(DateTime Day)
        {
            var LoadDiffH = Load.GetLoadDiff(Day).OrderBy(item => item.timeStamp);
            var SolarRadiationH = ((MCC_SolarUnit)Units.Where(unit => unit.Type == EnumCollection_EOMMarkets.UnitType.SOLAR).FirstOrDefault()).GetReducedVolume(Day, MA_Code).OrderBy(item => item.TimeStamp);

            var WindSpeedOffshoreH = ((MCC_WindUnit)Units.Where(unit => unit.Type == EnumCollection_EOMMarkets.UnitType.WIND_OFFSHORE).FirstOrDefault()).GetReducedVolume(Day, MA_Code).OrderBy(item => item.TimeStamp);
            var WindSpeedOnshoreH = ((MCC_WindUnit)Units.Where(unit => unit.Type == EnumCollection_EOMMarkets.UnitType.WIND_ONSHORE).FirstOrDefault()).GetReducedVolume(Day, MA_Code).OrderBy(item => item.TimeStamp);

            LoadDiffList = LoadDiffH
                .Zip(SolarRadiationH, (Load1, Solar1) => (Load1.timeStamp, Load1.LoadValue + (decimal)Solar1.Volume)).ToList()
                .Zip(WindSpeedOffshoreH, (Load1, Solar1) => (Load1.timeStamp, Load1.Item2 + (decimal)Solar1.Volume)).ToList()
                .Zip(WindSpeedOnshoreH, (Load1, Solar1) => (Load1.timeStamp, Load1.Item2 + (decimal)Solar1.Volume)).ToDictionary(item => item.timeStamp, item => item.Item2);
        }
        #endregion

        #region Merit Order
        /// <summary>
        /// Berechnung der Lastverschiebung und Anpassung der Erneuerbaren
        /// </summary>
        public override MA_Auction SetInitialResultVolumePriceOfCoupledMeritOrder(MA_Auction coupledMeritOrder)
        {
            coupledMeritOrder = SetVolumeWithoutLoadAndVREDiffOfMeritOrder(coupledMeritOrder);
            coupledMeritOrder = SetPriceWithoutLoadAndVREDiffOfMeritOrder(coupledMeritOrder);
            coupledMeritOrder = SetVolumeWithLoadAndVREDiffOfMeritOrder(coupledMeritOrder);
            coupledMeritOrder = SetPriceWithLoadAndVREDiffOfMeritOrder(coupledMeritOrder);
            return base.SetInitialResultVolumePriceOfCoupledMeritOrder(coupledMeritOrder);
        }

        public MA_Auction SetVolumeWithoutLoadAndVREDiffOfMeritOrder(MA_Auction coupledMeritOrder)
        {
            if (coupledMeritOrder.V_WithoutLoadDiff == default)
                coupledMeritOrder.V_WithoutLoadDiff = coupledMeritOrder.V_Start;
            return coupledMeritOrder;
        }

        public MA_Auction SetVolumeWithLoadAndVREDiffOfMeritOrder(MA_Auction coupledMeritOrder)
        {
            coupledMeritOrder.MaxEx = coupledMeritOrder.MaxExSave.Where(item => item.WindProgFrom <= coupledMeritOrder.WindProgDE).Last().Capacity;
            coupledMeritOrder.MaxIm = coupledMeritOrder.MaxImSave.Where(item => item.WindProgFrom <= coupledMeritOrder.WindProgDE).Last().Capacity;
            coupledMeritOrder.V_Start = coupledMeritOrder.V_WithoutLoadDiff + LoadDiffList[TS];
            return coupledMeritOrder;
        }


        public MA_Auction SetPriceWithoutLoadAndVREDiffOfMeritOrder(MA_Auction coupledMeritOrder)
        {
            if (coupledMeritOrder.P_WithoutLoadDiff == default)
                coupledMeritOrder.P_WithoutLoadDiff = coupledMeritOrder.P_Start;
            return coupledMeritOrder;
        }

        public MA_Auction SetPriceWithLoadAndVREDiffOfMeritOrder(MA_Auction coupledMeritOrder)
        {
            coupledMeritOrder.P_Start = coupledMeritOrder.V_Start < 0 ? LB_Price : //unlimitiertes Gebot nach unten
                coupledMeritOrder.V_Start > coupledMeritOrder.MaxOffer ? UB_Price : //unlimitiertes Gebot nach oben
                coupledMeritOrder.CalcPrice(coupledMeritOrder.V_Start);
            return coupledMeritOrder;
        }

        /// <summary>
        /// Erzeugung der Merit Order (Neuaufbau bei geänderten Commodity-Preisen)
        /// </summary>
        /// <param name="AuctionTime"></param>
        public override MA_Auction CreateMeritOrder(MA_Auction coupledMeritOrder, DateTime time)
        {
            MarketResultUP result = CalculateMarketResult(time);
            return InitMeritOrder(coupledMeritOrder, result);
        }

        public MarketResultUP CalculateMarketResult(DateTime time)
        {
            TS = time;
            CreateBidsPurchaseSell(time);
            return GetMarketResult();
        }

        public override void UpdateMeritOrder(MA_Auction coupledMeritOrder, DateTime time)
        {
            if (!LoadDiffList.Keys.Contains(time))
                UpdateLoadAndVREDiff(time);
            if (Commodities.HasChanged(time, TS) || coupledMeritOrder.IsMeritOrderEmpty())
                CreateMeritOrder(coupledMeritOrder, time);
            else
                UpdateVolumePriceOfMeritOrder(coupledMeritOrder, time);
            SetWindProgDE(coupledMeritOrder);
        }

        public MA_Auction UpdateVolumePriceOfMeritOrder(MA_Auction coupledMeritOrder, DateTime auctionTime)
        {
            if (!LoadDiffList.Keys.Contains(auctionTime))
                UpdateLoadAndVREDiff(auctionTime);
            coupledMeritOrder.TS = auctionTime;
            TS = auctionTime;
            coupledMeritOrder = SetInitialResultVolumePriceOfCoupledMeritOrder(coupledMeritOrder);
            coupledMeritOrder = SetMeritOrderLimits(coupledMeritOrder);
            return coupledMeritOrder;
        }

        public List<_MCC_GenerationUnit> CreateUnits(Capacities_Dto Capacities, int tyndp_Forecast_Year)
        {
            AddThermalUnits(Capacities);
            AddConstantUnit(Capacities);
            AddVREUnits(Capacities, tyndp_Forecast_Year);
            return Units;
        }

        /// <summary>
        /// Klassen zur Gebotserzeugung der thermische Kraftwerke für die Marktkoppelung erzeugen 
        /// </summary>
        /// <param name="capacities"></param>
        /// <param name="genPara"></param>
        /// <returns></returns>
        private void AddThermalUnits(Capacities_Dto capacities)
        {
            List<MCC_ThermalUnit> ThermalUnits = new List<MCC_ThermalUnit>();
            //Nuclear
            if (capacities.Nuclear != -1)
            {
                MCC_ThermalUnit Unit = new MCC_ThermalUnit(Unit_Parameters.GetThermalUnitParameters(EnumCollection_EOMMarkets.ThermalEnum.Nuclear),
                    capacities.Nuclear,
                    winMoz.Information.Commodities.Commodity.Uranium);
                ThermalUnits.Add(Unit);
            }
            //Hard coal
            if (capacities.Fossil_Hard_coal != -1)
            {
                MCC_ThermalUnit Unit = new MCC_ThermalUnit(Unit_Parameters.GetThermalUnitParameters(EnumCollection_EOMMarkets.ThermalEnum.HardCoal),
                    capacities.Fossil_Hard_coal,
                    winMoz.Information.Commodities.Commodity.Hard_Coal);
                ThermalUnits.Add(Unit);
            }
            //Lignite
            if (capacities.Fossil_Brown_Coal_Lignite != -1)
            {
                MCC_ThermalUnit Unit = new MCC_ThermalUnit(Unit_Parameters.GetThermalUnitParameters(EnumCollection_EOMMarkets.ThermalEnum.Lignite),
                    capacities.Fossil_Brown_Coal_Lignite,
                    winMoz.Information.Commodities.Commodity.Lignite);
                ThermalUnits.Add(Unit);
            }
            //Gas
            if (capacities.Fossil_gas != -1)
            {
                MCC_ThermalUnit Unit = new MCC_ThermalUnit(Unit_Parameters.GetThermalUnitParameters(EnumCollection_EOMMarkets.ThermalEnum.Gas),
                    capacities.Fossil_gas,
                    winMoz.Information.Commodities.Commodity.Natural_Gas);
                ThermalUnits.Add(Unit);
            }
            //Oil
            if (capacities.Fossil_Oil != -1)
            {
                MCC_ThermalUnit Unit = new MCC_ThermalUnit(Unit_Parameters.GetThermalUnitParameters(EnumCollection_EOMMarkets.ThermalEnum.Oil),
                    capacities.Fossil_Oil,
                    winMoz.Information.Commodities.Commodity.Oil);
                ThermalUnits.Add(Unit);
            }
            Units.AddRange(ThermalUnits);
        }

        /// <summary>
        /// Klasse zur Gebotsabgabe der nicht-variable nicht-fossilen Kraftwerke erzeugen 
        /// </summary>
        /// <param name="capacities"></param>
        /// <param name="genPara"></param>
        /// <returns></returns>
        private void AddConstantUnit(Capacities_Dto capacities)
        {
            float ActiveCapacity = 0;
            if (capacities.Biomass != -1) ActiveCapacity += Unit_Parameters.GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum.Biomass) * capacities.Biomass;
            if (capacities.Geothermal != -1) ActiveCapacity += Unit_Parameters.GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum.Biomass) * capacities.Geothermal;
            if (capacities.Hydro_Run_of_river_and_poundage != -1) ActiveCapacity += Unit_Parameters.GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum.Hydro) * capacities.Hydro_Run_of_river_and_poundage;
            if (capacities.Hydro_Pumped_Storage != -1) ActiveCapacity += Unit_Parameters.GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum.Hydro) * capacities.Hydro_Pumped_Storage;
            if (capacities.Hydro_Water_Reservoir != -1) ActiveCapacity += Unit_Parameters.GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum.Hydro) * capacities.Hydro_Water_Reservoir;
            if (capacities.Marine != -1) ActiveCapacity += Unit_Parameters.GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum.Marine) * capacities.Marine;
            if (capacities.Waste != -1) ActiveCapacity += Unit_Parameters.GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum.Waste) * capacities.Waste;
            if (capacities.Other_renewable != -1) ActiveCapacity += capacities.Other_renewable;
            if (capacities.Other != -1) ActiveCapacity += capacities.Other;
            if (capacities.Fossil_Peat != -1) ActiveCapacity += capacities.Fossil_Peat;
            if (capacities.Fossil_Oil_shale != -1) ActiveCapacity += capacities.Fossil_Oil_shale;
            MCC_ConstantUnit Unit = new MCC_ConstantUnit(ActiveCapacity);
            Units.Add(Unit);
        }

        /// <summary>
        /// Klassen zur Gebotsabgabe der variablen erneuerbaren Kraftwerke 
        /// </summary>
        /// <param name="capacities"></param>
        /// <param name="genPara"></param>
        /// <returns></returns>
        private void AddVREUnits(Capacities_Dto capacities, int tyndp_Forecast_Year)
        {
            var Start = TS.AddDays(-1);
            var End = Start.AddYears(1);
            var Duration = End - Start;
            var SolarSimulationTime = Weather.GetSolar(Start, Duration, nuts2: MA_Code.ToString() == "IT" ? "FR" : MA_Code.ToString() == "SE" ? "DK" : MA_Code.ToString(), isExtended: false).Select(solar => (solar.TimeStamp, solar.RadDiffHor + solar.RadDirHor)).ToList();
            var WindSimulationTime = Weather.GetWindspeed(Start, Duration, nuts2: MA_Code.ToString() == "IT" ? "FR" : MA_Code.ToString() == "SE" ? "DK" : MA_Code.ToString(), isExtended: false);
            WindDtoRepository Repo = new WindDtoRepository();
            WindDto Wind = Repo.FirstOrDefault(item => item.Id == (Int64)2683);
            //WindDtoContext WindPlantContext = new WindDtoContext();
            //WindDto Wind = WindPlantContext.WindPlants.Find((Int64)2683);
            var PowerCurve = Wind.PowerCurve.ToList(); //Windleistungskurve der Enercon E-115/3000
            //Solar
            decimal VBH_TYNDP = (decimal)Unit_Parameters.GetVBH_VRE_TYNDP(EnumCollection_EOMMarkets.VREType.Solar);
            MCC_SolarUnit VRESolar = new MCC_SolarUnit(capacities.Solar, VBH_TYNDP, tyndp_Forecast_Year, MA_Code, SolarSimulationTime);
            //Wind Offshore
            VBH_TYNDP = (decimal)Unit_Parameters.GetVBH_VRE_TYNDP(EnumCollection_EOMMarkets.VREType.WindOnshore);
            MCC_WindUnit VREWindOffshore = new MCC_WindUnit(capacities.Wind_Offshore, VBH_TYNDP, tyndp_Forecast_Year, PowerCurve, EnumCollection_EOMMarkets.UnitType.WIND_OFFSHORE, MA_Code, WindSimulationTime);
            //Wind Onshore
            VBH_TYNDP = (decimal)Unit_Parameters.GetVBH_VRE_TYNDP(EnumCollection_EOMMarkets.VREType.WindOffshore);
            MCC_WindUnit VREWindOnshore = new MCC_WindUnit(capacities.Wind_Onshore, VBH_TYNDP, tyndp_Forecast_Year, PowerCurve, EnumCollection_EOMMarkets.UnitType.WIND_ONSHORE, MA_Code, WindSimulationTime);
            List<_MCC_GenerationUnit> VREUnits = new List <_MCC_GenerationUnit> { VRESolar, VREWindOffshore, VREWindOnshore };
            Units.AddRange(VREUnits);
        }

        #endregion
    }
}
