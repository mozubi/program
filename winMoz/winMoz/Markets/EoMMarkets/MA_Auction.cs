﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Markets.Bids.EoMMarkets;
using winMoz.Markets.Elements;

namespace winMoz.Markets.EoMMarkets
{
    /// <summary>
    /// Klasse der Marktgebiete für die Marktkopplung
    /// Preise und Volumen werden aus deterministischen Gründen (ALG-Ende) auf 3 Stellen gerundet.
    /// </summary>
    /// 
    public class MA_Auction 
    {
        #region Variablen
        /// <summary>
        /// Shortcode des Landes (Kennung des Marktgebietes)
        /// </summary>
        public MarketAreaEnum MA_Code { get; set; }
        /// <summary>
        /// Merit-Order-Liste
        /// </summary>
        public List<BidTupel> MeritOrder { get; set; }
        /// <summary>
        /// Nachfrage ohne Marktkoppelung und Lasdtverschiebung
        /// </summary>
        public decimal V_Start { get; set; }
        /// <summary>
        /// Nachfrage ohne Marktkopplung mit Lastverschiebung
        /// </summary>
        public decimal V_WithoutLoadDiff { get; set; }
        /// <summary>
        /// Nachfrage mit Markkopplung
        /// </summary>
        public decimal V_Result { get; set; }
        /// <summary>
        /// Preis ohne Marktkoppelung und Lastverschiebung
        /// </summary>
        public decimal P_Start { get; set; }
        /// <summary>
        /// Preis ohne Marktkopplung mit Lastverschiebung
        /// </summary>
        public decimal P_WithoutLoadDiff { get; set; }
        /// <summary>
        /// Preis mit Marktkoppelung und Blockgeboten
        /// </summary>
        public decimal P_Result { get; set; }
        /// <summary>
        /// Maximalpreis im Markt
        /// </summary>
        public decimal UB_Price { get; set; }
        /// <summary>
        /// Minimalpreis im Markt
        /// </summary>
        public decimal LB_Price { get; set; }
        /// <summary>
        /// Maximale Importkapazität
        /// </summary>
        public decimal MaxIm { get; set; }
        /// <summary>
        /// Maximale Importkapazität
        /// </summary>
        public List<(decimal WindProgFrom, decimal Capacity)> MaxImSave { get; set; }
        /// <summary>
        /// Maximal mögliche Reduzierung der Produktion im Marktgebiet
        /// </summary>
        public decimal MaxReduceCapacity { get; set; }
        /// <summary>
        /// Maximale Exportkapazität
        /// </summary>
        public decimal MaxEx { get; set; }
        /// <summary>
        /// Maximale Exportkapazität
        /// </summary>
        public decimal WindProgDE { get; set; }
        /// <summary>
        /// Maximale Exportkapazität
        /// </summary>
        public List<(decimal WindProgFrom, decimal Capacity)> MaxExSave { get; set; }
        /// <summary>
        /// Maximal mögliche Steigerung der Produktion im Marktgebiet
        /// </summary>
        public decimal MaxIncreaseCapacity { get; set; }
        /// <summary>
        /// Maximal mögliches Gebot für das Marktgebiet
        /// </summary>
        public decimal MaxOffer { get; set; }
        /// <summary>
        /// Real gehandeltes Startvolumen
        /// </summary>
        public decimal Volume_Market_Start  { get; set; }
        /// <summary>
        /// Auktionszeitpunkt
        /// </summary>
        public DateTime TS { get; set; }

        public decimal StepBidVolume { get; } = 0.000001m;

        public decimal StepBidPriceDiff { get; } = 0.000001m;

        public MarketAreaUP MarketHandler { get; set; }

        public MarketResultUP InitialResult { get; set; }

        public decimal VolumeLastPurchasePart { get; private set; }
        public decimal VolumeLastSellPart { get; private set; }


        public decimal VolumeUpperLimitUnlimited => VolumeUpperLimitPurchaseUnlimited + VolumeUpperLimitSellUnlimited;
        public decimal VolumeLowerLimitUnlimited => VolumeLowerLimitPurchaseUnlimited + VolumeLowerLimitSellUnlimited;
        public decimal VolumeUpperLimitSellUnlimited { get; set; }
        public decimal VolumeUpperLimitPurchaseUnlimited { get; set; }

        public decimal VolumeLowerLimitSellUnlimited { get; set; }
        public decimal VolumeLowerLimitPurchaseUnlimited { get; set; }

        //public bool AuctionFailed { get; private set; } = false;

        public bool AuctionFailed() => IsMeritOrderEmpty() || V_Result > MaxOffer || V_Result < 0;

        public bool NoFurtherExchange { get; set; }

        public decimal VerschiebeVolumen { get; set; }
        #endregion

        public void Reset()
        {
            V_Result = V_Start;
            P_Result = P_Start;
            ResetImportExport();
        }

        public void ResetImportExport()
        {
            MaxIm = MaxImSave.Where(item => item.WindProgFrom <= this.WindProgDE).Last().Capacity;
            MaxEx = MaxExSave.Where(item => item.WindProgFrom <= this.WindProgDE).Last().Capacity;
            CalcMaxOffer();
            SetIncreaseReduceCapacity();
            NoFurtherExchange = false;
            VerschiebeVolumen = 0;
        }

        //public void SetAuctionFailed() => AuctionFailed = true;
        //public void SetAuctionDoesNotFail() => AuctionFailed = false;

        #region Initialisierung und Zerstörung

        /// <summary>
        /// Klasse der Marktgebiete für die Marktkopplung
        /// Preise und Volumen werden aus deterministischen Gründen (ALG-Ende) auf 3 Stellen gerundet.
        /// </summary>
        /// <param name="AuctionDate"></param>
        /// <param name="country"></param>
        /// <param name="ub_Price"></param>
        /// <param name="lb_Price"></param>
        public MA_Auction(DateTime AuctionDate, MarketAreaUP marketHandler)
        {
            MeritOrder = new List<BidTupel>();
            TS = new DateTime();
            TS = AuctionDate;
            MA_Code = marketHandler.MA_Code;
            UB_Price = marketHandler.UB_Price;
            LB_Price = marketHandler.LB_Price;
            MarketHandler = marketHandler;
            InitialResult = new MarketResultUP(0, 0, 0, 0, 0, 0, 0);
            NoFurtherExchange = false;
            VerschiebeVolumen = 0;
            MaxExSave = new List<(decimal WindProgFrom, decimal Capacity)>();
            MaxImSave = new List<(decimal WindProgFrom, decimal Capacity)>();
        }

        /// <summary>
        /// Klasse der Marktgebiete für die Marktkopplung
        /// Preise und Volumen werden aus deterministischen Gründen (ALG-Ende) auf 3 Stellen gerundet.
        /// </summary>
        /// <param name="AuctionDate"></param>
        /// <param name="MA"></param>
        public MA_Auction(DateTime AuctionDate, MA_Auction MA)
        {
            MA_Code = MA.MA_Code;
            MeritOrder = MA.MeritOrder;
            V_Start = MA.V_Start;
            P_Start = MA.P_Start;
            V_Result = MA.V_Result;
            P_Result = MA.P_Result;
            MaxIm = MA.MaxIm;
            MaxEx = MA.MaxEx;
            MaxReduceCapacity = MA.MaxReduceCapacity;
            MaxIncreaseCapacity = MA.MaxIncreaseCapacity;
            MaxOffer = MA.MaxOffer;
            UB_Price = MA.UB_Price;
            LB_Price = MA.LB_Price;
            TS = new DateTime();
            TS = AuctionDate;
            InitialResult = new MarketResultUP(0, 0, 0, 0, 0, 0, 0);
            NoFurtherExchange = MA.NoFurtherExchange;
            VerschiebeVolumen = MA.VerschiebeVolumen;
            MaxExSave = MA.MaxExSave;
            MaxImSave = MA.MaxImSave;
        }

        /// <summary>
        /// Leeren der Merit Order
        /// </summary>
        public void DeleteMeritOrder() => MeritOrder.Clear();

        #endregion

        #region Änderungen an der MeritOrder
        ///<summary>
        ///Fügt Tupel der MeritOrder hinzu
        ///Gebote mit 0 Menge (kleiner 0.000000001 werden ignoriert, da ansonsten ggf. Div0 Fehler.
        ///Muss GROESSER sein, als STEP BID ind Market Uniform Pricing...
        ///
        /// </summary>
        public void AddBidTupel(decimal fPrice, decimal fVolume, bool isPurchase, decimal fVolumePurchase)
        {
            if (fVolume > StepBidVolume)
            {
                BidTupel TmpBid;

                fPrice = Math.Round(fPrice, 3);
                TmpBid = new BidTupel(fPrice, fVolume, isPurchase, fVolumePurchase);
                MeritOrder.Add(TmpBid);
            }
        }

        public bool IsMeritOrderEmpty() => MeritOrder.Count() == 0;

        public void UpdateStartVolumePrice(decimal volume, decimal price)
        {
            V_Result = V_Start = volume;
            P_Result = P_Start = price;
        }

        /// <summary>
        /// Berechne Preis nach Marktkoppelung und Blockgeboten
        /// </summary>
        public void CalcPriceResult() => P_Result = CalcPrice(V_Result);


        ///<summary>
        ///Baut Die Meritorder auf:
        ///1. Sortieren
        ///2. Dubletten aggregieren
        ///3. Volumina agregieren 
        ///</summary>
        public void BuildCoupledMeritOrder()
        {
            //StepBids löschen, sortieren und Dubletten zusammenfassen
            MeritOrder = EoM_Extensions.MeritOrder_Couple_Douplets(
                EoM_Extensions.OrderByListsPrice(
                    EoM_Extensions.DeleteStepBids(MeritOrder)));

            if (MeritOrder.Count() > 0)
            {
                //StepBids einfügen
                MeritOrder = EoM_Extensions.InsertReducedStepBidsInFrontOf(EoM_Extensions.InsertNullBid(MeritOrder), StepBidVolume, StepBidPriceDiff);

                //Volumina aggregieren
                MeritOrder = EoM_Extensions.AggregateVolumesUP(MeritOrder, StepBidVolume);
            }

            CalcMaxOffer();

        }

        #endregion

        #region Preis- und Mengenermittlung


        public void SetIncreaseReduceCapacity()
        {
            MaxIncreaseCapacity = MaxOffer - V_Result;
            MaxReduceCapacity = V_Result;
        }

        public void CalcMaxOffer() => MaxOffer = MeritOrder.Count() == 0 ? 0 : MeritOrder[MeritOrder.Count() - 1].AggVol; //CalcVolume(UB_Price);

        /// <summary>
        /// Gibt den Preis zum angegebenen Volumen aus
        /// </summary>
        /// <param name="Volume"></param>
        /// <returns></returns>
        public decimal CalcPrice(decimal Volume)
        {
            if (MeritOrder.Count > 0)
            {
                /// <summary>
                /// Funktion berechnet den Preis auf der Meritorder bei gegebener Menge,
                /// Zwischen zwei Preispaaren wird linear Interpoliert.
                /// </summary>

                decimal Price, mG, bG;

                if (Volume > MeritOrder[MeritOrder.Count() - 1].AggVol) return UB_Price;
                if (Volume < 0) return LB_Price;

                (int Tupel1, int Tupel2, bool bolInFirstBidTupel) = EoM_Extensions.MeritOrder_FindBidTupels(MeritOrder.Select(item => item.AggVol).ToList(), Volume);

                //Bestimme Preis, der liegt zwischen den Indexen Tupel1 und Tupel2
                //Teile Geradengleichung
                //Interpolarisation
                (mG, bG) = !bolInFirstBidTupel ? EoM_Extensions.GetLinearParameter(MeritOrder, Tupel1, Tupel2) : (0, MeritOrder[0].Price);

                Price = GetYFromXLinear(x: Volume, slope: mG, y_intercept: bG);
                return Math.Round(Price, 12);
            }
            else
                return 0;
        }

        /// <summary>
        /// Gibt das Volumen zum angegebenen Preis aus.
        /// </summary>
        /// <param name="Price"></param>
        /// <returns></returns>
        public decimal CalcVolume(decimal Price)
        {
            if (MeritOrder.Count > 0)
            {
                decimal Volume, mG, bG;

                if (MeritOrder[0].Price > Price) return 0m;
                if (MeritOrder[MeritOrder.Count() - 1].Price < Price) return MaxOffer;

                (int Tupel1, int Tupel2, bool bolInFirstBidTupel) = EoM_Extensions.MeritOrder_FindBidTupels(MeritOrder.Select(item => item.Price).ToList(), Price);

                //Bestimme Preis, der liegt zwischen den Indexen Tupel1 und Tupel2
                //Teile Geradengleichung
                //Interpolarisation
                (mG, bG) = EoM_Extensions.GetLinearParameter(MeritOrder, Tupel1, Tupel2);

                Volume = GetXFromYLinear(y: Price, slope: mG, y_intercept: bG);

                return Math.Round(Volume, 12);
            }
            else
                return 0;
        }

        /// <summary>
        /// Berechnet für das letzte Gebot den Anteil, des ANgebots und der Nachfrage. 
        /// Negative Werte geben die Nachfrage an. 
        /// Positive Werte bestimmen das Angebot.
        /// </summary>
        /// <param name="Price"></param>
        /// <returns></returns>
        public void CalcVolumePurchaseSellOfLastMeritOrderAggBid()
        {
            decimal Price = P_Result;
            //To-Do: Überprüfen, ob kleine Werte der Ausgabeparameter verhindert werden sollten.

            decimal Volume, VolumeLastPurchase, VolumeLastSell, mG, bG;

            if (MeritOrder[0].Price > Price)
            {
                VolumeLastPurchase = Math.Min(V_Result, VolumeLowerLimitPurchaseUnlimited);
                VolumeLastSell = Math.Min(VolumeLowerLimitUnlimited + V_Result, VolumeLowerLimitSellUnlimited);

                VolumeLastPurchasePart = Math.Round(GetProportion(VolumeLastPurchase, VolumeLowerLimitPurchaseUnlimited), 5);
                VolumeLastSellPart = Math.Round(GetProportion(VolumeLastSell, VolumeLowerLimitSellUnlimited), 5);
            }
            else if (MeritOrder[MeritOrder.Count() - 1].Price < Price)
            {
                VolumeLastPurchase = Math.Min(VolumeUpperLimitUnlimited - (V_Result - MaxOffer), VolumeUpperLimitPurchaseUnlimited);
                VolumeLastSell = Math.Min(V_Result - MaxOffer, VolumeUpperLimitSellUnlimited);

                VolumeLastPurchasePart = Math.Round(GetProportion(VolumeLastPurchase, VolumeUpperLimitPurchaseUnlimited), 5);
                VolumeLastSellPart = Math.Round(GetProportion(VolumeLastSell, VolumeUpperLimitSellUnlimited), 5);
            }
            else
            {
                (int Tupel1, int Tupel2, bool bolInFirstBidTupel) = EoM_Extensions.MeritOrder_FindBidTupels(MeritOrder.Select(item => item.Price).ToList(), Price);

                //Bestimme Preis, der liegt zwischen den Indexen Tupel1 und Tupel2
                //Teile Geradengleichung
                //Interpolarisation
                (mG, bG) = EoM_Extensions.GetLinearParameter(MeritOrder, Tupel1, Tupel2);

                Volume = GetXFromYLinear(Price, mG, bG);

                VolumeLastPurchase = Math.Min(MeritOrder[Tupel2].AggVol - Volume, MeritOrder[Tupel2].VolumePurchase);
                VolumeLastSell = Math.Min(Volume - MeritOrder[Tupel1].AggVol, MeritOrder[Tupel2].Volume - MeritOrder[Tupel2].VolumePurchase);

                VolumeLastPurchasePart = Math.Round(GetProportion(VolumeLastPurchase, MeritOrder[Tupel2].VolumePurchase), 5);
                VolumeLastSellPart = Math.Round(GetProportion(VolumeLastSell, MeritOrder[Tupel2].Volume - MeritOrder[Tupel2].VolumePurchase), 5);
            }
        }

        public void CorrectVolumePriceStepBids()
        {
            (int Tupel1, int Tupel2, bool BolInFirstTupel) = EoM_Extensions.MeritOrder_FindBidTupels(MeritOrder.Select(item => item.Price).ToList(), P_Result);
            if (MeritOrder[Tupel2].AggVol - MeritOrder[Tupel1].AggVol == StepBidVolume)
            {
                V_Result = MeritOrder[Tupel1].AggVol;
                P_Result = MeritOrder[Tupel1].Price;
            }
        }

        //TODO: Überführen in Helper-Klasse
        public decimal GetXFromYLinear(decimal y, decimal slope, decimal y_intercept) => (y - y_intercept) / slope;

        public decimal GetYFromXLinear(decimal x, decimal slope, decimal y_intercept) => slope * x + y_intercept;

        public decimal GetProportion(decimal partition, decimal total) => total != 0 ? partition / total : 0;
        #endregion
    }
}
