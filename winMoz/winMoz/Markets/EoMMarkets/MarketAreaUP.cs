﻿using System;
using System.Collections.Generic;
using winMoz.Helper;
using winMoz.Markets.Bids.EoMMarkets;
using winMoz.Markets.Elements;
using System.Linq;
using winMoz.Simulation;
using winMoz.Tools.Viz;

namespace winMoz.Markets.EoMMarkets
{
    ///<summary>
    /// Marktklasse für Uniform Pricing
    /// Maximale Preise: -99999999 bis 99999999
    /// Im Fall eines Gebotsüberschusses sind die Preise die Rückgabewerte für erfüllte, unlimitiere Gebote.
    /// Berücksichtigen in der Umsetzung des Gebotsverhaltens der Agenten.
    /// Erbt von der Klasse Marktgebiet: Nach Berechnung des Marktergebnisses muss 'BuildMarkgebiet' aufgerufen werden, 
    /// bevor der Markt im Rahmen der Marktkopplung genutzt werden kann.
    /// </summary>
    public abstract class MarketAreaUP 
    {
        #region Variablen
        /// <summary>
        /// Liste für Nachfrage
        /// </summary>
        protected List<BidTupel> LstCurveSell { get; set; }
        /// <summary>
        /// List für Angebot
        /// </summary>
        protected List<BidTupel> LstCurvePurchase { get; set; }
        /// <summary>
        /// Maximalpreis
        /// </summary>
        public decimal UB_Price => (decimal)CoupledMarket.Attributes.Upper_Price_Bound;
        /// <summary>
        /// Minimalpreis
        /// </summary>
        public decimal LB_Price => (decimal)CoupledMarket.Attributes.Lower_Price_Bound;
        /// <summary>
        /// Merktmenge
        /// </summary>
        public decimal MarketVolume { get; set; }
        /// <summary>
        /// Marktpreis
        /// </summary>
        public decimal MarketPrice { get; set; }
        /// <summary>
        /// Erweiterung der Merit Order um unlimitierte Angebote mit Maximalpreis
        /// Diese Information wird benötigt, um das Volumen in der Merit Order richtig darstellen zu können, 
        /// bevor nachfolgend ein Korrekturterm, die Lastverschiebung und Verschiebung durch die 
        /// angepasste Erneuerbare-Stromproduktion angewendet werden. 
        /// </summary>
        decimal VolumeUnlimitedUpperPriceBound { get; set; }
        /// <summary>
        /// Shortcode des Landes (Kennung des Marktgebietes)
        /// </summary>
        public MarketAreaEnum MA_Code { get; set; }
        /// <summary>
        /// Auktionszeitpunkt
        /// </summary>
        public DateTime TS { get; set; }

        public MarketResultUP MarketUPResult { get; set; }

        //public enum MarketType { DayAhead, Intraday}

        //public MarketType MType { get; set; }

        public Market CoupledMarket { get; set; }

        protected (DateTime TimeStamp, TimeSpan Duration, TimeSpan Step) AuctionTimeframe => CoupledMarket.EventTimeframe;

        protected DefaultPrices Default_Prices;

        /// <summary>
        /// Windleistung für C-Funktion
        /// </summary>
        public Dictionary<DateTime, decimal> WindProgListDE { get; private set; }

      

        #endregion

        #region Initialisierung
        /// <summary>
        /// Marktklasse für Uniform Pricing
        /// </summary>
        /// <param name="priceUpperBound"></param>
        /// <param name="priceLowerBound"></param>
        /// <param name="MA_Code"></param>
        public MarketAreaUP(MarketAreaEnum ma_Code, Market coupledMarket, DefaultPrices default_Prices)
        {
            Default_Prices = default_Prices;
            CoupledMarket = coupledMarket;
            //SetEventTimeframe(coupledMarket.EventTimeframe);
            //MType = type;
            LstCurveSell = new List<BidTupel>();
            LstCurvePurchase = new List<BidTupel>();

            MA_Code = ma_Code;
            VolumeUnlimitedUpperPriceBound = 0;
            WindProgListDE = new Dictionary<DateTime, decimal>();

            //this.UB_Price = priceUpperBound;
            //this.LB_Price = priceLowerBound;

            AddBidsUnlimitedPrice();
        }

        public void SetWindProgListDE(Dictionary<DateTime, decimal> windProgDE)
        {
            WindProgListDE = windProgDE;
        }

        //public void SetEventTimeframe((DateTime TimeStamp, TimeSpan Duration, TimeSpan StepDuration) auctionTimeframe) => CoupledMarket.EventTimeFrame = auctionTimeframe;

        ///<summary>
        ///Initialisierungsfunktion der MeritOrder NACHDEM die PreisMengen-Paare hinzugefügt wurden.
        ///Override da Nachfrage aus dem Marktergebnis resultiert.
        ///Anpassung um Verschiebung MeritOrder bei Nutzung der Treppenfunktion (StepBids)
        /// </summary>
        public MA_Auction InitMeritOrder(MA_Auction coupledMeritOrder, MarketResultUP result)
        {
            coupledMeritOrder.Volume_Market_Start = result.ClearingVolume;
            coupledMeritOrder.VolumeUpperLimitPurchaseUnlimited = LstCurvePurchase.Where(item => item.Price == this.UB_Price).Select(item => item.Volume).Sum();
            coupledMeritOrder.VolumeUpperLimitSellUnlimited = LstCurveSell.Where(item => item.Price == this.UB_Price).Select(item => item.Volume).Sum();
            coupledMeritOrder.VolumeLowerLimitPurchaseUnlimited = LstCurvePurchase.Where(item => item.Price == this.LB_Price).Select(item => item.Volume).Sum();
            coupledMeritOrder.VolumeLowerLimitSellUnlimited = LstCurveSell.Where(item => item.Price == this.LB_Price).Select(item => item.Volume).Sum();
            coupledMeritOrder = AddBidsToCoupledMeritOrder(coupledMeritOrder, result);
            coupledMeritOrder.BuildCoupledMeritOrder();
            coupledMeritOrder = SetInitialVolumePriceOfCoupledMeritOrder(coupledMeritOrder, result);
            return coupledMeritOrder;
        }

        public abstract void UpdateMeritOrder(MA_Auction CoupledMeritOrder, DateTime time);


        public MA_Auction SetWindProgDE(MA_Auction coupledMeritOrder)
        {
            coupledMeritOrder.WindProgDE = WindProgListDE.Count() > 0 ?  WindProgListDE[TS] : 0;
            return coupledMeritOrder;
        }

        public MA_Auction SetInitialVolumePriceOfCoupledMeritOrder(MA_Auction coupledMeritOrder, MarketResultUP result)
        {
            coupledMeritOrder = SetStartVolumeOfMeritOrder(coupledMeritOrder, result);
            coupledMeritOrder = SetStartPriceOfMeritOrder(coupledMeritOrder);
            coupledMeritOrder = SetInitialResultVolumePriceOfCoupledMeritOrder(coupledMeritOrder);
            coupledMeritOrder = SetMeritOrderLimits(coupledMeritOrder);
            return coupledMeritOrder;
        }

        public virtual MA_Auction SetInitialResultVolumePriceOfCoupledMeritOrder(MA_Auction coupledMeritOrder)
        {
            coupledMeritOrder.V_Result = coupledMeritOrder.V_Start;
            coupledMeritOrder.P_Result = coupledMeritOrder.P_Start;
            return coupledMeritOrder;
        }

        public MA_Auction SetMeritOrderLimits(MA_Auction coupledMeritOrder)
        {
            coupledMeritOrder.SetIncreaseReduceCapacity();
            return coupledMeritOrder;
        }

        public MA_Auction SetStartVolumeOfMeritOrder(MA_Auction coupledMeritOrder, MarketResultUP result)
        {
            if (result != null)
            {
                coupledMeritOrder.V_Start = (MarketPrice == LB_Price ? 0 : //unlimitiertes Gebot nach unten
                    MarketPrice == UB_Price ? coupledMeritOrder.MaxOffer + coupledMeritOrder.VolumeUpperLimitUnlimited : //unlimitiertes Gebot nach oben
                    coupledMeritOrder.CalcVolume(Math.Round(MarketPrice, 4)))
                    + result.VolumeChangeMeritOrder;
                return coupledMeritOrder;
            }
            else
            {
                coupledMeritOrder.V_Start = 0;
                return coupledMeritOrder;
            }
        }

        public MA_Auction SetStartPriceOfMeritOrder(MA_Auction coupledMeritOrder)
        {
            coupledMeritOrder.P_Start = coupledMeritOrder.V_Start < 0 ? LB_Price : //unlimitiertes Gebot nach unten
                coupledMeritOrder.V_Start > coupledMeritOrder.MaxOffer ? UB_Price : //unlimitiertes Gebot nach oben
                coupledMeritOrder.CalcPrice(coupledMeritOrder.V_Start);
            return coupledMeritOrder;
        }

        /// <summary>
        /// Baue aus AF und NF Kurven eine MeritOrder
        /// Ignoriere dazu unlimitierte Gebote
        /// Nachfrage der MeritOrder ist die Nachfrage des Marktergebnisses, Handelsmenge entsprechend ebenfalls
        /// Darf erst aufgerufen werden, wenn Marktgebiet berechnet!
        /// </summary>
        public MA_Auction AddBidsToCoupledMeritOrder(MA_Auction coupledMeritOrder, MarketResultUP result)
        {
            //Merit Order leeren.
            coupledMeritOrder.DeleteMeritOrder();

            if (coupledMeritOrder.AuctionFailed())
                coupledMeritOrder.AddBidTupel(result.ClearingPrice, 0, false, 0);

            //Unlimiterte Gebote der Nachfrage in MO übernehmen und unlimitierte Gebote 
            //mit Maximalpreis für Erweiterung der Merit Order zählen 
            (coupledMeritOrder, VolumeUnlimitedUpperPriceBound) = EoM_Extensions.AddBidsFromMeritOrderToCoupledMeritOrder(coupledMeritOrder, LstCurvePurchase, VolumeUnlimitedUpperPriceBound, UB_Price, LB_Price);

            //Unlimiterte Gebote des Angebots in MO übernehmen und unlimitierte Gebote 
            //mit Maximalpreis für Erweiterung der Merit Order zählen 
            (coupledMeritOrder, VolumeUnlimitedUpperPriceBound) = EoM_Extensions.AddBidsFromMeritOrderToCoupledMeritOrder(coupledMeritOrder, LstCurveSell, VolumeUnlimitedUpperPriceBound, UB_Price, LB_Price);

            return coupledMeritOrder;
        }




        /// <summary>
        /// Angebots- und Nachfragekurve löschen.
        /// </summary>
        public void DeleteBidsPurchaseSell()
        {
            LstCurvePurchase = new List<BidTupel>();
            LstCurveSell = new List<BidTupel>();
            AddBidsUnlimitedPrice();
        }

        public void AddBidsUnlimitedPrice()
        {
            // Grenzgebote hinzufügen:
            LstCurveSell.Add(new BidTupel(LB_Price, 0, false, 0));
            LstCurveSell.Add(new BidTupel(UB_Price, 0, false, 0));
            LstCurvePurchase.Add(new BidTupel(LB_Price, 0, true, 0));
            LstCurvePurchase.Add(new BidTupel(UB_Price, 0, true, 0));
        }
        #endregion

        #region Erzeugen der Merit Order
        /// <summary>
        /// Merit Order erzeugen
        /// </summary>
        /// <param name="AuctionDate"></param>
        public abstract MA_Auction CreateMeritOrder(MA_Auction coupledMeritOrder, DateTime AuctionDate);
        #endregion

        #region Verändern der Angebots- und Nachfragekurve

        ///<summary>
        ///Fügt Gebot hinzu, Falls Preise ausserhalb der Preisgrenzen dann zu Preisgrenzen = unlimitiertes Gebot
        ///Gebote mit 0 Menge werden ignoriert, da ansonsten ggg. Div0 Fehler bei Markträumung!
        /// </summary>
        public void AddBid(decimal fPrice, decimal fVolume)
        {
            if (fVolume != 0)
            {
                fPrice = Math.Round(fPrice, 2);

                if (fPrice > UB_Price) { fPrice = UB_Price; }
                if (fPrice < LB_Price) { fPrice = LB_Price; }

                if (fVolume > 0) LstCurveSell.Add(new BidTupel(fPrice, fVolume, false, 0)); //Ist Angebot
                else LstCurvePurchase.Add(new BidTupel(fPrice, (-1) * fVolume, true, (-1) * fVolume)); // Ist Nachfrage
            }
        }

        ///<summary>
        ///Fügt Gebot hinzu, Falls Preise ausserhalb der Preisgrenzen dann zu Preisgrenzen = unlimitiertes Gebot
        ///Gebote mit 0 Menge werden ignoriert, da ansonsten ggg. Div0 Fehler bei Markträumung!
        /// </summary>
        public void AddBids(List<(decimal fPrice, decimal fVolume)> bids)
        {
            bids.ForEach(bid =>
                {
                    if (bid.fVolume != 0)
                    {
                        bid.fPrice = Math.Round(bid.fPrice, 2);

                        if (bid.fPrice > UB_Price) { bid.fPrice = UB_Price; }
                        if (bid.fPrice < LB_Price) { bid.fPrice = LB_Price; }

                        if (bid.fVolume > 0) LstCurveSell.Add(new BidTupel(bid.fPrice, bid.fVolume, false, 0)); //Ist Angebot
                        else LstCurvePurchase.Add(new BidTupel(bid.fPrice, (-1) * bid.fVolume, true, (-1) * bid.fVolume)); // Ist Nachfrage
                    }
                });
        }
        #endregion

        #region Berechnung des Schnittpunkts von Angebots- und Nachfragekurve

        /// <summary>
        /// 1. Sortiert Gebotslisten nach Preis
        /// 2. Fügt Bids mit selben Preis zusammen.
        /// 3. Bildet MeritOrder in dem Position in Reihenfolge (aggVol) ergänzt wird
        /// </summary>
        private void PrepareMeritOrderSellPurchase()
        {
            //Purchase
            LstCurvePurchase = EoM_Extensions.AggregateVolumesUP(EoM_Extensions.MeritOrder_Couple_Douplets(
                EoM_Extensions.OrderByDescendingListsPrice(LstCurvePurchase)));

            //Sell
            LstCurveSell = EoM_Extensions.AggregateVolumesUP(EoM_Extensions.MeritOrder_Couple_Douplets(
                EoM_Extensions.OrderByListsPrice(LstCurveSell)));
        }

        ///<summary>
        ///Hauptfunktion für die Berechnung des MArktergebnisses, ruft private Hilfsfunktionen aus. Reihenfolge ist entscheidend:
        ///Erst Sortieren, dann Duplikate aggregieren, dann Volumen Aggregieren.
        ///Für die Preisbildung wird ein Schnittpunkt der Angebots- und Nachfragekurve gesucht.
        ///In der Folge werden die Gebotspaare, die für die Preissetzung erforderlich sind u.U. zu einem anderen Preis nur teilweise geräumt
        ///Für die Bewertung des Kauf-Verkaufsverhaltens der Agenten sind somit einmal der Markträumungspreis für die Erlösseite
        ///sowie auch der Grenzpreis der Preisbildung und dem Pro-RataAnteil der Gebote zum grenzpreis für die Bewertung des Tatsächlichen Verkaufvorganges entscheidend.
        ///Bspw: Preis: 53,72€ mit 1084,68 MWh als Ergebnis linearen Interpolarisation zwischen den Preispaaren (€ | kumuliertes Vol): 
        ///Angebot:    (51 | 1072) und (54 | 1086) -> Verkaufsgebot mit 51 wird mit 53,72 € zu 90,57% erfüllt.
        ///Nachfrage:  (53 | 1099) und (54 | 1079) -> Kaufgebot mit 54 wird mit 53,72 € zu 28,4% erfüllt.
        ///</summary>
        public MarketResultUP GetMarketResult()
        {
            if (LstCurvePurchase.Count > 0 && LstCurveSell.Count() > 0) //  && LstCurvePurchase.Select(item => item.Volume).Sum() > 0 && LstCurveSell.Count > 0 && LstCurveSell.Select(item => item.Volume).Sum() > 0)
            {
                //Vorbereiten der MeritOrders für die Kauf und Verkaufsgebote
                PrepareMeritOrderSellPurchase();

                //Für eventuelle Plots und Outputs der MeritOrder
                if (this.MA_Code == MarketAreaEnum.DE)
                {
                    PlotAndOutput.Plot_MeritOrder(this.CoupledMarket, this.TS, default, LstCurveSell.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList(), LstCurvePurchase.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList());
                    PlotAndOutput.OutputConsole_MeritOrder(this.CoupledMarket, this.TS, default, LstCurveSell.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList(), LstCurvePurchase.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList());
                    PlotAndOutput.OutputCSV_MeritOrder(this.CoupledMarket, this.TS, default, LstCurveSell.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList(), LstCurvePurchase.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList());
                }

                MarketResultUP Result;
                decimal VolChangeMeritOrder;

                (int iS, int iP, DayAheadMarket.IrrationalType iIrrational, bool lastJumpInSell) = EoM_Extensions.MeritOrder_FindCrossingPoint(LstCurvePurchase, LstCurveSell);

                switch (iIrrational)
                {
                    case DayAheadMarket.IrrationalType.EXCESS_SUPPLY: //Angebotsüberschuss (Unlimitiertes Angebot > als gesamte Nachfrage)
                        {
                            VolChangeMeritOrder = -(LstCurveSell[iS].AggVol - (LstCurvePurchase[Math.Max(LstCurvePurchase.Count - 2, 0)].Price == LstCurveSell[iS].Price ? LstCurvePurchase[Math.Max(LstCurvePurchase.Count - 3, 0)].AggVol : LstCurvePurchase[Math.Max(LstCurvePurchase.Count - 2, 0)].AggVol));
                            decimal clearingVolume = LstCurvePurchase[Math.Max(LstCurvePurchase.Count - 2, 0)].AggVol;
                            Result = new MarketResultUP(clearingPrice: LstCurveSell[iS].Price,
                                clearingVolume,
                                marginalPriceSell: LstCurveSell[iS].Price,
                                proRataShareSell: clearingVolume / LstCurveSell[iS].AggVol,
                                marginalPricePurchase: LstCurveSell[iS].Price == LstCurvePurchase[iP - 1].Price ? LstCurvePurchase[Math.Max(iP - 1, 0)].Price : LstCurvePurchase[Math.Max(iP - 2, 0)].Price,
                                proRataSharePurchase: 1,
                                VolChangeMeritOrder);
                            break;
                        }
                    case DayAheadMarket.IrrationalType.EXCESS_DEMAND: //Nachfrageüberschuss (unlimitierte Nachfrage > als gesamtes Angebot)
                        {
                            VolChangeMeritOrder = - (LstCurveSell[Math.Max(LstCurveSell.Count - 2, 0)].AggVol - (iP == 0 ? 0 : LstCurvePurchase[iP - 1].AggVol));
                            decimal clearingVolume = LstCurveSell[Math.Max(LstCurveSell.Count - 2, 0)].AggVol;
                            Result = new MarketResultUP(clearingPrice: LstCurvePurchase[iP].Price,
                                clearingVolume,
                                marginalPriceSell: LstCurveSell[Math.Max(LstCurveSell.Count - 2, 0)].Price,
                                proRataShareSell: 1,
                                marginalPricePurchase: LstCurvePurchase[iP].Price,
                                proRataSharePurchase: LstCurvePurchase[iP].AggVol != 0 ? clearingVolume / LstCurvePurchase[iP].AggVol : 1,
                                VolChangeMeritOrder);
                            break;
                        }
                    default:
                        {
                            decimal marginalPriceSell = LstCurveSell[Math.Max(iS - 1, 0)].Price;
                            decimal marginalPricePurchase = LstCurvePurchase[iP].Price;

                            if (LstCurvePurchase[iP].AggVol == LstCurveSell[Math.Max(iS - 1, 0)].AggVol) //Mengengleichheit
                            {
                                VolChangeMeritOrder = 0;
                                Result = new MarketResultUP(clearingPrice: 0.5m * (LstCurvePurchase[iP].Price + LstCurveSell[Math.Max(iS - 1, 0)].Price),
                                    clearingVolume: LstCurvePurchase[iP].AggVol,
                                    marginalPriceSell,
                                    proRataShareSell: 1,
                                    marginalPricePurchase,
                                    proRataSharePurchase: 1,
                                    VolChangeMeritOrder);
                            }
                            else if (lastJumpInSell) //Wenn letzte Indexverschiebung in der Angebotsmeritorder stattgefunden hat, dann muss letztes Nachfragegebot preisbestimmend sein.
                            {
                                VolChangeMeritOrder = -(LstCurveSell[Math.Max(iS - 1, 0)].AggVol - LstCurvePurchase[Math.Max(iP - 1, 0)].AggVol);
                                decimal clearingVolume = iS > 0 ? LstCurveSell[Math.Max(iS - 1, 0)].AggVol : 0;
                                Result = new MarketResultUP(clearingPrice: LstCurvePurchase[iP].Price,
                                    clearingVolume,
                                    marginalPriceSell,
                                    proRataShareSell: iS > 0 ? (LstCurveSell[Math.Min(iS - 1, 0)].Price == LstCurvePurchase[iP].Price ? 1 : 0m) : 0m,
                                    marginalPricePurchase,
                                    proRataSharePurchase: (clearingVolume - (iP > 0 ? LstCurvePurchase[iP - 1].AggVol : 0m)) / (LstCurvePurchase[iP].AggVol - (iP > 0 ? LstCurveSell[Math.Max(iS - 1, 0)].AggVol : 0m)),
                                    VolChangeMeritOrder);
                            }
                            else //Wenn letzte Indexverschiebung in der Nachfragemeriorder stattgefunden hat, dann muss letztes Angebot preisbestimmend sein.
                            {
                                if (LstCurvePurchase[iP - 1].Price == LstCurveSell[iS].Price) //Preisgleichheit
                                    VolChangeMeritOrder = -(LstCurveSell[iS].AggVol - LstCurvePurchase[Math.Max(iP - 2, 0)].AggVol);
                                else
                                    VolChangeMeritOrder = -(LstCurveSell[iS].AggVol - LstCurvePurchase[Math.Max(iP - 1, 0)].AggVol);
                                decimal clearingVolume = iS > 0 ? LstCurvePurchase[Math.Max(iP - 1, 0)].AggVol : 0;
                                Result = new MarketResultUP(clearingPrice: LstCurveSell[iS].Price,
                                    clearingVolume,
                                    marginalPriceSell,
                                    proRataShareSell: (clearingVolume - (iS > 0 ? LstCurveSell[Math.Max(iS - 1, 0)].AggVol : 0)) / (LstCurveSell[iS].AggVol - (iS > 0 ? LstCurvePurchase[Math.Max(iP - 1, 0)].AggVol : 0m)),
                                    marginalPricePurchase,
                                    proRataSharePurchase: iP > 0 ? (LstCurvePurchase[iP - 1].Price == LstCurveSell[iS].Price ? 1 : 0m) : 0m,
                                    VolChangeMeritOrder);
                            }
                            break;

                            //Zur Erläuterung: VolChangeMeritOrder ist der Anteil, der in einer zusammengefügten Merit-Order den Anteil an Menge angibt, 
                            //der von der maximalen aggregierten Menge einer Stufe abgezogen werden muss. 
                        }
                }

                MarketVolume = Result.ClearingVolume;
                MarketPrice = Result.ClearingPrice;

                MarketUPResult = Result;
                return Result;
            }
            else
            {
                return GetDefaultMarketResult();
            }
        }

        protected MarketResultUP GetDefaultMarketResult()
        {
            if (CoupledMarket is DayAheadMarket)
                return Default_Prices.GetDefaultDayAheadLocalMarketResult(TS, AuctionTimeframe.Step);
            else
                return Default_Prices.GetDefaultIntradayLocalMarketResult(TS, AuctionTimeframe.Step);
        }
        #endregion
    }

    public class MarketResultUP 
    {
        public decimal ClearingPrice { get; set; }
        public decimal ClearingVolume { get; set; }
        public decimal MarginalPriceSell { get; set; }
        public decimal ProRataShareSell { get; set; }
        public decimal MarginalPricePurchase { get; set; }
        public decimal ProRataSharePurchase { get; set; }

        public decimal VolumeChangeMeritOrder { get; set; }

        public MarketResultUP(decimal clearingPrice, decimal clearingVolume, decimal marginalPriceSell, decimal proRataShareSell, decimal marginalPricePurchase, decimal proRataSharePurchase, decimal volumeChangeMeritOrder)
        {
            ClearingPrice = Math.Round(clearingPrice, 2);
            ClearingVolume = Math.Round(clearingVolume, 2);
            MarginalPriceSell = marginalPriceSell;
            ProRataShareSell = proRataShareSell;
            MarginalPricePurchase = marginalPricePurchase;
            ProRataSharePurchase = proRataSharePurchase;
            VolumeChangeMeritOrder = volumeChangeMeritOrder;
        }
    }
}
