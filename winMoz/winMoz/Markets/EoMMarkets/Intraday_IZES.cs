﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Bids;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Simulation;
using System.Threading;
using winMoz.Helper;
using winMoz.Markets.Results;
using winMoz.Markets;

namespace winMoz.Markets.EoMMarkets
{
    /// <summary>
    /// Zentrale Klasse Für IntradayMarkt
    /// 
    /// Aktuell nur eine Liste von 4 Einzelmärkten
    /// 
    /// Funktionlität analog zu DayAheadMarkt mit der Einschränkung, dass keine Marktkoppelung erfolgt.
    /// </summary>
    /// 
    public class Intraday_IZES : IntradayMarket, IIntraday_IZES
    {
        #region Variablen
        private LocalMarketArea LocalMarket { get; set; }
        private MarketAreaEnum Local_MA_Code { get; set; }

        private Dictionary<int, MA_Auction> LocalAuctions { get; set; }

        private DefaultPrices Default_Prices { get; set; }

        #endregion

        #region Initial

        protected Intraday_IZES() { }

        ///<summary>
        ///Konstruktur Erzeugt Dictonary über die einzelnen Stunden als jeweils einzelne "Märkte" vom Typ Uniform Pricing
        ///Standardname für das Simulationsgebiet ist "DE".
        ///Identifier ist jeweils ein um je 15 Minuten erhöhtes DateTime.                
        ///Geltungsstunde: Setzt die Stunde fest, muss zwingend XX:00 sein, da Ausgangspunkt für Anzahl an Viertelstunden.         
        /// </summary>
        public Intraday_IZES(IntradayAttributes attributes, int scenarioID = 0) : base(attributes)
        {
        }

        public override void Initialize(bool IsComplete = true)
        {
            base.Initialize(IsComplete: false);
            ((IntradayAttributes)this.Attributes).SetBlockTypesFromDB();
            Local_MA_Code = Attributes.LocalMACode;
            LocalMarket = new LocalMarketArea(Convert.ToDecimal(Attributes.Upper_Price_Bound), Convert.ToDecimal(Attributes.Lower_Price_Bound), Local_MA_Code, MarketAreaUP.MarketType.Intraday, this.EventTimeframe, Default_Prices);
            InitialiseLocalAuctions(Attributes.EventAttributes.StartDeterminationTime);

            if (IsComplete) Log.Info($"{this.GetType()} initialized.");
        }

        private void InitialiseLocalAuctions(DateTime dateStart)
        {
            LocalAuctions = new Dictionary<int, MA_Auction>();
            Enumerable.Range(0, 4).ToList().ForEach(quarterHour =>
            {
                LocalAuctions.Add(quarterHour, new MA_Auction(dateStart.AddMinutes(quarterHour * 15), LocalMarket));
            });
        }
        #endregion

        /// <remarks>
        /// Rückgabewerte sind p0 und m0 als isolierte Preise bzw. Menge vor irgendeiner Form von Koppelung
        /// Sollten im weiteren Verlauf Blöcke oder MCC oder so integriert werden, dann müssen die Rückgabewerte entsprechend auch
        /// p1 bzw. m1 als Werte Nach Optimierung gesetzt werden.
        /// </remarks>
        public void GatherResults(ref List<MarketResult> results)
        {
            foreach (MA_Auction auction in LocalAuctions.Values)
            {
                if (!auction.IsMeritOrderEmpty())
                {
                    auction.CalcVolumePurchaseSellOfLastMeritOrderAggBid();
                    var Volume = auction.Volume_Market_Start + auction.V_Result - auction.V_Start;
                    results.Add(new IntraDayResult(auction.TS, ShortCode.IntraDay.QuarterHour(auction.TS), (float)Math.Round(auction.P_Result, 3), (float)Math.Round(Volume, 3), (float)auction.VolumeLastPurchasePart, (float)auction.VolumeLastSellPart));
                }
                else
                {
                    var EndDuration = EventTimeframe.GetEndOfDuration();
                    var BidSegmentsAuction = Bids.Where(bid => bid.TimeStamp >= EventTimeframe.TimeStamp && bid.TimeStamp < EndDuration).ToList().SelectMany(item => item.Segments).Where(item => item.TimeStamp == auction.TS).ToList();
                    if (BidSegmentsAuction.Count() > 0)
                    {
                        var BidSegmentsPurchase = BidSegmentsAuction.Where(item => item.VolumeMW > 0);
                        var BidSegmentsSell = BidSegmentsAuction.Where(item => item.VolumeMW < 0);
                        if (BidSegmentsPurchase.Count() != 0) results.Add(new IntraDayResult(auction.TS, BidSegmentsPurchase.First().ShortCode, price: this.Attributes.Upper_Price_Bound, 0, 0, 0));
                        else if (BidSegmentsSell.Count() != 0) results.Add(new IntraDayResult(auction.TS, BidSegmentsSell.First().ShortCode, price: this.Attributes.Lower_Price_Bound, 0, 0, 0));
                    }
                    else
                    {
                        Log.Error("No Intraday result for " + auction.TS);
                    }
                        //results.Add(Default_Prices.GetDefaultIntradayResult(auction.TS, EventTimeframe.Step));
                }
            }
        }

        #region Je Auktionsereignis

        protected override void ExecuteAuction(out List<winMoz.Markets.Results.MarketResult> results)
        {
            LocalMarket.SetEventTimeframe(EventTimeframe);
            LocalMarket.ClearBidsSortedAuctionTime();

            // Platzhalter
            results = new List<winMoz.Markets.Results.MarketResult>();
            PutBidsToMarket();

            CalcInitialAuctionResults(EventTimeframe.TimeStamp);

            GatherResults(ref results);

        }

        public void CalcInitialAuctionResults(DateTime auctionTime)
        {
            foreach (int quarterHour in LocalAuctions.Keys)
            {
                var marketAuction = LocalAuctions[quarterHour];
                DateTime timeStamp = auctionTime.AddMinutes(quarterHour * 15);
                marketAuction.MarketHandler.UpdateMeritOrder(marketAuction, timeStamp);
            }
        }

        /// <summary>
        /// Fügt alle Einzelgebote dem Markt hinzu
        /// </summary>
        private void PutBidsToMarket()
        {
            BidsAuction.ForEach(tmpBid => tmpBid.Segments.ToList().ForEach(tmpSeg =>
            {
                AddBidToMarket(Convert.ToDecimal(tmpSeg.Price), Convert.ToDecimal(tmpSeg.VolumeMW), tmpSeg.TimeStamp.GetQuarterHourOfHour());
            }));
        }
        #endregion

        #region Gebotsabgabe
        public void AddBidToMarket(decimal Price, decimal Volume, int ViertelStunde)
            => LocalMarket.AddBidToListSortedAuction(Price, Volume, ViertelStunde - 1);

        #endregion

        #region Debug
        
        #endregion

    }
}