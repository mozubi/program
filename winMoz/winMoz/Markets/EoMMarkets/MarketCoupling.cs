﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Simulation;

namespace winMoz.Markets.EoMMarkets
{
    public class MarketCoupling 
    {
        /// <summary>
        /// Marktkoppelungsklasse benötigt Aufruf durch übergeordnete Klasse, die die Marktgebiete erzeugt.        
        /// Funktion sind Tielweise voneinander abhängig.
        /// Erst müssen ALLE Marktgebiete (als Referenz) über addMarktgebiet erfasst werden bevor über addExportCapacity die Kapazitäten erfasst werden können.
        /// Preise und Volumen werden aus deterministischen Gründen (ALG-Ende) auf 3 Stellen gerundet.
        /// Die Optimierung erfolgt über den Ansatz einer Merit-Order. Dabei wird der Im-Export allein über die Ncahfrage der Merit-Order abgebildet.
        /// Import verringert die (lokale) Nachfrage, Export erhöht sie entsprechend
        /// </summary>

        Dictionary<MarketAreaEnum, MA_Auction> MarketAreas = new Dictionary<MarketAreaEnum, MA_Auction>();
        List<ImExMC> TransferCapacities = new List<ImExMC>();
        decimal MinimalImport;
        public DateTime TS { get; set; }

        public List<string> LstDebugLog { get; } = new List<string>();

        MarketAreaEnum Local_MA_Code;

        

        public MarketCoupling(DateTime ts, MarketAreaEnum local_MA_Code, Dictionary<MarketAreaEnum, MA_Auction> marketAreas, List<(MarketAreaEnum ma_Code_Export, MarketAreaEnum ma_Code_Import, decimal capacity, decimal losses)> transferCapacities, List<(MarketAreaEnum ma_Code_Export, MarketAreaEnum ma_Code_Import, decimal capacity, decimal wind_prog_from)> cFunctions, decimal MinImport = 0.1m)
        {
            /// <summary>
            /// MinImport muss größer gleich 0.000001 sein.
            /// Defaultwert ist 0,1, entspricht somit 100 kW(h).
            /// </summary>
            MinimalImport = MinImport;
            Local_MA_Code = local_MA_Code;
            TS = ts;
            MarketAreas = marketAreas;
            AddTransferCapacities(transferCapacities, cFunctions);
        }

        public void SetTimeStamp() => TS = MarketAreas.First().Value.TS;

        /// <summary>
        /// Funktion liefert false, wenn Origin oder Target nicht in der Marktgebietsliste gefunden wurden.
        /// Losses erwartet einen Wert zwischen 0 (kein Verlust) und 1 (Totalverlust), Falls Wert Out of Bounce, dann losses = 1.
        /// Sammelt die Exportkapazitäten in den jeweiligen Listen und bestimmt entsprechend die maximalen Kapazitäten der jeweiligen Marktgebiete
        /// Der Import (bei IDTarget wird stets berechnet als Exportkapazit * Verlust           
        /// Der (verfügbare) Export von A->B kann in der Höhe vom (verfügbaren) Export von B->A abweichen (Tatsächlich gibt es im Ergebnis nur einen Fluss).       
        /// </summary>
        public void AddTransferCapacities(List<(MarketAreaEnum ma_Code_Export, MarketAreaEnum ma_Code_Import, decimal capacity, decimal losses)> capacities, List<(MarketAreaEnum ma_Code_Export, MarketAreaEnum ma_Code_Import, decimal capacityRel, decimal windProgFrom)> cFunctions)
        {
            var Capacities = capacities.Where(cap => MarketAreas.ContainsKey(cap.ma_Code_Export) && MarketAreas.ContainsKey(cap.ma_Code_Import));
            foreach(var Capacity in Capacities)
            {
                var cFunctionsImEx = cFunctions.Where(item => item.ma_Code_Import == Capacity.ma_Code_Import && item.ma_Code_Export == Capacity.ma_Code_Export).ToList();
                foreach (var CFunction in cFunctionsImEx)
                {
                    MA_Auction MA_Export = MarketAreas[Capacity.ma_Code_Export];
                    MA_Auction MA_Import = MarketAreas[Capacity.ma_Code_Import];
                    decimal Losses = (Capacity.losses < 0 || Capacity.losses > 1) ? 1 : Capacity.losses;
                    TransferCapacities.Add(new ImExMC(MA_Export, MA_Import, Capacity.capacity * CFunction.capacityRel, Losses, CFunction.windProgFrom));
                    if (MA_Export.MaxExSave.Select(item => item.WindProgFrom).Contains(CFunction.windProgFrom))
                    {
                        MA_Export.MaxExSave = AddToElementCapacity(MA_Export.MaxExSave, Capacity.capacity * CFunction.capacityRel, CFunction.windProgFrom);

                    }
                    else
                    {
                        var MaxExSaveSmaller = MA_Export.MaxExSave.Where(item => item.WindProgFrom < CFunction.windProgFrom).LastOrDefault();
                        MA_Export.MaxExSave.Add((CFunction.windProgFrom, MaxExSaveSmaller != default ? MaxExSaveSmaller.Capacity + Capacity.capacity * CFunction.capacityRel : Capacity.capacity * CFunction.capacityRel));
                        var MaxExBigger = MA_Export.MaxExSave.Where(item => item.WindProgFrom > CFunction.windProgFrom).ToList();
                        foreach (var item in MaxExBigger)
                            MA_Export.MaxExSave = AddToElementCapacity(MA_Export.MaxExSave, Capacity.capacity * CFunction.capacityRel, item.WindProgFrom);
                    }
                    if (MA_Import.MaxImSave.Select(item => item.WindProgFrom).Contains(CFunction.windProgFrom))
                    {
                        MA_Import.MaxImSave = AddToElementCapacity(MA_Import.MaxImSave, Capacity.capacity * (1 - Losses) * CFunction.capacityRel, CFunction.windProgFrom);
                    }
                    else
                    {
                        var MaxImSaveSmaller = MA_Import.MaxImSave.Where(item => item.WindProgFrom < CFunction.windProgFrom).LastOrDefault();
                        MA_Import.MaxImSave.Add((CFunction.windProgFrom, MaxImSaveSmaller != default ? MaxImSaveSmaller.Capacity + Capacity.capacity * (1- Losses) * CFunction.capacityRel : Capacity.capacity * (1 - Losses) * CFunction.capacityRel));
                        var MaxImBigger = MA_Import.MaxImSave.Where(item => item.WindProgFrom > CFunction.windProgFrom).ToList();
                        foreach (var item in MaxImBigger)
                            MA_Import.MaxImSave = AddToElementCapacity(MA_Import.MaxImSave, Capacity.capacity * (1 - Losses) * CFunction.capacityRel, item.WindProgFrom);
                    }
                }
            }
        }

        public List<(decimal windProgFrom, decimal capacity)> AddToElementCapacity(List<(decimal windProgFrom, decimal capacity)> imOrExBefore, decimal capacityToAdd, decimal windProgFrom)
        {
            var ImOrExSaveOld = imOrExBefore.SingleOrDefault(item => item.windProgFrom == windProgFrom);
            imOrExBefore.Remove(ImOrExSaveOld);
            imOrExBefore.Add((windProgFrom, ImOrExSaveOld.capacity + capacityToAdd));
            return imOrExBefore;
        }

        private MA_Auction Determine_Import_MA(Dictionary<MarketAreaEnum, MA_Auction> ma_List)
            => MarketAreas
            .Where(item => item.Value.NoFurtherExchange == false)
            .Where(ma => Math.Min(ma.Value.MaxIm, ma.Value.MaxReduceCapacity) > (MinimalImport - 0.00000001m))
                .OrderBy(ma => ma.Value.P_Result).LastOrDefault().Value;

        #region MarketResult
        private ImExMC DetermineTransferCapacityForImportMA(MA_Auction ma_IM)
        {
            return TransferCapacities
                //Wenn es Transferkapazitäten zum Import-Marktgebiet gibt
                .Where(Transfer => Transfer.MA_Import.MA_Code == ma_IM.MA_Code)
                //Wenn verfügbare Importmenge als Summe aus Transferkapazität*Verluste + Netting Kapazität > Minimale Importmenge
                .Where(Transfer => Transfer.CapacityRaw * (1m - Transfer.Losses) + Transfer.CapacityNetting > (MinimalImport - 0.0000001m)
                //Wenn Transferkapazitäten oder Steigerungsmöglichkeiten der Produktion vorhanden
                    && Math.Min(ma_IM.MaxReduceCapacity, Math.Min(Transfer.MA_Export.MaxEx, Transfer.MA_Export.MaxIncreaseCapacity)) > (MinimalImport - 0.0000001m)
                //Wenn es Preisunterschiede gibt
                    && Transfer.MA_Export.P_Result < ma_IM.P_Result - 0.0001m)
                 //Das letzte Marktgebiet für das gilt: Wenn Setzen des Preises auf Preis des Export-Marktgebiets einen Mengenunterschied größer als die minimale Importmenge zur Folge hat.
                .LastOrDefault(Transfer => ma_IM.V_Result - ma_IM.CalcVolume(Transfer.MA_Export.P_Result) > (MinimalImport - 0.00000001m)); 
        }

        private ImExMC DetermineImportToExport(ImExMC export)
            => TransferCapacities.FirstOrDefault(transfer => transfer.MA_Import.MA_Code == export.MA_Export.MA_Code && transfer.MA_Export.MA_Code == export.MA_Import.MA_Code);

        private (decimal VolumeRaw, decimal VolumeNetting) CalculateExchange(decimal VolumeExchange, decimal MaxVolumeExchange, ImExMC Export)
        {
            decimal volumeNetting = Math.Min(Export.CapacityNetting, VolumeExchange);
            decimal volumeRaw = VolumeExchange > Export.CapacityNetting ? Math.Min(Export.CapacityRaw, VolumeExchange - volumeNetting) : 0;
            return (volumeRaw, volumeNetting);
        }

        private bool IsIrrationalForExportMA(decimal volumeExchangeRaw, decimal volumeExchangeNetting, decimal losses, MA_Auction maExport)
            => Math.Abs(maExport.MaxIncreaseCapacity) < Math.Abs(volumeExchangeNetting * (1 - losses) + volumeExchangeRaw);

        private (decimal VolumeRaw, decimal VolumeNetting) SetIrrationalityTransferLimitExportMA(decimal volumeExchangeRaw, decimal volumeExchangeNetting, decimal losses, MA_Auction maExport)
        {
            decimal VolumeExchange = volumeExchangeNetting * (1m - losses) + volumeExchangeRaw;
            decimal fDeltaMO = maExport.MaxIncreaseCapacity - VolumeExchange;

            //fDeltaMO < 0 -> Export würde Angebot außerhalb der MeritOrder bewegen
            //(-1 * fDeltaMO) > volumeExchangeRaw -> Sollte eigentlich nicht auftreten, Nettingkapazität muss reduziert werden.
            decimal volumeRaw = Math.Max(0, volumeExchangeRaw + fDeltaMO); 
            decimal volumeNetting = (- fDeltaMO) > volumeExchangeRaw ? volumeExchangeNetting + (volumeExchangeRaw + fDeltaMO) / (1m - losses) : volumeExchangeNetting;
            //DEBUG
            if (IsIrrationalForExportMA(volumeRaw, volumeNetting, losses, maExport))
            {
                decimal VolumeExchangeCorrect = volumeNetting * (1m - losses) + volumeRaw;
                decimal fDeltaMOCorrect = VolumeExchangeCorrect - maExport.MaxIncreaseCapacity;
                if (volumeRaw > 0)
                    volumeRaw -= fDeltaMOCorrect;
                else
                    volumeNetting -= fDeltaMOCorrect;
                if (IsIrrationalForExportMA(volumeRaw, volumeNetting, losses, maExport))
                {
                    decimal VolumeExchangeCorrectCorrect = volumeNetting * (1m - losses) + volumeRaw;
                    decimal fDeltaMOCorrectCorrect = VolumeExchangeCorrect - maExport.MaxIncreaseCapacity;
                    Log.Error("Fehler beim Setzten des IrrationalityLimit Export: Austauschvolumen um " + fDeltaMOCorrectCorrect + " zu groß.");
                }
            }

            return (volumeRaw, volumeNetting);
        }

        /// <summary>
        /// Prüfe, ob Export Angebot außerhalb der MeritOrder bewegen würde
        /// </summary>
        private bool IsIrrationalForImportMA(decimal volumeExchangeRaw, decimal volumeExchangeNetting, decimal losses, MA_Auction maImport)
            => Math.Abs(maImport.MaxReduceCapacity) < Math.Abs(volumeExchangeNetting + volumeExchangeRaw * (1 - losses)); 

        private (decimal VolumeRaw, decimal VolumeNetting) SetIrrationalityTransferLimitImportMA(decimal volumeExchangeRaw, decimal volumeExchangeNetting, decimal losses, MA_Auction maImport)
        {
            decimal VolumeExchange = volumeExchangeNetting + volumeExchangeRaw * (1m - losses);
            decimal fDeltaMO = maImport.MaxReduceCapacity - VolumeExchange;

            decimal volumeRaw = Math.Max(0, volumeExchangeRaw + fDeltaMO / (1m - losses));
            //(-1 * fDeltaMO) > (volumeExchangeRaw * (1 - losses)) -> Sollte eigentlich nicht auftreten, Nettingkapazität muss reduziert werden.
            decimal volumeNetting = (-fDeltaMO) > (volumeExchangeRaw * (1m - losses)) ? volumeExchangeNetting + (volumeExchangeRaw + (fDeltaMO / (1 - losses))) * (1 - losses) : volumeExchangeNetting;

            //DEBUG
            if (IsIrrationalForImportMA(volumeRaw, volumeNetting, losses, maImport))
            {
                decimal VolumeExchangeCorrect = volumeNetting + volumeRaw * (1m - losses);
                decimal fDeltaMOCorrect = VolumeExchangeCorrect - maImport.MaxReduceCapacity;
                if (volumeRaw > 0)
                    volumeRaw -= fDeltaMOCorrect;
                else
                    volumeNetting -= fDeltaMOCorrect;
                if (IsIrrationalForImportMA(volumeRaw, volumeNetting, losses, maImport))
                {
                    decimal VolumeExchangeCorrectCorrect = volumeNetting + volumeRaw * (1m - losses);
                    decimal fDeltaMOCorrectCorrect = VolumeExchangeCorrect - maImport.MaxReduceCapacity;
                    Log.Error("Fehler beim setzten des IrrationalityLimit Import: Austauschvolumen um " + fDeltaMOCorrectCorrect + " zu groß.");
                }
            }

            return (volumeRaw, volumeNetting);
        }

        private void ChangeMarketParametersExportMA(MA_Auction ma_Export, decimal volumeExchangeRaw, decimal volumeExchangeNetting, decimal losses)
        {
            decimal VolumeExchange = volumeExchangeNetting * (1m - losses) + volumeExchangeRaw;
            ma_Export.V_Result += VolumeExchange;
            ma_Export.P_Result = ma_Export.V_Result > ma_Export.MaxOffer ? ma_Export.UB_Price :
                ma_Export.V_Result < 0 ? ma_Export.LB_Price : ma_Export.CalcPrice(ma_Export.V_Result);
            ma_Export.MaxEx -= VolumeExchange;
            ma_Export.VerschiebeVolumen += VolumeExchange;
            ma_Export.SetIncreaseReduceCapacity();
        }

        private void ChangeMarketParametersImportMA(MA_Auction ma_Import, decimal volumeExchangeRaw, decimal volumeExchangeNetting, decimal losses)
        {
            decimal VolumeExchange = volumeExchangeNetting + volumeExchangeRaw * (1m - losses);
            ma_Import.V_Result -= VolumeExchange;
            ma_Import.P_Result = ma_Import.V_Result > ma_Import.MaxOffer ? ma_Import.UB_Price :
                ma_Import.V_Result < 0 ? ma_Import.LB_Price : ma_Import.CalcPrice(ma_Import.V_Result);
            ma_Import.MaxIm -= VolumeExchange;
            ma_Import.VerschiebeVolumen -= VolumeExchange;
            ma_Import.SetIncreaseReduceCapacity();
        }

        /// <summary>
        /// Vollziehe Import-Export -> Berechne neue Grenzen für Austausch
        /// </summary>
        /// <param name="Export"></param>
        /// <param name="volumeExchangeRaw"></param>
        /// <param name="volumeExchangeNetting"></param>
        private void CalculateNewExchangeLimits(ImExMC export, decimal volumeExchangeRaw, decimal volumeExchangeNetting)
        {
            export.CapacityNetting -= volumeExchangeNetting;
            export.CapacityRaw -= volumeExchangeRaw;
            ImExMC Import = DetermineImportToExport(export);
            if (Import != default)
            {
                Import.CapacityRaw = Import.CapacityRaw + volumeExchangeNetting;
                Import.CapacityNetting = Import.CapacityNetting + volumeExchangeRaw;
            }
        }

        public void ResetExternalMarketsAndImportExport()
        {
            foreach(var ma in MarketAreas)
            {
                if (ma.Key == Local_MA_Code)
                    ma.Value.ResetImportExport();
                else
                    ma.Value.Reset();
            }
            SetTimeStamp();
        }

        /// <summary> 
        /// Zentrale Funktion zur Berechnung der Marktkopplungskapazitäten
        /// Zielfunktion:
        /// Bestimme den optimalen Import / Export, der zur geringsten, quadratischen Abweichungen aller Preise vom Gleichgewichtspreis ohne Engpässe und Übertragungsverluste führt
        /// Nebenbedingungen:
        /// 1.	Zwischen zwei Preis-Mengen-Paaren wird der Preis linear interpoliert.
        /// 2.	Der Preis vom Exportland ist nach dem erfolgten Export immer mindestens genau eine kleinste Preiseinheit (hier 0,001 €/MW) kleiner als der Preis des Importlandes: 
        /// 3.	Es gibt eine Menge, die Mindestens exportiert werden muss, damit der Export durchgeführt wird. (frei skalierbar, aktuell 0,1 MW).
        /// 
        /// Algorithmus:
        /// 1.)	Bestimme das Marktgebiet mit dem höchsten (aktuellen) Preis und freier (aktuellen) Import Kapazität über dem Exportminimum.
        ///     Falls kein Marktgebiet gefunden -> ENDE
        /// 2.)	Suche das Marktgebiet 〖MG〗_Ex mit dem niedrigsten Preis kleiner als P_(Im,I) 
        ///     Falle kein Importland gefunden, setze maximale Importkapazität für Importmarktgebiet auf 0
        /// </summary>
        public void CalculateExportImport(Boolean bolDebugLog = false)
        {
            decimal VolumeExchange, MaxVolumeExchange, VolumeExchangeRaw, VolumeExchangeNetting;
            MA_Auction MAExport, MAImport;

            int iDebug = 0;
            ResetExternalMarketsAndImportExport();
            do 
            {
                ///Schleife über die Verfügbaren Importgebiete, falls keine gefunden ist der ALG zuende und das optimale gefunden.
                MAImport = Determine_Import_MA(MarketAreas);
                if (MAImport == default)
                {
                    if (bolDebugLog)
                        Helper_DebugLog_MCC.DebugAusgabe(TransferCapacities, MarketAreas, LstDebugLog, TS);
                    break; //Abbruchbedingung, falls kein (weiteres) Importland gefunden
                }

                //Bestimme Land aus dem Importiert werden kann.
                ImExMC Transfer = DetermineTransferCapacityForImportMA(MAImport);

                if (Transfer == default)
                    //MAImport.MaxIm = 0;
                    MAImport.NoFurtherExchange = true;
                else
                {
                    MAExport = Transfer.MA_Export;
                    //Bestimme verfügbare Importmenge als Summe aus Exportkapazität*Verluste + Netting Kapazität
                    //Netting Kapazität ist die (temporär) verfügbbare Zusatzkapazität aus einem vorherigen, vom ALG bestimmten Exportgeschäft in der Gegenrichtung.
                    MaxVolumeExchange = Transfer.CapacityRaw * (1 - Transfer.Losses) + Transfer.CapacityNetting;

                    //Berechne Importmenge, die notwendig ist um "Preisgleichheit" zwischen beiden Marktgebieten herzustellen.
                    //Keine echte Preisparität, da der ALG ansonsten zwischen beiden Gebieten hin und her springt.
                    //Determinierung über Preisabstand.
                    VolumeExchange = 0.99m * (MAImport.V_Result - MAImport.CalcVolume(MAExport.P_Result)); //TODO: Versuch Pendeleffekt zu vermeiden, eine Art Reibung

                    //Begrenzung durch Maximale Importkapazität
                    VolumeExchange = Math.Min(MaxVolumeExchange, VolumeExchange);

                    //Berechne Ausstauschmengen, neue Kapazität (Raw) und Ausgleich bereits in der anderen Richtung ausgetauschter Menge (Netting).
                    (VolumeExchangeRaw, VolumeExchangeNetting) = CalculateExchange(VolumeExchange, MaxVolumeExchange, Transfer);

                    //Prüfe auf "Irrationalität", also ob nach dem Import bzw. Export die neue "Nachfrage" noch in der jeweiligen MeritOrder liegt

                    //Für das Exportland
                    if (IsIrrationalForExportMA(VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses, MAExport))
                        (VolumeExchangeRaw, VolumeExchangeNetting) = SetIrrationalityTransferLimitExportMA(VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses, MAExport);

                    //Für das Importland
                    if (IsIrrationalForImportMA(VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses, MAImport))
                        (VolumeExchangeRaw, VolumeExchangeNetting) = SetIrrationalityTransferLimitImportMA(VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses, MAImport);

                    string sDebugLog = "";

                    if (bolDebugLog)
                        sDebugLog = Helper_DebugLog_MCC.PrintDebugLogBefore(iDebug, MAExport, MAImport, Transfer.MA_Import.MA_Code, VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses, Transfer, TransferCapacities);

                    //Vollziehe Import-Export -> Berechne neue Grenzen für Austausch
                    CalculateNewExchangeLimits(Transfer, VolumeExchangeRaw, VolumeExchangeNetting);

                    //Bestimme V_Result und P_Result neu, passe Maximale Kapazitäten an
                    //Exportland
                    ChangeMarketParametersExportMA(MAExport, VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses);
                    //Importland
                    ChangeMarketParametersImportMA(MAImport, VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses);

                    #region Debug
                    //Debugging LOG
                    if (bolDebugLog)
                    {
                        LstDebugLog.Add(Helper_DebugLog_MCC.PrintDebugLogAfter(sDebugLog, iDebug, MAExport, MAImport, Transfer.MA_Import.MA_Code, VolumeExchangeRaw, VolumeExchangeNetting, Transfer.Losses, Transfer, TransferCapacities));
                        iDebug += 1;
                    }
                    #endregion

                } 
            } while (MAImport != default);
        }
        #endregion
    }

    public class ImExMC 
    {
        /// <summary>
        /// Namenskennung (Landkürzel) des Import-Marktgebietes
        /// </summary>
        public MA_Auction MA_Import { get; set; }
        /// <summary>
        /// Export-Marktgebiet
        /// </summary>
        public MA_Auction MA_Export { get; set; }
        /// <summary>
        /// Übertragenen Menge
        /// </summary>
        public decimal CapacityRaw { get; set; }
        /// <summary>
        /// Netting Kapazität ist die (temporär) verfügbbare Zusatzkapazität aus einem vorherigen, vom ALG bestimmten Exportgeschäft in der Gegenrichtung.
        /// </summary>
        public decimal CapacityNetting { get; set; }
        /// <summary>
        /// Übertragungsverluste
        /// </summary>
        public decimal Losses { get; set; }
        /// <summary>
        /// WindProgFrom
        /// </summary>
        public decimal WindProgFrom { get; set; }

        public ImExMC(MA_Auction ma_Export, MA_Auction ma_Import, decimal fCapacity, decimal fLosses, decimal windProgFrom)
        {
            MA_Export = ma_Export;
            MA_Import = ma_Import;
            Losses = fLosses;
            CapacityRaw = fCapacity;
            CapacityNetting = 0;
            WindProgFrom = windProgFrom;
        }
    }
}
