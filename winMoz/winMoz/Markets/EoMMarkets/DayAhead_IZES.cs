﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Bids;
using winMoz.Helper;
using winMoz.Markets.Bids.EoMMarkets;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Results;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Simulation;
using winMoz.Tools.Viz;
using winMoz.Markets;

namespace winMoz.Markets.EoMMarkets
{
    public class DayAhead_IZES : DayAheadMarket, IDayAhead_IZES
    {
        #region Variablen
        /// <summary>
        /// Lokaler Markt
        /// </summary>
        private LocalMarketArea LocalMarket { get; set; }
        /// <summary>
        /// Liste der externen Märkte 
        /// </summary>
        private Dictionary<MarketAreaEnum, ExternalMarketArea> ExternalMarkets { get; set; }

        private Dictionary<int, Dictionary<MarketAreaEnum, MA_Auction>> Auctions { get; set; }
        /// <summary>
        /// Liste der lokalen Auktionen für jede Stunde 
        /// (Jede Auktion besitz eine Verbindung zu lokalem oder externem Markt, 
        /// der die Aktualisierung der Auktionsdaten wie die MeritOrder durchführt)
        /// </summary>
        private Dictionary<int, MA_Auction> LocalAuctions { get; set; }
        /// <summary>
        /// Liste der Marktkoppelung für jede Stunde
        /// </summary>
        private Dictionary<int, MarketCoupling> MarketCouplings { get; set; }
        /// <summary>
        /// Manager der Blockgebote -> Hier wird auch der Marktkopplungsmechanismus aufgerufen.
        /// </summary>
        private BlockHandler BlockHandler { get; set; }

        /// <summary>
        /// Szenariorepository
        /// </summary>
        private MCC_Scenario_Repository Scenario_Repo { get; set; }

        private Dictionary<DateTime, Dictionary<MarketAreaEnum, decimal>> DayAheadResults;

        private DefaultPrices Default_Prices { get; set; }

        /// <summary>
        /// Szenariorepository
        /// </summary>
        private List<MarketAreaEnum> ScenarioMAs { get; set; }


        private Dictionary<ShortCode, float> BlockResults = new Dictionary<ShortCode, float>();


        public enum IrrationalType { IN_RANGE, EXCESS_SUPPLY, EXCESS_DEMAND }

        #endregion

        #region Initial
        protected DayAhead_IZES() { }
        ///<summary>
        ///Konstruktor Erzeugt Dictonary über die einzelnen Stunden als jeweils einzelne "Märkte" vom Typ Uniform Pricing
        ///Standardname für das Simulationsgebiet ist "DE".
        ///Identifier ist jeweils ein um je eine Stunde erhöhtes DateTime.
        ///Aktuell werden die gekoppelten Märkte (bei mitMarktkoppelung = true über einen Zufallsgenerator hinzugefügt.
        ///Geltungstag: Setzt den Tag fest, muss zwingend 00:00 sein, da Ausgangspunkt für Anzahl an Stunden.
        /// mitMarktkoppleung: Legt Fest, ob Amrktkoppelung und damit auch Externe Märkte geladen und optimiert werden sollen
        /// Preisuntergrenze/-obergrenze:   Maximal und Minimalpreise des Marktes: Preise >=, respektive kleinergleich der Grenze gelten als unlimitierte Gebote
        /// Name: Identifier des Lokalen Marktes. Muss identisch sein mit dem Namen, der im Rahmen der Austauschmengen vergeben wird, sofern diese Extern geladen werden sollten
        /// Blockdefinitionen Können hier angepasst werden. GGf. umbauen in eine Definitionsklasse...
        /// </summary> 
        public DayAhead_IZES(DayAheadAttributes attributes, int scenarioID = 0) : base(attributes)
        {
            if (Attributes == null) throw new ArgumentNullException(attributes.GetType().ToString());
        }

        public override void Initialize(bool IsComplete = true)
        {
            base.Initialize(IsComplete: false);
            ((DayAheadAttributes)this.Attributes).SetBlockTypesFromDB();
            Default_Prices = new DefaultPrices();
            CreateLocalMarket((decimal)((MarketAttributes)this.Attributes).Upper_Price_Bound, (decimal)((MarketAttributes)this.Attributes).Lower_Price_Bound);

            if (((DayAheadAttributes)this.Attributes).WithMarketCoupling)
            {
                Scenario_Repo = new MCC_Scenario_Repository(((DayAheadAttributes)this.Attributes).MCC_ID);
                ScenarioMAs = Scenario_Repo.GetScenarioCountries();
                CreateExternalMarketAreas(this.EventTimeframe.TimeStamp, this.Scenario_Repo.TotalID);
            }

            InitialiseAuctions();

            if (((DayAheadAttributes)this.Attributes).WithMarketCoupling)
                CreateMarketCouplings(((MarketAttributes)this.Attributes).LocalMACode, this.Scenario_Repo.TotalID);

            if (AreBlockHoursinAuctionDuration() && AreBlockLevelsWithoutDuplicatedBlockHours())
                CreateBlocks();
            else
                Log.Error("One or more Blocks not suitable for Day Ahead Auction.");

            if (IsComplete) Log.Info($"{this.GetType()} initialized.");
        }

        private bool AreBlockHoursinAuctionDuration() 
            => ((DayAheadAttributes)this.Attributes)._BlockTypes.SelectMany(item => item.GetBlockHours()).Any(hour => hour < this.EventTimeframe.TimeStamp.Hour || hour > this.EventTimeframe.TimeStamp.Add(this.EventTimeframe.Duration).Hour);

        private bool AreBlockLevelsWithoutDuplicatedBlockHours()
        {
            int End = ((DayAheadAttributes)this.Attributes)._BlockTypes.Select(item => item.GetLevel()).Max();
            for (int level = 1; level <= End; level++)
            {
                var BlockHoursLevel = ((DayAheadAttributes)this.Attributes)._BlockTypes.Where(block => block.GetLevel() == level).SelectMany(block => block.GetBlockHours());
                if (BlockHoursLevel.GroupBy(hour => hour, (hour, elements) => elements.Count() > 1).Any(IsDuplicatedBlockHour => IsDuplicatedBlockHour))
                    return false;
            }
            return true;
        }

        private void InitialiseAuctions()
        {
            Auctions = new Dictionary<int, Dictionary<MarketAreaEnum, MA_Auction>>();
            Enumerable.Range(0, 24).ToList().ForEach(hour => {
                Dictionary<MarketAreaEnum, MA_Auction> AuctionsHour = new Dictionary<MarketAreaEnum, MA_Auction>();
                if (((DayAheadAttributes)this.Attributes).WithMarketCoupling)
                {
                    foreach (var ma_Code in ScenarioMAs)
                    {
                        MarketAreaUP Market = ExternalMarkets[ma_Code];
                        DateTime TimeStamp = this.EventTimeframe.TimeStamp.AddHours(hour);
                        AuctionsHour.Add(ma_Code, new MA_Auction(TimeStamp, Market));
                    }
                }
                MA_Auction auction = new MA_Auction(this.EventTimeframe.TimeStamp.AddHours(hour), LocalMarket);
                AuctionsHour.Add(LocalMarket.MA_Code, auction);
                Auctions.Add(hour, AuctionsHour);
            });
            LocalAuctions = Auctions.Select(auction => (auction.Key, auction.Value[LocalMarket.MA_Code])).ToDictionary(auction => auction.Key, auction => auction.Item2);
        }

        private void CreateLocalMarket(decimal upper_Pricebound, decimal lower_Pricebound)
            => LocalMarket = new LocalMarketArea(upper_Pricebound, lower_Pricebound, ((MarketAttributes)this.Attributes).LocalMACode, MarketAreaUP.MarketType.DayAhead, this.EventTimeframe, Default_Prices);

        public List<(MarketAreaEnum Country_From, MarketAreaEnum Country_To, decimal Capacity, decimal Losses)> GetTransferCapacities()
        {
            ExportCapacities_Repository TransferCapacities = new ExportCapacities_Repository((int)Scenario_Repo.GetImExFixID(), (int)Scenario_Repo.GetC_FuncID());
            return TransferCapacities.GetExportCapacities();
        }

        public List<(MarketAreaEnum Country_From, MarketAreaEnum Country_To, decimal Capacity, decimal windProgFrom)> GetCFunctions()
        {
            ExportCapacities_Repository CFunctions = new ExportCapacities_Repository((int)Scenario_Repo.GetImExFixID(), (int)Scenario_Repo.GetC_FuncID());
            return CFunctions.GetExportCapacitiesCFunctions();
        }

        private void CreateMarketCouplings(MarketAreaEnum nameLocalMG, int scenarioID)
        {
            MarketCouplings = new Dictionary<int, MarketCoupling>();
            var ExportCapacities = GetTransferCapacities();
            var CFunctions = GetCFunctions();
            Enumerable.Range(0, 24).ToList().ForEach(hour =>
            {
                DateTime TimeStamp = this.EventTimeframe.TimeStamp.AddHours(hour);
                MarketCoupling MarketCoupling = new MarketCoupling(TimeStamp, ((MarketAttributes)this.Attributes).LocalMACode, Auctions[hour], ExportCapacities, CFunctions);
                MarketCouplings.Add(hour, MarketCoupling);
            });
        }

        /// <summary>
        /// Erzeuge externe Märkte
        /// </summary>
        /// <param name="dateCounter"></param>
        /// <param name="totalID"></param>
        private void CreateExternalMarketAreas(DateTime startDate, int scenarioID)
        {
            var IDs = Scenario_Repo.GetExternalMarketParameters();
            ExternalMarkets = new Dictionary<MarketAreaEnum, ExternalMarketArea>();
            ScenarioMAs.ForEach(Country => ExternalMarkets.Add(Country, new ExternalMarketArea((decimal)((MarketAttributes)this.Attributes).Upper_Price_Bound, (decimal)((MarketAttributes)this.Attributes).Lower_Price_Bound, Country, IDs, MarketAreaUP.MarketType.DayAhead, EventTimeframe, Default_Prices)));
        }

        ///<summary>
        ///Methode um die Verfügbaren Blockgebote zu Konstruieren.
        ///Aktuell darauf ausgerichtet, dass nur EINEN Parent Block gibt.
        ///Sollen mehrere ParentBlocks berücksichtigt werden muss diese Methode und die Methode AddBlocKBid angepasst werden.
        ///Auch sind die Verfügbaren Blöcke innerhalb der Methode "Hard" kodiert. Soll eine Variabilität erreicht werden müssen die beiden Funktionen 
        ///Ebenfalls angepasst werden.
        ///Zu beachten ist zudem, dass eine Änderung Auswirkungen auf die Gebotsfunktionen der einzelnen Agenten hat.
        /// </summary>
        private void CreateBlocks()
        {
            BlockHandler = new BlockHandler(
                LocalAuctions,
                MarketCouplings,
                ((DayAheadAttributes)this.Attributes).WithMarketCoupling,
                ((DayAheadAttributes)this.Attributes)._BlockTypes,
                (decimal)((MarketAttributes)this.Attributes).Upper_Price_Bound,
                (decimal)((MarketAttributes)this.Attributes).Lower_Price_Bound);
        }
        #endregion

        #region Je Auctionsereignis
        protected override void ExecuteAuction(out List<winMoz.Markets.Results.MarketResult> results)
        {
            // Platzhalter
            BlockHandler.Reset();
            LocalMarket.SetEventTimeframe(EventTimeframe);
            LocalMarket.ClearBidsSortedAuctionTime();

            results = new List<winMoz.Markets.Results.MarketResult>();

            var BidWithNanPrice = Bids.Where(item => item.Segments.Any(seg => float.IsNaN(seg.Price) )).ToList();

            SetNewWindProg();

            PutBidsToMarket();

            //if (WithDebugBids)
            //{
            //    var bids = DebugMarketObjectsGenerator.DebugAddRandomBidsDA(GetAuctionArgs(), 100);
            //    foreach (var bid in bids) AddBidToMarket(bid.Price, bid.Volume, bid.Hour);
            //    var blockBids = DebugMarketObjectsGenerator.DebugAddRandomBlockBidsDA(10);
            //    foreach (var bid in blockBids) AddBlockBid(bid.BlockID, bid.Price, bid.Volume);
            //}

            CalcInitialAuctionResults(EventTimeframe.TimeStamp);

            PrepareBlockList();
            BlockResults = GetResultBlockBids();

            GatherHourResults(ref results);
            GatherBlockResults(ref results);

            //Für eventuelle Plots und Outputs der CoupledMeritOrders
            PlotAndOutput.Plot_CoupledMeritOrder(this.EventTimeframe.TimeStamp, this.Auctions.Select(auction => (auction.Key, auction.Value.Select(auctionMA => (auctionMA.Key, auctionMA.Value.MeritOrder.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList(), (float)auctionMA.Value.V_Start, (float)auctionMA.Value.V_Result)).ToList())).ToList());
            PlotAndOutput.OutputConsole_CoupledMeritOrder(this.EventTimeframe.TimeStamp, this.Auctions.Select(auction => (auction.Key, auction.Value.Select(auctionMA => (auctionMA.Key, auctionMA.Value.MeritOrder.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList(), (float)auctionMA.Value.V_Start, (float)auctionMA.Value.V_Result)).ToList())).ToList());
            PlotAndOutput.OutputCSV_CoupledMeritOrder(this.EventTimeframe.TimeStamp, this.Auctions.Select(auction => (auction.Key, auction.Value.Select(auctionMA => (auctionMA.Key, auctionMA.Value.MeritOrder.Select(item => ((float)item.Volume, (float)item.Price, (float)item.AggVol)).ToList(), (float)auctionMA.Value.V_Start, (float)auctionMA.Value.V_Result)).ToList())).ToList());

            //Berechnung der "Wirtschaftlichkeit" der Monatsblöcke
            if (this.EventTime.IsLastDayOfMonth())
                PublishMonthlyBlocks();

        }

        public void SetNewWindProg()
        {
            var WindProg = BidsAuction.Where(item => item.Assets.First().GetType().Name == "Plant_Wind" || item.Assets.First().GetType().Name == "Plant_Wind_Aggregate").SelectMany(item => item.Segments).GroupBy(item => item.TimeStamp, (el1, el2) => (el1, el2.Select(item => item.VolumeMW).Sum())).ToDictionary(item => item.el1, item => (decimal)item.Item2);
            var Times = Enumerable.Range(0, (int)(EventTimeframe.Duration.TotalMinutes / EventTimeframe.Step.TotalMinutes)).Select(item => this.EventTimeframe.TimeStamp.AddMinutes(item * this.EventTimeframe.Step.TotalMinutes)).ToList();
            foreach(var time in Times)
            {
                if (!WindProg.ContainsKey(time))
                    WindProg.Add(time, 0);
            }
            this.ExternalMarkets.Values.ToList().ForEach(item => item.SetWindProgListDE(WindProg));
            this.LocalMarket.SetWindProgListDE(WindProg);
        }

        public void CalcInitialAuctionResults(DateTime auctionTime)
        {
            foreach (int hour in Auctions.Keys)
            {
                foreach (var ma in Auctions[hour].Keys)
                {
                    var marketAuction = Auctions[hour][ma];
                    DateTime timeStamp = auctionTime.AddHours(hour);
                    marketAuction.MarketHandler.UpdateMeritOrder(marketAuction, timeStamp);
                }
            }
        }

        private void PutBidsToMarket()
        {
            var Volumes = Bids.Select(item => (item.Assets.First().GetType().ToString(), item.Segments.First().VolumeMW)).ToList();
            BidsAuction.ForEach(bid => bid.Segments.ToList().ForEach(seg =>
            {
                if (seg.IsHour) //Wenn einzelnes Gebot
                    //if (seg.IsBuy)
                    AddBidToMarket(Convert.ToDecimal(seg.Price), Convert.ToDecimal(seg.VolumeMW), seg.TimeStamp);
                    //else
                    //    AddBidToMarket(Convert.ToDecimal(seg.Price), Convert.ToDecimal(seg.VolumeMW), seg.TimeStamp);
                else
                    //if (seg.IsBuy)
                    AddBlockBid(seg.ShortCode, Convert.ToDecimal(seg.Price), Convert.ToDecimal(-seg.VolumeMW));
                    //else
                    //    AddBlockBid(seg.ShortCode, Convert.ToDecimal(seg.Price), Convert.ToDecimal(seg.VolumeMW));
            }));
        }

        /// <summary>
        /// Hinzufügen eines Blockgebots
        /// </summary>
        /// <param name="BlockID"></param>
        /// <param name="price"></param>
        /// <param name="volume"></param>
        public void AddBlockBid(ShortCode code, decimal price, decimal volume)
        {
            if (code.IsDayAheadBlock())
                BlockHandler.AddBlockBidToBlock(code, price, volume < 0 ? -volume : volume, volume < 0 ? true : false);
        }

        public void PrepareBlockList() => BlockHandler.PrepareBlocks();

        /// <summary>
        /// Hinzufügen von Geboten zum Markt
        /// </summary>
        /// <param name="Preis"></param>
        /// <param name="Menge"></param>
        /// <param name="Stunde"></param>
        public void AddBidToMarket(decimal Price, decimal Volume, DateTime Stunde)
            => LocalMarket.AddBidToListSortedAuction(Price, Volume, Stunde.Hour);
    
        public Dictionary<ShortCode, float> GetResultBlockBids() => BlockHandler.CalculateMarketResult();

        /// <summary>
        /// Sammeln der Marktergebnisse
        /// </summary>
        public void GatherHourResults(ref List<MarketResult> results)
        {
            if (this.EventTimeframe.TimeStamp.Day == 5)
            {

            }
            foreach (MA_Auction auction in LocalAuctions.Values)
            {
                //if (!auction.AuctionFailed())
                if (!auction.IsMeritOrderEmpty())
                {
                    auction.CalcVolumePurchaseSellOfLastMeritOrderAggBid();
                    //auction.CorrectVolumePriceStepBids();
                    var Volume = auction.Volume_Market_Start + auction.V_Result - auction.V_Start;
                    results.Add(new DayAheadResult(auction.TS, ShortCode.DayAhead.Hour(auction.TS), (float)Math.Round(auction.P_Result, 3), (float)Math.Round(Volume, 3), (float)auction.VolumeLastPurchasePart, (float)auction.VolumeLastSellPart));
                }
                else
                {
                    //var EndDuration = AuctionTimeframe.TimeStamp.Add(AuctionTimeframe.Duration);
                    //var BidsAuction = Bids.Where(bid => bid.TimeStamp >= AuctionTimeframe.TimeStamp && bid.TimeStamp < EndDuration).ToList();
                    if (BidsAuction.Count() > 0)
                    {
                        var BidSegmentsPurchase = BidsAuction.SelectMany(item => item.Segments).Where(item => item.IsHour && item.TimeStamp == auction.TS).Where(item => item.VolumeMW > 0);
                        var BidSegmentsSell = BidsAuction.SelectMany(item => item.Segments).Where(item => item.IsHour && item.TimeStamp == auction.TS).Where(item => item.VolumeMW < 0);
                        if (BidSegmentsPurchase.Count() != 0) results.Add(new DayAheadResult(auction.TS, BidSegmentsPurchase.First().ShortCode, price: ((MarketAttributes)this.Attributes).Upper_Price_Bound, 0, 0, 0));
                        else if (BidSegmentsSell.Count() != 0) results.Add(new DayAheadResult(auction.TS, BidSegmentsSell.First().ShortCode, price: ((MarketAttributes)this.Attributes).Lower_Price_Bound, 0, 0, 0));
                        else results.Add(Default_Prices.GetDefaultDayAheadResult(auction.TS, EventTimeframe.Step));
                    }
                    else
                        results.Add(Default_Prices.GetDefaultDayAheadResult(auction.TS, EventTimeframe.Step));
                }
            }
            //TODO: Hier müssen noch die Ergebnisse gelöscht werden, deren Zeitpunkt in der Vergangenheit liegt.
            foreach (var auctionsTime in Auctions)
            {
                DateTime timeStamp = auctionsTime.Value.First().Value.TS;
                List<ExternalMarketResult> resultsTime = new List<ExternalMarketResult>();
                foreach (var auction in auctionsTime.Value)
                {
                    if (!auction.Value.AuctionFailed())
                        resultsTime.Add(new ExternalMarketResult(timeStamp, auction.Key, (float)Math.Round((float)auction.Value.P_Result, 2), (float)Math.Round((float)(auction.Value.MaxExSave.Where(item => item.WindProgFrom <= auction.Value.WindProgDE).Last().Capacity - auction.Value.MaxEx), 2), (float)Math.Round((float)(auction.Value.MaxImSave.Where(item => item.WindProgFrom <= auction.Value.WindProgDE).Last().Capacity - auction.Value.MaxIm), 2), (float)Math.Round((float)auction.Value.VerschiebeVolumen, 2)));
                    else
                        resultsTime.Add(new ExternalMarketResult(timeStamp, auction.Key, (float)Math.Round((float)auction.Value.P_Result, 2), (float)Math.Round((float)(auction.Value.MaxExSave.Where(item => item.WindProgFrom <= auction.Value.WindProgDE).Last().Capacity - auction.Value.MaxEx), 2), (float)Math.Round((float)(auction.Value.MaxImSave.Where(item => item.WindProgFrom <= auction.Value.WindProgDE).Last().Capacity - auction.Value.MaxIm), 2), (float)Math.Round((float)auction.Value.VerschiebeVolumen, 2)));
                }
                if (((DayAheadAttributes)this.Attributes).WithMarketCoupling)
                    ExternalMarketResultRepository.Add(resultsTime);
            }
        }

        public void GatherBlockResults(ref List<MarketResult> results)
        {
            foreach(var result in BlockResults)
                results.Add(new DayAheadResult(EventTimeframe.TimeStamp, result.Key, result.Value, 0.0F));
        }
        #endregion
    }
}
