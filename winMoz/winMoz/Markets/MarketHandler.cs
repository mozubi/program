﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;

namespace winMoz.Markets
{
    /// <summary>
    /// <see cref="MarketHandler"/>-Klasse wickelt alle Aktionen von und für Märkte über die Simulationszeit ab
    /// </summary>
    public sealed class MarketHandler
    {
        public List<IMarket> Markets { get; private set; } = new List<IMarket>();
        public MarketHandler(params IMarket[] markets)
        {
            this.Markets.AddRange(markets);
        }
        public void AddMarket(Market market) => this.Markets.Add(market);
        public void AddMarket(IEnumerable<Market> markets) => this.Markets.AddRange(markets);

        public IMarket GetMarket<TMarket, TAttributes>() where TMarket : Market => this.Markets.FirstOrDefault(market => market.GetType() == typeof(TMarket));

        public void RunMarkets(DateTime timeStamp) => Markets.ForEach(market => market.HandleAuction(timeStamp));
    }
}
