﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Information;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;

namespace winMoz.Markets
{
    public class IntradayMarket : Market
    {
        protected IntradayMarket() { }
        public IntradayMarket(IntradayAttributes attributes, int scenarioID = 0) : base(attributes, scenarioID) { }


        #region Auktionierungsmethoden
        protected override void ExecuteAuction(out List<MarketResult> results)
        {
            results = new List<MarketResult>();
            var qhpfc = Info.QHPFC.Get(EventTimeframe.TimeStamp, EventTimeframe.Duration, 0);
            results.AddRange(qhpfc.Select(el => new IntraDayResult(el.TimeStamp, ShortCode.IntraDay.QuarterHour(el.TimeStamp), el.PricePrediction, 0.0F)));
        }
        #endregion
    }
}
