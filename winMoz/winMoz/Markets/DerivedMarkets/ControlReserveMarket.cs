﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using winMoz.Markets.Results;
using winMoz.Markets.Bids;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.Elements;
using winMoz.Helper;
using winMoz.Assets;
using winMoz.Simulation;
using System.Threading;
using winMoz.Tools.Viz;


namespace winMoz.Markets
{
    /// <summary>
    /// Klasse für Regelleistungsmarkt (abgeleitet von Market)
    /// </summary>
    public abstract class ControlReserveMarket : Market
    {
        #region Attributes
        bool WithDebugBids = false;

        protected List<Bid_Segment> SegmentsAuction;

        public enum ControlReserveAuctionType
        {
            CAPACITY, ENERGY, ACTIVATION
        }

        #endregion

        #region Initialization
        /// <summary>
        /// Konstruktor der Klasse
        /// </summary>
        /// <param name="marketResults">Vergangene Marktergebnisse</param>
        protected ControlReserveMarket() { }
        public ControlReserveMarket(MarketAttributes attributes, int scenarioID = 0) : base(attributes, scenarioID)
        {
            if (Attributes == null) throw new ArgumentNullException(Attributes.GetType().ToString());
        }

        #endregion

        #region Auction

        protected List<(DateTime TimeStamp, float Volume, float Price)> GetSchedule(DateTime TimeStamp, float volume, float price)
        {
            List<(DateTime TimeStamp, float Volume, float Price)> schedule = new List<(DateTime TimeStamp, float Volume, float Price)>();
            for (int qh = 0; qh < 4; qh++)
            {
                DateTime time = TimeStamp.AddMinutes(qh * 15);
                schedule.Add((time, volume, price));
            }
            return schedule;
        }



        protected override void ExecuteAuction(out List<MarketResult> results)
        {
            results = new List<MarketResult>();

            foreach (int controlReserveType in typeof(ControlReserveShortCode.ShortCode).GetEnumValues())
            {
                SegmentsAuction = GetSegmentsForAuction((ControlReserveShortCode.ShortCode)controlReserveType).ToList();

                var Results = SegmentsAuction.Count() > 0 ? CalcResults((ControlReserveShortCode.ShortCode)controlReserveType) : new List<ControlReserveResult>();

                results.AddRange(Results);
            }
        }


        protected void PlotAndOutputMeritOrder(List<Bid_Segment> segmentsTime, DateTime time, ControlReserveShortCode.ShortCode controlReserveType)
        {
            var AggregatedMeritOrders = Helper.ControlReserve_Extensions.GetAggregatedMeritOrders(segmentsTime, this.Attributes.Upper_Price_Bound, this.Attributes.Lower_Price_Bound, isSellDescending: controlReserveType.ToString().Contains("n_"));
            PlotAndOutput.Plot_MeritOrder(this, time, controlReserveType, AggregatedMeritOrders);
            PlotAndOutput.OutputConsole_MeritOrder(this, time, controlReserveType, AggregatedMeritOrders);
            PlotAndOutput.OutputCSV_MeritOrder(this, time, controlReserveType, AggregatedMeritOrders);
        }

        protected abstract List<ControlReserveResult> CalcResults(ControlReserveShortCode.ShortCode controlReserveType);

        private ProcessArgs GetProcessArgs(IEnumerable<Bid_Segment> Bid_Segments, ControlReserveShortCode.ShortCode controlReserveType)
        {
            ProcessArgs args = new ProcessArgs(EventTimeframe.TimeStamp, EventTimeframe.Duration, EventTimeframe.Step, Bid_Segments, controlReserveType);
            return args;
        }

        protected abstract IEnumerable<Bid_Segment> GetSegmentsForAuction(ControlReserveShortCode.ShortCode controlReserveType);
        #endregion
    }
}
