﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;

namespace winMoz.Markets
{
    /// <summary>
    /// Futures-Marktklasse zur Durchführung von monatlichen Termingeschäften erbt von der <see langword="abstract"/> <see cref="Market"/>-Klasse
    /// </summary>
    public class FuturesMarket : Market
    {
        private List<Bid_DayAhead> underlyings = new List<Bid_DayAhead>();
        private int LastMonth;

        protected FuturesMarket() : base() { }
        public FuturesMarket(MarketAttributes attributes, int scenarioID = 0) 
            : base(attributes, scenarioID)
        {
        }


        #region Marktaktionen
        /// <summary>
        /// Implementierung der <see langword="abstract"/> <see cref="Market.ExecuteAuction"/>-Methode zur Ausführung der Auktionierung nach Sammlung aller Gebote 
        /// </summary>
        protected override void ExecuteAuction(out List<MarketResult> results)
        {
            results = new List<MarketResult>();
        }

        #endregion



        public override void SubmitBid(Bid bid)
        {
            base.SubmitBid(bid);
            //this.underlyings.AddRange(this.Bids.Cast<Bid_Future>().SelectMany(el => el.Underlyings));
        }

        #region Interaktion - DayAhead-Markt
        ///// <summary>
        ///// Methode stellt unlimitierte Gebote entsprechend der Futures-Bids am DayAhead-Markt ein
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="auctionArgs"></param>
        //private void DayAheadMarket_Auction(object sender, AuctionArgs auctionArgs)
        //{
        //    this.underlyings.Where(bid => bid.TimeStamp == auctionArgs.TimeStamp).ToList().ForEach(bid => this.dayAhead.SubmitBid(bid));
        //}

        #endregion
    }
}
