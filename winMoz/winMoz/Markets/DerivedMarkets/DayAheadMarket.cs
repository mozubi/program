﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;
using winMoz.Markets.Results;
using winMoz.Simulation;

namespace winMoz.Markets
{
    /// <summary>
    /// Verbindungsklasse zwischen <seealso cref="MarketHandler"/> und MarketEvents und <seealso cref="DayAheadMarket"/> als Kontrollklasse über die jeweiligen stündlichen Marktgebiete,+
    /// die über Blockgebote und Markkoppelung miteinander Verbunden sind.
    /// </summary>
    public class DayAheadMarket : Market
    {
        internal event EventHandler<IEnumerable<MarketResult>> PublishMonthlyResults;

        protected DayAheadMarket() { }
        public DayAheadMarket(DayAheadAttributes attributes, int scenarioID = 0) : base(attributes, scenarioID) { }

        #region Auktionierungsmethoden
        protected override void ExecuteAuction(out List<MarketResult> results)
        {
            results = new List<MarketResult>();

            //Rückgabewert ist immer der Wert der HPFC, also der erwartete Preis
            var hpfc = Info.HPFC.Get(EventTimeframe.TimeStamp, seed: 0);

            foreach (var price in hpfc)
                results.Add(new DayAheadResult(price.TimeStamp, ShortCode.DayAhead.Hour(price.TimeStamp.Hour), price.PricePrediction, 0.0F));

            results.AddRange(GetDailyBlocks(results));

            //Berechnung der "Wirtschaftlichkeit" der Monatsblöcke
            if (this.EventTime.IsLastDayOfMonth())
                PublishMonthlyBlocks();
        }

 
        protected void PublishMonthlyBlocks()
        {
            var monthblocks = GetMonthlyBlocks();
            this.PublishMonthlyResults?.Invoke(this, monthblocks);
        }

        #region Result-Calculations
        protected List<DayAheadResult> GetDailyBlocks(IEnumerable<MarketResult> hourResults)
        {
            var blockResults = new List<DayAheadResult>();
            if (hourResults.Count() == 24)
            {
                blockResults.Add(new DayAheadResult(this.EventTimeframe.TimeStamp, ShortCode.DayAhead.Base, hourResults.Average(el => el.Price), hourResults.Average(el => el.Volume)));
                blockResults.Add(new DayAheadResult(this.EventTimeframe.TimeStamp, ShortCode.DayAhead.OffPeak, hourResults.Where(el => el.TimeStamp.IsOffPeakTime()).Average(el => el.Price), hourResults.Where(el => el.TimeStamp.IsOffPeakTime()).Average(el => el.Volume)));

                if (hourResults.First().TimeStamp.IsWeekDay())
                    blockResults.Add(new DayAheadResult(hourResults.First().TimeStamp, ShortCode.DayAhead.Peak, hourResults.Where(el => el.TimeStamp.IsPeakTime()).Average(el => el.Price), hourResults.Where(el => el.TimeStamp.IsPeakTime()).Average(el => el.Volume)));
            }
            else
                throw new InvalidOperationException("Wrong number of hours");

            return blockResults;
        }

        protected IEnumerable<MarketResult> GetMonthlyBlocks()
        {
            var durationOfMonth = this.EventTime.GetDurationOfMonth();
            var startOfMonth = this.EventTime - durationOfMonth;

           return resultRepository.GetResults<DayAheadResult>(startOfMonth, durationOfMonth).Where(item => item.IsBlock).ToList();
        }
        #endregion



        #endregion
    }
}
