﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Tools.Helper;

namespace winMoz.Markets.Elements
{
    public partial class MarketAreaEnum : Enumeration
    {
        public string OtherValue { get; private set; }

        public string ToStringOther() => OtherValue;

        public static bool HasUniqueValues()
            => HasUniqueValues<MarketAreaEnum>();

        public static bool HasUniqueIds()
            => HasUniqueIds<MarketAreaEnum>();

        public static IEnumerable<MarketAreaEnum> GetAll()
            => GetAll<MarketAreaEnum>();

        public static MarketAreaEnum FromString(string value)
            => FromString<MarketAreaEnum>(value);

        public static MarketAreaEnum FromStringOrOtherString(string value)
        {
            var MAs = GetAll();
            var MAFromString = MAs.SingleOrDefault(prop => prop.Value.Equals(value));
            if (MAFromString != default) return MAFromString;
            else return MAs.SingleOrDefault(prop => prop.OtherValue.Equals(value));
        }
        public static MarketAreaEnum Default() => DE;

        public static MarketAreaEnum FromOtherString(string value)
            => GetAll().SingleOrDefault(prop => prop.OtherValue.Equals(value));

        public MarketAreaEnum(string value, int id, string otherValue = null) : base(value, id)
        {
            OtherValue = otherValue is null ? value : otherValue;
        }

        #region Variables
        public static MarketAreaEnum BE = new MarketAreaEnum("BE", 0);
        public static MarketAreaEnum CH = new MarketAreaEnum("CH", 1);
        public static MarketAreaEnum CZ = new MarketAreaEnum("CZ", 2);
        public static MarketAreaEnum DK = new MarketAreaEnum("DK", 3);
        public static MarketAreaEnum FR = new MarketAreaEnum("FR", 4);
        public static MarketAreaEnum IT = new MarketAreaEnum("IT", 5);
        public static MarketAreaEnum NL = new MarketAreaEnum("NL", 6);
        public static MarketAreaEnum SE = new MarketAreaEnum("SE", 7);
        public static MarketAreaEnum DE = new MarketAreaEnum("DE", 8);
        public static MarketAreaEnum AL = new MarketAreaEnum("AL", 9);
        public static MarketAreaEnum AT = new MarketAreaEnum("AT", 10);
        public static MarketAreaEnum BA = new MarketAreaEnum("BA", 11);
        public static MarketAreaEnum BG = new MarketAreaEnum("BG", 12);
        public static MarketAreaEnum EE = new MarketAreaEnum("EE", 13);
        public static MarketAreaEnum ES = new MarketAreaEnum("ES", 14);
        public static MarketAreaEnum FI = new MarketAreaEnum("FI", 15);
        public static MarketAreaEnum GB = new MarketAreaEnum("GB", 16, "UK");
        public static MarketAreaEnum GR = new MarketAreaEnum("GR", 17);
        public static MarketAreaEnum HR = new MarketAreaEnum("HR", 18);
        public static MarketAreaEnum HU = new MarketAreaEnum("HU", 19);
        public static MarketAreaEnum IE = new MarketAreaEnum("IE", 20);
        public static MarketAreaEnum LT = new MarketAreaEnum("LT", 21);
        public static MarketAreaEnum LV = new MarketAreaEnum("LV", 22);
        public static MarketAreaEnum ME = new MarketAreaEnum("ME", 23);
        public static MarketAreaEnum MK = new MarketAreaEnum("MK", 24);
        public static MarketAreaEnum MT = new MarketAreaEnum("MT", 25);
        public static MarketAreaEnum NI = new MarketAreaEnum("NI", 26);
        public static MarketAreaEnum NO = new MarketAreaEnum("NO", 27);
        public static MarketAreaEnum PL = new MarketAreaEnum("PL", 28);
        public static MarketAreaEnum PT = new MarketAreaEnum("PT", 29);
        public static MarketAreaEnum RO = new MarketAreaEnum("RO", 30);
        public static MarketAreaEnum RS = new MarketAreaEnum("RS", 31);
        public static MarketAreaEnum SI = new MarketAreaEnum("SI", 32);
        public static MarketAreaEnum SK = new MarketAreaEnum("SK", 33);
        public static MarketAreaEnum TN = new MarketAreaEnum("TN", 34);
        public static MarketAreaEnum TR = new MarketAreaEnum("TR", 35);
        public static MarketAreaEnum LU = new MarketAreaEnum("LU", 36);
        public static MarketAreaEnum CY = new MarketAreaEnum("CY", 37);
        public static MarketAreaEnum IL = new MarketAreaEnum("IL", 38);
        public static MarketAreaEnum IS = new MarketAreaEnum("IS", 39);
        #endregion
    }
}
