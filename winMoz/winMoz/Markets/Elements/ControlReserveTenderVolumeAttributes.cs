﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Markets.Elements
{
    public class ControlReserveTenderVolumeAttributes
    {
        public float ErrorLevel_FRR { get; set; }
        public float ErrorLevel_aFRR { get; set; }

        public ControlReserveTenderVolumeAttributes(): this(errorLevel_FRR: 0.03F / 100,
                errorLevel_aFRR: 0.2F / 100
                )
        { }
        public ControlReserveTenderVolumeAttributes(float errorLevel_FRR, float errorLevel_aFRR)
        {
            ErrorLevel_FRR = errorLevel_FRR;
            ErrorLevel_aFRR = errorLevel_aFRR;
        }
    }
}
