﻿using System.Linq;
using System.Collections.Generic;
using System;
using winMoz.Helper;
using winMoz.Markets.Bids;
using winMoz.Simulation;

namespace winMoz.Markets.Elements
{
    //Vorschlag für Umgestaltung der MarketResult Klasse, 
    //damit diese keine leeren Parameter für bestimmt Märkte enthält.
    public abstract class MarketAttributes 
    {
        public int Id {get; private set;}
        public SimEventAttributes EventAttributes { get; private set; }
        public float Upper_Price_Bound { get; private set; }
        public float Lower_Price_Bound { get; private set; }

        public MarketAreaEnum LocalMACode { get; private set; }

        public string AllowedBidType { get; private set; }

        public int NumberOfDaysToLetBidsInBidList { get; private set; }

        protected MarketAttributes()
        {

        }
        public MarketAttributes(DateTime StartDate, MarketAreaEnum localMACode)
        {
            EventAttributes = new SimEventAttributes(start: StartDate,
                             startEventTime: StartDate,
                             startDeterminationTime: StartDate.AddDays(1),
                             determinationDuration: new TimeSpanSave(0, 1, 0, 0, 0),
                             determinationStep: new TimeSpanSave(0, 0, 0, 1, 0));
            LocalMACode = localMACode;
            AllowedBidType = typeof(Bid_Future).ToString();
            NumberOfDaysToLetBidsInBidList = 1;
            Upper_Price_Bound = 9999.99F;
            Lower_Price_Bound = -9999.99F;
        }

        public MarketAttributes(SimEventAttributes eventAttributes, MarketAreaEnum localMACode, string allowedBidType, int numberOfDaysToLetBidsInBidList, params float[] priceBounds)
        {
            EventAttributes = eventAttributes;
            this.Upper_Price_Bound = priceBounds.Max();
            this.Lower_Price_Bound = priceBounds.Min();
            LocalMACode = localMACode;
            AllowedBidType = allowedBidType;
            NumberOfDaysToLetBidsInBidList = numberOfDaysToLetBidsInBidList;
        }
    }
    public class FuturesAttributes: MarketAttributes
    {
        protected FuturesAttributes()
        {

        }
        public FuturesAttributes(DateTime StartDate, MarketAreaEnum localMACode) : base(StartDate, localMACode) { }

        public FuturesAttributes(SimEventAttributes eventAttributes, MarketAreaEnum localMACode, string allowedBidType, int numberOfDaysToLetBidsInBidList, params float[] priceBounds):
            base(eventAttributes, localMACode, allowedBidType, numberOfDaysToLetBidsInBidList, priceBounds)
        { }
    }

    public class MarketWithBlocksAttributes : MarketAttributes
    {
        public string BlockTypes { get; set; }
        public List<ShortCode> _BlockTypes { get; set; }

        protected MarketWithBlocksAttributes()
        {

        }
        public MarketWithBlocksAttributes(DateTime StartDate, MarketAreaEnum localMACode, List<ShortCode> blockTypes): base(StartDate, localMACode)
        {
            SetBlockTypes(blockTypes);
        }
        public MarketWithBlocksAttributes(SimEventAttributes eventAttributes, MarketAreaEnum localMACode, string allowedBidType, int numberOfDaysToLetBidsInBidList, List<ShortCode> blockTypes, params float[] priceBounds) : base(eventAttributes, localMACode, allowedBidType, numberOfDaysToLetBidsInBidList, priceBounds)
        {
            SetBlockTypes(blockTypes);
        }

        public virtual void SetBlockTypes(List<ShortCode> blockTypes = null)
        {
            _BlockTypes = blockTypes != null ? blockTypes : new List<ShortCode>()
            {
            };
            BlockTypes = string.Join(";", _BlockTypes);
        }

        public virtual void SetBlockTypesFromDB()
        {
            var blockTypesStrings = BlockTypes.Split(';');
            if (blockTypesStrings.Count() > 0)
                _BlockTypes = blockTypesStrings.Select(item => ShortCode.FromString(item)).ToList();
        }

    }

    public class DayAheadAttributes : MarketWithBlocksAttributes
    {
        public bool WithMarketCoupling { get; set; } = true;

        public int MCC_ID { get; set; }

        protected DayAheadAttributes() { }

        public DayAheadAttributes(SimEventAttributes eventAttributes, MarketAreaEnum localMACode, string allowedBidType, int numberOfDaysToLetBidsInBidList, List<ShortCode> blockTypes, bool withMarketCoupling, int mcc_ID, params float[] priceBounds) : base(eventAttributes, localMACode, allowedBidType, numberOfDaysToLetBidsInBidList, blockTypes, priceBounds)
        {
            WithMarketCoupling = withMarketCoupling;
            MCC_ID = mcc_ID;
        }

        public DayAheadAttributes(DateTime StartDate, MarketAreaEnum localMACode) :
            this(new SimEventAttributes(start: StartDate,
                             startEventTime: new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 12, 0, 0),
                             startDeterminationTime: StartDate.AddDays(1),
                             determinationDuration: new TimeSpanSave(0, 0, 1, 0, 0),
                             determinationStep: new TimeSpanSave(0, 0, 0, 1, 0)),
                      localMACode,
                      allowedBidType: typeof(Bid_DayAhead).ToString(),
                      numberOfDaysToLetBidsInBidList: 1,
                      blockTypes: null, //null cause default is set in SetBlockTypes
                      withMarketCoupling: true,
                      mcc_ID: 100000,
                      new float[] {3000,
                             -500 })

        {

        }

        public override void SetBlockTypes(List<ShortCode> blockTypes = null)
        {
            _BlockTypes = blockTypes != null ? blockTypes : new List<ShortCode>() {
                ShortCode.DayAhead.Base,
                ShortCode.DayAhead.Peak,
                ShortCode.DayAhead.OffPeak1,
                ShortCode.DayAhead.OffPeak2,
                };
            BlockTypes = string.Join(";", _BlockTypes);
        }

        public override void SetBlockTypesFromDB()
        {
            _BlockTypes = BlockTypes.Split(';').Select(item => ShortCode.FromString(item)).ToList();
        }
    }

    public class IntradayAttributes : MarketWithBlocksAttributes
    {
        protected IntradayAttributes() { }

        public IntradayAttributes(SimEventAttributes eventAttributes,  MarketAreaEnum localMACode, string allowedBidType, int numberOfDaysToLetBidsInBidList, List<ShortCode> blockTypes, params float[] priceBounds) : base(eventAttributes, localMACode, allowedBidType, numberOfDaysToLetBidsInBidList, blockTypes, priceBounds)
        {
            
        }

        public IntradayAttributes(DateTime StartDate, MarketAreaEnum localMACode) :
            this(new SimEventAttributes(start: StartDate,
                             startEventTime: new DateTime(StartDate.AddDays(1).Year, StartDate.AddDays(1).Month, StartDate.AddDays(1).Day, StartDate.AddDays(1).Hour, 0, 0),
                             startDeterminationTime: StartDate.AddDays(1),
                             determinationDuration: new TimeSpanSave(0, 0, 0, 1, 0),
                             determinationStep: new TimeSpanSave(0, 0, 0, 0, 15)),
                    localMACode: localMACode,
                    allowedBidType: typeof(Bid_Intraday).ToString(),
                    numberOfDaysToLetBidsInBidList: 1,
                    blockTypes: null,
                    new float[] {9999.0F,
                             -9999.0F })
                
        {

        }
    }

    public class ControlReserve_Capacity_Attributes : MarketAttributes
    {
        protected ControlReserve_Capacity_Attributes() { }
        public ControlReserve_Capacity_Attributes(SimEventAttributes eventAttributes, MarketAreaEnum localMACode, string allowedBidType, int numberOfDaysToLetBidsInBidList, params float[] priceBounds) : base(eventAttributes, localMACode, allowedBidType, numberOfDaysToLetBidsInBidList, priceBounds)
        {
        }

        public ControlReserve_Capacity_Attributes(DateTime StartDate, MarketAreaEnum localMACode) :
           this(new SimEventAttributes(start: StartDate,
                             startEventTime: new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 9, 0, 0),
                             startDeterminationTime: StartDate.AddDays(1),
                             determinationDuration: new TimeSpanSave(0, 0, 1, 0, 0),
                             determinationStep: new TimeSpanSave(0, 0, 0, 4, 0)),
                   localMACode: localMACode,
                   allowedBidType: typeof(Bid_ControlReserve_Capacity).ToString(),
                   numberOfDaysToLetBidsInBidList: -1,
                   new float[] {9999.0F,
                             -9999.0F })

        {

        }
    }

    public class ControlReserve_Energy_Attributes : MarketAttributes
    {
        public bool OnlyBidsFromCapacityAllowed { get; set; }

        protected ControlReserve_Energy_Attributes() { }
        public ControlReserve_Energy_Attributes(SimEventAttributes eventAttributes, MarketAreaEnum localMACode, string allowedBidType, int numberOfDaysToLetBidsInBidList, bool onlyBidsFromCapacityAllowed, params float[] priceBounds) : base(eventAttributes, localMACode, allowedBidType, numberOfDaysToLetBidsInBidList, priceBounds)
        {
            OnlyBidsFromCapacityAllowed = onlyBidsFromCapacityAllowed;
        }
        public ControlReserve_Energy_Attributes(DateTime StartDate, MarketAreaEnum localMACode) :
           this(new SimEventAttributes(start: StartDate,
                             startEventTime: new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 23, 0, 0),
                             startDeterminationTime: StartDate.AddDays(1),
                             determinationDuration: new TimeSpanSave(0, 0, 0, 4, 0),
                             determinationStep: new TimeSpanSave(0, 0, 0, 4, 0)),
                   localMACode: localMACode,
                   allowedBidType: typeof(Bid_ControlReserve_Energy).ToString(),
                   numberOfDaysToLetBidsInBidList: -2,
                   onlyBidsFromCapacityAllowed: true,
                   new float[] {9999.0F,
                             -9999.0F })

        {

        }
    }
}
