﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Simulation;

namespace winMoz.Markets.Elements
{
    public class ActivationAttributes 
    {
        public SimEventAttributes EventAttributes { get; set; }
        public TimeSpan AuctionStep { get; set; }

        public float Upper_Price_Bound { get; set; }
        public float Lower_Price_Bound { get; set; }

        protected ActivationAttributes() { }
        public ActivationAttributes(SimEventAttributes eventAttributes, TimeSpan auctionStep, float Upper_Price_Bound, float Lower_Price_Bound)
        {
            EventAttributes = eventAttributes;
            AuctionStep = auctionStep;
            this.Upper_Price_Bound = Upper_Price_Bound;
            this.Lower_Price_Bound = Lower_Price_Bound;
        }
        public ActivationAttributes(DateTime StartDate): this(new SimEventAttributes(start: StartDate,
                startEventTime: new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0).AddDays(2),
                startDeterminationTime: new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0).AddDays(1),
                determinationDuration: new TimeSpanSave(0, 0, 1, 0, 0),
                determinationStep: new TimeSpanSave(0, 0, 0, 0, 15)),
                auctionStep: new TimeSpan(4, 0, 0),
                Upper_Price_Bound: 9999,
                Lower_Price_Bound: -9999)

        {
        }
    }
}
