﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Simulation;

namespace winMoz.Markets.Elements
{
    public class MFRR_Attributes : SimObject
    {
        //public int Id { get; private set; }
        /// <summary>
        /// Minutenreserve wird ab diesem Grenzwert abgerufen
        /// </summary>
        public float mFRR_Threshold { get; private set; }
        /// <summary>
        /// Faktor zu dem die Regelleistung durch Minutenreserve abgedeckt werden soll.
        /// </summary>
        public float mFRR_Factor { get; private set; }

        private MFRR_Attributes() { }

        public MFRR_Attributes(float mfrr_Threshold, float mfrr_Factor, int scenarioID = 0) : base(scenarioID)
        {
            mFRR_Threshold = mfrr_Threshold;
            mFRR_Factor = mfrr_Factor;
        }
    }
}
