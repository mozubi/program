﻿using System;
using System.Collections.Generic;
using winMoz.Markets.Bids;
using winMoz.Helper;
using winMoz.Markets;

namespace winMoz.Markets.Elements
{
    public class ProcessArgs 
    {
        public IEnumerable<Bid_Segment> Segments {get; set; }
        public DateTime DeterminationDate{ get; set; }
        public TimeSpan Duration { get; set; }
        public TimeSpan StepDuration { get; set; }
        public ControlReserveShortCode.ShortCode ControlReserveType { get; set; }

        public ProcessArgs(DateTime determinationDate, TimeSpan duration, TimeSpan stepDuration, IEnumerable<Bid_Segment> segments, ControlReserveShortCode.ShortCode controlReserveType)
        {
            DeterminationDate = determinationDate;
            Duration = duration;
            StepDuration = stepDuration;
            Segments = segments;
            ControlReserveType = controlReserveType;
        }
    }
}
