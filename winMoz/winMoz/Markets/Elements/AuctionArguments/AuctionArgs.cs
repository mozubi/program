﻿using System;

namespace winMoz.Markets.Elements
{
    public class AuctionArgs : EventArgs
    {
        public DateTime TimeStamp { get; set; }
        public TimeSpan Duration { get; set; }

        public TimeSpan Step { get; set; }

        public AuctionArgs((DateTime TimeStamp, TimeSpan Duration, TimeSpan Step) auctionTime)
        {
            this.TimeStamp = auctionTime.TimeStamp;
            this.Duration = auctionTime.Duration;
            this.Step = auctionTime.Step;
        }
    }

}

