﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace winMoz.Simulation.Logging
{
    public interface IMessage
    {
        int Id { get;}
        DateTime TimeStamp { get; }
        string Text { get;}
        string CallingClass { get; }
        string CallingMethod { get; } 
        int Type { get; }
    }
}
