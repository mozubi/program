﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation.Logging;

namespace winMoz.Simulation
{
    public class Log
    {
        static int msgCounter = 0;
        static LoggingContext LogContext = new LoggingContext();
        public static List<Message> Messages => LogContext.Messages.ToList();
        enum MessageType { Info, Warning, Error };
        public static VerbosityLevel Verbosity { get; set; } = VerbosityLevel.All;
        public enum VerbosityLevel { All, Info, Warning, Error, None }

        public Log()
        {
            AppDomain.CurrentDomain.ProcessExit += (s, e) => LogContext.SaveChanges();
        }

        public static void SetVerbosity(VerbosityLevel level) => Verbosity = level;
        public static void Info(string message) => CreateMessage(message, MessageType.Info);
        public static void Warning(string message) => CreateMessage(message, MessageType.Warning);
        public static void Error(string message) => CreateMessage(message, MessageType.Error);

        private static void CreateMessage(string message, MessageType msgType)
        {
            var calling = new StackTrace().GetFrames().Last().GetMethod();
            var msg = Message.Create(message, calling, (int)msgType);
            PrintByVerbosity(msg);
            LogContext.Add(msg);
            CheckCtx();
        }

        private static void CheckCtx()
        {
            msgCounter++;
            if (msgCounter > 100)
            {
                LogContext.SaveChanges();
                msgCounter = 0;
            }
        }

        private static void PrintByVerbosity(Message msg)
        {
            if (Verbosity == 0 || (int)Verbosity > msg.Type + 1)
                PrintMessage(msg);
        }

        private static void PrintMessage(Message msg)
        {
            SwitchOutputConsoleColor((MessageType)msg.Type);
            msg.WriteToConsole();
            SwitchOutputConsoleColor();
        }

        private static void SwitchOutputConsoleColor(MessageType msgType = default)
        {
            switch (msgType)
            {
                default:
                case MessageType.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MessageType.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case MessageType.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }
        }
    }
}
