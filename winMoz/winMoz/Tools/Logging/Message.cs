﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace winMoz.Simulation.Logging
{
    public class Message
    {
        public int Id { get; private set; }
        public DateTime TimeStamp { get; private set; }
        public string Text { get; private set; } = "";
        public string CallingClass { get; private set; } = "";
        public string CallingMethod { get; private set; } = "";
        public int Type { get; private set; } = 0;

        internal static Message Create(string message, MethodBase calling, int type)
        {
            ReadClassAndMethodName(calling, out string className, out string methodName);

            return new Message
            {
                TimeStamp = DateTime.Now,
                Type = type,
                Text = message,
                CallingClass = className,
                CallingMethod = methodName
            };
        }

        internal void WriteToConsole()
        {
            Console.WriteLine($"{TimeStamp.ToString("MM/dd/yyyy HH:mm")} - {Text} - {CallingClass} - {CallingMethod}");
        }

        private static void ReadClassAndMethodName(MethodBase calling, out string className, out string methodName)
        {
            className = calling.DeclaringType.FullName;

            int idx = className.IndexOf("+");

            if (idx > 0) className = className.Substring(0, idx);

            int idx2 = className.LastIndexOf('.');

            if (idx2 > 0) className = className.Substring(idx2 + 1);

            methodName = Regex.Match(calling.Name, @"\<([^><]*)\>").Groups[1].Value;

            if (methodName == "") methodName = calling.Name;
        }
    }
}
