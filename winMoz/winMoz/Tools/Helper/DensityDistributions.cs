﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace winMoz.Helper
{
    public class DensityDistributions
    {
        [Obsolete]
        /// <summary>
        /// Berechnet die Bandbreite für die Kerndichteschätzung mit Gaußkern
        /// nach der Faustformel (rule of thump) von Silverman
        /// </summary>
        /// <param name="standardDiviation">Standardabweichung</param>
        /// <param name="count">Anzahl</param>
        /// <returns>Bandbreite</returns>
        public static float CalculateBandwithWithSilvermansRuleOfThump(float standardDiviation, int count)
            => standardDiviation * 1.06F * (float)Math.Pow(count, -0.2F); //Silverman's rule of thump

        /// <summary>
        /// Schätzung der Warscheinlichkeitsverteilung der Werte über eine Kerndichtschätzung mit Gausskern
        /// </summary>
        /// <param name="values">gegebene Werte</param>
        ///<param name="border">symetrische Grenze des zu betrachtenden Wertebereichs</param>
        ///<param name="stepSize">Schrittweite der zu betrachtenden Werte</param>
        /// <returns>Wert, geschätzte Häufigkeit des Wertes</returns>
        public static Dictionary<int, float> PerformKernelDensityEstimationWithGaussianKernel(IEnumerable<float> values, int border, int stepSize)
        {
            int Count = values.Count();
            int bandwidth = 350;
            return PerformKernelDensityEstimationWithGaussianKernelElement(values, Count, border, stepSize, bandwidth: bandwidth).Select(item => (item.Key, item.Value / Count)).ToDictionary(item => item.Key, item => item.Item2);
        }


        public static IEnumerable<(int Key, float Value)> PerformKernelDensityEstimationWithGaussianKernelElement(IEnumerable<float> values, int Count, int border, int stepSize, float bandwidth)
        {
            List<double> valuesOrdered = values.OrderBy(item => item).ToList().ConvertAll(el => (double)el);
            var listRange = Enumerable.Range(border * -1 / stepSize, border * 2 / stepSize).ToList();
            var listResult = listRange.Select(item2 => (item2 * stepSize,
            Count != 0 ? (float)MathNet.Numerics.Statistics.KernelDensity.EstimateGaussian(item2 * stepSize, bandwidth, valuesOrdered) * Count : 0));
            return listResult;
        }

        /// <summary>
        /// Schätzung der Warscheinlichkeitsverteilung der Werte über eine Kerndichtschätzung mit Gausskern
        /// </summary>
        /// <param name="values">gegebene Werte</param>
        ///<param name="border">symetrische Grenze des zu betrachtenden Wertebereichs</param>
        ///<param name="stepSize">Schrittweite der zu betrachtenden Werte</param>
        /// <returns>Wert, geschätzte Häufigkeit des Wertes</returns>
        public static IEnumerable<(int Key, IEnumerable<(int Key, float Value)> Value)> PerformKernelDensityEstimationWithGaussianKernelPart(IEnumerable<(int, IEnumerable<float>)> values, int border, int stepSize, float bandwidth)
            => values.Select(item => (item.Item1, PerformKernelDensityEstimationWithGaussianKernelElement(item.Item2, item.Item2.Count(), border, stepSize, bandwidth)));

        /// <summary>
        /// Schätzung der Warscheinlichkeitsverteilung der Werte über eine Kerndichtschätzung mit Gausskern
        /// </summary>
        /// <param name="values">gegebene Werte</param>
        ///<param name="border">symetrische Grenze des zu betrachtenden Wertebereichs</param>
        ///<param name="stepSize">Schrittweite der zu betrachtenden Werte</param>
        /// <returns>Wert, geschätzte Häufigkeit des Wertes</returns>
        public static Dictionary<int, Dictionary<int, float>> PerformKernelDensityEstimationWithGaussianKernelAddSubtract(Dictionary<int, Dictionary<int, float>> valuesHistoric, IEnumerable<(int, IEnumerable<float>)> valuesAdd, IEnumerable<(int, IEnumerable<float>)> valuesSubtract, IEnumerable<int> CountHistOld, int border, int stepSize)
        {
            float bandwidth = 350;

            List<(int, IEnumerable<(int, float)>)> resultsAdd = PerformKernelDensityEstimationWithGaussianKernelPart(valuesAdd, border, stepSize, bandwidth).ToList();

            List<(int, IEnumerable<(int, float)>)> resultsSubtract = PerformKernelDensityEstimationWithGaussianKernelPart(valuesSubtract, border, stepSize, bandwidth).ToList();

            return valuesHistoric.ToList()
                .Select(item => (item.Key, SubtractDensityDistributions(
                    distribution1: AddDensityDistributions(distribution1: item.Value.Select(item2 => (item2.Key, item2.Value)), distribution2: resultsAdd[item.Key].Item2, count1: CountHistOld.ElementAt(item.Key), count2: valuesAdd.ElementAt(item.Key).Item2.Count()), 
                    distribution2: resultsSubtract[item.Key].Item2, 
                    count1: CountHistOld.ElementAt(item.Key) + valuesAdd.ElementAt(item.Key).Item2.Count(), 
                    count2: valuesAdd.ElementAt(item.Key).Item2.Count()).ToDictionary(item2 => item2.Item1, item2 => item2.Item2))).ToDictionary(item => item.Key, item => item.Item2);
        }

        public static IEnumerable<(int, float)> AddDensityDistributions(IEnumerable<(int, float)> distribution1, IEnumerable<(int, float)> distribution2, int count1, int count2)
            => distribution1.Zip(distribution2, (element1, element2) => (element1.Item1, CalculateWeightedAverageAddition(element1.Item2, element2.Item2, count1, count2)));

        public static IEnumerable<(int, float)> SubtractDensityDistributions(IEnumerable<(int, float)> distribution1, IEnumerable<(int, float)> distribution2, int count1, int count2)
            => distribution1.Zip(distribution2, (element1, element2) => (element1.Item1, CalculateWeightedAverageSubtraction(element1.Item2, element2.Item2, count1, count2)));

        /// <summary>
        /// value2 wird zu value1 gewichtet mit der Anzahl einer jeden Liste addiert
        /// </summary>
        /// <returns></returns>
        public static float CalculateWeightedAverageAddition(float value1, float value2, int count1, int count2)
            => (value1 * count1 + value2 * count2) / (count1 + count2);

        /// <summary>
        /// value2 wird von value1 gewichtet mit der Anzahl einer jeden Liste abgezogen
        /// </summary>
        /// <returns></returns>
        public static float CalculateWeightedAverageSubtraction(float value1, float value2, int count1, int count2)
            => (value1 * count1 - value2 * count2) / (count1 - count2);

        public static float Phi2(float x) => 0.5F * (float)(1 + MathNet.Numerics.SpecialFunctions.Erf(x * Math.Pow(2, -0.5)));

        
        public static float OwensT(float h, float a)
        {
            Func<double, double> integral = (x) => Math.Exp(-0.5 * Math.Pow(h, 2) * (1 + Math.Pow(x, 2))) * Math.Pow(1 + Math.Pow(x, 2), -1);
            return (float) (Math.Pow(2 * Math.PI, -1) * MathNet.Numerics.Integration.GaussLegendreRule.Integrate(integral, 0, a, 20));
        }

        /// <summary>
        /// Kummulierte Verteilung der schiefen Normalverteilung
        /// </summary>
        /// <param name="x"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static float DistributionFit(float x, float a, float b, float c)
            => Phi2((float)((x - a) * Math.Pow(b, -1)) - 2 * OwensT((x - a) * (float)Math.Pow(b, -1), c));
    }
}
