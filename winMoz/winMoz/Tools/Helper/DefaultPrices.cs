﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.Results;
using winMoz.Information.PFCs;
using winMoz.Helper;
using winMoz.Markets.Bids;

namespace winMoz.Helper
{
    public class DefaultPrices
    {
        Dictionary<DateTime, float> DayAheadPrices;

        Dictionary<DateTime, float> IntradayPrices;

        HPFCProvider DA_PricePredictor;
        QHPFCProvider ID_PricePredictor;
        public DefaultPrices()
        {
            DayAheadPrices = new Dictionary<DateTime, float>();
            IntradayPrices = new Dictionary<DateTime, float>();
            DA_PricePredictor = new HPFCProvider();
            ID_PricePredictor = new QHPFCProvider();
        }

        public void Predict_DA(DateTime timeStamp)
        {
            var DAPrices = DA_PricePredictor.Get(timeStamp: timeStamp, seed: default).ToList();
            foreach (var price in DAPrices) DayAheadPrices.Add(price.TimeStamp, price.PricePrediction);
        }

        public void Predict_ID(DateTime timeStamp)
        {
            var IDPrices = ID_PricePredictor.Get(timeStamp: timeStamp, seed: default).ToList();
            foreach (var price in IDPrices) IntradayPrices.Add(price.TimeStamp, price.PricePrediction);
        }

        public bool Contains_DA(DateTime timeStamp)
            => DayAheadPrices.ContainsKey(timeStamp);

        public bool Contains_ID(DateTime timeStamp)
            => IntradayPrices.ContainsKey(timeStamp);

        public float Get_DA_Price(DateTime timeStamp)
        {
            if (!Contains_DA(timeStamp))
                Predict_DA(timeStamp);
            return DayAheadPrices[timeStamp];
        }

        public float Get_ID_Price(DateTime timeStamp)
        {
            if (!Contains_ID(timeStamp))
                Predict_ID(timeStamp);
            return IntradayPrices[timeStamp];
        }

        public DayAheadResult GetDefaultDayAheadResult(DateTime timeStamp, TimeSpan duration)
        {
            var DAPrice = Get_DA_Price(timeStamp);
            return new DayAheadResult(timeStamp, ShortCode.DayAhead.Hour(timeStamp), DAPrice, 0);
        }

        public MarketResultUP GetDefaultDayAheadLocalMarketResult(DateTime timeStamp, TimeSpan duration)
        {
            var DAPrice = Get_DA_Price(timeStamp);
            return new MarketResultUP((decimal)DAPrice, 0, (decimal)DAPrice, 0, (decimal)DAPrice, 0, 0);
        }

        public IntraDayResult GetDefaultIntradayResult(DateTime timeStamp, TimeSpan duration)
        {
            var IDPrice = Get_ID_Price(timeStamp);
            return new IntraDayResult(timeStamp, ShortCode.IntraDay.QuarterHour(timeStamp), IDPrice, 0);
        }

        public MarketResultUP GetDefaultIntradayLocalMarketResult(DateTime timeStamp, TimeSpan duration)
        {
            var IDPrice = Get_ID_Price(timeStamp);
            return new MarketResultUP((decimal)IDPrice, 0, (decimal)IDPrice, 0, (decimal)IDPrice, 0, 0);
        }
    }
}
