﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Helper
{
    public static class Helper_Time_Translation
    {
        public static DateTime GetStartTimeFromQuarterHourOfTime(DateTime time)
        {
            int QuarterHour = Extensions.GetQuarterHourOfDay(time);
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddMinutes(QuarterHour * 15 - 15);
        }

        public static DateTime GetEndTimeFromQuarterHourOfTime(DateTime time)
        {
            int QuarterHour = Extensions.GetQuarterHourOfDay(time);
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddMinutes(QuarterHour * 15);
        }

        public static DateTime GetStartTimeFromQuarterHour(DateTime time, int quarterhour)
        {
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddMinutes(quarterhour * 15 - 15);
        }

        public static DateTime GetEndTimeFromQuarterHour(DateTime time, int quarterhour)
        {
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddMinutes(quarterhour * 15);
        }

        public static int GetSixthOfDay(this DateTime time)
        {
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return
                Convert.ToInt16(Math.Round(time.Subtract(baseTime).TotalHours / 4) + 1);
        }

        public static DateTime GetStartTimeFromSixthOfDayOfTime(DateTime time)
        {
            int Sixth = GetSixthOfDay(time);
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddHours(Sixth * 4 - 4);
        }

        public static DateTime GetEndTimeFromSixthOfDayOfTime(DateTime time)
        {
            int Sixth = GetSixthOfDay(time);
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddMinutes(Sixth * 4);
        }

        public static DateTime GetStartTimeFromSixthOfDay(DateTime time, int sixth)
        {
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddHours(sixth * 4 - 4);
        }

        public static DateTime GetEndTimeFromSixthOfDay(DateTime time, int sixth)
        {
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddHours(sixth * 4);
        }

        public static List<int> GetQuarterHoursOfSixthOfDay(DateTime time, int sixth)
        {
            List<int> quarters = new List<int>();
            DateTime starttime = GetStartTimeFromSixthOfDay(time, sixth);
            DateTime endtime = starttime.AddHours(4).AddMinutes(-15);
            return Enumerable.Range(starttime.Minute / 15, (int)((endtime - starttime).TotalMinutes / 15)).ToList();
        }

        public static List<int> GetYearsFromDuration(DateTime date1, DateTime date2)
            => Enumerable.Range(Math.Min(date1.Year, date2.Year), Math.Max(date1.Year, date2.Year) - Math.Min(date1.Year, date2.Year) + 1).ToList();

        public static DateTime GetStartTime(DateTime time, TimeSpan duration)
        {
            if (duration.TotalMinutes % TimeSpan.FromDays(1).TotalMinutes == 0) return new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            else if (duration.TotalMinutes % TimeSpan.FromHours(4).TotalMinutes == 0) return Extensions.GetSixthOfDayStartTime(time);
            else if (duration.TotalMinutes % TimeSpan.FromHours(1).TotalMinutes == 0) return new DateTime(time.Year, time.Month, time.Day, time.Hour, 0, 0);
            else if (duration.TotalMinutes % 15 == 0) return new DateTime(time.Year, time.Month, time.Day, time.Hour, (time.Minute / 15) * 15, 0);
            else return default;
        }

        public static DateTime GetStartTime(DateTime time, TimeSpan duration, TimeSpan stepDuration, int numberStep)
        {
            if (duration.TotalMinutes % TimeSpan.FromDays(1).TotalMinutes == 0) return new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            else if (duration.TotalMinutes % TimeSpan.FromHours(4).TotalMinutes == 0) return Extensions.GetSixthOfDayStartTime(time).AddMinutes(stepDuration.TotalMinutes * (numberStep - 1));
            else if (duration.TotalMinutes % TimeSpan.FromHours(1).TotalMinutes == 0) return new DateTime(time.Year, time.Month, time.Day, time.Hour, 0, 0).AddMinutes(stepDuration.TotalMinutes * (numberStep - 1));
            else if (duration.TotalMinutes % 15 == 0) return new DateTime(time.Year, time.Month, time.Day, time.Hour, (time.Minute / 15) * 15, 0).AddMinutes(stepDuration.TotalMinutes * (numberStep - 1));
            else return default;
        }

        public static DateTime GetStartTime(DateTime time, TimeSpan stepDuration, int numberStep)
            => time.AddMinutes(stepDuration.TotalMinutes * (numberStep - 1));

        public static int GetNumberOfTimeSpan(TimeSpan Duration, TimeSpan StepDuration)
            => (int)Math.Truncate(Math.Round(Duration.TotalMinutes, 0) / Math.Round(StepDuration.TotalMinutes, 0)) + 1;

        public static int GetNumberTimeSpanDay(DateTime time, TimeSpan duration)
        {
            TimeSpan DurationDay = time - new DateTime(time.Year, time.Month, time.Day);
            return GetNumberOfTimeSpan(DurationDay, duration);
        }

        public static int GetNumberOfTimeSpanDay(this DateTime time, TimeSpan duration)
        {
            TimeSpan DurationDay = time - new DateTime(time.Year, time.Month, time.Day);
            return GetNumberOfTimeSpan(DurationDay, duration);
        }

        public static DateTime GetStartTimeOfTimeSpanDay(this DateTime time, TimeSpan duration, int numberOfDuration)
        {
            DateTime timeStamp = new DateTime(time.Year, time.Month, time.Day);
            return timeStamp.AddMinutes(duration.TotalMinutes * (numberOfDuration - 1));
        }

        public static bool IsStartTimeOfDuration(this DateTime time, TimeSpan duration)
        {
            TimeSpan DurationDay = time - new DateTime(time.Year, time.Month, time.Day);
            return (DurationDay.TotalMinutes % duration.TotalMinutes) == 0;
        }

        public static int GetNumberTimeSpanTimeSpan(DateTime time, TimeSpan duration, TimeSpan stepDuration)
        {
            int StartTimePart = GetNumberTimeSpanDay(time, duration);
            TimeSpan DurationTimeSpan = time - new DateTime(time.Year, time.Month, time.Day).AddMinutes((StartTimePart - 1) * duration.TotalMinutes);
            return GetNumberOfTimeSpan(DurationTimeSpan, stepDuration);
        }
    }
}
