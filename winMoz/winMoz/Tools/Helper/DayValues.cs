﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Helper
{
    public class DayValues
    {
        /// <summary>
        /// Klasse zur Bestimmung der (Werk-)Tage für einzelne Länder unter Berücksichtigung der spezifischen Feiertage.
        /// 
        /// 
        /// </summary>
        private DateTime Karfreitag { get; set; }
        private DateTime Gruendonnerstag { get; set; }
        private DateTime Ostermontag { get; set; }
        private DateTime ChristiHimmelfahrt { get; set; }
        private DateTime Pfingstmontag { get; set; }
        private DateTime Fronleichnam { get; set; }
        private DateTime MariaHimmelfahrt { get; set; }
        private DateTime Neujahr { get; set; }
        private DateTime Allerheiligen { get; set; }
        private DateTime TagDerArbeit { get; set; }
        private DateTime ErsterWeichnachtstag { get; set; }
        private DateTime ZweiterWeihnachtstag { get; set; }
        private DateTime DreiKoenige { get; set; }

        private Dictionary<int, Dictionary<string, List<DateTime>>> Feiertage;
        public DayValues() => Feiertage = new Dictionary<int, Dictionary<string, List<DateTime>>>();

        public DayValues(int Year)
        {
            Feiertage = new Dictionary<int, Dictionary<string, List<DateTime>>>();
            Feiertage.Add(Year, new Dictionary<string, List<DateTime>>());
            InitializeDayValues(Year);
        }

        public DayValues(List<int> Years)
        {
            Feiertage = new Dictionary<int, Dictionary<string, List<DateTime>>>();

            Years.ForEach(Jahr =>
            {
                Feiertage.Add(Jahr, new Dictionary<string, List<DateTime>>());
                InitializeDayValues(Jahr);
            });
        }

        public DayValues(DateTime StartTag, DateTime EndTag)
        {
            Feiertage = new Dictionary<int, Dictionary<string, List<DateTime>>>();

            DateTime datTmp = StartTag;

            while (datTmp < EndTag.AddDays(355))
            {
                int Jahr = datTmp.Year;
                Feiertage.Add(Jahr, new Dictionary<string, List<DateTime>>());
                InitializeDayValues(Jahr);
                datTmp = datTmp.AddYears(1);
            }
        }

        public void Initialize(int Year)
        {
            if (Feiertage.Count() == 0 || !Feiertage.ContainsKey(Year))
            {
                Feiertage.Add(Year, new Dictionary<string, List<DateTime>>());
                InitializeDayValues(Year);
            }
        }

        public void InitializeDayValues(int Jahr)
        {
            SetGlobalHolidays(Jahr);

            SetAT(Jahr);
            SetBE(Jahr);
            SetCH(Jahr);
            SetCZ(Jahr);
            SetDE(Jahr);
            SetDK(Jahr);
            SetFR(Jahr);
            SetIT(Jahr);
            SetNL(Jahr);
            SetSE(Jahr);
            SetPL(Jahr);
        }

        /// <summary>
        /// Kernfunktion. Gibt 0 zurück, wenn Feiertag, ansonsten Tageswert 0 = Sonntag bis 6 = Samstag
        /// Falls keine Feiertage hinterlegt sind, werden diese ignoriert!
        /// </summary>
        public int getDay(string Shortcode, DateTime datum)
        {
            if (Feiertage.Count() == 0 || !Feiertage.ContainsKey(datum.Year))
                Initialize(datum.Year);
            return Feiertage[datum.Year][Shortcode].Contains(datum) ? 0 : (int)datum.DayOfWeek;
        }

        private void SetGlobalHolidays(int year)
        {
            DateTime Ostern;

            //http://stackoverflow.com/questions/2510383/how-can-i-calculate-what-date-good-friday-falls-on-given-a-year

            var g = year % 19;
            var c = year / 100;
            var h = (c - c / 4 - (8 * c + 13) / 25 + 19 * g + 15) % 30;
            var i = h - (h / 28) * (1 - (h / 28) * (29 / (h + 1)) * ((21 - g) / 11));

            var day = i - ((year + (int)(year / 4) + i + 2 - c + (int)(c / 4)) % 7) + 28;
            var month = 3;

            if (day > 31)
            {
                month++;
                day -= 31;
            }

            Ostern = new DateTime(year, month, day);

            Gruendonnerstag = Ostern.AddDays(-3);
            Karfreitag = Ostern.AddDays(-2);
            Ostermontag = Ostern.AddDays(1);
            ChristiHimmelfahrt = Ostern.AddDays(39);
            Pfingstmontag = Ostern.AddDays(50);
            Fronleichnam = Ostern.AddDays(60);

            Neujahr = new DateTime(year: year, month: 1, day: 1);
            TagDerArbeit = new DateTime(year: year, month: 5, day: 1);
            ErsterWeichnachtstag = new DateTime(year: year, month: 12, day: 25);
            ZweiterWeihnachtstag = new DateTime(year: year, month: 12, day: 26);
            MariaHimmelfahrt = new DateTime(year: year, month: 8, day: 15);
            Allerheiligen = new DateTime(year: year, month: 11, day: 1);
            DreiKoenige = new DateTime(year: year, month: 1, day: 6);

        }

        private void SetDE(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("DE", new List<DateTime>() {
                Neujahr,
                Karfreitag,
                Ostermontag,
                ChristiHimmelfahrt,
                Pfingstmontag,
                Fronleichnam,
                TagDerArbeit,
                ErsterWeichnachtstag,
                ZweiterWeihnachtstag,
                new DateTime(jahr, 10, 3)
            });
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetFR(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("FR", new List<DateTime>() {
             Neujahr,
            Ostermontag,
            ChristiHimmelfahrt,
            Pfingstmontag,
            Allerheiligen,
            TagDerArbeit,
            MariaHimmelfahrt,
            ErsterWeichnachtstag,
            ErsterWeichnachtstag,
            new DateTime(jahr, 5, 8), // Tag des Waffenstillstandes
            new DateTime(jahr, 7, 14) }); // Nationalfeiertag            
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetBE(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("BE", new List<DateTime>() {
            Neujahr,
            TagDerArbeit,
            MariaHimmelfahrt,
            Allerheiligen,
            ErsterWeichnachtstag,
            Ostermontag,
            ChristiHimmelfahrt,
            Pfingstmontag,
            new DateTime(jahr, 11, 11), // Tag des Waffenstillstandes
            new DateTime(jahr, 7, 21) }); // Nationalfeiertag                
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetDK(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("DK", new List<DateTime>() {
            Neujahr,
            Gruendonnerstag,
            Karfreitag,
            Ostermontag,
            ChristiHimmelfahrt,
            Pfingstmontag,
            ErsterWeichnachtstag,
            ZweiterWeihnachtstag,
            new DateTime(jahr, 5, 17) }); // Großer Bettag
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetNL(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("NL", new List<DateTime>() {
            Neujahr,
            Karfreitag,
            Ostermontag,
            ChristiHimmelfahrt,
            Pfingstmontag,
            ErsterWeichnachtstag,
            ZweiterWeihnachtstag,
            new DateTime(jahr, 4, 27), // Königstag
            new DateTime(jahr, 5, 5) }); // Befreiungstag
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetAT(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("AT", new List<DateTime>() {
            Neujahr,
            Ostermontag,
            ChristiHimmelfahrt,
            Pfingstmontag,
            Fronleichnam,
            MariaHimmelfahrt,
            Allerheiligen,
            ErsterWeichnachtstag,
            ZweiterWeihnachtstag,
            DreiKoenige,
            new DateTime(jahr, 5, 1), // Staatsfeiertag
            new DateTime(jahr, 10, 26), // Nationalfeiertag
            new DateTime(jahr, 12, 8) }); // Maria Empfängnis
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetCZ(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("CZ", new List<DateTime>() {
            Neujahr,
            Karfreitag,
            Ostermontag,
            TagDerArbeit,
            ErsterWeichnachtstag,
            ZweiterWeihnachtstag,

            new DateTime(jahr, 5, 1), // Befreiung
            new DateTime(jahr, 7, 5), // Apostel
            new DateTime(jahr, 7, 6), // Magister
            new DateTime(jahr, 9, 28), // Staatlichkeit
            new DateTime(jahr, 10, 28), // Staatsgründung
            new DateTime(jahr, 11, 17), // Studentenkampf
            new DateTime(jahr, 12, 24) }); // Heiligabend
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetCH(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("CH", new List<DateTime>() {
            Neujahr,
            ChristiHimmelfahrt,
            ErsterWeichnachtstag,
            Karfreitag,
            Ostermontag,
            Pfingstmontag,
            ZweiterWeihnachtstag,
            new DateTime(jahr, 8, 1) }); //Bundesfeier
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetPL(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("PL", new List<DateTime>() {
            Neujahr,
            DreiKoenige,
            Ostermontag,
            TagDerArbeit,
            Fronleichnam,
            MariaHimmelfahrt,
            Allerheiligen,
            ErsterWeichnachtstag,
            ZweiterWeihnachtstag,

            new DateTime(jahr, 5, 3), //Nationalfeiertag
            new DateTime(jahr, 11, 11) }); //UNabhängikeit
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetIT(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("IT", new List<DateTime>() {
            Neujahr,
            DreiKoenige,
            Ostermontag,
            TagDerArbeit,
            Pfingstmontag,
            MariaHimmelfahrt,
            Allerheiligen,
            ErsterWeichnachtstag,
            ZweiterWeihnachtstag,

            new DateTime(jahr, 4, 25), //Tag der Befreiung
            new DateTime(jahr, 6, 2), //Nationalfeiertag
            new DateTime(jahr, 12, 31) }); // Silvester
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }

        private void SetSE(int jahr)
        {
            var FeiertageJahr = new KeyValuePair<string, List<DateTime>>("SE", new List<DateTime>() {
            Neujahr,
            DreiKoenige,
            Karfreitag,
            Ostermontag,
            TagDerArbeit,
            ChristiHimmelfahrt,
            Allerheiligen,
            ErsterWeichnachtstag,
            ZweiterWeihnachtstag,
            new DateTime(jahr, 6, 6), //Nationalfeiertag
            new DateTime(jahr, 6, 24) }); //Midsommertag
            Feiertage[jahr].Add(FeiertageJahr.Key, FeiertageJahr.Value);
        }
    }
}
