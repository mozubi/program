﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace winMoz.Helper
{
    public static partial class XML
    {
        public static XDocument Load_Xml_File(string connectionString, Assembly assembly)
        {
            var stream = assembly.GetManifestResourceStream(connectionString);
            return XDocument.Load(stream);
        }

        #region XML - Laden spezifischer Werte
        public static List<float> GetGasSLPDailyFactors(XDocument XmlDoc, DateTime TimeStamp, float temperature)
        {
            List<float> factor = new List<float>();

            string day = TimeStamp.DayOfWeek.ToString();

            float weightComm = 0.75F;
            float weightHh = 0.25F;

            if (TimeStamp.IsWeekDay())
                day = "Working";

            for (int h = 1; h <= 24; h++)
            {
                var query = (from hourFactor in XmlDoc.Descendants("SLPHourFactor")
                             where hourFactor.Attribute("Day").Value.Equals(day.ToString())
                             where hourFactor.Attribute("Temperature").Value.Equals(temperature.ToString())
                             where hourFactor.Attribute("Hour").Value.Equals(h.ToString())
                             select new
                             {
                                 hour = hourFactor.Attribute("Hour").Value,
                                 FactorHH = Convert.ToSingle(hourFactor.Attribute("FactorHH").Value, CultureInfo.InvariantCulture),
                                 FactorComm = Convert.ToSingle(hourFactor.Attribute("FactorComm").Value, CultureInfo.InvariantCulture)
                             }).SingleOrDefault();

                factor.Add((query.FactorComm * weightComm + query.FactorHH * weightHh) / (weightHh + weightComm));
            }

            return factor;
        }

        public static float GetCommodityPrice(XDocument XmlDoc, DateTime TimeStamp, string commodityName, string type = "")
        {
            float price = 0;

            string date = TimeStamp.ToString().Remove(10, 9);

            var query = (from commodity in XmlDoc.Descendants("Commodity")
                         where commodity.Attribute("Name").Value.Equals("Coal")
                         where commodity.Attribute("Date").Value.Equals(date)
                         select commodity.Attribute("Price").Value).SingleOrDefault();

            price = Convert.ToSingle(query, CultureInfo.InvariantCulture);

            if (price == 0.0)
            {
                // Nimm ersten existenten Preis, wenn keine Preise vorhanden
                query = (from commodity in XmlDoc.Descendants("Commodity")
                         where commodity.Attribute("Name").Value.Equals("Coal")
                         select commodity.Attribute("Price").Value).First();

                price = Convert.ToSingle(query, CultureInfo.InvariantCulture);
            }


            return price;
        }

        #endregion
    }
}
