﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets.Bids.EoMMarkets;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.Elements;

namespace winMoz.Helper
{
    public static class EoM_Extensions
    {
        public static float CalculateTimeDifferenceFactor(int comparedYear, IEnumerable<DateTime> timeline)
        {
            var TimeLineOrdered = timeline.OrderBy(timeStamp => timeStamp);
            var StepDuration = TimeLineOrdered.ElementAt(1) - TimeLineOrdered.First();
            var Duration = TimeLineOrdered.Last() - TimeLineOrdered.First() + StepDuration;
            var ComparedDuration = new DateTime(comparedYear + 1, 1, 1, 0, 0, 0) - new DateTime(comparedYear, 1, 1, 0, 0, 0);
            return (float)(Duration.TotalMinutes / ComparedDuration.TotalMinutes);
        }
        /// <summary>
        /// Dubletten in MeritOrder zusammenfassen
        /// </summary>
        /// <param name="meritOrder"></param>
        /// <returns></returns>
        public static List<BidTupel> MeritOrder_Couple_Douplets(List<BidTupel> meritOrder)
        {
            if (meritOrder.Count == 0)
                return meritOrder;

            return meritOrder
                .GroupBy(bid => bid.Price, (price, bidList) => 
                new BidTupel(
                    price: price, 
                    volume: bidList.Select(bid2 => bid2.Volume).Sum(), 
                    isPurchase: bidList.Select(bid2 => bid2.IsPurchase).Any(bid2 => bid2),
                    volumePurchase: bidList.Select(bid2 => bid2.VolumePurchase).Sum(),
                    aggVol: bidList.Select(bid2 => bid2.AggVol).Max())
                    )
                .ToList();
        }

        /// <summary>
        /// Aggergiert die Volumina, der VORHER mit sortListPrice Sortierten Listen
        /// </summary>
        public static List<BidTupel> AggregateVolumesUP(List<BidTupel> meritOrder, decimal stepBidVolume = 0)
        {
            decimal aggVol = 0;

            foreach (BidTupel tmp in meritOrder)
            {
                aggVol += tmp.Volume;
                tmp.AggVol = aggVol;
            }
            return meritOrder;
        }

        /// <summary>
        /// Aggergiert die Volumina, der VORHER mit sortListPrice Sortierten Listen
        /// </summary>
        public static List<BidTupel.PayAsBid> AggregateVolumesPayAsBid(List<BidTupel.PayAsBid> meritOrder, decimal stepBidVolume = 0)
        {
            decimal aggVol = 0;
            decimal costRevenues = 0;

            foreach (BidTupel.PayAsBid bid in meritOrder)
            {
                aggVol += bid.Volume;
                costRevenues += bid.Price * bid.Volume;
                bid.AggVol =  aggVol;
                bid.CostRevenues = costRevenues;
            }
            return meritOrder;
        }

        /// <summary>
        /// Sortiert nach Preis aufsteigend
        /// </summary>
        public static List<BidTupel> OrderByListsPrice(List<BidTupel> meritOrder)
        {
            meritOrder.Sort((x, y) => x.Price.CompareTo(y.Price));
            return meritOrder;
        }

        /// <summary>
        /// Sortiert nach Preis absteigend
        /// </summary>
        public static List<BidTupel> OrderByDescendingListsPrice(List<BidTupel> meritOrder)
        {
            meritOrder.Sort((y, x) => x.Price.CompareTo(y.Price));
            return meritOrder;
        }

        public static MA_Auction ShiftMeritOrderFromStart(MA_Auction ma_Auction, decimal shift)
        {
            //Setze temporäre Menge m1 neu, ergänzt um Blockgebote (Verschiebung NF auf FlexMO)
            ma_Auction.V_Result = ma_Auction.V_Start + shift;
            ma_Auction.SetIncreaseReduceCapacity();

            //Berechne neuen Preis zur temporären Menge V_Result über MeritOrder.
            ma_Auction.CalcPriceResult();
            return ma_Auction;
        }

        /// <summary>
        /// Fügt Nullvolumengebot hinzu
        /// </summary>
        /// <param name="meritOrder"></param>
        /// <returns></returns>
        public static (List<BidTupel> meritOrder, int iStart) InsertNullBid(List<BidTupel> meritOrder)
        {
            // NUllVolumengebot
            BidTupel StepBid = new BidTupel(meritOrder[0].Price, 0, false, 0);
            int iStart = 0;
            //if (meritOrder[0].Price > 0)
            //{
            //    meritOrder.Insert(0, StepBid);
            //    iStart = 1;
            //}
            return (meritOrder, iStart);
        }

        //StepBids einfügen
        public static List<BidTupel> InsertReducedStepBidsInFrontOf((List<BidTupel> meritOrderOld, int iStart) meritOrderPlusStart, decimal stepBidVolume, decimal stepBidPriceDiff)
        {    
            int i;
            List<BidTupel> meritOrder = meritOrderPlusStart.meritOrderOld;
            for (i = meritOrderPlusStart.iStart; i < meritOrder.Count; i += 2)
            {
                BidTupel StepBid = new BidTupel(
                    meritOrder[i].Price,
                    stepBidVolume,
                    meritOrder[i].IsPurchase,
                    meritOrder[i].IsPurchase ? stepBidVolume : 0);
                meritOrder.Insert(i, StepBid);
                meritOrder[i].Price -= stepBidPriceDiff;
            }

            return meritOrder;
        }

        //StepBids einfügen
        public static List<BidTupel> InsertIncreasedStepBidsInFrontOf((List<BidTupel> meritOrderOld, int iStart) meritOrderPlusStart, decimal stepBidVolume, decimal stepBidPriceDiff)
        {
            int i;

            List<BidTupel> meritOrder = meritOrderPlusStart.meritOrderOld;
            for (i = meritOrderPlusStart.iStart; i <= meritOrder.Count - 1; i += 2)
            {
                BidTupel StepBid = new BidTupel(
                    meritOrder[i].Price,
                    stepBidVolume,
                    meritOrder[i].IsPurchase,
                    meritOrder[i].IsPurchase ? stepBidVolume : 0);
                meritOrder.Insert(i, StepBid);
                meritOrder[i].Price += stepBidPriceDiff;
            }

            return meritOrder;
        }


        /// <summary>
        /// Löschen der StepBids
        /// </summary>
        /// <param name="meritOrder"></param>
        /// <returns></returns>
        public static List<BidTupel> DeleteStepBids(List<BidTupel> meritOrder)
        {
            for(int i = meritOrder.Count() - 1; i >= 0; i--)
                if (meritOrder[i].Volume < (decimal)0.009) meritOrder.RemoveAt(i);     

            return meritOrder;
        }

        public static (decimal mg, decimal bg) GetLinearParameter(List<BidTupel> meritOrder, int tupel1, int tupel2)
        {
            decimal mG = ((meritOrder[tupel2].Price - meritOrder[tupel1].Price) / (meritOrder[tupel2].AggVol - meritOrder[tupel1].AggVol));
            decimal bG = (((meritOrder[tupel1].Price * meritOrder[tupel2].AggVol) - (meritOrder[tupel2].Price * meritOrder[tupel1].AggVol)) / (meritOrder[tupel2].AggVol - meritOrder[tupel1].AggVol));
            return (mG, bG);
        }

        public static (int Tupel1, int Tupel2, bool BolInFirstOrLastBidTupel) MeritOrder_FindBidTupels(List<decimal> meritOrder, decimal value)
        {
            int a, b, c;

            a = 0;
            c = meritOrder.Count - 1;

            if (meritOrder[a] > value) return (Tupel1: 0, Tupel2: 0, BolInFirstOrLastBidTupel: true);
            if (meritOrder[c] < value) return (Tupel1: meritOrder.Count() - 1, Tupel2: meritOrder.Count() - 1, BolInFirstOrLastBidTupel: true);

            //vorher i + 1 statt i
            for (int i = 0; 2 + (1 - Math.Pow(2, i)) / (1 - 2) <= meritOrder.Count(); i++) //Ausfallbedingung durch geometrische Summenformel.
            {
                b = (a + c) / 2;
                if (meritOrder[b] > value)
                {
                    if (meritOrder[b - 1] > value)
                        //Menge ist zu hoch, setzte c = b
                        c = b;
                    else
                        //Preis liegt zwischen b und b-1
                        return (Tupel1: b - 1, Tupel2: b, BolInFirstOrLastBidTupel: false);
                }
                else
                {
                    if (meritOrder[b + 1] < value)
                        //Menge ist zu niedrig, setzte a = b
                        a = b;
                    else
                        //Preis liegt zwischen b und b +1
                        return (Tupel1: b, Tupel2: b + 1, BolInFirstOrLastBidTupel: false);
                }
            }
            return (Tupel1: a, Tupel2: c, BolInFirstOrLastBidTupel: false);
        }

        public static (int iS, int iP, DayAheadMarket.IrrationalType iIrrational, bool lastSell) MeritOrder_FindCrossingPoint(List<BidTupel> meritOrderPurchase, List<BidTupel> meritOrderSell)
        {
            int iP, iS;

            iP = 0;
            iS = 0;

            int MaxSellIndex = meritOrderSell.Count() - 1;
            int MaxPurchaseIndex = meritOrderPurchase.Count() - 1;
            bool lastJumpInSell = true; //Variable beantwortet Frage, ob der Index zuletzt in der Angebotsmeritorder erhöht wurde?

            while (iS <= MaxSellIndex && iP <= MaxPurchaseIndex)
            {
                if (meritOrderSell[iS].Price > meritOrderPurchase[iP].Price)
                    return (iS, iP, iIrrational: iS == meritOrderSell.Count() - 1 ? DayAheadMarket.IrrationalType.EXCESS_DEMAND : DayAheadMarket.IrrationalType.IN_RANGE, lastJumpInSell);
                else if (meritOrderPurchase[iP].AggVol < meritOrderSell[iS].AggVol)
                {
                    ++iP;
                    lastJumpInSell = false;
                    if (iP > meritOrderPurchase.Count - 1)
                        return (iS, iP, iIrrational: DayAheadMarket.IrrationalType.EXCESS_SUPPLY, lastJumpInSell); //Angebotsüberschuss
                }
                else
                {
                    ++iS;
                    lastJumpInSell = true;
                    if (iS > meritOrderSell.Count - 1)
                        return (iS, iP, iIrrational: DayAheadMarket.IrrationalType.EXCESS_DEMAND, lastJumpInSell); //Nachfrageüberschuss
                }
            }

            return (iS, iP, iIrrational: lastJumpInSell ? DayAheadMarket.IrrationalType.EXCESS_SUPPLY : DayAheadMarket.IrrationalType.EXCESS_DEMAND, lastJumpInSell);
        }

        public static (MA_Auction coupledMeritOrder, decimal MOExtension) AddBidsFromMeritOrderToCoupledMeritOrder(MA_Auction coupledMeritOrder, List<BidTupel> MeritOrder, decimal moExtension, decimal ub_Price, decimal lb_Price)
        {
            MeritOrder.ForEach(tempBid =>
            {
                if (tempBid.Price < ub_Price && tempBid.Price > lb_Price)
                    coupledMeritOrder.AddBidTupel(tempBid.Price, tempBid.Volume, tempBid.VolumePurchase > 0 ? true : false, tempBid.VolumePurchase);
                else if (tempBid.Price == ub_Price)
                    moExtension += tempBid.Volume;
            });
            return (coupledMeritOrder, moExtension);
        }

        //TODO: Überprüfen, ob noch gebraucht
        public static (decimal Price, decimal Volume, int iS, int iP) TestRunOver(decimal price, decimal volume, List<BidTupel> meritOrderPurchase, List<BidTupel> meritOrderSell, int iS, int iP)
        {
            decimal mA, bA, mB, bB;
            decimal Price = price;
            decimal Volume = volume;
            while (price > meritOrderPurchase[iP - 1].Price)
            {
                //Rückläufer, falls Ergebnis "überlaufen".
                iP = iP - 1;
                (mA, bA) = GetLinearParameter(meritOrderPurchase, iP - 1, iP);
                (mB, bB) = GetLinearParameter(meritOrderSell, iS - 1, iS);

                Volume = (bB - bA) / (mA - mB);
                Price = mA * Volume + bA;
            }

            while (price > meritOrderSell[iS].Price)
            {
                //Rückläufer, falls Ergebnis "überlaufen".
                iS = iS + 1;
                (mA, bA) = GetLinearParameter(meritOrderPurchase, iP - 1, iP);
                (mB, bB) = GetLinearParameter(meritOrderSell, iS - 1, iS);

                Volume = (bB - bA) / (mA - mB);
                Price = mA * Volume + bA;

            }
            return (Price, Volume, iS, iP);
        }
    }
}
