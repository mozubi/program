﻿using MathNet.Numerics.Interpolation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace winMoz.Helper
{
    public static class Resampling
    {
        public enum Method { Average, Sum, Max, Min, Linear, Akima, FFill }
        public enum TimeStep { QuarterHour, Hour, Sixth, Day, Week, Month }

        #region Resampling Methods (public)
        public static List<(DateTime TimeStamp, float)> Resample(this List<(DateTime TimeStamp, float)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series.Cast<object>().ToList(), step, method);
            return Enumerable.Range(0, result.Count).Select(el => Tuple.Create(result[el].Item1, result[el].Item2[0]).ToValueTuple()).ToList();
        }
        public static List<(DateTime TimeStamp, float, float)> Resample(this List<(DateTime TimeStamp, float, float)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series.Cast<object>().ToList(), step, method);
            return Enumerable.Range(0, result.Count).Select(el => Tuple.Create(result[el].Item1, result[el].Item2[0], result[el].Item2[1]).ToValueTuple()).ToList();
        }
        public static List<(DateTime TimeStamp, float, float, float)> Resample(this List<(DateTime TimeStamp, float, float, float)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series.Cast<object>().ToList(), step, method);
            return Enumerable.Range(0, result.Count).Select(el => Tuple.Create(result[el].Item1, result[el].Item2[0], result[el].Item2[1], result[el].Item2[2]).ToValueTuple()).ToList();
        }
        public static List<(DateTime TimeStamp, float, float, float, float)> Resample(this List<(DateTime TimeStamp, float, float, float, float)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series.Cast<object>().ToList(), step, method);
            return Enumerable.Range(0, result.Count).Select(el => Tuple.Create(result[el].Item1, result[el].Item2[0], result[el].Item2[1], result[el].Item2[2], result[el].Item2[3]).ToValueTuple()).ToList();
        }
        public static List<(DateTime TimeStamp, float, float, float, float, float)> Resample(this List<(DateTime TimeStamp, float, float, float, float, float)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series.Cast<object>().ToList(), step, method);
            return Enumerable.Range(0, result.Count).Select(el => Tuple.Create(result[el].Item1, result[el].Item2[0], result[el].Item2[1], result[el].Item2[2], result[el].Item2[3], result[el].Item2[4]).ToValueTuple()).ToList();
        }
        public static List<(DateTime TimeStamp, float, float, float, float, float, float)> Resample(this List<(DateTime TimeStamp, float, float, float, float, float, float)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series.Cast<object>().ToList(), step, method);
            return Enumerable.Range(0, result.Count).Select(el => Tuple.Create(result[el].Item1, result[el].Item2[0], result[el].Item2[1], result[el].Item2[2], result[el].Item2[3], result[el].Item2[4], result[el].Item2[5]).ToValueTuple()).ToList();
        }
        public static List<(DateTime TimeStamp, float, float, float, float, float, float, float)> Resample(this List<(DateTime TimeStamp, float, float, float, float, float, float, float)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series.Cast<object>().ToList(), step, method);
            return Enumerable.Range(0, result.Count).Select(el => Tuple.Create(result[el].Item1, result[el].Item2[0], result[el].Item2[1], result[el].Item2[2], result[el].Item2[3], result[el].Item2[4], result[el].Item2[5], result[el].Item2[6]).ToValueTuple()).ToList();
        }
        public static List<(DateTime TimeStamp, List<float>)> Resample(this List<(DateTime TimeStamp, List<float>)> series, TimeStep step, Method method = Method.Average)
        {
            var result = _resample(series, step, method);
            return result.Select(item => (item.TimeStamp, item.Item2.ToList())).ToList();
        }
        #endregion

        #region Resampling-Methoden (private)
        private static Func<IEnumerable<float>, float> GetMethod(Method method)
        {
            switch (method)
            {
                case Method.Sum: return Enumerable.Sum;
                case Method.Average: return Enumerable.Average;
                case Method.Max: return Enumerable.Max;
                case Method.Min: return Enumerable.Min;
                default: return Enumerable.Average;
            };
        }

        private static TimeSpan GetStepDuration(TimeStep step)
        {

            switch (step)
            {
                case TimeStep.QuarterHour: return TimeSpan.FromHours(0.25);
                case TimeStep.Hour: return TimeSpan.FromHours(1);
                case TimeStep.Sixth: return TimeSpan.FromHours(4);
                case TimeStep.Day: return TimeSpan.FromHours(24);
                case TimeStep.Week: return TimeSpan.FromHours(0.25);
                default:
                case TimeStep.Month: throw new NotImplementedException();
            }
        }

        public static TimeStep GetTimeStep(TimeSpan span)
        {
            if (TimeSpan.FromHours(0.25).TotalMinutes == span.TotalMinutes)
                return TimeStep.QuarterHour;
            else if (TimeSpan.FromHours(1).TotalMinutes == span.TotalMinutes)
                return TimeStep.Hour;
            else if (TimeSpan.FromHours(4).TotalMinutes == span.TotalMinutes)
                return TimeStep.Sixth;
            else if (TimeSpan.FromDays(1).TotalMinutes == span.TotalMinutes)
                return TimeStep.Day;
            else
                throw new NotImplementedException();
        }


        private static List<(DateTime TimeStamp, float[])> Duplicate(List<(DateTime TimeStamp, float[])> listIn, TimeSpan duration)
        {
            var n_dupl = (int)((listIn[1].TimeStamp - listIn[0].TimeStamp).Ticks / duration.Ticks);
            var n_total = listIn.Count;
            for (int i = 0; i < n_total; i++)
                listIn.AddRange(Enumerable.Range(1, n_dupl - 1).Select(t => (listIn[i].TimeStamp.AddMinutes(duration.TotalMinutes * t), listIn[i].Item2)));
            var test = listIn.OrderBy(el => el.TimeStamp).ToList();
            return listIn.OrderBy(el => el.TimeStamp).ToList();
        }
        private static List<(DateTime TimeStamp, float[])> QuarterHourly(List<(DateTime TimeStamp, float[])> listIn, Func<IEnumerable<float>, float> method)
            => listIn.Select(el => (el.TimeStamp, el.Item2, el.TimeStamp.GetQuarterHourOfYear())).GroupBy(el => new { el.TimeStamp.Year, el.Item3 })
                .Select(el => (new DateTime(el.Key.Year, 1, 1, 0, 0, 0).AddMinutes(el.Key.Item3 * 15), el.Select(it => it.Item2.ToList()).ToList().Transpose().Select(it => method(it)).ToArray())).ToList();
        private static List<(DateTime TimeStamp, float[])> Hourly(List<(DateTime TimeStamp, float[])> listIn, Func<IEnumerable<float>, float> method)
            => listIn.GroupBy(el => new { el.TimeStamp.Year, el.TimeStamp.DayOfYear, el.TimeStamp.Hour })
            .Select(el => (new DateTime(el.Key.Year, 1, 1, el.Key.Hour, 0, 0).AddDays(el.Key.DayOfYear - 1), el.Select(it => it.Item2.ToList()).ToList().Transpose().Select(it => method(it)).ToArray())).ToList();
        private static List<(DateTime TimeStamp, float[])> Sixthly(List<(DateTime TimeStamp, float[])> listIn, Func<IEnumerable<float>, float> method)
            => listIn.GroupBy(el => new { el.TimeStamp.Year, el.TimeStamp.DayOfYear, Sixth = Helper_Time_Translation.GetNumberTimeSpanDay(el.TimeStamp, TimeSpan.FromHours(4))})
            .Select(el => (new DateTime(el.Key.Year, 1, 1, el.Key.Sixth * 4 - 4, 0, 0).AddDays(el.Key.DayOfYear - 1), el.Select(it => it.Item2.ToList()).ToList().Transpose().Select(it => method(it)).ToArray())).ToList();
        private static List<(DateTime TimeStamp, float[])> Daily(List<(DateTime TimeStamp, float[])> listIn, Func<IEnumerable<float>, float> method)
            => listIn.GroupBy(el => new { el.TimeStamp.Year, el.TimeStamp.DayOfYear }).Select(el => (new DateTime(el.Key.Year, 1, 1).AddDays(el.Key.DayOfYear - 1), el.Select(it => it.Item2.ToList()).ToList().Transpose().Select(it => method(it)).ToArray())).ToList();
        private static List<(DateTime TimeStamp, float[])> Weekly(List<(DateTime TimeStamp, float[])> listIn, Func<IEnumerable<float>, float> method)
            => listIn.GroupBy(el => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(el.TimeStamp, CalendarWeekRule.FirstDay, DayOfWeek.Monday)).Select(el => (listIn.First().TimeStamp.AddDays(7 * (el.Key - 1)), el.Select(it => it.Item2.ToList()).ToList().Transpose().Select(it => method(it)).ToArray())).ToList();
        private static List<(DateTime TimeStamp, float[])> Monthly(List<(DateTime TimeStamp, float[])> listIn, Func<IEnumerable<float>, float> method)
            => listIn.GroupBy(el => new { el.TimeStamp.Year, el.TimeStamp.Month }).Select(el => (new DateTime(el.Key.Year, el.Key.Month, 1), el.Select(it => it.Item2.ToList()).ToList().Transpose().Select(it => method(it)).ToArray())).ToList();
        private static List<(DateTime TimeStamp, float[])> _resample(this List<object> series, TimeStep step, Method method = Method.Average)
        {
            if (series.Count() == 0) return new List<(DateTime TimeStamp, float[])>();
            var times = series.Select(row => row.GetType().GetFields()[0].GetValue(row)).Cast<DateTime>();
            var fields = series.Select(row => row.GetType().GetFields().Skip(1).Select(el => el.GetValue(row)).ToList().ConvertAll(el => (float)el).ToArray());
            if (series.Count() == 1) return new List<(DateTime TimeStamp, float[])>() { (times.First(), fields.First()) };
            var ts = fields.Zip(times, (f, t) => (Time: t, Values: f)).ToList();

            var function = GetMethod(method);
            var timeStep = times.ElementAt(1) - times.ElementAt(0);
            var stepDuration = GetStepDuration(step);

            // TODO: implement fill missing
            if (ts.IsRegular() && timeStep == stepDuration) return ts;
            if (timeStep < stepDuration)
                switch (step)
                {
                    case TimeStep.QuarterHour: return QuarterHourly(ts, function);
                    case TimeStep.Sixth: return Sixthly(ts, function);
                    case TimeStep.Hour: return Hourly(ts, function);
                    case TimeStep.Day: return Daily(ts, function);
                    case TimeStep.Week: return Weekly(ts, function);
                    case TimeStep.Month: return Monthly(ts, function);
                    default: throw new NotImplementedException();
                }
            else
                switch (method)
                {
                    default:
                    case Method.FFill: return Duplicate(ts, GetStepDuration(step));
                    case Method.Linear: return ts.Interpolate(0, GetStepDuration(step));
                    case Method.Akima: return ts.Interpolate(1, GetStepDuration(step));
                }

            throw new NotImplementedException();
        }

        private static List<(DateTime TimeStamp, float[])> _resample(this List<(DateTime TimeStamp, List<float> Values)> series, TimeStep step, Method method = Method.Average)
        {
            var times = series.Select(row => row.TimeStamp).ToList();
            var fields = series.Select(row => row.Values.ToList()).ToList();
            var ts = fields.Zip(times, (f, t) => (Time: t, Values: f.ToArray())).ToList();

            var function = GetMethod(method);
            var timeStep = times.ElementAt(1) - times.ElementAt(0);
            var stepDuration = GetStepDuration(step);

            // TODO: implement fill missing
            if (ts.IsRegular() && timeStep == stepDuration) return ts;
            if (timeStep < stepDuration)
                switch (step)
                {
                    case TimeStep.QuarterHour: return QuarterHourly(ts, function);
                    case TimeStep.Sixth: return Sixthly(ts, function);
                    case TimeStep.Hour: return Hourly(ts, function);
                    case TimeStep.Day: return Daily(ts, function);
                    case TimeStep.Week: return Weekly(ts, function);
                    case TimeStep.Month: return Monthly(ts, function);
                    default: throw new NotImplementedException();
                }
            else
                switch (method)
                {
                    default:
                    case Method.FFill: return Duplicate(ts, GetStepDuration(step));
                    case Method.Linear: return ts.Interpolate(0, GetStepDuration(step));
                    case Method.Akima: return ts.Interpolate(1, GetStepDuration(step));
                }

            throw new NotImplementedException();
        }

        private static List<(DateTime TimeStamp, float[])> Interpolate(this List<(DateTime TimeStamp, float[])> tsIn, int type, TimeSpan timeStep = default)
        {
            var tsTimeStep = tsIn[1].TimeStamp - tsIn[0].TimeStamp;
            if (timeStep == default) timeStep = TimeSpan.FromMinutes(15);

            tsIn = Duplicate(tsIn, timeStep);

            int stepSize = (int)(tsTimeStep.Ticks / timeStep.Ticks);
            var tsSteps = tsIn.Where((v, id) => id % stepSize == 0);

            while (tsSteps.Count() < 6)
            {
                tsSteps = tsIn.Where((v, id) => id % stepSize == 0);
                if (stepSize == 0) return tsIn;
                stepSize--;
            }

            int n_items = tsIn[0].Item2.Count();
            var interp = new List<List<float>>();
            for (int i = 0; i < n_items; i++)
            {
                IInterpolation spline = GetInterpolation(type, tsSteps, i);

                var interpList = new List<float>();
                for (int j = 0; j < tsIn.Count; j++)
                    interpList.Add((float)spline.Interpolate(tsIn[j].TimeStamp.Ticks));
                interp.Add(interpList);
            }
            interp = interp.Transpose();
            return interp.Zip(tsIn, (Interp, ts) => (ts.TimeStamp, Interp.ToArray())).ToList();
        }

        static IInterpolation GetInterpolation(int type, IEnumerable<(DateTime TimeStamp, float[])> tsSteps, int i)
        {
            if (type == 0) return LinearSpline.Interpolate(tsSteps.Select(el => Convert.ToDouble(el.TimeStamp.Ticks)).ToArray(), tsSteps.Select(el => (double)el.Item2[i]).ToArray());
            else return CubicSpline.InterpolateAkimaSorted(tsSteps.Select(el => Convert.ToDouble(el.TimeStamp.Ticks)).ToArray(), tsSteps.Select(el => (double)el.Item2[i]).ToArray());
        }

        #endregion

        #region Interpolation Methods
        public static List<(DateTime TimeStamp, float Value)> MovingAverage(this List<(DateTime TimeStamp, float Value)> tsIn, int windowSize)
        {

            var avgValues = tsIn.Select(el => el.Value).ToList().MovAverage(windowSize);
            var times = tsIn.Select(el => el.TimeStamp).ToList();

            return times.Zip(avgValues, (time, avg) => (time, avg)).ToList();
        }


        public static List<(DateTime TimeStamp, float Value)> SmoothLinear(this List<(DateTime TimeStamp, float Value)> tsIn)
        {
            LinearSpline linSpline = LinearSpline.Interpolate(tsIn.Where(el => el.TimeStamp.Minute == 0).Select(el => Convert.ToDouble(el.TimeStamp.Ticks)).ToArray(), tsIn.Where(el => el.TimeStamp.Minute == 0).Select(el => (double)el.Value).ToArray());

            var interpList = new List<float>();
            for (int i = 0; i < tsIn.Count; i++)
                interpList.Add((float)linSpline.Interpolate(tsIn[i].TimeStamp.Ticks));

            return interpList.Zip(tsIn, (interp, ts) => (ts.TimeStamp, interp)).ToList();
        }
        #endregion
    }
}