﻿using MathNet.Numerics.Interpolation;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Markets;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.Elements;
using System.Threading.Tasks;
using winMoz.Markets.EoMMarkets;
using winMoz.Simulation;

namespace winMoz.Helper
{
    public static class Extensions
    {
        #region DateTime-Extensionmethods


        /// <summary>
        /// Erweiterungs-Methode berechnet die 1-basierte Zahl der Viertelstunde eines gegebenen Datums im Verhältnis zum Studenanfang
        /// </summary>
        /// <param name="Time">DateTime-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static int GetQuarterHourOfHour(this DateTime time) => 1 + (time.Minute / 15);

        /// <summary>
        /// Erweiterungs-Methode berechnet die Zahl der Viertelstunde eines gegebenen Datums im Verhältnis zum Jahresanfang
        /// </summary>
        /// <param name="time">DateTime-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static int GetQuarterHourOfYear(this DateTime time)
        {
            DateTime baseTime = new DateTime(time.Year, 1, 1, 0, 0, 0);
            return Convert.ToInt32(Math.Round(time.Subtract(baseTime).TotalMinutes / 15));
        }

        public static int GetDaysOfYear(this DateTime time) => (time.AddYears(1) - time).Days;

        /// <summary>
        /// Erweiterungs-Methode berechnet die 1-basierte Zahl der Viertelstunde eines gegebenen Datums im Verhältnis zum Tagesanfang
        /// </summary>
        /// <param name="time">DateTime-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static int GetQuarterHourOfDay(this DateTime time) => time.GetQuarterHourOfHour() + (time.Hour * 4);

        /// <summary>
        /// Erweiterungs-Methode berechnet die Zahl der Viertelstunde eines gegebenen Datums im Verhältnis zu einem Referenzdatum
        /// </summary>
        /// <param name="time"><seealso cref="DateTime"/>-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static int GetQuarterHourSinceDate(this DateTime time, DateTime refDate) => Convert.ToInt32(Math.Truncate(time.Subtract(refDate).TotalMinutes / 15) + 1);

        public static int GetHoursSinceDate(this DateTime time, DateTime refDate) => Convert.ToInt32(Math.Truncate(time.Subtract(refDate).TotalHours) + 1);

        /// <summary>
        /// Erweiterungs-Methode berechnet Dauer des zugrundeliegenden Datums und gibt diese als <seealso cref="TimeSpan"/> zurück        
        /// </summary>
        /// <param name="time">Datum dessen Monatsdauer bestimmt werden soll</param>
        /// <returns>Monatsdauer als <seealso cref="TimeSpan"/></returns>
        public static TimeSpan GetDurationOfMonth(this DateTime time) => new TimeSpan(DateTime.DaysInMonth(time.Year, time.Month), 0, 0, 0);

        /// <summary>
        /// Erweiterungs-Methode berechnet die 1-basierte Zahl des Vier-Stunden-Zeitraums eines gegebenen Datums im Verhältnis zum Anfang des Tages
        /// </summary>
        /// <param name="Time">DateTime-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static int GetSixthOfDayNumber(this DateTime time)
        {
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return
                Convert.ToInt16(Math.Truncate(time.Subtract(baseTime).TotalHours)) / 4 + 1;
        }

        /// <summary>
        /// Erweiterungs-Methode berechnet die 1-basierte Zahl der Viertelstunde eines gegebenen Datums im Verhältnis zum Vierstunden-Zeitraum des Tages
        /// </summary>
        /// <param name="Time">DateTime-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static int GetQuarterOfSixthOfDay(this DateTime time, int SixthOfDay)
        {
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 4 * (SixthOfDay - 1), 0, 0);
            return
                Convert.ToInt16(Math.Truncate(time.Subtract(baseTime).TotalMinutes)) / 15 + 1;
        }

        /// <summary>
        /// Erweiterungs-Methode berechnet den Startzeitpunkt des Vier-Stunden-Zeitraums und die 
        /// 1-basierte Zahl der Viertelstunde eines gegebenen Datums im Verhältnis zum Vierstunden-Zeitraum des Tages
        /// </summary>
        /// <param name="Time">DateTime-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static (DateTime timeStamp, int QuarterOfSixthOfDay) GetSixthOfDayDuration(this DateTime time)
        {
            int Sixth = GetSixthOfDayNumber(time);
            int QuarterOfSixthOfDay = GetQuarterOfSixthOfDay(time, Sixth);
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return (baseTime.AddHours(Sixth * 4 - 4), QuarterOfSixthOfDay);
        }

        /// <summary>
        /// Erweiterungs-Methode berechnet den Startzeitpunkt des Vier-Stunden-Zeitraums und die 
        /// 1-basierte Zahl der Viertelstunde eines gegebenen Datums im Verhältnis zum Vierstunden-Zeitraum des Tages
        /// </summary>
        /// <param name="Time">DateTime-Wert dessen Viertelstundenzahl berechnet werden soll</param>
        /// <returns>Zahl der Viertelstunde als Integer</returns>
        public static DateTime GetSixthOfDayStartTime(this DateTime time)
        {
            int Sixth = GetSixthOfDayNumber(time);
            DateTime baseTime = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            return baseTime.AddHours(Sixth * 4 - 4);
        }

        /// <summary>
        /// Methode berechnet Gesamt-Dauer einer Liste von Zeitstempeln zwischen min. und max. Zeitstempel
        /// </summary>
        /// <param name="times">Liste mit DateTime-Werten</param>
        /// <returns>TimeSpan mit Dauer</returns>
        public static TimeSpan GetDuration(this IEnumerable<DateTime> times) => times.LastOrDefault() - times.FirstOrDefault();

        /// <summary>
        /// Methode gibt Liste mit DateTime-Objekten mit Zeiten für eine gewählte Dauer ab dem eigenen Zeitpunkt zurück
        /// </summary>
        /// <param name="timeStamp">Zeitpunkt, ab dem die TimeSeries generiert werden soll</param>
        /// <param name="duration">Dauer, für die die TimeSeries erzeugt wird</param>
        /// <param name="step">Schrittweite als TimeSpan (Default = Hour)</param>
        /// <returns>Liste mit DateTime-Objekten</returns>
        public static List<DateTime> GetTimeVector(this DateTime timeStamp, TimeSpan duration, Expression<Func<DateTime, bool>> predicate = default, TimeSpan? step = null)
        {
            if (!step.HasValue) step = TimeSpan.FromMinutes(15);
            if (predicate == default) predicate = e => true;
            return Enumerable.Range(0, (int)Math.Round(duration.TotalMinutes / step.Value.TotalMinutes, 0)).Select(i => timeStamp.AddMinutes(step.Value.TotalMinutes * i)).AsQueryable().Where(predicate).ToList();
        }

        /// <summary>
        /// Methode gibt Liste mit DateTime-Objekten mit Zeiten für eine gewählte Dauer ab dem eigenen Zeitpunkt zurück
        /// </summary>
        /// <param name="timeStamp">Zeitpunkt, ab dem die TimeSeries generiert werden soll</param>
        /// <param name="duration">Dauer, für die die TimeSeries erzeugt wird</param>
        /// <param name="step">Schrittweite als TimeSpan (Default = Hour)</param>
        /// <returns>Liste mit DateTime-Objekten</returns>
        public static List<DateTime> GetTimeVectorWithoutPredicate(this DateTime timeStamp, TimeSpan duration, TimeSpan? step = null)
        {
            if (!step.HasValue) step = TimeSpan.FromMinutes(15);
            return Enumerable.Range(0, (int)Math.Round(duration.TotalMinutes / step.Value.TotalMinutes, 0)).Select(i => timeStamp.AddMinutes(step.Value.TotalMinutes * i)).ToList();
        }

        public static List<DateTime> GetTimeVectorWithoutPredicate(this TimeSpan duration, DateTime timeStamp, TimeSpan? step = null) => GetTimeVectorWithoutPredicate(timeStamp, duration, step);

        public static List<DateTime> GetTimeVector(this TimeSpan duration, DateTime timeStamp, Expression<Func<DateTime, bool>> predicate = default, TimeSpan? step = null) => GetTimeVector(timeStamp, duration, predicate, step);

        /// <summary>
        /// Erweiterungs-Methode konvertiert Datum in Julianisches Datum
        /// </summary>
        /// <param name="date">DateTime-Datum, das konvertiert werden soll</param>
        /// <returns></returns>
        public static float ToJulianDate(this DateTime time) => (float)time.ToOADate() + 2415018.5F;

        public static DateTime ToFullHour(this DateTime time) => time.AddMinutes(-time.Minute);

        public static DateTime ToStartOfMonth(this DateTime time) => new DateTime(time.Year, time.Month, 1, 0, 0, 0);

        public static DateTime ToStartOfDay(this DateTime time) => new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);

        /// <summary>
        /// Erweiterungs-Methode bestimmt, ob die volle Stunde eines DateTime-Objekts der übergebenen vollen Stunde entspricht und gibt Bool zurück
        /// </summary>
        /// <param name="time">DateTime, dessen Stundenwert überprüft wird</param>
        /// <param name="hour">(Integer)Gesuchter Stundenwert</param>
        /// <returns>True, wenn die Stunde gleich ist; sonst false</returns>
        public static bool IsFullHour(this DateTime time, int hour) => time.Hour == hour && time.Minute == 0;

        public static bool IsFullHour(this DateTime time) => time.Minute == 0;

        public static bool IsQuarterHour(this DateTime date) => date.Minute % 15 == 0;
        public static bool IsWeekDay(this DateTime time) => (int)time.DayOfWeek > 0 && (int)time.DayOfWeek < 6;

        public static bool IsPeakTime(this DateTime time) => time.IsWeekDay() && time.Hour >= 8 && time.Hour < 20;

        public static bool IsOffPeakTime(this DateTime time) => !time.IsPeakTime();

        public static bool IsLastDayOfMonth(this DateTime time) => time.Day == time.GetMonthAhead().Start.AddDays(-1).Day;

        public static bool IsLastTimeOfMonth(this DateTime time) => time.Day == time.GetMonthAhead().Start.AddDays(-1).Day && time.Hour == 23 && time.Minute == 45;

        public static bool IsSixthOfDay(this DateTime time) => time.Hour == time.GetSixthOfDayStartTime().Hour;

        /// <summary>
        /// Erweiterungsmethode nimmt Zeitstempel und Dauer und gibt Peak-Viertelstunden-Zeiten in Liste zurück
        /// </summary>
        /// <param name="timeStamp">DateTime-Zeitstempel, ab dem die Peak-Viertelstunden bestimmt werden sollen</param>
        /// <param name="duration">TimeSpan-Dauer, für die die Viertelstunden-Zeiten bestimmt werden sollen</param>
        /// <returns>Liste mit DateTime-Zeitstempeln der Peakzeiten im Zeitraum</returns>
        public static List<DateTime> GetPeakTimes(this TimeSpan duration, DateTime timeStamp) => GetTimeVector(timeStamp, duration, t => t.IsPeakTime());

        /// <summary>
        /// Erweiterungsmethode nimmt Zeitstempel und Dauer und gibt Offpeak-Viertelstunden-Zeiten in Liste zurück
        /// </summary>
        /// <param name="timeStamp">DateTime-Zeitstempel, ab dem die OffPeak-Viertelstunden bestimmt werden sollen</param>
        /// <param name="duration">TimeSpan-Dauer, für die die Viertelstunden-Zeiten bestimmt werden sollen</param>
        /// <returns>Liste mit DateTime-Zeitstempeln der OffPeakzeiten im Zeitraum</returns>
        public static List<DateTime> GetOffPeakTimes(this TimeSpan duration, DateTime timeStamp) => GetTimeVector(timeStamp, duration, t => t.IsOffPeakTime());

        public static (DateTime Start, TimeSpan Duration) GetMonthAhead(this DateTime currentTime)
        {
            DateTime time = new DateTime(currentTime.Year, currentTime.Month, 1, 0, 0, 0).AddMonths(1);

            return (time, new TimeSpan(DateTime.DaysInMonth(time.Year, time.Month), 0, 0, 0));
        }

        /// <summary>
        /// Gibt die Position in einem gestaffelten Zeitintervall zurück
        /// </summary>
        public static int GetPositionOfTimeInterval(DateTime TimeStamp, DateTime StartTime, TimeSpan StepDuration)
        {
            var position = (int)Math.Truncate((TimeStamp - StartTime).TotalMinutes / StepDuration.TotalMinutes);
            return position;
        }

        /// <summary>
        /// Gibt die Anzahl der Teile in einem gestaffelten Zeitintervall zurück
        /// </summary>
        public static int GetCountOfTimeIntervals(TimeSpan Duration, TimeSpan StepDuration)
            => (int)Math.Truncate(Duration.TotalMinutes / StepDuration.TotalMinutes);

        public static DateTime GetEndOfDuration(this (DateTime timeStamp, TimeSpan duration, TimeSpan step) timeFrame) => timeFrame.timeStamp.Add(timeFrame.duration);

        public static DateTime GetEndOfDuration(DateTime timeStamp, TimeSpan duration) => timeStamp.Add(duration);

        public static bool IsInTimeFrame(this (DateTime start, DateTime end) timeFrame, DateTime timeToCheck) => timeToCheck >= timeFrame.start && timeToCheck < timeFrame.end;
        
        public static DateTime Add(this DateTime timeStamp, TimeSpanSave duration)
            => timeStamp.AddYears(duration.Years).AddMonths(duration.Months).AddDays(duration.Days).AddHours(duration.Hours).AddMinutes(duration.Minutes);
        
        /// <summary>
        /// Returns the Difference from two DateTimes as TimeSpanSave
        /// </summary>
        /// <param name="dates"></param>
        /// <returns></returns>
        public static TimeSpanSave GetDiff(params DateTime[] dates)
        {
            int IdOfLatest = 0;
            int IdOfOther = 1;
            if (dates[1].Ticks > dates[0].Ticks)
            {
                IdOfLatest = 1;
                IdOfOther = 0;
            }
            int YearsCount = dates[IdOfLatest].Year - dates[IdOfOther].Year - 1;
            for (int i = YearsCount; i <= 3000; i++)
            {
                if (dates[IdOfOther].AddYears(i) <= dates[IdOfLatest])
                    YearsCount = i;
                else
                    break;
            }
            int MonthsCount = dates[IdOfLatest].Month - dates[IdOfOther].Month - 1;
            for (int i = MonthsCount; i < 12; i++)
            {
                if (dates[IdOfOther].AddMonths(i) <= dates[IdOfLatest])
                    MonthsCount = i;
                else
                    break;
            }
            DateTime DateForDayCounting = dates[IdOfOther].AddYears(YearsCount).AddMonths(MonthsCount);
            TimeSpan DurationSmallerMonth = dates[IdOfLatest] - DateForDayCounting;
            return new TimeSpanSave(YearsCount, MonthsCount, DurationSmallerMonth.Days, DurationSmallerMonth.Hours, DurationSmallerMonth.Minutes);
        }

        public static string ConvertToString(this TimeSpanSave timeSpan)
        {
            return "Y" + timeSpan.Years + "M" + timeSpan.Months + "D" + timeSpan.Days + "H" + timeSpan.Hours + "m" + timeSpan.Minutes;
        }

        public static TimeSpanSave TimeSpanSaveFromString(this string text)
        {
            var DateArray = text.Split('Y', 'M', 'D', 'H', 'm');
            return new TimeSpanSave(DateArray.Skip(1).Select(item => (int)Convert.ToInt64(item)).ToArray());
        }

        #endregion

        #region float-ExtensionMethods
        /// <summary>
        /// Erweiterungs-Methode multipliziert Wert mit einem zufälligen Wert in den definierten Grenzen von rndMin und rndMax
        /// </summary>
        /// <typeparam name="T">Generischer Seed-Wert-Typ</typeparam>
        /// <param name="val">Wert, der multipliziert werden soll</param>
        /// <param name="rndMin">Minimaler Multiplikationswert</param>
        /// <param name="rndMax">Maximaler Multiplikationswert</param>
        /// <param name="Seed">Seed-Wert</param>
        /// <returns>mit Random-Zahl multiplizierter Wert</returns>
        public static float MultRndBetween(this float val, float rndMin, float rndMax, int seed = 0)
        {
            Random random;
            if (seed != 0)
                random = new Random(seed);
            else
                random = new Random();

            return val * ((float)random.NextDouble() * (rndMax - rndMin) + rndMin);
        }

        /// <summary>
        /// Erweiterungs-Methode multipliziert Wert mit einem zufälligen Wert in den definierten Grenzen von rndMin und rndMax
        /// </summary>
        /// <param name="val">Wert, der multipliziert werden soll</param>
        /// <param name="rndMin">Minimaler Multiplikationswert</param>
        /// <param name="rndMax">Maximaler Multiplikationswert</param>
        /// <returns>mit Random-Zahl multiplizierter Wert</returns>
        public static float MultRndBetween(this float val, float rndMin, float rndMax)
        {
            Random random = new Random();
            return val * ((float)random.NextDouble() * (rndMax - rndMin) + rndMin);
        }

        /// <summary>
        /// Erweiterungs-Methode zur Umrechnung eines Grad-Wertes in Bogenmaß
        /// </summary>
        /// <param name="val"></param>
        /// <returns>Wert im Bogenmaß</returns>
        /// 
        public static float ToRadians(this float val) => (float)(Math.PI / 180) * val;

        /// <summary>
        /// Erweiterungs-Methode zur Umrechnung eines Bogenmaß-Wertes in Grad
        /// </summary>
        /// <param name="val"></param>
        /// <returns>Wert im Grad</returns>
        public static float ToDegrees(this float val) => (float)(180 / Math.PI) * val;

        /// <summary>
        /// Methode gibt den Sinus eines Winkels in Grad zurück
        /// </summary>
        /// <param name="val">Winkel in Degrees</param>
        /// <returns>(float) Sinus eines Winkels</returns>
        public static float SinD(this float val) => (float)Math.Sin(val.ToRadians());

        /// <summary>
        /// Methode gibt den Cosinus eines Winkels in Grad zurück
        /// </summary>
        /// <param name="val">Winkel in Degrees</param>
        /// <returns>(float) Cosinus eines Winkels</returns>
        public static float CosD(this float val) => (float)Math.Cos(val.ToRadians());

        /// <summary>
        /// Erweiterungs-Methode rundet Wert auf nächsten 0.5-Wert
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Gerundeten Wert als float</returns>
        public static float RoundToNearestHalf(this float value) => (float)Math.Round(value * 2) / 2;

        public static float Pow(this float value, float exp) => (float)Math.Pow(value, exp);
        public static float Pow(this int value, float exp) => (float)Math.Pow(value, exp);

        public static float Round(this float value, int dec) => (float)Math.Round(value, dec);
        #endregion

        #region IEnumerable-ExtensionMethods
        public static IEnumerable<T> WhereOrEmptyList<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            if (source.Count() > 0)
                return source.Where(predicate);
            else
                return new List<T>().AsEnumerable();
        }

        public static IEnumerable<TResult> SelectOrEmptyList<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            if (source.Count() > 0)
                return source.Select(selector);
            else
                return new List<TResult>().AsEnumerable();
        }
        public static IEnumerable<T> Append<T>(this IEnumerable<T> list, IEnumerable<T> elements2Add)
        {
            var Elements2Add = elements2Add.ToList();
            list.ToList().AddRange(Elements2Add);
            return list;
        }

        #endregion

        #region List-ExtensionMethods
        public static void ForEachOrNothing<T>(this List<T> source, Action<T> action)
        {
            if (source.Count() > 0)
                source.ForEach(action);
        }
        public static List<List<T>> Transpose<T>(this List<List<T>> arr) => arr.SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();
        public static IEnumerable<IEnumerable<T>> Transpose<T>(this IEnumerable<IEnumerable<T>> arr) => arr.SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.AsEnumerable());

        public static List<float> Normalize(this List<float> list, float normalizeBy = default, float multiplier = 1)
        {
            if (normalizeBy == default)
                normalizeBy = list.Max();

            for (int i = 0; i < list.Count; i++)
                list[i] = (list[i] / normalizeBy) * multiplier;

            return list;
        }

        /// <summary>
        /// Erweiterungs-Methode gibt Liste der Indices einer Input-Liste zurück
        /// </summary>
        /// <param name="list">Liste deren Indices bestimmt werden sollen</param>
        /// <returns></returns>
        /// 
        public static List<float> GetIndices<T>(this List<T> list) => list.Select((v, i) => new { v, i }).Select(v => Convert.ToSingle(v.i)).ToList();

        /// <summary>
        /// Erweiterungs-Methode nimmt Liste aus floats und interpoliert mit LinearSpline auf neu Sample-Anzahl
        /// </summary>
        /// <param name="list">Liste zum Resamplen</param>
        /// <param name="newStepCount">(float) Neue Anzahl Samples</param>
        /// <returns>Interpolierte Liste</returns>
        public static List<float> ResampleLinear(this List<float> list, float newStepCount)
        {
            List<float> interpList = new List<float>();
            double[] indices = list.GetIndices().ConvertAll(el => (double)el).ToArray();
            double[] values = list.ConvertAll(el => (double)el).ToArray();

            LinearSpline linSpline = LinearSpline.InterpolateSorted(indices, values);

            float stepIncrement = (list.Count / newStepCount);

            for (float i = 0; i < list.Count; i = i + stepIncrement)
                interpList.Add((float)linSpline.Interpolate(i));

            return interpList;
        }

        /// <summary>
        /// Erweiterungs-Methode interpoliert mit LinearSpline zwischen zwei Punkten auf neu Sample-Anzahl
        /// </summary>
        /// <param name="A">Startwert</param>
        /// <param name="B">Endwert</param>
        /// <param name="newStepCount">(float) Neue Anzahl Samples</param>
        /// <returns>Interpolierte Liste</returns>
        public static List<float> InterpolateLinear(this float A, float B, float newStepCount)
        {
            List<float> interpList = new List<float>();

            LinearSpline linSpline = LinearSpline.InterpolateSorted(new double[] { 0, 1 }, new double[] { A, B });

            float stepIncrement = 1 / newStepCount;

            for (int i = 0; i <= newStepCount; i++)
                interpList.Add((float)linSpline.Interpolate(i * stepIncrement));

            return interpList;
        }

        public static void ToCSV(this List<float> values, params string[] itemNames) => CSV.ToCSV(values, itemNames);
        public static void ToCSV(this List<(float, float)> values, params string[] itemNames) => CSV.ToCSV(values, itemNames);
        public static void ToCSV(this List<(float, float, float)> values, params string[] itemNames) => CSV.ToCSV(values, itemNames);
        #endregion

        #region BlockingCollection-ExtensionMethods

        public static IEnumerable<TResult> SelectManyOrEmptyList<TStart, TResult>(this BlockingCollection<TStart> source, Func<TStart, IEnumerable<TResult>> selector)
        {
            if (source.Count() > 0)
                return source.SelectMany(selector);
            else
                return new List<TResult>().AsEnumerable();
        }

        public static void AddRange<T>(this BlockingCollection<T> list, IEnumerable<T> elements2Add)
        {
            var Elements2Add = elements2Add.ToList();
            Parallel.ForEach(Elements2Add, el => list.Add(el));
        }

        #endregion

        #region Other ExtensionMethods
        public static bool IsFuturesMarket(this Market market) => market is FuturesMarket;
        public static bool IsDayAheadMarket(this Market market) => market is DayAheadMarket;
        public static bool IsControlReserveMarket(this Market market)=> market is ControlReserve_Capacity || market is ControlReserve_Energy;
        public static bool IsControlReserve_Capacity(this Market market) => market is ControlReserve_Capacity;
        public static bool IsControlReserve_Energy(this Market market) => market is ControlReserve_Energy;

        public static bool IsIntradayMarket(this Market market) => market is IntradayMarket;

        public static List<(bool IsVariableRenewable, List<IAsset> AssetsPart)>  DifferByIsVariableRenewable(this IEnumerable<IAsset> assets)
        {
            return assets.GroupBy(item => item.IsVariableRenewable, (el1, el2) => (el1, el2.ToList() )).ToList();
        }
        public static float GetWeightedValue(IEnumerable<float> values, IEnumerable<float> weights) => weights.Sum() != 0 ? values.Zip(weights, (val, wght) => val * wght).Sum() / weights.Sum() : 0;


        #endregion
    }
}

