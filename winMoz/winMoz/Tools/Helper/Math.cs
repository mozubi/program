﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace winMoz.Helper
{
    public static class MathHelper
    {

        /// <summary>
        /// Methode randomisiert Reihenfolge einer Liste über den RNG der System.Security.Cryptography-Bibliothek
        /// (Fisher-Yates-Shuffle)
        /// </summary>
        /// <typeparam name="T">Typ der Liste, die randomisiert werden soll</typeparam>
        /// <param name="list">Liste, die randomisiert werden soll</param>
        public static void Shuffle<T>(this List<T> list)
        {
            if (list.Count < 255) // To Fix für größere Zahl an zu shuffelnden Agenten
            {

                RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
                int n = list.Count;

                while (n > 1)
                {
                    byte[] box = new byte[1];
                    do provider.GetBytes(box);
                    while (!(box[0] < n * (Byte.MaxValue / n)));
                    int k = (box[0] % n);
                    n--;
                    T value = list[k];
                    list[k] = list[n];
                    list[n] = value;
                }
            }
        }

        /// <summary>
        /// Methode gibt Moving-Average über gegebenen WindowSize zurück; Implementierung durch MathNet.Numerics
        /// </summary>
        /// <param name="list"></param>
        /// <param name="windowSize"></param>
        /// <returns></returns>
        public static List<float> MovAverage(this List<float> list, int windowSize)
        {
            return MathNet.Numerics.Statistics.Statistics.MovingAverage(list.ConvertAll(el => (double)el), windowSize).ToList().ConvertAll(el => (float)el);
        }

        /// <summary>
        /// Methode liefert normalverteilte Zufallszahlen mit Erwartungswert 1 definiertem Setzwert, Standardabweichung und Anzahl
        /// </summary>
        /// <param name="seed">Setzwert für die Ziehung der Zufallszahlen</param>
        /// <param name="std">Standardabweichung der Zufallszahlen</param>
        /// <param name="amount">Anzahl an Zufallszahlen</param>
        /// <returns>Liste mit Zufallszahlen</returns>
        public static List<float> GetRandomDistribution(float mean, float std, int amount, int seed = 0)
        {
            double[] rndNumbers = new double[amount];
            var rnd = new SystemRandomSource(seed, true);
            //var dist = Normal.WithMeanStdDev(mean, std, rnd);
            //dist.Samples(rndNumbers);

            StudentT.Samples(rnd, rndNumbers, mean, std, amount);

            return rndNumbers.ToList().ConvertAll(el => (float)el);
        }

        /// <summary>
        /// Methode lädt randomisierte Folge von Setzwerten mit definiertem Setzwert
        /// </summary>
        /// <param name="seed">Setzwert zur Ziehung von Setzwerten</param>
        /// <param name="amount">Anzahl an zufälligen Setzwerten</param>
        /// <returns>Liste mit zufälligen Integer-Setzwerten</returns>
        public static List<int> GetRandomSeeds(int seed, int amount)
        {
            int[] randomSeeds = new int[amount];

            new MersenneTwister(seed, true).NextInt32s(randomSeeds);

            return randomSeeds.ToList();
        }

        /// <summary>
        /// Methode berechnet den Root-Mean-Squared-Error für zwei übergebene Listen
        /// </summary>
        /// <param name="actual"></param>
        /// <param name="predicted">[%]</param>
        /// <returns></returns>
        public static float RMSE(List<float> actual, List<float> predicted)
        {
            var squaredError = actual.Zip(predicted, (act, pred) => Math.Pow((pred - act), 2));
            var averageSqErr = squaredError.Average();

            return (float)Math.Sqrt(averageSqErr);
        }

        public static float MAE(List<float> actual, List<float> predicted)
        {
            var absError = actual.Zip(predicted, (act, pred) => Math.Abs(pred - act));

            return absError.Average();
        }

        public static decimal GetNormVerteilt(float Mittelwert, float StabW, int rndSeed)
        {
            //Einzelne Normalverteilte Zufallszahl für Testfunktionen Blockgebote / Marktkoppelung

            Random r = new Random(rndSeed);
            float a, b, x;
            decimal Res;

            x = (float)r.NextDouble();
            if (x < 0.00001) { x = 0.00001F; }
            if (x > 0.99999) { x = 0.99999F; }
            a = (float)Math.Sqrt(-2 * Math.Log(x));
            b = (float)Math.Cos(2 * Math.PI * Math.Log(x));

            Res = (decimal)(StabW * (a * b) + Mittelwert);

            return Res;

        }

        public static List<float> NormalizeMinMax(this IEnumerable<float> raw)
        {
            var min = raw.Min();
            var max = raw.Max();

            raw = raw.ToList().Select(value => value = (value - min) / (max - min));
            return raw.ToList();
        }

    }
}



