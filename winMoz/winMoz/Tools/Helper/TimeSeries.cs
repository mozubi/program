﻿using Google.OrTools.ConstraintSolver;
using MathNet.Numerics.Interpolation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using winMoz.Agents;
using winMoz.Simulation;

// TODO: Rename namespace
namespace winMoz.Helper
{
    public static class TimeSeries
    {

        #region Extension
        /// <summary>
        /// Erweiterungsmethode überprüft TimeSeries auf Gültigkeit und Regelmäßigkeit in Zeitraum und gibt Bool zurück
        /// </summary>
        /// <param name="tsIn">TimeSeries, die überprüft werden soll</param>
        /// <param name="startTime">Startzeitpunkt der Überprüfung</param>
        /// <param name="duration">Dauer der Überprüfung</param>
        /// <returns>Bool - TimeSeries gültig/ungültig</returns>
        public static bool IsValid<T>(this List<(DateTime TimeStamp, T)> tsIn, DateTime startTime, TimeSpan duration)
        {
            if (tsIn == null || tsIn.Count == 0)
                return false;

            TimeSpan timeStep = tsIn[1].TimeStamp - tsIn[0].TimeStamp;

            bool startIsFound = tsIn.Exists(el => el.TimeStamp == startTime);
            bool endIsFound = tsIn.Exists(el => el.TimeStamp == (startTime + duration - timeStep));
            bool isRegular = tsIn.IsRegular();


            if (startIsFound && endIsFound && isRegular) return true;

            return false;
        }
        public static bool IsRegular<T>(this IEnumerable<(DateTime TimeStamp, T)> ts) => ts.Select(el => el.TimeStamp).ToList().IsRegular();
        public static bool IsRegular<T1, T2>(this IEnumerable<(DateTime TimeStamp, T1, T2)> ts) => ts.Select(el => el.TimeStamp).ToList().IsRegular();
        public static bool IsRegular<T1, T2, T3>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3)> ts) => ts.Select(el => el.TimeStamp).ToList().IsRegular();
        public static bool IsRegular<T1, T2, T3, T4>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3, T4)> ts) => ts.Select(el => el.TimeStamp).ToList().IsRegular();
        public static bool IsRegular<T1, T2, T3, T4, T5>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3, T4, T5)> ts) => ts.Select(el => el.TimeStamp).ToList().IsRegular();

        private static bool IsRegular(this List<DateTime> ts)
        {
            if (ts.Count < 2) return true;

            var timeStep = ts[1] - ts[0];

            for (int i = 1; i < ts.Count; i++)
                if (ts[i] - ts[i - 1] != timeStep) return false;

            return true;
        }

        public static (DateTime TimeStamp, T1 Value) GetItem<T1>(this IEnumerable<(DateTime TimeStamp, T1)> tsIn, DateTime timeStamp)
        {
            var entry = tsIn.SingleOrDefault(el => el.TimeStamp == timeStamp);

            if (entry.TimeStamp == default)
            {
                Log.Error($"Value not found @ {timeStamp}. Replacing with default.");
                return (timeStamp, default);
            }
            else
                return entry;
        }

        public static TimeSpan Duration(this IEnumerable<DateTime> tsIn)
        {
            if (tsIn != null && tsIn.Count() > 0)
                if ((tsIn.ElementAtOrDefault(1) - tsIn.ElementAt(0)).TotalMinutes == 15)
                    return tsIn.Last().AddMinutes(15) - tsIn.First();
                else
                    return tsIn.Last() - tsIn.First();

            return default;
        }
        public static TimeSpan Duration<T1>(this IEnumerable<(DateTime TimeStamp, T1)> tsIn) => tsIn.Select(el => el.TimeStamp).Duration();
        public static TimeSpan Duration<T1, T2>(this IEnumerable<(DateTime TimeStamp, T1, T2)> tsIn) => tsIn.Select(el => el.TimeStamp).Duration();
        public static TimeSpan Duration<T1, T2, T3>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3)> tsIn) => tsIn.Select(el => el.TimeStamp).Duration();
        public static TimeSpan Duration<T1, T2, T3, T4>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3, T4)> tsIn) => tsIn.Select(el => el.TimeStamp).Duration();

        public static void ToCSV(this List<(DateTime, float)> ts, params string[] itemNames) => CSV.ToCSV(ts, itemNames);
        public static void ToCSV(this List<(DateTime, float, float)> ts, params string[] itemNames) => CSV.ToCSV(ts, itemNames);
        public static void ToCSV(this List<(DateTime, float, float, float)> ts, params string[] itemNames) => CSV.ToCSV(ts, itemNames);
        public static void ToCSV(this List<(DateTime, float, float, float, float)> ts, params string[] itemNames) => CSV.ToCSV(ts, itemNames);


        public static (DateTime, T1) At<T1>(this IEnumerable<(DateTime TimeStamp, T1)> ts, DateTime timeStamp) => ts.SingleOrDefault(el => el.TimeStamp == timeStamp);
        public static (DateTime, T1, T2) At<T1, T2>(this IEnumerable<(DateTime TimeStamp, T1, T2)> ts, DateTime timeStamp) => ts.SingleOrDefault(el => el.TimeStamp == timeStamp);
        public static (DateTime, T1, T2, T3) At<T1, T2, T3>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3)> ts, DateTime timeStamp) => ts.SingleOrDefault(el => el.TimeStamp == timeStamp);
        public static (DateTime, T1, T2, T3, T4) At<T1, T2, T3, T4>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3, T4)> ts, DateTime timeStamp) => ts.SingleOrDefault(el => el.TimeStamp == timeStamp);
        public static (DateTime, T1, T2, T3, T4, T5) At<T1, T2, T3, T4, T5>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3, T4, T5)> ts, DateTime timeStamp) => ts.SingleOrDefault(el => el.TimeStamp == timeStamp);
        public static (DateTime, T1, T2, T3, T4, T5, T6) At<T1, T2, T3, T4, T5, T6>(this IEnumerable<(DateTime TimeStamp, T1, T2, T3, T4, T5, T6)> ts, DateTime timeStamp) => ts.SingleOrDefault(el => el.TimeStamp == timeStamp);
        #endregion

        #region Merging
        public enum Method { Average, Sum, Max, Min, First, Last }

        public static List<(DateTime TimeStamp, float)> Merge(this List<(DateTime, float)> ts, Method method = Method.First, params List<(DateTime, float)>[] lsts)
        {
            foreach (var lst in lsts) ts = ts.Concat(lst).ToList();
            return ts.GroupBy(el => el.Item1.Ticks).Select(el => (el.First().Item1, GetMethod(method).Invoke(el.Select(it => it.Item2)))).ToList();
        }

        public static List<(DateTime TimeStamp, float, float)> Merge(this List<(DateTime, float, float)> ts, Method method = Method.First, params List<(DateTime, float, float)>[] lsts)
        {
            foreach (var lst in lsts) ts = ts.Concat(lst).ToList();
            return ts.GroupBy(el => el.Item1.Ticks).Select(el => (el.First().Item1, GetMethod(method).Invoke(el.Select(it => it.Item2)), GetMethod(method).Invoke(el.Select(it => it.Item3)))).ToList();
        }

        public static List<(DateTime TimeStamp, float, float, float)> Merge(this List<(DateTime, float, float, float)> ts, Method method = Method.First, params List<(DateTime, float, float, float)>[] lsts)
        {
            foreach (var lst in lsts) ts = ts.Concat(lst).ToList();
            return ts.GroupBy(el => el.Item1.Ticks).Select(el => (el.First().Item1, GetMethod(method).Invoke(el.Select(it => it.Item2)), GetMethod(method).Invoke(el.Select(it => it.Item3)), GetMethod(method).Invoke(el.Select(it => it.Item4)))).ToList();
        }

        private static Func<IEnumerable<float>, float> GetMethod(Method method)
        {
            switch (method)
            {
                case Method.Average: return Enumerable.Average;
                case Method.Sum: return Enumerable.Sum;
                case Method.Max: return Enumerable.Max;
                case Method.Min: return Enumerable.Min;
                case Method.First: return Enumerable.First;
                default: return Enumerable.Last;
            }
        }
        #endregion
    }
}
