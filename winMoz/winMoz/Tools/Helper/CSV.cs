﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using winMoz.Assets.Schedules;
using winMoz.Simulation;

namespace winMoz.Helper
{
    public static class CSV
    {

        private static string path => $@"{Config.ProjectPath}\winMoz\Data\Files\Export\Export_{DateTime.Now.ToString("yyyyMMddHHmm")}.csv";

        #region Extensions
        public static void ToCSV(List<float> values, string[] itemNames = default, string filepath = default) => Write2CSV(filepath, itemNames, null, values.ToArray());
        public static void ToCSV(List<(float, float)> values, string[] itemNames = default, string filepath = default) => Write2CSV(filepath, itemNames, null, values.Select(el => el.Item1).ToArray(), values.Select(el => el.Item2).ToArray());
        public static void ToCSV(List<(DateTime TimeStamp, List<float> Values)> values, string[] itemNames = default, string filepath = default) => Write2CSV(filepath, itemNames, values.Select(item => item.TimeStamp).ToList(), values.Select(item => item.Values.ToArray()).ToArray());

        public static void ToCSV(List<(float, float, float)> values, string[] itemNames = default, string filepath = default) => Write2CSV(filepath, itemNames, null, values.Select(el => el.Item1).ToArray(), values.Select(el => el.Item2).ToArray(), values.Select(el => el.Item3).ToArray());
        public static void ToCSV(List<(DateTime, float)> ts, string[] itemNames = default, string filepath = default) => Write2CSV(filepath, itemNames, ts.Select(el => el.Item1), ts.Select(el => el.Item2).ToArray());
        public static void ToCSV(List<(DateTime, float, float)> ts, string[] itemNames = default, string filepath = default)
            => Write2CSV(filepath, itemNames, ts.Select(el => el.Item1), ts.Select(el => el.Item2).ToArray(), ts.Select(el => el.Item3).ToArray());
        public static void ToCSV(List<(DateTime, float, float, float)> ts, string[] itemNames = default, string filepath = default)
            => Write2CSV(filepath, itemNames, ts.Select(el => el.Item1), ts.Select(el => el.Item2).ToArray(), ts.Select(el => el.Item3).ToArray(), ts.Select(el => el.Item4).ToArray());
        public static void ToCSV(List<(DateTime, float, float, float, float)> ts, string[] itemNames = default, string filepath = default)
            => Write2CSV(filepath, itemNames, ts.Select(el => el.Item1), ts.Select(el => el.Item2).ToArray(), ts.Select(el => el.Item3).ToArray(), ts.Select(el => el.Item4).ToArray(), ts.Select(el => el.Item5).ToArray());
        public static void ToCSV(List<(DateTime, float, float, float, float, float)> ts, string[] itemNames = default, string filepath = default)
            => Write2CSV(filepath, itemNames, ts.Select(el => el.Item1), ts.Select(el => el.Item2).ToArray(), ts.Select(el => el.Item3).ToArray(), ts.Select(el => el.Item4).ToArray(), ts.Select(el => el.Item5).ToArray(), ts.Select(el => el.Item6).ToArray());
        #endregion

        internal static void Write2CSV(string filepath = default, string[] itemNames = default, IEnumerable<DateTime> times = null, params float[][] series)
        {
            if (filepath == default) filepath = path;
            RecreateFile(filepath);

            string nameStr = "";
            int id = 1;
            if (times != null) nameStr += "Time;";

            for (int i = 0; i < series.Count(); i++)
                if (itemNames == null || i >= itemNames.Count() || itemNames[i] == "")
                {
                    nameStr += "Value" + $"{id}" + ";";
                    id++;
                }
                else nameStr += itemNames[i] + ";";

            var export = new List<string>();
            for (int rw = 0; rw < series.First().Count(); rw++)
            {
                string value = "";
                for (int col = 0; col < series.Count(); col++)
                    value += Convert.ToString(series[col][rw], CultureInfo.InvariantCulture) + ";";
                export.Add(value);
            }

            if (times != null)
                export = export.Zip(times, (val, tm) => tm.ToString("MM/dd/yyyy HH:mm") + ";" + val).ToList();

            File.AppendAllLines(filepath, export.Prepend(nameStr));
        }

        public static void WriteSchedule2CSV(List<ScheduleItem> schedule)
        {
            string file = path;
            RecreateFile(file);

            var stringList = new List<string>();

            stringList.Add("TimeStamp;Power;PowerProcured;PowerDiff;Price;PriceProcured");
            schedule.ForEach(item => stringList.Add(string.Join(";", item.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss"), item.Power, item.PowerProcured, item.PowerDiff)));


            File.AppendAllLines(file, stringList);
        }

        public static List<(DateTime TimeStamp, float Value)> ReadTSCSV2(string path)
        {
            var readout = File.ReadAllLines(path);

            List<(DateTime TimeStamp, float Value)> TS = new List<(DateTime, float)>();
            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    TS.Add((Convert.ToDateTime(values[0]), Convert.ToSingle(values[1], CultureInfo.InvariantCulture)));
                }
            }

            return TS;
        }

        public static List<(float Time, float Value)> ReadTSrelCSV2(string path)
        {
            var readout = File.ReadAllLines(path);

            List<(float Time, float Value)> TS = new List<(float, float)>();
            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    TS.Add((Convert.ToSingle(values[0], CultureInfo.InvariantCulture), Convert.ToSingle(values[1], CultureInfo.InvariantCulture)));
                }
            }

            return TS;
        }
        private static void RecreateFile(string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);
            File.Create(filePath).Close();
        }
    }
}
