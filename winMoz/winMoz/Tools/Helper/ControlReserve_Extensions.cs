﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets.Bids;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.Results;
using winMoz.Markets;
using winMoz.Helper;

namespace winMoz.Helper
{
    public static class ControlReserve_Extensions
    {

        /// <summary>
        /// Marktergebnis nach PayAsBid mit einem Käufer berechnen.
        /// </summary>
        /// <param name="max">Positiver Regelleistungsbedarf</param>
        /// <param name="segmentsMax"></param>
        /// <returns></returns>
        public static ControlReserveResult CalculateMarketResultsPayAsBidOneBuyer(List<Bid_Segment> segments, bool isEnergy, float Lower_Bound_Price, float Upper_Price_Bound, bool isNegative = false, List<Bid_Segment> otherUncleared = null, bool lastOfSameAuction = true)
        {
            if (segments.Count() == 0)
                return null;
            else
            {
                float volume = 0;
                float volumeClearedWithMaxPrice = 0;
                float lastPrice = 0;
                float costRevenues = 0;
                float price_Max = 0;
                List<Bid_Segment> cleared = new List<Bid_Segment>();
                List<Bid_Segment> uncleared = new List<Bid_Segment>();
                if (otherUncleared != null)
                    uncleared.AddRange(otherUncleared);
                bool auctionOver = false;
                float volumeLimit = isNegative ? segments.Where(seg => seg.VolumeMW >= 0).Select(seg => -seg.VolumeMW).Sum() : segments.Where(seg => seg.VolumeMW <= 0).Select(seg => -seg.VolumeMW).Sum();
                var segmentsAuction = isNegative ? segments.Where(seg => seg.VolumeMW < 0).OrderBy(item => item.Price).ToList() : segments.Where(seg => seg.VolumeMW > 0).OrderBy(item => item.Price).ToList();
                if (segmentsAuction.Count() > 0)
                {
                float volumeAuctioned = isNegative ? segments.Where(seg => seg.VolumeMW < 0).Select(seg => seg.VolumeMW).Sum() : segments.Where(seg => seg.VolumeMW > 0).Select(seg => seg.VolumeMW).Sum();
                float price_Min = segmentsAuction.First().Price;
                    if (Math.Abs(volumeAuctioned) <= Math.Abs(volumeLimit) && !lastOfSameAuction)
                    {
                        return null;
                    }
                    else
                    {
                        if (volumeLimit != 0)
                        {
                            foreach (Bid_Segment segment in segmentsAuction)
                            {
                                if (!auctionOver)
                                {
                                    if (volumeLimit > 0 ? (volume + segment.VolumeMW < volumeLimit) : (volume + segment.VolumeMW > volumeLimit))
                                    {
                                        volume += segment.VolumeMW;
                                        costRevenues += segment.Price * Math.Abs(segment.VolumeMW) * (float)(isEnergy ? segment.Duration.TotalHours : 1);
                                        cleared.Add(segment);
                                        if (lastPrice == segment.Price) volumeClearedWithMaxPrice += segment.VolumeMW;
                                        else volumeClearedWithMaxPrice = segment.VolumeMW;
                                        lastPrice = segment.Price;

                                    }
                                    else
                                    {
                                        costRevenues += Math.Abs((volumeLimit - volume)) * segment.Price * (float)(isEnergy ? segment.Duration.TotalHours : 1);
                                        volume += (volumeLimit - volume);
                                        if (lastPrice == segment.Price) volumeClearedWithMaxPrice += volumeLimit - volume;
                                        else volumeClearedWithMaxPrice = volumeLimit - volume;
                                        price_Max = segment.Price;
                                        cleared.Add(segment);
                                        auctionOver = true;
                                    }
                                }
                            }
                            if (Math.Abs(volume) < Math.Abs(volumeLimit))
                                price_Max = Upper_Price_Bound;
                            float price = volume != 0 ? costRevenues / Math.Abs(volume * (float)(isEnergy ? segmentsAuction.First().Duration.TotalHours : 1)) : 0;
                            float proRataVolume = segmentsAuction.Where(item => item.Price == price_Max).Select(item => item.VolumeMW * (float)(isEnergy ? item.Duration.TotalHours : 1)).Count() > 0 ? volumeClearedWithMaxPrice / segmentsAuction.Where(item => item.Price == price_Max).Select(item => item.VolumeMW).Sum() : 0;
                            return new ControlReserveResult(segments.First().TimeStamp, segments.First().ShortCode, price, volume * (float)(isEnergy ? segmentsAuction.First().Duration.TotalHours : 1), price_Min, price_Max, isNegative ? proRataVolume : 0, !isNegative ? proRataVolume : 0);
                        }
                        else
                            return new ControlReserveResult(segments.First().TimeStamp, segments.First().ShortCode, price: Lower_Bound_Price, volume: 0, price_Min: Lower_Bound_Price, price_Max: Lower_Bound_Price, proRataVolumePurchase: 1, proRataVolumeSell: 0);
                    }
                }
                else
                    return new ControlReserveResult(segments.First().TimeStamp, segments.First().ShortCode, Upper_Price_Bound, 0, Upper_Price_Bound, Upper_Price_Bound, proRataVolumePurchase: 0, proRataVolumeSell: 1);
            }
        }

        public static List<(float Volume, float Price, float aggVol)>[] GetAggregatedMeritOrders(List<Bid_Segment> segments, float upperPriceBound, float lowerPriceBound, bool isSellDescending)
        {
           return new List<(float Volume, float Price, float aggVol)>[] { GetAggregatedMeritOrder(segments.Where(item => item.IsSell).ToList(), upperPriceBound, lowerPriceBound, isSellDescending), GetAggregatedMeritOrder(segments.Where(item => item.IsBuy).ToList(), upperPriceBound, lowerPriceBound, !isSellDescending) };
        }

        public static List<(float Volume, float Price, float aggVol)> GetAggregatedMeritOrder(List<Bid_Segment> segments, float upperPriceBound, float lowerPriceBound, bool isOrderDescending = false)
        {
            var MeritOrder = segments.Select(item => (item.VolumeMW, item.Price)).ToList().GroupBy(item => item.Price, (group, elements) => new { Volume = elements.Select(item => item.VolumeMW).Sum(), Price = group }).OrderBy(item => item.Price).ToList().Select(item => (item.Volume, item.Price)).ToList();
            float AggregatedVolume = 0;
            List<(float Volume, float Price, float aggVol)> MeritOrderAggregated = new List<(float Volume, float Price, float aggVol)>();
            //Füge Grenze am Anfang ein
            if (isOrderDescending) MeritOrderAggregated.Add((0, upperPriceBound, 0));
            else MeritOrderAggregated.Add((0, lowerPriceBound, 0));

            MeritOrder.ForEach(item =>
                {
                    AggregatedVolume += Math.Abs(item.Volume);
                    MeritOrderAggregated.Add((Math.Abs(item.Volume), Math.Min(item.Price, upperPriceBound), AggregatedVolume));
                }
                );

            //Füge Grenze am Ende ein
            if (isOrderDescending) MeritOrderAggregated.Add((0, lowerPriceBound, AggregatedVolume));
            else MeritOrderAggregated.Add((0, upperPriceBound, AggregatedVolume));

            return MeritOrderAggregated;
        }

        public static bool IsControlReserve_Capacity(this Bid bid) => bid.GetType().Equals(typeof(Bid_ControlReserve_Capacity));
        public static bool IsControlReserve_Energy(this Bid bid) => bid.GetType().Equals(typeof(Bid_ControlReserve_Energy));
        public static bool IsControlReserve_Activation(this Bid bid) => bid.GetType().Equals(typeof(Bid_ControlReserve_Activation));

        public static ShortCode CapacityFromEnum(this ShortCode shortCode, ControlReserveShortCode.ShortCode type, int sixth)
            => new ShortCode(type.ToString() + $"_S{sixth}_C");
        public static ShortCode EnergyFromEnum(this ShortCode shortCode, ControlReserveShortCode.ShortCode type, int sixth)
            => new ShortCode(type.ToString() + $"_S{sixth}_E");
        public static ShortCode ActivationFromEnum(this ShortCode shortCode, ControlReserveShortCode.ShortCode type, int sixth, int quarterHourOfSixth)
            => new ShortCode(type.ToString() + $"_S{sixth}_V{quarterHourOfSixth}_A");

    }
}
