﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace winMoz.Tools.Helper
{
    public abstract class Enumeration : IComparable
    {
        public string Value { get; private set; }

        public int Id { get; private set; }

        public Enumeration(string value, int id) => (Id, Value) = (id, value);

        public string ToString() => Value;

        public override bool Equals(object ToCompare)
        {
            if (!(ToCompare is Enumeration))
                return false;
            else
            {
                bool IsSameType = ToCompare.GetType().Equals(this.GetType());
                bool IsSameValue = Id.Equals(((Enumeration)ToCompare).Id);
                return IsSameType && IsSameValue;
            }
        }

        public static IEnumerable<T> GetAll<T>() where T : Enumeration =>
        typeof(T).GetFields(BindingFlags.Public |
                            BindingFlags.Static |
                            BindingFlags.DeclaredOnly)
                 .Select(f => f.GetValue(null))
                 .Cast<T>();

        public static T FromString<T>(string value) where T : Enumeration
            => GetAll<T>().SingleOrDefault(prop => prop.Value.Equals(value));

        public int CompareTo(object item) => Id.CompareTo(((Enumeration)item).Id);

        public static bool HasUniqueValues<T>() where T : Enumeration
            => GetAll<T>().GroupBy(prop => prop.Value, (el1, el2) => el2.Count() == 1).All(el => el);

        public static bool HasUniqueIds<T>() where T : Enumeration
            => GetAll<T>().GroupBy(prop => prop.Id, (el1, el2) => el2.Count() == 1).All(el => el);
    }
}
