﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using winMoz.Simulation;
using System.Text;
using System.Globalization;

namespace winMoz.Tools.Viz
{
    public static class Plotter_Extensions
    {
        #region public
        public static bool PlotOneXAndManyY(List<List<float>> series, List<string> namesXY, string name_y_axis, string title = "")
        {
            if (series.Count() == namesXY.Count())
            {
                List<string> names = new List<string>();
                names.Add(namesXY[0] + "XData");
                namesXY.Skip(1).ToList().ForEach(item => names.Add(item + "YData"));
                var args = WriteString(series, names);
                RunPythonPlotOneXAndManyY(args, name_y_axis, title);
                return true;
            }
            else
            {
                Log.Error("PlotterExtensions.PlotOneXAndManyY: Number of names not same as number of data");
                return false;
            }
        }

        public static bool PlotManyXY(List<List<(float, float)>> seriesXY, List<string> namesXY, string name_x_axis, string name_y_axis, float plotSize = 1, float transparency = 100, bool firstAsScatter = false, string title = "")
        {
            if (seriesXY.Count() == namesXY.Count())
            {
                List<List<float>> series = new List<List<float>>();
                seriesXY.ForEach(item =>
                {
                    series.Add(item.Select(item2 => item2.Item1).ToList());
                    series.Add(item.Select(item2 => item2.Item2).ToList());
                });
                List<string> names = new List<string>();
                namesXY.ForEach(item => names.AddRange(new List<string>() { item + "XData", item + "YData" }));
                var args = WriteString(series, names);
                RunPythonPlotManyXY(args, plotSize, transparency, firstAsScatter, name_x_axis, name_y_axis, title);
                return true;
            }
            else
            {
                Log.Error("PlotterExtensions.PlotManyXY: Number of names not same as number of data");
                return false;
            }
        }

        public static bool PlotManyXYWithVerticalLines(List<List<(float, float)>> seriesXY, List<string> namesXY, string name_x_axis, string name_y_axis, float line1, float line2, float plotSize = 1, float transparency = 100, bool firstAsScatter = false, string title = "")
        {
            if (seriesXY.Count() == namesXY.Count())
            {
                List<List<float>> series = new List<List<float>>();
                seriesXY.ForEach(item =>
                {
                    series.Add(item.Select(item2 => item2.Item1).ToList());
                    series.Add(item.Select(item2 => item2.Item2).ToList());
                });
                List<string> names = new List<string>();
                namesXY.ForEach(item => names.AddRange(new List<string>() { item + "XData", item + "YData" }));
                var args = WriteString(series, names);
                RunPythonPlotManyXYWithVerticalLines(args, plotSize, transparency, firstAsScatter, line1, line2, name_x_axis, name_y_axis, title);
                return true;
            }
            else
            {
                Log.Error("PlotterExtensions.PlotManyXY: Number of names not same as number of data");
                return false;
            }
        }

        public static bool PlotScatter(List<List<(float, float)>> seriesXY, List<string> namesXY, string name_x_axis, string name_y_axis, float plotSize = 1, float transparency = 100, string title = "")
        {
            if (seriesXY.Count() == namesXY.Count())
            {
                List<List<float>> series = new List<List<float>>();
                seriesXY.ForEach(item =>
                {
                    series.Add(item.Select(item2 => item2.Item1).ToList());
                    series.Add(item.Select(item2 => item2.Item2).ToList());
                });
                List<string> names = new List<string>();
                namesXY.ForEach(item => names.AddRange(new List<string>() { item + "XData", item + "YData" }));
                var args = WriteString(series, names);
                RunPythonPlotScatter(args, plotSize, transparency, name_x_axis, name_y_axis, title);
                return true;
            }
            else
            {
                Log.Error("PlotterExtensions.PlotScatter: Number of names not same as number of data");
                return false;
            }
        }
        #endregion



        #region Write Data to CSV

        private static string WriteString(List<List<float>> collection, List<string> names)
        {
            var exportCollection = collection.ConnectElementsWithSameIndex();

            var export = new StringBuilder(names.TransformToStringRow());
            //Console.WriteLine(export);
            foreach (string row in exportCollection.TransformToStringRows())
                export.Append(row);
            //Console.WriteLine(export.ToString());
            return export.ToString();
        }

        public static List<List<float>> ConnectElementsWithSameIndex(this List<List<float>> collection)
        {
            List<List<float>> exportCollection = new List<List<float>>();
            if (collection.Count() > 1 && collection[0] != null && collection[1] != null)
                exportCollection = collection[0].Zip(collection[1], (el1, el2) => new List<float> { el1, el2 }).ToList();

            for (int i = 2; i < collection.Count(); i++)
            {
                if (collection[i] != null)
                    exportCollection = exportCollection.Zip(collection[i], (el1, el2) => el1.AddAndReturn(el2)).ToList();
            }
            return exportCollection;
        }

        public static List<string> TransformToCSVStringList(this List<List<float>> exportCollection)
            => exportCollection.Select(el => string.Join(";", el.Select(it => it.ToString())) + ";").ToList();

        public static List<string> TransformToStringRows(this List<List<float>> exportCollection)
            => exportCollection.Select(el => string.Join(",", el.Select(it => Convert.ToString(it, CultureInfo.GetCultureInfo("en-GB")))) + ";").ToList();

        public static string TransformToCSVString(this List<string> names)
            => string.Join(";", names.Where(name => name != null).Select(name => string.Join(";", name)).ToList()) + ";";

        public static string TransformToStringRow(this List<string> names)
            => string.Join(",", names.Where(name => name != null).Select(name => string.Join(",", name)).ToList()) + ";";


        public static List<float> AddAndReturn(this List<float> list, float element)
        {
            list.Add(element);
            return list;
        }

        public static List<float> AddRangeAndReturn(this List<float> list, List<float> elements)
        {
            list.AddRange(elements);
            return list;
        }
        #endregion

        private static void RunPythonPlotOneXAndManyY(string args, string name_y_axis = "", string title = "")
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = string.Format("-i \"{0}\" \"{1}\" \"{2}\"", $@"{Config.ProjectPath}\winMoz\Tools\Visualization\Plot_OneXAndManyY.py", name_y_axis, title);
            RunPythonPlot(start, args);
        }

        private static void RunPythonPlotManyXY(string args, float plot_size, float transparency, bool firstAsScatter, string name_x_axis = "", string name_y_axis = "", string title = "")
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = string.Format("-i \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\"", $@"{Config.ProjectPath}\winMoz\Tools\Visualization\Plot_ManyXY.py", name_x_axis, name_y_axis, plot_size, transparency, firstAsScatter ? 1 : 0, title);
            RunPythonPlot(start, args);
        }

        private static void RunPythonPlotManyXYWithVerticalLines(string args, float plot_size, float transparency, bool firstAsScatter, float line1, float line2, string name_x_axis = "", string name_y_axis = "", string title = "")
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = string.Format("-i \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\" \"{7}\" \"{8}\"", $@"{Config.ProjectPath}\winMoz\Tools\Visualization\Plot_ManyXYWithVerticalLines.py", name_x_axis, name_y_axis, plot_size, transparency, firstAsScatter ? 1 : 0, title, line1 * 100, line2 * 100);
            RunPythonPlot(start, args);
        }

        private static void RunPythonPlotScatter(string args, float plot_size, float transparency, string name_x_axis = "", string name_y_axis = "", string title = "")
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = string.Format("-i \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"", $@"{Config.ProjectPath}\winMoz\Tools\Visualization\Plot_Scatter.py", name_x_axis, name_y_axis, plot_size, transparency, title);
            RunPythonPlot(start, args);
        }
        
        private static void RunPythonPlot(ProcessStartInfo start, string args)
        {
            start.FileName = GetPythonFromPATH();
            //start.Arguments = string.Format("-i \"{0}\" \"{1}\"", $@"{Config.ProjectPath}\winMoz\Tools\Visualization\Plot.py", 0);
            start.UseShellExecute = false;// Do not use OS shell
            start.CreateNoWindow = true; // We don't need new window
            start.RedirectStandardInput = true;
            start.RedirectStandardOutput = true;// Any output, generated by application will be redirected back
            start.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
            using (Process process = Process.Start(start))
            {
                using (var writer = process.StandardInput)
                    writer.Write(args);

                //using (StreamReader reader = process.StandardOutput)
                //{
                //    string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                //    if (stderr.Length > 0)
                //    {
                //        Log.Info($"ERROR: Python-Error - ERRMSG{stderr}");
                //        Log.Info($"Info: Python needs to be installed and on PATH. Required Packages: Pandas, Matplotlib");
                //    }

                //    string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                //    Console.WriteLine(result);
                //}
            }
        }

        private static string GetPythonFromPATH()
        {
            var path = Environment.GetEnvironmentVariable("PATH");
            //var pythonPath = $@"C:\Users\mantke\AppData\Local\Programs\Python\Python37\python.exe";
            string pythonPath = null;
            foreach (var p in path.Split(new char[] { ';' }))
            {
                var fullPath = Path.Combine(p, "python.exe");
                if (File.Exists(fullPath))
                {
                    pythonPath = fullPath;
                    break;
                }
            }

            if (pythonPath != null)
            {
                return pythonPath;
            }
            else
            {
                Config.AddPythonToPATH();
                //throw new Exception("Couldn't find python on %PATH%");
            }
            return GetPythonFromPATH();

        }
    }
}
