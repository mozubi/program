import os,sys
import pandas as pd, numpy as np, matplotlib.pyplot as plt, seaborn as sns
from matplotlib import cm
sns.set(style="whitegrid",palette=sns.color_palette("Paired"))

def on_close(event):
    sys.exit()

def plotManyXY(df, xLabel, yLabel, plotSize, transparency, title):
    #drawstyle="steps-post"
    xcols = [col for col in df.columns if 'XData' in col ]
    ycols = [col for col in df.columns if 'YData' in col ]
    ycolsLabel = []
    for i in range(0, len(ycols)):
        ycolsLabel.append(ycols[i].replace("YData", "").replace("Scatter", ""))
    #df.index = df[xcols[0]]
    ax = plt.subplot(111)
    colormap = ["tab:blue", "tab:orange", "r","y", "g", "c", "m", "k", "b", "tab:green", "tab:red", "tab:brown", "tab:pink", "tab:gray", "tab:olive", "tab:cyan2"]
    df.plot(kind="scatter", ax=ax, x = str(xcols[0]), y = str(ycols[0]), s=plotSize, alpha = transparency)
    for i in range(1, len(xcols)):
        j = int(((i + 1) % len(colormap)) * len(colormap) - 1)
        ax = plot(ax, df, xcols[i], ycols[i], plotSize, colormap[i], transparency)
    ax.legend(ycolsLabel)
    ax.set_xlabel(xLabel)
    ax.set_ylabel(yLabel)
    ax.set_title(title)
    fig = ax.get_figure()
    fig.canvas.mpl_connect('close_event', on_close)
    plt.show(block=False)
    

def plot(ax, df, xcol, ycol, plotSize, color, transparency):
    df.plot(kind="scatter", ax=ax, x=xcol, y = ycol, s=plotSize, c=color, alpha=transparency)
    return ax


def getDf(splitted, header):
    values = [row.split(',') for row in splitted[1:-1]]
    values = [np.array(value).astype(np.float) for value in values]
    return pd.DataFrame(data=values,columns=header)


string = str(sys.stdin.read())
splitted = string.split(';')
header = splitted[0].split(",")
xLabel = str(sys.argv[1])
yLabel = str(sys.argv[2])
plotSize = float(sys.argv[3])
transparency = float(sys.argv[4]) / 100
title = str(sys.argv[5])
df = getDf(splitted, header)
plotManyXY(df, xLabel, yLabel, plotSize, transparency, title)