import os,sys
import pandas as pd, numpy as np, matplotlib.pyplot as plt, seaborn as sns
from matplotlib import cm
sns.set(style="whitegrid",palette=sns.color_palette("Paired"))

def on_close(event):
    sys.exit()

def plotManyXY(df, xLabel, yLabel, plotSize, transparency, firstAsScatter, title, line1, line2):
    #drawstyle="steps-post"
    xcols = [col for col in df.columns if 'XData' in col ]
    ycols = [col for col in df.columns if 'YData' in col ]
    ycolsLabel = []
    for i in range(0, len(xcols)):
        ycolsLabel.append(ycols[i].replace("YData", "").replace("Scatter", ""))
    df.index = df[xcols[0]]
    colormap = ["tab:blue", "tab:orange", "r","y", "g", "c", "m", "k", "b", "tab:green", "tab:red", "tab:brown", "tab:pink", "tab:gray", "tab:olive", "tab:cyan2"]
    ax = df.plot(kind = 'scatter', x = str(xcols[0]), y = str(ycols[0]), s=plotSize, c= colormap[0], alpha = transparency) if firstAsScatter == 1 else df.plot(kind = 'line', y = str(ycols[0]))
    for i in range(1, len(xcols)):
        j = int(((i + 1) % len(colormap)) * len(colormap) - 1)
        df, ax = plot(ax, df, xcols[i], ycols[i], plotSize, firstAsScatter, colormap[i])
    if len(ycolsLabel) > 1 and firstAsScatter == 1:
        label1 = ycolsLabel[0]
        ycolsLabel.remove(label1)
        ycolsLabel.append(label1)
    ax.legend(ycolsLabel)
    ax.set_xlabel(xLabel)
    ax.set_ylabel(yLabel)
    plt.axvline(line1,color='red')
    plt.axvline(line2,color='green')
    ax.set_title(title)
    fig = ax.get_figure()
    fig.canvas.mpl_connect('close_event', on_close)
    plt.show(block=False)
    

def plot(ax, df, xcol, ycol, plotSize, fistAsScatter, color):
    df.index = df[xcol]
    if firstAsScatter == 1:
        df[ycol].plot(c = color)
    else:
        df[ycol].plot()
    return (df, ax)


def getDf(splitted, header):
    values = [row.split(',') for row in splitted[1:-1]]
    values = [np.array(value).astype(np.float) for value in values]
    return pd.DataFrame(data=values,columns=header)


string = str(sys.stdin.read())
splitted = string.split(';')
header = splitted[0].split(",")
xLabel = str(sys.argv[1])
yLabel = str(sys.argv[2])
plotSize = float(sys.argv[3])
transparency = float(sys.argv[4]) / 100
firstAsScatter = int(sys.argv[5])
title = str(sys.argv[6])
line1 = float(sys.argv[7]) / 100
line2 = float(sys.argv[8]) / 100
df = getDf(splitted, header)
plotManyXY(df, xLabel, yLabel, plotSize, transparency, firstAsScatter, title, line1, line2)