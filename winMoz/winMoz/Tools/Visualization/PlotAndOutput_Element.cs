﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets;
using winMoz.Markets.Elements;

namespace winMoz.Simulation
{
    public class PlotAndOutput_Element : IPlotAndOutput_Element
    {
        public int Id { get; private set; }

        protected PlotAndOutput_Element() { }
    }

    public class PlotAndOutput_Element_Bid : PlotAndOutput_Element
    {
        public string Market { get; private set; }
        public bool WithStep { get; private set; }
        public bool WithBlock { get; private set; }
        public string Plant_Type { get; private set; }
        public string SubType { get; private set; }
        public int Plant_Id { get; private set; }
        public string ControlReserveType { get; private set; }
        public bool DifferByPlantTypeAndSubType { get; private set; }

        public bool OnlyOnePlantType { get; private set; }

        public bool OnlyOnePlant { get; private set; }

        public bool OnlyOneControlReserveType { get; private set; }

        public bool OnlyCapacityBids { get; private set; }

        private PlotAndOutput_Element_Bid() : base() { }
        public PlotAndOutput_Element_Bid(string market, bool withStep, bool withBlock, bool differByPlantTypeAndSubType, bool onlyOnePlantType, bool onlyOnePlant, bool onlyOneControlReserveType, bool onlyCapacityBids, string plant_Type = null, string subType = null, int plant_Id = 0, string controlReserveType = null) //: base(id)
        {
            Market = market;
            WithStep = withStep;
            WithBlock = withBlock;
            Plant_Type = plant_Type;
            SubType = subType;
            Plant_Id = plant_Id;
            ControlReserveType = controlReserveType;

            DifferByPlantTypeAndSubType = differByPlantTypeAndSubType;
            OnlyOnePlantType = onlyOnePlantType;
            OnlyOnePlant = onlyOnePlant;
            OnlyOneControlReserveType = onlyOneControlReserveType;
            OnlyCapacityBids = onlyCapacityBids;
        }
    }

    public class PlotAndOutput_Element_MeritOrder : PlotAndOutput_Element
    {
        public string SimEvent { get; private set; }
        public bool OnlyOneControlReserveType { get; private set; }
        public string ControlReserveType { get; private set; }

        public PlotAndOutput_Element_MeritOrder(string simEvent, bool onlyOneControlReserveType, string controlReserveType = null) : base()
        {
            SimEvent = simEvent;
            OnlyOneControlReserveType = onlyOneControlReserveType;
            ControlReserveType = controlReserveType;
        }
    }

    public class PlotAndOutput_Element_CoupledMeritOrder : PlotAndOutput_Element
    {
        public MarketAreaEnum MarketArea { get; private set; }
        public bool MarketAreaSpecified { get; private set; }

        private PlotAndOutput_Element_CoupledMeritOrder() : base() { }
        public PlotAndOutput_Element_CoupledMeritOrder(bool marketAreaSpecified, MarketAreaEnum marketArea = null)
        {
            MarketArea = marketArea;
            MarketAreaSpecified = marketAreaSpecified;
        }
    }

    public class PlotAndOutput_Element_Schedule : PlotAndOutput_Element
    {
        public string Plant_Type { get; private set; }

        public int Plant_Id { get; private set; }

        public string SubType { get; private set; }

        public bool DifferByPlantTypeAndSubType { get; private set; }

        public bool OnlyOnePlantType { get; private set; }

        public bool OnlyOnePlant { get; private set; }

        public bool WithPrices { get; private set; }

        public bool IsOutputSimTimeSet { get; private set; }
        public DateTime OutputSimTime { get; private set; }

        private PlotAndOutput_Element_Schedule() : base() { }
        public PlotAndOutput_Element_Schedule(bool differByPlantTypeAndSubType, bool onlyOnePlantType, bool onlyOnePlant, bool withPrices, bool isOutputSimTimeSet, string plant_Type = null, string subType = null, int plant_Id = 0, DateTime outputSimTime = default)
        {
            Plant_Type = plant_Type;
            Plant_Id = plant_Id;
            SubType = subType;
            OutputSimTime = outputSimTime;

            DifferByPlantTypeAndSubType = differByPlantTypeAndSubType;
            OnlyOnePlantType = onlyOnePlantType;
            OnlyOnePlant = onlyOnePlant;
            WithPrices = withPrices;
            IsOutputSimTimeSet = isOutputSimTimeSet;
        }
    }

    public class PlotAndOutput_Element_MarketResults : PlotAndOutput_Element
    {
        public string MarketResultType { get; private set; }

        public bool IsMarketTypeSpecified { get; private set; }

        public bool DifferByMarketType { get; private set; }

        public bool VolumeVersusPrice { get; private set; }
        public bool TimeVersusVolume { get; private set; }
        public bool TimeVersusPrice { get; private set; }

        public bool TimeVersusVolumeAndPrice { get; private set; }
        private PlotAndOutput_Element_MarketResults() : base() { }
        public PlotAndOutput_Element_MarketResults(bool isMarketTypeSpecified, bool differByMarketType, bool volumeVersusPrice, bool timeVersusVolume, bool timeVersusPrice, bool timeVersusVolumeAndPrice, string marketResultType = null)
        {
            MarketResultType = marketResultType;

            IsMarketTypeSpecified = isMarketTypeSpecified;
            DifferByMarketType = differByMarketType;
            VolumeVersusPrice = volumeVersusPrice;
            TimeVersusVolume = timeVersusVolume;
            TimeVersusPrice = timeVersusPrice;
            TimeVersusVolumeAndPrice = timeVersusVolumeAndPrice;
        }
    }
}
