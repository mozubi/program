import os,sys
import pandas as pd, numpy as np, matplotlib.pyplot as plt, seaborn as sns
sns.set(style="whitegrid",palette=sns.color_palette("Paired"))


def on_close(event):
    sys.exit()

def plotTimeseries(df, plotPrices, title, titleX, titleY, titleYSecondAxis):
    #drawstyle="steps-post"
    priceCols = [col for col in df.columns if ('Costs' in col or 'Revenues' in col ) ]
    cols = [col for col in df.columns if ('Costs' not in col and 'Revenues' not in col)]
    colsEmpty = False
    if len(cols) == 0: 
        cols = priceCols
        colsEmpty = True
    ax = df[cols].plot()
    if plotPrices and len(priceCols) > 0 and colsEmpty == False: df[priceCols].plot(secondary_y=True, ax=ax,mark_right=False)
    plt.axhline(0,color='black')
    ax.set_xlabel(titleX)
    ax.set_ylabel(titleY)
    if plotPrices and len(priceCols) > 0 and colsEmpty == False: ax.right_ax.set_ylabel(titleYSecondAxis)
    ax.set_title(title)

    return ax

def __getDf(text:str) -> pd.DataFrame:
    splitted = text.split(";")
    header = splitted[0].split(",")
    values = [row.split(',') for row in splitted[1:-1]]
    try:
        idx = pd.to_datetime([str(value[0]) for value in values])
        start = 1
    except: 
        start=0
        idx = range(0,len(values))
        
    values = [np.array(value[start:]).astype(np.float) for value in values]
    return pd.DataFrame(values,index=idx,columns=header[start:])

df = __getDf(sys.stdin.read())
title = str(sys.argv[2])
titleX = str(sys.argv[3])
titleY = str(sys.argv[4])
titleYSecondAxis = str(sys.argv[5])
ax = plotTimeseries(df, sys.argv[1] == '1', title, titleX, titleY, titleYSecondAxis)
fig = ax.get_figure()
fig.canvas.mpl_connect('close_event', on_close)
plt.show(block=False)