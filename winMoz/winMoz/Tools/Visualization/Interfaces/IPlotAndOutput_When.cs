﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Simulation
{
    public interface IPlotAndOutput_Times
    {
        int Id { get; }
    }
    public interface IPlotAndOutput_Times_Durations : IPlotAndOutput_Times
    {
        DateTime Start { get; }
        DateTime End { get; }
    }
}
