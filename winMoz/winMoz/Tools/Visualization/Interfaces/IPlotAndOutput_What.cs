﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets;
using winMoz.Markets.Elements;

namespace winMoz.Simulation
{
    public interface IPlotAndOutput_Element
    {
        int Id { get; }
    }

    public interface IPlotAndOutput_Element_Bid : IPlotAndOutput_Element
    {
        string Market { get; }
        bool WithStep { get; }
        bool WithBlock { get; }
        string Plant_Type { get; }
        string SubType { get; }
        int Plant_Id { get; }
        bool DifferByPlantTypeAndSubType { get; }

        bool OnlyOnePlantType { get; }

        bool OnlyOnePlant { get; }
    }

    public interface IPlotAndOutput_Element_CoupledMeritOrder : IPlotAndOutput_Element
    {
        MarketAreaEnum MarketArea { get; }
        bool MarketAreaSpecified { get; }
    }

    public interface IPlotAndOutput_Element_Schedule : IPlotAndOutput_Element
    {
        string Plant_Type { get; }

        int Plant_Id { get; }

        string SubType { get; }

        bool DifferByPlantTypeAndSubType { get; }

        bool OnlyOnePlantType { get; }

        bool OnlyOnePlant { get; }

        bool WithPrices { get; }
    }

    public interface IPlotAndOutput_Element_MarketResults : IPlotAndOutput_Element
    {
        string MarketResultType { get; }

        bool IsMarketTypeSpecified { get; }

        bool DifferByMarketType { get; }

        bool VolumeVersusPrice { get; }
        bool TimeVersusVolume { get; }
        bool TimeVersusPrice { get; }

        bool TimeVersusVolumeAndPrice { get; }
    }
}
