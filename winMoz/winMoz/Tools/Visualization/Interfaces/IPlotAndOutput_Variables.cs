﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.DTO;

namespace winMoz.Simulation
{
    public interface IPlotAndOutput_Variables
    {
        List<PlotAndOutput_Variables_Dto> FeaturesToPlot { get; set; }

        List<PlotAndOutput_Variables_Dto> FeaturesToPrintConsole { get; set; }

        List<PlotAndOutput_Variables_Dto> FeaturesToPrintCSV { get; set; }

    }
}
