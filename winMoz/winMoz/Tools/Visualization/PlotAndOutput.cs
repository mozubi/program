﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Information;
using winMoz.Data.Access.DTO;
using winMoz.Markets.Bids;
using winMoz.Assets.Schedules;
using winMoz.Simulation;
using winMoz.Assets;
using winMoz.Markets;
using System.IO;
using System.Globalization;
using winMoz.Markets.Elements;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Results;
using winMoz.Helper;
using winMoz.Assets.Plants;
using winMoz.Markets.ControlReserve;

namespace winMoz.Tools.Viz
{
    public static class PlotAndOutput
    {
        //TODO: Wartbarkeitsindex verbessern -> Referenz ist region Bids

        #region Bids
        public static void Plot__Bids(Market market, List<Bid> bids)
        {
            var PlotActions = PlotAndOutput_Variables_Information.GetPlot();
            var PlotActionsBid = GetPlotAndOutputActionsFromTypes("PlotAndOutput_Element_Bid", "PlotAndOutput_Times_Durations", PlotActions)
                .Where(item => ((PlotAndOutput_Element_Bid)item.Element).Market == market.GetType().Name).ToList();
            if (PlotActionsBid.Count() > 0)
            {
                var GroupedBidsByAction = GroupBidsByAction(PlotActionsBid, bids);
                foreach (var GroupedBid in GroupedBidsByAction)
                    Plot_Bids(GroupedBid, market);
            }
        }

        public static void OutputConsole__Bids(Market market, List<Bid> bids)
        {
            var OutputConsoleActions = PlotAndOutput_Variables_Information.GetConsole();
            var OutputConsoleActionsBid = GetPlotAndOutputActionsFromTypes("PlotAndOutput_Element_Bid", "PlotAndOutput_Times_Durations", OutputConsoleActions)
                .Where(item => ((PlotAndOutput_Element_Bid)item.Element).Market == market.GetType().Name).ToList();
            if (OutputConsoleActionsBid.Count() > 0)
            {
                var GroupedBidsByAction = GroupBidsByAction(OutputConsoleActionsBid, bids);
                foreach (var GroupedBid in GroupedBidsByAction)
                    OutputConsole_Bids(GroupedBid, market);
            }
        }

        public static void OutputCSV__Bids(Market market, List<Bid> bids)
        {
            var OutputCSVActions = PlotAndOutput_Variables_Information.GetCSV();
            var OutputCSVActionsBid = GetPlotAndOutputActionsFromTypes("PlotAndOutput_Element_Bid", "PlotAndOutput_Times_Durations", OutputCSVActions)
                .Where(item => ((PlotAndOutput_Element_Bid)item.Element).Market == market.GetType().Name).ToList();
            if (OutputCSVActionsBid.Count() > 0)
            {
                var GroupedBidsByAction = GroupBidsByAction(OutputCSVActionsBid, bids);
                foreach (var GroupedBid in GroupedBidsByAction)
                    OutputCSV_Bids(GroupedBid, market);
            }
        }

        private static void Plot_Bids((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            if (groupedBid.Bids.Count() > 0 && groupedBid.Action != null)
            {
                if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).DifferByPlantTypeAndSubType)
                    PlotBidsDifferedByPlantType(groupedBid, market);
                else if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyOnePlantType)
                    PlotBidsByPlantType(groupedBid, market);
                else if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyOnePlant)
                    PlotBidsByPlantTypeAndId(groupedBid, market);
                else
                    PlotAllBids(groupedBid);
            }
        }

        private static void OutputConsole_Bids((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            if (groupedBid.Bids.Count() > 0 && groupedBid.Action != null)
            {
                if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).DifferByPlantTypeAndSubType)
                    OutputConsoleBidsDifferedByPlantType(groupedBid, market);
                else if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyOnePlantType)
                    OutputConsoleBidsByPlantType(groupedBid, market);
                else if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyOnePlant)
                    OutputConsoleBidsByPlantTypeAndId(groupedBid, market);
                else
                    OutputConsoleAllBids(groupedBid);
            }
        }

        private static void OutputCSV_Bids((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            if (groupedBid.Bids.Count() > 0 && groupedBid.Action != null)
            {
                if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).DifferByPlantTypeAndSubType)
                    OutputCSVBidsDifferedByPlantType(groupedBid, market);
                else if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyOnePlantType)
                    OutputCSVBidsByPlantType(groupedBid, market);
                else if (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyOnePlant)
                    OutputCSVBidsByPlantTypeAndId(groupedBid, market);
                else
                    OutputCSVAllBids(groupedBid);
            }
        }

        private static void PlotBidsDifferedByPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsDifferedByPlantType = DifferBidSegmentsByPlantType(groupedBid);
            if (market is ControlReserveMarket)
            {
                var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsDifferedByPlantType, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                {
                    if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                    {
                        PlotBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2.Select(item => (item.Plant_Type, item.Segments.Where(seg => seg.IsCapacity).ToList())).ToList(), title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Capacity Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                        PlotBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2.Select(item => (item.Plant_Type, item.Segments.Where(seg => seg.IsEnergy).ToList())).ToList(), title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Energy Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                    }
                    else
                        PlotBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + ", Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                }
            }
            else
                PlotBidsDifferedByPlantType(groupedBid.Action, BidsDifferedByPlantType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);


        }

        private static void OutputConsoleBidsDifferedByPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsDifferedByPlantType = DifferBidSegmentsByPlantType(groupedBid);
            if (market is ControlReserveMarket)
            {
                var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsDifferedByPlantType, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                {
                    if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                    {
                        OutputConsoleBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2.Select(item => (item.Plant_Type, item.Segments.Where(seg => seg.IsCapacity).ToList())).ToList(), title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Capacity Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End + "_");
                        OutputConsoleBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2.Select(item => (item.Plant_Type, item.Segments.Where(seg => seg.IsEnergy).ToList())).ToList(), title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Energy Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End + "_");
                    }
                    else
                        OutputConsoleBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End + "_");
                }
            }
            else
                OutputConsoleBidsDifferedByPlantType(groupedBid.Action, BidsDifferedByPlantType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End + "_");
        }

        private static void OutputCSVBidsDifferedByPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsDifferedByPlantType = DifferBidSegmentsByPlantType(groupedBid);
            if (market is ControlReserveMarket)
            {
                var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsDifferedByPlantType, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                {
                    if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                    {
                        OutputCSVBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2.Select(item => (item.Plant_Type, item.Segments.Where(seg => seg.IsCapacity).ToList())).ToList(), title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Capacity_Bids_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End) + "_");
                        OutputCSVBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2.Select(item => (item.Plant_Type, item.Segments.Where(seg => seg.IsEnergy).ToList())).ToList(), title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Energy_Bids_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End) + "_");
                    }
                    else
                        OutputCSVBidsDifferedByPlantType(groupedBid.Action, controlReserveTypeElement.Item2, title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End) + "_");
                }
            }
            else
                OutputCSVBidsDifferedByPlantType(groupedBid.Action, BidsDifferedByPlantType, title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End) + "_");
        }

        private static void PlotBidsDifferedByPlantType(PlotAndOutput_Variables_Dto Action, List<(string Plant_Type, List<Bid_Segment> Segments)> bidsDifferedByPlantType, string title)
        {
            var BidsDifferedByPlantTypeEqualSize = FillUpUntilAllHaveSameNumberOfElements(bidsDifferedByPlantType);
            Plotter_Extensions.PlotScatter(BidsDifferedByPlantTypeEqualSize.Select(item => item.Item2.Select(seg => (seg.VolumeMW, seg.Price)).ToList()).ToList(), BidsDifferedByPlantTypeEqualSize.Select(bid => bid.Item1).ToList(), "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: title);
        }

        private static void OutputConsoleBidsDifferedByPlantType(PlotAndOutput_Variables_Dto Action, List<(string Plant_Type, List<Bid_Segment> Segments)> bidsDifferedByPlantType, string title)
        {
            foreach (var segmentsByPlantType in bidsDifferedByPlantType) OutputConsoleBids(title + segmentsByPlantType.Plant_Type, segmentsByPlantType.Segments.ToList());
        }

        private static void OutputCSVBidsDifferedByPlantType(PlotAndOutput_Variables_Dto Action, List<(string Plant_Type, List<Bid_Segment> Segments)> bidsDifferedByPlantType, string title)
        {
            OutputCSVBids(title, bidsDifferedByPlantType.Select(item => item.Segments.Select(seg => (item.Plant_Type, seg)).ToList()).SelectMany(item => item).ToList());
        }

        private static List<(string Plant_Type, List<Bid_Segment> Segments)> DifferBidSegmentsByPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid)
        {
            return groupedBid.Bids.Where(bid => bid.Assets != null && bid.Assets.Count() != 0)
                .GroupBy(bid => bid.Assets.First().GetType().Name + (bid.Assets.First() is Block ? "_" + (((Block)bid.Assets.First()).Plant).EconomicsSubType : ""),
                (PlantType, Bids) =>
                (
                PlantType, GetBidSegmentsInDuration(groupedBid.Action, Bids.ToList())
                )).ToList();
        }

        private static List<(string ControlReserveType, List<(string Plant_Type, List<Bid_Segment> Segments)>)> DifferBidSegmentsByControlReserveType(List<(string Plant_Type, List<Bid_Segment> Segments)> groupedBidSegments, bool onlyCapacityBids = false)
        {
            return groupedBidSegments.Select(item =>
                item.Segments.Where(seg => onlyCapacityBids ? seg.ShortCode.Is_Capacity : true).GroupBy(seg => seg.ShortCode.GetEnum(),
                (ControlReserveType, BidSegments) =>
                (
                item.Plant_Type, ControlReserveType.ToString(), BidSegments.ToList()
                )).ToList()).SelectMany(item => item).GroupBy(item => item.Item2, (group, elements) => (group, elements.Select(el => (el.Plant_Type, el.Item3)).ToList())).ToList();
        }

        private static void PlotBidsByPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsPlantType = GetBidsForPlantType(groupedBid);
            if (BidsPlantType.Count() > 0)
            {
                var BidsToPlot = GetBidSegmentsInDuration(groupedBid.Action, BidsPlantType);
                if (market is ControlReserveMarket)
                {
                    var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsToPlot, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                    foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                    {
                        if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                        {
                            PlotBidsByPlantType(controlReserveTypeElement.Item2.Where(seg => seg.IsCapacity).ToList(), plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Capacity Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                            PlotBidsByPlantType(controlReserveTypeElement.Item2.Where(seg => seg.IsEnergy).ToList(), plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Energy Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                        }
                        else
                            PlotBidsByPlantType(controlReserveTypeElement.Item2, plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + ", Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                    }
                }
                else
                    PlotBidsByPlantType(BidsToPlot, plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
            }
        }

        private static void PlotBidsByPlantType(List<Bid_Segment> segments, string plantType, string title)
        {
            Plotter_Extensions.PlotScatter(new List<List<(float, float)>>() { segments.Select(item => (item.VolumeMW, item.Price)).ToList() }, new List<string>() { plantType }, "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: title);
        }

        private static void OutputConsoleBidsByPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsPlantType = GetBidsForPlantType(groupedBid);
            if (BidsPlantType.Count() > 0)
            {
                var BidsToPlot = GetBidSegmentsInDuration(groupedBid.Action, BidsPlantType);
                if (market is ControlReserveMarket)
                {
                    var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsToPlot, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                    foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                    {
                        if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                        {
                            OutputConsoleBidsByPlantType(controlReserveTypeElement.Item2.Where(seg => seg.IsCapacity).ToList(), plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Capacity Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                            OutputConsoleBidsByPlantType(controlReserveTypeElement.Item2.Where(seg => seg.IsEnergy).ToList(), plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Energy Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                        }
                        else
                            OutputConsoleBidsByPlantType(controlReserveTypeElement.Item2, plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + ", Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                    }
                }
                else
                    OutputConsoleBidsByPlantType(BidsToPlot, plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
            }
        }

        private static void OutputConsoleBidsByPlantType(List<Bid_Segment> segments, string plantType, string title)
        {
            OutputConsoleBids(title + "_" + plantType, segments);
        }

        private static void OutputCSVBidsByPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsPlantType = GetBidsForPlantType(groupedBid);
            if (BidsPlantType.Count() > 0)
            {
                var BidsToPlot = GetBidSegmentsInDuration(groupedBid.Action, BidsPlantType);
                if (market is ControlReserveMarket)
                {
                    var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsToPlot, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                    foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                    {
                        if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                        {
                            OutputCSVBidsByPlantType(controlReserveTypeElement.Item2.Where(seg => seg.IsCapacity).ToList(), plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Capacity_Bids_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End));
                            OutputCSVBidsByPlantType(controlReserveTypeElement.Item2.Where(seg => seg.IsEnergy).ToList(), plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Energy_Bids_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End));
                        }
                        else
                            OutputCSVBidsByPlantType(controlReserveTypeElement.Item2, plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_" + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                    }
                }
                else
                    OutputCSVBidsByPlantType(BidsToPlot, plantType: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType, title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End));
            }
        }

        private static void OutputCSVBidsByPlantType(List<Bid_Segment> segments, string plantType, string title)
        {
            OutputCSVBids(title + "_" + plantType, segments.Select(item => (plantType, item)).ToList());
        }

        private static List<Bid> GetBidsForPlantType((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid)
        {
            return groupedBid.Bids
                .Where(item =>
                item.Assets.First().GetType().Name == ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type
                && (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type == "Plant_Thermal" ? ((item.Assets.First() as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType) : true)
                ).ToList();
        }

        private static List<(string ControlReserveType, List<Bid_Segment> Segments)> DifferBidSegmentsByControlReserveType(List<Bid_Segment> segments, bool onlyCapacityBids = false)
        {
            return segments.Where(seg => onlyCapacityBids ? seg.ShortCode.Is_Capacity : true).GroupBy(seg => seg.ShortCode.GetEnum(),
                (ControlReserveType, BidSegments) =>
                (
                ControlReserveType.ToString(), BidSegments.ToList()
                )).ToList();
        }

        private static void PlotBidsByPlantTypeAndId(List<Bid_Segment> segments, string plantTypeAndId, string title)
        {
            Plotter_Extensions.PlotScatter(new List<List<(float, float)>>() { segments.Select(item => (item.VolumeMW, item.Price)).ToList() }, new List<string>() { plantTypeAndId }, "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: title);
        }

        private static void PlotBidsByPlantTypeAndId((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsPlantTypeAndId = GetBidsForPlantTypeAndId(groupedBid);
            if (BidsPlantTypeAndId.Count() > 0)
            {
                var BidsToPlot = GetBidSegmentsInDuration(groupedBid.Action, BidsPlantTypeAndId);
                if (market is ControlReserveMarket)
                {
                    var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsToPlot, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                    foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                    {
                        if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                        {
                            PlotBidsByPlantTypeAndId(controlReserveTypeElement.Item2.Where(seg => seg.IsCapacity).ToList(), plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Capacity Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                            PlotBidsByPlantTypeAndId(controlReserveTypeElement.Item2.Where(seg => seg.IsEnergy).ToList(), plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Energy Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                        }
                        else
                            PlotBidsByPlantTypeAndId(controlReserveTypeElement.Item2, plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + ", Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                    }
                }
                else
                    PlotBidsByPlantTypeAndId(BidsToPlot, plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
            }
        }

        private static void OutputConsoleBidsByPlantTypeAndId(List<Bid_Segment> segments, string plantTypeAndId, string title)
        {
            OutputConsoleBids(title + "_" + plantTypeAndId, segments);
        }


        private static void OutputConsoleBidsByPlantTypeAndId((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsPlantTypeAndId = GetBidsForPlantTypeAndId(groupedBid);
            if (BidsPlantTypeAndId.Count() > 0)
            {
                var BidsToOutputConsole = GetBidSegmentsInDuration(groupedBid.Action, BidsPlantTypeAndId);
                if (market is ControlReserveMarket)
                {
                    var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsToOutputConsole, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                    foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                    {
                        if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                        {
                            OutputConsoleBidsByPlantTypeAndId(controlReserveTypeElement.Item2.Where(seg => seg.IsCapacity).ToList(), plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Capacity Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                            OutputConsoleBidsByPlantTypeAndId(controlReserveTypeElement.Item2.Where(seg => seg.IsEnergy).ToList(), plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " Energy Bids, Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                        }
                        else
                            OutputConsoleBidsByPlantTypeAndId(controlReserveTypeElement.Item2, plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + ", Control Reserve Type: " + controlReserveTypeElement.ControlReserveType + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
                    }
                }
                else
                    OutputConsoleBidsByPlantTypeAndId(BidsToOutputConsole, plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
            }
        }

        private static void OutputCSVBidsByPlantTypeAndId(List<Bid_Segment> segments, string plantTypeAndId, string title)
        {
            OutputCSVBids(title + "_" + plantTypeAndId, segments.Select(item => (plantTypeAndId, item)).ToList());
        }
        private static void OutputCSVBidsByPlantTypeAndId((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid, Market market)
        {
            var BidsPlantTypeAndId = GetBidsForPlantTypeAndId(groupedBid);
            if (BidsPlantTypeAndId.Count() > 0)
            {
                var BidsToOutputConsole = GetBidSegmentsInDuration(groupedBid.Action, BidsPlantTypeAndId);
                if (market is ControlReserveMarket)
                {
                    var BidsDifferedByPlantTypeAndControlReserveType = DifferBidSegmentsByControlReserveType(BidsToOutputConsole, ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids);
                    foreach (var controlReserveTypeElement in BidsDifferedByPlantTypeAndControlReserveType)
                    {
                        if (market is ControlReserve_Capacity && !((PlotAndOutput_Element_Bid)groupedBid.Action.Element).OnlyCapacityBids)
                        {
                            OutputCSVBidsByPlantTypeAndId(controlReserveTypeElement.Item2.Where(seg => seg.IsCapacity).ToList(), plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Capacity_Bids_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End));
                            OutputCSVBidsByPlantTypeAndId(controlReserveTypeElement.Item2.Where(seg => seg.IsEnergy).ToList(), plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Energy_Bids_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End));
                        }
                        else
                            OutputCSVBidsByPlantTypeAndId(controlReserveTypeElement.Item2, plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_" + controlReserveTypeElement.ControlReserveType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End));
                    }
                }
                else
                    OutputCSVBidsByPlantTypeAndId(BidsToOutputConsole, plantTypeAndId: ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType + "_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Id, title: "Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End));
            }
        }

        private static List<Bid> GetBidsForPlantTypeAndId((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid)
        {
            return groupedBid.Bids
                .Where(item =>
                item.Assets.First().GetType().Name == ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type
                && (((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Type == "Plant_Thermal" ? ((item.Assets.First() as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).SubType) : true)
                && item.Assets.First().Id == ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Plant_Id
                ).ToList();
        }

        private static List<Bid_Segment> GetBidSegmentsInDuration(PlotAndOutput_Variables_Dto action, List<Bid> bids)
        {
            return bids
                .SelectMany(bid => bid.Segments)
                .Where(seg =>
                seg.TimeStamp >= ((PlotAndOutput_Times_Durations)action.Times).Start
                && seg.TimeStamp < ((PlotAndOutput_Times_Durations)action.Times).End
                && (seg.ShortCode.IsBlock == ((PlotAndOutput_Element_Bid)action.Element).WithBlock || seg.ShortCode.IsBlock != ((PlotAndOutput_Element_Bid)action.Element).WithStep))
                .ToList();
        }

        private static void PlotAllBids((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid)
        {
            var BidsDuration = GetBidSegmentsInDuration(groupedBid.Action, groupedBid.Bids);
            Plotter_Extensions.PlotScatter(new List<List<(float, float)>>() { BidsDuration.Select(item => (item.VolumeMW, item.Price)).ToList() }, new List<string>() { "Bid_Segments" }, "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: "Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End);
        }

        private static void OutputConsoleAllBids((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid)
        {
            var BidsDuration = GetBidSegmentsInDuration(groupedBid.Action, groupedBid.Bids);
            OutputConsoleBids("Bids of " + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + " " + "Duration: " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start + " until " + ((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End + "_Bid_Segments", BidsDuration);
        }

        private static void OutputCSVAllBids((PlotAndOutput_Variables_Dto Action, List<Bid> Bids) groupedBid)
        {
            var BidsDuration = GetBidSegmentsInDuration(groupedBid.Action, groupedBid.Bids);
            OutputCSVBids("Bids_" + ((PlotAndOutput_Element_Bid)groupedBid.Action.Element).Market + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)groupedBid.Action.Times).End) + "_Bid_Segments", BidsDuration.Select(item => ("", item)).ToList());
        }

        private static List<PlotAndOutput_Variables_Dto> GetPlotAndOutputActionsFromTypes(string ElementType, string TimesType, List<PlotAndOutput_Variables_Dto> variables)
            => variables.Where(item => item.Element.GetType().Name == ElementType).Where(item => item.Times.GetType().Name == TimesType).ToList();

        private static List<(PlotAndOutput_Variables_Dto Action, List<Bid> Bids)> GroupBidsByAction(List<PlotAndOutput_Variables_Dto> actions, List<Bid> bids)
        {
            return bids.Select(bid => GetActionForBid(actions, bid).Count() != 0 ? GetActionForBid(actions, bid).Select(item => new { bid, action = item }).Select(item => (item.action, item.bid)).ToList() : new List<(PlotAndOutput_Variables_Dto action, Bid bid)>()).ToList()
                .SelectMany(bid => bid).ToList()
                    .GroupBy(bid =>
                        bid.action,
                        (action, bidsAction) => (action, action == null ? bids.ToList() : bidsAction.Select(item => item.bid).Where(item => item.Segments.Any(seg => seg.ShortCode.IsBlock == ((PlotAndOutput_Element_Bid)action.Element).WithBlock || seg.ShortCode.IsBlock != ((PlotAndOutput_Element_Bid)action.Element).WithStep))
                        .ToList()))
                    .ToList();
        }

        private static List<PlotAndOutput_Variables_Dto> GetActionForBid(List<PlotAndOutput_Variables_Dto> actions, Bid bid)
        {
            if (GetActionsForBid(actions, bid).Count() == 0)
                return new List<PlotAndOutput_Variables_Dto>();
            else
                return GetActionsForBid(actions, bid);
        }

        private static List<PlotAndOutput_Variables_Dto> GetActionsForBid(List<PlotAndOutput_Variables_Dto> actions, Bid bid)
        {
            return actions.Where(time => bid.Segments.Any(seg =>
                seg.TimeStamp >= ((PlotAndOutput_Times_Durations)time.Times).Start
                && seg.TimeStamp < ((PlotAndOutput_Times_Durations)time.Times).End)
                ).ToList();
        }

        private static List<(string, List<Bid_Segment>)> FillUpUntilAllHaveSameNumberOfElements(List<(string, List<Bid_Segment>)> segments)
        {
            int MinCount = segments.Select(item => item.Item2.Count()).Min();
            int MaxCount = segments.Select(item => item.Item2.Count()).Max();
            for (int i = MinCount; i <= MaxCount; i++)
                segments.Where(item => item.Item2.Count() == i).ToList().ForEach(item => item.Item2.Add(item.Item2[i - 1]));
            return segments;
        }

        //private static List<(string, string, List<Bid_Segment>)> FillUpUntilAllHaveSameNumberOfElements(List<(string, string, List<Bid_Segment>)> segments)
        //{
        //    int MinCount = segments.Select(item => item.Item2.Count()).Min();
        //    int MaxCount = segments.Select(item => item.Item2.Count()).Max();
        //    for (int i = MinCount; i <= MaxCount; i++)
        //        segments.Where(item => item.Item3.Count() == i).ToList().ForEach(item => item.Item3.Add(item.Item3[i - 1]));
        //    return segments;
        //}

        public static void OutputCSVBids(string title, List<(string Type, Bid_Segment Segment)> segments)
        {
            var ListExport = segments.Select(seg => (seg.Segment.TimeStamp, new List<string>() { seg.Segment.ShortCode.ToString(), seg.Type, seg.Segment.Price.ToString(), seg.Segment.VolumeMW.ToString() })).ToList();
            var NamesExport = new List<string>() { "Time", "ShortCode", "Type", "Price in EUR/MWh", "Volume in MW" };
            WriteToCSV(ListExport, NamesExport, title);
        }

        public static void OutputConsoleBids(string title, List<Bid_Segment> segments)
        {
            Console.WriteLine(title);
            foreach (var segment in segments)
                Console.WriteLine("Time: " + segment.TimeStamp + " ShortCode: " + segment.ShortCode + " Price in EUR / MWh: " + segment.Price + " Volume in MW: " + segment.VolumeMW);
            Console.ReadLine();
        }
        #endregion

        public static void Plot_MeritOrder(SimEvent simEvent, DateTime timeStamp, ControlReserveShortCode.ShortCode controlReserveType = default, params List<(float Volume, float Price, float aggVol)>[] meritOrders) //Erst Sell dann Purchase
        {
            var variables = PlotAndOutput_Variables_Information.GetPlot();
            var plotTimesH = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_MeritOrder").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            if (plotTimesH.Count() > 0)
            {
                var plotTimesInDuration = plotTimesH.Where(item => timeStamp >= ((PlotAndOutput_Times_Durations)item.Times).Start && timeStamp < ((PlotAndOutput_Times_Durations)item.Times).End).ToList();
                foreach (var plotTime in plotTimesInDuration)
                {
                    if (((PlotAndOutput_Element_MeritOrder)plotTime.Element).SimEvent == simEvent.GetType().Name)
                    {
                        var meritOrdersPlotH = meritOrders.ToList().Select(item => item.Select(tupel => new List<(float, float)>() { (tupel.aggVol - tupel.Volume, tupel.Price), (tupel.aggVol, tupel.Price) }).SelectMany(tupel => tupel).ToList());
                        var meritOrdersPlot = FillUpUntilAllHaveSameNumberOfElements(meritOrdersPlotH.ToList());
                        if (simEvent is ControlReserve_Capacity || simEvent is ControlReserve_Energy || simEvent is ControlReserve_Energy)
                        {
                            if (plotTime != default && (!((PlotAndOutput_Element_MeritOrder)plotTime.Element).OnlyOneControlReserveType || ((PlotAndOutput_Element_MeritOrder)plotTime.Element).ControlReserveType == controlReserveType.ToString()))
                                Plotter_Extensions.PlotManyXY(meritOrdersPlot, new List<string>() { "Sell", "Purchase" }, "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: "MeritOrder of " + ((PlotAndOutput_Element_MeritOrder)plotTime.Element).SimEvent + " control reserve type: " + controlReserveType + " for " + timeStamp);
                        }
                        else
                        {
                            if (plotTime != default)
                                Plotter_Extensions.PlotManyXY(meritOrdersPlot, new List<string>() { "Sell", "Purchase" }, "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: "MeritOrder of " + ((PlotAndOutput_Element_MeritOrder)plotTime.Element).SimEvent + " for " + timeStamp);
                        }
                    }
                }
            }
        }

        public static void Plot_CoupledMeritOrder(DateTime auctionStart, List<(int Hour, List<(MarketAreaEnum marketArea, List<(float Volume, float Price, float AggVol)>, float VolumeStart, float VolumeResult)>)> coupledMeritOrders) //Erst Sell dann Purchase
        {
            var variables = PlotAndOutput_Variables_Information.GetPlot();
            var plotTimesH = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_CoupledMeritOrder").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            if (plotTimesH.Count() > 0)
            {
                var meritOrdersToPlot = coupledMeritOrders
                    .GroupBy(meritOrder =>
                        plotTimesH.Where(time => auctionStart.AddHours(meritOrder.Hour) >= ((PlotAndOutput_Times_Durations)time.Times).Start && auctionStart.AddHours(meritOrder.Hour) < ((PlotAndOutput_Times_Durations)time.Times).End && meritOrder.Item2.Any(mo => ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketAreaSpecified ? ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketArea.ToString() == mo.ToString() : true)).Count() == 0 ? null : plotTimesH.Where(time => auctionStart.AddHours(meritOrder.Hour) >= ((PlotAndOutput_Times_Durations)time.Times).Start && auctionStart.AddHours(meritOrder.Hour) < ((PlotAndOutput_Times_Durations)time.Times).End && meritOrder.Item2.Any(mo => ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketAreaSpecified ? ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketArea.ToString() == mo.ToString() : true)).First(),
                        (el1, el2) => (el1, el2))
                        .ToList();
                if (meritOrdersToPlot.Count() > 0)
                {
                    foreach (var meritOrders in meritOrdersToPlot)
                    {
                        if (meritOrders.Item2.Count() > 0 && meritOrders.el1 != null)
                        {
                            if (((PlotAndOutput_Element_CoupledMeritOrder)meritOrders.el1.Element).MarketAreaSpecified)
                            {
                                var PlottingListH = meritOrders.Item2.Select(mo => (mo.Hour, mo.Item2.Where(item => item.marketArea == ((PlotAndOutput_Element_CoupledMeritOrder)meritOrders.el1.Element).MarketArea).ToList())).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    foreach (var moPlotTime in PlottingListH)
                                    {
                                        if (moPlotTime.Item2.Count() > 0)
                                        {
                                            var meritOrderPlot = moPlotTime.Item2.First().Item2.ToList().Select(tupel => new List<(float, float)>() { (tupel.AggVol - tupel.Volume, tupel.Price), (tupel.AggVol, tupel.Price) }).SelectMany(tupel => tupel).ToList();
                                            Plotter_Extensions.PlotManyXYWithVerticalLines(new List<List<(float, float)>>() { meritOrderPlot }, new List<string>() { "CoupledMeritOrder" }, "Volume in MW", "Price in EUR / MWh", moPlotTime.Item2.First().VolumeStart, moPlotTime.Item2.First().VolumeResult, title: "CoupledMeritOrder of DayAheadMarket in " + moPlotTime.Item2.First().marketArea.ToString() + " for " + auctionStart.AddHours(moPlotTime.Hour));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var PlottingListH = meritOrders.Item2.ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    foreach (var moPlotTime in PlottingListH)
                                    {
                                        if (moPlotTime.Item2.Count() > 0)
                                        {
                                            foreach (var moPlotMA in moPlotTime.Item2)
                                            {
                                                var meritOrderPlot = moPlotMA.Item2.ToList().Select(tupel => new List<(float, float)>() { (tupel.AggVol - tupel.Volume, tupel.Price), (tupel.AggVol, tupel.Price) }).SelectMany(tupel => tupel).ToList();
                                                Plotter_Extensions.PlotManyXYWithVerticalLines(new List<List<(float, float)>>() { meritOrderPlot }, new List<string>() { "CoupledMeritOrder" }, "Volume in MW", "Price in EUR / MWh", moPlotMA.VolumeStart, moPlotMA.VolumeResult, title: "CoupledMeritOrder of DayAheadMarket in " + moPlotMA.marketArea.ToString() + " for " + auctionStart.AddHours(moPlotTime.Hour));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static List<List<(float, float)>> FillUpUntilAllHaveSameNumberOfElements(List<List<(float, float)>> meritOrders)
        {
            int MinCount = meritOrders.Select(item => item.Count()).Min();
            int MaxCount = meritOrders.Select(item => item.Count()).Max();
            for (int i = MinCount; i <= MaxCount; i++)
                meritOrders.Where(item => item.Count() == i).ToList().ForEach(item => item.Add(item[i - 1]));
            return meritOrders;
        }

        public static void Plot_Schedules(List<IAsset> assets)
        {
            var variables = PlotAndOutput_Variables_Information.GetPlot();
            var plotTimes = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_Schedule").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations")
                .ToList();
            if (assets.Count() > 0)
            {
                if (plotTimes.Count() > 0)
                {
                    foreach (var plotTime in plotTimes)
                    {
                        if (!((PlotAndOutput_Element_Schedule)plotTime.Element).IsOutputSimTimeSet || ((PlotAndOutput_Element_Schedule)plotTime.Element).OutputSimTime.Ticks == Simulator.Times.SimTime.Ticks)
                        {
                            if (((PlotAndOutput_Element_Schedule)plotTime.Element).DifferByPlantTypeAndSubType)
                            {
                                var PlottingList = assets.GroupBy(asset => asset.GetType().Name + (asset is Block ? "_" + (asset as Block).Commodity.ToString() : ""), (el1, el2) => (el1, el2.GetSumSchedule(((PlotAndOutput_Times_Durations)plotTime.Times).Start, ((PlotAndOutput_Times_Durations)plotTime.Times).End - ((PlotAndOutput_Times_Durations)plotTime.Times).Start))).ToList();
                                foreach (var plotList in PlottingList)
                                {
                                    if (plotList.Item2.Count() > 0)
                                        Plotter.Plot(plotList.Item2.ToList(), ((PlotAndOutput_Element_Schedule)plotTime.Element).WithPrices, "SumSchedule of " + plotList.el1 + " " + "Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End);
                                }
                            }
                            else if (((PlotAndOutput_Element_Schedule)plotTime.Element).OnlyOnePlantType)
                            {
                                var PlottingListH = assets.Where(asset => asset.GetType().Name == ((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Type && (((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Type == "Plant_Thermal" ? ((asset as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Schedule)plotTime.Element).SubType) : true)).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    var PlottingList = PlottingListH.GetSumSchedule(((PlotAndOutput_Times_Durations)plotTime.Times).Start, ((PlotAndOutput_Times_Durations)plotTime.Times).End - ((PlotAndOutput_Times_Durations)plotTime.Times).Start).ToList();
                                    if (PlottingList.Count() > 0)
                                        Plotter.Plot(PlottingList, ((PlotAndOutput_Element_Schedule)plotTime.Element).WithPrices, "SumSchedule of " + ((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Type + "_" + (((PlotAndOutput_Element_Schedule)plotTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)plotTime.Element).SubType : "" + " ") + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End);
                                }
                            }
                            else if (((PlotAndOutput_Element_Schedule)plotTime.Element).OnlyOnePlant)
                            {
                                var PlottingListH = assets.Where(asset => asset.GetType().Name == ((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Type && (((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Type == "Plant_Thermal" ? (asset as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Schedule)plotTime.Element).SubType : true && ((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Id == asset.Id)).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    var PlottingList = PlottingListH.GetSumSchedule(((PlotAndOutput_Times_Durations)plotTime.Times).Start, ((PlotAndOutput_Times_Durations)plotTime.Times).End - ((PlotAndOutput_Times_Durations)plotTime.Times).Start).ToList();
                                    if (PlottingList.Count() > 0)
                                        Plotter.Plot(PlottingList, ((PlotAndOutput_Element_Schedule)plotTime.Element).WithPrices, "SumSchedule of " + ((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Type + "_" + (((PlotAndOutput_Element_Schedule)plotTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)plotTime.Element).SubType : "") + "_" + ((PlotAndOutput_Element_Schedule)plotTime.Element).Plant_Id + " " + "Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End);
                                }
                            }
                            else
                            {
                                var PlottingList = assets.GetSumSchedule(((PlotAndOutput_Times_Durations)plotTime.Times).Start, ((PlotAndOutput_Times_Durations)plotTime.Times).End - ((PlotAndOutput_Times_Durations)plotTime.Times).Start).ToList();
                                if (PlottingList.Count() > 0)
                                    Plotter.Plot(PlottingList, ((PlotAndOutput_Element_Schedule)plotTime.Element).WithPrices, "SumSchedule " + "Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End);
                            }
                        }
                    }
                }
            }
        }

        public static void Plot_MarketResults()
        {
            var variables = PlotAndOutput_Variables_Information.GetPlot();
            var plotTimes = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_MarketResults").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            foreach (var plotTime in plotTimes)
            {
                var MarketResults = MarketResultRepository.GetResultsFromDB(((PlotAndOutput_Times_Durations)plotTime.Times).Start, ((PlotAndOutput_Times_Durations)plotTime.Times).End - ((PlotAndOutput_Times_Durations)plotTime.Times).Start).ToList(); ;
                if (MarketResults.Count() > 0)
                {
                    var MarketResultsWithoutBlock = MarketResults.Where(item => item.IsBlock == false).ToList();
                    if (MarketResultsWithoutBlock.Count() > 0)
                    {
                        if (((PlotAndOutput_Element_MarketResults)plotTime.Element).DifferByMarketType)
                        {
                            var MarketResultsPlot = MarketResultsWithoutBlock.GroupBy(item => GetMarketResultTypeFromShortCode(item.ShortCode), (marketResultType, results) => (marketResultType, results)).ToList();
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).VolumeVersusPrice) Plotter_Extensions.PlotScatter(MarketResultsPlot.Select(element => element.results.Select(result => (result.Volume, result.Price)).ToList()).ToList(), MarketResultsPlot.Select(element => element.marketResultType).ToList(), "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: "MarketResults" + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End);
                            var ResultsPlot = FillUpGapsWithNaN(MarketResultsPlot.Select(element => (element.marketResultType, element.results.Select(result => (result.TimeStamp, result.Volume, result.Price)).ToList().Resample(Resampling.TimeStep.QuarterHour, Resampling.Method.FFill).ToList())).Select(item => item.Item2.Select(result => new { result.TimeStamp, item.marketResultType, Volume = result.Item2, Price = result.Item3 })).SelectMany(item => item).GroupBy(item => item.TimeStamp, (timeStamp, results) => (timeStamp, results.Select(result => (result.marketResultType, result.Volume, result.Price)))).ToList().Where(item => item.timeStamp >= ((PlotAndOutput_Times_Durations)plotTime.Times).Start && item.timeStamp < ((PlotAndOutput_Times_Durations)plotTime.Times).End).ToList());
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusVolume) Plotter.Plot(ResultsPlot.Select(result => (result.timeStamp, result.Item2.Select(item => item.Volume).ToList())).ToList(), title: "MarketResults" + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, ResultsPlot.First().Item2.Select(result => result.marketResultType).ToList());
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusPrice) Plotter.Plot(ResultsPlot.Select(result => (result.timeStamp, result.Item2.Select(item => item.Price).ToList())).ToList(), title: "MarketResults" + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, ResultsPlot.First().Item2.Select(result => result.marketResultType).ToList());
                        }
                        else if (((PlotAndOutput_Element_MarketResults)plotTime.Element).IsMarketTypeSpecified)
                        {
                            var MarketResultsPlot = GetFromMarketResultType(((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType, MarketResultsWithoutBlock);
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).VolumeVersusPrice) Plotter_Extensions.PlotScatter(new List<List<(float, float)>>() { MarketResultsPlot.Select(result => (result.Volume, result.Price)).ToList() }, new List<string>() { "MarketResults" }, "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End);
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusVolume) Plotter.Plot(MarketResultsPlot.Select(result => (result.TimeStamp, result.Volume)).ToList(), title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, "MarketResultsVolume");
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusPrice) Plotter.Plot(MarketResultsPlot.Select(result => (result.TimeStamp, result.Price)).ToList(), title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, "MarketResultsPrice");
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusVolumeAndPrice) Plotter.Plot(MarketResultsPlot.Select(result => (result.TimeStamp, result.Volume, result.Price)).ToList(), title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, "MarketResultsVolume", "MarketResultsPrice");
                        }
                        else
                        {
                            var MarketResultsPlot = MarketResultsWithoutBlock;
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).VolumeVersusPrice) Plotter_Extensions.PlotScatter(new List<List<(float, float)>>() { MarketResultsPlot.Select(result => (result.Volume, result.Price)).ToList() }, new List<string>() { "MarketResults" }, "Volume in MW", "Price in EUR / MWh", plotSize: 3, title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End);
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusVolume) Plotter.Plot(MarketResultsPlot.Select(result => (result.TimeStamp, result.Volume)).ToList(), title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, "MarketResultsVolume");
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusPrice) Plotter.Plot(MarketResultsPlot.Select(result => (result.TimeStamp, result.Price)).ToList(), title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, "MarketResultsPrice");
                            if (((PlotAndOutput_Element_MarketResults)plotTime.Element).TimeVersusVolumeAndPrice) Plotter.Plot(MarketResultsPlot.Select(result => (result.TimeStamp, result.Volume, result.Price)).ToList(), title: ((PlotAndOutput_Element_MarketResults)plotTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)plotTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)plotTime.Times).End, "MarketResultsVolume", "MarketResultsPrice");
                        }
                    }
                }
            }
        }

        private static List<(DateTime timeStamp, IEnumerable<(string marketResultType, float Volume, float Price)> elements)> FillUpGapsWithNaN(List<(DateTime timeStamp, IEnumerable<(string marketResultType, float Volume, float Price)> elements)> resultsBefore)
        {
            var MaxMarketTypes = resultsBefore.Select(item => item.elements.Count()).Max();
            var MarketTypes = resultsBefore.Where(item => item.elements.Count() == MaxMarketTypes).SelectMany(item => item.elements.Select(el => el.marketResultType).ToList()).ToList();
            List<(DateTime timeStamp, IEnumerable<(string marketResultType, float Volume, float Price)> elements)> resultsAfter = new List<(DateTime timeStamp, IEnumerable<(string marketResultType, float Volume, float Price)> elements)>();
            foreach(var element in resultsBefore)
            {
                var Elements = element.elements.ToList().Select(item => (item.marketResultType, item.Volume, (item.Volume == 0 && !item.marketResultType.Contains("ReBAP")) ? 0: item.Price)).ToList();
                foreach(var type in MarketTypes)
                {
                    if (!Elements.Select(item => item.marketResultType).Contains(type))
                        Elements.Add((type, float.NaN, float.NaN));
                }
                resultsAfter.Add((element.timeStamp, Elements));
            }
            return resultsAfter;
        }

        private static string GetMarketResultTypeFromShortCode(ShortCode code)
        {
            if (code.IsDayAhead)
                return "DayAheadResult";
            else if (code.IsFutures)
                return "FuturesResult";
            else if (code.IsIntraDay)
                return "IntradayResult";
            else if (code.IsControlReserve)
                return "ControlReserveResult_" + string.Join("", code.ToString().Take(6)) + "_" + code.ToString().Last().ToString();
            else if (code.Is_reBAP)
                return "ReBAP";
            else
                return "Not defined";
        }

        private static List<MarketResult> GetFromMarketResultType(string marketResultType, List<MarketResult> results)
            => results.Where(result => GetMarketResultTypeFromShortCode(result.ShortCode) == marketResultType).ToList();

        public static void OutputConsole_MeritOrder(SimEvent simEvent, DateTime timeStamp, ControlReserveShortCode.ShortCode controlReserveType = default, params List<(float Volume, float Price, float aggVol)>[] meritOrders) //Erst Sell dann Purchase
        {
            var variables = PlotAndOutput_Variables_Information.GetConsole();
            var outputConsoleTimesH = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_MeritOrder").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            if (outputConsoleTimesH.Count() > 0)
            {
                var outputConsoleTimesInDuration = outputConsoleTimesH.Where(item => timeStamp >= ((PlotAndOutput_Times_Durations)item.Times).Start && timeStamp < ((PlotAndOutput_Times_Durations)item.Times).End).ToList();
                foreach (var outputConsoleTime in outputConsoleTimesInDuration)
                {
                    if (((PlotAndOutput_Element_MeritOrder)outputConsoleTime.Element).SimEvent == simEvent.GetType().Name)
                    {
                        var meritOrdersPlotH = meritOrders.ToList().Select(item => item.Select(tupel => new List<(float, float)>() { (tupel.aggVol - tupel.Volume, tupel.Price), (tupel.aggVol, tupel.Price) }).SelectMany(tupel => tupel).ToList());
                        var meritOrdersPlot = FillUpUntilAllHaveSameNumberOfElements(meritOrdersPlotH.ToList());
                        if (simEvent is ControlReserve_Capacity || simEvent is ControlReserve_Energy || simEvent is ControlReserve_Energy)
                        {
                            if (outputConsoleTime != default && (!((PlotAndOutput_Element_MeritOrder)outputConsoleTime.Element).OnlyOneControlReserveType || ((PlotAndOutput_Element_MeritOrder)outputConsoleTime.Element).ControlReserveType == controlReserveType.ToString()))
                            {
                                OutputConsoleMeritOrder("MeritOrder Sell of " + ((PlotAndOutput_Element_MeritOrder)outputConsoleTime.Element).SimEvent + " control reserve type: " + controlReserveType + " for " + timeStamp, meritOrders[0]);
                                OutputConsoleMeritOrder("MeritOrder Purchase of " + ((PlotAndOutput_Element_MeritOrder)outputConsoleTime.Element).SimEvent + " control reserve type: " + controlReserveType + " for " + timeStamp, meritOrders[1]);
                            }
                        }
                        else
                        {
                            if (outputConsoleTime != default)
                            {
                                OutputConsoleMeritOrder("MeritOrder Sell of " + ((PlotAndOutput_Element_MeritOrder)outputConsoleTime.Element).SimEvent + " for " + timeStamp, meritOrders[0]);
                                OutputConsoleMeritOrder("MeritOrder Purchase of " + ((PlotAndOutput_Element_MeritOrder)outputConsoleTime.Element).SimEvent + " for " + timeStamp, meritOrders[1]);
                            }
                        }
                    }
                }
            }
        }

        private static void OutputConsoleMeritOrder(string title, List<(float Volume, float Price, float AggVol)> tupels)
        {
            Console.WriteLine(title);
            foreach (var tupel in tupels)
                Console.WriteLine("Volume in MW: " + tupel.Volume + " Price in EUR / MWh: " + tupel.Price + " Aggregated Volume in MW: " + tupel.AggVol);
            Console.ReadLine();
        }

        public static void OutputConsole_CoupledMeritOrder(DateTime auctionStart, List<(int Hour, List<(MarketAreaEnum marketArea, List<(float Volume, float Price, float AggVol)>, float VolumeStart, float VolumeResult)>)> coupledMeritOrders) //Erst Sell dann Purchase
        {
            var variables = PlotAndOutput_Variables_Information.GetConsole();
            var plotTimesH = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_CoupledMeritOrder").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            if (plotTimesH.Count() > 0)
            {
                var meritOrdersToPlot = coupledMeritOrders
                    .GroupBy(meritOrder =>
                        plotTimesH.Where(time => auctionStart.AddHours(meritOrder.Hour) >= ((PlotAndOutput_Times_Durations)time.Times).Start && auctionStart.AddHours(meritOrder.Hour) < ((PlotAndOutput_Times_Durations)time.Times).End && meritOrder.Item2.Any(mo => ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketAreaSpecified ? ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketArea.ToString() == mo.ToString() : true)).Count() == 0 ? null : plotTimesH.Where(time => auctionStart.AddHours(meritOrder.Hour) >= ((PlotAndOutput_Times_Durations)time.Times).Start && auctionStart.AddHours(meritOrder.Hour) < ((PlotAndOutput_Times_Durations)time.Times).End && meritOrder.Item2.Any(mo => ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketAreaSpecified ? ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketArea.ToString() == mo.ToString() : true)).First(),
                        (el1, el2) => (el1, el2))
                        .ToList();
                if (meritOrdersToPlot.Count() > 0)
                {
                    foreach (var meritOrders in meritOrdersToPlot)
                    {
                        if (meritOrders.Item2.Count() > 0 && meritOrders.el1 != null)
                        {
                            if (((PlotAndOutput_Element_CoupledMeritOrder)meritOrders.el1.Element).MarketAreaSpecified)
                            {
                                var PlottingListH = meritOrders.Item2.Select(mo => (mo.Hour, mo.Item2.Where(item => item.marketArea == ((PlotAndOutput_Element_CoupledMeritOrder)meritOrders.el1.Element).MarketArea).ToList())).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    foreach (var moPlotTime in PlottingListH)
                                    {
                                        if (moPlotTime.Item2.Count() > 0)
                                            OutputConsoleMeritOrder("CoupledMeritOrder of DayAhead in " + moPlotTime.Item2.First().marketArea.ToString() + " for " + auctionStart.AddHours(moPlotTime.Hour) + " Volume_Start: " + moPlotTime.Item2.First().VolumeStart + " Volume_Result: " + moPlotTime.Item2.First().VolumeResult, moPlotTime.Item2.First().Item2.ToList());
                                    }
                                }
                            }
                            else
                            {
                                var PlottingListH = meritOrders.Item2.ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    foreach (var moPlotTime in PlottingListH)
                                    {
                                        if (moPlotTime.Item2.Count() > 0)
                                        {
                                            foreach (var moPlotMA in moPlotTime.Item2)
                                                OutputConsoleMeritOrder("CoupledMeritOrder of DayAhead in " + moPlotMA.marketArea.ToString() + " for " + auctionStart.AddHours(moPlotTime.Hour) + " Volume_Start: " + moPlotMA.VolumeStart + " Volume_Result: " + moPlotMA.VolumeResult, moPlotMA.Item2.ToList());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void OutputConsole_Schedules(List<IAsset> assets)
        {
            var variables = PlotAndOutput_Variables_Information.GetConsole();
            var outputConsoleTimes = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_Schedule").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations")
                .ToList();
            if (assets.Count() > 0)
            {
                if (outputConsoleTimes.Count() > 0)
                {
                    foreach (var outputConsoleTime in outputConsoleTimes)
                    {
                        if (!((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).IsOutputSimTimeSet || ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).OutputSimTime.Ticks == Simulator.Times.SimTime.Ticks)
                        {
                            if (((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).DifferByPlantTypeAndSubType)
                            {
                                var PlottingList = assets.GroupBy(asset => asset.GetType().Name + (asset.GetType().Name == "Plant_Thermal" ? "_" + (asset as Plant_Thermal).Units.First().Commodity.ToString() : ""), (el1, el2) => (el1, el2.GetSumSchedule(((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start, ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End - ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start))).ToList();
                                foreach (var plotList in PlottingList)
                                {
                                    if (plotList.Item2.Count() > 0)
                                        OutputConsoleSchedule("SumSchedule of " + plotList.el1 + " " + "Duration: " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End, plotList.Item2.ToList(), ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).WithPrices);
                                }
                            }
                            else if (((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).OnlyOnePlantType)
                            {
                                var PlottingListH = assets.Where(asset => asset.GetType().Name == ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).Plant_Type && (((PlotAndOutput_Element_Bid)outputConsoleTime.Element).Plant_Type == "Plant_Thermal" ? (asset as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).SubType : true)).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    var PlottingList = PlottingListH.GetSumSchedule(((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start, ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End - ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start).ToList();
                                    if (PlottingList.Count() > 0)
                                        OutputConsoleSchedule("SumSchedule of " + ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).SubType : "" + " " + "Duration: " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End, PlottingList, ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).WithPrices);
                                }
                            }
                            else if (((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).OnlyOnePlant)
                            {
                                var PlottingListH = assets.Where(asset => asset.GetType().Name == ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).Plant_Type && (((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).Plant_Type == "Plant_Thermal" ? ((asset as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).SubType) : true && ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).Plant_Id == asset.Id)).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    var PlottingList = PlottingListH.GetSumSchedule(((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start, ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End - ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start).ToList();
                                    if (PlottingList.Count() > 0)
                                        OutputConsoleSchedule("SumSchedule of " + ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).Plant_Type + "_" + ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).SubType : "" + "_" + ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).Plant_Id + " " + "Duration: " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End, PlottingList, ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).WithPrices);
                                }
                            }
                            else
                            {
                                var PlottingList = assets.GetSumSchedule(((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start, ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End - ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start).ToList();
                                if (PlottingList.Count() > 0)
                                    OutputConsoleSchedule("SumSchedule " + "Duration: " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End, PlottingList, ((PlotAndOutput_Element_Schedule)outputConsoleTime.Element).WithPrices);
                            }
                        }
                    }
                }
            }
        }

        public static void OutputConsoleSchedule(string title, List<ScheduleItem> schedule, bool withPrices)
        {
            Console.WriteLine(title);
            foreach (var element in schedule)
                Console.WriteLine("Time: " + element.TimeStamp + "Power in MW: " + element.Power + " PowerProcured in MW: " + element.PowerProcured + " Costs in Tsd. EUR: " + element.Costs + " Revenues in Tsd. EUR: " + element.Revenues);
            Console.ReadLine();
        }

        public static void OutputConsole_MarketResults()
        {
            var variables = PlotAndOutput_Variables_Information.GetConsole();
            var outputConsoleTimes = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_MarketResults").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            foreach (var outputConsoleTime in outputConsoleTimes)
            {
                var MarketResults = MarketResultRepository.GetResultsFromDB(((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start, ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End - ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start).ToList(); ;
                if (MarketResults.Count() > 0)
                {
                    var MarketResultsWithoutBlock = MarketResults.Where(item => item.IsBlock == false).ToList();
                    if (MarketResultsWithoutBlock.Count() > 0)
                    {
                        if (((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).DifferByMarketType)
                        {
                            var MarketResultsPlot = MarketResultsWithoutBlock.GroupBy(item => GetMarketResultTypeFromShortCode(item.ShortCode), (marketResultType, results) => (marketResultType, results)).ToList();
                            foreach (var Result in MarketResultsPlot)
                                OutputConsoleMarketResult(title: Result.marketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End, Result.results.ToList(), ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).VolumeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusVolume, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusVolumeAndPrice);
                        }
                        else if (((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).IsMarketTypeSpecified)
                        {
                            var MarketResultsPlot = GetFromMarketResultType(((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).MarketResultType, MarketResultsWithoutBlock);
                            OutputConsoleMarketResult(title: ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).MarketResultType + " Duration: " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End, MarketResultsPlot.ToList(), ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).VolumeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusVolume, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusVolumeAndPrice);
                        }
                        else
                        {
                            var MarketResultsPlot = MarketResultsWithoutBlock;
                            OutputConsoleMarketResult(title: "MarketResults" + " Duration: " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).Start + " until " + ((PlotAndOutput_Times_Durations)outputConsoleTime.Times).End, MarketResultsPlot.ToList(), ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).VolumeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusVolume, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputConsoleTime.Element).TimeVersusVolumeAndPrice);
                        }
                    }
                }
            }
        }

        private static void OutputConsoleMarketResult(string title, List<MarketResult> marketResults, bool volumeVersusPrice, bool timeVersusVolume, bool timeVersusPrice, bool timeVersusVolumeAndPrice)
        {
            if (volumeVersusPrice)
            {
                Console.WriteLine(title + " VolumeVersusPrice");
                foreach (var result in marketResults)
                    Console.WriteLine("ShortCode: " + result.ShortCode + " Volume in MW: " + result.Volume + " Price in EUR / MWh: " + result.Price);
                Console.ReadLine();
            }
            else if (timeVersusVolume)
            {
                Console.WriteLine(title + " TimeVersusVolume");
                foreach (var result in marketResults)
                    Console.WriteLine("ShortCode: " + result.ShortCode + " Time: " + result.TimeStamp + "Volume in MW: " + result.Volume);
                Console.ReadLine();
            }
            else if (timeVersusPrice)
            {
                Console.WriteLine(title + " TimeVersusPrice");
                foreach (var result in marketResults)
                    Console.WriteLine("ShortCode: " + result.ShortCode + " Time: " + result.TimeStamp + " Price in EUR / MWh: " + result.Price);
                Console.ReadLine();
            }
            else if (timeVersusVolumeAndPrice)
            {
                Console.WriteLine(title + " TimeVersusVolumeAndPrice");
                foreach (var result in marketResults)
                    Console.WriteLine("ShortCode: " + result.ShortCode + " Time: " + result.TimeStamp + "Volume in MW: " + result.Volume + " Price in EUR / MWh: " + result.Price);
                Console.ReadLine();
            }
        }

        public static string ConvertDateToString(DateTime date)
            => Convert.ToString(date.Year) + Convert.ToString(date.Month) + Convert.ToString(date.Day) + "_" + Convert.ToString(date.Hour) + "_" + Convert.ToString(date.Minute) + "_" + Convert.ToString(date.Second);



        private static void WriteToCSV(List<(DateTime, List<string>)> collection, List<string> names, string title)
        {
            var filePath = $@"{Config.ProjectPath}\winMoz\Data\Files\CSV\" + title + ".csv";

            if (File.Exists(filePath))
                File.Delete(filePath);
            File.Create(filePath).Close();

            List<string> exportStrings = new List<string>();
            exportStrings.Add(TransformToStringRow(names));

            exportStrings.AddRange(TransformToStringRows(collection));

            File.AppendAllLines(filePath, exportStrings);
        }

        public static List<string> TransformToStringRows(List<(DateTime, List<string>)> exportCollection)
            => exportCollection.Select(el => Convert.ToString(el.Item1, CultureInfo.GetCultureInfo("de-DE")) + ";" + string.Join(";", el.Item2.Select(it => Convert.ToString(it, CultureInfo.GetCultureInfo("de-DE")))) + ";").ToList();

        public static string TransformToStringRow(List<string> names)
            => string.Join(";", names.Where(name => name != null).Select(name => string.Join(";", name)).ToList()) + ";";


        public static void OutputCSV_MeritOrder(SimEvent simEvent, DateTime timeStamp, ControlReserveShortCode.ShortCode controlReserveType = default, params List<(float Volume, float Price, float aggVol)>[] meritOrders) //Erst Sell dann Purchase
        {
            var variables = PlotAndOutput_Variables_Information.GetCSV();
            var outputCSVTimesH = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_MeritOrder").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            if (outputCSVTimesH.Count() > 0)
            {
                var outputCSVTimesInDuration = outputCSVTimesH.Where(item => timeStamp >= ((PlotAndOutput_Times_Durations)item.Times).Start && timeStamp < ((PlotAndOutput_Times_Durations)item.Times).End).ToList();
                foreach (var outputCSVTime in outputCSVTimesInDuration)
                {
                    if (((PlotAndOutput_Element_MeritOrder)outputCSVTime.Element).SimEvent == simEvent.GetType().Name)
                    {
                        var meritOrdersPlotH = meritOrders.ToList().Select(item => item.Select(tupel => new List<(float, float)>() { (tupel.aggVol - tupel.Volume, tupel.Price), (tupel.aggVol, tupel.Price) }).SelectMany(tupel => tupel).ToList());
                        var meritOrdersPlot = FillUpUntilAllHaveSameNumberOfElements(meritOrdersPlotH.ToList());
                        if (simEvent is ControlReserve_Capacity || simEvent is ControlReserve_Energy || simEvent is ControlReserve_Energy)
                        {
                            if (outputCSVTime != default && (!((PlotAndOutput_Element_MeritOrder)outputCSVTime.Element).OnlyOneControlReserveType || ((PlotAndOutput_Element_MeritOrder)outputCSVTime.Element).ControlReserveType == controlReserveType.ToString()))
                            {
                                var ListExportSell = meritOrders[0].Select(tupel => new List<string>() { tupel.Volume.ToString(), tupel.Price.ToString(), tupel.aggVol.ToString() }).ToList();
                                var NamesExport = new List<string>() { "Volume in MW", "Price in EUR/MWh", "Aggregated Volume in MW" };
                                WriteToCSV(ListExportSell, NamesExport, "MeritOrder_Sell_" + ((PlotAndOutput_Element_MeritOrder)outputCSVTime.Element).SimEvent + "_" + controlReserveType + "_" + ConvertDateToString(timeStamp));
                                var ListExportPurchase = meritOrders[1].Select(tupel => new List<string>() { tupel.Volume.ToString(), tupel.Price.ToString(), tupel.aggVol.ToString() }).ToList();
                                WriteToCSV(ListExportPurchase, NamesExport, "MeritOrder_Purchase_" + ((PlotAndOutput_Element_MeritOrder)outputCSVTime.Element).SimEvent + "_" + controlReserveType + "_" + ConvertDateToString(timeStamp));
                            }
                        }
                        else
                        {
                            if (outputCSVTime != default)
                            {
                                var ListExportSell = meritOrders[0].Select(tupel => new List<string>() { tupel.Volume.ToString(), tupel.Price.ToString(), tupel.aggVol.ToString() }).ToList();
                                var NamesExport = new List<string>() { "Volume in MW", "Price in EUR/MWh", "Aggregated Volume in MW" };
                                WriteToCSV(ListExportSell, NamesExport, "MeritOrder_Sell_" + ((PlotAndOutput_Element_MeritOrder)outputCSVTime.Element).SimEvent + "_" + ConvertDateToString(timeStamp));
                                var ListExportPurchase = meritOrders[1].Select(tupel => new List<string>() { tupel.Volume.ToString(), tupel.Price.ToString(), tupel.aggVol.ToString() }).ToList();
                                WriteToCSV(ListExportPurchase, NamesExport, "MeritOrder_Purchase_" + ((PlotAndOutput_Element_MeritOrder)outputCSVTime.Element).SimEvent + "_" + ConvertDateToString(timeStamp));
                            }
                        }
                    }
                }
            }
        }

        public static void OutputCSV_CoupledMeritOrder(DateTime auctionStart, List<(int Hour, List<(MarketAreaEnum marketArea, List<(float Volume, float Price, float AggVol)>, float VolumeStart, float VolumeResult)>)> coupledMeritOrders) //Erst Sell dann Purchase
        {
            var variables = PlotAndOutput_Variables_Information.GetCSV();
            var plotTimesH = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_CoupledMeritOrder").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
            if (plotTimesH.Count() > 0)
            {
                var meritOrdersToPlot = coupledMeritOrders
                    .GroupBy(meritOrder =>
                        plotTimesH.Where(time => auctionStart.AddHours(meritOrder.Hour) >= ((PlotAndOutput_Times_Durations)time.Times).Start && auctionStart.AddHours(meritOrder.Hour) < ((PlotAndOutput_Times_Durations)time.Times).End && meritOrder.Item2.Any(mo => ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketAreaSpecified ? ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketArea.ToString() == mo.ToString() : true)).Count() == 0 ? null : plotTimesH.Where(time => auctionStart.AddHours(meritOrder.Hour) >= ((PlotAndOutput_Times_Durations)time.Times).Start && auctionStart.AddHours(meritOrder.Hour) < ((PlotAndOutput_Times_Durations)time.Times).End && meritOrder.Item2.Any(mo => ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketAreaSpecified ? ((PlotAndOutput_Element_CoupledMeritOrder)time.Element).MarketArea.ToString() == mo.ToString() : true)).First(),
                        (el1, el2) => (el1, el2))
                        .ToList();
                if (meritOrdersToPlot.Count() > 0)
                {
                    foreach (var meritOrders in meritOrdersToPlot)
                    {
                        if (meritOrders.Item2.Count() > 0 && meritOrders.el1 != null)
                        {
                            if (((PlotAndOutput_Element_CoupledMeritOrder)meritOrders.el1.Element).MarketAreaSpecified)
                            {
                                var PlottingListH = meritOrders.Item2.Select(mo => (mo.Hour, mo.Item2.Where(item => item.marketArea == ((PlotAndOutput_Element_CoupledMeritOrder)meritOrders.el1.Element).MarketArea).ToList())).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    foreach (var moPlotTime in PlottingListH)
                                    {
                                        if (moPlotTime.Item2.Count() > 0)
                                        {
                                            var ListExport = moPlotTime.Item2.First().Item2.Select(tupel => new List<string>() { tupel.Volume.ToString(), tupel.Price.ToString(), tupel.AggVol.ToString() }).ToList();
                                            var NamesExport = new List<string>() { "Volume in MW", "Price in EUR/MWh", "Aggregated Volume in MW", "Volume_Start: " + moPlotTime.Item2.First().VolumeStart.ToString(), "Volume_Result: " + moPlotTime.Item2.First().VolumeResult.ToString() };
                                            WriteToCSV(ListExport, NamesExport, "CoupledMeritOrder_DayAhead_" + moPlotTime.Item2.First().marketArea + "_" + ConvertDateToString(auctionStart.AddHours(moPlotTime.Hour)));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var PlottingListH = meritOrders.Item2.ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    foreach (var moPlotTime in PlottingListH)
                                    {
                                        if (moPlotTime.Item2.Count() > 0)
                                        {
                                            foreach (var moPlotMA in moPlotTime.Item2)
                                            {
                                                var ListExport = moPlotMA.Item2.Select(tupel => new List<string>() { tupel.Volume.ToString(), tupel.Price.ToString(), tupel.AggVol.ToString() }).ToList();
                                                var NamesExport = new List<string>() { "Volume in MW", "Price in EUR/MWh", "Aggregated Volume in MW", "Volume_Start: " + moPlotTime.Item2.First().VolumeStart.ToString(), "Volume_Result: " + moPlotTime.Item2.First().VolumeResult.ToString() };
                                                WriteToCSV(ListExport, NamesExport, "CoupledMeritOrder_DayAhead_" + moPlotMA.marketArea + "_" + ConvertDateToString(auctionStart.AddHours(moPlotTime.Hour)));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void WriteToCSV(List<List<string>> collection, List<string> names, string title)
        {
            var filePath = $@"{Config.ProjectPath}\winMoz\Data\Files\CSV\" + title + ".csv";

            if (File.Exists(filePath))
                File.Delete(filePath);
            File.Create(filePath).Close();

            List<string> exportStrings = new List<string>();
            exportStrings.Add(TransformToStringRow(names));

            exportStrings.AddRange(TransformToStringRows(collection));

            File.AppendAllLines(filePath, exportStrings);
        }

        public static List<string> TransformToStringRows(List<List<string>> exportCollection)
            => exportCollection.Select(el => string.Join(";", el.Select(it => Convert.ToString(it, CultureInfo.GetCultureInfo("de-DE")))) + ";").ToList();


        public static void OutputCSV_Schedules(List<IAsset> assets)
        {
            var variables = PlotAndOutput_Variables_Information.GetCSV();
            var outputCSVTimes = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_Schedule").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations")
                .ToList();
            if (assets.Count() > 0)
            {
                if (outputCSVTimes.Count() > 0)
                {
                    foreach (var outputCSVTime in outputCSVTimes)
                    {
                        if (!((PlotAndOutput_Element_Schedule)outputCSVTime.Element).IsOutputSimTimeSet || ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).OutputSimTime.Ticks == Simulator.Times.SimTime.Ticks)
                        {
                            if (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).DifferByPlantTypeAndSubType)
                            {
                                var PlottingList = assets.GroupBy(asset => asset.GetType().Name + (asset.GetType().Name == "Plant_Thermal" ? "_" + ((asset as Plant_Thermal).Units.First().Commodity.ToString()) : ""), (el1, el2) => (el1, el2.GetSumSchedule(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start, ((PlotAndOutput_Times_Durations)outputCSVTime.Times).End - ((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start))).ToList();
                                foreach (var plotList in PlottingList)
                                {
                                    if (plotList.Item2.Count() > 0)
                                    {
                                        if (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).WithPrices)
                                        {
                                            var ListExport = plotList.Item2.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                            var NamesExport = new List<string>() { "Time", "Power in MW", "Price in EUR / MWh", "PowerProcured in MW", "PowerReserved in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                            WriteToCSV(ListExport, NamesExport, "SumSchedule_" + plotList.el1 + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                        }
                                        else
                                        {
                                            var ListExport = plotList.Item2.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                            var NamesExport = new List<string>() { "Time", "Power in MW", "PowerProcured in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                            WriteToCSV(ListExport, NamesExport, "SumSchedule_" + plotList.el1 + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                        }
                                    }
                                }
                            }
                            else if (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).OnlyOnePlantType)
                            {
                                var PlottingListH = assets.Where(asset => asset.GetType().Name == ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Type && (((PlotAndOutput_Element_Bid)outputCSVTime.Element).Plant_Type == "Plant_Thermal" ? ((asset as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType) : true)).ToList();
                                if (PlottingListH.Count() > 0)
                                {

                                    var PlottingList = PlottingListH.GetSumSchedule(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start, ((PlotAndOutput_Times_Durations)outputCSVTime.Times).End - ((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start).ToList();
                                    if (PlottingList.Count() > 0)
                                    {
                                        if (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).WithPrices)
                                        {
                                            var ListExport = PlottingList.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                            var NamesExport = new List<string>() { "Time", "Power in MW", "PowerProcured in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                            WriteToCSV(ListExport, NamesExport, "SumSchedule_" + ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Type + "_" + (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType : "") + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                        }
                                        else
                                        {
                                            var ListExport = PlottingList.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                            var NamesExport = new List<string>() { "Time", "Power in MW", "PowerProcured in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                            WriteToCSV(ListExport, NamesExport, "SumSchedule_" + ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Type + "_" + (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType : "") + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                        }
                                    }
                                }
                            }
                            else if (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).OnlyOnePlant)
                            {
                                var PlottingListH = assets.Where(asset => asset.GetType().Name == ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Type && (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Type == "Plant_Thermal" ? ((asset as Plant_Thermal).Units.First().Commodity.ToString() == ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType) : true && ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Id == asset.Id)).ToList();
                                if (PlottingListH.Count() > 0)
                                {
                                    var PlottingList = PlottingListH.GetSumSchedule(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start, ((PlotAndOutput_Times_Durations)outputCSVTime.Times).End - ((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start).ToList();
                                    if (PlottingList.Count() > 0)
                                    {
                                        if (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).WithPrices)
                                        {
                                            var ListExport = PlottingList.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                            var NamesExport = new List<string>() { "Time", "Power in MW", "Price in EUR / MWh", "PowerProcured in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                            WriteToCSV(ListExport, NamesExport, "SumSchedule_" + ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Type + "_" + (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType : "") + "_" + ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Id + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                        }
                                        else
                                        {
                                            var ListExport = PlottingList.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                            var NamesExport = new List<string>() { "Time", "Power in MW", "PowerProcured in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                            WriteToCSV(ListExport, NamesExport, "SumSchedule_" + ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Type + "_" + (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType != null ? ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).SubType : "") + "_" + ((PlotAndOutput_Element_Schedule)outputCSVTime.Element).Plant_Id + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var PlottingList = assets.GetSumSchedule(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start, ((PlotAndOutput_Times_Durations)outputCSVTime.Times).End - ((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start).ToList();
                                if (PlottingList.Count() > 0)
                                {
                                    if (((PlotAndOutput_Element_Schedule)outputCSVTime.Element).WithPrices)
                                    {
                                        var ListExport = PlottingList.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                        var NamesExport = new List<string>() { "Time", "Power in MW", "Price in EUR / MWh", "PowerProcured in MW", "PowerReserved in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                        WriteToCSV(ListExport, NamesExport, "SumSchedule_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                    }
                                    else
                                    {
                                        var ListExport = PlottingList.Select(element => (element.TimeStamp, new List<string>() { element.Power.ToString(), element.PowerProcured.ToString(), element.Costs.ToString(), element.Revenues.ToString() })).ToList();
                                        var NamesExport = new List<string>() { "Time", "Power in MW", "PowerProcured in MW", "PowerReserved in MW", " Costs in Tsd. EUR", " Revenues in Tsd. EUR" };
                                        WriteToCSV(ListExport, NamesExport, "SumSchedule_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void OutputCSV_MarketResults(params Schedule[] meritOrders)
        {
                var variables = PlotAndOutput_Variables_Information.GetCSV();
                var outputCSVTimes = variables.Where(item => item.Element.GetType().Name == "PlotAndOutput_Element_MarketResults").Where(item => item.Times.GetType().Name == "PlotAndOutput_Times_Durations").ToList();
                foreach (var outputCSVTime in outputCSVTimes)
                {
                    var MarketResults = MarketResultRepository.GetResultsFromDB(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start, ((PlotAndOutput_Times_Durations)outputCSVTime.Times).End - ((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start).ToList(); ;
                    if (MarketResults.Count() > 0)
                    {
                        var MarketResultsWithoutBlock = MarketResults.Where(item => item.IsBlock == false).ToList();
                        if (MarketResultsWithoutBlock.Count() > 0)
                        {
                            if (((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).DifferByMarketType)
                            {
                                var MarketResultsPlot = MarketResultsWithoutBlock.GroupBy(item => GetMarketResultTypeFromShortCode(item.ShortCode), (marketResultType, results) => (marketResultType, results)).ToList();
                                foreach (var Result in MarketResultsPlot)
                                    OutputCSVMarketResult(title: Result.marketResultType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End), Result.results.ToList(), ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).VolumeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusVolume, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusVolumeAndPrice);
                            }
                            else if (((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).IsMarketTypeSpecified)
                            {
                                var MarketResultsPlot = GetFromMarketResultType(((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).MarketResultType, MarketResultsWithoutBlock);
                                OutputCSVMarketResult(title: ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).MarketResultType + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End), MarketResultsPlot.ToList(), ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).VolumeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusVolume, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusVolumeAndPrice);
                            }
                            else
                            {
                                var MarketResultsPlot = MarketResultsWithoutBlock;
                                OutputCSVMarketResult(title: "MarketResults" + "_Duration_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).Start) + "_until_" + ConvertDateToString(((PlotAndOutput_Times_Durations)outputCSVTime.Times).End), MarketResultsPlot.ToList(), ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).VolumeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusVolume, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusPrice, ((PlotAndOutput_Element_MarketResults)outputCSVTime.Element).TimeVersusVolumeAndPrice);
                            }
                        }
                    }
                }
        }

        private static void OutputCSVMarketResult(string title, List<MarketResult> marketResults, bool volumeVersusPrice, bool timeVersusVolume, bool timeVersusPrice, bool timeVersusVolumeAndPrice)
        {
            if (volumeVersusPrice)
                WriteToCSV(marketResults.Select(result => new List<string>() { result.ShortCode.ToString(), result.Volume.ToString(), result.Price.ToString() }).ToList(),  new List<string>() { "ShortCode", "Volume in MW", "Price in EUR / MWh" }, title + "_VolumeVersusPrice");
            else if (timeVersusVolume)
                WriteToCSV(marketResults.Select(result => new List<string>() { result.ShortCode.ToString(), result.TimeStamp.ToString(), result.Volume.ToString() }).ToList(), new List<string>() { "ShortCode", "Time", "Volume in MW" }, title + "_TimeVersusVolume");
            else if (timeVersusPrice)
                WriteToCSV(marketResults.Select(result => new List<string>() { result.ShortCode.ToString(), result.TimeStamp.ToString(), result.Price.ToString() }).ToList(), new List<string>() { "ShortCode", "Time", "Volume in MW", "Price in EUR / MWh" }, title + "_TimeVersusPrice");
            else if (timeVersusVolumeAndPrice)
                WriteToCSV(marketResults.Select(result => new List<string>() { result.ShortCode.ToString(), result.TimeStamp.ToString(), result.Price.ToString() }).ToList(), new List<string>() { "ShortCode", "Time", "Volume in MW", "Price in EUR / MWh" }, title + "_TimeVersusVolumeAndPrice");
        }
    }
}
