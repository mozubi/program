import os,sys
import pandas as pd, numpy as np, matplotlib.pyplot as plt, seaborn as sns
sns.set(style="whitegrid",palette=sns.color_palette("Paired"))


def on_close(event):
    sys.exit()

def plotOneXAndManyY(df, yLabel, title):
    #drawstyle="steps-post"
    xcols = [col for col in df.columns if 'XData' in col ]
    ycols = [col for col in df.columns if 'YData' in col ]
    xLabel = str(xcols[0].replace("XData", ""))
    ycolsLabel = []
    for i in range(0, len(xcols)):
        ycolsLabel.append(ycols[i].replace("YData", "").replace("Scatter",""))
    df.index = df[xcols[0]]
    ax = df[ycols].plot()
    if len(ycolsLabel) > 1 and firstAsScatter == 1:
        label1 = ycolsLabel[0]
        ycolsLabel.remove(label1)
        ycolsLabel.append(label1)
    ax.legend(ycolsLabel)
    ax.set_xlabel(xLabel)
    ax.set_ylabel(yLabel)
    ax.set_title(title)
    fig = ax.get_figure()
    fig.canvas.mpl_connect('close_event', on_close)
    plt.show(block=False)


def getDf(splitted, header):
    values = [row.split(',') for row in splitted[1:-1]]
    values = [np.array(value).astype(np.float) for value in values]
    return pd.DataFrame(data=values,columns=header)


string = str(sys.stdin.read())
splitted = string.split(';')
header = splitted[0].split(",")
yLabel = str(sys.argv[1])
title = str(sys.argv[2])
df = getDf(splitted, header)
plotOneXAndManyY(df, yLabel, title)