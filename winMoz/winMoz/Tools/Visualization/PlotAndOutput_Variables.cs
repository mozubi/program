﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.DTO;

namespace winMoz.Simulation
{
    public class PlotAndOutput_Variables : IPlotAndOutput_Variables
    {
        public List<PlotAndOutput_Variables_Dto> FeaturesToPlot { get; set; }

        public List<PlotAndOutput_Variables_Dto> FeaturesToPrintConsole { get; set; }

        public List<PlotAndOutput_Variables_Dto> FeaturesToPrintCSV { get; set; }

        public PlotAndOutput_Variables(List<PlotAndOutput_Variables_Dto> featuresToPlot = null, List<PlotAndOutput_Variables_Dto> featuresToPrintConsole = null, List<PlotAndOutput_Variables_Dto> featuresToPrintCSV = null)
        {
            FeaturesToPlot = featuresToPlot;
            FeaturesToPrintConsole = featuresToPrintConsole;
            FeaturesToPrintCSV = featuresToPrintCSV;
        }
    }
}
