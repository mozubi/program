﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using winMoz.Assets.Schedules;
using winMoz.Simulation;
using winMoz.Helper;

namespace winMoz.Tools.Viz
{
    public static class Plotter
    {
        #region Plot-Functions
        private static string WriteString(List<List<float>> series, List<DateTime> times = null, params string[] itemNames)
        {
            int cols = series.Count();
            int rows = series.First().Count();

            string names = string.Join(",", itemNames.Take(cols));
            if (itemNames == default || itemNames.Count() < cols) names += string.Join(",", Enumerable.Range(itemNames.Count(), cols - itemNames.Count()).Select(col => $"Value{col + 1}").ToArray());
            if (times != null) names = "Time," + names;
            names += ";";

            var export = new StringBuilder(names);
            for (int row = 0; row < rows; row++)
                export.Append(GetDate(row, times) + string.Join(",", Enumerable.Range(0, cols).Select(col => Convert.ToString(series[col][row], CultureInfo.InvariantCulture))) + ";");

            return export.ToString();       
        }
        static string GetDate(int row, List<DateTime> times = null) => times != null ? times[row].ToString("MM/dd/yyyy HH:mm") + "," : "";

        #region Schedules
        private static void _plotSchedule(this List<ScheduleItem> schedule, bool plotPrices, string title, params object[] seriesAndNames)
        {
            var names = schedule.First().GetType().GetProperties().Where(prop => prop.PropertyType == typeof(float)).Select(prop => prop.Name);

            var seriesNames = seriesAndNames.Where(el => el.GetType() == typeof(string)).ToList();
            seriesNames.ToList().ForEach(name => names = names.Append((string)name));

            var series = seriesAndNames.Where(el => el.GetType() == typeof(List<float>)).Cast<List<float>>().ToList();

            var times = new List<DateTime>();
            var values = new List<List<float>>();
            for (int i = 0; i < schedule.Count; i++)
            {
                var item = schedule[i];
                times.Add((DateTime)item.GetType().GetProperties().FirstOrDefault(prop => prop.PropertyType == typeof(DateTime)).GetValue(item));
                var itemValues = item.GetType().GetProperties().Where(prop => prop.PropertyType == typeof(float)).Select(prop => (float)prop.GetValue(item)).ToList();

                foreach (var serie in series) itemValues.Add(serie[i]);

                values.Add(itemValues);
            }

            values = values.SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();// transpose

            string args = WriteString(values, times, names.ToArray());
            ExecutePython(plotPrices ? 1 : 0, args, title, "Time", "Power in MW", "Cost and Revenues in tsd. EUR");
        }

        public static void Plot(List<ScheduleItem> schedule, bool plotPrices = false, string title = "") => schedule._plotSchedule(plotPrices, title);
        public static void Plot(List<ScheduleItem> schedule, List<float> a, string nameA = "SeriesA", bool plotPrices = false, string title = "") => schedule._plotSchedule(plotPrices, title, a, nameA);
        public static void Plot(List<ScheduleItem> schedule, List<float> a, List<float> b, string nameA = "SeriesA", string nameB = "SeriesB", bool plotPrices = false, string title = "") => schedule._plotSchedule(plotPrices, title, a, b, nameA, nameB);
        public static void Plot(List<ScheduleItem> schedule, List<float> a, List<float> b, List<float> c, string nameA = "SeriesA", string nameB = "SeriesB", string nameC = "SeriesC", bool plotPrices = false, string title = "") => schedule._plotSchedule(plotPrices, title, a, b, c, nameA, nameB, nameC);
        #endregion

        #region TimeSeries
        private static void _plotTimeSeries(List<object> series, params string[] names)
        {
            var times = series.Select(row => row.GetType().GetFields()[0].GetValue(row)).Cast<DateTime>().ToList();
            var values = Enumerable.Range(1, series.First().GetType().GetFields().Count() - 1).Select(col => series.Select(row => row.GetType().GetFields()[col].GetValue(row)).ToList().ConvertAll(el => (float)el)).ToList();
            var args = WriteString(values, times, names);

            ExecutePython(0, args);
        }

        private static void _plotTimeSeries(List<object> series, string title, params string[] names)
        {
            var times = series.Select(row => row.GetType().GetFields()[0].GetValue(row)).Cast<DateTime>().ToList();
            var values = Enumerable.Range(1, series.First().GetType().GetFields().Count() - 1).Select(col => series.Select(row => row.GetType().GetFields()[col].GetValue(row)).ToList().ConvertAll(el => (float)el)).ToList();
            var args = WriteString(values, times, names);

            ExecutePython(1, args, title);
        }

        private static void _plotTimeSeries(List<(DateTime, List<float>)> series, string title, List<string> names)
        {
            var times = series.Select(row => row.Item1).ToList();
            var values = series.Select(row => row.Item2).ToList().Transpose();
            var args = WriteString(values, times, names.ToArray());

            ExecutePython(1, args, title);
        }

        public static void Plot(this IEnumerable<(DateTime, float)> series, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), names);
        public static void Plot(this IEnumerable<(DateTime, float, float)> series, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float)> series, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float, float)> series, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float, float, float)> series, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float, float, float, float)> series, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), names);
        public static void Plot(this IEnumerable<(DateTime, float)> series, List<(DateTime, float)> seriesB, params string[] names)
            => _plotTimeSeries(series.Join(seriesB, outer => outer.Item1, inner => inner.Item1, (a, b) => (a.Item1, a.Item2, b.Item2)).Cast<object>().ToList(), names);

        public static void Plot(this IEnumerable<(DateTime, float)> series, string title, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), title, names);
        public static void Plot(this IEnumerable<(DateTime, float, float)> series, string title, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), title, names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float)> series, string title, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), title, names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float, float)> series, string title, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), title, names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float, float, float)> series, string title, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), title, names);
        public static void Plot(this IEnumerable<(DateTime, float, float, float, float, float, float)> series, string title, params string[] names) => _plotTimeSeries(series.Cast<object>().ToList(), title, names);

        public static void Plot(this IEnumerable<(DateTime, List<float>)> series, string title, List<string> names) => _plotTimeSeries(series.ToList(), title, names);
        public static void Plot(this IEnumerable<(DateTime, float)> series, List<(DateTime, float)> seriesB, string title, params string[] names)
            => _plotTimeSeries(series.Join(seriesB, outer => outer.Item1, inner => inner.Item1, (a, b) => (a.Item1, a.Item2, b.Item2)).Cast<object>().ToList(), title, names);
        #endregion

        #region Series
        private static void _plotSeries(params object[] seriesAndNames)
        {
            var names = (string[])seriesAndNames.FirstOrDefault(el => el.GetType() == typeof(string[]));
            var series = seriesAndNames.Where(el => el.GetType() == typeof(List<float>)).Cast<List<float>>().ToList();
            var args = WriteString(series, null, names);

            ExecutePython(0, args);
        }

        public static void Plot(this List<float> A, params string[] names) => _plotSeries(A.ToList(), names);
        public static void Plot(this List<float> A, List<float> B, params string[] names) => _plotSeries(A, B, names);
        public static void Plot(this List<float> A, List<float> B, List<float> C, params string[] names) => _plotSeries(A, B, C, names);
        public static void Plot(this List<float> A, List<float> B, List<float> C, List<float> D, params string[] names) => _plotSeries(A, B, C, D, names);
        public static void Plot(this List<float> A, List<float> B, List<float> C, List<float> D, List<float> E, params string[] names) => _plotSeries(A, B, C, D, E, names);

        #endregion

        #endregion

        #region Execution
        private static void ExecutePython(int plotPrices, string args, string title = "x", string titleX = "x", string titleY = "x", string titleYSecondAxis = "x")
        {
            var startInfo = new ProcessStartInfo();
            startInfo.FileName = GetPythonFromPATH();
            startInfo.Arguments = string.Format("-i \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"", $@"{Config.ProjectPath}\winMoz\Tools\Visualization\Plot.py", plotPrices, title, titleX, titleY, titleYSecondAxis);
            startInfo.UseShellExecute = false;// Do not use OS shell
            startInfo.CreateNoWindow = true; // We don't need new window
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;// Any output, generated by application will be redirected back
            startInfo.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
            using (var process = Process.Start(startInfo))
            {
                using (var writer = process.StandardInput)
                    writer.Write(args);

                //using (StreamReader reader = process.StandardOutput)
                //{
                //    string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                //    if (stderr.Length > 0)
                //    {
                //        Console.WriteLine($"ERROR: Python-Error - ERRMSG{stderr}");
                //        //Console.WriteLine($"Info: Python needs to be installed and on PATH. Required Packages: Pandas, Matplotlib");
                //    }

                //    string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                //    Console.WriteLine(result);
                //}
            }
        }

        private static string GetPythonFromPATH()
        {
            var path = Environment.GetEnvironmentVariable("PATH");
            string pythonPath = null;
            foreach (var p in path.Split(new char[] { ';' }))
            {
                var fullPath = Path.Combine(p, "python.exe");
                if (File.Exists(fullPath))
                {
                    pythonPath = fullPath;
                    break;
                }
            }

            if (pythonPath != null) return pythonPath;
            else Config.AddPythonToPATH();

            return GetPythonFromPATH();
        }
        #endregion
    }
}
