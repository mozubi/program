﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Simulation
{
    public class PlotAndOutput_Times : IPlotAndOutput_Times
    {
        public int Id { get; private set; }
        protected PlotAndOutput_Times() { }
    }

    public class PlotAndOutput_Times_Durations : PlotAndOutput_Times
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
        private PlotAndOutput_Times_Durations() : base() { }
        public PlotAndOutput_Times_Durations(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public PlotAndOutput_Times_Durations(DateTime start, TimeSpan duration)
        {
            Start = start;
            End = start.Add(duration);
        }
    }
}
