﻿namespace winMoz.Tools.Prediction
{
    public interface IPredictionScore
    {
        float Score { get; set; }
    }
}
