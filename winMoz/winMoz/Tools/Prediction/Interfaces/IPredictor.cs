﻿using System.Collections.Generic;

namespace winMoz.Tools.Prediction
{
    public interface IPredictor<PredictionInput, PredictionResult>
    {
        void Train(IEnumerable<PredictionInput> trainingData);
        PredictionResult Predict(PredictionInput input);
        IEnumerable<PredictionResult> Predict(IEnumerable<PredictionInput> input);
    }
}
