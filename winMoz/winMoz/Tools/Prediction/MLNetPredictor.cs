﻿using Microsoft.EntityFrameworkCore;
using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Reflection;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Tools.Prediction.KerasPredictor;

namespace winMoz.Tools.Prediction
{
    public abstract class MLNetPredictor<PredictionInput, PredictionResult> : IPredictor<PredictionInput, PredictionResult> where PredictionInput : ForecastInput<PredictionInput>
    {
        protected abstract string MODEL_FILEPATH { get; }

        protected ITransformer mlModel { get; set; }

        // Create MLContext to be shared across the model creation workflow objects 
        // Set a seed for repeatable/deterministic results across multiple trainings.
        protected static MLContext mlContext = new MLContext(seed: 1);
        protected static DbContext dbContext = new PredictionDataContext();

        protected static bool useSimulationData = false;

        protected MLNetPredictor()
        {
            var directory = MODEL_FILEPATH.Substring(0, MODEL_FILEPATH.LastIndexOf('\\'));
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            mlModel = LoadModel();
        }

        public abstract PredictionResult Predict(PredictionInput input);

        public abstract IEnumerable<PredictionResult> Predict(IEnumerable<PredictionInput> input);

        public virtual void Train(IEnumerable<PredictionInput> trainingData = null)
        {
            if (trainingData != null)
                CreateModel(mlContext.Data.LoadFromEnumerable(trainingData));
            else
                CreateModel(mlContext.Data.LoadFromEnumerable(ForecastInput<PredictionInput>.GetTrainingData()));
        }

        protected virtual List<float> MultistepPrediction(IEnumerable<PredictionInput> forecast)
        {
            var dataView = mlContext.Data.LoadFromEnumerable(forecast);

            var predictions = mlModel.Transform(dataView);

            var scores = predictions.GetColumn<float>("Score").ToList();

            return scores.Select(el => (float)el).ToList();
        }


        #region ML.Net - Methods
        protected virtual ITransformer LoadModel()
        {
            // Laden des Modells
            try
            {
                return mlContext.Model.Load(MODEL_FILEPATH, out DataViewSchema inputscheme);
            }
            catch (Exception ex)
            {
                if (ex is FileNotFoundException || ex is DirectoryNotFoundException)
                {
                    CreateModel();
                    return mlContext.Model.Load(MODEL_FILEPATH, out DataViewSchema inputscheme);
                }
            }
            throw new InvalidOperationException();
        }

        protected void CreateModel(IDataView trainingDataView = null)
        {
            if (File.Exists(MODEL_FILEPATH))
                File.Delete(MODEL_FILEPATH);

            // Load Data
            if (trainingDataView == null)
                trainingDataView = mlContext.Data.LoadFromEnumerable(ForecastInput<PredictionInput>.GetTrainingData());

            //trainingDataView = Normalize(trainingDataView);

            // Build training pipeline
            IEstimator<ITransformer> trainingPipeline = BuildTrainingPipeline(mlContext, trainingDataView);

            // Evaluate quality of Model
            Evaluate(mlContext, trainingDataView, trainingPipeline);

            // Train Model
            ITransformer mlModel = TrainModel(mlContext, trainingDataView, trainingPipeline);

            if (false) //if do PFI
            {
                PFI(mlContext, trainingDataView, mlModel);
            }

            // Save model
            SaveModel(mlContext, mlModel, MODEL_FILEPATH, trainingDataView.Schema);
        }

        /// <summary>
        /// Führt eine Permutation Feature Importance Analyse aus. Damit wird identifizert, welche Feature
        /// besonders relevant für den Regressionsbaum sind.
        /// </summary>
        private void PFI(MLContext mlContext, IDataView trainingDataView, ITransformer model)
        {
            //ITransformer model = trainingPipeline.Fit(trainingDataView);
            var transformedData = model.Transform(trainingDataView);

            //get Last transformer
            IEnumerable<ITransformer> chain = model as IEnumerable<ITransformer>;
            ISingleFeaturePredictionTransformer<object> predictionTransformer =
                chain.Last() as ISingleFeaturePredictionTransformer<object>;


            ImmutableArray<RegressionMetricsStatistics> permutationFeatureImportance = mlContext.Regression.PermutationFeatureImportance(predictionTransformer, transformedData, permutationCount: 3, labelColumnName: "Price");
            // Order features by importance
            //string[] featureColumnNames = { "j" };
            var features = GetFeatureNames(trainingDataView, true).ToList();
            var featureImportanceMetrics = permutationFeatureImportance
                .Select((metric, index) => new { index, metric.RSquared })
                .OrderByDescending(myFeatures => Math.Abs(myFeatures.RSquared.Mean));
            Console.WriteLine("Feature\tPFI");
            foreach (var feature in featureImportanceMetrics)
            {
                Console.WriteLine($"{features[feature.index],-20}|\t{feature.RSquared.Mean:F6}");
            }

        }

        protected abstract IEnumerable<string> GetFeatureNames(IDataView dataView, bool forPFI = false);

        protected abstract IEstimator<ITransformer> BuildTrainingPipeline(MLContext mlContext, IDataView dataView);

        public static void PrintRegressionMetrics(RegressionMetrics metrics)
        {
            Console.WriteLine($"*************************************************");
            Console.WriteLine($"*       Metrics for regression model      ");
            Console.WriteLine($"*------------------------------------------------");
            Console.WriteLine($"*       LossFn:        {metrics.LossFunction:0.##}");
            Console.WriteLine($"*       R2 Score:      {metrics.RSquared:0.##}");
            Console.WriteLine($"*       Absolute loss: {metrics.MeanAbsoluteError:#.##}");
            Console.WriteLine($"*       Squared loss:  {metrics.MeanSquaredError:#.##}");
            Console.WriteLine($"*       RMS loss:      {metrics.RootMeanSquaredError:#.##}");
            Console.WriteLine($"*************************************************");
        }

        private static ITransformer TrainModel(MLContext mlContext, IDataView trainingDataView, IEstimator<ITransformer> trainingPipeline)
        {
            Console.WriteLine("=============== Training  model ===============");
            ITransformer model = trainingPipeline.Fit(trainingDataView);

            Console.WriteLine("=============== End of training process ===============");
            return model;
        }

        protected virtual void Evaluate(MLContext mlContext, IDataView trainingDataView, IEstimator<ITransformer> trainingPipeline)
        {
            // Cross-Validate with single dataset (since we don't have two datasets, one for training and for evaluate)
            // in order to evaluate and get the model's accuracy metrics
            Console.WriteLine("=============== Cross-validating to get model's accuracy metrics ===============");
            var crossValidationResults = mlContext.Regression.CrossValidate(trainingDataView, trainingPipeline, numberOfFolds: 5, labelColumnName: trainingDataView.Schema[0].Name);
            PrintRegressionFoldsAverageMetrics(crossValidationResults);
        }


        /// <summary>
        /// Evaluation for Test set, without Crossvalidation
        /// </summary>
        /// <param name=""></param>
        private void Evaluate(MLContext mlContext, ITransformer mlModel, IDataView testData)
        {
            Console.WriteLine("=============== Validate to get model's accuracy metrics on TestData ===============");
            var metrics = mlContext.Regression.Evaluate(mlModel.Transform(testData), labelColumnName: testData.Schema[0].Name);
            PrintRegressionMetrics(metrics);
        }


        private static void SaveModel(MLContext mlContext, ITransformer mlModel, string modelRelativePath, DataViewSchema modelInputscheme)
        {
            // Save/persist the trained model to a .ZIP file
            Console.WriteLine($"=============== Saving the model  ===============");
            mlContext.Model.Save(mlModel, modelInputscheme, GetAbsolutePath(modelRelativePath));
            Console.WriteLine("The model is saved to {0}", GetAbsolutePath(modelRelativePath));
        }

        public static void PrintRegressionFoldsAverageMetrics(IEnumerable<TrainCatalogBase.CrossValidationResult<RegressionMetrics>> crossValidationResults)
        {
            var L1 = crossValidationResults.Select(r => r.Metrics.MeanAbsoluteError);
            var L2 = crossValidationResults.Select(r => r.Metrics.MeanSquaredError);
            var RMS = crossValidationResults.Select(r => r.Metrics.RootMeanSquaredError);
            var lossFunction = crossValidationResults.Select(r => r.Metrics.LossFunction);
            var R2 = crossValidationResults.Select(r => r.Metrics.RSquared);

            Console.WriteLine($"*************************************************************************************************************");
            Console.WriteLine($"*       Metrics for Regression model      ");
            Console.WriteLine($"*------------------------------------------------------------------------------------------------------------");
            Console.WriteLine($"*       Average L1 Loss:       {L1.Average():0.###} ");
            Console.WriteLine($"*       Average L2 Loss:       {L2.Average():0.###}  ");
            Console.WriteLine($"*       Average RMS:           {RMS.Average():0.###}  ");
            Console.WriteLine($"*       Average Loss Function: {lossFunction.Average():0.###}  ");
            Console.WriteLine($"*       Average R-squared:     {R2.Average():0.###}  ");
            Console.WriteLine($"*************************************************************************************************************");
        }
        #endregion

        private static string GetAbsolutePath(string relativePath)
        {
            var _dataRoot = new FileInfo(Assembly.GetExecutingAssembly().Location);
            string assemblyFolderPath = _dataRoot.Directory.FullName;

            string fullPath = Path.Combine(assemblyFolderPath, relativePath);

            return fullPath;
        }
    }
}
