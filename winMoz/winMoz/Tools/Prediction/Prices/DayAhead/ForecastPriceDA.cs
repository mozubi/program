using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Tools.Prediction.KerasPredictor;

namespace winMoz.Tools.Prediction.Prices.DayAhead
{
    public class ForecastPriceDA : ForecastInput<ForecastPriceDA>
    {
        public ForecastPriceDA()
        {

        }

        public ForecastPriceDA(DateTime timeStamp, float lastDAPrice, float priceH23, float pvAnte, float windAnte, float load)
        {
            TimeStamp = timeStamp;
            PriceLastDay = (float)lastDAPrice;
            PriceH23 = priceH23;
            WeekDay = (int)TimeStamp.DayOfWeek;
            Hour = TimeStamp.Hour;
            PVAnte = (float)pvAnte;
            WindAnte = (float)windAnte;
            Load = (float)load;
        }

        public ForecastPriceDA(DateTime timeStamp, float DAPrice, float lastDAPrice, float priceH23, float pvAnte, float windAnte, float load)
        {
            TimeStamp = timeStamp;
            Price = (float)DAPrice;
            PriceLastDay = (float)lastDAPrice;
            PriceH23 = priceH23;
            WeekDay = (int)TimeStamp.DayOfWeek;
            Hour = TimeStamp.Hour;
            PVAnte = (float)pvAnte;
            WindAnte = (float)windAnte;
            Load = (float)load;
        }


        public DateTime TimeStamp { get; set; }

        [ColumnName("Price"), LoadColumn(0)]
        public float Price { get; set; }

        [ColumnName("WeekDay"), LoadColumn(1)]
        public float WeekDay { get; set; }

        [ColumnName("Hour"), LoadColumn(2)]
        public float Hour { get; set; }

        [ColumnName("PVAnte"), LoadColumn(3)]
        public float PVAnte { get; set; }

        [ColumnName("WindAnte"), LoadColumn(4)]
        public float WindAnte { get; set; }

        [ColumnName("Load"), LoadColumn(5)]
        public float Load { get; set; }

        [ColumnName("PriceLastDay"), LoadColumn(6)]
        public float PriceLastDay { get; set; }
        [ColumnName("PriceH23"), LoadColumn(6)]
        public float PriceH23 { get; set; }

        protected override IEnumerable<ForecastPriceDA> trainingData()
        {
            var repo = new PredictionDataRepository();
            //get the hourly dayahead prices
            var DA_Data = repo.DayAheadDataRepo.Get();
            var prices23h = DA_Data
               .Where(el => el.TimeStamp.Hour == 23)
               .Select(el => (el.TimeStamp.Date, el.Price))
               .ToDictionary(el => el.Date, el => el.Price);
            // lag price-data for autocorrelation
            IEnumerable<(DateTime TimeStamp, float laggedPrice, float priceH23)> HistData = DA_Data
                .Skip(24)
                .Select(el => (el.TimeStamp.AddHours(24), el.Price, prices23h[el.TimeStamp.Date]));

            //get the hourly renewables
            var RE_Data = repo.REDataRepo.Get()
            .AsEnumerable() //grouping needs the data on client site
            .GroupBy(el => new { el.TimeStamp.Date, el.TimeStamp.Hour }, el => new { el.PV_Ante, el.WI_Ante })
            .Select(el => new
            {
                TimeStamp = el.Key.Date.AddHours(el.Key.Hour),
                PV_Ante = el.Average(x => x.PV_Ante),
                WI_Ante = el.Average(x => x.WI_Ante)
            });

            // get hourly load
            var load = repo.LoadDataRepo.Get() //grouping needs the data on client site
            .GroupBy(el => new { el.TimeStamp.Date, el.TimeStamp.Hour }, el => new { el.Volume })
            .Select(el => new
            {
                TimeStamp = el.Key.Date.AddHours(el.Key.Hour),
                Volume = el.Average(x => x.Volume),
            });

            repo.Dispose();

            return HistData.Join(RE_Data,
                    hist => hist.TimeStamp,
                    re => re.TimeStamp,
                    (hist, re) => new { hist.TimeStamp, hist.laggedPrice, hist.priceH23, re.PV_Ante, re.WI_Ante })
                .Join(load,
                hist => hist.TimeStamp,
                ld => ld.TimeStamp,
                (hist, ld) => new { hist.TimeStamp, hist.laggedPrice, hist.priceH23, hist.PV_Ante, hist.WI_Ante, ld.Volume })
                .Join(DA_Data,
                    hist => hist.TimeStamp,
                    da => da.TimeStamp,
                    (hist, da) =>
                    new ForecastPriceDA(da.TimeStamp, da.Price, hist.laggedPrice, hist.priceH23, hist.PV_Ante, hist.WI_Ante, hist.Volume.HasValue ? hist.Volume.Value : 0)).AsEnumerable();
        }
    }
}
