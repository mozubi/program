using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Simulation;

namespace winMoz.Tools.Prediction.Prices.DayAhead
{
    public class PredictorPriceDA : MLNetPredictor<ForecastPriceDA, (DateTime TimeStamp, float PricePrediction)>
    {
        protected override string MODEL_FILEPATH => $@"{Config.ProjectPath}\winMoz\Data\Files\Prediction\DayAheadPrices\PriceDAModel.zip";

        public override (DateTime TimeStamp, float PricePrediction) Predict(ForecastPriceDA input)
        {
            var predEngine = mlContext.Model.CreatePredictionEngine<ForecastPriceDA, PredictionScore>(mlModel);

            // Vorhersagefunktion
            return (input.TimeStamp, Convert.ToSingle(predEngine.Predict(input).Score));
        }

        public override IEnumerable<(DateTime TimeStamp, float PricePrediction)> Predict(IEnumerable<ForecastPriceDA> input)
        {
            return MultistepPrediction(input).Zip(input.Select(el => el.TimeStamp), (pred, ts) => (ts, pred));
        }

        #region ML.Net-Funktionen


        protected override IEstimator<ITransformer> BuildTrainingPipeline(MLContext mlContext, IDataView dataView)
        {
            var features = GetFeatureNames(dataView).ToList();

            // Data process configuration with pipeline data transformations 
            var dataProcessPipeline = mlContext.Transforms.Concatenate("Features", features.ToArray());

            // Set the training algorithm 
            var trainer = mlContext.Regression.Trainers.Ols(labelColumnName: "Price", featureColumnName: "Features");
            var trainingPipeline = dataProcessPipeline.Append(trainer);
            return trainingPipeline;
        }

        protected override IEnumerable<string> GetFeatureNames(IDataView dataView, bool forPFI = false)
        {
            return new List<string>() { "WeekDay", "Hour", "PVAnte", "WindAnte", "Load", "PriceLastDay", "PriceH23" };
        }
        #endregion
    }
}
