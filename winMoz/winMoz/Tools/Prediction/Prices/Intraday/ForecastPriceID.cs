﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Tools.Prediction.KerasPredictor;

namespace winMoz.Tools.Prediction.Prices.Intraday
{
    public class ForecastPriceID : ForecastInput<ForecastPriceID>
    {
        public ForecastPriceID()
        {

        }

        /// <summary>
        /// Constructor for ID Forecast
        /// </summary>
        /// <param name="timestamp">Prediction timestamp</param>
        /// <param name="last4IDPrices">The ID Prices of the previous hour. Last price in the list is the newest</param>
        public ForecastPriceID(DateTime timestamp, float DAPrice, float REGrad)
        {
            TimeStamp = timestamp;
            QHour = TimeStamp.GetQuarterHourOfDay();
            REGradient = REGrad;
            DayAhead_Price = (float)DAPrice;
        }
        public ForecastPriceID(DateTime timestamp, float IDPrice, float DAPrice, float REGrad)
        {
            TimeStamp = timestamp;
            QHour = TimeStamp.GetQuarterHourOfDay();
            REGradient = REGrad;
            DayAhead_Price = (float)DAPrice;
            Price = (float)IDPrice;
        }
        [NoColumn]
        public DateTime TimeStamp { get; set; }

        [ColumnName("Price"), LoadColumn(0)]
        public float Price { get; set; }

        [ColumnName("QHour"), LoadColumn(1)]
        public float QHour { get; set; }

        [ColumnName("REGradient"), LoadColumn(2)]
        public float REGradient { get; set; }

        [ColumnName("DayAhead_Price"), LoadColumn(3)]
        public float DayAhead_Price { get; set; }

        protected override IEnumerable<ForecastPriceID> trainingData()
        {
            var repo = new PredictionDataRepository();
            // Get historic DA Prices
            List<(DateTime TimeStamp, float Price)> DAPrices = repo.DayAheadDataRepo.Get().Select(el => (el.TimeStamp, el.Price))
                .ToList()
                .Resample(Resampling.TimeStep.Hour, Resampling.Method.Average);
            // this is only done, cause there are no open Source ID Prices, and cannot therefore not be published.
            // We create a quarterhourly resolution of prices to try simulating the price a bit
            List<(DateTime TimeStamp, float Price)> IDPrices = repo.IntradayDataRepo.Get().Select(el => (el.TimeStamp, el.Price)).ToList();
            if (IDPrices.Count == 0)
            {
                IDPrices = DAPrices.Resample(Resampling.TimeStep.QuarterHour, Resampling.Method.Akima);
            }
            
            // Get RE Data to calculate the difference to the average production
            List<(DateTime TimeStamp, float RE)> RE = repo.REDataRepo.Get().Select(item => (item.TimeStamp, item.PV_Ante + item.WI_Ante)).ToList();
            var REAverage = RE.Resample(Resampling.TimeStep.Hour, Resampling.Method.Average).ToList()
                    .Select(item => new { item.TimeStamp, RE_Av = item.Item2 }).ToList();
            var trainingData = new List<ForecastPriceID>();

            foreach (var IDPrice in IDPrices)
            {
                DateTime t = IDPrice.TimeStamp;
                float DA = DAPrices.First(el => el.TimeStamp.Date == t.Date && el.TimeStamp.Hour == t.Hour).Price;
                var REav = REAverage.First(el => el.TimeStamp.Date == t.Date && el.TimeStamp.Hour == t.Hour).RE_Av;
                var REGrad = RE.First(el => el.TimeStamp == t).RE - REav;
                trainingData.Add(new ForecastPriceID(t, IDPrice.Price, DA, REGrad));
            }

            return trainingData;
        }
    }
}
