﻿using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers.FastTree;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Simulation;

namespace winMoz.Tools.Prediction.Prices.Intraday
{
    public class PredictorPriceID : MLNetPredictor<ForecastPriceID, (DateTime TimeStamp, float PricePrediction)>
    {
        protected override string MODEL_FILEPATH => $@"{Config.ProjectPath}\winMoz\Data\Files\Prediction\IntraDayPrices\PriceIDModel.zip";

        public override (DateTime TimeStamp, float PricePrediction) Predict(ForecastPriceID input)
        {
            var predEngine = mlContext.Model.CreatePredictionEngine<ForecastPriceID, PredictionScore>(mlModel);

            // Vorhersagefunktion
            return (input.TimeStamp, Convert.ToSingle(predEngine.Predict(input).Score));
        }

        public override IEnumerable<(DateTime TimeStamp, float PricePrediction)> Predict(IEnumerable<ForecastPriceID> input)
        {
            return MultistepPrediction(input).Zip(input.Select(el => el.TimeStamp), (pred, ts) => (ts, pred));
        }

        #region ML.Net-Funktionen
        protected override IEstimator<ITransformer> BuildTrainingPipeline(MLContext mlContext, IDataView dataView)
        {
            // Data process configuration with pipeline data transformations 
            var features = GetFeatureNames(dataView);
            var dataProcessPipeline = mlContext.Transforms.Concatenate("Features", features.ToArray());
            //Trainer Options
            var trainer = mlContext.Regression.Trainers.FastTree(labelColumnName: "Price", featureColumnName: "Features");
            var trainingPipeline = dataProcessPipeline.Append(trainer);

            return trainingPipeline;
        }

        protected override IEnumerable<string> GetFeatureNames(IDataView dataView, bool forPFI = false)
        {
            return new List<string>() { "QHour", "REGradient", "DayAhead_Price"};
        }

        #endregion
    }
}
