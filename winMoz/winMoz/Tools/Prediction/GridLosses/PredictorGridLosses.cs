﻿using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers.FastTree;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Simulation;
using winMoz.Tools.Prediction;

namespace winMoz.Tools.Prediction.GridLosses
{
    public class PredictorGridLosses : MLNetPredictor<ForecastGridLosses, (DateTime TimeStamp, float GridLossPrediction)>
    {
        protected override string MODEL_FILEPATH => $@"{Config.ProjectPath}\winMoz\Data\Files\Prediction\GridLoss\GridLossModel.zip";

        public override (DateTime TimeStamp, float GridLossPrediction) Predict(ForecastGridLosses input)
        {
            ITransformer mlModel = LoadModel();
            var predEngine = mlContext.Model.CreatePredictionEngine<ForecastGridLosses, PredictionScore>(mlModel);

            // Vorhersagefunktion
            return (input.TimeStamp, Convert.ToSingle(predEngine.Predict(input).Score));
        }

        public override IEnumerable<(DateTime TimeStamp, float GridLossPrediction)> Predict(IEnumerable<ForecastGridLosses> input)
        {
            return MultistepPrediction(input).Zip(input.Select(el => el.TimeStamp), (pred, ts) => (ts, pred));
        }

        #region ML.Net-Funktionen
        protected override IEstimator<ITransformer> BuildTrainingPipeline(MLContext mlContext, IDataView dataView)
        {
            var features = GetFeatureNames(dataView).ToList();

            // Data process configuration with pipeline data transformations 
            var dataProcessPipeline = mlContext.Transforms.Concatenate("Features", features.ToArray());

            var options = new FastTreeTweedieTrainer.Options
            {
                FeatureColumnName = "Features",
                LabelColumnName = "GridLoss",
                LearningRate = 0.2753930389881134,
                MinimumExampleCountPerLeaf = 50,
                NumberOfLeaves = 41,
                NumberOfTrees = 500,
                Shrinkage = 1.9029048681259155
            };

            var trainer = mlContext.Regression.Trainers.FastTreeTweedie(options);
            // Set the training algorithm 
            var trainingPipeline = dataProcessPipeline.Append(trainer);

            return trainingPipeline;
        }

        protected override IEnumerable<string> GetFeatureNames(IDataView dataView, bool forPFI = false)
        {
            return new List<string>() { "SLP", "QHour" };
        }
        #endregion
    }
}
