﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Tools.Prediction.KerasPredictor;
using winMoz.Tools.Viz;

namespace winMoz.Tools.Prediction.GridLosses
{
    public class ForecastGridLosses : ForecastInput<ForecastGridLosses>, IForecastGridLosses
    {
        public ForecastGridLosses()
        {
        }

        public ForecastGridLosses(DateTime timeStamp, float sLP)
        {
            TimeStamp = timeStamp;
            SLP = (float)sLP;
          
            QHour = (float)timeStamp.GetQuarterHourOfDay();
        }

        public ForecastGridLosses(DateTime timeStamp, float gridLoss, float sLP)
        {
            TimeStamp = timeStamp;
            GridLoss = (float)gridLoss;
            SLP = (float)sLP;
            QHour = (float)timeStamp.GetQuarterHourOfDay();
        }

        [NoColumn]
        public DateTime TimeStamp { get; set; }

        [ColumnName("GridLoss"), LoadColumn(0)]
        public float GridLoss { get; set; }

        [ColumnName("SLP"), LoadColumn(1)]
        public float SLP { get; set; }

        [ColumnName("QHour"), LoadColumn(2)]
        public float QHour { get; set; }

        protected override IEnumerable<ForecastGridLosses> trainingData()
        {
            var data = new List<ForecastGridLosses>();
            using (var repo = new PredictionDataRepository())
                data = repo.Get<WestNetz>().Select(el => new ForecastGridLosses(el.TimeStamp, el.NetzverlusteMW, el.SlpMW)).ToList();

            data.Select(el => (el.TimeStamp, (float)el.GridLoss, (float)el.SLP)).Plot("Loss", "SLP");

            return data;
        }

        public override IEnumerable<float[]> TransformAndNormalize(IEnumerable<ForecastGridLosses> input)
        {
            throw new NotImplementedException();
        }
    }
}
