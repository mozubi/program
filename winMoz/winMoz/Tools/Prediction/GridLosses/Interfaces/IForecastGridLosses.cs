﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Tools.Prediction.KerasPredictor;
using winMoz.Tools.Viz;

namespace winMoz.Tools.Prediction.GridLosses
{
    public interface IForecastGridLosses : IForecastInput<ForecastGridLosses>
    {


        [NoColumn]
        DateTime TimeStamp { get; set; }

        [ColumnName("GridLoss"), LoadColumn(0)]
        float GridLoss { get; set; }

        [ColumnName("SLP"), LoadColumn(1)]
        float SLP { get; set; }

        [ColumnName("QHour"), LoadColumn(2)]
        float QHour { get; set; }
    }
}
