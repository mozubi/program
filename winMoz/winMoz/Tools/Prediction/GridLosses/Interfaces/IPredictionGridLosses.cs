﻿namespace winMoz.Tools.Prediction.GridLosses
{
    public interface IPredictionGridLosses
    {
        float Score { get; set; }
    }
}
