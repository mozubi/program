﻿namespace winMoz.Tools.Prediction
{
    internal class PredictionScore : IPredictionScore
    {
        public float Score { get; set; }
    }
}
