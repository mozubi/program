﻿using Keras.Callbacks;
using Keras.Layers;
using Keras.Models;
using Keras.Optimizers;
using Numpy;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using winMoz.Simulation;
using System.IO;

namespace winMoz.Tools.Prediction.KerasPredictor
{
    public class PredictorKeras<TForecast> : IPredictor<TForecast, (DateTime TimeStamp, float Power)> where TForecast : ForecastInput<TForecast>
    {
        private string MODEL_DIR => $@"{Config.ProjectPath}\winMoz\Data\Files\Prediction\Keras\";
        private string MODEL_FILEPATH => MODEL_DIR + $"model_{typeof(TForecast).Name}.h5";
       
        public void Train(IEnumerable<TForecast> trainingData)
        {
            //var constraint = new Keras.StringOrInstance(new Keras.Constraints.MaxNorm().ToPython());
            int numOfFeatures = trainingData.First().NumOfFeatures - 1;
            try
            {
                var model = GetModel(numOfFeatures);
                var (x, y) = TransformData(trainingData);

                var now = DateTime.Now;
                string TensorBoardDirectory = MODEL_DIR + $@"\Logs\Fit\" + $"{ typeof(TForecast).Name}" + now.ToFileTime();
                var TensorBoardCallback = new Keras.Callbacks.TensorBoard(TensorBoardDirectory, histogram_freq: 1);
                var cbs = new Callback[] { new EarlyStopping(monitor: "val_mae", patience: 10), TensorBoardCallback };
                var history = model.Fit(x, np.array(y, dtype: np.float32), epochs: 100, batch_size: 24, validation_split: (float)0.15, callbacks: cbs);


                model.Save(MODEL_FILEPATH);
            }
            catch (DllNotFoundException)
            {
                Debugger.Break();
                Log.Error("Python not found. Try adding the python-path to your PATH environment variables.");
                Config.AddPythonToPATH();
            }
        }

        private Sequential GetModel(int features)
        {
            var model = new Sequential();
            model.Add(new Dense(features + 2, activation: "sigmoid", input_shape: new Keras.Shape(features)));
            model.Add(new Dense(features + 2, activation: "sigmoid"));
            model.Add(new Dense(features + 2, activation: "tanh"));
            model.Add(new Dense(1));
            model.Compile(loss: "mse", optimizer: new RMSprop(), metrics: new string[] { "mae", "mse" });
            return model;
        }

        private (NDarray x, NDarray y) TransformData(IEnumerable<TForecast> forecast)
        {
            var input = Activator.CreateInstance<TForecast>().TransformAndNormalize(forecast).ToList();

            var _x = input.Select(el => el.Skip(1)).ToList();
            var _y = input.Select(el => el[0]).ToList();

            var x = new List<NDarray<float>>();
            for (int i = 0; i < input.Count; i++)
                x.Add(np.array(_x[i].ToArray()));

            return (np.array(x), np.array(_y.ToArray()));
        }

        public (DateTime TimeStamp, float Power) Predict(TForecast input) => Predict(new List<TForecast>() { input }).First();

        public IEnumerable<(DateTime TimeStamp, float Power)> Predict(IEnumerable<TForecast> input)
        {
            if (!File.Exists(MODEL_FILEPATH))
                Train(ForecastInput<TForecast>.GetTrainingData());
            var model = BaseModel.LoadModel(MODEL_FILEPATH, compile: false);
            var data = TransformData(input);
            var prediction = model.Predict(data.x);
            return input.Zip(prediction.GetData<float>().Select(el => (float)el).ToList(), (inp, pred) => (inp.TimeStamp, pred));
        }
    }
}
