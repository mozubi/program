﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace winMoz.Tools.Prediction.KerasPredictor
{
    public abstract class ForecastInput<TForecast> where TForecast : ForecastInput<TForecast>
    {
        protected ForecastInput() { }

        protected ForecastInput(DateTime timeStamp)
        {
            TimeStamp = timeStamp;
        }

        public DateTime TimeStamp { get; set; }

        public int NumOfFeatures => GetType().GetProperties().Where(prop => prop.PropertyType == typeof(float)).Count() - 1;

        public virtual IEnumerable<float[]> TransformAndNormalize(IEnumerable<TForecast> input) => throw new NotImplementedException();

        protected abstract IEnumerable<TForecast> trainingData();

        public static IEnumerable<TForecast> GetTrainingData() => ((TForecast)Activator.CreateInstance(typeof(TForecast))).trainingData();
    }
}