﻿using System.Collections.Generic;
using System;

namespace winMoz.Tools.Prediction.KerasPredictor
{
    public interface IForecastInput<TForecast>
    {

        DateTime TimeStamp { get; set; }

        int NumOfFeatures { get; }

        IEnumerable<float[]> TransformAndNormalize(IEnumerable<TForecast> input);
    }
}
