﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Helper;
using winMoz.Simulation;

namespace winMoz.Tools.Prediction
{
    public static class Forecast
    {
        private const int Hours = 5;
        private static List<int> SeedValues = MathHelper.GetRandomSeeds(Simulator.Times.Start.Year, Simulator.Times.SimDuration().Days * 5);
        public enum Type
        {
            SolarRadDir, SolarRadDiff, Wind, Temperature, DayAheadPrice, IntraDayPrice, MonthlyPrices, Run_Of_River
        }

        #region Public - Methods
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="actual"></param>
        /// <param name="seed"></param>
        /// <param name="callingFunction">Wind:1, Solar: 2, Temperatur: 3, SpotPrices:  4</param>
        /// <returns></returns>
        public static List<(DateTime TimeStamp, float value)> GetForecast(List<(DateTime TimeStamp, float value)> actual, Type forecastType, ISeedable seedable) => GetForecast(actual, forecastType, seedable.GetSeed());
        public static List<(DateTime TimeStamp, float value)> GetForecast(List<(DateTime TimeStamp, float value)> actual, Type forecastType, int seed = default)
        {
            var forecast = new List<(DateTime, float)>();

            if (actual.Count() != 0 && (Simulator.Times.SimTime == default || Simulator.Times.SimTime.Ticks < actual.Last().TimeStamp.Ticks))
            {
                var targetRMSEbyTime = GetTargetRMSE(actual.Select(el => el.TimeStamp).ToList(), forecastType);

                forecast.AddRange(actual.Where(el => el.TimeStamp < targetRMSEbyTime[0].TimeStamp).ToList());

                for (int i = 1; i < targetRMSEbyTime.Count; i++)
                {
                    if (!actual.Any(el => el.TimeStamp < targetRMSEbyTime[i].TimeStamp && el.TimeStamp >= targetRMSEbyTime[i - 1].TimeStamp))
                        continue;

                    var actualSelection = actual.Where(el => el.TimeStamp < targetRMSEbyTime[i].TimeStamp && el.TimeStamp >= targetRMSEbyTime[i - 1].TimeStamp).ToList();
                    if (actualSelection.Count() != 0)
                    {
                        bool smoothErrors = forecastType == Type.Wind && (actualSelection.First().TimeStamp - actual.First().TimeStamp).Days > 3;

                        var forecastError = GetForecastErrors(forecastType, actualSelection, targetRMSEbyTime[i].RMSError, seed, smoothErrors);
                        forecast = forecast.Concat(forecastError).ToList();
                    }
                }
            }
            else
                forecast = actual;

            return forecast;
        }
        #endregion

        #region Private - Methods
        private static List<(DateTime TimeStamp, float Value)> GetForecastErrors(Type forecastType, List<(DateTime TimeStamp, float Value)> actual, float targetAverageRmse, ISeedable seedable = null, bool smoothErrors = false)
            => GetForecastErrors(forecastType, actual, targetAverageRmse, seedable.GetSeed(), smoothErrors);

        private static List<(DateTime TimeStamp, float Value)> GetForecastErrors(Type forecastType, List<(DateTime TimeStamp, float Value)> actual, float targetAverageRmse, int seed = default, bool smoothErrors = false)
        {
            var forecastErrors = new List<(DateTime TimeStamp, float Value)>();
            bool typeIsSolar = forecastType == Type.SolarRadDir || forecastType == Type.SolarRadDiff;

            if (typeIsSolar)
                if (actual.Average(el => el.Value) <= 0.2) return actual.MovingAverage(3); // Nachtstunden nicht rechnen => Prognosefehler = 0

            int i = 0;
            float rmse = 0;
            float scale = Math.Min(10.0F, Math.Max(0.0001F, 0.01F * targetAverageRmse));

            float scaleOfScale = 1.1F;

            (float upper, float lower) ForecastLimits = GetLimits(forecastType);
            List<float> rndErrors = new List<float>();

            int actualCount = actual.Select(el => el.Value).ToList().Count;
            if (!actual.All(item => item.Value == 0))
            {
                while (rmse < targetAverageRmse || i == 0)
                {
                    rndErrors = MathHelper.GetRandomDistribution(1, scale, actualCount, seed);

                    if (smoothErrors)
                        rndErrors = rndErrors.MovAverage(24);

                    forecastErrors = MergeActualWithErrors(actual, rndErrors, ForecastLimits);

                    rmse = MathHelper.RMSE(actual.Select(el => el.Value).ToList(), forecastErrors.Select(el => el.Value).ToList());

                    scaleOfScale = Math.Min(10001F, 1.0001F * (float)Math.Pow(1.1, Math.Max(1.1, -Math.Log(rmse / targetAverageRmse) / Math.Log(1.1))));

                    //if (Math.Abs(rmse - targetAverageRmse) / targetAverageRmse < 0.8)
                    //    scaleOfScale = 1.4641; //1.1^4

                    //if (Math.Abs(rmse - targetAverageRmse) / targetAverageRmse < 0.5)
                    //    scaleOfScale = 1.21; //1.1^2

                    //if (Math.Abs(rmse - targetAverageRmse) / targetAverageRmse < 0.2)
                    //    scaleOfScale = 1.1;

                    if (rmse < targetAverageRmse)
                        scale *= scaleOfScale;

                    if (rmse > targetAverageRmse)
                        scale /= scaleOfScale;

                    scale = Math.Min(10001F, scale);

                    i++;

                    if (i > 1000 || scale <= 0)
                    {
                        // TODO: Forecast-Error RMSE bei PV-Anlagen - Nachts berücksichtigen
                        //Log.Warning($"Couldn't find distribution for error. Adjust Target-RMSE for {forecastType}");
                        break;
                    }
                } 
            }
            else
                forecastErrors = actual;
                
                if (forecastErrors.Any(item => item.Value > 10000000))
                forecastErrors = forecastErrors.Select(item => (item.TimeStamp, item.Value > 100000000 ? 0F : item.Value)).ToList();

            return forecastErrors;
        }

        private static List<(DateTime TimeStamp, float Value)> MergeActualWithErrors(List<(DateTime TimeStamp, float Value)> actual, List<float> rndErrors, (float upper, float lower) limits)
        {
            if (actual.Count > 20) // Spline-Interpolation erst ab 5h | 20qh
                return rndErrors.Zip(actual, (x, y) => (y.TimeStamp, x * y.Value)).ToList().Select(el => (el.TimeStamp, ApplyLimits(el.Item2, limits))).ToList();
            else
            {
                //List<(DateTime TimeStamp, float Value)> coupledList = new List<(DateTime TimeStamp, float Value)>();
                //if (actual.Count() < 10)
                //{
                //    for (int i = 0; i < actual.Count(); i++)
                //        coupledList.Add((actual[i].TimeStamp, actual[i].Value * rndErrors[i]));
                //}
                //else
                var coupledList = actual.Zip(rndErrors, (x, y) => (x.TimeStamp, y * x.Value)).ToList();
                var movingAverage = coupledList.MovingAverage(2).ToList();
                var result = movingAverage.Select(el => (el.TimeStamp, ApplyLimits(el.Item2, limits))).ToList();
                return result;
            }
        }

        private static float ApplyLimits(float value, (float upper, float lower) limit)
        {
            return Math.Min(Math.Max(limit.lower, value), limit.upper);
        }

        private static (float upper, float lower) GetLimits(Type errorType)
        {
            switch (errorType)
            {
                case Type.SolarRadDiff:
                case Type.SolarRadDir:
                    return (float.MaxValue, 0);
                case Type.Wind:
                    return (float.MaxValue, 0);
                case Type.Temperature:
                    return (50, -50);
                case Type.DayAheadPrice:
                case Type.MonthlyPrices:
                    return (3000, -500);
                case Type.IntraDayPrice:
                    return (3000, -3000);
                case Type.Run_Of_River:
                    return (float.MaxValue, 0);
                default:
                    return (float.MaxValue, float.MinValue);
            }
        }

        private static List<(DateTime TimeStamp, float RMSError)> GetTargetRMSE(List<DateTime> times, Type errorType)
        {
            var timeSpans = new List<TimeSpan>();

            if (errorType != Type.MonthlyPrices)
            {
                timeSpans.Add(new TimeSpan(1, 0, 0));       // One Hour Ahead
                timeSpans.Add(new TimeSpan(2, 0, 0));         // Two Hours Ahead
                timeSpans.Add(new TimeSpan(6, 0, 0));          // Six Hours Ahead
                timeSpans.Add(new TimeSpan(1, 0, 0, 0));           // Day Ahead
                timeSpans.Add(new TimeSpan(7, 0, 0, 0));// Week Ahead
                timeSpans.Add((Simulator.Times.SimTime != default ? Simulator.Times.SimTime.GetDurationOfMonth() : times.First().GetDurationOfMonth()).Add(TimeSpan.FromDays(4))); // Month Ahead
            }
            else
            {
                if (Simulator.Times.SimTime != default) //Fülle Bereich zwischen SimTime und times.First().
                {
                    times.AddRange(Enumerable.Range(0, (int)((times.First() - Simulator.Times.SimTime).TotalMinutes / 15)).Select(el => Simulator.Times.SimTime.AddMinutes(15 * el)).ToList());
                    times = times.OrderBy(el => el).ToList();
                }
                times = times.Where(el => el.Hour == 0 && el.Minute == 0).ToList();
                timeSpans.Add(new TimeSpan(1, 0, 0, 0));
                times.ForEach(time => timeSpans.Add(timeSpans.Last().Add(new TimeSpan(1, 0, 0, 0))));
            }

            List<float> rmseSolar;
            switch (errorType)
            {
                case Type.SolarRadDiff:
                    rmseSolar = new List<float>()
                    {
                        0,       // Actual
                        2,       // One Hour Ahead
                        4,       // Two Hours Ahead
                        6,       // Six Hours Ahead
                        7,       // Day Ahead
                        35,      // Week Ahead
                        70       // Month Ahead
                    };
                    return GenerateRMSEValues(times, timeSpans, rmseSolar).Where(item => item.TimeStamp >= times.First()).ToList();
                case Type.SolarRadDir:
                    rmseSolar = new List<float>()
                    {
                        0,       // Actual
                        0.25F,       // One Hour Ahead
                        1,       // Two Hours Ahead
                        2,       // Six Hours Ahead
                        3,       // Day Ahead
                        3,       // Week Ahead
                        3        // Month Ahead
                    };
                    return GenerateRMSEValues(times, timeSpans, rmseSolar).Where(item => item.TimeStamp >= times.First()).ToList();
                case Type.Wind:
                    List<float> rmseWind = new List<float>()
                    {
                        0,       // Actual
                        0.25F,    // One Hour Ahead
                        0.5F,     // Two Hours Ahead
                        0.75F,    // Six Hours Ahead
                        1,       // Day Ahead
                        5,       // Week Ahead
                        9        // Month Ahead
                    };
                    return GenerateRMSEValues(times, timeSpans, rmseWind).Where(item => item.TimeStamp >= times.First()).ToList();
                case Type.Temperature:
                    List<float> rmseTemp = new List<float>()
                    {
                        0,       // Actual
                        0.2F,     // One Hour Ahead
                        0.35F,    // Two Hours Ahead
                        0.5F,     // Six Hours Ahead
                        0.75F,    // Day Ahead
                        4,       // Week Ahead
                        7.5F      // Month Ahead
                    };
                    return GenerateRMSEValues(times, timeSpans, rmseTemp).Where(item => item.TimeStamp >= times.First()).ToList();
                case Type.DayAheadPrice:
                    List<float> rmseDA = new List<float>()
                    {
                        0,       // Actual
                        0.425F,   // One Hour Ahead
                        0.425F,   // Two Hours Ahead
                        0.45F,    // Six Hours Ahead
                        0.5F,     // Day Ahead
                        4,       // Week Ahead
                        7.5F      // Month Ahead
                    };
                    return GenerateRMSEValues(times, timeSpans, rmseDA).Where(item => item.TimeStamp >= times.First()).ToList();
                case Type.MonthlyPrices:
                    var rmseValues = new List<float>() { 1 };
                    timeSpans.ForEach(el => rmseValues.Add(1));
                    return GenerateRMSEValues(times, timeSpans, rmseValues).Where(item => item.TimeStamp >= times.First()).ToList();
                case Type.IntraDayPrice:

                case Type.Run_Of_River:
                    List<float> rmseRunOfRiver = new List<float>()
                    {
                        0,       // Actual
                        0.0061F,   // One Hour Ahead
                        0.0061F,   // Two Hours Ahead
                        0.034F,    // Six Hours Ahead
                        0.034F,     // Day Ahead
                        0.15F,       // Week Ahead
                        0.15F      // Month Ahead
                    };
                    return GenerateRMSEValues(times, timeSpans, rmseRunOfRiver).Where(item => item.TimeStamp >= times.First()).ToList();
                default:
                    return null;
            }
        }

        private static List<(DateTime TimeStamp, float RMSError)> GenerateRMSEValues(List<DateTime> times, List<TimeSpan> timeSpans, List<float> rmseValues)
        {
            List<(DateTime Time, float RMSE)> temp = new List<(DateTime, float)>();

            for (int i = 0; i < timeSpans.Count; i++)
            {
                temp.Add(((Simulator.Times.SimTime != default ? Simulator.Times.SimTime : times.First()) + timeSpans[i], rmseValues[i]));
            }

            // INTERPOLATE AND FILL UP DAY-GAPS
            int count = temp.Count;
            for (int i = 0; i < count; i++)
            {
                if (count - i == 1)
                    break;

                int days = (temp[i + 1].Time - temp[i].Time).Days;
                if (days > 1)
                {
                    var rmse2insert = temp[i].RMSE.InterpolateLinear(temp[i + 1].RMSE, days);

                    var values = new List<(DateTime, float)>();

                    for (int j = 1; j < rmse2insert.Count - 1; j++)
                    {
                        values.Add((temp[i].Time.AddDays(j), rmse2insert[j]));
                    }

                    temp.InsertRange(i + 1, values);
                    count = temp.Count;
                }

            }

            //if (temp.First().Time.Ticks > times.First().Ticks)
            //{
            //    if (Simulator.Times.SimTime != default) //Fülle Bereich zwischen SimTime und times.First().
            //    {
            //        temp.AddRange(Enumerable.Range(0, (int)((Simulator.Times.SimTime - times.First()).TotalMinutes / 15)).Select(el => (Simulator.Times.SimTime.AddMinutes(15 * el), 0.0F)).ToList());
            //        temp = temp.OrderBy(el => el).ToList();
            //    }
            //}

            return temp;
        }
    }
    #endregion
}
