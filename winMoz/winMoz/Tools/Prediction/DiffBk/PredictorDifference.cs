﻿using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Simulation;

namespace winMoz.Tools.Prediction.DiffBk
{
    public class PredictorDifference : MLNetPredictor<ForecastDiff, (DateTime TimeStamp, float DifferencePrediction)>, IPredictorDifference
    {
        protected override string MODEL_FILEPATH => $@"{Config.ProjectPath}\winMoz\Data\Files\Prediction\Difference\DifferenceModel.zip";

        public List<ForecastDiff> HistoricValues { get; private set; } = ForecastDiff.GetTrainingData().ToList();


        public override (DateTime TimeStamp, float DifferencePrediction) Predict(ForecastDiff input)
        {
            ITransformer mlModel = LoadModel();
            var predEngine = mlContext.Model.CreatePredictionEngine<ForecastDiff, PredictionDifference>(mlModel);

            // Vorhersagefunktion
            return (input.TimeStamp, Convert.ToSingle(predEngine.Predict(input).Score));
        }

        public override IEnumerable<(DateTime TimeStamp, float DifferencePrediction)> Predict(IEnumerable<ForecastDiff> input)
        {
            return MultistepPrediction(input).Zip(input.Select(el => el.TimeStamp), (pred, ts) => (ts, pred));
        }


        protected override List<float> MultistepPrediction(IEnumerable<ForecastDiff> forecast)
        {
            var dataView = mlContext.Data.LoadFromEnumerable(forecast);

            ITransformer mlModel = LoadModel();

            var predictions = mlModel.Transform(dataView);

            var scores = predictions.GetColumn<float[]>("Score").ToList();

            return scores.Select(el => (float)el[0]).ToList();
        }

        protected override IEstimator<ITransformer> BuildTrainingPipeline(MLContext mlContext, IDataView dataView)
        {
            return mlContext.Forecasting.ForecastBySsa(
                    outputColumnName: "Score",
                    inputColumnName: "DifferencePowerMW",
                    windowSize: 96,
                    seriesLength: 3 * 96,
                    trainSize: 35000,
                    horizon: 96,
                    confidenceLevel: 0.95f,
                    confidenceLowerBoundColumn: "LowerBound",
                    confidenceUpperBoundColumn: "UpperBound");

            // The forecastingPipeline takes 365 data points for the first year and samples or splits the time - 
            // series dataset into 30 - day(monthly) intervals as specified by the seriesLength parameter.
            // Each of these samples is analyzed through weekly or a 7 - day window.When determining what the forecasted 
            // value for the next period(s) is, the values from previous seven days are used to make a prediction.The model 
            // is set to forecast seven periods into the future as defined by the horizon parameter.Because a forecast is 
            // an informed guess, it's not always 100% accurate. Therefore, it's good to know the range of values in the best 
            // and worst -case scenarios as defined by the upper and lower bounds.In this case, the level of confidence for 
            // the lower and upper bounds is set to 95 %.The confidence level can be increased or decreased accordingly.
            // The higher the value, the wider the range is between the upper and lower bounds to achieve the desired 
            // level of confidence.
        }

        protected override void Evaluate(MLContext mlContext, IDataView trainingDataView, IEstimator<ITransformer> trainingPipeline)
        {
            IDataView predictions = trainingPipeline.Fit(trainingDataView).Transform(trainingDataView);
            IEnumerable<float> actual = mlContext.Data.CreateEnumerable<ForecastDiff>(trainingDataView, true).Select(observed => observed.DifferencePowerMW);
            IEnumerable<float> forecast = mlContext.Data.CreateEnumerable<PredictionDifference>(predictions, true).Select(prediction => prediction.Score[0]);
            var metrics = actual.Zip(forecast, (actualValue, forecastValue) => actualValue - forecastValue);
            var MAE = metrics.Average(error => Math.Abs(error)); // Mean Absolute Error
            var RMSE = Math.Sqrt(metrics.Average(error => Math.Pow(error, 2))); // Root Mean Squared Erro

            Console.WriteLine($"MAE: {MAE} ; RMSE: {RMSE}");
        }


        protected override IEnumerable<string> GetFeatureNames(IDataView dataView, bool forPFI = false)
        {
            return new[] { "" };
            //return new[] { "DifferencePowerMW" };
        }
    }
}
