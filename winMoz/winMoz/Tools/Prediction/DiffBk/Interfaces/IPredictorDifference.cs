﻿using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Simulation;

namespace winMoz.Tools.Prediction.DiffBk
{
    public interface IPredictorDifference : IPredictor<ForecastDiff, (DateTime TimeStamp, float DifferencePrediction)>
    {
        List<ForecastDiff> HistoricValues { get; }


    }
}
