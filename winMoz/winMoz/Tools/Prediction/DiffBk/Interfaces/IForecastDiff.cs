﻿using Microsoft.ML.Data;
using NumSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Tools.Prediction.KerasPredictor;

namespace winMoz.Tools.Prediction.DiffBk
{
    public interface IForecastDiff : IForecastInput<ForecastDiff>
    {
        float DifferencePowerMW { get; set; }
    }

    public interface IPredictionDifference
    {
        float[] Score { get; set; }
        float[] LowerBound { get; set; }
        float[] UpperBound { get; set; }
    }
}
