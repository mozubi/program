﻿using Microsoft.ML.Data;
using NumSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Tools.Prediction.KerasPredictor;

namespace winMoz.Tools.Prediction.DiffBk
{
    public class ForecastDiff : ForecastInput<ForecastDiff>, IForecastDiff
    {
        public float DifferencePowerMW { get; set; }

        public ForecastDiff(DateTime timeStamp, float differencePowerMW) : base(timeStamp)
        {
            DifferencePowerMW = differencePowerMW;
        }

        public ForecastDiff()
        {
        }

        protected override IEnumerable<ForecastDiff> trainingData()
        {
            using (var repo = new PredictionDataRepository())
            {

                var data = repo.Get<WestNetz>(it => it.TimeStamp.Year == 2017 || it.TimeStamp.Year == 2018).Select(el => (el.TimeStamp, el.DiffBk)).ToList().OrderBy(el => el.TimeStamp);

                var years = data.Select(el => el.TimeStamp.Year).Distinct().OrderBy(el => el);

                var output = new List<(DateTime, float)>();
                foreach(var year in years)
                {
                    var set = data.Where(el => el.TimeStamp.Year == year);
                    var sum = set.Sum(el => Math.Abs(el.DiffBk));
                    output.AddRange(set.Select(el => (el.TimeStamp, el.DiffBk / sum)));
                }

                return output.Select(el => new ForecastDiff(el.Item1, (float)(el.Item2))).ToList();
            }

        }
    }

    public class PredictionDifference
    {
        public float[] Score { get; set; }
        public float[] LowerBound { get; set; }
        public float[] UpperBound { get; set; }
    }
}
