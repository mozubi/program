﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using winMoz.Helper;
using winMoz.Tools.Prediction.KerasPredictor;

namespace winMoz.Tools.Prediction.GridLoad
{
    public class ForecastGridLoad : ForecastInput<ForecastGridLoad>, IForecastGridLoad
    {
        public ForecastGridLoad()
        {
        }

        public ForecastGridLoad(DateTime time)
        {
            TimeStamp = time;
            DayOfYear = time.DayOfYear;
            WeekDay = (float)time.DayOfWeek;
            QHour = time.GetQuarterHourOfDay();
        }



        public ForecastGridLoad(DateTime time, float _GridLoad) : this(time)
        {
            GridLoad = (float)_GridLoad;
        }

        [NoColumn]
        public DateTime TimeStamp { get; set; }

        [ColumnName("GridLoad"), LoadColumn(0)]
        public float GridLoad { get; set; }

        [ColumnName("DayOfYear"), LoadColumn(1)]
        public float DayOfYear { get; set; }

        [ColumnName("WeekDay"), LoadColumn(2)]
        public float WeekDay { get; set; }

        [ColumnName("QHour"), LoadColumn(3)]
        public float QHour { get; set; }

        protected override IEnumerable<ForecastGridLoad> trainingData()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<float[]> TransformAndNormalize(IEnumerable<ForecastGridLoad> input)
        {
            throw new NotImplementedException();
        }
    }
}
