﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using winMoz.Helper;
using winMoz.Tools.Prediction.KerasPredictor;

namespace winMoz.Tools.Prediction.GridLoad
{
    public interface IForecastGridLoad : IForecastInput<ForecastGridLoad>
    {

        [NoColumn]
        DateTime TimeStamp { get; set; }

        [ColumnName("GridLoad"), LoadColumn(0)]
        float GridLoad { get; set; }

        [ColumnName("DayOfYear"), LoadColumn(1)]
        float DayOfYear { get; set; }

        [ColumnName("WeekDay"), LoadColumn(2)]
        float WeekDay { get; set; }

        [ColumnName("QHour"), LoadColumn(3)]
        float QHour { get; set; }
    }
}
