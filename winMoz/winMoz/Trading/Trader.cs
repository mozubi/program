﻿using System;
using System.Linq;
using System.Collections.Generic;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Markets;
using winMoz.Markets.Elements;
using winMoz.Simulation;
using winMoz.Markets.Bids;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.EoMMarkets;
using System.Threading.Tasks;
using winMoz.Assets.Plants;
using winMoz.Data.Access.Repositories.Prediction;
using System.Threading;

namespace winMoz.Portfolios
{
    public class Trader : ITrader
    {
        // Strategien bewerten und adaptieren
        List<TradingStrategy> Strategies = new List<TradingStrategy>();
        IEnumerable<Bid> Bids => this.Strategies.SelectMany(strategy => strategy.Bids);
        IEnumerable<IAsset> Assets => this.Strategies.SelectMany(el => el.Assets).Distinct();
        public List<Type> ExcludedMarkets { get; set; } = new List<Type>() { typeof(FuturesMarket), typeof(DayAheadMarket), typeof(IntradayMarket), typeof(ControlReserve_Capacity), typeof(ControlReserve_Energy) };
        #region Constructors
        public Trader() { }
        public Trader(params IAsset[] assets) : this(assets.ToList()) { }
        public Trader(IEnumerable<IAsset> assets) { BuildStrategies(assets); }
        public Trader(params TradingStrategy[] strategies) { this.Strategies.AddRange(strategies); }
        #endregion
        
        #region Markets
        public void SubscribeToMarket(params Market[] markets)
        { 
            markets.ToList().ForEach(market => { market.Auction += Trade; IncludeMarketType(market); });
            this.Strategies.ForEach(strategy => strategy.ChangeExcludedMarkets(ExcludedMarkets.ToArray()));
        }

        private void IncludeMarketType(Market market)
        {
            //wenn Wert nicht vorhanden wird auch nichts entfernt
            ExcludedMarkets.Remove(market.GetType());
        }

        /// <summary>
        /// Methode löst Handelsaktivitäten der Agenten aus
        /// </summary>
        /// <param name="callingMarket">Auslösendes Marktevent</param>
        /// <param name="auctionArgs">Übergeben Auktionsargumente</param>
        private void Trade(Market callingMarket, AuctionArgs auctionArgs)
        {
            //var commodities = Commodities.GetPricesOfAllCommodities(auctionArgs.TimeStamp, auctionArgs.Duration);
            //DateTime timeStart = DateTime.Now;
            foreach (var asset in this.Assets)
            {
                asset.CalcSchedule(auctionArgs.TimeStamp, auctionArgs.Duration);
            }
            
            //var timeExecutionEnd = DateTime.Now;
            //Log.Info("CalcSchedule " + Assets.FirstOrDefault() + " started at: " + timeStart.ToLongTimeString());
            //Log.Info("CalcSchedule " + Assets.FirstOrDefault() + " ended at: " + timeExecutionEnd.ToLongTimeString());
            //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
            //timeStart = DateTime.Now;
            this.Strategies.ForEach(strategy => strategy.Execute((Market)callingMarket));
            //timeExecutionEnd = DateTime.Now;
            //Log.Info("Strategies " + Assets.FirstOrDefault() + " started at: " + timeStart.ToLongTimeString());
            //Log.Info("Strategies " + Assets.FirstOrDefault() + " ended at: " + timeExecutionEnd.ToLongTimeString());
            //Log.Info("It took " + (timeExecutionEnd - timeStart).TotalMilliseconds + " milliseconds");
            //if ((timeExecutionEnd - timeStart).TotalMilliseconds > 1000) Debugger.Break();
        }

        #endregion

        #region Strategies
        private void BuildStrategies(IEnumerable<IAsset> assets)
        {
            var AssetStrategyGroups = assets.GroupBy(item => item.TradingStrategy, (el1, el2) => new { Strategy = el1, Assets = el2 }).ToList();
            foreach (var group in AssetStrategyGroups)
                this.Strategies.Add((TradingStrategy)Activator.CreateInstance(group.Strategy.TradingType, new object[] { group.Assets, ExcludedMarkets.ToArray() }));
        }

        public void Add(params TradingStrategy[] strategies) => this.Strategies.AddRange(strategies);
        public void Add(params IAsset[] assets) => BuildStrategies(assets);
        #endregion
    }
}
