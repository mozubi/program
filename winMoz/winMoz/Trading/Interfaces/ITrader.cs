﻿using System;
using System.Linq;
using System.Collections.Generic;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Markets;
using winMoz.Markets.Elements;
using winMoz.Simulation;
using winMoz.Markets.Bids;
using Keras.Layers;
using System.Windows.Forms;
using winMoz.Trading.Strategies;
using winMoz.Assets;
using winMoz.Information;
using winMoz.Helper;
using winMoz.Tools.Helper;
using System.Diagnostics;


namespace winMoz.Portfolios
{
    public interface ITrader
    {
        List<Type> ExcludedMarkets { get; set; }

        #region Markets
        void SubscribeToMarket(params Market[] markets);
        #endregion

        #region Strategies

        void Add(params TradingStrategy[] strategies);
        void Add(params IAsset[] assets);
        #endregion
    }
}
