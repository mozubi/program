﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using winMoz.Assets;
using winMoz.Helper;
using winMoz.Markets;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;

namespace winMoz.Portfolios
{
    public interface ITradingStrategy
    {
        ReadOnlyCollection<Bid> Bids { get; }
        IEnumerable<IAsset> Assets { get; }

        #region Base
        void Execute(Market market) ; 
        #endregion
    }
}