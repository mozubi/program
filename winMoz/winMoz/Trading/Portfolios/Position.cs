﻿using System;

namespace winMoz.Portfolios
{
    public abstract class Position 
    {
        #region Position-Attributes
        public DateTime TimeStamp { get; protected set; }
        public TimeSpan Duration { get; protected set; }
        private float HourDuration => (float)Duration.TotalHours;
        public float Power { get; protected set; }
        public float Price { get; protected set; }
        public float PriceTotal => this.Price * this.Volume;
        public float Volume => this.Power * this.HourDuration;
        #endregion

        #region Booleans
        public bool IsScheduled { get; set; }
        public bool IsProcured { get; private set; } = false;
        /// <summary>
        /// An open position is a trade that has been established, but which has not yet been closed out with an opposing trade.
        /// </summary>
        public bool IsOpen { get; internal set; } = true;
        /// <summary>
        /// An open position is a trade that has been established, but which has not yet been closed out with an opposing trade.
        /// </summary>
        public bool IsClosed => !IsOpen;
        #endregion

        #region Konstruktoren
        public Position( DateTime timeStamp, TimeSpan duration )
        {
            this.TimeStamp = timeStamp;
            this.Duration = duration;

            CalcPosition();
        }

        #endregion

        protected abstract void CalcPosition();

    }
}
