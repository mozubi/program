﻿using System;
using System.Collections.Generic;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Portfolios;

namespace winMoz.Trading.Portfolios
{
    public class AssetPosition : Position
    {
        public IAsset Asset { get; private set; }
        public List<ScheduleItem> Schedule => Asset.Schedule.Get(this.TimeStamp, this.Duration);

        public AssetPosition(IAsset asset, DateTime timeStamp, TimeSpan duration) : base(timeStamp, duration)
        {
            this.Asset = asset;
        }

        protected override void CalcPosition()
        {
            throw new NotImplementedException();
        }
    }
}
