﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using winMoz.Assets.Schedules;
using winMoz.Markets.Bids;
using winMoz.Trading.Portfolios;

namespace winMoz.Portfolios
{
    /// <summary>
    /// PortfolioBuch - Klasse - Enthält Elemente und Methoden zur strukturierten Bewirtschaftung von Positionen einer Portfoliostruktur
    /// </summary>
    public class PortfolioBook 
    {
        /// <summary>
        /// List aus Portfoliopositionen
        /// </summary>
        private ObservableCollection<Position> Positions;

        // TODO: make hedgerate timeseries
        public float HedgeRate { get; private set; }
        public (float Volume, float Price, float Total) Open { get; private set; }
        public (float Volume, float Price, float Total) Sales { get; private set; }
        public (float Volume, float Price, float Total) Procurement { get; private set; }

        public IEnumerable<TradePosition> Positions_Trade => Positions.Where(el => el.GetType() == typeof(TradePosition)).Cast<TradePosition>();
        public IEnumerable<AssetPosition> Positions_Asset => Positions.Where(el => el.GetType() == typeof(AssetPosition)).Cast<AssetPosition>();


        public PortfolioBook()
        {
            Positions = new ObservableCollection<Position>();
            Positions.CollectionChanged += CalcPortfolioValue;
        }

        internal void AddPosition(Position position) => this.Positions.Append(position);

        #region Methoden zur Berechnung der Portfoliokennzahlen
        private void CalcPortfolioValue(object sender, NotifyCollectionChangedEventArgs e)
        {
            CalcSales();
            CalcProcurement();
            CalcOpen();
            CalcHedgeRate();
        }

        private void CalcHedgeRate()
        {
            this.HedgeRate = (Procurement.Volume / Sales.Volume) * 100;
        }

        private void CalcOpen()
        {
            var openVolume = Sales.Volume - Procurement.Volume;
            var openPrice = Sales.Price - Procurement.Price;
            var openTotal = Sales.Total - Procurement.Total;

            this.Open = (openVolume, openPrice, openTotal);
        }

        private void CalcProcurement()
        {
            if (this.Positions?.Any(pos => pos.IsProcured) == true)
            {
                var total = this.Positions.Where(pos => pos.IsProcured).Select(el => el.PriceTotal).Sum();
                var price = this.Positions.Where(pos => pos.IsProcured).Select(el => el.Price).Average();
                var volume = this.Positions.Where(pos => pos.IsProcured).Select(el => el.Volume).Sum();
                this.Procurement = (volume, price, total);
            }
        }

        private void CalcSales()
        {
            if (this.Positions?.Any(pos => !pos.IsOpen && pos.IsProcured) == true)
            {
                var total = this.Positions.Where(pos => !pos.IsOpen && pos.IsProcured).Select(el => el.PriceTotal).Sum();
                var price = this.Positions.Where(pos => !pos.IsOpen && pos.IsProcured).Select(el => el.Price).Average();
                var volume = this.Positions.Where(pos => !pos.IsOpen && pos.IsProcured).Select(el => el.Volume).Sum();
                this.Sales = (volume, price, total);
            }
        }
        #endregion


    }
}
