﻿using System.Linq;
using winMoz.Markets.Bids;
using winMoz.Portfolios;

namespace winMoz.Trading.Portfolios
{
    public class TradePosition : Position
    {
        // a position in a futures contract does not reflect ownership but rather a binding commitment to buy or sell a given number of financial instruments, such as securities, currencies or commodities, for a given price
        public Bid Bid { get; private set; }

        public TradePosition(Bid bid) : base(bid.TimeStamp, bid.Duration)
        {
            this.Bid = bid;
        }

        protected override void CalcPosition()
        {
            this.Power = this.Bid.GetClearedTimeSeries().Average(el => el.VolumeMW);
        }
    }
}
