﻿using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Markets;

namespace winMoz.Portfolios
{
    /// <summary>
    /// Portfolio-Klasse enthält alle Eigenschaften, Objekte und Operationen für das Portfolio eines Agenten
    /// </summary>
    public class Portfolio 
    {
        #region Kraftwerkszahlen und -listen für Portfoliozusammensetzung
        /// <summary>
        /// Kraftwerksliste mit jew. Objekt-Instanzen
        /// </summary>
        public List<IAsset> Assets { get; private set; } = new List<IAsset>();
        public PortfolioBook Book { get; private set; } = new PortfolioBook();
        public void AddAsset(params IAsset[] asset) => this.Assets.AddRange(asset);
        #endregion

        #region Konstruktoren
        public Portfolio() { }
        public Portfolio(IEnumerable<IAsset> assets) { this.Assets = new List<IAsset>(assets); }
        public Portfolio(params IAsset[] assets) : this(assets.ToList()) { }
        #endregion
    }
}
