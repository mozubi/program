﻿//using Google.OrTools.Sat;
//using Google.OrTools.Util;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using winMoz.Assets.Plants;
//using winMoz.Helper;
//using winMoz.Information;
//using winMoz.Simulation;

//namespace winMoz.Portfolios.Optimization
//{
//    internal static class StorageOptimization
//    {
//        private static long[] GetStepRange(int idx, List<int> range)
//        {
//            List<long> values = new List<long>();

//            int startVal = idx;
//            if (idx > 0)
//                startVal = range[idx - 1];

//            for (int i = startVal; i <= range[idx]; i++)
//            {
//                values.Add(i);
//            }
//            return values.ToArray();
//        }

//        internal static List<(DateTime Timestamp, float Power, float Capacity)> SpotOptimization(Input input)
//        {
//            int qh = input.Capacity.Count + 1;

//            CpModel model = new CpModel();

//            #region Variables
//            IntVar[] P = new IntVar[qh];
//            IntVar[] E = new IntVar[qh];
//            IntVar[] Trade = new IntVar[qh];
//            IntVar[] ProdCost = new IntVar[qh];
//            IntVar[][] ETA = new IntVar[qh][];
//            ILiteral[][] ETARange = new ILiteral[qh][];

//            for (int t = 0; t < qh; t++)
//            {
//                P[t] = model.NewIntVar((long)-input.Plant.PowerMW, (long)input.Plant.PowerMW, $"P({t})");
//                Trade[t] = model.NewIntVar(-(long)input.Plant.PowerMW * 20000, (long)input.Plant.PowerMW * 20000, $"Trade{t}");
//                ProdCost[t] = model.NewIntVar(-1000000, 1000000, $"ProductionCosts({t})");
//                E[t] = model.NewIntVar(0, (long)input.Plant.Capacity * 4, $"Stored Energy({t})");
//                ETA[t] = new IntVar[input.P_Range.Count];
//                ETARange[t] = new ILiteral[input.P_Range.Count];

//                for (int j = 0; j < input.P_Range.Count; j++)
//                {
//                    ETA[t][j] = model.NewIntVar(0, 1, $"ETA[{t},{j}]");
//                    ETARange[t][j] = model.NewBoolVar($"ETARange[{t},{j}]");

//                    model.AddLinearExpressionInDomain(P[t],
//                        Domain.FromValues(GetStepRange(j, input.P_Range))).OnlyEnforceIf(ETARange[t][j]);
//                }
//            }
//            #endregion

//            #region Constraints
//            int lastSoC = (int)input.Plant.StateOfCharge.FirstOrDefault(el => el.TimeStamp == input.StartTime.AddMinutes(-15)).Level * 4;
//            if (lastSoC <= (int)input.Capacity[0].Min)
//                lastSoC = (int)input.Capacity[0].Min;
//            model.Add(E[0] == lastSoC);

//            var lastItem = input.Plant.Schedule.SingleOrDefault(el => el.TimeStamp == input.StartTime.AddMinutes(-15));
//            if (lastItem != null)
//                model.Add(P[0] == (int)Math.Round(lastItem.Power, 0));
//            else model.Add(P[0] == 0);

//            for (int t = 0; t < qh; t++)
//            {
//                if (t > 0)
//                {
//                    // Increase
//                    model.Add(P[t] >= -(int)input.Plant.P_Delta_QH);
//                    // Decrease
//                    model.Add(P[t] <= (int)input.Plant.P_Delta_QH);

//                    // Min&Max Power
//                    model.Add(E[t] == E[t - 1] + P[t]);
//                    model.Add(E[t] >= (int)input.Capacity[0].Min);
//                    model.Add(E[t] <= (int)input.Capacity[0].Max);

//                    // Speicher-Leistungsband
//                    //model.Add(P[t] <= E[t] - (int)input.Capacity[t+1].Max);
//                    //model.Add(P[t] >= (int)input.Capacity[t+1].Min - E[t]);

//                    model.Add(Trade[t] == -P[t] * (int)Math.Round(input.Prices[t - 1], 0));

//                    #region Dyn. Wirkungsgrad
//                    model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select ETA[t][n]).ToArray()) == 1);

//                    for (int j = 0; j < input.P_Range.Count; j++)
//                    {
//                        model.Add(ETA[t][j] == 1).OnlyEnforceIf(ETARange[t][j]);
//                        model.Add(ETA[t][j] == 0).OnlyEnforceIf(ETARange[t][j].Not());
//                    }

//                    model.Add(ProdCost[t] == LinearExpr.Sum(from n in Enumerable.Range(0, input.P_Range.Count) select LinearExpr.Prod(ETA[t][n], input.CostCurve[n])));
//                    #endregion
//                }
//            }
//            #endregion

//            #region Zielfunktion
//            var revenue = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select Trade[t]);
//            var costs = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select ProdCost[t]);

//            model.Maximize(revenue - costs);
//            #endregion

//            #region Solver
//            CpSolver solver = new CpSolver();
//            solver.StringParameters = $"max_time_in_seconds:{5}";
//            CpSolverStatus result = solver.Solve(model);
//            #endregion

//            var output = new List<(DateTime Timestamp, float Power, float Capacity, float Revenue, float Costs)>();

//            for (int t = 1; t < qh; t++)
//                output.Add((input.StartTime.AddMinutes(15 * (t - 1)), solver.Value(P[t]), solver.Value(E[t]), solver.Value(Trade[t]), solver.Value(ProdCost[t])));


//            GC.KeepAlive(result);
//            GC.KeepAlive(P);
//            GC.KeepAlive(E);
//            GC.KeepAlive(Trade);
//            GC.KeepAlive(ProdCost);
//            GC.KeepAlive(ETA);
//            GC.KeepAlive(ETARange);
//            GC.KeepAlive(revenue);
//            GC.KeepAlive(costs);
//            GC.KeepAlive(solver);
//            GC.KeepAlive(model);
//            GC.Collect();
//            return output.Select(el => (el.Timestamp, el.Power, el.Capacity)).ToList();
//        }

//        internal class Input
//        {
//            public Plant_Storage Plant { get; private set; }
//            public List<(float Min, float Max)> Capacity { get; private set; } = new List<(float Min, float Max)>();
//            public List<(float Min, float Max)> PowerLimit { get; private set; } = new List<(float Min, float Max)>();
//            public List<float> Prices { get; private set; } = new List<float>();
//            public DateTime StartTime { get; private set; }
//            public TimeSpan Duration { get; private set; }
//            public List<long> CostCurve { get; internal set; } = new List<long>();
//            public List<int> P_Range { get; private set; } = new List<int>();

//            public static Input Create(Plant_Storage plant, DateTime startTime, TimeSpan duration, bool quarterHourly = true)
//            {
//                var inp = new Input();
//                inp.StartTime = startTime;
//                inp.Duration = duration;
//                inp.Plant = plant;
//                inp.Prices.AddRange(Info.HPFC.Get(startTime, seedable: plant.BalancingGroup).ToList().Resample( Helper_TimeSeries.TimeStep.QuarterHour).Select(el => el.Item2).ToList());
//                inp.P_Range = ToInt(plant.Efficiency.GetPowerRange(plant), 1);

//                for (int i = 1; i < inp.P_Range.Count; i++)
//                    inp.CostCurve.Add((long)Math.Round(inp.P_Range[i] * (plant.Economics.LCOE * 0.25 / plant.Efficiency.Curve[i]), 0));
//                inp.CostCurve = inp.CostCurve.Prepend((long)inp.CostCurve.Average()).ToList();

//                var negPrange = new List<int>(inp.P_Range.Skip(1).Select(el => -el));
//                negPrange.Reverse();

//                for (int i = negPrange.Count - 1; i >= 0; i--)
//                    inp.P_Range = inp.P_Range.Prepend(negPrange[i]).ToList();

//                var dupl = new List<long>(inp.CostCurve.Skip(1));
//                dupl.Reverse();

//                for (int i = dupl.Count - 1; i >= 0; i--)
//                    inp.CostCurve = inp.CostCurve.Prepend(dupl[i]).ToList();

//                #region Dynamisches Speicherband
//                int qh = 4;
//                if (!quarterHourly) qh = 1;

//                for (DateTime t = startTime; t < startTime + duration; t = t.AddMinutes(15))
//                {
//                    inp.Capacity.Add((plant.MinSoC * qh, plant.MaxSoC * qh));
//                    inp.PowerLimit.Add((-plant.PowerMW, plant.PowerMW));
//                }

//                // Speicherband
//                //var totalQh = 48.0;
//                //for (int i = 0; i < inp.Capacity.Count; i++)
//                //{
//                //    float factor;
//                //    if (i < 48)
//                //        factor = 1 - (i / totalQh);
//                //    else
//                //        factor = 1 - ((inp.Capacity.Count - i) / totalQh);

//                //    inp.Capacity[i] = (inp.Capacity[i].Min, Math.Min(inp.Capacity[i].Min +
//                //        (plant.Capacity * factor) * 4 * 0.9, plant.Capacity * qh));
//                //}
//                #endregion

//                return inp;
//            }

//            private static List<int> ToInt(IEnumerable<float> floatListIn, float multiplier = 1)
//            {
//                return floatListIn.Select(el => (int)Math.Round(el * multiplier, 0)).ToList();
//            }

//        }
//    }
//}

