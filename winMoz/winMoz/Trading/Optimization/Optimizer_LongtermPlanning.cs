﻿using Google.OrTools.LinearSolver;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets.Plants;
using winMoz.Assets.Schedules;
using winMoz.Helper;

namespace winMoz.Portfolios.Optimization
{
    internal static class LongtermPlanning
    {
        internal static List<(DateTime TimeStamp, int[] Revision)> PlanYearlyRevision(Plant_Thermal plant, DateTime startTime)
        {
            TimeSpan duration = new DateTime(startTime.Year + 1, 1, 1, 0, 0, 0) - startTime;

            var thermalDemand = new List<(DateTime TimeStamp, float PowerPlanned)>();
            var cleanSpread = plant.CleanSpread(plant, startTime, duration, hourly: true).Resample(Resampling.TimeStep.Week);

            if (plant.TLP != null)
                thermalDemand = plant.TLP.GetOrSetSchedule(startTime, startTime.AddDays(365) - startTime).Select(el => (el.TimeStamp,el.Power)).ToList().Resample(Resampling.TimeStep.Week);
            else
                for (DateTime t = startTime; t.CompareTo(startTime.AddDays(365)) < 0; t = t.AddDays(7))
                    thermalDemand.Add((t, 0));

            Solver solver = new Solver("LP", Solver.OptimizationProblemType.CBC_MIXED_INTEGER_PROGRAMMING);

            // Integer-Matrix der thermischen Leistungswerte der Kraftwerksblöcke
            var P = solver.MakeIntVarMatrix(cleanSpread.Count, plant.Units.Count, 0, plant.Units.OfType<Block_CHP>().Select(unt => unt.P_th_Kwk).Sum());
            var commit = solver.MakeBoolVarMatrix(cleanSpread.Count, plant.Units.Count, "commit");
            var revenue = solver.MakeIntVarArray(cleanSpread.Count, long.MinValue, long.MaxValue);

            // Bool-Matrix der Start-Variablen (0 wenn Kraftwerk zum Zeitpunkt gestoppt wird, 1 wenn gestartet wird)
            var sUp = solver.MakeBoolVarMatrix(cleanSpread.Count, plant.Units.Count, "Startup");
            // Bool-Matrix der Stop-Variablen (1 wenn Kraftwerk zum Zeitpunkt gestoppt wird, 0 wenn gestartet wird)
            var sDown = solver.MakeBoolVarMatrix(cleanSpread.Count, plant.Units.Count, "Shutdown");

            for (int t = 1; t < cleanSpread.Count; t++)
            {
                foreach (Block_CHP b in plant.Units.OfType<Block_CHP>())
                {
                    // STARTUP-SHUTDOWN-COMMITMENT-CONSTRAINT
                    // - Startup-Variable sUp[t] wird 1, wenn Kraftwerk gestartet wird (u[t]-u[t-1] = 1); sDown[t] muss dann 0 sein
                    // - umgekehrt für Herunterfahren des Kraftwerks Shutdown-Variable sDown
                    solver.Add(commit[t, b.Id] - commit[t - 1, b.Id] == sUp[t, b.Id] - sDown[t, b.Id]);

                    // Restriktion sorgt dafür, dass Kraftwerk nicht im gleichen Zeitschritt gestoppt und gestartet werden können
                    solver.Add(sUp[t, b.Id] + sDown[t, b.Id] <= 1);

                    // Leistungs-Constraint
                    // - maximale Leistung des Kraftwerks wird, wenn es im Zeitschritt commited ist, auf maximale thermische (Block-)Kraftwerksleistung beschränkt
                    solver.Add(P[t, b.Id] <= commit[t, b.Id] * b.P_th_Kwk);
                    // - minimale Leistung des Kraftwerks wird, wenn es im Zeitschritt commited ist, auf die minimale (Block-)Kraftwerksleistung beschränkt
                    solver.Add(P[t, b.Id] >= commit[t, b.Id] * b.P_th_min);
                }

                solver.Add((from b in Enumerable.Range(0, plant.Units.Count) select P[t, b]).ToArray().Sum() >= (int)(thermalDemand[t].Item2));
                solver.Add(revenue[t] == (from b in Enumerable.Range(0, plant.Units.Count) select P[t, b]).ToArray().Sum() * (int)Math.Round(cleanSpread[t].Item2));
            }

            foreach (Block b in plant.Units)
            {
                solver.Add((from t in Enumerable.Range(1, cleanSpread.Count - 1)
                            select commit[t, b.Id]).ToArray().Sum() == 48);
            }

            var totalRevenue = (from t in Enumerable.Range(1, cleanSpread.Count - 1)
                                select revenue[t]).ToArray().Sum();

            var objective = solver.Objective();

            objective.SetOptimizationDirection(true);

            solver.Maximize(totalRevenue);
            solver.SetTimeLimit(1000);
            var result = solver.Solve();

            #region Printing
            //int x = P.GetLength(1);
            //int y = P.GetLength(0);

            //for (int i = 0; i < y; i++)
            //{
            //    string str = $"Hour:{i} \t";
            //    for (int j = 0; j < x; j++)
            //    {
            //        str += $"{P[i, j].SolutionValue()} | ";
            //    }

            //    str += " \t ";

            //    for (int j = 0; j < x; j++)
            //    {
            //        str += $"{commit[i, j].SolutionValue()} | ";
            //    }

            //    Console.WriteLine(str);
            //}
            #endregion

            var revisionSchedule = new List<(DateTime TimeStamp, int[] Revision)>();

            for (int t = 0; t < cleanSpread.Count; t++)
            {
                var revision = new int[plant.Units.Count];
                foreach (Block block in plant.Units)
                {
                    revision[block.Id] = 1 - (int)commit[t, block.Id].SolutionValue();
                }

                revisionSchedule.Add((cleanSpread[t].TimeStamp, revision));
            }

            List<(DateTime TimeStamp, List<int> Revision)> revisionsQh = new List<(DateTime TimeStamp, List<int> Revision)>();
            int unitCount = revisionSchedule.Select(el => el.Revision).First().Length;


            for (int i = 0; i < unitCount; i++)
            {
                var revision = revisionSchedule.Select(el => (el.TimeStamp, (float)el.Revision[i])).ToList().Resample( Resampling.TimeStep.QuarterHour).ToList();

                if (i == 0)
                    revision.ForEach(el =>
                    {
                        revisionsQh.Add((el.TimeStamp, new List<int>(2)));
                        revisionsQh.Last().Revision.Add((int)el.Item2);
                    });

                if (i > 0)
                    for (int j = 0; j < revision.Count; j++)
                    {
                        revisionsQh[j].Revision.Add((int)revision[j].Item2);
                    }
            }

            return revisionsQh.Select(el => (el.TimeStamp, el.Revision.ToArray())).ToList();
        }

        internal static void MonthlyThermal(Plant_Thermal plant, DateTime startTime, TimeSpan duration)
        {
            int idxOffset = 0;

            int timeSplit = 1;
            //int timeSplit = duration.Days/2;
            TimeSpan durationSplit = new TimeSpan(duration.Ticks / timeSplit);
            DateTime startTimeSplit = new DateTime();
            startTimeSplit = startTime;

            for (int t = 0; t < timeSplit; t++)
            {
                bool solutionFoundMILP = true;
                bool solutionFoundCP = true;

                if (t == timeSplit - 1)
                    durationSplit = startTime.AddMonths(1) - startTimeSplit;

                //solutionFoundMILP = Optimizer.MonthlyThermal.MILP(plant, startTimeSplit, durationSplit, idxOffset);

                if (!solutionFoundMILP)
                    //solutionFoundCP = RunOptimizer.MonthlyThermal.CPSAT(idxOffset, plant, startTimeSplit, durationSplit, ref thermalDemand, priceEUA, priceFuel);

                    idxOffset += (int)durationSplit.TotalHours;
                startTimeSplit = startTime.AddHours(idxOffset);
            }

            foreach (var unit in plant.Units)
            {
                // TODO: Use resampling helper
                //unit.Schedule.Hour2QuarterHour(startTime);

                if (!unit.Schedule.IsRegularQh())
                    throw new Exception("Error Irregular Schedules");
            }
        }
    }
}
