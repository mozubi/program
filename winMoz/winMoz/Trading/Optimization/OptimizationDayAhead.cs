﻿using Google.OrTools.Sat;
using Google.OrTools.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets.Plants;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Simulation;
using winMoz.Information.PFCs;

namespace winMoz.Portfolios.Optimization
{
    internal static class Thermal_DA
    {
        internal static void DayAhead(Plant_Thermal plant, DateTime startTime, TimeSpan duration, TimeSpan stepDuration, int scaleFactor = 1000)
        {
            if (duration.Days == 1)
            {
                var input = new Input(plant, startTime, duration, stepDuration, scaleFactor: scaleFactor);

                var results = OptimizeMakeOrBuy(input);
                WritePlantAndUnitSchedules(plant, results);

                // 2nd pass - MakeMore?
                input.Update(results);
                results = OptimizeMakeMore(input);

                WritePlantAndUnitSchedules(plant, results);
            }
        }


        #region Optimization

        private static List<(DateTime Time, List<float> UnitPowerDAOpt, float Price)> OptimizeMakeMore(Input input)
        {
            var unitSchedule = input.UnitSchedule.Select(el => el.Select(it => it / input.ScaleFactor).ToList()).ToList();
            var pfc = input.HPFC;
            var procured = input.PowerProcured.Select(el => el / input.ScaleFactor).ToList();

            var result = new List<(DateTime Time, List<float> UnitPowerDAOpt, float)>();

            for (int t = input.LeadTimeSpans; t < procured.Count - input.LeadTimeSpans; t++)
            {
                var temp = (input.Times[t], new List<float>(), 0.0F);

                if (unitSchedule[t].Sum() >= procured[t] && pfc[t] > 1.25 * input.Plant.Units[0].ProductionCostsPerMWh_el( input.Times[t], input.Times[1] - input.Times[0]).First().Item2 ) // input.Plant.Economics.LCOE)
                    for (int b = 0; b < unitSchedule[t].Count; b++)
                    {
                        var PowerMinus1 = unitSchedule[t - 1][b];
                        var PowerPlus1 = unitSchedule[t + 1][b];
                        var currentPower = unitSchedule[t][b];
                        if (currentPower <= (input.Plant.Units[b] as Block_CHP).P_th_Kwk)
                            temp.Item2.Add(Math.Min(Math.Min((int)(PowerPlus1 + input.Plant.Units.OfType<Block_CHP>().ToList()[b].P_th_DeltaQh * input.RampScaling), Math.Min((int)(PowerMinus1 + input.Plant.Units.OfType<Block_CHP>().ToList()[b].P_th_DeltaQh * input.RampScaling), (int)(currentPower + input.Plant.Units.OfType<Block_CHP>().ToList()[b].P_th_DeltaQh * input.RampScaling))), input.Plant.Units.OfType<Block_CHP>().ToList()[b].P_th_Kwk - input.ReservedPositivePowerPerUnit[t][b] / 1000));
                    }
                else
                    temp.Item2 = unitSchedule[t].Select(el => (float)el).ToList();

                int ProdCostProcured = 0;
                int ProdCost = 0;
                for (int i = 0; i < input.P_Range.Count(); i++)
                {
                    if (input.P_Range[i] <= input.PowerProcured[t] * input.ScaleFactor)
                    {
                        var ETAFactorBefore = input.ProductionCosts[t] / input.Plant.Units[0].ThermalData.Efficiency.Curve[i];
                        ProdCostProcured = (int)(input.PowerProcured[t] * ETAFactorBefore);
                    }
                    if (input.P_Range[i] <= temp.Item2.Sum() * input.ScaleFactor)
                    {
                        var ETAFactorAfter = input.ProductionCosts[t] / input.Plant.Units[0].ThermalData.Efficiency.Curve[i];
                        ProdCost = (int)(temp.Item2.Sum() * input.ScaleFactor * ETAFactorAfter);
                    }
                }
                //ProdCostProcured = (int)(input.PowerProcured[t] * input.ProductionCosts[t] / input.Plant.Efficiency.Curve.Max());
                var PowerAfter = temp.Item2.Sum();
                var Price = temp.Item2.Sum() * input.ScaleFactor - (input.PowerProcured[t]) != 0 ? (ProdCost - ProdCostProcured) / (temp.Item2.Sum() * input.ScaleFactor - input.PowerProcured[t]) : 0;
                temp.Item3 = Price;
                result.Add(temp);
            }

            return result;
        }


        private static List<(DateTime Time, List<float> UnitPowerDAOpt, float Price)> OptimizeMakeOrBuy(Input input, bool forControlReserve = false)
        {
            bool print = true;
            //int scaleFactor = 1000;

            #region Preparation of Constraint-Data
            int unitCount = input.Plant.Units.Count;
            int plantTotalPower = (input.Plant.Units.OfType<Block_CHP>().Select(el => (int) el.P_th_Kwk).Sum()) * input.ScaleFactor;
            List<int> unitPowerNet = input.Plant.Units.OfType<Block_CHP>().Select(el => (int) el.P_th_Kwk * input.ScaleFactor).ToList();
            List<int> unitDeltaPower = input.Plant.Units.OfType<Block_CHP>().Select(el => (int)(el.P_th_DeltaQh * input.RampScaling * input.ScaleFactor)).ToList();
            //List<int> unitMinPower = input.Plant.Units.Select(el => el.P_th_Min * scaleFactor).ToList();
            List<int> unitMinPower = input.Plant.Units.OfType<Block_CHP>().Select(el => (int)(el.P_th_Kwk * input.ScaleFactor * el.ThermalData.MinimumStableGeneration)).ToList();
            //int minUpTime = (int)Math.Round(input.Plant.ThermalData.Times.Min_Uptime.TotalHours, 0);
            //int minDownTime = (int)Math.Round(input.Plant.ThermalData.Times.Min_Downtime.TotalHours, 0);
            int minUpTime = (int)Math.Ceiling(11 * (TimeSpan.FromHours(1).TotalHours / input.StepDuration.TotalHours));
            int minDownTime = (int)Math.Ceiling(11 * (TimeSpan.FromHours(1).TotalHours / input.StepDuration.TotalHours));
            #endregion

            #region Variables
            CpModel model = new CpModel();

            IntVar[][] P = new IntVar[unitCount][];
            IntVar[][] u = new IntVar[unitCount][];
            IntVar[][] ETAFactor = new IntVar[unitCount][];
            IntVar[][] ProdCost = new IntVar[unitCount][];
            IntVar[] Trade = new IntVar[input.TimeSpanNumber]; // Gehandelte Menge in KW (- Verkauf, + Zukauf)
            IntVar[][] sUp = new IntVar[unitCount][];
            IntVar[][] sDown = new IntVar[unitCount][];
            IntVar[][] pR = new IntVar[unitCount][];
            IntVar[][][] ETA = new IntVar[unitCount][][];
            ILiteral[][][] ETAsel = new ILiteral[unitCount][][];



            for (int b = 0; b < unitCount; b++)
            {
                P[b] = new IntVar[input.TimeSpanNumber];
                ETAFactor[b] = new IntVar[input.TimeSpanNumber];
                ProdCost[b] = new IntVar[input.TimeSpanNumber];
                u[b] = new IntVar[input.TimeSpanNumber];
                sUp[b] = new IntVar[input.TimeSpanNumber];
                sDown[b] = new IntVar[input.TimeSpanNumber];
                pR[b] = new IntVar[input.TimeSpanNumber];

                ETA[b] = new IntVar[input.TimeSpanNumber][];
                ETAsel[b] = new ILiteral[input.TimeSpanNumber][];

            }

            for (int t = 0; t < input.TimeSpanNumber; t++)
            {
                for (int b = 0; b < unitCount; b++)
                {
                    P[b][t] = model.NewIntVar(0, plantTotalPower, $"P(b{b},t{t})");
                    ETAFactor[b][t] = model.NewIntVar(0, 999 * input.ScaleFactor, $"ETAFactor(b{b},t{t})");
                    ProdCost[b][t] = model.NewIntVar(0, 100000 * input.ScaleFactor, $"P(b{b},t{t})");
                    u[b][t] = model.NewIntVar(0, 1, $"UnitCommitment({b},{t})");

                    sUp[b][t] = model.NewIntVar(0, 1, $"Startup(b{b},t{t})");
                    sDown[b][t] = model.NewIntVar(0, 1, $"Shutdown(b{b},t{t})");
                    pR[b][t] = model.NewIntVar(0, 1, $"IsPowerReserved(b{b},t{t})");

                    ETA[b][t] = new IntVar[input.P_Range.Count];
                    ETAsel[b][t] = new ILiteral[input.P_Range.Count];

                    for (int j = 0; j < input.P_Range.Count; j++)
                    {
                        ETA[b][t][j] = model.NewIntVar(0, 1, $"ETA[{b},{t},{j}]");
                        ETAsel[b][t][j] = model.NewBoolVar($"ETAsel[{b},{t},{j}]");

                        model.AddLinearExpressionInDomain(P[b][t],
                            Domain.FromValues(GetStepRange(j, input.P_Range))).OnlyEnforceIf(ETAsel[b][t][j]);
                    }
                }
                Trade[t] = model.NewIntVar(-plantTotalPower, plantTotalPower, $"Trade{t}");
            }
            #endregion

            #region Constraints
            for (int b = 0; b < unitCount; b++)
            {
                for (int t = input.LeadTimeSpans; t < input.TimeSpanNumber; t++)
                {
                    model.Add(P[b][t] <= unitPowerNet[b] - input.ReservedPositivePowerPerUnit[t][b]); // Maximale Blockleistung
                    model.Add(P[b][t] >= u[b][t] * (int)(unitPowerNet[b] * input.Plant.Units[0].ThermalData.MinimumStableGeneration) + pR[b][t] * input.ReservedNegativePowerPerUnit[t][b]); // Minimale Blockleistung

                    model.Add(u[b][t] >= pR[b][t]);

                    if (t > 0)
                    {
                        // Increase
                        model.Add(P[b][t] - P[b][t - 1] <= unitDeltaPower[b] + sUp[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
                        // Decrease
                        model.Add(P[b][t - 1] - P[b][t] <= unitDeltaPower[b] + sDown[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
                        
                        //Damit die Regelenergie nicht die Hoch- und Herunterfahrkapazitäten übersteigt. 
                        if (input.IsPowerReserved[t][b] == 1)
                        {
                            // Increase (schedule schould be in Borders with and without reserved power)
                            model.Add(P[b][t] + input.ReservedPositivePowerPerUnit[t][b] - input.ReservedNegativePowerPerUnit[t][b] - P[b][t - 1] <= unitDeltaPower[b] + sUp[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
                            model.Add(P[b][t] - (P[b][t - 1] + input.ReservedPositivePowerPerUnit[t - 1][b] - input.ReservedNegativePowerPerUnit[t - 1][b]) <= unitDeltaPower[b] + sUp[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
                            // Decrease (schedule schould be in Borders with and without reserved power)
                            model.Add(P[b][t - 1] - (P[b][t] + input.ReservedPositivePowerPerUnit[t][b] - input.ReservedNegativePowerPerUnit[t][b]) <= unitDeltaPower[b] + sDown[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
                            model.Add(P[b][t - 1] + input.ReservedPositivePowerPerUnit[t - 1][b] - input.ReservedNegativePowerPerUnit[t - 1][b] - P[b][t] <= unitDeltaPower[b] + sDown[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
                        }
                    }

                    #region STARTUP-SHUTDOWN-COMMITMENT-CONSTRAINT

                    // - Startup-Variable sUp[t] wird 1, wenn Kraftwerk gestartet wird (u[t]-u[t-1] = 1); sDown[t] muss dann 0 sein
                    // - umgekehrt für Herunterfahren des Kraftwerks Shutdown-Variable sDown
                    model.Add(u[b][t] - u[b][t - 1] == sUp[b][t] - sDown[b][t]);

                    // Restriktion sorgt dafür, dass Kraftwerk nicht im gleichen Zeitschritt gestoppt und gestartet werden können
                    model.Add(sUp[b][t] + sDown[b][t] <= 1);
                    #endregion

                    #region Min Up&-Downtime
                    model.Add(sUp[b][t] + LinearExpr.Sum((from n in Enumerable.Range(1, minUpTime) select sDown[b][t - n]).ToArray()) <= 1);
                    model.Add(sDown[b][t] + LinearExpr.Sum((from n in Enumerable.Range(1, minDownTime) select sUp[b][t - n]).ToArray()) <= 1);
                    #endregion

                    model.Add(pR[b][t] == input.IsPowerReserved[t][b]);

                    #region Dyn. Wirkungsgrad
                    model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select ETA[b][t][n]).ToArray()) == 1);

                    for (int j = 0; j < input.P_Range.Count; j++)
                    {
                        model.Add(ETA[b][t][j] == 1).OnlyEnforceIf(ETAsel[b][t][j]);
                        model.Add(ETA[b][t][j] == 0).OnlyEnforceIf(ETAsel[b][t][j].Not());
                    }

                    model.Add(ETAFactor[b][t] == LinearExpr.Sum(from n in Enumerable.Range(0, input.P_Range.Count)
                                                                select LinearExpr.Prod(ETA[b][t][n], (long)(input.ProductionCosts[t] / input.Plant.Units[0].ThermalData.Efficiency.Curve[n]))));

                    model.AddProdEquality(ProdCost[b][t], new List<IntVar>() { ETAFactor[b][t], P[b][t] });
                    #endregion

                    #region Demand/Sales-Constraints
                    model.Add(Trade[t] == input.PowerProcured[t] - LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select P[n][t]).ToArray()));
                    #endregion
                }
            }

            for (int t = input.LeadTimeSpans; t < input.TimeSpanNumber; t++)
            {
                model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select P[n][t]).ToArray()) <= input.PowerProcured[t]);
            }

            for (int t = input.LeadTimeSpans; t < input.TimeSpanNumber; t++)
            {
                model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select P[n][t]).ToArray()) >= input.Demand[t]);
            }

            #region Anfangsbedingungen
            for (int t = 0; t < input.LeadTimeSpans; t++)
            {
                for (int b = 0; b < unitCount; b++)
                {
                    model.Add(u[b][t] == input.u[t][b]);
                    model.Add(sUp[b][t] == input.sUp[t][b]);
                    model.Add(sDown[b][t] == input.sDown[t][b]);

                    model.Add(P[b][t] == input.UnitSchedule[t][b]);
                    model.Add(Trade[t] == 0);
                    #endregion
                }
            }
            #endregion

            #region Zielfunktion
            var costsFromProduction = LinearExpr.Sum(from t in Enumerable.Range(input.LeadTimeSpans - 1, input.TimeSpanNumber - input.LeadTimeSpans)
                                                     select LinearExpr.Sum(from b in Enumerable.Range(0, unitCount) select ProdCost[b][t]));

            var revenue = LinearExpr.Sum((from t in Enumerable.Range(input.LeadTimeSpans - 1, input.TimeSpanNumber - input.LeadTimeSpans)
                                          select (input.HPFC[t] * Trade[t])).ToArray());

            var totalRevenue = revenue - costsFromProduction;

            model.Maximize(totalRevenue);
            #endregion

            CpSolver solver = new CpSolver();
            solver.StringParameters = $"num_search_workers:{Environment.ProcessorCount - 2}";

            solver.StringParameters += $" max_time_in_seconds:{5}";

            SolutionCallbackNew SC = new SolutionCallbackNew();

            CpSolverStatus result = solver.SolveWithSolutionCallback(model, SC);
            var response = solver.Response;
            var numIntegerPropagation = response.NumIntegerPropagations;
            var size = response.CalculateSize();
            var info = response.SolutionInfo;

            var deterministicTime = response.DeterministicTime;

            GC.KeepAlive(result);
            GC.KeepAlive(P);
            GC.KeepAlive(u);
            GC.KeepAlive(Trade);
            GC.KeepAlive(ProdCost);
            GC.KeepAlive(ETA);
            GC.KeepAlive(ETAFactor);
            GC.KeepAlive(ETAsel);
            GC.KeepAlive(revenue);
            GC.KeepAlive(costsFromProduction);
            GC.KeepAlive(ProdCost);
            GC.KeepAlive(solver);
            GC.KeepAlive(Trade);
            GC.KeepAlive(sUp);
            GC.KeepAlive(sDown);
            GC.KeepAlive(pR);
            GC.KeepAlive(model);

            #region Printing
            if (print)
            {
                if (result != CpSolverStatus.Infeasible)
                    PrintResults(input, unitCount, Trade, P, ProdCost, totalRevenue, solver, result);
                else
                {
                    var Results = new List<(DateTime, List<float>, float)>();
                    for (int i = input.LeadTimeSpans; i < input.UnitSchedule.Count() - input.LeadTimeSpans; i++)
                    {
                        //Console.WriteLine(string.Join(";", input.UnitSchedule[i].Select(item => item.ToString()).ToList()));
                        Results.Add((input.Times[i], input.UnitSchedule[i].Select(item => (float)item / input.ScaleFactor).ToList(), 0.0F));
                    }
                    return Results;
                }

            }

            #endregion

            List<int> ProdCostProcured = new List<int>();
            for (int i = 0; i < input.PowerProcured.Count(); i++)
            {
                ProdCostProcured.Add((int)(input.PowerProcured[i] * input.ProductionCosts[i] / input.Plant.Units[0].ThermalData.Efficiency.Curve.Max()));
            }
            return GetResults(solver, Trade, P, input.LeadTimeSpans, input.TimeSpanNumber, unitCount, input.Times, ProdCost, ProdCostProcured, input.ScaleFactor);
        }

        /// <summary>
        /// Funktion zur Steuerung des Ausgabewertes der Optimierung.
        /// Um die Zeit zu verkürzen wird die erste Lösung, die als Maximum gekenntzeitet ist genommen.
        /// </summary>
        public class SolutionCallbackNew : CpSolverSolutionCallback
        {
            public SolutionCallbackNew() : base() { count = 0; }
            int count;
            public override void OnSolutionCallback()
            {
                count++;
                //Console.WriteLine(this.Response().SolutionInfo);
                if (count > 100 || this.Response().SolutionInfo == "max_lp")
                    StopSearch();

            }

            public bool IsCountGreaterThan(int value) => count > value;
        }
        #endregion

        #region Result-Handling
        private static List<(DateTime Time, List<float> UnitPowerDAOpt, float Price)> GetResults(CpSolver solver, IntVar[] Trade, IntVar[][] P, int leadTimeSpans, int hourDuration, int unitCount, List<DateTime> times, IntVar[][] prodCost, List<int> prodCostProcured, int scaleFactor)
        {
            var optOutput = new List<(DateTime timeStamp, List<float> blockPower, float Price)>();

            for (int t = leadTimeSpans; t < hourDuration - leadTimeSpans; t++)
            {
                var blockPower = new List<float>();
                float ProdCost = 0;
                for (int b = 0; b < unitCount; b++)
                {
                    blockPower.Add((float)Math.Truncate(solver.Value(P[b][t]) / (float)scaleFactor));
                    ProdCost += solver.Value(prodCost[b][t]);
                }
                var Trade1 = solver.Value(Trade[t]);
                var Price = solver.Value(Trade[t]) != 0 ? (prodCostProcured[t] - ProdCost) / (float)solver.Value(Trade[t]) : 0;
                optOutput.Add((times[t], blockPower, Price));
            }


            return optOutput;
        }

        private static void PrintResults(Input input, int unitCount, IntVar[] Trade, IntVar[][] P, IntVar[][] ProdCost, LinearExpr totalRevenue, CpSolver solver, CpSolverStatus result)
        {
            Console.WriteLine("====================================");
            Console.WriteLine("CPSAT-Solver Result: " + result.ToString());
            Console.WriteLine("Number of Branches: " + solver.NumBranches());
            Console.WriteLine("Optimal objective value :" + solver.Value(totalRevenue));
            Console.WriteLine("Time: " + solver.WallTime() * 1000 + "ms");
            Console.WriteLine("====================================");

            string hourFmt = "00.##";
            string valFmt = "000.##";
            for (int i = 0; i < input.TimeSpanNumber; i++)
            {
                Console.Write($"Hour {i.ToString(hourFmt)} \t");
                //Console.Write($"D {demand[i].ToString(valFmt)} \t");


                for (int b = 0; b < unitCount; b++)
                {
                    Console.Write(String.Concat("P", ($"[{i.ToString("00.##")},{b}] {solver.Value(P[b][i]).ToString(valFmt)} ")));
                    Console.Write($"CO {solver.Value(ProdCost[b][i]).ToString(valFmt)} ");
                }


                Console.Write("\t");
                if (i > input.LeadTimeSpans)
                {
                    Console.Write($"Proc {(input.CostsAndRevenuesProcured[i]).ToString(valFmt)} ");
                }
                else
                {
                    Console.Write($"Proc {000} ");
                }
                Console.Write(String.Concat("TRD", ($" {solver.Value(Trade[i]).ToString("000.##")} ")));
                Console.Write($"HPFC {input.HPFC[i].ToString("00.##")} ");

                Console.WriteLine();
            }
        }

        private static void WritePlantAndUnitSchedules(Plant_Thermal plant, List<(DateTime Time, List<float> UnitPowerDAOpt, float Price)> optimizationResult)
        {
            WriteUnitSchedules(plant, optimizationResult);

            var resultPowerAndPrice = optimizationResult.Select(el => (el.Time, el.UnitPowerDAOpt.Select(it => it).Sum(), el.Price)).ToList().Resample(Resampling.TimeStep.QuarterHour, method: Resampling.Method.FFill);
            var plantSchedule = plant.Schedule.Get(optimizationResult.First().Time, optimizationResult.Select(el => el.Time).ToList().GetDuration().Add(TimeSpan.FromHours(1)));// + new TimeSpan(1, 0, 0));

            for (int i = 0; i < plantSchedule.Count; i++)
                plantSchedule[i].Set(resultPowerAndPrice[i].Item2);

        }

        private static void WriteUnitSchedules(Plant_Thermal plant, List<(DateTime Time, List<float> UnitPowerDAOpt, float Price)> result)
        {
            // Resampling auf 15min
            for (int i = 0; i < result.First().UnitPowerDAOpt.Count; i++)
            {
                var schedule = result.Select(el => (el.Time, i < el.UnitPowerDAOpt.Count ? el.UnitPowerDAOpt[i] : 0)).ToList().Resample(Resampling.TimeStep.QuarterHour, method: Resampling.Method.FFill);//.SmoothLinear();
                plant.Units[i].Schedule.SetPower(schedule.Select(el => (el.TimeStamp, el.Item2)).ToList());
            }
        }

        private static long[] GetStepRange(int idx, List<int> range)
        {
            List<long> values = new List<long>();

            int startVal = idx;
            if (idx > 0)
                startVal = range[idx - 1];

            for (int i = startVal; i <= range[idx]; i++)
                values.Add(i);

            return values.ToArray();
        }
        #endregion

        private class Input
        {
            internal int ScaleFactor = 1000;

            internal DateTime StartTime;
            internal TimeSpan Duration;

            internal Plant_Thermal Plant;
            internal int TimeSpanNumber;
            internal int LeadTimeSpans;
            internal TimeSpan StepDuration;
            internal float RampScaling;

            internal List<DateTime> Times;
            internal List<int> HPFC;
            internal List<int> Demand;
            internal List<int> PowerProcured;
            internal List<int> CostsAndRevenuesProcured;
            //internal List<float> Price;
            internal List<int> ProductionCosts;
            internal List<int> P_Range;
            internal List<List<int>> UnitSchedule;
            internal List<List<int>> ReservedPositivePowerPerUnit;
            internal List<List<int>> ReservedNegativePowerPerUnit;
            internal List<List<int>> IsPowerReserved;
            internal List<List<int>> u; //Kraftwerkseiheitszustand (1 = an / 0 = aus)
            internal List<List<int>> sUp; //Hochfahrenzeiten für Kraftwerkseiheiten (1 = Hochfahren / 0 = nicht Hochfahren)
            internal List<List<int>> sDown; //Herunterfahrzeiten für Kraftwerkseiheiten (1 = Herunterfahren / 0 = nicht Herunterfahrenfahren)

            public Input(Plant_Thermal plant, DateTime startTime, TimeSpan duration, TimeSpan stepDuration, int leadHours = 12, TimeSpan rampDuration = default, int scaleFactor = 1000) //, List<List<int>> reservedPowerPerUnit = null)
            {
                this.Plant = plant;
                this.ScaleFactor = scaleFactor;
                this.StepDuration = stepDuration;
                this.LeadTimeSpans = (int)Math.Ceiling(leadHours * (TimeSpan.FromHours(1).TotalHours / stepDuration.TotalHours)) + 1;
                this.StartTime = startTime.AddHours(-LeadTimeSpans * stepDuration.TotalHours);
                this.Duration = duration.Add(TimeSpan.FromHours(2 * LeadTimeSpans * stepDuration.TotalHours));
                var EndOfDuration = this.StartTime.AddHours(this.Duration.TotalHours);
                this.Plant.Schedule.Extend(this.StartTime, this.Duration);
                this.Plant.Units.ForEach(unit => unit.Schedule.Extend(this.StartTime, this.Duration));

                var RampDuration = rampDuration != default ? rampDuration : TimeSpan.FromMinutes(15);
                this.RampScaling = (float)(RampDuration.TotalHours / TimeSpan.FromMinutes(15).TotalHours);

                this.TimeSpanNumber = (int)Math.Round(this.Duration.TotalHours / stepDuration.TotalHours, 0);

                this.PowerProcured = ToInt(
                    plant.Schedule.Get(this.StartTime, this.Duration)
                                  .Select(el => (el.TimeStamp, Math.Abs(el.PowerProcured))).ToList()
                                  .Resample(Resampling.GetTimeStep(stepDuration))
                                  .Select(el => el.Item2), ScaleFactor).ToList();

                //this.Price = plant.Schedule.Get(this.StartTime, this.Duration).Select(el => (el.TimeStamp, el.PriceProcured * ScaleFactor)).ToList().Resample(Resampling.GetTimeStep(stepDuration)).Select(el => el.Item2).ToList();

                this.UnitSchedule = plant.Units.Select(unit => unit.Schedule.Get(this.StartTime, this.Duration))
                    .Select(el => el.Select(EL => (EL.TimeStamp, EL.Power)).ToList().Resample(Resampling.GetTimeStep(stepDuration))
                    .Select(it => (int)Math.Round(it.Item2, 0) * ScaleFactor).ToList())
                    .SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();

                this.Demand = plant.TLP != null ? ToInt(plant.TLP.GetDemand(this.StartTime, this.Duration).Select(el => (el.timeStamp, el.demand)).ToList().Resample(Resampling.GetTimeStep(stepDuration)).Select(el => Math.Min(el.Item2, plant.Units.OfType<Block_CHP>().Select(unt => unt.P_th_Kwk).Sum())).ToList(), ScaleFactor) : this.PowerProcured.Select(el => 0).ToList();

                this.ProductionCosts = ToInt(plant.Units[0].ProductionCostsPerMWh_el( this.StartTime, this.Duration).Select(el => el.Item2)).ToList();
                this.CostsAndRevenuesProcured = ToInt(plant.Schedule.Get(this.StartTime, this.Duration).Select(el => (el.TimeStamp, (el.Revenues - el.Costs) * ScaleFactor)).ToList().Resample(Resampling.GetTimeStep(stepDuration)).Select(el => el.Item2));


                this.HPFC = plant.BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, this.StartTime, this.Duration).Where(el => el.TimeStamp >= this.StartTime && el.TimeStamp < this.StartTime + this.Duration).ToList().Resample(Resampling.GetTimeStep(stepDuration)).ToList().Select(el => (int)Math.Round(el.Item2, 0)).ToList();

                this.Times = this.StartTime.GetTimeVector(this.Duration, step: stepDuration);
                this.P_Range = ToInt(plant.Units[0].ThermalData.Efficiency.GetPowerRange(plant.PowerMW), ScaleFactor);

                //Regelenergie nach positiver und negativer sortiert.
                this.ReservedPositivePowerPerUnit = plant.Units.Select(unit => unit.ControlReserveSchedule.Get(this.StartTime, this.Duration))
                    .Select(el => el.Select(EL => (EL.TimeStamp, Math.Max(EL.PowerReservedOrActivatedPositive, 0))).ToList().Resample(Resampling.GetTimeStep(stepDuration))
                    .Select(it => (int)Math.Round(it.Item2, 0) * ScaleFactor).ToList())
                    .SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();
                this.ReservedNegativePowerPerUnit = plant.Units.Select(unit => unit.ControlReserveSchedule.Get(this.StartTime, this.Duration))
                    .Select(el => el.Select(EL => (EL.TimeStamp, -Math.Min(EL.PowerReservedOrActivatedNegative, 0))).ToList().Resample(Resampling.GetTimeStep(stepDuration))
                    .Select(it => (int)Math.Round(it.Item2, 0) * ScaleFactor).ToList())
                    .SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();

                this.IsPowerReserved = plant.Units.Select(unit => unit.ControlReserveSchedule.Get(this.StartTime, this.Duration))
                     .Select(el => el.Select(EL => (EL.TimeStamp, EL.PowerReservedOrActivatedPositive, EL.PowerReservedOrActivatedNegative)).ToList().Resample(Resampling.GetTimeStep(stepDuration))
                     .Select(it => (it.Item2 != 0 || it.Item3 != 0) ? 1 : 0).ToList())
                    .SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();

                //Anschaltzustand "u" und Hoch- "sUp" und Herunterfahrfahrzeiten "sDown" der Kraftwerkseinheiten.
                this.u = plant.Units.Select(unit => unit.Schedule.Get(this.StartTime, this.Duration))
                    .Select(el => el.Select(EL => (EL.TimeStamp, EL.Power)).ToList().Resample(Resampling.GetTimeStep(stepDuration))
                    .Select(it => (int)Math.Round(it.Item2, 0) * ScaleFactor > 0 ? 1 : 0).ToList())
                    .SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();

                (this.sUp, this.sDown) = GetUpAndDownTimes(this.u);
            }

            public (List<List<int>> S_Up, List<List<int>> S_Down) GetUpAndDownTimes(List<List<int>> OnOffTimes)
            {
                //Verschiebe Liste der Kraftwerkanschaltzustände um jeweils Paare von t und (t - 1) zu haben, um An- und Ausschalten schnell kennzeichnen zu können.
                var u_tMinus1 = this.u.Prepend(this.u.First()).ToList();
                u_tMinus1.RemoveAt(u_tMinus1.Count() - 1);

                var SUP = this.u.Zip(u_tMinus1, (el_t, el_tMinus1) => el_t.Zip(el_tMinus1, (EL_t, EL_tMinus1) => EL_tMinus1 < EL_t ? 1 : 0).ToList()).ToList();

                var SDOWN = this.u.Zip(u_tMinus1, (el_t, el_tMinus1) => el_t.Zip(el_tMinus1, (EL_t, EL_tMinus1) => EL_tMinus1 > EL_t ? 1 : 0).ToList()).ToList();
                return (SUP, SDOWN);
            }

            public void Update(List<(DateTime Time, List<float> UnitPowerDAOpt, float Price)> results, TimeSpan newRampDuration = default)
            {
                var Results = results;
                var resultTimes = Results.Select(item => item.Time).ToList();
                for (int t = 0; t < this.Times.Count(); t++)
                {
                    if (resultTimes.Contains(this.Times[t]))
                        this.UnitSchedule[t] = Results.FirstOrDefault(item => item.Time == this.Times[t]).UnitPowerDAOpt.Select(item => (int)(item * ScaleFactor)).ToList();
                    //this.Price[t] = Results.FirstOrDefault(item => item.Time == this.Times[t]).Price;
                }
                SetRampDuration(newRampDuration);
            }

            public void SetRampDuration(TimeSpan newRampDuration = default)
            {
                var RampDuration = newRampDuration != default ? newRampDuration : TimeSpan.FromMinutes(15);
                this.RampScaling = (float)(RampDuration.TotalHours / TimeSpan.FromMinutes(15).TotalHours);
            }

            private static List<int> ToInt(IEnumerable<float> floatListIn, float multiplier = 1)
            {
                return floatListIn.Select(el => (int)Math.Round(el * multiplier, 0)).ToList();
            }
        }
    }
}
