﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets.Plants;
using Google.OrTools.Sat;
using Google.OrTools.Util;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Simulation;
using winMoz.Information.PFCs;

namespace winMoz.Trading.Optimization
{
    //public static class ThermalOptimization_ControlReserve
    //{
    //    public static void Optimize_ControlReserve_Capacity(this Plant_Thermal plant, DateTime startTime, TimeSpan duration)
    //    {
    //        if (duration.Days == 1)
    //        {
    //            var input = new Input(plant, startTime, duration);

    //            var results = OptimizeMakeOrBuyDA_CR(input);
    //            WritePlantAndUnitSchedules(plant, results);

    //            // 2nd pass - MakeMore?
    //            input.Update();
    //            results = OptimizeMakeMore(input);

    //            WritePlantAndUnitSchedules(plant, results);
    //        }
    //    }

    //    private static List<(DateTime Time, List<float> UnitPowerDAOpt)> Optimize_ControlReserve(Input input)
    //    {
    //        bool print = false;
    //        int scaleFactor = 1000;
    //        //TODO: Werte noch zu bestimmen.
    //        float q_base = 0.2;
    //        int costOfControlReserve = 2;
    //        int[] q_pos = new int[] {100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0 };
    //        int[] Ep_Intervall = new int[] { -9999, -1000, -100, -10, 0, 10, 100, 500, 1000, 5000, 9999 };
    //        int[] q_neg = new int[] { 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1 };

    //        #region Preparation of Constraint-Data
    //        int unitCount = input.Plant.Units.Count;
    //        int plantTotalPower = (input.Plant.Units.Select(el => el.P_th_Kwk).Sum()) * scaleFactor;
    //        List<int> unitPowerNet = input.Plant.Units.Select(el => el.P_th_Kwk * scaleFactor).ToList();
    //        List<int> unitDeltaPower = input.Plant.Units.Select(el => el.P_th_DeltaQh * scaleFactor).ToList();
    //        List<int> unitMinPower = input.Plant.Units.Select(el => el.P_th_Min * scaleFactor).ToList();
    //        //int minUpTime = (int)Math.Round(input.Plant.ThermalData.Times.Min_Uptime.TotalHours, 0);
    //        //int minDownTime = (int)Math.Round(input.Plant.ThermalData.Times.Min_Downtime.TotalHours, 0);
    //        int minUpTime = 11;
    //        int minDownTime = 11;
    //        #endregion

    //        #region Variables
    //        CpModel model = new CpModel();

    //        IntVar[][] P_CR_P = new IntVar[unitCount][]; // Gesamtleistung
    //        IntVar[][] P_CR_E = new IntVar[unitCount][]; //Regelarbeit
    //        IntVar[][] u = new IntVar[unitCount][];
    //        IntVar[][] ETAFactor = new IntVar[unitCount][];
    //        IntVar[] qFactor = new IntVar[input.TimeSpanNumber];
    //        IntVar[][] ProdCost = new IntVar[unitCount][];
    //        IntVar[][] ProdCostWithProbabilityFactor = new IntVar[unitCount][];
    //        IntVar[] Trade = new IntVar[input.TimeSpanNumber]; // Gehandelte Menge DA in KW (- Verkauf, + Zukauf)
    //        IntVar[] TradeRevenues = new IntVar[input.TimeSpanNumber]; // Gehandelte Menge DA in KW (- Verkauf, + Zukauf)
    //        IntVar[] TradeRevenues_CR_P = new IntVar[input.TimeSpanNumber]; // Gehandelte Menge DA in KW (- Verkauf, + Zukauf)
    //        IntVar[] TradeRevenues_CR_E = new IntVar[input.TimeSpanNumber]; // Gehandelte Menge DA in KW (- Verkauf, + Zukauf)
    //        IntVar[] Price_CR_P = new IntVar[input.TimeSpanNumber];
    //        IntVar[] Price_CR_E = new IntVar[input.TimeSpanNumber];
    //        IntVar[] OP_Cost = new IntVar[input.TimeSpanNumber];
    //        IntVar[][] sUp = new IntVar[unitCount][];
    //        IntVar[][] sDown = new IntVar[unitCount][];
    //        IntVar[][][] ETA = new IntVar[unitCount][][];
    //        ILiteral[][][] ETAsel = new ILiteral[unitCount][][];
    //        IntVar[][] q = new IntVar[input.TimeSpanNumber][];
    //        ILiteral[][] q_sel = new ILiteral[input.TimeSpanNumber][];
    //        ILiteral[] q_sel_pos_neg = new ILiteral[input.TimeSpanNumber];

    //        for (int b = 0; b < unitCount; b++)
    //        {
    //            P_CR_P[b] = new IntVar[input.TimeSpanNumber];
    //            P_CR_E[b] = new IntVar[input.TimeSpanNumber];
    //            ETAFactor[b] = new IntVar[input.TimeSpanNumber];
    //            ProdCost[b] = new IntVar[input.TimeSpanNumber];
    //            ProdCostWithProbabilityFactor[b] = new IntVar[input.TimeSpanNumber];
    //            u[b] = new IntVar[input.TimeSpanNumber];
    //            sUp[b] = new IntVar[input.TimeSpanNumber];
    //            sDown[b] = new IntVar[input.TimeSpanNumber];

    //            ETA[b] = new IntVar[input.TimeSpanNumber][];
    //            ETAsel[b] = new ILiteral[input.TimeSpanNumber][];
    //        }

    //        for (int t = 0; t < input.TimeSpanNumber; t++)
    //        {
    //            for (int b = 0; b < unitCount; b++)
    //            {
    //                P_CR_P[b][t] = model.NewIntVar(0, plantTotalPower, $"P_CR_P(b{b},t{t})");
    //                P_CR_E[b][t] = model.NewIntVar(0, plantTotalPower, $"P_CR_E(b{b},t{t})");
    //                ETAFactor[b][t] = model.NewIntVar(0, 999000, $"ETAFactor(b{b},t{t})");
    //                qFactor[t] = model.NewIntVar(0, 999000, $"ETAFactor(t{t})");
    //                ProdCost[b][t] = model.NewIntVar(-100000000, 100000000, $"P(b{b},t{t})");
    //                ProdCostWithProbabilityFactor[b][t] = model.NewIntVar(-100000000, 100000000, $"P(b{b},t{t})");
    //                u[b][t] = model.NewIntVar(0, 1, $"UnitCommitment({b},{t})");

    //                sUp[b][t] = model.NewIntVar(0, 1, $"Startup(b{b},t{t})");
    //                sDown[b][t] = model.NewIntVar(0, 1, $"Shutdown(b{b},t{t})");

    //                ETA[b][t] = new IntVar[input.P_Range.Count];
    //                ETAsel[b][t] = new ILiteral[input.P_Range.Count];

    //                for (int j = 0; j < input.P_Range.Count; j++)
    //                {
    //                    ETA[b][t][j] = model.NewIntVar(0, 1, $"ETA[{b},{t},{j}]");
    //                    ETAsel[b][t][j] = model.NewBoolVar($"ETAsel[{b},{t},{j}]");

    //                    model.AddLinearExpressionInDomain(P_CR_P[b][t],
    //                        Domain.FromValues(GetStepRange(j, input.P_Range))).OnlyEnforceIf(ETAsel[b][t][j]);
    //                }

    //                q[t] = new IntVar[Ep_Intervall.Count()];
    //                q_sel[t] = new ILiteral[Ep_Intervall.Count()];

    //                for (int j = 0; j < input.P_Range.Count; j++)
    //                {
    //                    q[t][j] = model.NewIntVar(0, 1, $"q[{t},{j}]");
    //                    q_sel[t][j] = model.NewBoolVar($"q_sel[{t},{j}]");
    //                }
    //            }
    //            Trade[t] = model.NewIntVar(-plantTotalPower, plantTotalPower, $"Trade{t}");
    //            Price_CR_P[t] = model.NewIntVar(-9999, 9999, $"Price_CR_P{t}");
    //            Price_CR_E[t] = model.NewIntVar(-9999, 9999, $"Price_CR_E{t}");
    //            TradeRevenues[t] = model.NewIntVar(-99999999, 999999999, $"TradeRevenues{t}");
    //            TradeRevenues_CR_P[t] = model.NewIntVar(-99999999, 999999999, $"TradeRevenues{t}");
    //            TradeRevenues_CR_E[t] = model.NewIntVar(-99999999, 999999999, $"TradeRevenues{t}");
    //            OP_Cost[t] = model.NewIntVar(-99999999, 999999999, $"TradeRevenues{t}");
    //        }
    //        #endregion

    //        #region Constraints
    //        for (int b = 0; b < unitCount; b++)
    //        {
    //            for (int t = input.LeadHours; t < input.TimeSpanNumber; t++)
    //            {
    //                model.Add(P_CR_P[b][t] <= u[b][t] * (unitPowerNet[b] - input.ReservedPowerPerUnit[b][t])); // Maximale Blockleistung
    //                model.Add(P_CR_P[b][t] >= u[b][t] * (int)(unitPowerNet[b] * input.Plant.P_Min_Relative)); // Minimale Blockleistung
    //                if (t > 0)
    //                {
    //                    // Increase
    //                    model.Add(P_CR_P[b][t] - P_CR_P[b][t - 1] <= unitDeltaPower[b] + sUp[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
    //                    // Decrease
    //                    model.Add(P_CR_P[b][t - 1] - P_CR_P[b][t] <= unitDeltaPower[b] + sDown[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
    //                }

    //                #region STARTUP-SHUTDOWN-COMMITMENT-CONSTRAINT
    //                // - Startup-Variable sUp[t] wird 1, wenn Kraftwerk gestartet wird (u[t]-u[t-1] = 1); sDown[t] muss dann 0 sein
    //                // - umgekehrt für Herunterfahren des Kraftwerks Shutdown-Variable sDown
    //                model.Add(u[b][t] - u[b][t - 1] == sUp[b][t] - sDown[b][t]);

    //                // Restriktion sorgt dafür, dass Kraftwerk nicht im gleichen Zeitschritt gestoppt und gestartet werden können
    //                model.Add(sUp[b][t] + sDown[b][t] <= 1);
    //                #endregion

    //                #region Min Up&-Downtime
    //                model.Add(sUp[b][t] + LinearExpr.Sum((from n in Enumerable.Range(1, minUpTime) select sDown[b][t - n]).ToArray()) <= 1);
    //                model.Add(sDown[b][t] + LinearExpr.Sum((from n in Enumerable.Range(1, minDownTime) select sUp[b][t - n]).ToArray()) <= 1);
    //                #endregion

    //                #region Dyn. Wirkungsgrad
    //                model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select ETA[b][t][n]).ToArray()) == 1);

    //                for (int j = 0; j < input.P_Range.Count; j++)
    //                {
    //                    model.Add(ETA[b][t][j] == 1).OnlyEnforceIf(ETAsel[b][t][j]);
    //                    model.Add(ETA[b][t][j] == 0).OnlyEnforceIf(ETAsel[b][t][j].Not());
    //                }

    //                model.Add(ETAFactor[b][t] == LinearExpr.Sum(from n in Enumerable.Range(0, input.P_Range.Count)
    //                                                            select LinearExpr.Prod(ETA[b][t][n], (long)(input.ProductionCosts[t] / input.Plant.Efficiency.Curve[n]))));

    //                model.Add(qFactor[t] == LinearExpr.Sum(from n in Enumerable.Range(0, q_pos.Count())
    //                                                            select LinearExpr.Prod(q[t][n], q_pos[n]))).OnlyEnforceIf(q_sel_pos_neg);

    //                model.Add(qFactor[t] == LinearExpr.Sum(from n in Enumerable.Range(0, q_neg.Count())
    //                                                       select LinearExpr.Prod(q[t][n], q_neg[n]))).OnlyEnforceIf(q_sel_pos_neg);

    //                model.Add()

    //                model.AddProdEquality(ProdCost[b][t], new List<IntVar>() {ETAFactor[b][t], P_CR_P[b][t] });
    //                #endregion

    //                #region Demand/Sales-Constraints
    //                model.Add(Trade[t] == input.PowerProcured[t] - LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select P_CR_P[n][t]).ToArray()));
    //                model.Add(TradeRevenues_CR_P[t] == -TradeRevenues_CR_E[t] + LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select ProdCostWithProbabilityFactor[n][t]).ToArray()) + OP_Cost[t] + costOfControlReserve);
    //                model.AddDivisionEquality(Price_CR_P[t], TradeRevenues_CR_P[t], P_CR_P[t]);
    //                model.AddProdEquality(ProdCostWithProbabilityFactor[b][t], new List<IntVar> { ProdCost[b][t], qFactor[t] });
    //                model.Add(OP_Cost[t] == LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select ProdCost[n][t]).ToArray()) + (-input.HPFC[t]));
    //                model.AddProdEquality(TradeRevenues_CR_P[t], new List<IntVar> { Price_CR_P[t], Trade[t] });
    //                model.AddProdEquality(TradeRevenues_CR_E[t], new List<IntVar> {qFactor[t], Price_CR_P[t], Trade[t] });
    //                model.Add(TradeRevenues[t] == TradeRevenues_CR_P[t] + TradeRevenues_CR_E[t]);
    //                #endregion
    //            }
    //        }

    //        for (int t = input.LeadHours; t < input.TimeSpanNumber; t++)
    //        {
    //            model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select P_CR_P[n][t]).ToArray()) <= input.PowerProcured[t]);
    //        }

    //        for (int t = input.LeadHours; t < input.TimeSpanNumber; t++)
    //        {
    //            model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, unitCount) select P_CR_P[n][t]).ToArray()) >= input.Demand[t]);
    //        }

    //        #region Anfangsbedingungen
    //        for (int t = 0; t < input.LeadHours; t++)
    //        {
    //            for (int b = 0; b < unitCount; b++)
    //            {
    //                // Startup-Shutdown Constraints
    //                model.Add(sUp[b][t] + sDown[b][t] <= 1);

    //                #region Leistungs-Constraints
    //                if (t > 0)
    //                {
    //                    model.Add(u[b][t] - u[b][t - 1] == sUp[b][t] - sDown[b][t]);
    //                    // Increase
    //                    model.Add(P_CR_P[b][t] - P_CR_P[b][t - 1] <= unitDeltaPower[b] + sUp[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
    //                    // Decrease
    //                    model.Add(P_CR_P[b][t - 1] - P_CR_P[b][t] <= unitDeltaPower[b] + sDown[b][t] * (unitMinPower[b] - unitDeltaPower[b]));
    //                }
    //                model.Add(P_CR_P[b][t] <= u[b][t] * unitPowerNet[b] - input.ReservedPowerPerUnit[b][t]);
    //                model.Add(P_CR_P[b][t] >= u[b][t] * (int)(unitPowerNet[b] * input.Plant.P_Min_Relative));

    //                if (t < input.LeadHours - minDownTime)
    //                    model.Add(P_CR_P[b][t] == input.UnitSchedule[t][b]);
    //                #endregion
    //            }
    //        }
    //        #endregion
    //        #endregion

    //        #region Zielfunktion

    //        var costsFromProduction = LinearExpr.Sum(from t in Enumerable.Range(input.LeadHours - 1, input.TimeSpanNumber - input.LeadHours)
    //                                                 select LinearExpr.Sum(from b in Enumerable.Range(0, unitCount) select ProdCostWithProbabilityFactor[b][t]));

    //        var revenue = LinearExpr.Sum((from t in Enumerable.Range(input.LeadHours - 1, input.TimeSpanNumber - input.LeadHours)
    //                                      select (input.PowerProcured[t] * input.PriceProcured[t]) - TradeRevenues[t]).ToArray());

    //        var totalRevenue = revenue - costsFromProduction;

    //        model.Maximize(totalRevenue);
    //        #endregion

    //        CpSolver solver = new CpSolver();
    //        solver.StringParameters = $"num_search_workers:{Environment.ProcessorCount - 2}";

    //        solver.StringParameters += $" max_time_in_seconds:{5}";

    //        CpSolverStatus result = solver.Solve(model);

    //        #region Printing
    //        if (print)
    //            PrintResults(input, unitCount, Trade, P_CR_P, ProdCost, totalRevenue, solver, result);
    //        #endregion

    //        return GetResults(solver, Trade, P_CR_P, input.LeadHours, input.TimeSpanNumber, unitCount, input.Times);
    //    }

    //    private static void PrintResults(Input input, int unitCount, IntVar[] Trade, IntVar[][] P, IntVar[][] ProdCost, LinearExpr totalRevenue, CpSolver solver, CpSolverStatus result)
    //    {
    //        Console.WriteLine("====================================");
    //        Console.WriteLine("CPSAT-Solver Result: " + result.ToString());
    //        Console.WriteLine("Number of Branches: " + solver.NumBranches());
    //        Console.WriteLine("Optimal objective value :" + solver.Value(totalRevenue));
    //        Console.WriteLine("Time: " + solver.WallTime() * 1000 + "ms");
    //        Console.WriteLine("====================================");

    //        string hourFmt = "00.##";
    //        string valFmt = "000.##";
    //        for (int i = 0; i < input.TimeSpanNumber; i++)
    //        {
    //            Console.Write($"Hour {i.ToString(hourFmt)} \t");
    //            //Console.Write($"D {demand[i].ToString(valFmt)} \t");


    //            for (int b = 0; b < unitCount; b++)
    //            {
    //                Console.Write(String.Concat("P", ($"[{i.ToString("00.##")},{b}] {solver.Value(P[b][i]).ToString(valFmt)} ")));
    //                Console.Write($"CO {solver.Value(ProdCost[b][i]).ToString(valFmt)} ");
    //            }


    //            Console.Write("\t");
    //            if (i > input.LeadHours)
    //            {
    //                Console.Write($"Proc {(input.PowerProcured[i] * input.PriceProcured[i]).ToString(valFmt)} ");
    //            }
    //            else
    //            {
    //                Console.Write($"Proc {000} ");
    //            }
    //            Console.Write(String.Concat("TRD", ($" {solver.Value(Trade[i]).ToString("000.##")} ")));
    //            Console.Write($"HPFC {input.HPFC[i].ToString("00.##")} ");

    //            Console.WriteLine();
    //        }
    //    }

    //    private static List<(DateTime Time, List<float> UnitPowerDAOpt)> GetResults(CpSolver solver, IntVar[] Trade, IntVar[][] P, int leadHours, int hourDuration, int unitCount, List<DateTime> times)
    //    {
    //        int scaleFactor = 1000;
    //        var optOutput = new List<(DateTime timeStamp, List<float> blockPower)>();

    //        for (int t = leadHours; t < hourDuration - leadHours; t++)
    //        {
    //            var blockPower = new List<float>();
    //            for (int b = 0; b < unitCount; b++)
    //            {
    //                blockPower.Add(solver.Value(P[b][t]) / scaleFactor);
    //            }
    //            optOutput.Add((times[t], blockPower));
    //        }

    //        return optOutput;
    //    }

    //    private static long[] GetStepRange(int idx, List<int> range)
    //    {
    //        List<long> values = new List<long>();

    //        int startVal = idx;
    //        if (idx > 0)
    //            startVal = range[idx - 1];

    //        for (int i = startVal; i <= range[idx]; i++)
    //        {
    //            values.Add(i);
    //        }
    //        return values.ToArray();
    //    }



    //    private class Input
    //    {
    //        internal static int ScaleFactor = 1000;

    //        internal DateTime StartTime;
    //        internal TimeSpan Duration;

    //        internal Plant_Thermal Plant;
    //        internal int HourDuration;
    //        internal int LeadHours;

    //        internal List<DateTime> Times;
    //        internal List<int> HPFC;
    //        internal List<int> Demand;
    //        internal List<int> PowerProcured;
    //        internal List<int> PriceProcured;
    //        internal List<int> ProductionCosts;
    //        internal List<int> P_Range;
    //        internal List<List<int>> UnitSchedule;
    //        internal List<List<int>> ReservedPowerPerUnit;

    //        public Input(Plant_Thermal plant, DateTime startTime, TimeSpan duration, int leadHours = 12) //, List<List<int>> reservedPowerPerUnit = null)
    //        {
    //            this.Plant = plant;

    //            this.LeadHours = leadHours;
    //            this.StartTime = startTime.AddHours(-LeadHours);
    //            this.Duration = duration.Add(new TimeSpan(LeadHours * 2, 0, 0));

    //            this.HourDuration = (int)Math.Round(this.Duration.TotalHours, 0);

    //            this.PowerProcured = ToInt(
    //                plant.Schedule.Get(this.StartTime, this.Duration)
    //                              .Select(el => (el.TimeStamp, Math.Abs(el.PowerProcured))).ToList()
    //                              .Resample(Resampling.TimeStep.Hour)
    //                              .Select(el => el.Item2), ScaleFactor).ToList();


    //            this.UnitSchedule = plant.Units.Select(unit => unit.Schedule.Get(this.StartTime, this.Duration))
    //                .Select(el => el.Select(EL => (EL.TimeStamp, EL.Power)).ToList().Resample(Resampling.TimeStep.Hour)
    //                .Select(it => (int)Math.Round(it.Item2, 0) * ScaleFactor).ToList())
    //                .SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();

    //            this.Demand = plant.TLP != null ? ToInt(plant.TLP.GetDemand(this.StartTime, this.Duration).Select(el => (el.TimeStamp, el.Power)).ToList().Resample(Resampling.TimeStep.Hour).Select(el => Math.Min(el.Item2, plant.Units.Select(unt => unt.P_th_Kwk).Sum())).ToList(), ScaleFactor) : this.PowerProcured.Select(el => 0).ToList();

    //            this.LeadHours = leadHours;
    //            this.ProductionCosts = ToInt(plant.ThermalProductionCostsPerMW(plant, this.StartTime, this.Duration).Select(el => el.ProductionCosts)).ToList();
    //            this.PriceProcured = ToInt(plant.Schedule.Get(this.StartTime, this.Duration).Select(el => (el.TimeStamp, el.PriceProcured)).ToList().Resample(Resampling.TimeStep.Hour).Select(el => el.Item2));
    //            this.HPFC = plant.BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, this.StartTime, this.Duration).Where(el => el.TimeStamp >= this.StartTime && el.TimeStamp < this.StartTime + this.Duration).Select(el => (int)Math.Round(el.PricePrediction, 0)).ToList();

    //            this.Times = this.StartTime.GetTimeVector(this.Duration);
    //            this.P_Range = ToInt(plant.Efficiency.GetPowerRange(plant), ScaleFactor);

    //            this.ReservedPowerPerUnit = plant.Units.Select(unit => unit.Schedule.Get(this.StartTime, this.Duration))
    //                .Select(el => el.Select(EL => (EL.TimeStamp, EL.PowerReserved)).ToList().Resample(Resampling.TimeStep.Hour)
    //                .Select(it => (int)Math.Round(it.Item2, 0) * ScaleFactor).ToList())
    //                .SelectMany(inner => inner.Select((item, index) => new { item, index })).GroupBy(i => i.index, i => i.item).Select(g => g.ToList()).ToList();
    //        }
    //    }
}
