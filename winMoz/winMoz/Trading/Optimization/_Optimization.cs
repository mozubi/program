﻿using System;
using System.Collections.Generic;
using winMoz.Assets.Plants;
using winMoz.Markets;
using winMoz.Assets;

namespace winMoz.Portfolios.Optimization
{
    public sealed class Optimization
    {
        public static List<(DateTime TimeStamp, int[] Revision)> PlantRevisionSchedule(Plant_Thermal plant, DateTime startTime)
            => LongtermPlanning.PlanYearlyRevision(plant, startTime);

        public static void DayAhead(Plant_Thermal plant, DateTime startTime, TimeSpan duration, TimeSpan stepDuration)
            => Thermal_DA.DayAhead(plant, startTime, duration, stepDuration, scaleFactor: 1000);

        public static List<(DateTime TimeStamp, float, float, float)> ControlReserve(Block plant, DateTime startTime, TimeSpan duration, TimeSpan stepDuration, ControlReserveShortCode.ShortCode controlReserveType)
            => Thermal.ControlReserve(plant, startTime, duration, stepDuration, controlReserveType);
    }
}
