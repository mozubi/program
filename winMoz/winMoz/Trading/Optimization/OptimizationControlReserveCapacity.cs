﻿using Google.OrTools.Sat;
using Google.OrTools.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets.Plants;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Simulation;
using winMoz.Information.PFCs;
using winMoz.Markets;

namespace winMoz.Portfolios.Optimization
{
    internal static class Thermal
    {

        public static List<(DateTime TimeStamp, float powerMW, float priceCapacity, float priceEnergy)> ControlReserve(Block plant, DateTime startTime, TimeSpan duration, TimeSpan stepDuration, winMoz.Markets.ControlReserveShortCode.ShortCode controlReserveType)
        {
            if (duration.Days == 1)
            {
                TimeSpan rampDuration = controlReserveType == ControlReserveShortCode.ShortCode.mFRR ? TimeSpan.FromMinutes(15) : controlReserveType == ControlReserveShortCode.ShortCode.aFRR ? TimeSpan.FromMinutes(5) : TimeSpan.FromMinutes(15);
                //ScaleFactor kann geändert werden. Er bestimmt welche Genauigkeit die Leistungswerte im Optimierungmodell haben. ScaleFactor = 1000 bedeutet, dass die Leistung bis auf kW genau bestimmt werden kann.
                var input = new Input(plant, startTime, duration, stepDuration, rampDuration: rampDuration);

                List<(DateTime TimeStamp, float powerMW, float priceCapacity, float priceEnergy)> results = new List<(DateTime TimeStamp, float powerMW, float priceCapacity, float priceEnergy)>();

                int i = 0;
                foreach (DateTime time in input.Times)
                {
                    float PriceCapacity = 0;
                    float PriceEnergy = 0;
                    float TradeMW = 0;
                    if (input.Plant is Block_Kond)
                    {
                        var ProductionCostsOpt = input.Plant.GetCostForMWh(time, input.PlantSchedule_el[i].PowerProcured);
                        TradeMW = Math.Min(Math.Max(Math.Min(-input.PlantSchedule_el[i].PowerProcured + input.Plant.ThermalData.MinimumStableGeneration * input.Plant.PowerMW, 0), -GetPowerGradient(input.Plant) * input.Plant.PowerMW * (controlReserveType == ControlReserveShortCode.ShortCode.aFRR ? 5 : 15)), 0);
                        var ProductionCostsWithFRR = input.Plant.GetCostForMWh(time, input.PlantSchedule_el[i].PowerProcured + TradeMW);
                        var Costs = (ProductionCostsWithFRR * (input.PlantSchedule_el[i].PowerProcured + TradeMW) - ProductionCostsOpt * input.PlantSchedule_el[i].PowerProcured) * (float)stepDuration.TotalHours;
                        var TradeCost = input.HPFC[i].Price * TradeMW * (float)stepDuration.TotalHours;
                        (PriceCapacity, PriceEnergy) = GetPricesControlReserve(TradeCost, Costs, TradeMW, isMakeOrBuy: false, input.Plant.RiskAversion, input.Plant._AsumedCallProbability[controlReserveType == ControlReserveShortCode.ShortCode.aFRR ? ControlReserveShortCode.ShortCode.n_aFRR : ControlReserveShortCode.ShortCode.n_mFRR], stepDuration);
                    }
                    if (Math.Abs(TradeMW) > 0)
                        results.Add((time, TradeMW, PriceCapacity, PriceEnergy));
                    i++;
                }

                i = 0;
                // 2nd pass - MakeMore?
                foreach (DateTime time in input.Times)
                {
                    float PriceCapacity = 0;
                    float PriceEnergy = 0;
                    float TradeMW = 0;
                    var ProductionCostsOpt = input.Plant.GetCostForMWh(time, input.PlantSchedule_el[i].PowerProcured);
                    TradeMW = (input.PlantSchedule_el[i].PowerProcured > input.Plant.ThermalData.MinimumStableGeneration * input.Plant.PowerMW) ? Math.Max(Math.Min(input.Plant.PowerMW * input.Plant.ThermalData.Efficiency.PowerCurve.Max() - input.PlantSchedule_el[i].PowerProcured, GetPowerGradient(input.Plant) * input.Plant.PowerMW * (controlReserveType == ControlReserveShortCode.ShortCode.aFRR ? 5 : 15)), 0) : 0;
                    var ProductionCostsWithFRR = input.Plant.GetCostForMWh(time, Math.Max(input.PlantSchedule_el[i].PowerProcured + TradeMW, 0));
                    var Costs = (ProductionCostsWithFRR * (input.PlantSchedule_el[i].PowerProcured + TradeMW) - ProductionCostsOpt * input.PlantSchedule_el[i].PowerProcured) * (float)stepDuration.TotalHours;
                    var TradeCost = input.HPFC[i].Price * TradeMW * (float)stepDuration.TotalHours;
                    (PriceCapacity, PriceEnergy) = GetPricesControlReserve(TradeCost, Costs, TradeMW, isMakeOrBuy: false, input.Plant.RiskAversion, input.Plant._AsumedCallProbability[controlReserveType == ControlReserveShortCode.ShortCode.aFRR ? ControlReserveShortCode.ShortCode.p_aFRR : ControlReserveShortCode.ShortCode.p_mFRR], stepDuration);
                    if (Math.Abs(TradeMW) > 0)
                        results.Add((time, TradeMW, PriceCapacity, PriceEnergy));
                    i++;
                }
                return results;
            }
            else return null;
        }
        /// <summary>
        /// Leistungsgradient nach https://de.wikipedia.org/wiki/Lastfolgebetrieb
        /// </summary>
        /// <param name="asset">Kraftwerk</param>
        /// <returns></returns>
        private static float GetPowerGradient(Block asset)
        {
            if (asset.Commodity == Commodities.Commodity.Uranium)
                return 0.045F;
            else if (asset.Commodity == Commodities.Commodity.Hard_Coal)
                return 0.04F;
            else if (asset.Commodity == Commodities.Commodity.Lignite)
                return 0.03F;
            else if (asset.Commodity == Commodities.Commodity.Natural_Gas)
                return 0.20F;
            else //Fall Öl-Kraftwerk ist nicht geklärt
                return 0.025F;
        }

        private static (float CapacityPrice, float EnergyPrice) GetPricesControlReserve(float TradeCost, float Costs, float Trade, bool isMakeOrBuy, float riskAversion, float startAssumedCallProbability, TimeSpan stepDuration)
        {
            float DB = 5F * Math.Abs(Trade);
            //float q = 0.01F;
            float OPCost = isMakeOrBuy ? Math.Min(TradeCost - Costs, 0) : Math.Max(TradeCost - Costs, 0);
            float AUC = OPCost + DB; //Arbeitsunabhängige Kosten
            float PriceCapacity = Trade != 0 ? (riskAversion * AUC) / Math.Abs(Trade) : 0;
            float PriceEnergy = Trade != 0 ? ((1 / startAssumedCallProbability) * (1 - riskAversion) * (AUC) + Costs) / Math.Abs(Trade) / (float)stepDuration.TotalHours : 0;
            return (PriceCapacity, PriceEnergy);
        }

        private class Input
        {
            internal int ScaleFactor = 1000;

            internal DateTime StartTime;
            internal TimeSpan Duration;

            internal Block Plant;
            internal int TimeSpanNumber;
            internal int LeadTimeSpans;
            internal float RampScaling;
            internal TimeSpan StepDuration;

            internal List<DateTime> Times;
            internal List<(DateTime TimeStamp, float Price)> HPFC;
            internal List<(DateTime TimeStamp, float PowerProcured)> PlantSchedule_el;

            internal List<int> FirstPossibleMarketing;

            public Input(Block plant, DateTime startTime, TimeSpan duration, TimeSpan stepDuration, int leadHours = 12, TimeSpan rampDuration = default, int minUpTime = 11, int mitDownTime = 11) //, List<List<int>> reservedPowerPerUnit = null)
            {
                this.Plant = plant;

                this.StartTime = startTime;
                this.Duration = duration;
                this.LeadTimeSpans = 0;

                this.StepDuration = stepDuration;
                this.Plant.Schedule.Extend(this.StartTime, this.Duration);

                var RampDuration = rampDuration != default ? rampDuration : TimeSpan.FromMinutes(15);
                this.RampScaling = (float)(RampDuration.TotalHours / TimeSpan.FromMinutes(15).TotalHours);

                this.TimeSpanNumber = (int)Math.Round(this.Duration.TotalHours / stepDuration.TotalHours, 0);

                this.PlantSchedule_el = plant.Schedule.Get(this.StartTime, this.Duration)
                    .Select(el => (el.TimeStamp, el.Power)).ToList().Resample(Resampling.GetTimeStep(stepDuration))
                    .Select(it => (it.TimeStamp, (float)Math.Round(it.Item2, 2))).ToList();

                this.HPFC = plant.BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, this.StartTime, this.Duration).Where(el => el.TimeStamp >= this.StartTime && el.TimeStamp < this.StartTime + this.Duration).ToList().Resample(Resampling.GetTimeStep(stepDuration)).ToList().Select(el => (el.TimeStamp, (float)Math.Round(el.Item2, 0))).ToList();

                this.Times = this.StartTime.GetTimeVector(this.Duration, step: stepDuration);


            }

        }
    }
}
