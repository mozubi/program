﻿using Google.OrTools.Sat;
using Google.OrTools.Util;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets.Plants;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Information.PFCs;


namespace winMoz.Trading.Optimization
{
    public static class StorageOptimization
    {
        private static List<(DateTime Timestamp, float Power, float Capacity, float Revenue, float Costs)> _optimizeSingle(Plant_Storage plant, DateTime startTime, TimeSpan duration, bool onlyDayAhead, bool onlyIntraDay, bool isForIntraday)
        {
            var output = new List<(DateTime Timestamp, float Power, float Capacity, float Revenue, float Costs)>();

            using (var input = Input.Create(plant, startTime, duration, onlyDayAhead, onlyIntraDay))
            {
                int qh = input.Capacity.Count + 1;

                CpModel model = new CpModel();

                #region Variables
                IntVar[] P = new IntVar[qh];
                IntVar[] E = new IntVar[qh];
                IntVar[] Trade = new IntVar[qh];
                IntVar[] ProdCost = new IntVar[qh];
                IntVar[][] ETA = new IntVar[qh][];
                ILiteral[][] ETARange = new ILiteral[qh][];

                for (int t = 0; t < qh; t++)
                {
                    P[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH, (int)input.Plant.P_Delta_QH, $"P({t})");
                    Trade[t] = model.NewIntVar(-(int)input.Plant.PowerMW * 20000, (long)input.Plant.PowerMW * 20000, $"Trade{t}");
                    ProdCost[t] = model.NewIntVar(-1000000, 1000000, $"ProductionCosts({t})");
                    E[t] = model.NewIntVar((int)input.Capacity[0].Min, (int)input.Capacity[0].Max * 4, $"Stored Energy({t})");
                    ETA[t] = new IntVar[input.P_Range.Count];
                    ETARange[t] = new ILiteral[input.P_Range.Count];

                    for (int j = 0; j < input.P_Range.Count; j++)
                    {
                        // TODO: Validate P_Range - Step range - Cost range
                        ETA[t][j] = model.NewIntVar(0, 1, $"ETA[{t},{j}]");
                        ETARange[t][j] = model.NewBoolVar($"ETARange[{t},{j}]");

                        //model.AddLinearExpressionInDomain(P[t],
                        //    Domain.FromValues(GetStepRange(j, input.P_Range))).OnlyEnforceIf(ETARange[t][j]);
                    }
                }
                #endregion

                #region Constraints
                model.Add(E[0] == input.LastSoC);

                if (input.LastItem != null)
                    model.Add(P[0] == (int)Math.Round(input.LastItem.Power, 0));
                else model.Add(P[0] == 0);

                for (int t = 0; t < qh; t++)
                {
                    // DayAhead - Stunden
                    if (onlyDayAhead)
                        if ((t - 1) % 4 == 0 /*&& t < qh - 4*/)
                            for (int i = 1; i < 4; i++)
                                model.Add(P[t + i] == P[t]);


                    if (t > 0)
                    {
                        // Min&Max Power
                        model.Add(E[t] == E[t - 1] + P[t]);
                        
                        // Speicher-Leistungsband
                        //model.Add(P[t] <= E[t] - (int)input.Capacity[t+1].Max);
                        //model.Add(P[t] >= (int)input.Capacity[t+1].Min - E[t]);

                        model.Add(Trade[t] == -P[t] * (int)Math.Round(input.Prices[t - 1], 0));

                        #region Dyn. Wirkungsgrad
                        model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select ETA[t][n]).ToArray()) == 1);

                        model.Add(P[t] == LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select input.P_Range[n] * ETA[t][n]).ToArray()));

                        for (int j = 0; j < input.P_Range.Count; j++)
                        {
                            model.Add(ETA[t][j] == 1).OnlyEnforceIf(ETARange[t][j]);
                            model.Add(ETA[t][j] == 0).OnlyEnforceIf(ETARange[t][j].Not());
                        }

                        model.Add(ProdCost[t] == LinearExpr.Sum(from n in Enumerable.Range(0, input.P_Range.Count) select LinearExpr.Prod(ETA[t][n], input.CostCurve[n])));
                        #endregion
                    }
                }
                #endregion

                #region Zielfunktion
                var revenue = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select Trade[t]);
                var costs = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select ProdCost[t]);

                model.Maximize(revenue - costs);
                #endregion

                #region Solver
                CpSolver solver = new CpSolver();
                solver.StringParameters = $"max_time_in_seconds:{5}";
                CpSolverStatus result = solver.Solve(model);
                #endregion


                for (int t = 1; t < qh; t++)
                    output.Add((input.StartTime.AddMinutes(15 * (t - 1)), solver.Value(P[t]) / 4.0F, solver.Value(E[t]) / 4.0F, solver.Value(Trade[t]) / 4.0F, solver.Value(ProdCost[t]) / 4.0F));

            }
            return output.Select(el => (el.Timestamp, el.Power, el.Capacity, el.Revenue, el.Costs)).ToList();
        }

        /// <summary>
        /// Optimization Single Market DayAhead
        /// </summary>
        /// <param name="plant"></param>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        public static List<(DateTime Timestamp, float Power, float Capacity, float Revenue, float Costs)> OptimizeDayAhead(this Plant_Storage plant, DateTime startTime, TimeSpan duration) => _optimizeSingle(plant, startTime, duration, true, false, false);
        /// <summary>
        /// Optimization Single Market Intraday
        /// </summary>
        /// <param name="plant"></param>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        public static List<(DateTime Timestamp, float Power, float Capacity, float Revenue, float Costs)> OptimizeIntraDay(this Plant_Storage plant, DateTime startTime, TimeSpan duration) => _optimizeSingle(plant, startTime, duration, false, true, true);
        /// <summary>
        /// Optimization Multimarket
        /// </summary>
        /// <param name="plant"></param>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        public static List<(DateTime Timestamp, float Power_Total, float Power_DA, float Power_ID, float Capacity, float Revenue, float Costs)> OptimizeSpot(this Plant_Storage plant, DateTime startTime, TimeSpan duration, TimeSpan smallestStep, bool isIntraday)
        {
            if (isIntraday)
                return OptimizeSpotIntraday(plant, startTime, duration, smallestStep);
            else
                return OptimizeSpotDayAhead(plant, startTime, duration, smallestStep);
        }
        /// <summary>
        /// Optimization Multi Market DayAhead (Intraday und DayAhead-Leistung werden auf Profit optimiert)
        /// </summary>
        /// <param name="plant"></param>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
         public static List<(DateTime Timestamp, float Power_Total, float Power_DA, float Power_ID, float Capacity, float Revenue, float Costs)> OptimizeSpotDayAhead(this Plant_Storage plant, DateTime startTime, TimeSpan duration, TimeSpan smallestStep)
        {
            var output = new List<(DateTime Timestamp, float PowerTotal, float Power_DA, float Power_ID, float Capacity, float Revenue, float Costs)>();

            using (var input = Input.Create(plant, startTime, duration, smallestStep, false, false, false))
            {
                int qh = input.Capacity.Count + 1;

                CpModel model = new CpModel();

                #region Variables
                IntVar[] P = new IntVar[qh];
                IntVar[] P_DA = new IntVar[qh];
                IntVar[] P_ID = new IntVar[qh];
                IntVar[] E = new IntVar[qh];
                IntVar[] TradeDA = new IntVar[qh];
                IntVar[] TradeID = new IntVar[qh];
                IntVar[] ProdCost = new IntVar[qh];
                IntVar[][] ETA = new IntVar[qh][];

                for (int t = 0; t < qh; t++)
                {
                    P[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH, (int)input.Plant.P_Delta_QH, $"P({t})");
                    P_DA[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH, (int)input.Plant.P_Delta_QH, $"P_DA({t})");
                    P_ID[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH, (int)input.Plant.P_Delta_QH, $"P_ID({t})");
                    TradeDA[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH * 20000, (int)input.Plant.P_Delta_QH * 20000, $"Trade{t}");
                    TradeID[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH * 20000, (int)input.Plant.P_Delta_QH * 20000, $"Trade{t}");
                    ProdCost[t] = model.NewIntVar(-1000000, 1000000, $"ProductionCosts({t})");
                    E[t] = model.NewIntVar((int)input.Capacity[0].Min, (int)input.Capacity[0].Max * 4, $"Stored Energy({t})");
                    ETA[t] = new IntVar[input.P_Range.Count];

                    for (int j = 0; j < input.P_Range.Count; j++)
                    {
                        // TODO: Validate P_Range - Step range - Cost range
                        ETA[t][j] = model.NewIntVar(0, 1, $"ETA[{t},{j}]");

                    }
                }
                #endregion

                #region Constraints
                 model.Add(E[0] == input.LastSoC);

                //var lastItem = input.Plant.Schedule.ToList().SingleOrDefault(el => el.TimeStamp == input.StartTime.AddMinutes(-15));
                if (input.LastItem != null)
                    model.Add(P[0] == (int)Math.Round(input.LastItem.Power, 0));
                else model.Add(P[0] == 0);

                for (int t = 0; t < qh; t++)
                {
                    // DayAhead - Stunden
                    if ((t - 1) % 4 == 0 /*&& t < qh - 4*/)
                        for (int i = 1; i < 4; i++)
                            model.Add(P_DA[t + i] == P_DA[t]);

                    if (t > 0)
                    {
                        model.Add(P[t] == P_DA[t] + P_ID[t]);

                        // Min&Max Power
                        model.Add(E[t] == E[t - 1] + P[t]);
                        
                        //// Speicher-Leistungsband
                        ////model.Add(P[t] <= E[t] - (int)input.Capacity[t+1].Max);
                        ////model.Add(P[t] >= (int)input.Capacity[t+1].Min - E[t]);

                        model.Add(TradeDA[t] == -P_DA[t] * (int)Math.Round(input.PricesDA[t - 1], 0));
                        model.Add(TradeID[t] == -P_ID[t] * (int)Math.Round(input.PricesID[t - 1], 0));

                        #region Dyn. Wirkungsgrad
                        model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select ETA[t][n]).ToArray()) == 1);

model.Add(P[t] == LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select input.P_Range[n] * ETA[t][n]).ToArray()));


                        model.Add(ProdCost[t] == LinearExpr.Sum(from n in Enumerable.Range(0, input.P_Range.Count) select LinearExpr.Prod(ETA[t][n], input.CostCurve[n])));
                        #endregion
                    }
                }
                #endregion

                #region Zielfunktion
                var revenue = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select TradeDA[t] + TradeID[t]);
                var costs = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select ProdCost[t]);

                model.Maximize(revenue - costs);
                #endregion

                #region Solver
                CpSolver solver = new CpSolver();
                solver.StringParameters = $"max_time_in_seconds:{5}";
                CpSolverStatus result = solver.Solve(model);
                #endregion

                for (int t = 1; t < qh; t++)
                    output.Add((input.StartTime.AddMinutes(15 * (t - 1)), solver.Value(P[t]) / 4.0F, solver.Value(P_DA[t]) / 4.0F, solver.Value(P_ID[t]) / 4.0F, solver.Value(E[t]) / 4.0F, solver.Value(TradeDA[t] + TradeID[t]) / 4.0F, solver.Value(ProdCost[t]) / 4.0F));

                /*GC.KeepAlive(result);
                GC.KeepAlive(P);
                GC.KeepAlive(P_DA);
                GC.KeepAlive(P_ID);
                GC.KeepAlive(E);
                GC.KeepAlive(TradeDA);
                GC.KeepAlive(TradeID);
                GC.KeepAlive(ProdCost);
                GC.KeepAlive(ETA);
                GC.KeepAlive(revenue);
                GC.KeepAlive(costs);
                GC.KeepAlive(solver);
                GC.KeepAlive(model);*/
            }
            //GC.Collect();
            return output;
        }
        /// <summary>
        /// Optimization Multi Market Intraday (Leistung nach DayAhead-Markt als PowerProcured (kann nicht mehr verändert werden) 
        /// </summary>
        /// <param name="plant"></param>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        public static List<(DateTime Timestamp, float Power_Total, float Power_DA, float Power_ID, float Capacity, float Revenue, float Costs)> OptimizeSpotIntraday(this Plant_Storage plant, DateTime startTime, TimeSpan duration, TimeSpan smallestStep)
        {
            var output = new List<(DateTime Timestamp, float PowerTotal, float Power_DA, float Power_ID, float Capacity, float Revenue, float Costs)>();
            using (var input = Input.Create(plant, startTime, duration, smallestStep, false, false, true))
            {
                int qh = input.Capacity.Count + 1;

                CpModel model = new CpModel();

                #region Variables
                IntVar[] P = new IntVar[qh];
                IntVar[] P_ID = new IntVar[qh];
                IntVar[] E = new IntVar[qh];
                IntVar[] TradeID = new IntVar[qh];
                IntVar[] ProdCost = new IntVar[qh];
                IntVar[][] ETA = new IntVar[qh][];
                ILiteral[][] ETARange = new ILiteral[qh][];

                for (int t = 0; t < qh; t++)
                {
                    P[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH, (int)input.Plant.P_Delta_QH, $"P({t})");
                    P_ID[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH, (int)input.Plant.P_Delta_QH, $"P_ID({t})");
                    TradeID[t] = model.NewIntVar(-(int)input.Plant.P_Delta_QH * 20000, (int)input.Plant.P_Delta_QH * 20000, $"Trade{t}");
                    ProdCost[t] = model.NewIntVar(-1000000, 1000000, $"ProductionCosts({t})");
                    //E[t] = model.NewIntVar(0, (long)input.Plant.Attributes.Capacity * 4, $"Stored Energy({t})");
                    E[t] = model.NewIntVar((int)input.Capacity[0].Min, (int)input.Capacity[0].Max, $"Stored Energy({t})");
                    ETA[t] = new IntVar[input.P_Range.Count];
                    ETARange[t] = new ILiteral[input.P_Range.Count];

                    for (int j = 0; j < input.P_Range.Count; j++)
                    {
                        // TODO: Validate P_Range - Step range - Cost range
                        ETA[t][j] = model.NewIntVar(0, 1, $"ETA[{t},{j}]");
                        ETARange[t][j] = model.NewBoolVar($"ETARange[{t},{j}]");

                    }
                    
                }
                #endregion

                #region Constraints
                model.Add(E[0] == input.LastSoC);

                if (input.LastItem != null)
                    model.Add(P[0] == (int)Math.Round(input.LastItem.Power, 0));
                else model.Add(P[0] == 0);

                for (int t = 0; t < qh; t++)
                {
                    if (t > 0)
                    {
                        model.Add(P[t] == (int)input.PowerProcured[t - 1] + P_ID[t]);

                        // Min&Max Power
                        model.Add(E[t] == E[t - 1] + P[t]);
                        model.Add(TradeID[t] == -P_ID[t] * (int)Math.Round(input.PricesID[t - 1], 0));

                        #region Dyn. Wirkungsgrad
                        model.Add(LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select ETA[t][n]).ToArray()) == 1);

                        model.Add(P[t] == LinearExpr.Sum((from n in Enumerable.Range(0, input.P_Range.Count) select input.P_Range[n] * ETA[t][n]).ToArray()));
                        
                        for (int j = 0; j < input.P_Range.Count; j++)
                        {
                            model.Add(ETA[t][j] == 1).OnlyEnforceIf(ETARange[t][j]);
                            model.Add(ETA[t][j] == 0).OnlyEnforceIf(ETARange[t][j].Not());
                        }

                        model.Add(ProdCost[t] == LinearExpr.Sum(from n in Enumerable.Range(0, input.P_Range.Count) select LinearExpr.Prod(ETA[t][n], input.CostCurve[n])));
                        #endregion
                    }
                }
                #endregion

                #region Zielfunktion
                var revenue = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select TradeID[t]);
                var costs = LinearExpr.Sum(from t in Enumerable.Range(1, qh - 1) select ProdCost[t]);

                model.Maximize(revenue - costs);
                #endregion

                #region Solver
                CpSolver solver = new CpSolver();
                solver.StringParameters = $"max_time_in_seconds:{5}";
                CpSolverStatus result = solver.Solve(model);
                #endregion

                for (int t = 1; t < qh; t++)
                    output.Add((input.StartTime.AddMinutes(15 * (t - 1)), solver.Value(P[t]) / 4.0F, input.PowerProcured[t - 1] / 4.0F, solver.Value(P_ID[t]) / 4.0F, solver.Value(E[t]) / 4.0F, solver.Value(TradeID[t]) / 4.0F, solver.Value(ProdCost[t]) / 4.0F));


                /*GC.KeepAlive(result);
                GC.KeepAlive(P);
                GC.KeepAlive(P_ID);
                GC.KeepAlive(E);
                GC.KeepAlive(TradeID);
                GC.KeepAlive(ProdCost);
                GC.KeepAlive(ETA);
                GC.KeepAlive(ETARange);
                GC.KeepAlive(revenue);
                GC.KeepAlive(costs);
                GC.KeepAlive(solver);
                GC.KeepAlive(model);*/
            }
            //GC.Collect();
            return output;
        }

        private static long[] GetStepRange(int idx, List<int> range)
        {
            var values = new List<long>();

            int startVal = range[idx];
            if (idx > 0)
                startVal = range[idx - 1];

            for (int i = startVal; i <= range[idx]; i++)
                values.Add(i);

            return values.ToArray();
        }

        private class Input : IDisposable
        {
            private bool disposedValue;

            private Input() { }
            public Plant_Storage Plant { get; private set; }
            public List<(float Min, float Max)> Capacity { get; private set; } = new List<(float Min, float Max)>();
            public List<(float Min, float Max)> PowerLimit { get; private set; } = new List<(float Min, float Max)>();
            public List<float> PricesDA { get; private set; } = new List<float>();
            public List<float> PricesID { get; private set; } = new List<float>();
            public List<float> Prices { get; private set; } = new List<float>();
            public List<float> PowerProcured { get; private set; } = new List<float>();
            public DateTime StartTime { get; private set; }
            public TimeSpan Duration { get; private set; }
            public List<long> CostCurve { get; internal set; } = new List<long>();
            public List<int> P_Range { get; private set; } = new List<int>();
            public int LastSoC { get; private set; }
            public ScheduleItem LastItem { get; private set; }

            public static Input Create(Plant_Storage plant, DateTime startTime, TimeSpan duration, bool onlyDayAhead, bool onlyIntraDay)
            {
                var inp = new Input();
                inp.StartTime = startTime;
                inp.Duration = duration;
                inp.Plant = plant;
                if (onlyDayAhead == onlyIntraDay)
                {
                    var EndOfDuration = startTime + duration;
                    inp.PricesDA.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, startTime, duration).ToList().Resample(Resampling.TimeStep.QuarterHour).Where(item => item.TimeStamp >= startTime && item.TimeStamp < EndOfDuration).Select(el => el.Item2).ToList());
                    inp.PricesID.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.QHPFC, startTime, duration).ToList().Where(item => item.TimeStamp >= startTime && item.TimeStamp < EndOfDuration).Select(el => el.PricePrediction).ToList());
                }
                else if (onlyDayAhead)
                    inp.Prices.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, startTime, duration).ToList().Resample(Resampling.TimeStep.QuarterHour).Select(el => el.Item2).ToList());
                else if (onlyIntraDay)
                    inp.Prices.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.QHPFC, startTime, duration).ToList().Select(el => el.PricePrediction).ToList());

                inp.P_Range = ToInt(plant.Efficiency.GetPowerRange(plant), 1);

                for (int i = 1; i < inp.P_Range.Count; i++)
                    inp.CostCurve.Add((long)Math.Round(inp.P_Range[i] * (plant.Economics.LCOE * 0.25 / plant.Efficiency.Curve[i]), 0));
                inp.CostCurve = inp.CostCurve.Prepend((long)inp.CostCurve.Average()).ToList();

                var negPrange = new List<int>(inp.P_Range.Skip(1).Select(el => -el));
                negPrange.Reverse();

                for (int i = negPrange.Count - 1; i >= 0; i--)
                    inp.P_Range = inp.P_Range.Prepend(negPrange[i]).ToList();

                var dupl = new List<long>(inp.CostCurve.Skip(1));
                dupl.Reverse();

                for (int i = dupl.Count - 1; i >= 0; i--)
                    inp.CostCurve = inp.CostCurve.Prepend(dupl[i]).ToList();

                #region Dynamisches Speicherband
                int qh = 4;
                //if (!quarterHourly) qh = 1;

                for (DateTime t = startTime; t < startTime + duration; t = t.AddMinutes(15))
                {
                    inp.Capacity.Add((plant.MinSoC * qh, plant.MaxSoC * qh));
                    inp.PowerLimit.Add((-plant.PowerMW, plant.PowerMW));
                }

                inp.LastSoC = (int)inp.Plant.StateOfCharge.FirstOrDefault(el => el.TimeStamp == inp.StartTime.AddMinutes(-15)).Level * 4;
                if (inp.LastSoC <= (int)inp.Capacity[0].Min)
                    inp.LastSoC = (int)inp.Capacity[0].Min;

                inp.LastItem = inp.Plant.Schedule.ToList().SingleOrDefault(el => el.TimeStamp == inp.StartTime.AddMinutes(-15));

     // Speicherband
                //var totalQh = 48.0;
                //for (int i = 0; i < inp.Capacity.Count; i++)
                //{
                //    float factor;
                //    if (i < 48)
                //        factor = 1 - (i / totalQh);
                //    else
                //        factor = 1 - ((inp.Capacity.Count - i) / totalQh);

                //    inp.Capacity[i] = (inp.Capacity[i].Min, Math.Min(inp.Capacity[i].Min +
                //        (plant.Attributes.Capacity * factor) * 4 * 0.9, plant.Attributes.Capacity * qh));
                //}
                #endregion

                return inp;
            }

            public static Input Create(Plant_Storage plant, DateTime startTime, TimeSpan duration, TimeSpan smallestStep, bool onlyDayAhead, bool onlyIntraDay, bool isForIntraday)
            {
                var inp = new Input();
                inp.StartTime = startTime;
                inp.Duration = duration;
                inp.Plant = plant;
                if (onlyDayAhead == onlyIntraDay)
                {
                    var EndOfDuration = startTime + duration;
                    inp.PricesDA.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, startTime, duration).ToList().Resample(Resampling.GetTimeStep(smallestStep)).Where(item => item.TimeStamp >= startTime && item.TimeStamp < EndOfDuration).Select(el => el.Item2).ToList());
                    inp.PricesID.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.QHPFC, startTime, duration).ToList().Resample(Resampling.GetTimeStep(smallestStep)).Where(item => item.TimeStamp >= startTime && item.TimeStamp < EndOfDuration).Select(el => el.Item2).ToList());
                    if (isForIntraday) inp.PowerProcured.AddRange(plant.Schedule.Get(startTime, duration).Select(sch => (sch.TimeStamp, sch.PowerProcured)).ToList().Resample(Resampling.GetTimeStep(smallestStep)).Select(el => el.Item2).ToList());
                }
                else if (onlyDayAhead)
                    inp.Prices.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, startTime, duration).ToList().Resample(Resampling.GetTimeStep(smallestStep)).Select(el => el.Item2).ToList());
                else if (onlyIntraDay)
                    inp.Prices.AddRange(plant.BalancingGroup.GetPriceCurve(PriceProviderType.QHPFC, startTime, duration).ToList().Resample(Resampling.GetTimeStep(smallestStep)).Select(el => el.Item2).ToList());

                inp.P_Range = ToInt(plant.Efficiency.GetPowerRange(plant), 1);

                for (int i = 1; i < inp.P_Range.Count; i++)
                    inp.CostCurve.Add((long)Math.Round(inp.P_Range[i] * (plant.Economics.LCOE * 0.25 / plant.Efficiency.Curve[i]), 0));
                inp.CostCurve = inp.CostCurve.Prepend((long)inp.CostCurve.Average()).ToList();

                var negPrange = new List<int>(inp.P_Range.Skip(1).Select(el => -el));
                negPrange.Reverse();

                for (int i = negPrange.Count - 1; i >= 0; i--)
                    inp.P_Range = inp.P_Range.Prepend(negPrange[i]).ToList();

                var dupl = new List<long>(inp.CostCurve.Skip(1));
                dupl.Reverse();

                for (int i = dupl.Count - 1; i >= 0; i--)
                    inp.CostCurve = inp.CostCurve.Prepend(dupl[i]).ToList();

                #region Dynamisches Speicherband
                //int qh = 4;
                int qh = (int)(TimeSpan.FromHours(1).TotalMinutes / smallestStep.TotalMinutes);
                //if (!quarterHourly) qh = 1;

                for (DateTime t = startTime; t < startTime + duration; t = t.Add(smallestStep))
                {
                    inp.Capacity.Add((plant.MinSoC * qh, plant.MaxSoC * qh));
                    inp.PowerLimit.Add((-plant.PowerMW, plant.PowerMW));
                }

                inp.LastSoC = (int)inp.Plant.StateOfCharge.FirstOrDefault(el => el.TimeStamp == inp.StartTime.AddMinutes(-15)).Level * 4;
                if (inp.LastSoC <= (int)inp.Capacity[0].Min)
                    inp.LastSoC = (int)inp.Capacity[0].Min;

                inp.LastItem = inp.Plant.Schedule.ToList().SingleOrDefault(el => el.TimeStamp == inp.StartTime.AddMinutes(-15));

                // Speicherband
                //var totalQh = 48.0;
                //for (int i = 0; i < inp.Capacity.Count; i++)
                //{
                //    float factor;
                //    if (i < 48)
                //        factor = 1 - (i / totalQh);
                //    else
                //        factor = 1 - ((inp.Capacity.Count - i) / totalQh);

                //    inp.Capacity[i] = (inp.Capacity[i].Min, Math.Min(inp.Capacity[i].Min +
                //        (plant.Capacity * factor) * 4 * 0.9, plant.Capacity * qh));
                //}
                #endregion

                return inp;
            }

            private static List<int> ToInt(IEnumerable<float> floatListIn, float multiplier = 1)
            {
                return floatListIn.Select(el => (int)Math.Round(el * multiplier, 0)).ToList();
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        this.PricesDA = null;
                        this.PricesID = null;
                        this.Capacity = null;
                        this.CostCurve = null;
                        this.P_Range = null;
                    }

                    disposedValue = true;
                }
            }

            public void Dispose()
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(this);
            }
        }
    }
}

