﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using winMoz.Assets;
using winMoz.Helper;
using winMoz.Markets;
using winMoz.Markets.Bids;
using winMoz.Markets.Elements;
using winMoz.Simulation;

namespace winMoz.Portfolios
{
    public abstract class TradingStrategy : ITradingStrategy
    {
        public ReadOnlyCollection<Bid> Bids => _bids.ToList().AsReadOnly();
        protected ObservableCollection<Bid> _bids { get; set; } = new ObservableCollection<Bid>();
        protected Type[] ExcludedMarkets { get; set; }
        public IEnumerable<IAsset> Assets { get; protected set; }

        #region Constructors
        protected TradingStrategy(IEnumerable<IAsset> assets, params Type[] excludedMarkets)
        {
            this.Assets = assets;
            this.ExcludedMarkets = excludedMarkets.Count() > 0 ? excludedMarkets : new Type[] {};

            this._bids.CollectionChanged += SubscribeToBid;
        }

        public void ChangeExcludedMarkets(Type[] excludedMarkets)
        {
            if (excludedMarkets.Count() > 0) this.ExcludedMarkets = excludedMarkets;
            else this.ExcludedMarkets = new Type[0] { };
        }
        #endregion

        #region Base
        public void Execute(Market market)
        {
            if (!MarketIsExcluded(market))
                CreateBids(market);
            market.SubmitBid(this._bids.Where(bid => bid.GetType() == Type.GetType(market.Attributes.AllowedBidType)).Where(bid => bid.TimeStamp >= market.EventTimeframe.TimeStamp && bid.TimeStamp <= market.EventTimeframe.TimeStamp+market.EventTimeframe.Duration));
            this._bids = new ObservableCollection<Bid>();
            this._bids.CollectionChanged += SubscribeToBid;
        }

        private void SubscribeToBid(object sender, NotifyCollectionChangedEventArgs bids)
        {
            foreach (var bid in bids.NewItems.Cast<Bid>())
            {
                bid.Clearing += (clearedBid) => clearedBid.UpdateSchedule();
                if (bid.IsFuture()) ((Bid_Future)bid).Underlyings.ForEach(underlyingBid => underlyingBid.Clearing += (clearedBid) => clearedBid.UpdateSchedule());
            }
        }

        protected void AddBid(Bid bid) { if (bid.Segments.Count > 0) this._bids.Add(bid); }

        protected void AddBid(IEnumerable<Bid> bids) => bids.ToList().ForEach(bid => AddBid(bid));

        private bool MarketIsExcluded(Market market) => this.ExcludedMarkets.Contains(market.GetType());
        #endregion

        #region Overrides
        protected abstract void CreateBids(Market callingMarket);
        #endregion
    }
}