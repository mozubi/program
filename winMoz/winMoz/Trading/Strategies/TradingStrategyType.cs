﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Portfolios;
using winMoz.Tools.Helper;

namespace winMoz.Trading.Strategies
{
    public partial class TradingStrategyType : Enumeration
    {
        public Type TradingType { get; private set; }
        public TradingStrategyType(string value, int id, Type type) : base(value, id)
        {
            if (typeof(TradingStrategy).IsAssignableFrom(type)) TradingType = type;
            else throw new Exception($"TradingStrategy has to be Assignable from Type {type}.");
        }

        public static bool HasUniqueValues()
            => HasUniqueValues<TradingStrategyType>();

        public static bool HasUniqueIds()
            => HasUniqueIds<TradingStrategyType>();

        public static bool HasUniqueTradingTypes()
            => GetAll().GroupBy(prop => prop.TradingType, (el1, el2) => el2.Count() == 1).All(el => el);

        public static IEnumerable<TradingStrategyType> GetAll()
            => GetAll<TradingStrategyType>();

        public static TradingStrategyType FromString(string value)
            => FromString<TradingStrategyType>(value);

        #region Values
        public static TradingStrategyType Arbitrage = new TradingStrategyType("Arbitrage", 0, typeof(Arbitrage));
        public static TradingStrategyType Hedge = new TradingStrategyType("Hedge", 1, typeof(Hedge));
        public static TradingStrategyType Hedge_DA_CR = new TradingStrategyType("Hedge_DA_CR", 2, typeof(Hedge_DA_CR));
        public static TradingStrategyType Unlimited = new TradingStrategyType("Unlimited", 2, typeof(Unlimited));
        public static TradingStrategyType MakeOrBuy = new TradingStrategyType("MakeOrBuy", 2, typeof(MakeOrBuy));
        public static TradingStrategyType HedgeVRE = new TradingStrategyType("HedgeVRE", 2, typeof(HedgeVRE));
        public static TradingStrategyType Nothing = new TradingStrategyType("Nothing", 3, typeof(Nothing));
        #endregion
    }
}
