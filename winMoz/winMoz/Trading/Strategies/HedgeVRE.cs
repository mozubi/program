﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Information.PFCs;
using winMoz.Markets;
using winMoz.Markets.Bids;
using winMoz.Portfolios;
using winMoz.Markets.Elements;
using winMoz.Assets.Plants;

namespace winMoz.Trading.Strategies
{
    public class HedgeVRE : TradingStrategy
    {


        public HedgeVRE(IEnumerable<IAsset> assets, params Type[] excludedMarkets) : base(assets,excludedMarkets) { }


        protected override void CreateBids(Market market)
        {
            // TODO: implement limiting of hedge-bids
            var time = market.EventTimeframe.TimeStamp;
            var duration = market.EventTimeframe.Duration;
            var step = market.EventTimeframe.Step;
            //var weightedPrice = GetWeightedValue(Assets.Select(el => el.Economics.LCOE), this.Assets.Select(el => el.PowerMW));
            var schedule = new Schedule();
            if (Assets.Count() > 0)
                schedule = Assets.GetSumSchedule(time, duration);
            else
                schedule = Assets.First().Schedule;

            var weightedPriceTimeLine = GetWeightedPriceTimeLine(Assets, time, duration);

            var bids = new List<Bid>();

            //if (market.IsDayAheadMarket())
            //    this.Bids.Where(bid => bid.IsFuture()).SelectMany(bid => bid.Cast<Bid_Future>().Underlyings).Where(bid => bid.IsValid(time, duration, step)).ToList().ForEach(bid => schedule.Procure(bid.GetTimeSeries()));

            if (market.IsDayAheadMarket())
                bids.Add(Bid_DayAhead.Hour(Assets, market, schedule.Join(weightedPriceTimeLine, item => item.TimeStamp, item => item.TimeStamp, (sch, prices) => (sch.TimeStamp, sch.PowerDiff, sch.PowerDiff > 0 ? prices.WeightedPrice : ((MarketAttributes)market.Attributes).Upper_Price_Bound)).ToList()));


            if (market.IsIntradayMarket())
                bids.Add(Bid_Intraday.QuarterHour(Assets, market, schedule.Join(weightedPriceTimeLine, item => item.TimeStamp, item => item.TimeStamp, (sch, prices) => (sch.TimeStamp, sch.PowerDiff, sch.PowerDiff > 0 ? prices.WeightedPrice : ((MarketAttributes)market.Attributes).Upper_Price_Bound)).ToList()));


            bids.ToList().ForEach(bid => bid.Segments.ForEach(seg => seg.SetUnlimitedIfTooHigh(market)));
            foreach (var bid in bids)
            {
                bid.Segments.ForEach(seg => seg.SetUnlimitedIfTooHigh(market));
                AddBid(bid);
            }
        }
        public static List<(DateTime TimeStamp, float WeightedPrice)> GetWeightedPriceTimeLine(IEnumerable<IAsset> assets, DateTime startTime, TimeSpan duration)
        {
            var EndOfDuration = startTime.Add(duration);
            return assets.Select(asset => asset.Schedule.Get(startTime, duration).Select(item => new { item.TimeStamp, Revenues = -item.PowerDiff * ((asset is Plant_Biomass || asset is Plant_Hydro) ? 9999 : asset.Economics.LCOE), PowerDiff = item.PowerDiff }).ToList()).SelectMany(item => item).GroupBy(item => item.TimeStamp, (group, values) => (group, values.Select(item => item.PowerDiff).Sum() != 0 ? values.Select(el => el.Revenues).Sum() / values.Select(item => item.PowerDiff).Sum() : 0.0F)).ToList();
        }


    }
}
