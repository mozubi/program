﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Portfolios;
using winMoz.Markets;
using winMoz.Assets;
using winMoz.Markets.Bids;
using winMoz.Assets.Schedules;
using System.Collections.ObjectModel;
using winMoz.Helper;

namespace winMoz.Trading.Strategies
{
    public class Nothing : TradingStrategy
    {
        public Nothing(IEnumerable<IAsset> assets, params Type[] excludedMarkets) : base(assets, excludedMarkets) { }

        protected override void CreateBids(Market market) 
        {
            // Dummy Strategie für das Handling von thermischen Kraftwerken, da diese nur ein loser Bund aus Blöcken sind.
            // Damit wird CalcSchedule für thermische Kraftwerke aufgerufen und dann der Schedule entsprechend der Leistungen der Blöcke vermarktet.
        }
    }
}
