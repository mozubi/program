﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Portfolios;
using winMoz.Markets;
using winMoz.Assets;
using winMoz.Markets.Bids;
using winMoz.Assets.Schedules;
using System.Collections.ObjectModel;
using winMoz.Helper;

namespace winMoz.Trading.Strategies
{
    public class Unlimited : TradingStrategy
    {
        public Unlimited(IEnumerable<IAsset> assets, params Type[] excludedMarkets) : base(assets, excludedMarkets) { }

        protected override void CreateBids(Market market) 
        {
            // TODO: implement limiting of hedge-bids
            var time = market.EventTimeframe.TimeStamp;
            var duration = market.EventTimeframe.Duration;
            var step = market.EventTimeframe.Step;
            var schedule = this.Assets.GetSumSchedule(time, duration);

            var bids = new ObservableCollection<Bid>();
            bids.CollectionChanged += (s, e) =>
            {
                var newBid = (Bid)e.NewItems[0];
                //schedule.Procure(newBid.GetTimeSeries());
                newBid.Segments.ForEach(seg => seg.SetUnlimited(market));
                AddBid(newBid);
            };

            if (market.IsFuturesMarket())
                bids.Add(Bid_Future.Hour(this.Assets, market, schedule.Select(el => (el.TimeStamp, el.PowerDiff * 0.8F, market.Attributes.Upper_Price_Bound)).ToList()));

            if (market.IsDayAheadMarket())
                bids.Add(Bid_DayAhead.Hour(this.Assets, market, schedule.Select(el => (el.TimeStamp, el.PowerDiff, market.Attributes.Upper_Price_Bound)).ToList()));

            if (market.IsIntradayMarket())
                bids.Add(Bid_Intraday.QuarterHour(this.Assets, market, schedule.Select(el => (el.TimeStamp, el.PowerDiff, market.Attributes.Upper_Price_Bound))));
        }
    }
}
