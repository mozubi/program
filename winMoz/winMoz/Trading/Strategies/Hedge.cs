﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Information.PFCs;
using winMoz.Markets;
using winMoz.Markets.Bids;
using winMoz.Portfolios;
using winMoz.Markets.Elements;
using winMoz.Assets.Plants;
using winMoz.Markets.EoMMarkets;

namespace winMoz.Trading.Strategies
{
    public class Hedge : TradingStrategy
    {
        public enum StandardTypes { Base, PeakOffpeak, BasePeak, None }
        public StandardTypes StandardType { get; private set; } = StandardTypes.PeakOffpeak;
        public MPFCProvider MPFC { get; private set; } = Info.MPFC;
        public HPFCProvider HPFC { get; private set; } = Info.HPFC;
        public QHPFCProvider QHPFC { get; private set; } = Info.QHPFC;

        public Hedge(IEnumerable<IAsset> assets, params Type[] excludedMarkets) : base(assets,excludedMarkets) { }

        public Hedge(IEnumerable<IAsset> assets, StandardTypes hedgetype, params Type[] excludedMarkets) : this(assets, excludedMarkets)
        {
            this.StandardType = hedgetype;
        }

        protected override void CreateBids(Market market)
        {
            // TODO: implement limiting of hedge-bids
            var time = market.EventTimeframe.TimeStamp;
            var duration = market.EventTimeframe.Duration;
            var EndOfDuration = time.Add(duration);
            var step = market.EventTimeframe.Step;
            var AssetsDiffVRE = this.Assets.DifferByIsVariableRenewable();
            foreach(var AssetsDiff in AssetsDiffVRE)
            {
                List<IAsset> AssetsPart = AssetsDiff.AssetsPart;

                List<float> costs = AssetsPart.Select(asset => ((Block)asset).ProductionCostsPerMWh_el(time, duration).ToList().Resample(Resampling.TimeStep.QuarterHour).Select(item => item.Item2).Average()).ToList();
                List<(DateTime TimeStamp, float WeightedPrice)> WeightedPriceTimeLine = GetWeightedPriceTimeLine(AssetsPart.Where(item => item is Block), time, duration);

                //var weightedPrice = GetWeightedValue(AssetsPart.Select(el => el.Attributes.Economics.LCOE), this.Assets.Select(el => el.Attributes.PowerMW));
                var weightedPrice = GetWeightedValue(costs, this.Assets.Select(el => el.Schedule.Where(item => item.TimeStamp >= time && item.TimeStamp < EndOfDuration).Select(item => item.PowerDiff).Average()).ToList());

                var schedule = AssetsPart.GetSumSchedule(time, duration);

                var bids = new ObservableCollection<Bid>();
                bids.CollectionChanged += (s, e) =>
                {
                    var newBid = (Bid)e.NewItems[0];
                    schedule.Procure(newBid.GetTimeSeries());
                    //TODO: Preissetzung überarbeiten.
                    newBid.Segments.ForEach(seg => seg.SetUnlimitedIfTooHigh(market));
                    AddBid(newBid);
                };

                List<(DateTime Timestamp, float Trade, float Price_Capacity, float Price_Energy)> result = new List<(DateTime TimeStamp, float Trade, float Price_Capacity, float Price_Energy)>();
                foreach (IAsset asset in AssetsDiff.AssetsPart)
                {
                    if (asset is Block)
                    {
                        if (market.IsControlReserve_Capacity())
                        {
                            Random rnd = new Random(asset.BalancingGroup.GetSeed());
                            bool Is_aFRR = rnd.NextDouble() >= 0.33; //Aufteilung nach 1/3 - 2/3
                            if (Is_aFRR)
                            {
                                result = winMoz.Portfolios.Optimization.Optimization.ControlReserve((Block)asset, time, duration, step, ControlReserveShortCode.ShortCode.aFRR);
                                var resultPositive = result.Where(item => item.Trade > 0).ToList();
                                var resultNegative = result.Where(item => item.Trade < 0).ToList();
                                if (resultPositive.Count() > 0) bids.Add(Bid_ControlReserve_Capacity.aFRR(new List<IAsset>() { asset }, market, step, resultPositive.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp, step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => item2.Price_Capacity).Average(), el2.Select(item2 => item2.Price_Energy).Average())).ToList()));
                                if (resultNegative.Count() > 0) bids.Add(Bid_ControlReserve_Capacity.aFRR(new List<IAsset>() { asset }, market, step, resultNegative.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp, step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => item2.Price_Capacity).Average(), el2.Select(item2 => item2.Price_Energy).Average())).ToList()));
                                //asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, el.Trade, el.Price_Capacity)).ToList());
                            }
                            else
                            {
                                result = winMoz.Portfolios.Optimization.Optimization.ControlReserve((Block)asset, time, duration, step, ControlReserveShortCode.ShortCode.mFRR);
                                var resultPositive = result.Where(item => item.Trade > 0).ToList();
                                var resultNegative = result.Where(item => item.Trade < 0).ToList();
                                if (resultPositive.Count() > 0) bids.Add(Bid_ControlReserve_Capacity.mFRR(new List<IAsset>() { asset }, market, step, resultPositive.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp, step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => item2.Price_Capacity).Average(), el2.Select(item2 => item2.Price_Energy).Average())).ToList()));
                                if (resultNegative.Count() > 0) bids.Add(Bid_ControlReserve_Capacity.mFRR(new List<IAsset>() { asset }, market, step, resultNegative.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp, step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => item2.Price_Capacity).Average(), el2.Select(item2 => item2.Price_Energy).Average())).ToList()));
                                //asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, el.Trade, el.Price_Capacity)).ToList());
                            }
                        }
                    }
                }

                if (market.IsFuturesMarket())
                    bids.Add(Bid_Future.Hour(AssetsPart, market, schedule.Join(WeightedPriceTimeLine, item => item.TimeStamp, item => item.TimeStamp, (sch, prices) => (sch.TimeStamp, sch.PowerDiff, prices.WeightedPrice)).ToList()));

                if (!AssetsDiff.IsVariableRenewable && !(market is FuturesMarket))
                {
                    if (this.StandardType != StandardTypes.None)
                        if (Type.GetType(market.Attributes.AllowedBidType).GetInterfaces().Any(x => x == typeof(IStandardBlockBid)))
                        {
                            var bid = Standard(market, time, duration, weightedPrice, schedule.ToList());
                            if (bid.Segments.Any(seg => float.IsNaN(seg.VolumeMW))) Debugger.Break();
                            bids.Add(bid);
                        }
                }

                if (market.IsDayAheadMarket())
                    bids.Add(Bid_DayAhead.Hour(AssetsPart, market, schedule.Join(WeightedPriceTimeLine, item => item.TimeStamp, item => item.TimeStamp, (sch, prices) => (sch.TimeStamp, sch.PowerDiff, prices.WeightedPrice)).ToList()));


                if (market.IsIntradayMarket())
                    bids.Add(Bid_Intraday.QuarterHour(AssetsPart, market, schedule.Join(WeightedPriceTimeLine, item => item.TimeStamp, item => item.TimeStamp, (sch, prices) => (sch.TimeStamp, sch.PowerDiff, prices.WeightedPrice)).ToList()));
            }
        }

        public static List<(DateTime TimeStamp, float WeightedPrice)> GetWeightedPriceTimeLine(IEnumerable<IAsset> assets, DateTime startTime, TimeSpan duration)
        {
            var EndOfDuration = startTime.Add(duration);
            var weightedPrices =  assets.Select(asset => asset.Schedule.Get(startTime, duration).Join(((Block)asset).ProductionCostsPerMWh_el(startTime, duration.Add(TimeSpan.FromMinutes(15))).ToList().Resample(Resampling.TimeStep.QuarterHour, Resampling.Method.FFill).ToList(), item => item.TimeStamp, item => item.TimeStamp, (sch, prices) => new { sch.TimeStamp, Revenues = sch.PowerDiff * prices.Item2, PowerDiff = sch.PowerDiff })).ToList().SelectMany(item => item).GroupBy(item => item.TimeStamp, (group, values) => (group, values.Select(item => item.PowerDiff).Sum() != 0 ? values.Select(el => el.Revenues).Sum() / values.Select(item => item.PowerDiff).Sum() : 0.0F)).ToList();
            return weightedPrices;
        }


        private Bid Standard(Market market, DateTime time, TimeSpan duration, float weightedPrice, List<ScheduleItem> schedule)
        {
            var bid = (Bid)Activator.CreateInstance(Type.GetType(market.Attributes.AllowedBidType), this.Assets, market);
            switch (StandardType)
            {
                case StandardTypes.Base:
                    Base(schedule, this.Assets.First().BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, time, duration), out float basePower);
                    ((IStandardBlockBid)bid).Base(time, market, basePower, weightedPrice);
                    break;
                default:
                case StandardTypes.PeakOffpeak:
                    PeakOffpeak(schedule, this.Assets.First().BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, time, duration), out float peakPower, out float offPeakPower);
                    ((IStandardBlockBid)bid).PeakOffPeak(time, market, peakPower, weightedPrice, offPeakPower, weightedPrice);
                    break;
                case StandardTypes.BasePeak:
                    BasePeak(schedule, this.Assets.First().BalancingGroup.GetPriceCurve(PriceProviderType.HPFC, time, duration), out basePower, out peakPower);
                    ((IStandardBlockBid)bid).Base(time, market, basePower, weightedPrice);
                    ((IStandardBlockBid)bid).Peak(time, market, peakPower, weightedPrice);
                    break;
            }
            return bid;
        }

        /// <summary>
        /// Methode zerlegt den Fahrplan eines Assets wertneutral in Offpeak- und Peak-Leistung und gibt handelbare Trades zurück
        /// </summary>
        /// <param name="start">Startzeit des Hedging-Zeitraums</param>
        /// <param name="duration">Dauer des Hedging-Zeitraums</param>
        /// <returns>Liste mit Offpeak-Peak-Trades</returns>
        private void PeakOffpeak(List<ScheduleItem> schedule, IEnumerable<(DateTime TimeStamp, float PricePrediction)> prices, out float peakPower, out float offPeakPower)
        {
            var offPeakLoad = schedule.Select(el => (el.TimeStamp, el.PowerDiff)).ToList().Resample(Resampling.TimeStep.Hour).Where(el => el.TimeStamp.IsOffPeakTime()).Select(el => el.Item2);
            var peakLoad = schedule.Select(el => (el.TimeStamp, el.PowerDiff)).ToList().Resample(Resampling.TimeStep.Hour).Where(el => el.TimeStamp.IsPeakTime()).Select(el => el.Item2);

            peakPower = peakLoad.Count() == 0 ? 0.0F : GetWeightedValue(peakLoad, prices.Where(el => el.TimeStamp.IsPeakTime()).Select(el => el.PricePrediction));
            offPeakPower = GetWeightedValue(offPeakLoad, prices.Where(el => el.TimeStamp.IsOffPeakTime()).Select(el => el.PricePrediction));

            if (float.IsNaN(peakPower) || float.IsNaN(offPeakPower)) Debugger.Break();
        }

        private void Base(List<ScheduleItem> schedule, IEnumerable<(DateTime TimeStamp, float PricePrediction)> prices, out float basePower)
        {
            basePower = GetWeightedValue(schedule.Select(el => (el.TimeStamp, el.PowerDiff)).ToList().Resample(Resampling.TimeStep.Hour).Select(el => el.Item2), prices.Select(el => el.PricePrediction));
        }

        private void BasePeak(List<ScheduleItem> schedule, IEnumerable<(DateTime TimeStamp, float PricePrediction)> prices, out float basePower, out float peakPower)
        {
            PeakOffpeak(schedule, prices, out peakPower, out float offPeakPower);
            basePower = offPeakPower;
            peakPower -= offPeakPower;
        }

        private static float GetWeightedValue(IEnumerable<float> values, IEnumerable<float> weights) => weights.Sum() != 0 ? values.Zip(weights, (val, wght) => val * wght).Sum() / weights.Sum() : 0.0F;

    }
}
