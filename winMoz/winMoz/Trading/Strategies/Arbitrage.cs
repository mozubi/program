﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using winMoz.Assets;
using winMoz.Assets.Plants;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Markets;
using winMoz.Markets.Bids;
using winMoz.Portfolios;
using winMoz.Simulation;
using winMoz.Tools.Viz;
using winMoz.Trading.Optimization;
using winMoz.Markets.Elements;
using winMoz.Markets.EoMMarkets;

namespace winMoz.Trading.Strategies
{
    public class Arbitrage : TradingStrategy
    {
        public Arbitrage(IEnumerable<IAsset> assets, params Type[] excludedMarkets) : base(assets, excludedMarkets.Append(typeof(FuturesMarket)).ToArray())
        {
        }

        protected override void CreateBids(Market callingMarket)
        {
            var time = callingMarket.EventTimeframe.TimeStamp;
            var duration = callingMarket.EventTimeframe.Duration;
            var EndOfDuration = Extensions.GetEndOfDuration(time, duration);

            var weightedPrice = GetWeightedValue(this.Assets.Select(el => el.Economics.LCOE), this.Assets.Select(el => el.PowerMW));
            var bids = new List<Bid>();

            foreach (var asset in this.Assets)
                if (asset.IsFlexibility)
                {
                    try
                    {
                        if (callingMarket.IsDayAheadMarket() && !this.ExcludedMarkets.Contains(typeof(DayAheadMarket)) && this.ExcludedMarkets.Contains(typeof(IntradayMarket)))
                        {
                            var result = ((Plant_Storage)asset).OptimizeDayAhead(time, duration);
                            bids.Add(Bid_DayAhead.Hour(asset, callingMarket, result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power, weightedPrice)).ToList()));
                            asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power)).ToList());
                        }
                        else if (callingMarket.IsIntradayMarket() && !this.ExcludedMarkets.Contains(typeof(IntradayMarket)) && this.ExcludedMarkets.Contains(typeof(DayAheadMarket)))
                        {
                            var result = ((Plant_Storage)asset).OptimizeIntraDay(time, duration);
                            bids.Add(Bid_Intraday.QuarterHour(asset, callingMarket, result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power, weightedPrice)).ToList()));
                            asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power)).ToList());
                        }
                        else if (!callingMarket.IsFuturesMarket() && !callingMarket.IsControlReserveMarket())
                        {
                            var result = ((Plant_Storage)asset).OptimizeSpot(time, duration, TimeSpan.FromMinutes(15), callingMarket.IsIntradayMarket());
                            if (callingMarket.IsDayAheadMarket())
                            {
                                bids.Add(Bid_DayAhead.Hour(asset, callingMarket, result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power_DA, weightedPrice)).ToList()));
                                asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power_DA)).ToList());
                            }
                            else if (callingMarket.IsIntradayMarket())
                            {
                                bids.Add(Bid_Intraday.QuarterHour(asset, callingMarket, result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power_ID, weightedPrice))));
                                asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, -el.Power_ID)).ToList());
                            }


                            //winMoz.Tools.Viz.Plotter.Plot(result, "Power_Total", "Power_DA", "Power_ID", "Capacity", "Revenue", "Costs");
                        }
                    }
                    catch (System.AccessViolationException)
                    {
                        Debugger.Break();
                        throw;
                    }
                }
                else
                    Log.Error($"Multimarket bidding for asset of type {asset.GetType().FullName} is not supported.");

            //TODO: Preissetzung überarbeiten
            //bids.SelectMany(bid => bid.Segments).ToList().ForEach(seg => seg.SetUnlimited());
            bids.ForEach(bid => bid.Segments.ForEach(seg => seg.SetUnlimited(callingMarket)));
            AddBid(bids);
        }

        private static float GetWeightedValue(IEnumerable<float> values, IEnumerable<float> weights) => values.Zip(weights, (val, wght) => val * wght).Sum() / weights.Sum();

    }
}
