﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Portfolios;
using winMoz.Markets;
using winMoz.Assets;
using winMoz.Markets.Bids;
using winMoz.Helper;
using winMoz.Markets.Elements;
using winMoz.Assets.Plants;

namespace winMoz.Trading.Strategies
{
    public class MakeOrBuy: TradingStrategy
    {
        public MakeOrBuy(IEnumerable<IAsset> assets, params Type[] excludedMarkets) : base(assets, excludedMarkets) { }
        protected override void CreateBids(Market callingMarket)
        {
            DateTime startTime = callingMarket.EventTimeframe.TimeStamp;
            TimeSpan duration = callingMarket.EventTimeframe.Duration;
            TimeSpan step = callingMarket.EventTimeframe.Step;
            var EndOfDuration = startTime.Add(duration);
            if (callingMarket.IsDayAheadMarket())
            {
                foreach (IAsset asset in this.Assets)
                {
                    if (asset is Assets.Plants.Block)
                    {
                        if (!((Assets.Plants.Block)asset).Plant.IsIndustry)
                        {
                            //Gesicherte Leistung abrufen
                            var powerProcured = asset.Schedule.Get(startTime, duration).Select(el => (el.TimeStamp, el.PowerProcured));
                            //Reglreserve abrufen
                            var powerReserved = asset.ControlReserveSchedule.Get(startTime, duration).Select(el => (el.TimeStamp, el.PowerReservedOrActivatedPositive, el.PowerReservedOrActivatedNegative)).ToList();
                            //Grenzkostenkurve abrufen, Leistung zu Kosten
                            List<(DateTime time, List<(float volume, float cost)>)> marginalCostsPerMWh = asset.GetCostCurvePerMWh(startTime, duration, step);
                            // Volumenkurve ist immer gleich
                            var powerCurve = marginalCostsPerMWh.First().Item2.Select(el => el.volume).ToList();

                            //Korrektur Grenzen durch Regelreserve
                            var marginalCostsPerMWhCorrectedFRR = powerReserved.Join(marginalCostsPerMWh, el1 => el1.TimeStamp, el2 => el2.time, (el, el2) => new { el.TimeStamp, min = Math.Abs(el.PowerReservedOrActivatedNegative) + el2.Item2.First().volume, max = -el.PowerReservedOrActivatedPositive + el2.Item2.Last().volume, costcurve = el2.Item2 })
                                .Select(el => (el.TimeStamp, el.costcurve.Where(item => item.volume >= el.min && item.volume <= el.max).ToList())).ToList();

                            // Aktuelle Kosten bei momentaner Leistung herausfinden
                            var diffList = powerProcured.Join(marginalCostsPerMWhCorrectedFRR, el1 => el1.TimeStamp, el2 => el2.TimeStamp, (el, el2) => (el.TimeStamp, el.PowerProcured, el2.Item2))
                                            .Select(el1 => (el1.Item3
                                            .Select(el2 => (Math.Abs(el1.PowerProcured - el2.volume))))).ToList();
                            var countsDiffList = diffList.Select(items => items.Count());
                            var minimalValueIndex = diffList.Select(el => el.Count() > 0 ? el.ToList().IndexOf(el.Min()) : -1).ToList();

                            //Intervallgröße bestimmen
                            float powerIntervals = powerCurve.ElementAt(1) - powerCurve.First();


                            List<(DateTime, float volume, float price)> bidlist = new List<(DateTime, float, float)>();

                            for (int a = 0; a < marginalCostsPerMWhCorrectedFRR.Count; a++)
                            {
                                (DateTime time, List<(float volume, float cost)>) ellist = marginalCostsPerMWhCorrectedFRR.ElementAt(a);
                                if (ellist.Item2.Count > 1)
                                {
                                    int indexOfProcured = minimalValueIndex.ElementAt(a);
                                    if (indexOfProcured >= 0)
                                    {
                                        //aktuelle Grenzkosten bei gesicherter Leistung 
                                        float costAtProcured = ellist.Item2.ElementAt(indexOfProcured).cost * ellist.Item2.ElementAt(indexOfProcured).volume;

                                        //Buy Gebote
                                        float bidPrice = float.MinValue;
                                        //maximaler Gebotspreis; stellt sicher, dass gestaffelte Gebote nur kleiner werden können
                                        float maxPrice = float.MaxValue;
                                        float bidVolume = 0;
                                        Bid_DayAhead bid = new Bid_DayAhead(asset, callingMarket);
                                        for (int i = indexOfProcured - 1; i >= 0; i--)
                                        {

                                            float neededPrice = (costAtProcured - ellist.Item2.ElementAt(i).cost * ellist.Item2.ElementAt(i).volume) / ((indexOfProcured - i) * powerIntervals);
                                            if (neededPrice > bidPrice)
                                            {
                                                bidPrice = (neededPrice > maxPrice) ? maxPrice : neededPrice;
                                                bidVolume -= powerIntervals;
                                            }
                                            else
                                            {
                                                bid.Hour(ellist.time, bidVolume, bidPrice);
                                                //asset.Schedule.AddPower(new List<(DateTime, float, float)>() { (ellist.time, -bidVolume, bidPrice) });
                                                bidVolume = -powerIntervals;
                                                bidPrice = neededPrice;
                                                //wenn einmal niedrgier angeboten wurde 
                                                maxPrice = bidPrice;
                                            }
                                        }
                                        bid.Hour(ellist.time, bidVolume, bidPrice);

                                        //Sell Gebote
                                        bidPrice = float.MaxValue;
                                        bidVolume = 0;
                                        for (int i = indexOfProcured + 1; i < ellist.Item2.Count; i++)
                                        {
                                            float neededPrice = (costAtProcured - ellist.Item2.ElementAt(i).cost * ellist.Item2.ElementAt(i).volume) / ((indexOfProcured - i) * powerIntervals);
                                            if (neededPrice < bidPrice)
                                            {
                                                bidPrice = neededPrice;
                                                bidVolume += powerIntervals;
                                            }
                                            else
                                            {
                                                bid.Hour(ellist.time, bidVolume, bidPrice);
                                                bidVolume = powerIntervals;
                                                bidPrice = neededPrice;
                                            }

                                            //bidlist.Add((ellist.time, -powerIntervals, bidPrice));
                                            bid.Hour(ellist.time, powerIntervals, bidPrice);
                                        }
                                        _bids.Add(bid);
                                        //winMoz.Tools.Viz.Plotter_Extensions.PlotScatter(new List<List<(float, float)>>() { _bids.SelectMany(item => item.Segments).Select(item => (item.VolumeMW, item.Price)).ToList() }, new List<string>() { "Gebote eines Kraftwerks" }, "Menge in MW", "Preis in EUR/MWh", plotSize:8);
                                        //var bidSegments = _bids.SelectMany(item => item.Segments).ToList();
                                        //foreach (var segment in bidSegments) Console.WriteLine("Menge in MW: " + segment.VolumeMW + ", Preis in EUR: " + segment.Price + "IstKaufgebot:" + segment.IsBuy);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                }
            }
            else if (callingMarket.IsIntradayMarket())
            {
                foreach (IAsset asset in this.Assets)
                {
                    if (asset is Assets.Plants.Block)
                    {
                        if (!((Assets.Plants.Block)asset).Plant.IsIndustry)
                        {
                            //Gesicherte Leistung abrufen
                            var powerProcured = asset.Schedule.Get(startTime, duration).Select(el => (el.TimeStamp, el.PowerProcured));
                            //Reglreserve abrufen
                            var powerReserved = asset.ControlReserveSchedule.Get(startTime, duration).Select(el => (el.TimeStamp, el.PowerReservedOrActivatedPositive, el.PowerReservedOrActivatedNegative)).ToList();
                            //Grenzkostenkurve abrufen, Leistung zu Kosten
                            List<(DateTime time, List<(float volume, float cost)>)> marginalCostsPerMWh = asset.GetCostCurvePerMWh(startTime, duration, step);
                            // Volumenkurve ist immer gleich
                            var powerCurve = marginalCostsPerMWh.First().Item2.Select(el => el.volume).ToList();
                            //Korrektur Grenzen durch Regelreserve
                            var marginalCostsPerMWhCorrectedFRR = powerReserved.Join(marginalCostsPerMWh, el1 => el1.TimeStamp, el2 => el2.time, (el, el2) => new { el.TimeStamp, min = Math.Abs(el.PowerReservedOrActivatedNegative) + el2.Item2.First().volume, max = -el.PowerReservedOrActivatedPositive + el2.Item2.Last().volume, costcurve = el2.Item2 })
                                .Select(el => (el.TimeStamp, el.costcurve.Where(item => item.volume >= el.min && item.volume <= el.max).ToList())).ToList();

                            // Aktuelle Kosten bei momentaner Leistung herausfinden
                            var diffList = powerProcured.Join(marginalCostsPerMWhCorrectedFRR, el1 => el1.TimeStamp, el2 => el2.TimeStamp, (el, el2) => (el.TimeStamp, el.PowerProcured, el2.Item2))
                                            .Select(el1 => (el1.Item3
                                            .Select(el2 => (Math.Abs(el1.PowerProcured - el2.volume))))).ToList();

                            if (powerProcured.Any(item => item.PowerProcured > 0.5 * asset.PowerMW))
                            {

                            }
                            var countsDiffList = diffList.Select(items => items.Count());
                            var minimalValueIndex = diffList.Select(el => el.Count() > 0 ? el.ToList().IndexOf(el.Min()) : -1).ToList();

                            //Intervallgröße bestimmen
                            float powerIntervals = powerCurve.ElementAt(1) - powerCurve.First();


                            List<(DateTime, float volume, float price)> bidlist = new List<(DateTime, float, float)>();

                            for (int a = 0; a < marginalCostsPerMWhCorrectedFRR.Count; a++)
                            {
                                (DateTime time, List<(float volume, float cost)>) ellist = marginalCostsPerMWhCorrectedFRR.ElementAt(a);
                                if (ellist.Item2.Count > 1)
                                {
                                    int indexOfProcured = minimalValueIndex.ElementAt(a);

                                    //aktuelle Grenzkosten bei gesicherter Leistung 
                                    float costAtProcured = ellist.Item2.ElementAt(indexOfProcured).cost * ellist.Item2.ElementAt(indexOfProcured).volume;

                                    //Buy Gebote
                                    float bidPrice = float.MinValue;
                                    //maximaler Gebotspreis; stellt sicher, dass gestaffelte Gebote nur kleiner werden können
                                    float maxPrice = float.MaxValue;
                                    float bidVolume = 0;
                                    Bid_Intraday bid = new Bid_Intraday(asset, callingMarket);
                                    for (int i = indexOfProcured - 1; i >= 0; i--)
                                    {

                                        float neededPrice = (costAtProcured - ellist.Item2.ElementAt(i).cost * ellist.Item2.ElementAt(i).volume) / ((indexOfProcured - i) * powerIntervals);
                                        if (neededPrice > bidPrice)
                                        {
                                            bidPrice = (neededPrice > maxPrice) ? maxPrice : neededPrice;
                                            bidVolume -= powerIntervals;
                                        }
                                        else
                                        {
                                            bid.QuarterHour(ellist.time, bidVolume, bidPrice);
                                            //asset.Schedule.AddPower(new List<(DateTime, float, float)>() { (ellist.time, -bidVolume, bidPrice) });
                                            bidVolume = -powerIntervals;
                                            bidPrice = neededPrice;
                                            //wenn einmal niedrgier angeboten wurde 
                                            maxPrice = bidPrice;
                                        }
                                    }
                                    bid.QuarterHour(ellist.time, bidVolume, bidPrice);

                                    //Sell Gebote
                                    bidPrice = float.MaxValue;
                                    bidVolume = 0;
                                    for (int i = indexOfProcured + 1; i < ellist.Item2.Count; i++)
                                    {
                                        float neededPrice = (costAtProcured - ellist.Item2.ElementAt(i).cost * ellist.Item2.ElementAt(i).volume) / ((indexOfProcured - i) * powerIntervals);
                                        if (neededPrice < bidPrice)
                                        {
                                            bidPrice = neededPrice;
                                            bidVolume += powerIntervals;
                                        }
                                        else
                                        {
                                            bid.QuarterHour(ellist.time, bidVolume, bidPrice);
                                            bidVolume = powerIntervals;
                                            bidPrice = neededPrice;
                                        }

                                        //bidlist.Add((ellist.time, -powerIntervals, bidPrice));

                                    }

                                    bid.QuarterHour(ellist.time, powerIntervals, bidPrice);
                                    bid.Segments.ForEach(seg => seg.SetUnlimitedIfTooHigh(callingMarket));
                                    _bids.Add(bid);
                                    //winMoz.Tools.Viz.Plotter_Extensions.PlotScatter(new List<List<(float, float)>>() { _bids.SelectMany(item => item.Segments).Select(item => (item.VolumeMW, item.Price)).ToList() }, new List<string>() { "Gebote eines Kraftwerks" }, "Menge in MW", "Preis in EUR/MWh", plotSize:8);
                                    //var bidSegments = _bids.SelectMany(item => item.Segments).ToList();
                                    //foreach (var segment in bidSegments) Console.WriteLine("Menge in MW: " + segment.VolumeMW + ", Preis in EUR: " + segment.Price + "IstKaufgebot:" + segment.IsBuy);
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
            }
            else if (callingMarket.IsFuturesMarket())
            {
                foreach (IAsset asset in this.Assets)
                    if (asset is Assets.Plants.Block && !((Assets.Plants.Block)asset).Plant.IsIndustry)
                        _bids.Add(Bid_Future.Hour(asset, callingMarket, asset.Schedule.Get(startTime, duration).Select(item => (item.TimeStamp, item.Power, ((Block)asset).ProductionCostsPerMWh_el(item.TimeStamp, TimeSpan.FromMinutes(15)).First().Item2)).ToList()));
            }
            else if (callingMarket.IsControlReserve_Capacity())
            {
                List<(DateTime Timestamp, float Trade, float Price_Capacity, float Price_Energy)> result = new List<(DateTime TimeStamp, float Trade, float Price_Capacity, float Price_Energy)>();
                foreach (IAsset asset in this.Assets)
                {
                    if (asset is Block)
                    {
                        Random rnd = new Random(asset.BalancingGroup.GetSeed());
                        bool Is_aFRR = rnd.NextDouble() >= 0.33;  //Aufteilung nach 1/3 - 2/3
                        if (Is_aFRR)
                        {
                            result = winMoz.Portfolios.Optimization.Optimization.ControlReserve((Block)asset, startTime, duration, step, ControlReserveShortCode.ShortCode.aFRR);
                            var resultPositive = result.Where(item => item.Trade > 0).ToList();
                            var resultNegative = result.Where(item => item.Trade < 0).ToList();
                            if (resultPositive.Count() > 0) _bids.Add(Bid_ControlReserve_Capacity.aFRR(new List<IAsset>() { asset }, callingMarket, step, resultPositive.Where(item => item.Timestamp >= startTime && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp, step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Capacity, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Energy, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average())).ToList()));
                            if (resultNegative.Count() > 0) _bids.Add(Bid_ControlReserve_Capacity.aFRR(new List<IAsset>() { asset }, callingMarket, step, resultNegative.Where(item => item.Timestamp >= startTime && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp, step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Capacity, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Energy, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average())).ToList()));
                            //asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, el.Trade, el.Price_Capacity)).ToList());
                        }
                        else
                        {
                            result = winMoz.Portfolios.Optimization.Optimization.ControlReserve((Block)asset, startTime, duration, step, ControlReserveShortCode.ShortCode.mFRR);
                            var resultPositive = result.Where(item => item.Trade > 0).ToList();
                            var resultNegative = result.Where(item => item.Trade < 0).ToList();
                            if (resultPositive.Count() > 0) _bids.Add(Bid_ControlReserve_Capacity.mFRR(new List<IAsset>() { asset }, callingMarket, step, resultPositive.Where(item => item.Timestamp >= startTime && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp,step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Capacity, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Energy, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average())).ToList()));
                            if (resultNegative.Count() > 0) _bids.Add(Bid_ControlReserve_Capacity.mFRR(new List<IAsset>() { asset }, callingMarket, step, resultNegative.Where(item => item.Timestamp >= startTime && item.Timestamp < EndOfDuration).GroupBy(el => Helper_Time_Translation.GetNumberOfTimeSpanDay(el.Timestamp, step), (el1, el2) => (Helper_Time_Translation.GetStartTimeOfTimeSpanDay(el2.First().Timestamp, step, el1), el2.Select(item2 => item2.Trade).First(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Capacity, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average(), el2.Select(item2 => Math.Max(Math.Min(item2.Price_Energy, callingMarket.Attributes.Upper_Price_Bound), callingMarket.Attributes.Lower_Price_Bound)).Average())).ToList()));
                            //asset.Schedule.AddPower(result.Where(item => item.Timestamp >= time && item.Timestamp < EndOfDuration).Select(el => (el.Timestamp, el.Trade, el.Price_Capacity)).ToList());
                        }
                    }
                }

            }
        }

    }
}
