﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace winMoz.Data.Access.Contexts
{

    public class WxDto
    {
        public string NUTS2 { get; set; }
        public DateTime TimeStamp { get; set; }
        public float Radiation_diffuse_horizontal { get; set; }
        public float Radiation_direct_horizontal { get; set; }
        public float Temperature { get; set; }
        public float Windspeed_10m { get; set; }

        private WxDto() { }

        public WxDto(DateTime timeStamp, List<float> values)
        {
            TimeStamp = timeStamp;
            Windspeed_10m = (float)values[0];
            Temperature = (float)values[1];
            Radiation_direct_horizontal = (float)values[2];
            Radiation_diffuse_horizontal = (float)values[3];
        }

        public WxDto(WxDto dto)
        {
            TimeStamp = dto.TimeStamp;
            Windspeed_10m = dto.Windspeed_10m;
            Temperature = dto.Temperature;
            Radiation_direct_horizontal = dto.Radiation_direct_horizontal;
            Radiation_diffuse_horizontal = dto.Radiation_diffuse_horizontal;
        }

        internal static IEnumerable<WxDto> GetFromView(int nutsStartIndex, List<(DateTime TimeStamp, List<float> Values)> rowCollection)
            => rowCollection.Select(row => new WxDto(row.TimeStamp, row.Values.Skip(nutsStartIndex - 1).Take(4).ToList())).ToList();

        internal static IEnumerable<WxDto> GetFromView(string NUTS2, List<DataRow> rowCollection)
        {
            var wxEntries = new List<WxDto>();
            foreach (var row in rowCollection)
            {
                var wx = new WxDto();
                wx.NUTS2 = NUTS2;
                wx.TimeStamp = (DateTime)row[0];

                foreach (var property in wx.GetType().GetProperties().Where(el => el.PropertyType == typeof(float)))
                    property.SetValue(wx, row[$"{NUTS2}_" + property.Name.ToLower()]);

                wxEntries.Add(wx);
            }
            return wxEntries;
        }
    }
}
