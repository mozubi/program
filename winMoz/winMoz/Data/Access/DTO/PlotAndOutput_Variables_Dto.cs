﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Simulation;

namespace winMoz.Data.Access.DTO
{
    public class PlotAndOutput_Variables_Dto 
    {
        public int Id { get; private set; }
        public PlotAndOutput_Element Element { get; private set; }

        public PlotAndOutput_Times Times { get; private set; }

        private PlotAndOutput_Variables_Dto() { }
        public PlotAndOutput_Variables_Dto(PlotAndOutput_Element element, PlotAndOutput_Times times)
        {
            Element = element;
            Times = times;
        }
    }
}
