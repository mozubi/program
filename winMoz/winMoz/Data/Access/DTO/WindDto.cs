﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using winMoz.Assets;
using winMoz.Data.Access.Repositories;

namespace winMoz.Data.Access.DTO
{
    public class WindDto : WindBaseDto
    {

        public virtual void SetCurve(IList<float> powerAtWind)
        {
            this.PowerCurve = new WindPowerCurve(powerAtWind);
        }

        public static WindDto Get(int modelId)
        {
            modelId = Math.Max(1, Math.Min(modelId, 25705));
            using (var repo = new WindDtoRepository())
                return repo.FirstOrDefault(el => el.WECmodelId == modelId);
        }


        public static WindDto GetRandom() => Get(new Random((int)DateTime.Now.Ticks).Next(1, 25705));
    }
}
