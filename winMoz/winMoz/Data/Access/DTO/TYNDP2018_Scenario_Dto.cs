﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Data.Access.DTO
{
    public class TYNDP2018_Scenario_Dto
    {
        public string Country { get; private set; }
        public float Biofuels { get; private set; }
        public float Gas { get; private set; }
        public float Hardcoal { get; private set; }
        public float Hydro_pump { get; private set; }
        public float Hydro_run { get; private set; }
        public float Hydro_turbine { get; private set; }
        public float Lignite { get; private set; }
        public float Nuclear { get; private set; }
        public float Oil { get; private set; }
        public float Other_nonRES { get; private set; }
        public float Other_RES { get; private set; }
        public float Solar_thermal { get; private set; }
        public float Solar_PV { get; private set; }
        public float Wind_Onshore { get; private set; }
        public float Wind_Offshore { get; private set; }
    }
}
