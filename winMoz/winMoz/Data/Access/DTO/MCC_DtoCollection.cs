﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO
{
    public class VBH_VRE_Dto 
    {
        public MarketAreaEnum ShortCode { get; private set; }
        public Int64 TYNDP_Forecast_Year { get; private set; }
        public Int64 TYNDP_Year { get; private set; }
        public EnumCollection_EOMMarkets.TYNDP_Scenario_Type TYNDP_Scenario { get; private set; }
        public Int64 TYNDP_Climate_Year { get; private set; }
        public EnumCollection_EOMMarkets.VREType VRE_Type { get; private set; }
        public float Generation_GWh { get; private set; }
        public float Capacity_MW { get; private set; }
        public float VBH_h { get; private set; }
        public float SimultaneityFactor { get; private set; }
        public float VBH_WithSimultaneityFactor_h { get; private set; }
    }

    public class Capacities_Dto 
    {
        public Int64 CapaID { get; private set; }
        public MarketAreaEnum ShortCode { get; private set; }
        public string src { get; private set; }
        public float ProductionType { get; private set; }
        public float Biomass { get; private set; }
        public float Fossil_Brown_Coal_Lignite { get; private set; }
        public float Fossil_Coal_derived_gas { get; private set; }
        public float Fossil_gas { get; private set; }
        public float Fossil_Hard_coal { get; private set; }
        public float Fossil_Oil { get; private set; }
        public float Fossil_Oil_shale { get; private set; }
        public float Fossil_Peat { get; private set; }
        public float Geothermal { get; private set; }
        public float Hydro_Pumped_Storage { get; private set; }
        public float Hydro_Run_of_river_and_poundage { get; private set; }
        public float Hydro_Water_Reservoir { get; private set; }
        public float Marine { get; private set; }
        public float Nuclear { get; private set; }
        public float Other { get; private set; }
        public float Other_renewable { get; private set; }
        public float Solar { get; private set; }
        public float Waste { get; private set; }
        public float Wind_Offshore { get; private set; }
        public float Wind_Onshore { get; private set; }
        public float Total_Grand_capacity { get; private set; }
    }

    public class ExportCapacity_Dto 
    {
        public Int64 ImExFixID { get; private set; }
        public MarketAreaEnum ShortCode_From { get; private set; }
        public MarketAreaEnum ShortCode_To { get; private set; }
        public float Capacity { get; private set; }
    }

    public class CFunction_Dto 
    {
        public Int64 C_FuncID { get; private set; }
        public MarketAreaEnum ShortCode_From { get; private set; }
        public MarketAreaEnum ShortCode_To { get; private set; }
        public float Wind_Prog_From { get; private set; }
        public float CapacityRel { get; private set; }

        public CFunction_Dto() { }

        public CFunction_Dto(Int64 c_FuncID, MarketAreaEnum shortCode_From, MarketAreaEnum shortCode_To, float wind_Prog_From, float capacityRel)
        {
            C_FuncID = c_FuncID;
            ShortCode_From = shortCode_From;
            ShortCode_To = shortCode_To;
            Wind_Prog_From = wind_Prog_From;
            CapacityRel = capacityRel;
        }
    }

    public class GenPara_Thermal_Dto 
    {
        public Int64 GenParaID { get; private set; }
        public EnumCollection_EOMMarkets.ThermalEnum GenerationType { get; private set; }
        public float wk { get; private set; }
        public float carbon{ get; private set; }
        public float var_costs { get; private set; }
        public float own_usage { get; private set; }
        public float relfuture{ get; private set; }
        public float normStab { get; private set; }
        public float minWK { get; private set; }
        public decimal makeBuy { get; private set; }
        public float outageSummer { get; private set; }
        public float outageWinter { get; private set; }
    }

    public class GenPara_Constant_Dto 
    {
        public Int64 GenParaID { get; private set; }
        public EnumCollection_EOMMarkets.ConstantEnum GenerationType { get; private set; }
        public float vbh { get; private set; }
    }

    public class Load_MCC_Dto 
    {
        public Int64 LoadValID { get; private set; }
        public MarketAreaEnum ShortCode { get; private set; }
        public Int64 Year { get; private set; }
        public float Demand_TWh { get; private set; }
        public float AvgLoadMW { get; private set; }
    }

    public class LoadScale_Dto 
    {
        public Int64 LoadScaleID { get; private set; }
        public MarketAreaEnum ShortCode { get; private set; }
        public float AvgMean_hist { get; private set; }
        public float Intercept { get; private set; }
        public float Temperature { get; private set; }
        public float wQuarter_0 { get; private set; }
        public float wQuarter_1 { get; private set; }
        public float wQuarter_2 { get; private set; }
        public float wQuarter_3 { get; private set; }
        public float wDay_0 { get; private set; }
        public float wDay_1 { get; private set; }
        public float wDay_2 { get; private set; }
        public float wDay_3 { get; private set; }
        public float wDay_4 { get; private set; }
        public float wDay_5 { get; private set; }
        public float wDay_6 { get; private set; }
        public float wHour_0 { get; private set; }
        public float wHour_1 { get; private set; }
        public float wHour_2 { get; private set; }
        public float wHour_3 { get; private set; }
        public float wHour_4 { get; private set; }
        public float wHour_5 { get; private set; }
        public float wHour_6 { get; private set; }
        public float wHour_7 { get; private set; }
        public float wHour_8 { get; private set; }
        public float wHour_9 { get; private set; }
        public float wHour_10 { get; private set; }
        public float wHour_11 { get; private set; }
        public float wHour_12 { get; private set; }
        public float wHour_13 { get; private set; }
        public float wHour_14 { get; private set; }
        public float wHour_15 { get; private set; }
        public float wHour_16 { get; private set; }
        public float wHour_17 { get; private set; }
        public float wHour_18 { get; private set; }
        public float wHour_19 { get; private set; }
        public float wHour_20 { get; private set; }
        public float wHour_21 { get; private set; }
        public float wHour_22 { get; private set; }
        public float wHour_23 { get; private set; }
    }

    public class SzenarioCountries_Dto 
    {
        public Int64 ScenID { get; private set; }
        public MarketAreaEnum ShortCode { get; private set; }
    }

    public class TotalSzenario_Dto 
    {
        public Int64 TotalID { get; private set; }
        public Int64 ImExFixID { get; private set; }
        public Int64 CapaID { get; private set; }
        public Int64 GenParaID { get; private set; }
        public Int64 LoadScaleID { get; private set; }
        public Int64 LoadValID { get; private set; }
        public Int64 LoadValYearStatic { get; private set; }
        public Int64 WeatherYear { get; private set; }
        public Int64 CountryID { get; private set; }
        public Int64 TYNDP_Forecast_Year { get; private set; }
        public Int64 TYNDP_Year { get; private set; }
        public EnumCollection_EOMMarkets.TYNDP_Scenario_Type TYNDP_Scenario { get; private set; }
        public Int64 TYNDP_Climate_Year { get; private set; }
        public Int64 C_FuncID { get; private set; }
    }

}
