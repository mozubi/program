﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Data.Access.DTO
{
    public class ResultPowerDto
    {
        public DateTime TimeStamp { get; private set; }
        public float Wind { get; private set; }
        public float Solar { get; private set; }
        public float Oil { get; private set; }
        public float Gas { get; private set; }
        public float Nuclear { get; private set; }
        public float Hard_Coal { get; private set; }
        public float Lignite { get; private set; }
        public float Thermal_Total { get; private set; }
        public float Storage { get; private set; }
        public float Load { get; private set; }

        public ResultPowerDto(DateTime timeStamp, float wind, float solar, float oil, float gas, float nuclear, float hard_Coal, float lignite, float thermal_Total, float storage, float load)
        {
            this.TimeStamp = timeStamp;
            this.Wind = wind;
            this.Solar = solar;
            this.Oil = oil;
            this.Gas = gas;
            this.Nuclear = nuclear;
            this.Hard_Coal = hard_Coal;
            this.Lignite = lignite;
            this.Thermal_Total = thermal_Total;
            this.Storage = storage;
            this.Load = load;
        }

    }
}
