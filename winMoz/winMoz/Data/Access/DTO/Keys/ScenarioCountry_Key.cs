﻿using System;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    public class ScenarioCountry_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 ScenID { get; private set; }
        [Key]
        [LoadColumn(1)]
        public MarketAreaEnum ShortCode { get; private set; }

        public ScenarioCountry_Key(Int64 scenID, MarketAreaEnum shortCode)
        {
            ScenID = scenID;
            ShortCode = shortCode;
        }
    }
}
