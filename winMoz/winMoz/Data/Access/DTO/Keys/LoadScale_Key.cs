﻿using System;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    public class LoadScale_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 LoadScaleID { get; private set; }
        [Key]
        [LoadColumn(1)]
        public MarketAreaEnum ShortCode { get; private set; }

        public LoadScale_Key(Int64 loadScaleID, MarketAreaEnum shortCode)
        {
            LoadScaleID = loadScaleID;
            ShortCode = shortCode;
        }
    }
}
