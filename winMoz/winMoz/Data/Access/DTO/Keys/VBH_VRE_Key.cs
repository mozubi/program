﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    public class VBH_VRE_Key
    {
        [Key]
        [LoadColumn(0)]
        public MarketAreaEnum ShortCode { get; private set; }
        [Key]
        [LoadColumn(1)]
        public Int64 TYNDP_Forecast_Year { get; private set; }
        [Key]
        [LoadColumn(2)]
        public Int64 TYNDP_Year { get; private set; }
        [Key]
        [LoadColumn(3)]
        public EnumCollection_EOMMarkets.TYNDP_Scenario_Type TYNDP_Scenario { get; private set; }
        [Key]
        [LoadColumn(4)]
        public Int64 TYNDP_Climate_Year { get; private set; }
        [Key]
        [LoadColumn(5)]
        public EnumCollection_EOMMarkets.VREType VRE_Type { get; private set; }

        public VBH_VRE_Key(
            MarketAreaEnum shortCode, 
            Int64 tyndp_Forecast_Year, 
            Int64 tyndp_Year, 
            EnumCollection_EOMMarkets.TYNDP_Scenario_Type tyndp_Scenario,
            Int64 tyndp_Climate_Year,
            EnumCollection_EOMMarkets.VREType vre_Type)
        {
            ShortCode = shortCode;
            TYNDP_Forecast_Year = tyndp_Forecast_Year;
            TYNDP_Year = tyndp_Year;
            TYNDP_Scenario = tyndp_Scenario;
            TYNDP_Climate_Year = tyndp_Climate_Year;
            VRE_Type = vre_Type;
        }
    }
}
