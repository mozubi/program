﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    class ExternalMarketResults_Key
    {
        [Key]
        [LoadColumn(0)]
        public DateTime TimeStamp { get; private set; }
        [Key]
        [LoadColumn(1)]
        public MarketAreaEnum MarketArea{ get; private set; }

        public ExternalMarketResults_Key(DateTime timeStamp, MarketAreaEnum marketArea)
        {
            this.TimeStamp = timeStamp;
            this.MarketArea = marketArea;
        }
    }
}
