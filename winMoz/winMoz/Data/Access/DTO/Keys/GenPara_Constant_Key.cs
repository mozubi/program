﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.EoMMarkets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    public class GenPara_Constant_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 GenParaID { get; private set; }
        [Key]
        [LoadColumn(1)]
        public EnumCollection_EOMMarkets.ConstantEnum GenerationType { get; private set; }

        public GenPara_Constant_Key(Int64 genParaID, EnumCollection_EOMMarkets.ConstantEnum generationType)
        {
            GenParaID = GenParaID;
            GenerationType = generationType;
        }

    }
}
