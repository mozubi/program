﻿using System;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    class ExportCapacity_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 ImExFixID { get; private set; }
        [Key]
        [LoadColumn(1)]
        public MarketAreaEnum ShortCodeFrom { get; private set; }
        [Key]
        [LoadColumn(2)]
        public MarketAreaEnum ShortCodeTo { get; private set; }

        public ExportCapacity_Key(Int64 imExFixID, MarketAreaEnum shortCodeFrom, MarketAreaEnum shortCodeTo)
        {
            ImExFixID = imExFixID;
            ShortCodeFrom = shortCodeFrom;
            ShortCodeTo = shortCodeTo;
        }
    }
}
