﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets;

namespace winMoz.Data.Access.DTO
{
    public class ControlReserveDemand_Keys
    {
        public class Demand_Key
        {
            public DateTime TimeStamp { get; private set; }
            public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }

            public Demand_Key(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type)
            {
                this.TimeStamp = timeStamp;
                this.FRR_Type = frr_Type;
            }
        }

        public class ActivationVolume_Key
        {
            public DateTime TimeStamp { get; private set; }
            public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }

            public ActivationVolume_Key(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type)
            {
                this.TimeStamp = timeStamp;
                this.FRR_Type = frr_Type;
            }
        }

        public class Limits_Key
        {
            public DateTime TimeStamp { get; private set; }
            public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }

            public Limits_Key(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type)
            {
                this.TimeStamp = timeStamp;
                this.FRR_Type = frr_Type;
            }
        }

        public class Balances_Key
        {
            public DateTime TimeStamp { get; private set; }
            public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }

            public Balances_Key(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type)
            {
                this.TimeStamp = timeStamp;
                this.FRR_Type = frr_Type;
            }
        }

    }
}
