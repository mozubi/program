﻿using System;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    public class Capacities_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 CapaID { get; private set; }
        [Key]
        [LoadColumn(1)]
        public MarketAreaEnum ShortCode { get; private set; }

        public Capacities_Key(Int64 capaID, MarketAreaEnum shortCode)
        {
            CapaID = capaID;
            ShortCode = shortCode;
        }
    }
}
