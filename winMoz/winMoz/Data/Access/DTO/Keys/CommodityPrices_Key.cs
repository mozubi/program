﻿using System;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;

namespace winMoz.Data.Access.DTO.Keys
{
    class CommodityPrices_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 CommodityID { get; private set; }
        [Key]
        [LoadColumn(1)]
        public Int64 DayINT{ get; private set; }

        public CommodityPrices_Key(Int64 comodityID, Int64 dayINT)
        {
            CommodityID = comodityID;
            DayINT = dayINT;
        }
    }
}
