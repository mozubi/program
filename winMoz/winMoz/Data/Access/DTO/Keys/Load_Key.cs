﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    public class Load_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 LoadValID { get; private set; }
        [Key]
        [LoadColumn(1)]
        public MarketAreaEnum ShortCode { get; private set; }
        [Key]
        [LoadColumn(2)]
        public Int64 Year { get; private set; }

        public Load_Key(Int64 loadValID, MarketAreaEnum shortCode, Int64 year)
        {
            LoadValID = loadValID;
            ShortCode = shortCode;
            Year = year;
        }
    }
}
