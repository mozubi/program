﻿using System;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.DTO.Keys
{
    class CFunction_Key
    {
        [Key]
        [LoadColumn(0)]
        public Int64 C_FuncId { get; private set; }
        [Key]
        [LoadColumn(1)]
        public MarketAreaEnum ShortCodeFrom { get; private set; }
        [Key]
        [LoadColumn(2)]
        public MarketAreaEnum ShortCodeTo { get; private set; }
        [Key]
        [LoadColumn(3)]
        public float Wind_Prog_From { get; private set; }

        public CFunction_Key(Int64 cFuncId, MarketAreaEnum shortCodeFrom, MarketAreaEnum shortCodeTo,float wind_Prog_From)
        {
            C_FuncId = cFuncId;
            ShortCodeFrom = shortCodeFrom;
            ShortCodeTo = shortCodeTo;
            Wind_Prog_From = wind_Prog_From;
        }
    }
}
