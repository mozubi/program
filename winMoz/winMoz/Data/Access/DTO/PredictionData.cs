﻿using System;
using System.Linq;

namespace winMoz.Data.Access.DTO
{
    public abstract class PredictionData : IComparable<PredictionData>
    {
        public DateTime TimeStamp { get; set; }
        protected PredictionData()
        { }
        public PredictionData(DateTime TimeStamp)
        {
            this.TimeStamp = TimeStamp;
        }
        public Int64 Id { get; private set; }

        #region IComparable
        public int CompareTo(PredictionData other)
        {
            return TimeStamp.CompareTo(other.TimeStamp);
        }
        #endregion
    }

    public class LoadData : PredictionData
    {
        public LoadData(DateTime TimeStamp, float? volume):base(TimeStamp)
        {
            this.Volume = volume;
        }
        public float? Volume { get; set; }
    }

    public class SpotData : PredictionData
    {
        public SpotData(DateTime TimeStamp, float Price, float Volume): base(TimeStamp)
        {
            this.Price = Price;
            this.Volume = Volume;
        }
        public float Price { get; private set; }
        public float Volume { get; private set; }
    }
    public class IntradayData : SpotData
    {
        public IntradayData(DateTime TimeStamp, float Price, float Volume) : base(TimeStamp, Price, Volume)
        {
        }
    }

    public class DayAheadData : SpotData
    {
        public DayAheadData(DateTime TimeStamp, float Price, float Volume) : base(TimeStamp, Price, Volume)
        {
        }
    }

    public class DayAheadDataAndereQuelle2015
    {
        public Int64 Id { get; private set; }
        public string TimeStamp { get; private set; }
        public float Price { get; private set; }
        public float Volume { get; private set; }
    }

    public class REData : PredictionData
    {
        private REData()
        { }
        public REData(DateTime TimeStamp, float PV, float Wind) : base(TimeStamp) 
        {
            PV_Ante = PV;
            WI_Ante = Wind;
        }
        public float PV_Ante { get; private set; }
        public float PV_Post { get; private set; }
        public float WI_Ante { get; private set; }
        public float WI_Post { get; private set; }
    }

    public class RebapData : PredictionData
    {
        public RebapData(DateTime TimeStamp) : base(TimeStamp) { }
        public float Price { get; private set; }
    }

    public class OperatingReserveData : PredictionData
    {
        public float? AbgerMenge_Pos { get; private set; }
        public float? AbgerMenge_Neg { get; private set; }
        public float? Arbeitspreis_Pos { get; private set; }
        public float? Arbeitspreis_Neg { get; private set; }
        public float? VorgehMenge_Pos { get; private set; }
        public float? VorgehMenge_Neg { get; private set; }
        public float? Leistungspreis_Pos { get; private set; }
        public float? Leistungspreis_Neg { get; private set; }
        public OperatingReserveData(DateTime TimeStamp) : base(TimeStamp) { }
    }
    public class SRLData : OperatingReserveData
    {
        public SRLData(DateTime TimeStamp) : base(TimeStamp) { }
    }
    public class MRLData : OperatingReserveData
    {
        public MRLData(DateTime TimeStamp) : base(TimeStamp) { }
    }

    public class NRVData : PredictionData
    {
        public NRVData(DateTime TimeStamp) : base(TimeStamp) { }
        public float Saldo { get; private set; }
    }

    public class APMaxData : PredictionData
    {
        public float APMax { get; private set; }
    }

    public class GenerationUnits
    {
        public float GetHKWPowerSum()
        {
            return this.GetType().GetProperties().ToList().Where(el => el.Name.ToLowerInvariant().Contains("hkw"))
                 .Select(el => (float)el.GetValue(this)).Sum();
        }
        public DateTime TimeStamp { get; private set; }

        public float Block_1 { get; set; }
        public float Block_2 { get; set; }
        public float Block_3 { get; set; }
        public float Block_B { get; set; }
        public float Brokdorf { get; set; }
        public float DEFARGE____1_____ { get; set; }
        public float DEWHV______1______ { get; set; }
        public float DEZOLLI____1_____ { get; set; }
        public float Franken_I_Block_1 { get; set; }
        public float Franken_I_Block_2_GT { get; set; }
        public float Grohnde { get; set; }
        public float GuD_Block { get; set; }
        public float HKW_West_Block_1 { get; set; }
        public float HKW_West_Block_2 { get; set; }
        public float Heyden { get; set; }
        public float Huntorf_GT { get; set; }
        public float Ingolstadt_3 { get; set; }
        public float Ingolstadt_4 { get; set; }
        public float Irsching_3 { get; set; }
        public float Irsching_4 { get; set; }
        public float Irsching_5 { get; set; }
        public float Isar_2 { get; set; }
        public float KW_Hafen_Block_5 { get; set; }
        public float KW_Hafen_Block_6 { get; set; }
        public float KW_Hastedt_Block_14 { get; set; }
        public float KW_Hastedt_Block_15 { get; set; }
        public float KW_Mittelsbueren_Block_4 { get; set; }
        public float Kiel { get; set; }
        public float Maschine_1 { get; set; }
        public float Maschine_2 { get; set; }
        public float Nord_2_T20 { get; set; }
        public float Staudinger_4 { get; set; }
        public float Staudinger_5 { get; set; }
        public float Sued_GuD1_GT2 { get; set; }
        public float Sued_GuD1_GT3 { get; set; }
        public float Sued_GuD2_DT60 { get; set; }
        public float Sued_GuD2_GT61 { get; set; }
        public float Sued_GuD2_GT62 { get; set; }
        public float Waldeck_II_M5 { get; set; }
        public float Waldeck_II_M6 { get; set; }
        public float Wilhelmshaven { get; set; }
        public float GTHKW_Nossener_Bruecke { get; set; }
        public float HKW_Klingenberg_1_3 { get; set; }
        public float HKW_Lichterfelde_Block_1 { get; set; }
        public float HKW_Lichterfelde_Block_3 { get; set; }
        public float HKW_Mitte_GUD { get; set; }
        public float HKW_Moorburg_Block_A { get; set; }
        public float HKW_Moorburg_Block_B { get; set; }
        public float HKW_Nord_GuD_Nord { get; set; }
        public float HKW_Reuter_Block_C { get; set; }
        public float HKW_Reuter_West_Block_D { get; set; }
        public float HKW_Reuter_West_Block_E { get; set; }
        public float HKW_Tiefstack_Block_2 { get; set; }
        public float HKW_Tiefstack_GuD { get; set; }
        public float HKW_Wedel_Block_1 { get; set; }
        public float HKW_Wedel_Block_2 { get; set; }
        public float IKS_Schwedt_SE1_Block_1 { get; set; }
        public float IKS_Schwedt_SE2_Block_2 { get; set; }
        public float KW_Boxberg_Block_N { get; set; }
        public float KW_Boxberg_Block_P { get; set; }
        public float KW_Boxberg_Block_Q { get; set; }
        public float KW_Boxberg_Block_R { get; set; }
        public float KW_Jänschwalde_Block_A { get; set; }
        public float KW_Jänschwalde_Block_B { get; set; }
        public float KW_Jänschwalde_Block_C { get; set; }
        public float KW_Jänschwalde_Block_D { get; set; }
        public float KW_Jänschwalde_Block_E { get; set; }
        public float KW_Jänschwalde_Block_F { get; set; }
        public float KW_Lippendorf_Block_R { get; set; }
        public float KW_Lippendorf_Block_S { get; set; }
        public float KW_Schwarze_Pumpe_Block_A { get; set; }
        public float KW_Schwarze_Pumpe_Block_B { get; set; }
        public float Kraftwerk_Rostock { get; set; }
        public float PSW_Goldisthal_PSS_A { get; set; }
        public float PSW_Goldisthal_PSS_B { get; set; }
        public float PSW_Goldisthal_PSS_C { get; set; }
        public float PSW_Goldisthal_PSS_D { get; set; }
        public float PSW_Markersbach_PSS_A { get; set; }
        public float PSW_Markersbach_PSS_B { get; set; }
        public float PSW_Markersbach_PSS_C { get; set; }
        public float PSW_Markersbach_PSS_D { get; set; }
        public float PSW_Markersbach_PSS_E { get; set; }
        public float PSW_Markersbach_PSS_F { get; set; }
        public float Schkopau_A { get; set; }
        public float Schkopau_B { get; set; }
        public float GKM_AG_DBEnergie { get; set; }
        public float GKM_AG_TNG { get; set; }
        public float HKW_Altbach_Deizisau_Block_2 { get; set; }
        public float HKW_Altbach_Deizisau_Block_1 { get; set; }
        public float HKW_Heilbronn_Block_7 { get; set; }
        public float KKW_Neckarwestheim_2 { get; set; }
        public float KKW_Philippsburg_2 { get; set; }
        public float KW_Laufenburg { get; set; }
        public float KW_Rheinfelden_DE { get; set; }
        public float Kopswerk_2_M1 { get; set; }
        public float Kopswerk_2_M2 { get; set; }
        public float Kopswerk_2_M3 { get; set; }
        public float Kraftwerk_Säckingen { get; set; }
        public float Kraftwerk_Wahlheim_GT_D { get; set; }
        public float Kraftwerk_Wehr { get; set; }
        public float Kraftwerk_Witznau { get; set; }
        public float RDK_4 { get; set; }
        public float RDK_7 { get; set; }
        public float RDK_8 { get; set; }
        public float Rheinkraftwerk_Iffezheim { get; set; }
        public float Rodund_2 { get; set; }
        public float Stuttgart_Münster_DT { get; set; }
        public float Buschhaus { get; set; }
        public float GKB_Mittelsbueren_GuD { get; set; }
        public float HKW_Altbach_Deizisau_2 { get; set; }
        public float Obervermuntwerk_2_M1 { get; set; }
        public float Obervermuntwerk_2_M2 { get; set; }
        public float Lichterfelde_GUD { get; set; }
        public float HKW_West_Block_4 { get; set; }
        public float Küstenkraftwerk { get; set; }
    }

    public class Generation : PredictionData
    {
        public Generation(DateTime TimeStamp) : base(TimeStamp) { }
        public float? Biomass { get; set; }
        public float? Fossil_Brown_coal_Lignite { get; set; }
        public float? Fossil_Gas { get; set; }
        public float? Fossil_Hard_coal { get; set; }
        public float? Fossil_Oil { get; set; }
        public float? Geothermal { get; set; }
        public float? Hydro_Pumped_Storage { get; set; }
        public float? Hydro_Run_of_river_and_poundage { get; set; }
        public float? Hydro_Water_Reservoir { get; set; }
        public float? Nuclear { get; set; }
        public float? Other { get; set; }
        public float? Other_renewable { get; set; }
        public float? Solar { get; set; }
        public float? Waste { get; set; }
        public float? Wind_Offshore { get; set; }
        public float? Wind_Onshore { get; set; }
        public float? Marine { get; set; }
        public float? Fossil_Oil_shale { get; set; }
        public float? Fossil_Peat { get; set; }
        public float? Fossil_Coal_derived_gas { get; set; }
    }


    public class WxNinja : PredictionData
    {
        public WxNinja(DateTime TimeStamp) : base(TimeStamp) { }
        public float Precipitation { get; set; }
        public float Temperature { get; set; }
        public float Irradiance_surface { get; set; }
        public float Irradiance_toa { get; set; }
        public float Snowfall { get; set; }
        public float Snow_mass { get; set; }
        public float Cloud_cover { get; set; }
        public float Air_density { get; set; }
    }

    public class WestNetz : PredictionData
    {
        public WestNetz(DateTime TimeStamp) : base(TimeStamp) { }
        public float NetzverlusteMW { get; set; }
        public float ErzeugungMW { get; set; }
        public float SlpMW { get; set; }
        public float RlmMW { get; set; }
        public float DiffBk { get; set; }
    }
}

