﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets;
using winMoz.Data.Access.Repositories;

namespace winMoz.Data.Access.DTO
{
    public class WindAggDto : WindBaseDto
    {
        public float Roughness { get; set; }

        public WindAggDto() { }

        public WindAggDto(int id, string wECmodel, int wECmodelId, string tSO, int postcode, float locationHeight, float latitude, float longitude, float powerKW, float rotorDiameter, float hubHeight, float roughness)
        {
            this.Id = id;
            this.WECmodel = wECmodel;
            this.WECmodelId = wECmodelId;
            this.TSO = tSO;
            this.Postcode = postcode;
            this.LocationHeight = locationHeight;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.PowerKW = powerKW;
            this.RotorDiameter = rotorDiameter;
            this.HubHeight = hubHeight;
            this.Roughness = roughness;
        }

        public virtual void SetCurve(IList<float> powerAtWind)
        {
            this.PowerCurve = new WindPowerCurve(powerAtWind, isScaleIntegrated: true);
        }

        public static WindAggDto Get(int modelId)
        {
            modelId = Math.Max(1, Math.Min(modelId, 25705));
            using (var repo = new WindDtoRepository())
                return repo.FirstAggregatedOrDefault(el => el.WECmodelId == modelId);
        }

        public static WindAggDto GetRandom() => Get(new Random((int)DateTime.Now.Ticks).Next(1, 25705));
    }
}
