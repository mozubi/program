﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets;
using winMoz.Data.Access;

namespace winMoz.Data.Access.DTO
{

    public class TenderVolume_Dto : IComparable<TenderVolume_Dto>
    {
        public DateTime TimeStamp { get; protected set; }
        public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }
        public int TenderVolume { get; private set; }

        private TenderVolume_Dto() { }
        public TenderVolume_Dto(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type, int demand)
        {
            this.TimeStamp = timeStamp;
            this.FRR_Type = frr_Type;
            this.TenderVolume = demand;
        }

        #region IComparable
        public int CompareTo(TenderVolume_Dto other)
        {
            int val = TimeStamp.CompareTo(other.TimeStamp);
            if (val == 0)
            {
                return FRR_Type - other.FRR_Type;
            }
            else
            {
                return val;
            }
        }
        #endregion

    }

    public class ActivationVolume_Dto : IComparable<ActivationVolume_Dto>
    {
        public DateTime TimeStamp { get; protected set; }
        public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }
        public int ActivationVolume { get; private set; }

        private ActivationVolume_Dto() { }
        public ActivationVolume_Dto(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type, int activationVolume)
        {
            this.TimeStamp = timeStamp;
            this.FRR_Type = frr_Type;
            this.ActivationVolume = activationVolume;
        }
        #region IComparable
        public int CompareTo(ActivationVolume_Dto other)
        {
            int val = TimeStamp.CompareTo(other.TimeStamp);
            if (val == 0)
            {
                return FRR_Type - other.FRR_Type;
            }
            else
            {
                return val;
            }
        }
        #endregion
    }

    public class Limits_Dto : IComparable<Limits_Dto>
    {
        public DateTime TimeStamp { get; protected set; }
        public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }
        public bool LimitExceeded { get; private set; }

        private Limits_Dto() { }
        public Limits_Dto(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type, bool limitExceeded)
        {
            this.TimeStamp = timeStamp;
            this.FRR_Type = frr_Type;
            this.LimitExceeded = limitExceeded;
        }
        #region IComparable
        public int CompareTo(Limits_Dto other)
        {
            int val = TimeStamp.CompareTo(other.TimeStamp);
            if (val == 0)
            {
                return FRR_Type - other.FRR_Type;
            }
            else
            {
                return val;
            }
        }
        #endregion
    }

    public class Balances_Dto : IComparable<Balances_Dto>
    {
        public DateTime TimeStamp { get; protected set; }
        public ControlReserveShortCode.ShortCode FRR_Type { get; private set; }
        public float Balance { get; private set; }

        private Balances_Dto() { }
        public Balances_Dto(DateTime timeStamp, ControlReserveShortCode.ShortCode frr_Type, float balance)
        {
            this.TimeStamp = timeStamp;
            this.FRR_Type = frr_Type;
            this.Balance = balance;
        }
        #region IComparable
        public int CompareTo(Balances_Dto other)
        {
            int val = TimeStamp.CompareTo(other.TimeStamp);
            if (val == 0)
            {
                return FRR_Type - other.FRR_Type;
            }
            else
            {
                return val;
            }
        }
        #endregion

    }

}
