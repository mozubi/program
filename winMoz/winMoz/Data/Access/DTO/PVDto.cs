﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using winMoz.Assets;
using winMoz.Data.Access.Repositories;

namespace winMoz.Data.Access.DTO
{
    public class PVDto 
    {
        public int Id { get; set; }
        public float PowerMW { get; set; }
        public int PostCode { get; set; }
        public string EconomicsSubType { get; set; }
        public float ModuleAngle { get; set; }
        public float ModuleAzimut { get; set; }
        public float LoadFactor { get; set; }
    }
}
