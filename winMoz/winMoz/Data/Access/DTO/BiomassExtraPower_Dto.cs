﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Data.Access.DTO
{
    public class BiomassExtraPower_Dto
    {
        public int Year { get; private set; }
        public float BiomassExtraPowerKW { get; private set; }
    }
}
