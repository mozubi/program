﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Data.Access.DTO
{
    public class CommodityPrices_Dto
    {
        public Int64 CommodityID { get; private set; }
        public Int64 DayINT { get; private set; }
        public DateTime TS_normCET { get; private set; }
        public float carbon { get; private set; }
        public float hard_coal { get; private set; }
        public float natural_gas { get; private set; }
        public float lignite { get; private set; }
        public float oil { get; private set; }
        public float uranium { get; private set; }
    }
}
