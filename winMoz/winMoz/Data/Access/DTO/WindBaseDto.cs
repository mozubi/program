﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets;

namespace winMoz.Data.Access.DTO
{
    public class WindBaseDto 
    {
        public Int64 Id { get; set; }
        public string WECmodel { get; set; }
        public Int64 WECmodelId { get; set; }
        public string TSO { get; set; }
        public Int64 Postcode { get; set; }
        public float LocationHeight { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public float PowerKW { get; set; }
        public float RotorDiameter { get; set; }
        public float HubHeight { get; set; }

        public WindPowerCurve PowerCurve { get; set; }


    }
}
