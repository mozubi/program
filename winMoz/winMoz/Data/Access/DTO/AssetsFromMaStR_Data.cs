﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Data.Access.DTO
{
    public class Thermal_KWK_Nuts2_Dto
    {
        public string Energietraeger { get; private set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
        public float Nettonennleistung { get; set; }
        public bool IstIndustrie { get; private set; }
        public float KWK_ThermischeNutzleistung { get; set; }
        public float KWK_ElektrischeNutzleistung { get; set; }
        public string Nuts2_code { get; private set; }
        public bool IstGUD { get; private set; }
        public bool IstKWK { get; private set; }
        public int Inbetriebnahmejahrfuenft { get; set; }
        public int Stilllegungsjahrfuenft { get; set; }

        private Thermal_KWK_Nuts2_Dto() { }

        public Thermal_KWK_Nuts2_Dto(string energietraeger,
            int inbetriebnahmejahr,
            int stilllegungsjahr,
            float nettonennleistung,
            bool istIndustrie,
            float kwk_ThermischeNutzleistung,
            float kwk_ElektrischeNutzleistung,
            string nuts2_code,
            bool istGUD,
            bool istKWK,
            int inbetriebnahmejahrfuenft,
            int stilllegungsjahrfuenft)
        {
            Energietraeger = energietraeger;
            Inbetriebnahmejahr = inbetriebnahmejahr;
            Stilllegungsjahr = stilllegungsjahr;
            Nettonennleistung = nettonennleistung;
            IstIndustrie = istIndustrie;
            KWK_ThermischeNutzleistung = kwk_ThermischeNutzleistung;
            KWK_ElektrischeNutzleistung = kwk_ElektrischeNutzleistung;
            Nuts2_code = nuts2_code;
            IstGUD = istGUD;
            IstKWK = istKWK;
            Inbetriebnahmejahrfuenft = inbetriebnahmejahrfuenft;
            Stilllegungsjahrfuenft = stilllegungsjahrfuenft;
        }

        public void ReducePower(float nettonennleistung, float kwk_th, float kwk_el)
        {
            nettonennleistung -= nettonennleistung;
            KWK_ThermischeNutzleistung -= kwk_th;
            KWK_ElektrischeNutzleistung -= kwk_el;
        }
    }

    public class Thermal_KWK_All_Dto
    {
        public int MastrID { get; private set; }
        public string Energietraeger { get; private set; }
        public float Nettonennleistung { get; set; }
        public bool IstIndustrie { get; private set; }
        public float KWK_ThermischeNutzleistung { get; set; }
        public float KWK_ElektrischeNutzleistung { get; set; }
        public int Plz { get; private set; }
        public string Nuts2_code { get; private set; }
        public bool IstGUD { get; private set; }
        public bool IstKWK { get; private set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
        public int Inbetriebnahmejahrfuenft { get; set; }
        public int Stilllegungsjahrfuenft { get; set; }
    }

    public class Thermal_Kond_DE_Dto
    {
        public string Energietraeger { get; private set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
        public float Nettonennleistung { get; set; }
        public bool IstIndustrie { get; private set; }
        public bool IstGUD { get; private set; }
        public bool IstKWK { get; private set; }
        public int Inbetriebnahmejahrfuenft { get; set; }
        public int Stilllegungsjahrfuenft { get; set; }

        private Thermal_Kond_DE_Dto() { }

        public Thermal_Kond_DE_Dto(string energietraeger,
            int inbetriebnahmejahr,
            int stilllegungsjahr,
            float nettonennleistung,
            bool istIndustrie,
            bool istGUD,
            bool istKWK,
            int inbetriebnahmejahrfuenft,
            int stilllegungsjahrfuenft)
        {
            Energietraeger = energietraeger;
            Inbetriebnahmejahr = inbetriebnahmejahr;
            Stilllegungsjahr = stilllegungsjahr;
            Nettonennleistung = nettonennleistung;
            IstIndustrie = istIndustrie;
            IstGUD = istGUD;
            IstKWK = istKWK;
            Inbetriebnahmejahrfuenft = inbetriebnahmejahrfuenft;
            Stilllegungsjahrfuenft = stilllegungsjahrfuenft;
        }

        public void ReducePower(float nettonennleistung)
        {
            Nettonennleistung -= nettonennleistung;
        }
    }

    public class Thermal_Kond_All_Dto
    {
        public int MastrID { get; private set; }
        public string Energietraeger { get; private set; }
        public float Nettonennleistung { get; set; }
        public bool IstIndustrie { get; private set; }
        public int Plz { get; private set; }
        public string Nuts2_code { get; private set; }
        public bool IstGUD { get; private set; }
        public bool IstKWK { get; private set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
        public int Inbetriebnahmejahrfuenft { get; set; }
        public int Stilllegungsjahrfuenft { get; set; }
    }

    public class WindOnshore_Nuts2_Dto
    {
        public string Energietraeger { get; private set; }
        public float Nettonennleistung { get; set; }
        public string Nuts2_code { get; private set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
    }

    public class Hydro_RunOfRiver_DE_Dto
    {
        public string Energietraeger { get; private set; }
        public float Nettonennleistung { get; set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
        public bool IstEEG { get; set; }
    }

 public class PV_DE_Dto
    {
        public string Energietraeger { get; private set; }
        public float Nettonennleistung { get; set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
    }

    public class Biomass_DE_Dto
    {
        public string Energietraeger { get; private set; }
        public float Nettonennleistung { get; set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
    }

    public class Storage_DE_Dto
    {
        public string Energietraeger { get; private set; }
        public string Technologie { get; private set; }
        public float Nettonennleistung { get; set; }
        public int Inbetriebnahmejahr { get; set; }
        public int Stilllegungsjahr { get; set; }
    }
    }
