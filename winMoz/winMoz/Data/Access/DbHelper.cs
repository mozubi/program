﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Windows.Forms;
using winMoz.Simulation;

namespace winMoz.Data.Access
{
    public static class DbHelper
    {
        public static void ReCreateDb<TContext>() where TContext : DbContext
        => DbHelper.ReCreateDb((DbContext)Activator.CreateInstance(typeof(TContext)));

        public static void ReCreateDb(DbContext ctx)
        {
            using (ctx)
            {
                try
                {
                    ctx.Database.EnsureDeleted();
                    ctx.Database.EnsureCreated(); 
                }
                catch (System.IO.IOException e)
                {
                    Log.Error(e.Message + $" Please disconnect from your Database {ctx.GetType().FullName} ");
                    if (DialogResult.OK == MessageBox.Show($" Please disconnect from your Database {ctx.GetType().FullName} "))
                        ReCreateDb(ctx);
                }
            }
        }
    }
}
