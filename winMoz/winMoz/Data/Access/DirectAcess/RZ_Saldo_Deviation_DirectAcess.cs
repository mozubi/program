﻿using System.Collections.Generic;
using winMoz.Markets.ControlReserve;

namespace winMoz.Data.Access.DirectAcess
{
    public static class RZ_Saldo_Deviation_DirectAcess
    {
        public static Dictionary<DeviationFromRZSaldo_Handler.ParamaFRRDeviation, float[][]> GetParameters()
        {
            Dictionary<DeviationFromRZSaldo_Handler.ParamaFRRDeviation, float[][]> Parameters = new Dictionary<DeviationFromRZSaldo_Handler.ParamaFRRDeviation, float[][]>();
            Parameters.Add(DeviationFromRZSaldo_Handler.ParamaFRRDeviation.MIN, new float[3][] {
            new float[1] { -193.9625163169645F }, //mean
            new float[1] { 238.03318502628372F }, //divergence
            new float[1] { -3.626632600565366F } }); //skewness
            Parameters.Add(DeviationFromRZSaldo_Handler.ParamaFRRDeviation.MAX, new float[3][] {
            new float[1] { 199.96624867518835F } ,//mean
            new float[1] { 229.93114931216948F} , //divergence
            new float[1] { 3.0464724265702463F} }); //skewness
            Parameters.Add(DeviationFromRZSaldo_Handler.ParamaFRRDeviation.QUARTER_MIN, new float[3][] {
            new float[4] { -12.052436348559091F, 95.48505140600199F, -225.58978126858673F, 145.73250585900132F },
            new float[4] { 36.00981643387298F, -285.1894041976297F, 669.5292295836399F, -434.5289600855898F },
            new float[4] { -0.2790561913035739F, 2.3109381357073913F, -5.76045327126816F, 4.383486977570868F} });
            Parameters.Add(DeviationFromRZSaldo_Handler.ParamaFRRDeviation.QUARTER_MAX, new float[3][] {
            new float[4] { 12.53135606299852F, -100.26799376911995F, 242.52588958323014F, -165.91369770374712F },
            new float[4] { 15.942156803051937F, -133.416110805912F, 327.95649371535006F, -216.8015937276623F },
            new float[4] { 0.058549850111236014F, -0.6738761140256633F, 2.0557974352464474F, -1.6657526540485268F } });
            Parameters.Add(DeviationFromRZSaldo_Handler.ParamaFRRDeviation.DEMAND_MIN, new float[3][] {
            new float[6] { 0.009787858574526848F, -0.7316659055581562F, 21.58336219514786F, -312.68842827131897F, 2209.5750162128415F, -6036.169163765553F },
            new float[6] { 0.00308059549821509F, -0.21780148842368352F, 5.9470507651514755F, -79.50162913112914F, 535.6554391740985F, -1492.1500161099636F },
            new float[6] { -0.00013861123290806872F, 0.009134602352892143F, -0.23836973818193824F, 3.091678346543567F, -20.078509141218092F, 54.220413207239524F } });
            Parameters.Add(DeviationFromRZSaldo_Handler.ParamaFRRDeviation.DEMAND_MAX, new float[3][] {
            new float[6] { 0.0050088031245366215F, -0.2170582136223474F, 2.426155311630561F, 12.373481777970863F, -345.9668323203495F, 1416.4734433281628F },
            new float[6] { 0.0006306343294928637F, -0.0704081708924684F, 2.571871883644254F, -43.60667880907817F, 364.0655719217589F, -1208.1758693996965F },
            new float[6] { -6.206698925950183e-05F, 0.0031628445016541927F, -0.05647664801675911F, 0.3680867728514238F, 0.13093502567287138F, -8.122257859318859F } });
            return Parameters;
        }
            
    }
}
