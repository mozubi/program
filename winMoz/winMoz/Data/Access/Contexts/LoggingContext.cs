﻿using Microsoft.EntityFrameworkCore;
using winMoz.Simulation;
using winMoz.Simulation.Logging;

namespace winMoz.Data.Access.Contexts
{
    public class LoggingContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.Logging);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
