﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using winMoz.Simulation;
using winMoz.Assets;
using System.Collections.Concurrent;

namespace winMoz.Data.Access.Contexts
{
    public class WxContext : IDisposable
    {
        string connString;
        List<(DateTime TimeStamp, List<float> Values)> wxData = new List<(DateTime TimeStamp, List<float> Values)>();
        ConcurrentDictionary<string, int> NutsAndColumnInRow = new ConcurrentDictionary<string, int>();
        ConcurrentDictionary<string, List<WxDto>> wxDataShortTime = new ConcurrentDictionary<string, List<WxDto>>();
        ConcurrentDictionary<string, List<WxDto>> wxDataAuction = new ConcurrentDictionary<string, List<WxDto>>();
        (DateTime First, DateTime Last) wxDatesMinMax;
        (DateTime First, DateTime Last) wxDatesCached;
        ConcurrentDictionary<string, (DateTime First, DateTime Last)> wxDatesShortTime = new ConcurrentDictionary<string, (DateTime First, DateTime Last)>();
        ConcurrentDictionary<string, (DateTime First, DateTime Last)> wxDatesAuction = new ConcurrentDictionary<string, (DateTime First, DateTime Last)>();

        public WxContext() :this (Config.ConnStrings.Weather) { }
        public WxContext(string connString)
        {
            this.connString = connString;
            wxDatesMinMax = GetDbLimits();
            CacheNUTS2Indeces();
            
        }

        public WxContext(string connString, DateTime cacheTimeFrom, TimeSpan cacheDuration) : this(connString)
        {
            CacheWxData(cacheTimeFrom, cacheDuration);
        }

        public void CacheNUTS2Indeces()
        {
            string qryFirst = string.Format(@"SELECT * FROM {0} WHERE ROWID LIKE 1", "[data_weatherUTC]");

            var nuts = new List<string>();

            var dt = new DataTable();
            using (var adapter = new SQLiteDataAdapter(qryFirst, connString))
            {
                adapter.FillSchema(dt, SchemaType.Source);
            }
            DataColumnCollection dataColumnCollection = (DataColumnCollection)dt.Columns;
            foreach(var column in dataColumnCollection)
            {
                string columnString = column.ToString();
                var nut = columnString.Split('_')[0];
                if (!nuts.Contains(nut) && nut != "utc")
                    nuts.Add(nut);
            }
   
            var indexList = nuts.Select(NUTS2 => (NUTS2, dataColumnCollection.IndexOf(NUTS2 + "_windspeed_10m")));
            NutsAndColumnInRow = new ConcurrentDictionary<string, int>(indexList.ToDictionary(item => item.NUTS2, item => item.Item2));
            //nuts.Select(item => Rows.FirstOrDefault(row => row.)
        }

        public IEnumerable<WxDto> Get(DateTime timeStamp, TimeSpan duration, string NUTS2)
        {
            var EndOfDuration = timeStamp.Add(duration);
            if (timeStamp.Year != EndOfDuration.Year && !(EndOfDuration.Day == 1 && EndOfDuration.Month == 1 && EndOfDuration.Hour == 0 && EndOfDuration.Minute == 0))
            {
                List<WxDto> FirstElements = GetFromWeatherYear(timeStamp, new DateTime(timeStamp.Year, 12, 31, 23, 45, 0) - timeStamp, NUTS2).ToList();
                for(int year = timeStamp.Year + 1; year < EndOfDuration.Year; year++)
                {
                    FirstElements.AddRange(GetFromWeatherYear(new DateTime(year, 1, 1), new DateTime(year, 12, 31, 23, 45, 0) - new DateTime(year, 1, 1), NUTS2));
                }
                FirstElements.AddRange(GetFromWeatherYear(new DateTime(EndOfDuration.Year, 1, 1), EndOfDuration.AddMinutes(-15) - new DateTime(EndOfDuration.Year, 1, 1), NUTS2));
                var durationNew = FirstElements.Where(item => item.TimeStamp.Month == 2 && item.TimeStamp.Day == 29).ToList();
                return FirstElements;
            }
            else
                return GetFromWeatherYear(timeStamp, EndOfDuration.AddMinutes(-15) - timeStamp, NUTS2).ToList();
        }


        private IEnumerable<WxDto> GetFromWeatherYear(DateTime timeStamp, TimeSpan duration, string NUTS2, bool useWeatherYear = true)
        {
            var Shift = timeStamp.Year % 4 == 0 && Simulator.WeatherYear.Year % 4 != 0 ? new DateTime(Simulator.WeatherYear.Year, timeStamp.Month, timeStamp.Day == 29 && timeStamp.Month == 2 ? 28 : timeStamp.Day, timeStamp.Hour, timeStamp.Minute, 0) - timeStamp : new DateTime(Simulator.WeatherYear.Year, timeStamp.Month, timeStamp.Day, timeStamp.Hour, timeStamp.Minute, 0) - timeStamp;
            // Round to nearest 15min, set seconds to 0
            var timeReset = GetTimeReset(timeStamp, duration, useWeatherYear, Shift);

            var durationReset = GetDurationReset(timeStamp,timeReset, duration, useWeatherYear, Shift);

            var EndOfDurationReset = timeReset.Add(durationReset);

            if (durationReset.Days <= 31 && wxDataShortTime.ContainsKey(NUTS2) && IsTimeFrameShortTime(NUTS2, timeReset, timeReset + durationReset))
                return ShiftResultToSimulationYear(wxDataShortTime[NUTS2].Last().TimeStamp > timeReset + durationReset ? wxDataShortTime[NUTS2].Where(item => item.TimeStamp >= timeReset && item.TimeStamp <= timeReset.Add(durationReset)).ToList() : wxDataShortTime[NUTS2], timeReset, durationReset, timeStamp, duration, useWeatherYear, Shift);

            if (wxDataAuction.ContainsKey(NUTS2) && IsTimeFrameAuction(NUTS2, timeReset, timeReset + durationReset + TimeSpan.FromMinutes(-15)))
                return ShiftResultToSimulationYear(wxDataAuction[NUTS2].Last().TimeStamp > timeReset + durationReset ? wxDataAuction[NUTS2].Where(item => item.TimeStamp >= timeReset && item.TimeStamp <= timeReset.Add(durationReset)).ToList() : wxDataAuction[NUTS2], timeReset, durationReset, timeStamp, duration, useWeatherYear, Shift);


            if (IsTimeFrameCached(timeReset, timeReset + durationReset))
            {
                //var result = WxDto.GetFromView(NUTS2, wxData.AsEnumerable().Where(row => (DateTime)row[0] >= timeReset && (DateTime)row[0] < timeReset + duration));
                var result = WxDto.GetFromView(nutsStartIndex: NutsAndColumnInRow[NUTS2], wxData.Where(row => row.TimeStamp  >= timeReset && row.TimeStamp <= timeReset + durationReset).ToList());
                if (durationReset.Days <= 31)
                    CacheWxDataShortTime(NUTS2, result.ToList());
                else
                    CacheWxDataAuction(NUTS2, result.ToList());
                return ShiftResultToSimulationYear(result, timeReset, durationReset, timeStamp, duration, useWeatherYear, Shift);
            }

            CacheWxData(new DateTime(Simulator.WeatherYear.Year, 1, 1), new DateTime(Simulator.WeatherYear.Year, 12, 31, 23, 45, 0) - new DateTime(Simulator.WeatherYear.Year, 1, 1));

            return GetFromWeatherYear(timeStamp, duration, NUTS2, useWeatherYear);
        }

        private IEnumerable<WxDto> ShiftResultToSimulationYear(IEnumerable<WxDto> wxDtos, DateTime timeReset, TimeSpan durationReset, DateTime timeStamp, TimeSpan duration, bool useWeatherYear, TimeSpan shift)
        {
            if (!useWeatherYear || shift.TotalMinutes == 0)
                return wxDtos;
            else
            {
                List<WxDto> wxDtosNew = new List<WxDto>();
                wxDtos.ToList().ForEach(item => wxDtosNew.Add(new WxDto(item)));
                var Last = wxDtosNew.Last();
                var durationToShift = TimeSpan.FromMinutes(-shift.TotalMinutes);
                if (Is29FebruaryInDuration(timeReset, durationReset) && !Is29FebruaryInDuration(timeStamp, duration))
                {
                    wxDtosNew = wxDtosNew.Where(item => !(item.TimeStamp.Month == 2 && item.TimeStamp.Day == 29)).ToList();
                    var elementsToShift = wxDtosNew.SkipWhile(item => !(item.TimeStamp.Month == 3 && item.TimeStamp.Day != 1)).ToList();
                    elementsToShift.ForEach(item => item.TimeStamp = item.TimeStamp.Add(TimeSpan.FromDays(-1)));
                }
                wxDtosNew.ForEach(item => item.TimeStamp = item.TimeStamp.Add(durationToShift));
                if (!Is29FebruaryInDuration(timeReset, durationReset) && Is29FebruaryInDuration(timeStamp, duration))
                {
                    var elementsToCopy = new List<WxDto>(wxDtosNew.SkipWhile(item => !(item.TimeStamp.Month == 2 && item.TimeStamp.Day == 28)).TakeWhile(item => item.TimeStamp.Month == 2 && item.TimeStamp.Day == 28).ToList());
                    var newElements = new List<WxDto>();
                    elementsToCopy.ForEach(item => newElements.Add(new WxDto(item)));
                    var elementsToShift = wxDtosNew.SkipWhile(item => !(item.TimeStamp.Month == 2 && item.TimeStamp.Day >= 29)).ToList();
                    elementsToShift.ForEach(item => item.TimeStamp = item.TimeStamp.Add(TimeSpan.FromDays(1)));
                    newElements.ForEach(item => item.TimeStamp = item.TimeStamp.Add(TimeSpan.FromDays(1)));
                    wxDtosNew.AddRange(newElements);
                    wxDtosNew = wxDtosNew.OrderBy(item => item.TimeStamp).ToList();
                }
                var result =  wxDtosNew.Where(item => item.TimeStamp >= timeStamp && item.TimeStamp <= timeStamp.Add(duration)).ToList();
                return result;
            }
        }

        private bool Is29FebruaryInDuration(DateTime timeStamp, TimeSpan duration)
            => (timeStamp.Year % 4 == 0 && timeStamp.Month <= 2) && (timeStamp.Add(duration).Year % 4 == 0 && timeStamp.Add(duration).Month > 2);

        private DateTime GetTimeReset(DateTime timeStamp, TimeSpan duration, bool useWeatherYear, TimeSpan shift)
        {
            var timeReset = new DateTime(timeStamp.Year, timeStamp.Month, timeStamp.Day, timeStamp.Hour, (int)(Math.Round(timeStamp.Minute / 15.0, 0) * 15), 0);
            if (!useWeatherYear)
                return timeReset;
            else
                return timeReset.Add(shift);
        }

        private TimeSpan GetDurationReset(DateTime timeStamp, DateTime timeReset, TimeSpan duration, bool useWeatherYear, TimeSpan shift)
        {
            if (Is29FebruaryInDuration(timeReset, duration) && !Is29FebruaryInDuration(timeStamp, duration))
                return duration.Add(TimeSpan.FromDays(1)); 
            else if (shift.TotalMinutes != 0 && !Is29FebruaryInDuration(timeReset, duration) && Is29FebruaryInDuration(timeStamp, duration) && duration.TotalDays > 1)
                return duration.Add(TimeSpan.FromDays(-1));
            else
                return duration;
        }

        private bool IsTimeFrameCached(DateTime first, DateTime second)
        {
            if (first >= wxDatesCached.First && first < wxDatesCached.Last)
                if (second <= wxDatesCached.Last && second > wxDatesCached.First)
                    return true;
            return false;
        }

        private bool IsTimeFrameAuction(string NUTS2, DateTime first, DateTime second)
        {
            var First = wxDatesAuction.ContainsKey(NUTS2) ? wxDatesAuction[NUTS2].First : default;
            var Last = wxDatesAuction.ContainsKey(NUTS2) ? wxDatesAuction[NUTS2].Last : default;
            if (First != default && Last != default)
            {
                if (first >= First && first < Last)
                    if (second <= Last && second > First)
                        return true;
                return false;
            }
            else
                return IsTimeFrameAuction(NUTS2, first, second);
        }

        private bool IsTimeFrameShortTime(string NUTS2, DateTime first, DateTime second)
        {
            if (wxDatesShortTime.ContainsKey(NUTS2))
            {
            if (first >= wxDatesShortTime[NUTS2].First && first < wxDatesShortTime[NUTS2].Last)
                if (second <= wxDatesShortTime[NUTS2].Last && second > wxDatesShortTime[NUTS2].First)
                    return true;
            }
            return false;
        }

        private int[] WxRowsInDb(DateTime first, DateTime second)
        {
            int firstRow, secondRow;
            if (first > wxDatesMinMax.Last || second < wxDatesMinMax.First)
                throw new InvalidOperationException("No Wx data available for selected timeframe.");

            if (first < wxDatesMinMax.First)
                firstRow = 0;
            else
                firstRow = (int)Math.Round((first - wxDatesMinMax.First).TotalMinutes / 15.0, 0);

            secondRow = (int)Math.Round((second - first).TotalMinutes / 15.0, 0) + firstRow;

            return new int[2] { firstRow, secondRow };
        }

        private DataTable GetWxTable(DateTime timeStamp, TimeSpan duration)
        {
            int[] rows = WxRowsInDb(timeStamp, timeStamp + duration);

            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("utc_timestamp", typeof(DateTime)));

            string qry = string.Format(@"SELECT * FROM [data_weatherUTC] WHERE ROWID BETWEEN {0} AND {1}", rows[0], rows[1]);

            using (var adapter = new SQLiteDataAdapter(qry, connString))
                adapter.Fill(dt);

            return InterpolateDST(dt);
        }

        private DataTable InterpolateDST(DataTable dt)
        {
            var affected = IdDSTAffectedRows(dt);

            foreach (var idx in affected.Where(el => !el.st).Select(el => el.idx))
                dt.Rows.RemoveAt(idx);

            foreach (var idx in affected.Where(el => el.st).Select(el => el.idx))
            {
                var rows2add = new List<DataRow>();
                var rowA = dt.Rows[idx];
                var rowB = dt.Rows[idx + 1];
                for (int qh = 1; qh <= 4; qh++)
                {
                    var newRow = dt.NewRow();
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (col.DataType == typeof(double))
                        {
                            var diff = ((double)rowB[col] - (double)rowA[col]) / 4;
                            newRow[col] = (double)rowA[col] + qh * diff;
                        }

                        if (col.DataType == typeof(DateTime))
                            newRow[col] = ((DateTime)rowA[col]).AddMinutes(15 * qh);
                    }
                    dt.Rows.InsertAt(newRow, idx + qh);
                }
            }
            return dt;
        }

        private List<(int idx, bool st)> IdDSTAffectedRows(DataTable dt)
        {
            var affected = new List<(int idx, bool st)>();
            var tbl = dt.AsEnumerable();
            int rowCount = tbl.Count();
            int idx = 1;

            while (idx > 0 && idx < rowCount)
            {
                idx = tbl.IndexOf(tbl.Skip(idx).FirstOrDefault(row => ((DateTime)row[0]).IsDaylightSavingTime()));
                if (idx > 0)
                    affected.Add((idx - 1, true));

                idx = tbl.IndexOf(tbl.Skip(idx).FirstOrDefault(row => !((DateTime)row[0]).IsDaylightSavingTime()));
                if (idx > 0)
                    for (int qh = 0; qh < 4; qh++)
                        affected.Add((idx + qh, false));
            }
            return affected;
        }

        public void CacheWxData(DateTime timeStamp, TimeSpan duration)
        {
            var EndOfDuration = timeStamp.Add(duration).AddMinutes(-15);
            this.wxData = GetWxTable(timeStamp, duration).AsEnumerable().Select(row => 
                ((DateTime)row.ItemArray[0], 
                row.ItemArray
                    .Skip(1)
                    .Select(item =>  Convert.ToSingle(item))
                    .ToList()))
                .ToList();
            if (this.wxData.First().TimeStamp.Ticks != timeStamp.Ticks)
            {
                var newTimeStamp = timeStamp.AddMinutes((timeStamp - this.wxData.First().TimeStamp).TotalMinutes);
                //var newEndOfDuration = EndOfDuration.AddMinutes((EndOfDuration - this.wxData.Last().TimeStamp).TotalMinutes);
                var newDuration = duration.Add(TimeSpan.FromMinutes(duration.TotalMinutes - (this.wxData.Last().TimeStamp - this.wxData.First().TimeStamp).TotalMinutes));
                this.wxData = GetWxTable(newTimeStamp, newDuration).AsEnumerable().Select(row =>
                ((DateTime)row.ItemArray[0],
                row.ItemArray
                    .Skip(1)
                    .Select(item => Convert.ToSingle(item))
                    .ToList()))
                .ToList();
                var Last = this.wxData.Last();
            }
            SetCachedDates();
            if (this.wxDatesCached.First > timeStamp)
            {
                this.wxData = GetWxTable(timeStamp.Add(timeStamp - this.wxDatesCached.First), duration).AsEnumerable().Select(row =>
                ((DateTime)row.ItemArray[0],
                row.ItemArray
                    .Skip(1)
                    .Select(item => Convert.ToSingle(item))
                    .ToList()))
                .ToList();
                if (this.wxData.First().TimeStamp.Ticks != timeStamp.Ticks)
                {
                    var newTimeStamp = timeStamp.AddMinutes((timeStamp - this.wxData.First().TimeStamp).TotalMinutes);
                    var newDuration = duration.Add(timeStamp - this.wxData.First().TimeStamp);
                    this.wxData = GetWxTable(timeStamp, duration).AsEnumerable().Select(row =>
                    ((DateTime)row.ItemArray[0],
                    row.ItemArray
                        .Skip(1)
                        .Select(item => Convert.ToSingle(item))
                        .ToList()))
                    .ToList();
                }
                SetCachedDates();
            }
        }
        public void CacheWxDataShortTime(string NUTS2, List<WxDto> dataToCache)
        {
            if (this.wxDataShortTime.ContainsKey(NUTS2))
                this.wxDataShortTime[NUTS2] = dataToCache;
            else
                this.wxDataShortTime.TryAdd(NUTS2, dataToCache);
            SetCachedDatesShortTime(NUTS2);
        }

        public void CacheWxDataAuction(string NUTS2, List<WxDto> dataToCache)
        {
            if (this.wxDataAuction.ContainsKey(NUTS2))
                this.wxDataAuction[NUTS2] = dataToCache;
            else
                this.wxDataAuction.TryAdd(NUTS2, dataToCache);
            SetCachedDatesAuction(NUTS2);
        }

        private (DateTime, DateTime) GetDbLimits()
        {
            string qryFirst = string.Format(@"SELECT utc_timestamp FROM {0} WHERE ROWID LIKE 1", "[data_weatherUTC]");
            string qryLast = string.Format(@"SELECT utc_timestamp FROM {0} WHERE ROWID LIKE (SELECT COUNT(*) FROM {0})", "[data_weatherUTC]");

            var times = new DateTime[2];
            using (var cmd = new SQLiteCommand(qryFirst, new SQLiteConnection(connString)))
            {
                cmd.Connection.Open();
                DateTime.TryParse((string)cmd.ExecuteScalar(), out times[0]);
            }

            using (var cmd = new SQLiteCommand(qryLast, new SQLiteConnection(connString)))
            {
                cmd.Connection.Open();
                DateTime.TryParse((string)cmd.ExecuteScalar(), out times[1]);
            }

            return (times[0], times[1]);
        }

        private void SetCachedDates()
        {
            this.wxDatesCached = ((DateTime)wxData.First().TimeStamp, (DateTime)wxData.Last().TimeStamp);
        }

        private void SetCachedDatesShortTime(string NUTS2)
        {
            if (this.wxDatesShortTime.ContainsKey(NUTS2))
                this.wxDatesShortTime[NUTS2] = wxDataShortTime.ContainsKey(NUTS2) ? ((DateTime)wxDataShortTime[NUTS2].First().TimeStamp, (DateTime)wxDataShortTime[NUTS2].Last().TimeStamp) : (new DateTime(1990, 1, 1), new DateTime(1990, 1, 1));
            else
                this.wxDatesShortTime.TryAdd(NUTS2, wxDataShortTime.ContainsKey(NUTS2) ? ((DateTime)wxDataShortTime[NUTS2].First().TimeStamp, (DateTime)wxDataShortTime[NUTS2].Last().TimeStamp) : (new DateTime(1990, 1, 1), new DateTime(1990, 1, 1)));
        }

        private void SetCachedDatesAuction(string NUTS2)
        {
            if (this.wxDatesAuction.ContainsKey(NUTS2))
                this.wxDatesAuction[NUTS2] = wxDataAuction.ContainsKey(NUTS2) ? ((DateTime)wxDataAuction[NUTS2].First().TimeStamp, (DateTime)wxDataAuction[NUTS2].Last().TimeStamp) : (new DateTime(1990, 1, 1), new DateTime(1990, 1, 1));
            else
                this.wxDatesAuction.TryAdd(NUTS2, wxDataAuction.ContainsKey(NUTS2) ? ((DateTime)wxDataAuction[NUTS2].First().TimeStamp, (DateTime)wxDataAuction[NUTS2].Last().TimeStamp) : (new DateTime(1990, 1, 1), new DateTime(1990, 1, 1)));
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                    this.wxData.Clear();

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

}
