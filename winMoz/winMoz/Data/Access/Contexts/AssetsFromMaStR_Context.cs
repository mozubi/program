﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;

namespace winMoz.Data.Access.Contexts
{
    public class AssetsFromMaStR_Context : DbContext
    {
        public virtual DbSet<Thermal_KWK_Nuts2_Dto> KraftwerkeKWKFossilAggregiertNuts2 { get; set; }
        public virtual DbSet<Thermal_KWK_All_Dto> KraftwerkeKWKFossilGesamt { get; set; }
        public virtual DbSet<Thermal_Kond_DE_Dto> KraftwerkeKondFossilAggregiertDeutschlandweit { get; set; }
        public virtual DbSet<Thermal_Kond_All_Dto> KraftwerkeKondFossilGesamt { get; set; }
        public virtual DbSet<WindOnshore_Nuts2_Dto> KraftwerkeWindOnshoreAggregiertNuts2 { get; set; }
        public virtual DbSet<Hydro_RunOfRiver_DE_Dto> KraftwerkeLaufwasserkraftAggregiertDeutschlandweit { get; set; }
        public virtual DbSet<PV_DE_Dto> KraftwerkeSolarAggregiertDeutschlandweit { get; set; }
        public virtual DbSet<Biomass_DE_Dto> KraftwerkeBiomasseAggregiertDeutschlandweit { get; set; }
        public virtual DbSet<Storage_DE_Dto> KraftwerkeSpeicherAggregiertDeutschlandweit { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.AssetsFromMaStR);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Thermal_KWK_Nuts2_Dto>()
                .HasNoKey();

            modelBuilder.Entity<Thermal_KWK_All_Dto>()
                .HasNoKey();

            modelBuilder.Entity<Thermal_Kond_DE_Dto>()
                .HasNoKey();

            modelBuilder.Entity<Thermal_Kond_All_Dto>()
                .HasNoKey();

            modelBuilder.Entity<WindOnshore_Nuts2_Dto>()
                .HasNoKey();

            modelBuilder.Entity<Hydro_RunOfRiver_DE_Dto>()
                .HasNoKey();

            modelBuilder.Entity<PV_DE_Dto>()
                .HasNoKey();

            modelBuilder.Entity<Biomass_DE_Dto>()
                .HasNoKey();

            modelBuilder.Entity<Storage_DE_Dto>()
                .HasNoKey();
                
            base.OnModelCreating(modelBuilder);
        }
    }
}
