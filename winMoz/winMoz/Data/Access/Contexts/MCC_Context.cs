﻿using System;
using Microsoft.EntityFrameworkCore;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.DTO.Keys;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Markets.Elements;
using winMoz.Helper;

namespace winMoz.Data.Access.Contexts
{
    //TODO: Szenario_Repository-Klasse bzw. Repository-Klassen erzeugen für bessere Übertragbarkeit von Context zu aufgerufenen Funktionen
    public class MCC_Context : DbContext
    {
        public virtual DbSet<VBH_VRE_Dto> VBH_VRE { get; set; }
        public virtual DbSet<Capacities_Dto> Capacities { get; set; }
        public virtual DbSet<ExportCapacity_Dto> Export_Capacity_static { get; set; }
        //public virtual DbSet<CFunction_Dto> C_Functions { get; set; }
        public virtual DbSet<CFunction_Dto> C_Functions_Complete { get; set; }
        public virtual DbSet<GenPara_Thermal_Dto> Generation_Parameter_Thermal { get; set; }
        public virtual DbSet<GenPara_Constant_Dto> Generation_Parameter_Constant { get; set; }
        public virtual DbSet<Load_MCC_Dto> Load_Values { get; set; }
        public virtual DbSet<LoadScale_Dto> LoadScaleParameter { get; set; }
        public virtual DbSet<SzenarioCountries_Dto> ScenarioCountries { get; set; }
        public virtual DbSet<TotalSzenario_Dto> TotalScenario { get; set; }

        public override int SaveChanges()
        {
            throw new InvalidOperationException("This context is read-only.");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.MCCData);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VBH_VRE_Dto>()
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<VBH_VRE_Dto>()
                .Property(prop => prop.TYNDP_Scenario)
                .HasConversion(el => el.ToString(), cd => (EnumCollection_EOMMarkets.TYNDP_Scenario_Type)Enum.Parse(typeof(EnumCollection_EOMMarkets.TYNDP_Scenario_Type), cd.ToString()));
            modelBuilder.Entity<VBH_VRE_Dto>()
                .Property(prop => prop.VRE_Type)
                .HasConversion(el => el.ToString(), cd => (EnumCollection_EOMMarkets.VREType)Enum.Parse(typeof(EnumCollection_EOMMarkets.VREType), cd.ToString()));
            modelBuilder.Entity<VBH_VRE_Dto>()
                .HasKey(prop => new VBH_VRE_Key(prop.ShortCode, prop.TYNDP_Forecast_Year, prop.TYNDP_Year, prop.TYNDP_Scenario, prop.TYNDP_Climate_Year, prop.VRE_Type));

            modelBuilder.Entity<Capacities_Dto>()
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<Capacities_Dto>()
                .HasKey(prop => new Capacities_Key(prop.CapaID, prop.ShortCode));
            modelBuilder.Entity<Capacities_Dto>()
                .Ignore(prop => prop.ProductionType);

            modelBuilder.Entity<ExportCapacity_Dto>()
                .Property(prop => prop.ShortCode_From)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<ExportCapacity_Dto>()
                .Property(prop => prop.ShortCode_To)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<ExportCapacity_Dto>()
                .HasKey(prop => new ExportCapacity_Key(prop.ImExFixID, prop.ShortCode_From, prop.ShortCode_To));

            modelBuilder.Entity<CFunction_Dto>()
                .Property(prop => prop.ShortCode_From)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<CFunction_Dto>()
                .Property(prop => prop.ShortCode_To)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<CFunction_Dto>()
                .HasKey(prop => new CFunction_Key(prop.C_FuncID, prop.ShortCode_From, prop.ShortCode_To, prop.Wind_Prog_From));

            //modelBuilder.Entity<CFunction_Dto2>()
            //    .Property(prop => prop.ShortCode_From)
            //    .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            //modelBuilder.Entity<CFunction_Dto2>()
            //    .Property(prop => prop.ShortCode_To)
            //    .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            //modelBuilder.Entity<CFunction_Dto2>()
            //    .HasKey(prop => new CFunction_Key2(prop.C_FuncID, prop.ShortCode_From, prop.ShortCode_To, prop.Wind_Prog_From));


            modelBuilder.Entity<Load_MCC_Dto>()
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<Load_MCC_Dto>()
                .HasKey(prop => new Load_Key(prop.LoadValID, prop.ShortCode, prop.Year));

            modelBuilder.Entity<LoadScale_Dto>()
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<LoadScale_Dto>()
                .HasKey(prop => new LoadScale_Key(prop.LoadScaleID, prop.ShortCode));

            modelBuilder.Entity<SzenarioCountries_Dto>()
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
            modelBuilder.Entity<SzenarioCountries_Dto>()
                .HasKey(prop => new ScenarioCountry_Key(prop.ScenID, prop.ShortCode));

            modelBuilder.Entity<TotalSzenario_Dto>()
                .Property(prop => prop.TYNDP_Scenario)
                .HasConversion(el => el.ToString(), cd => (EnumCollection_EOMMarkets.TYNDP_Scenario_Type)Enum.Parse(typeof(EnumCollection_EOMMarkets.TYNDP_Scenario_Type), cd.ToString()));
            modelBuilder.Entity<TotalSzenario_Dto>()
                .HasKey(prop => prop.TotalID);

            modelBuilder.Entity<GenPara_Thermal_Dto>()
                .Property(prop => prop.GenerationType)
                .HasConversion(el => el.ToString(), cd => (EnumCollection_EOMMarkets.ThermalEnum)Enum.Parse(typeof(EnumCollection_EOMMarkets.ThermalEnum), cd.ToString()));
            modelBuilder.Entity<GenPara_Thermal_Dto>()
                .HasKey(prop => new GenPara_Thermal_Key(prop.GenParaID, prop.GenerationType));

            modelBuilder.Entity<GenPara_Constant_Dto>()
                .Property(prop => prop.GenerationType)
                .HasConversion(el => el.ToString(), cd => (EnumCollection_EOMMarkets.ConstantEnum)Enum.Parse(typeof(EnumCollection_EOMMarkets.ConstantEnum), cd.ToString()));
            modelBuilder.Entity<GenPara_Constant_Dto>()
                .HasKey(prop => new GenPara_Constant_Key(prop.GenParaID, prop.GenerationType));

            base.OnModelCreating(modelBuilder);
        }
    }
}
