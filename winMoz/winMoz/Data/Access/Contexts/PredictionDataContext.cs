﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;
using System.Collections.Generic;

namespace winMoz.Data.Access.Contexts
{
    // TODO: Make context un-editable by hiding behind layer
    public class PredictionDataContext : DbContext
    {
        public virtual DbSet<DayAheadData> DayAhead { get; set; }
        public virtual DbSet<IntradayData> Intraday { get; set; }
        public virtual DbSet<NRVData> NRV { get; set; }
        public virtual DbSet<REData> RE { get; set; }
        public virtual DbSet<RebapData> Rebap { get; set; }
        public virtual DbSet<MRLData> MRL { get; set; }
        public virtual DbSet<SRLData> SRL { get; set; }
        public virtual DbSet<APMaxData> APMax { get; set; }
        public virtual DbSet<LoadData> GridLoad { get; set; }
        public virtual DbSet<WxNinja> WxNinja { get; set; }
        public virtual DbSet<GenerationUnits> GenerationUnits { get; set; }
        public virtual DbSet<Generation> Generation { get; set; }
        public virtual DbSet<WestNetz> WestNetz { get; set; }

        public override int SaveChanges()
        {
            throw new InvalidOperationException("This context is read-only.");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.PredictionData);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GenerationUnits>().ToTable("Generation_Units");
            modelBuilder.Entity<GenerationUnits>().HasNoKey();
            modelBuilder.Entity<WxNinja>().ToTable("Weather");
            modelBuilder.Entity<WxNinja>().HasNoKey();
            modelBuilder.Entity<LoadData>().HasNoKey().Ignore(prop => prop.Id);
            base.OnModelCreating(modelBuilder);
        }
    }


}
