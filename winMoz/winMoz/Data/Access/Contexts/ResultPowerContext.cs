﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Markets.Bids;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;

namespace winMoz.Data.Access.Contexts
{
    public class ResultPowerContext : DbContext
    {
        public virtual DbSet<ResultPowerDto> ResultPower { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.ResultPower);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ResultPowerDto>().HasKey(prop => prop.TimeStamp);
            base.OnModelCreating(modelBuilder);
        }
    }
}
