﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;
using winMoz.Data.Access;

namespace winMoz.Data.Access.Contexts
{
    public class ControlReserveDemandContext : DbContext
    {
        public virtual DbSet<TenderVolume_Dto> TenderVolume { get; set; }
        public virtual DbSet<ActivationVolume_Dto> ActivationVolume { get; set; }
        public virtual DbSet<Limits_Dto> Limits { get; set; }
        public virtual DbSet<Balances_Dto> Balances { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.ControlReserveDemand);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TenderVolume_Dto>()
                .HasKey(prop => new ControlReserveDemand_Keys.Demand_Key(prop.TimeStamp, prop.FRR_Type));

            modelBuilder.Entity<ActivationVolume_Dto>()
                .HasKey(prop => new ControlReserveDemand_Keys.ActivationVolume_Key(prop.TimeStamp, prop.FRR_Type));

            modelBuilder.Entity<Limits_Dto>()
                .HasKey(prop => new ControlReserveDemand_Keys.Limits_Key(prop.TimeStamp, prop.FRR_Type));

            modelBuilder.Entity<Balances_Dto>()
                .HasKey(prop => new ControlReserveDemand_Keys.Balances_Key(prop.TimeStamp, prop.FRR_Type));

            base.OnModelCreating(modelBuilder);
        }
    }
}
