﻿using Microsoft.EntityFrameworkCore;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;

namespace winMoz.Data.Access.Contexts
{
    public class PVDtoContext : DbContext
    {
        public virtual DbSet<PVDto> PVPlantsAggregate { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.PVPlants);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
