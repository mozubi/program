﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using winMoz;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.DTO.Keys;

namespace winMoz.Data.Access.Contexts
{
    public class CommoditiesContext : DbContext
    {
        public List<CommodityPrices_Dto> CachedPrices { get; private set; } = new List<CommodityPrices_Dto>();

        public void CachePrices(DateTime timeStamp, TimeSpan duration)
        {
            var EndOfDuration = timeStamp.Add(duration);
            if (CachedPrices.Count() == 0 || CachedPrices.Last().DayINT < GetDayINT(EndOfDuration))
                CachedPrices = this.CommodityPrices.Where(item => item.DayINT >= GetDayINT(timeStamp) && item.DayINT <= GetDayINT(EndOfDuration)).ToList();
        }

        public static Int64 GetDayINT(DateTime timeStamp)
            => timeStamp.Year * 10000 + timeStamp.Month * 100 + timeStamp.Day;

        public virtual DbSet<CommodityPrices_Dto> CommodityPrices { get; set; }

        public override int SaveChanges()
        {
            throw new InvalidOperationException("This context is read-only.");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.CommodititiesData);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<CommodityPrices_Dto>()
                .HasKey(prop => new CommodityPrices_Key(prop.CommodityID, prop.DayINT));


            base.OnModelCreating(modelBuilder);
        }
    }
}
