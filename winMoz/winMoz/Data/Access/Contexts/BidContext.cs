﻿using Microsoft.EntityFrameworkCore;
using System;
using winMoz.Markets.Bids;
using winMoz.Simulation;
using winMoz.Markets;
using winMoz.Data.Access.Repositories;

namespace winMoz.Data.Access.Contexts
{
    public class BidContext : DbContext
    {
        public virtual DbSet<Bid> Bids { get; set; }

        public virtual DbSet<Bid_Segment> Bid_Segments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.Bids);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bid_Intraday>().HasBaseType(typeof(Bid));//.HasKey(item => item.BidId); //.HasKey(bid => bid.BidId);
            modelBuilder.Entity<Bid_DayAhead>().HasBaseType(typeof(Bid));
            modelBuilder.Entity<Bid_Future>().HasBaseType(typeof(Bid));
            modelBuilder.Entity<Bid_ControlReserve_Capacity>().HasBaseType(typeof(Bid));
            modelBuilder.Entity<Bid_ControlReserve_Energy>().HasBaseType(typeof(Bid));
            modelBuilder.Entity<Bid_ControlReserve_Activation>().HasBaseType(typeof(Bid));
            modelBuilder.Entity<Bid>().Ignore(bid => bid.Agent).Ignore(bid => bid.Assets).Ignore(bid => bid.Segments);
            modelBuilder.Entity<Bid>().HasKey(bid => bid.BidId);
            modelBuilder.Entity<Bid_Segment>().HasKey(seg => seg.SegmentId);
            modelBuilder.Entity<Bid>().Property(prop => prop.Event)
                .HasConversion(a => a.GetType().FullName + ";" + a.Id.ToString(), b => new MarketRepository(new SimContext()).GetEvent(Type.GetType(b.Split(';')[0]), (int)Convert.ToInt64(b.Split(';')[1])));

            //modelBuilder.Entity<Bid>().Property(item => item.Event.Id);
            modelBuilder.Entity<Bid_DayAhead>().Ignore(bid => bid.Derivative);
            modelBuilder.Entity<Bid_DayAhead>().Ignore(bid => bid.IsUnderlying);
            modelBuilder.Entity<Bid_Future>().Ignore(bid => bid.Underlyings);
            //modelBuilder.Entity<Bid_Future>().HasMany(bid => bid.Underlyings).WithOne(Bid => Bid.Derivative);

            // TODO: Implement a try to get asset from sim in repo
            modelBuilder.Entity<Bid_Segment>().Property(prop => prop.ShortCode).HasConversion(a => a.ToString(), b => ShortCode.FromString(b));
            modelBuilder.Entity<Bid_Segment_ControlReserve>().HasBaseType(typeof(Bid_Segment));
            modelBuilder.Entity<Bid_Segment_ControlReserve>().Ignore(item => item.TransferEvent);
            modelBuilder.Entity<Bid_Segment_ControlReserve>().Property(prop => prop.TransferEvent)
                .HasConversion(a => a.GetType().FullName + ";" + a.Id.ToString(), b => new MarketRepository(new SimContext()).GetEvent(Type.GetType(b.Split(';')[0]), (int)Convert.ToInt64(b.Split(';')[1])));
            modelBuilder.Entity<Bid_Segment_ControlReserve>().Property(prop => prop.FirstEvent)
                .HasConversion(a => a.GetType().FullName + ";" + a.Id.ToString(), b => new MarketRepository(new SimContext()).GetEvent(Type.GetType(b.Split(';')[0]), (int)Convert.ToInt64(b.Split(';')[1])));

            base.OnModelCreating(modelBuilder);
        }
    }
}
