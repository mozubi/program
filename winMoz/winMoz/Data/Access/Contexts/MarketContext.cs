﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using winMoz.Assets;
using winMoz.Markets.Bids;
using winMoz.Markets.Results;
using winMoz.Simulation;
using System.Data;

namespace winMoz.Data.Access.Contexts
{
    public class MarketContext : DbContext
    {
        /*DataStorage<FuturesResult> FuturesStorage;
        DataStorage<DayAheadResult> DayAheadStorage;
        DataStorage<IntraDayResult> IntradayStorage;
        DataStorage<ControlReserveResult> ControlReserveStorage;

        public Dictionary<string, bool> IsEmptyList = new Dictionary<string, bool>() { { "FuturesResult", true }, { "DayAheadResult", true }, { "IntraDayResult", true }, { "ControlReserveResult", true } };

        public MarketContext()
        {
            FuturesStorage = new DataStorage<FuturesResult>(shouldElementsBeCached: Simulator.SaveStates.AreMarketResultsSaved);
            DayAheadStorage = new DataStorage<DayAheadResult>(shouldElementsBeCached: Simulator.SaveStates.AreMarketResultsSaved);
            IntradayStorage = new DataStorage<IntraDayResult>(shouldElementsBeCached: Simulator.SaveStates.AreMarketResultsSaved);
            ControlReserveStorage = new DataStorage<ControlReserveResult>(shouldElementsBeCached: Simulator.SaveStates.AreMarketResultsSaved);
        }
        public MarketContext(DateTime startPreCache, TimeSpan durationPreCache)
        {
            FuturesStorage = new DataStorage<FuturesResult>(context: new MarketContext(), startPreCache: startPreCache, durationPreCache : durationPreCache);
            DayAheadStorage = new DataStorage<DayAheadResult>(context: new MarketContext(), startPreCache: startPreCache, durationPreCache: durationPreCache);
            IntradayStorage = new DataStorage<IntraDayResult>(context: new MarketContext(), startPreCache: startPreCache, durationPreCache: durationPreCache);
            ControlReserveStorage = new DataStorage<ControlReserveResult>(context: new MarketContext(), startPreCache: startPreCache, durationPreCache: durationPreCache);
        }

        public bool IsEmpty<TStorage>() where TStorage : StorageType
        {
            if (typeof(TStorage).Name == "FuturesResult") return FuturesStorage.IsEmpty();
            else if (typeof(TStorage).Name == "DayAheadResult") return DayAheadStorage.IsEmpty();
            else if (typeof(TStorage).Name == "IntraDayResult") return IntradayStorage.IsEmpty();
            else if (typeof(TStorage).Name == "ControlReserveResult") return ControlReserveStorage.IsEmpty();
            else return true;
        }

        public void Add<TStorage>(IEnumerable<TStorage> results, string type = "") where TStorage : StorageType
        {
            if (results.Count() > 0)
            {
                if (typeof(TStorage).Name == "FuturesResult" || type == "FuturesResult") { FuturesStorage.Add(new MarketContext(), results.Cast<FuturesResult>().ToList()); if (typeof(TStorage).Name == "FuturesResult") IsEmptyList[typeof(TStorage).Name] = false; else IsEmptyList[type] = false; }
                else if (typeof(TStorage).Name == "DayAheadResult" || type == "DayAheadResult") {DayAheadStorage.Add(new MarketContext(), results.Cast<DayAheadResult>().ToList()); if (typeof(TStorage).Name == "DayAheadResult") IsEmptyList[typeof(TStorage).Name] = false; else IsEmptyList[type] = false; }
                else if (typeof(TStorage).Name == "IntraDayResult" || type == "IntraDayResult") {IntradayStorage.Add(new MarketContext(), results.Cast<IntraDayResult>().ToList()); if (typeof(TStorage).Name == "IntraDayResult") IsEmptyList[typeof(TStorage).Name] = false; else IsEmptyList[type] = false; }
                else if (typeof(TStorage).Name == "ControlReserveResult" || type == "ControlReserveResult") {ControlReserveStorage.Add(new MarketContext(), results.Cast<ControlReserveResult>().ToList()); if (typeof(TStorage).Name == "ControlReserveResult") IsEmptyList[typeof(TStorage).Name] = false; else IsEmptyList[type] = false; }
                else Add(results, type: results.First().GetType().Name);
            }
        }

        public void Add(IEnumerable<FuturesResult> results) => FuturesStorage.Add(new MarketContext(), results);
        public void Add(IEnumerable<DayAheadResult> results) => DayAheadStorage.Add(new MarketContext(), results);
        public void Add(IEnumerable<IntraDayResult> results) => IntradayStorage.Add(new MarketContext(), results);
        public void Add(IEnumerable<ControlReserveResult> results) => ControlReserveStorage.Add(new MarketContext(), results);

        public void CacheToDB()
        {
            FuturesStorage.CacheToDB(new MarketContext());
            DayAheadStorage.CacheToDB(new MarketContext());
            IntradayStorage.CacheToDB(new MarketContext());
            ControlReserveStorage.CacheToDB(new MarketContext());
        }

        public List<TStorage> GetResults<TStorage>(DateTime timeStamp, TimeSpan duration) where TStorage : StorageType
        {
            if (typeof(TStorage).Name == "FuturesResult") return !IsEmptyList[typeof(TStorage).Name] ? FuturesStorage.Get(new MarketContext(), timeStamp, duration).Cast<TStorage>().ToList() : new List<TStorage>();
            else if (typeof(TStorage).Name == "DayAheadResult") return !IsEmptyList[typeof(TStorage).Name] ? DayAheadStorage.Get(new MarketContext(), timeStamp, duration).Cast<TStorage>().ToList() : new List<TStorage>();
            else if (typeof(TStorage).Name == "IntraDayResult") return !IsEmptyList[typeof(TStorage).Name] ? IntradayStorage.Get(new MarketContext(), timeStamp, duration).Cast<TStorage>().ToList() : new List<TStorage>();
            else return !IsEmptyList[typeof(TStorage).Name] ? ControlReserveStorage.Get(this, timeStamp, duration).Cast<TStorage>().ToList() : new List<TStorage>();
        }

        public List<TStorage> GetResults<TStorage>(DateTime timeStamp) where TStorage : StorageType
        {
            if (typeof(TStorage).Name == "FuturesResult") return !IsEmptyList[typeof(TStorage).Name] ? FuturesStorage.Get(new MarketContext(), timeStamp).Cast<TStorage>().ToList() : new List<TStorage>();
            else if (typeof(TStorage).Name == "DayAheadResult") return !IsEmptyList[typeof(TStorage).Name] ? DayAheadStorage.Get(new MarketContext(), timeStamp).Cast<TStorage>().ToList() : new List<TStorage>();
            else if (typeof(TStorage).Name == "IntraDayResult") return !IsEmptyList[typeof(TStorage).Name] ? IntradayStorage.Get(new MarketContext(), timeStamp).Cast<TStorage>().ToList() : new List<TStorage>();
            else return !IsEmptyList[typeof(TStorage).Name] ? ControlReserveStorage.Get(this, timeStamp).Cast<TStorage>().ToList() : new List<TStorage>();
        }

        public List<MarketResult> GetResults(ShortCode shortCode, DateTime timeStamp)
        {
            List<MarketResult> results = new List<MarketResult>();
            if (shortCode.IsFutures) { if (!IsEmptyList["FuturesResult"]) results.AddRange(FuturesStorage.Get(new MarketContext(), timeStamp).ToList()); }
            else if (shortCode.IsDayAhead) { if (!IsEmptyList["DayAheadResult"]) results.AddRange(DayAheadStorage.Get(new MarketContext(), timeStamp).ToList()); }
            else if (shortCode.IsIntraDay) { if (!IsEmptyList["IntraDayResult"]) results.AddRange(IntradayStorage.Get(new MarketContext(), timeStamp).ToList()); }
            else if (shortCode.IsControlReserve) { if (!IsEmptyList["ControlReserveResult"]) results.AddRange(ControlReserveStorage.Get(new MarketContext(), timeStamp).ToList()); }
            return results;
        }

        public List<MarketResult> GetResults(ShortCode shortCode, DateTime timeStamp, TimeSpan duration)
        {
            List<MarketResult> results = new List<MarketResult>();
            if (shortCode.IsFutures) { if (!IsEmptyList["FuturesResult"]) results.AddRange(FuturesStorage.Get(new MarketContext(), timeStamp, duration).ToList()); }
            else if (shortCode.IsDayAhead) { if (!IsEmptyList["DayAheadResult"]) results.AddRange(DayAheadStorage.Get(new MarketContext(), timeStamp, duration).ToList()); }
            else if (shortCode.IsIntraDay) { if (!IsEmptyList["IntraDayResult"]) results.AddRange(IntradayStorage.Get(new MarketContext(), timeStamp, duration).ToList()); }
            else if (shortCode.IsControlReserve) { if (!IsEmptyList["ControlReserveResult"]) results.AddRange(ControlReserveStorage.Get(new MarketContext(), timeStamp, duration).ToList()); }
                return results;
            }*/


        public virtual DbSet<FuturesResult> FuturesResults { get; set; }
        public virtual DbSet<DayAheadResult> DayAheadResults { get; set; }
        public virtual DbSet<IntraDayResult> IntradayResults { get; set; }
        public virtual DbSet<ControlReserveResult> ControlReserveResults { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.MarketResults);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FuturesResult>().HasDiscriminator(prop => prop.ShortCode);
            modelBuilder.Entity<FuturesResult>().HasKey(prop => prop.Id);
            modelBuilder.Entity<FuturesResult>()
                .Ignore(prop => prop.IsIntraDay)
                .Ignore(prop => prop.IsDayAhead)
                .Ignore(prop => prop.IsControlReserve)
                .Ignore(prop => prop.IsBlock)
                .Ignore(prop => prop.ProRataVolumePurchase)
                .Ignore(prop => prop.ProRataVolumeSell)
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => ShortCode.FromString(cd));

            modelBuilder.Entity<DayAheadResult>().HasDiscriminator(prop => prop.ShortCode);
            modelBuilder.Entity<DayAheadResult>().HasKey(prop => prop.Id);
            modelBuilder.Entity<DayAheadResult>()
                .Ignore(prop => prop.IsIntraDay)
                .Ignore(prop => prop.IsDayAhead)
                .Ignore(prop => prop.IsControlReserve)
                .Ignore(prop => prop.IsBlock)
                .Ignore(prop => prop.ProRataVolumePurchase)
                .Ignore(prop => prop.ProRataVolumeSell)
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => ShortCode.FromString(cd));


            modelBuilder.Entity<IntraDayResult>().HasKey(prop => prop.Id);
            modelBuilder.Entity<IntraDayResult>().HasDiscriminator(prop => prop.ShortCode);
            modelBuilder.Entity<IntraDayResult>()
                .Ignore(prop => prop.IsDayAhead)
                .Ignore(prop => prop.IsIntraDay)
                .Ignore(prop => prop.IsControlReserve)
                .Ignore(prop => prop.ProRataVolumePurchase)
                .Ignore(prop => prop.ProRataVolumeSell)
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => ShortCode.FromString(cd));

            modelBuilder.Entity<ControlReserveResult>().HasKey(prop => prop.Id);
            modelBuilder.Entity<ControlReserveResult>().HasDiscriminator(prop => prop.ShortCode);
            modelBuilder.Entity<ControlReserveResult>()
                .Ignore(prop => prop.IsDayAhead)
                .Ignore(prop => prop.IsIntraDay)
                .Ignore(prop => prop.IsControlReserve)
                .Ignore(prop => prop.IsBlock)
                .Ignore(prop => prop.ProRataVolumePurchase)
                .Ignore(prop => prop.ProRataVolumeSell)
                .Property(prop => prop.ShortCode)
                .HasConversion(el => el.ToString(), cd => ShortCode.FromString(cd));          

            base.OnModelCreating(modelBuilder);
        }
    }
}
