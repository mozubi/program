﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;

namespace winMoz.Data.Access.Contexts
{
    public class TYNDP2018_Scenario_Context : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.TYNP2018Scenario);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TYNDP2018_Scenario_Dto>()
                .HasKey(prop => prop.Country);

            base.OnModelCreating(modelBuilder);
        }
    }
    public class Generation2025BEST_Context : TYNDP2018_Scenario_Context
    {
        public virtual DbSet<TYNDP2018_Scenario_Dto> Generation2025BEST { get; set; }
    }

    public class Generation2030DG_Context : TYNDP2018_Scenario_Context
    {
        public virtual DbSet<TYNDP2018_Scenario_Dto> Generation2030DG { get; set; }
    }

    public class Generation2030EUCO_Context : TYNDP2018_Scenario_Context
    {
        public virtual DbSet<TYNDP2018_Scenario_Dto> Generation2030EUCO { get; set; }
    }

    public class Generation2030ST_Context : TYNDP2018_Scenario_Context
    {
        public virtual DbSet<TYNDP2018_Scenario_Dto> Generation2030ST { get; set; }
    }

    public class Generation2040DG_Context : TYNDP2018_Scenario_Context
    {
        public virtual DbSet<TYNDP2018_Scenario_Dto> Generation2040DG { get; set; }
    }

    public class Generation2040GCA_Context : TYNDP2018_Scenario_Context
    {
        public virtual DbSet<TYNDP2018_Scenario_Dto> Generation2040GCA { get; set; }
    }

    public class Generation2040ST_Context : TYNDP2018_Scenario_Context
    {
        public virtual DbSet<TYNDP2018_Scenario_Dto> Generation2040ST { get; set; }
    }
}
