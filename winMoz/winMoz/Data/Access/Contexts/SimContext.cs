﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using winMoz.Assets;
using winMoz.Assets.Loads;
using winMoz.Assets.Plants;
using AgeTypes = winMoz.Assets.Plants.Plant_Thermal.AgeTypes;
using winMoz.Simulation;
using winMoz.Trading.Strategies;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets.Schedules;
using winMoz.Information;
using winMoz.Markets.Elements;
using winMoz.Markets;
using winMoz.Data.Access.DTO;
using System.Reflection;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.Bids;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using winMoz.Helper;
using System.ComponentModel.DataAnnotations.Schema;

namespace winMoz.Data.Access.Contexts
{
    public class SimContext : DbContext
    {
        public List<MFRR_Attributes> CachedAttributes { get; private set; } = new List<MFRR_Attributes>();

        int ScenarioID;

        public void CacheAttributes()
        {
            if (ScenarioID > -1)
                CachedAttributes = this.MFRR_Attributes.Where(item => item.ScenarioID == ScenarioID).ToList();
            else
                CachedAttributes = this.MFRR_Attributes.ToList();
        }

        public void SetScenarioID(int scenarioID) => ScenarioID = scenarioID;

        public List<MFRR_Attributes> GetAttributes()
        {
            if (CachedAttributes.Count() == 0)
                CacheAttributes();
            var Attributes = CachedAttributes.Where(item => ScenarioID > -1 ? item.ScenarioID == ScenarioID : true).ToList();
            if (Attributes.Count() < CachedAttributes.Count())
                CachedAttributes = Attributes;
            return Attributes;
        }
        public virtual DbSet<SLPValue> SLPs { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<EconomicAttributes> Economics { get; set; }
        public virtual DbSet<Tynp> Tynp { get; set; }
        public virtual DbSet<Plant_PV> Plant_PV { get; set; }
        public virtual DbSet<Plant_Hydro> Plant_Hydro { get; set; }
        public virtual DbSet<Plant_Storage> Plant_Storage { get; set; }
        public virtual DbSet<Plant_Wind> Plant_Wind { get; set; }
        public virtual DbSet<Plant_Thermal> Plant_Thermal { get; set; }
        public virtual DbSet<Block> Block { get; set; }
        public virtual DbSet<Load_SLP> Load_SLP { get; set; }
        public virtual DbSet<Load_Grid> Load_Grid { get; set; }

        public virtual DbSet<DSO> DSO { get; set; }
        public virtual DbSet<BalancingGroup> BalancingGroup { get; set; }

        public virtual DbSet<AvailabiltyScheduleItem> AvailabiltySchedule { get; set; }

        public virtual DbSet<FuturesMarket> FuturesMarket { get; set; }
        public virtual DbSet<DayAheadMarket> DayAheadMarket { get; set; }
        public virtual DbSet<IntradayMarket> IntradayMarket { get; set; }
        public virtual DbSet<ControlReserve_Capacity> ControlReserve_Capacity { get; set; }
        public virtual DbSet<ControlReserve_Energy> ControlReserve_Energy { get; set; }

        public virtual DbSet<MFRR_Attributes> MFRR_Attributes { get; set; }
        public virtual DbSet<ControlReserve_Activation> ControlReserve_Activation { get; set; }
        public virtual DbSet<ControlReserveActivationVolumeHandler> ControlReserveActivationVolumeHandler { get; set; }
        public virtual DbSet<ControlReserveTenderVolumeHandler> ControlReserveTenderVolumeHandler { get; set; }
        public virtual DbSet<DeviationFromRZSaldo_Handler> DeviationFromRZSaldo_Handler { get; set; }
        public virtual DbSet<ReBAP_Calculation> ReBAP_Calculation { get; set; }
        public virtual DbSet<IGCC_Calculation> IGCC_Calculation { get; set; }
        public virtual DbSet<Scenario> ScenarioData { get; set; }
        //public virtual DbSet<StartDateDto> StartDate { get; set; }
        public bool IsRead;

        public SimContext() : this(true) { }
        public SimContext(bool isRead = true) : 
            base(Config.DBOptions.simContextOptions)      
        {
            IsRead = isRead;
        }

        /// <summary>
        /// Methode zum Erzeugen eines Standard Assets. Wird so verwendet, da die aktuelle EF Core Version kein Table per Hierarchy unterstützt und net Framework 4.8, EF Core 5 nicht unterstützt.
        /// </summary>
        /// <typeparam name="TAsset"></typeparam>
        /// <param name="asset"></param>
        /// <returns></returns>
        static private EntityTypeBuilder<TAsset> AssetCreate<TAsset>(EntityTypeBuilder<TAsset> asset) where TAsset : Asset
        {
            asset.HasOne(el => el.Location)
                        .WithMany()
                        .OnDelete(DeleteBehavior.NoAction);
            asset.HasOne(el => el.Economics)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
            asset.Property(prop => prop.TradingStrategy)
                        .HasConversion(a => a.ToString(), b => TradingStrategyType.FromString(b));
            asset.HasOne(prop => prop.BalancingGroup as BalancingGroup)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);
            asset.Ignore(prop => prop.ControlReserveSchedule);
            asset.Ignore(prop => prop._AsumedCallProbability);
            return asset;
        }

        static private EntityTypeBuilder<TMarket> OwnsMarketAttribute<TAttributes, TMarket>(EntityTypeBuilder<TMarket> market) where TAttributes : MarketAttributes where TMarket : Market
        {
            market.OwnsOne(prop => prop.Attributes as TAttributes, attr =>
            {
                attr.Property(prop => prop.LocalMACode)
                    .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));
                attr.OwnsOne(prop => prop.EventAttributes, evt => {
                        evt.Ignore("StartEventTime");
                        evt.Property(prop => prop.StartEventTimeDurationFromStart).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                        evt.Ignore("StartDeterminationTime");
                        evt.Property(prop => prop.StartDeterminationTimeDurationFromStart).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                        evt.Property(prop => prop.DeterminationDuration).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                        evt.Property(prop => prop.DeterminationStep).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                    });
                attr.Ignore("_BlockTypes");
                attr.ToTable("Markets");
            });
            return market;
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Location
            modelBuilder.Entity<Location>().ToTable("Locations").HasKey(loc => loc.Postcode);

            //SLP
            modelBuilder.Entity<SLPValue>().ToTable("SLP").HasNoKey().Property(el => el.Profile).HasConversion(a => a.ToString(), b => (SLP.Profile)Enum.Parse(typeof(SLP.Profile), b, true));

            //Economics
            modelBuilder.Entity<EconomicAttributes>().ToTable("Economics").HasKey(el => el.SubType);

            //Tynp
            modelBuilder.Entity<Tynp>().ToTable("TYNP").HasKey(nt => nt.Category);

            modelBuilder.Entity<Tynp>().Property(el => el.AgeType).HasConversion(agetype => agetype.ToString(), str => (AgeTypes)Enum.Parse(typeof(AgeTypes), str, true));
            modelBuilder.Entity<Tynp>().Property(el => el.Commodity).HasConversion(commodity => commodity.ToString(), str => (Commodities.Commodity)Enum.Parse(typeof(Commodities.Commodity), str, true));

            modelBuilder.Entity<Tynp>().OwnsOne(typ => typ.TransitionTime, time =>
            {
                time.Property(t => t.HotCold).HasConversion(toDb => toDb.TotalMinutes, fromDb => TimeSpan.FromMinutes(fromDb));
                time.Property(t => t.HotWarm).HasConversion(toDb => toDb.TotalMinutes, fromDb => TimeSpan.FromMinutes(fromDb));
                time.Property(t => t.MinHoursOff).HasConversion(toDb => toDb.TotalHours, fromDb => TimeSpan.FromHours(fromDb));
                time.Property(t => t.MinHoursOn).HasConversion(toDb => toDb.TotalHours, fromDb => TimeSpan.FromHours(fromDb));
            });
            modelBuilder.Entity<Tynp>().OwnsOne(typ => typ.Efficiency, eff =>
            {
                eff.Property(el => el.MinimimumStableGeneration).HasColumnName("MinimumStableGeneration");
            });

            // Plant_Storage
            modelBuilder.Entity<Plant_Storage>(plant =>
                {
                    AssetCreate(plant);
                    plant.Ignore(el => el.Efficiency);

                    plant.Ignore(el => el.StateOfCharge);
                }
              );

            // Plant_Wind
            modelBuilder.Entity<Plant_Wind>(plant =>
                 {
                     AssetCreate(plant);
                     plant.Ignore(el => el.PowerCurve);
                 }
                    );



            // Load_SLP
            modelBuilder.Entity<Load_SLP>(load =>
            {
                AssetCreate(load);
                load.Property(el => el.SLP).HasConversion(a => string.Join(";", a.Profiles.Select(el => string.Join(",", el.Type.ToString(), el.EnergyAnnualkWh.ToString()))),
                                                      b => new SLP(b.Split(';').Select(el => el.Split(',')).Select(el => ValueTuple.Create(el[0], Convert.ToSingle(el[1]))).ToArray()));
            });


            modelBuilder.Entity<Difference>(load =>
            {
                AssetCreate(load);
            });

            // Load_Grid
            modelBuilder.Entity<Load_Grid>(load =>
            {
                AssetCreate(load);
            });

            // Plant_PV
            modelBuilder.Entity<Plant_PV>(plant =>
            {
                AssetCreate(plant);
            });


            // Plant_Thermal

            if (IsRead)
                modelBuilder.Entity<Plant_Thermal>()
                .HasMany(prop => prop.Units)
                .WithOne(prop => prop.Plant)
                .HasForeignKey(prop => prop.PlantId)
                .OnDelete(DeleteBehavior.Cascade);
            else
                modelBuilder.Entity<Plant_Thermal>()
                .Ignore(prop => prop.Units);

            modelBuilder.Entity<Plant_Thermal>(plant =>
            {
                AssetCreate(plant);
                plant.Ignore(prop => prop.TLP);
            });

            //Plant_Biomass
            modelBuilder.Entity<Plant_Biomass>(plant =>
            {
                AssetCreate(plant);
                plant.Ignore(prop => prop.TLP);
            });

            //Plant_Hydro
            modelBuilder.Entity<Plant_Hydro>(plant =>
            {
                AssetCreate(plant);
            });


            // Balancing Group
            modelBuilder.Entity<TSO>(tso =>
            {
                tso.HasMany(el => el.dsos)
                .WithOne(el => el.TSO)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<DSO>()
                 .Ignore(prop => prop.TimeSeries);

            modelBuilder.Entity<BalancingGroup>(bg =>
            {
                bg.HasOne(el => el.DSO)
                .WithMany(el => el._BalancingGroups)
                .OnDelete(DeleteBehavior.Cascade);
                bg.Ignore(el => el.Assets);
            }
            );

            modelBuilder.Entity<Block>(unit =>
            {
                AssetCreate(unit);
                unit.HasMany(el => el.AvailabiltySchedule);
                unit.HasOne(el => el.ThermalData).WithMany()
                .OnDelete(DeleteBehavior.NoAction);
                //unit.ToTable("Block");
            });
            modelBuilder.Entity<Block_Kond>().HasBaseType<Block>();
            modelBuilder.Entity<Block_CHP>().HasBaseType<Block>();

            modelBuilder.Entity<AvailabiltyScheduleItem>(sched =>
            {
                sched.Property<int>("Id");
                sched.HasKey("Id");
            });

            //Markets

            modelBuilder.Entity<Market>(market =>
            {
                market.Ignore(prop => prop.Attributes);
                market.Ignore(prop => prop.EventTimeframe);
                market.Ignore(prop => prop.Results);
                market.HasKey(prop => prop.Id);
                market.ToTable("Markets");
            });

            modelBuilder.Entity<ControlReserve_Activation>().Ignore(prop => prop.EventTimeframe);

            modelBuilder.Entity<ControlReserve_Activation>().Ignore(prop => prop.Results);

            modelBuilder.Entity<ControlReserve_Activation>()
                .OwnsOne(prop => prop.Attributes, attr => attr.OwnsOne(prop => prop.EventAttributes));
            
            modelBuilder.Entity<ControlReserve_Activation>().OwnsOne(prop => prop.Attributes, attr => attr.OwnsOne(prop => prop.EventAttributes, evt => {
                evt.Ignore("StartEventTime");
                evt.Property(prop => prop.StartEventTimeDurationFromStart).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                evt.Ignore("StartDeterminationTime");
                evt.Property(prop => prop.StartDeterminationTimeDurationFromStart).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                evt.Property(prop => prop.DeterminationDuration).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                evt.Property(prop => prop.DeterminationStep).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
            }));

            modelBuilder.Entity<ControlReserveTenderVolumeHandler>()
               .Property(prop => prop.MarketIDs)
               .HasConversion(a => string.Join(";", a), b => b.Split(';').Select(item => (int)Convert.ToInt64(item)).ToList());


            modelBuilder.Entity<ControlReserveTenderVolumeHandler>()
                .OwnsOne(prop => prop.Attributes);

            modelBuilder.Entity<ControlReserveTenderVolumeHandler>()
                .HasMany(prop => prop.Markets)
                .WithOne()
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<ControlReserveActivationVolumeHandler>()
               .Property(prop => prop.ActivationIDs)
               .HasConversion(a => string.Join(";", a), b => b.Split(';').Select(item => (int)Convert.ToInt64(item)).ToList());

            modelBuilder.Entity<ControlReserveActivationVolumeHandler>()
                .HasMany(prop => prop.Activations)
                .WithOne()
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<ControlReserveActivationVolumeHandler>()
                .HasOne(prop => prop.DeviationFromRZSaldo)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<ControlReserveActivationVolumeHandler>()
                .HasOne(prop => prop.IGCC)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);



            modelBuilder.Entity<ReBAP_Calculation>().Ignore(prop => prop.EventTimeframe);

            modelBuilder.Entity<ReBAP_Calculation>().OwnsOne(prop => prop.Attributes, evt => {
                    evt.Ignore("StartEventTime");
                    evt.Property(prop => prop.StartEventTimeDurationFromStart).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                    evt.Ignore("StartDeterminationTime");
                    evt.Property(prop => prop.StartDeterminationTimeDurationFromStart).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                    evt.Property(prop => prop.DeterminationDuration).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                    evt.Property(prop => prop.DeterminationStep).HasConversion(ts => ts.ConvertToString(), str => str.TimeSpanSaveFromString());
                });


            modelBuilder.Entity<DayAheadMarket>(market =>
            {
                OwnsMarketAttribute<DayAheadAttributes, DayAheadMarket>(market);

            });

            modelBuilder.Entity<IntradayMarket>(market =>
            {
                OwnsMarketAttribute<IntradayAttributes, IntradayMarket>(market);
            });

            modelBuilder.Entity<FuturesMarket>(market =>
            {
                OwnsMarketAttribute<FuturesAttributes, FuturesMarket>(market);
            });

            modelBuilder.Entity<ControlReserve_Capacity>(market =>
            {
                OwnsMarketAttribute<ControlReserve_Capacity_Attributes, ControlReserve_Capacity>(market);
                market.HasOne(prop => prop.EnergyMarket)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
            });

            modelBuilder.Entity<ControlReserve_Energy>(market =>
            {
                OwnsMarketAttribute<ControlReserve_Energy_Attributes, ControlReserve_Energy>(market);
                market.HasOne(prop => prop.Activation)
                    .WithMany().OnDelete(DeleteBehavior.NoAction);
                market.HasOne(prop => prop.Intraday)
                .WithMany().OnDelete(DeleteBehavior.NoAction);
            });
            modelBuilder.Entity<ReBAP_Calculation>().OwnsOne(prop => prop.Attributes);

            modelBuilder.Entity<Scenario>().HasKey(prop => prop.Total_ID);

            modelBuilder.Entity<Scenario>().Property(prop => prop.LocalMACode)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));

            modelBuilder.Entity<Scenario>().Property(prop => prop.AdditionalAssetsMaStR)
                .HasConversion(el => string.Join(";", el), cd => cd == "" ? new List<int>() : cd.Split(';').Select(item => (int)Convert.ToInt64(item)).ToList());

            modelBuilder.Entity<Scenario>().Property(prop => prop.TYNDP2018Scenario)
                .HasConversion(el => el.ToString(), cd => (TYNDP2018_ScenarioCalculation.ScenarioEnum)Enum.Parse(typeof(TYNDP2018_ScenarioCalculation.ScenarioEnum), cd));

            // modelBuilder.Entity<Scenario>().HasIndex("Assets_ID").IsUnique(false);
            //
            modelBuilder.Entity<Scenario>(scen =>
            {
                scen.Ignore(prop => prop.Markets);
                scen.Ignore(prop => prop.tsos);
                scen.Ignore(prop => prop.controlReserveTenderVolumeHandlers);
                scen.Ignore(prop => prop.mFRR_Attributes);
                scen.Ignore(prop => prop.controlReserveActivationVolumeHandlers);
                scen.Ignore(prop => prop.ControlReserveActivations);
                scen.Ignore(prop => prop.ReBAP_Calculations);
            });
            ///modelBuilder.Entity<Scenario>().HasOne<Market>().WithMany().HasForeignKey(scen => scen.Markets_ID).HasPrincipalKey(el => el.ScenarioID);
           
            //modelBuilder.Entity<Scenario>().HasMany(el => el.controlReserveTenderVolumeHandlers).WithOne().HasForeignKey(market => market.ScenarioID).HasPrincipalKey("Markets_ID").OnDelete(DeleteBehavior.NoAction);
            //modelBuilder.Entity<Scenario>().HasMany(el => el.mFRR_Attributes).WithOne().HasForeignKey(market => market.ScenarioID).HasPrincipalKey("Markets_ID").OnDelete(DeleteBehavior.NoAction);

            //modelBuilder.Entity<Scenario>().HasMany(el => el.ControlReserveActivations).WithOne().HasForeignKey(market => market.ScenarioID).HasPrincipalKey("Markets_ID").OnDelete(DeleteBehavior.NoAction);
            //modelBuilder.Entity<Scenario>().HasMany(el => el.ReBAP_Calculations).WithOne().HasForeignKey(market => market.ScenarioID).HasPrincipalKey("Markets_ID").OnDelete(DeleteBehavior.NoAction);

            //modelBuilder.Entity<Scenario>().HasIndex(el => el.Assets_ID).IsUnique(false);


            base.OnModelCreating(modelBuilder);

        }
    }
}
