﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using winMoz.Assets;
using winMoz.Assets.Loads;
using winMoz.Assets.Plants;
using AgeTypes = winMoz.Assets.Plants.Plant_Thermal.AgeTypes;
using winMoz.Simulation;
using winMoz.Trading.Strategies;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets.Schedules;
using winMoz.Information;

namespace winMoz.Data.Access.Contexts
{
    public class SumScheduleContext : DbContext
    {
       
        public Dictionary<string, bool> IsEmptyList = new Dictionary<string, bool>() { { "FuturesResult", true }, { "DayAheadResult", true }, { "IntraDayResult", true }, { "ControlReserveResult", true } };

        public virtual DbSet<SumScheduleItem> SumSchedules { get; set; }

        public bool IsRead;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.SumSchedule);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SumScheduleItem>()
                .Ignore(prop => prop.AssetId)
                .Ignore(prop => prop.ThermalBlockId);

            base.OnModelCreating(modelBuilder);

        }
    }
}
