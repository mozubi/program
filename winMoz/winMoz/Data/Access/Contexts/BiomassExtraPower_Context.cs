﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;

namespace winMoz.Data.Access.Contexts
{
    public class BiomassExtraPower_Context : DbContext
    {
        public virtual DbSet<BiomassExtraPower_Dto> BiomassExtraPower { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.BiomassExtraPower);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BiomassExtraPower_Dto>().HasKey(prop => prop.Year);

            base.OnModelCreating(modelBuilder);
        }
    }
    
}
