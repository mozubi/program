﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using winMoz.Simulation;

using winMoz.Markets.Results;
using winMoz.Data.Access.DTO.Keys;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.Contexts
{
    public class ExternalMarketResultsContext : DbContext
    { 

        public Dictionary<string, bool> IsEmptyList = new Dictionary<string, bool>() { { "FuturesResult", true }, { "DayAheadResult", true }, { "IntraDayResult", true }, { "ControlReserveResult", true } };

        public virtual DbSet<ExternalMarketResult> ExternalMarketResultsDayAhead { get; set; }


        public bool IsRead;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.ExternalMarketResults);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExternalMarketResult>()
                .HasKey(prop => new ExternalMarketResults_Key(prop.TimeStamp, prop.MarketArea));

            modelBuilder.Entity<ExternalMarketResult>()
                .Property(prop => prop.MarketArea)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));


            base.OnModelCreating(modelBuilder);

        }
    }
}
