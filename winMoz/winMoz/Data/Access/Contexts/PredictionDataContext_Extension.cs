﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;
using System.Collections.Generic;

namespace winMoz.Data.Access.Contexts
{
    // TODO: Make context un-editable by hiding behind layer
    public class PredictionDataContext_Extension : DbContext
    {
        public virtual DbSet<DayAheadDataAndereQuelle2015> DayAheadAndereQuelle2015 { get; set; }

        public override int SaveChanges()
        {
            throw new InvalidOperationException("This context is read-only.");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.PredictionData);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GenerationUnits>().ToTable("Generation_Units");
            modelBuilder.Entity<GenerationUnits>().HasNoKey();
            modelBuilder.Entity<WxNinja>().ToTable("Weather");
            modelBuilder.Entity<WxNinja>().HasNoKey();
            modelBuilder.Entity<LoadData>().HasNoKey().Ignore(prop => prop.Id);
            base.OnModelCreating(modelBuilder);
        }
    }


}
