﻿using Microsoft.EntityFrameworkCore;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;

namespace winMoz.Data.Access.Contexts
{
    public class WindDtoContext : DbContext
    {
        public virtual DbSet<WindDto> WindPlants { get; set; }

        public virtual DbSet<WindAggDto> WindPlantsAggregate { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.WindPlants);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WindDto>().Ignore(p => p.PowerCurve);

            modelBuilder.Entity<WindAggDto>().Ignore(p => p.PowerCurve);

            base.OnModelCreating(modelBuilder);
        }
    }
}
