﻿using System;
using Microsoft.EntityFrameworkCore;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;
using winMoz.Markets.Elements;
using System.Collections.Generic;
using System.Linq;


namespace winMoz.Data.Access.Contexts
{
    public class PlotAndOutput_Variables_Plot_Context : DbContext
    {
        public List<PlotAndOutput_Variables_Dto> CachedPlotAndOutputVariablesPlot { get; private set; } = new List<PlotAndOutput_Variables_Dto>();

        public bool IsEmpty = false;

        public void CachePlotAndOutputVariables()
        {
            CachedPlotAndOutputVariablesPlot = this.Set<PlotAndOutput_Variables_Dto>()
                .Include(el => el.Element)
                .Include(el => el.Times).ToList();
            if (CachedPlotAndOutputVariablesPlot.Count() == 0)
                IsEmpty = true;
        }

        public List<PlotAndOutput_Variables_Dto> Get()
        {
            if (CachedPlotAndOutputVariablesPlot.Count() == 0 && !IsEmpty)
                CachePlotAndOutputVariables();
            return CachedPlotAndOutputVariablesPlot;
        }

        public virtual DbSet<PlotAndOutput_Variables_Dto> PlotAndOutput_Variables_Plot { get; set; }
        public virtual DbSet<PlotAndOutput_Element> Elements { get; set; }
        public virtual DbSet<PlotAndOutput_Times> Timess { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.ConnStrings.PlotAndOutputVariablesPlot);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PlotAndOutput_Variables_Dto>()
                .HasOne(prop => prop.Element);

            modelBuilder.Entity<PlotAndOutput_Variables_Dto>()
                .HasOne(prop => prop.Times);

            modelBuilder.Entity<PlotAndOutput_Element_Bid>()
                .HasBaseType(typeof(PlotAndOutput_Element));

            modelBuilder.Entity<PlotAndOutput_Element_MeritOrder>()
                .HasBaseType(typeof(PlotAndOutput_Element));

            modelBuilder.Entity<PlotAndOutput_Element_CoupledMeritOrder>()
                .HasBaseType(typeof(PlotAndOutput_Element));

            modelBuilder.Entity<PlotAndOutput_Element_CoupledMeritOrder>()
                .Property(prop => prop.MarketArea)
                .HasConversion(el => el.ToString(), cd => MarketAreaEnum.FromStringOrOtherString(cd));

            modelBuilder.Entity<PlotAndOutput_Element_Schedule>()
                .HasBaseType(typeof(PlotAndOutput_Element));

            modelBuilder.Entity<PlotAndOutput_Element_MarketResults>()
                .HasBaseType(typeof(PlotAndOutput_Element));

            modelBuilder.Entity<PlotAndOutput_Times_Durations>()
                .HasBaseType(typeof(PlotAndOutput_Times));

            modelBuilder.Entity<PlotAndOutput_Variables_Dto>()
                .HasKey(prop => prop.Id);

            base.OnModelCreating(modelBuilder);
        }
    }
}
