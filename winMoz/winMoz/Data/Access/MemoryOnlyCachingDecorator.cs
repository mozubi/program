﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;

namespace winMoz.Data.Access
{
    public class MemoryOnlyCachingDecorator<TEntity> : IRepository<TEntity> where TEntity : class, IComparable<TEntity>
    {
        private readonly IRepository<TEntity> _decoratedRepo;
        private readonly MemoryCache _cache;
        private readonly Expression<Func<TEntity, bool>> _startPredicate;
        public MemoryOnlyCachingDecorator(IRepository<TEntity> decoratedRepo, Expression<Func<TEntity, bool>> startPredicate)
        {
            _decoratedRepo = decoratedRepo;
            _cache = MemoryCache.Default;
            _startPredicate = startPredicate;
        }
        public IEnumerable<TEntity> Get()
        {
                SortedSet<TEntity> values = _cache.Get(typeof(TEntity).Name) as SortedSet<TEntity>;
                if (values == null)
                {
                    values = new SortedSet<TEntity>(_decoratedRepo.Get().OrderBy(el => el).AsQueryable().Where(_startPredicate));
                    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                    cacheItemPolicy.Priority = CacheItemPriority.NotRemovable;
                    _cache.Set(typeof(TEntity).Name, values, cacheItemPolicy);
                }
                return values;
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Get().AsQueryable().SingleOrDefault(predicate);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Get().AsQueryable().FirstOrDefault(predicate);
        }

        public void Add(TEntity entity)
        {
            SortedSet<TEntity> values = Get() as SortedSet<TEntity>;
            values.Add(entity);
        }

        public void Add(IEnumerable<TEntity> entity)
        {
            SortedSet<TEntity> values = Get() as SortedSet<TEntity>;
            values.UnionWith(entity);
        }

        public void Delete(TEntity entity)
        {
            SortedSet<TEntity> values = Get() as SortedSet<TEntity>;
            values.Remove(entity);
        }

        public void Update(IEnumerable<TEntity> entities)
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            _cache.Remove(typeof(TEntity).Name);
        }

        public void Dispose()
        {
            _decoratedRepo.Dispose();
        }
    }
}
