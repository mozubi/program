﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace winMoz.Data.Access.Repositories
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        IEnumerable<TEntity> Get();
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        void Add(IEnumerable<TEntity> entity);
        void Delete(TEntity entity);
        void Update(IEnumerable<TEntity> entities);
        void Update(TEntity entity);

        void SaveChanges();
    }
}
