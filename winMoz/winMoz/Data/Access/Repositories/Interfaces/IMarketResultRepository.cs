﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.Bids;
using winMoz.Markets.Results;

namespace winMoz.Data.Access.Repositories
{
    /// <summary>
    /// MarketResultsRepository
    /// </summary>
    public interface IMarketResultRepository : IRepository<IMarketResult>
    {
        #region Results 

        void CacheToDB();


        IEnumerable<MarketResult> GetResultsFromDB(DateTime timeStamp, TimeSpan duration);

        IEnumerable<TResult> GetResults<TResult>(DateTime timeStamp, TimeSpan duration) where TResult : MarketResult;

        IEnumerable<TResult> GetResults<TResult>(DateTime timeStamp) where TResult : MarketResult;



        MarketResult GetResult(ShortCode shortCode, DateTime timeStamp);

        IEnumerable<MarketResult> GetResults(ShortCode shortCode, DateTime timeStamp, TimeSpan duration);
        #endregion
    }
}