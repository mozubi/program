﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Contexts;

namespace winMoz.Data.Access.Repositories
{
    public class BiomassExtraPower_Repository : Repository<BiomassExtraPower_Dto>
    {
        public BiomassExtraPower_Repository() : base(new BiomassExtraPower_Context())
        {
        }

        public float GetForYear(int year)
            => this.Get().Where(item => item.Year <= year).Select(item => item.BiomassExtraPowerKW / 1000.0F).Sum();

        public int GetFirstYear()
            => this.Get().Select(item => item.Year).First();
    }

  

}
