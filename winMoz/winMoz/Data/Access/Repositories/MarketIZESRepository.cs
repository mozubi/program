﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.ControlReserve;
using Microsoft.EntityFrameworkCore;
using winMoz.Markets;
using winMoz.Markets.Elements;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.Elements;
using winMoz.Simulation;
using System.Data.SQLite;
using winMoz.Information;

namespace winMoz.Data.Access.Repositories
{
    public static class MarketIZESRepository
    {
        private static SimContext _context = new SimContext();

        public static MFRR_Attributes Get_mFRRAttributes(int scenarioID = -1) => _context.GetAttributes().FirstOrDefault();

        public static void SetScenarioID(int scenarioID) => _context.SetScenarioID(scenarioID);

        private static IQueryable<TMarket> GetBasicMarkets<TMarket, TAttributes>(int ScenarioID) where TMarket : class, IMarket
        {
            return _context.Set<TMarket>()
                .Where(market => ScenarioID == market.ScenarioID)
                .Include(el => el.Attributes);

        }

        public static List<Market> GetMarkets(int ScenarioID = 0, bool withIntitialization = true)
        {
            List<Market> Markets = new List<Market>();
            Markets.AddRange(GetFuturesIZES(ScenarioID, withIntitialization));
            Markets.AddRange(GetDayAheadIZES(ScenarioID, withIntitialization));
            Markets.AddRange(GetControlReserveMarketsAndIntradays(ScenarioID, withIntitialization));
            var MarketIds = Markets.Select(item => item.Id).ToList();
            Markets.AddRange(GetIntradayIZES(ScenarioID, marketIdsAlreadyInitialized: Markets.Select(market => market.Id).ToList(), withInitialization: withIntitialization));
            return Markets;
        }

        public static List<ControlReserve_Activation> GetControlReserveActivation(int ScenarioID = 0, bool withInitialization = true)
        {
            var activations = _context.Set<ControlReserve_Activation>()
               .Where(handler => ScenarioID == handler.ScenarioID)
               .ToList();
           activations.ForEach(act => { if (withInitialization) act.Initialize(); });
            return activations;
        }

        public static List<ControlReserveTenderVolumeHandler> GetControlReserveTenderVolumeHandlers(int ScenarioID = 0, bool withInitialization = true, bool withMarkets = false)
        {
            var demandHandlers = _context.Set<ControlReserveTenderVolumeHandler>()
               .Where(handler => ScenarioID == handler.ScenarioID)
               .ToList();
            demandHandlers.ForEach(handler => {
                if (withInitialization) handler.Initialize();
                if (withMarkets) handler.SetMarkets(GetMarkets(ScenarioID));
            });
            return demandHandlers;
        }

        public static List<ControlReserveActivationVolumeHandler> GetControlReserveActivationVolumeHandlers(int ScenarioID = 0, bool withInitialization = true, bool withActivations = false)
        {
            var activationVolumeHandlers = _context.Set<ControlReserveActivationVolumeHandler>()
               .Where(handler => ScenarioID == handler.ScenarioID)
               .Include(handler => handler.IGCC)
               .Include(handler => handler.DeviationFromRZSaldo)
               .ToList();
            activationVolumeHandlers.ForEach(handler => {
                if (withInitialization) handler.Initialize();
                if (withInitialization) handler.IGCC.Initialize();
                if (withInitialization) handler.DeviationFromRZSaldo.Initialize();
                if (withActivations) handler.SetActivations(GetControlReserveActivation(ScenarioID));
            });
            return activationVolumeHandlers;
        }

        public static List<ReBAP_Calculation> GetReBAP_Calculations(int ScenarioID = 0, bool withInitialization = true, bool includeIGCC = false)
        {
            var reBAP_CalculationsQuery = _context.Set<ReBAP_Calculation>()
               .Where(handler => ScenarioID == handler.ScenarioID);
            List<ReBAP_Calculation> reBAP_Calculations = new List<ReBAP_Calculation>();
            if (includeIGCC)
            {
                reBAP_Calculations = reBAP_CalculationsQuery.Include(item => item.IGCC).ToList();
            }
            else
                reBAP_Calculations = reBAP_CalculationsQuery.ToList();
            reBAP_Calculations.ForEach(calc =>
            {
                if (withInitialization) calc.Initialize();
            });
            return reBAP_Calculations;
        }

        public static List<IGCC_Calculation> GetIGCCs(int ScenarioID = 0, bool withInitialization = true)
        {
            var IGCC_Calculations = _context.Set<IGCC_Calculation>()
               .Where(igcc => ScenarioID == igcc.ScenarioID)
               .ToList();
            IGCC_Calculations.ForEach(calc =>
            {
                if (withInitialization) calc.Initialize();
            });
            return IGCC_Calculations;
        }

        public static List<DeviationFromRZSaldo_Handler> GetDeviationFromRZSaldoHandlers(int ScenarioID = 0, bool withInitialization = true)
        {
            var DeviationHandlers = _context.Set<DeviationFromRZSaldo_Handler>()
               .Where(handler => ScenarioID == handler.ScenarioID)
               .ToList();
            DeviationHandlers.ForEach(handler =>
            {
                if (withInitialization) handler.Initialize();
            });
            return DeviationHandlers;
        }

        public static List<FuturesMarket> GetFuturesIZES(int ScenarioID = 0, bool withInitialization = true)
        {
            var markets = GetBasicMarkets<FuturesMarket, MarketAttributes>(ScenarioID)
                .ToList();

            markets.ForEach(market =>
            {
                if (withInitialization) market.Initialize();
            });
            return markets;
        }

        public static List<DayAheadMarket> GetDayAheadIZES(int ScenarioID = 0, bool withInitialization = true)
        {
            var markets = GetBasicMarkets<DayAheadMarket, DayAheadAttributes>(ScenarioID)
                .ToList();
            markets.ForEach(market =>
            {
                if (withInitialization) market.Initialize();
            });
            return markets;
        }


        public static List<IntradayMarket> GetIntradayIZES(int ScenarioID = 0, List<int> marketIdsAlreadyInitialized = null, bool withInitialization = true)
        {
            var markets = GetBasicMarkets<IntradayMarket, IntradayAttributes>(ScenarioID)
                .ToList();
            if (marketIdsAlreadyInitialized != null) markets = markets.Where(item => !marketIdsAlreadyInitialized.Contains(item.Id)).ToList();
            markets.ForEach(market =>
            {
                if (withInitialization) market.Initialize();
            });
            return markets;
        }
        public static List<ControlReserve_Capacity> GetControlReserveCapacity(int ScenarioID = 0, bool withInitialization = true)
        {
            var markets = GetBasicMarkets<ControlReserve_Capacity, MarketAttributes>(ScenarioID)
                .Include(el => el.EnergyMarket)
                .Include(el => el.EnergyMarket.Activation)
                .Include(el => el.EnergyMarket.Intraday)
                .ToList();
            markets.ForEach(market =>
            {
                if (withInitialization) market.Initialize();
                if (withInitialization) market.EnergyMarket.Initialize();
                if (withInitialization) market.EnergyMarket.Intraday.Initialize();
                if (withInitialization) market.EnergyMarket.Activation.Initialize();
            });

            return markets;
        }

        public static List<ControlReserve_Energy> GetControlReserveEnergy(int ScenarioID = 0, bool withInitialization = true)
        {
            var markets = GetBasicMarkets<ControlReserve_Energy, MarketAttributes>(ScenarioID)
                .Include(el => el.Intraday)
                .Include(el => el.Activation)
                .ToList();
            markets.ForEach(market =>
            {
                if (withInitialization) market.Initialize();
                if (withInitialization) market.Intraday.Initialize();
                if (withInitialization) market.Activation.Initialize();
            });

            return markets;
        }

        public static List<Market> GetControlReserveMarketsAndIntradays(int ScenarioID = 0, bool withIntitialization = true)
        {
            List<Market> ControlReserveMarkets = new List<Market>();
            var ControlReserveCapcities = GetControlReserveCapacity(ScenarioID, withIntitialization);
            var ControlReserveEnergies = ControlReserveCapcities.Select(item => item.EnergyMarket).ToList();
            var Intradays = ControlReserveEnergies.Select(item => item.Intraday).ToList();
            ControlReserveMarkets.AddRange(ControlReserveCapcities);
            ControlReserveMarkets.AddRange(ControlReserveEnergies);
            ControlReserveMarkets.AddRange(Intradays);
            return ControlReserveMarkets;
        }

        public static void Add(SimObject marketObject)
        {
                _context.Add(marketObject);
                _context.SaveChanges();
        }

        private static void DeleteID(string tableName, int Id)
        {
            string qry = string.Format($@"DELETE FROM '{tableName}' WHERE 'Id' = {Id}");
            var Command = new SQLiteCommand(qry);
            var Connection = new SQLiteConnection(Config.ConnStrings.Sim);
            Connection.Open();
            Command.Connection = Connection;
            Command.ExecuteNonQuery();
            SetTableAutoId(tableName, GetHighestId(tableName));
        }

        public static void DeleteAllFromScenarioID(int scenarioID)
        {
            DeleteScenarioID("Markets", scenarioID);
            DeleteScenarioID("ControlReserveActivation", scenarioID);
            DeleteScenarioID("ControlReserveTenderVolumeHandler", scenarioID);
            DeleteScenarioID("ControlReserveActivationVolumeHandler", scenarioID);
            DeleteScenarioID("IGCC_Calculation", scenarioID);
            DeleteScenarioID("ReBAP_Calculation", scenarioID);
            DeleteScenarioID("DeviationFromRZSaldo_Handler", scenarioID);
        }

        private static void DeleteScenarioID(string tableName, int scenarioID)
        {
            string qry = string.Format($@"DELETE FROM '{tableName}' WHERE [ScenarioID] = {scenarioID}");
            var Command = new SQLiteCommand(qry);
            var Connection = new SQLiteConnection(Config.ConnStrings.Sim);
            Connection.Open();
            Command.Connection = Connection;
            Command.ExecuteNonQuery();
            _context.SaveChanges();
            SetTableAutoId(tableName, GetHighestId(tableName));
            _context.SaveChanges();
        }

        private static void SetTableAutoId(string tableName, int id)
        {
            string qry = string.Format($@"UPDATE sqlite_sequence SET seq = {id} WHERE name = '{tableName}'");
            var Command = new SQLiteCommand(qry);
            var Connection = new SQLiteConnection(Config.ConnStrings.Sim);
            Connection.Open();
            Command.Connection = Connection;
            Command.ExecuteNonQuery();
        }

        private static int GetHighestId(string tableName)
        {
            string qry = string.Format($@"SELECT MAX([Id]) FROM [{tableName}]");
            var Command = new SQLiteCommand(qry);
            var Connection = new SQLiteConnection(Config.ConnStrings.Sim);
            Connection.Open();
            Command.Connection = Connection;
            var Scalar = Command.ExecuteScalar();
            var HighestId = !(Scalar is DBNull) ? (int)Convert.ToInt64(Command.ExecuteScalar()) : 0;
            return HighestId;
        }

        public static void Delete(SimObject marketObject)
        {
            if (marketObject is Market)
                DeleteID("Markets", marketObject.Id);
            else if (marketObject is ControlReserve_Activation)
                DeleteID("ControlReserve_Activation", marketObject.Id);
            else if (marketObject is ControlReserveTenderVolumeHandler)
                DeleteID("ControlReserveTenderVolumeHandler", marketObject.Id);
            else if (marketObject is ControlReserveActivationVolumeHandler)
                DeleteID("ControlReserveActivationVolumeHandler", marketObject.Id);
            else if (marketObject is IGCC_Calculation)
                DeleteID("IGCC_Calculation", marketObject.Id);
            else if (marketObject is ReBAP_Calculation)
                DeleteID("ReBAP_Calculation", marketObject.Id);
            else if (marketObject is DeviationFromRZSaldo_Handler)
                DeleteID("DeviationFromRZSaldo_Handler", marketObject.Id);
        }

        public static SimObject GetIntitializedVersionOf(SimObject marketObject)
        {
            if (marketObject is FuturesMarket)
                return GetFuturesIZES(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is DayAheadMarket)
                return GetDayAheadIZES(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is IntradayMarket)
                return GetIntradayIZES(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is ControlReserve_Capacity)
                return GetControlReserveCapacity(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is ControlReserve_Energy)
                return GetControlReserveEnergy(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is ControlReserve_Activation)
                return GetControlReserveActivation(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is ControlReserveTenderVolumeHandler)
                return GetControlReserveTenderVolumeHandlers(marketObject.ScenarioID, withMarkets: true).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is ControlReserveActivationVolumeHandler)
                return GetControlReserveActivationVolumeHandlers(marketObject.ScenarioID, withActivations: true).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is IGCC_Calculation)
                return GetIGCCs(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is ReBAP_Calculation)
                return GetReBAP_Calculations(marketObject.ScenarioID, includeIGCC: true).SingleOrDefault(item => item.Id == marketObject.Id);
            else if (marketObject is DeviationFromRZSaldo_Handler)
                return GetDeviationFromRZSaldoHandlers(marketObject.ScenarioID).SingleOrDefault(item => item.Id == marketObject.Id);
            else
                return null;
        }
    }
}