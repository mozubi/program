﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Contexts;

namespace winMoz.Data.Access.Repositories
{
    public class Thermal_KWK_Nuts2_Repository : Repository<Thermal_KWK_Nuts2_Dto>
    {
        public Thermal_KWK_Nuts2_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public List<Thermal_KWK_Nuts2_Dto> GetAllForYear(int year)
            => this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).ToList().GroupBy(nuts2 => (nuts2.Energietraeger,
                nuts2.Inbetriebnahmejahrfuenft,
                nuts2.Stilllegungsjahrfuenft,
                nuts2.Nuts2_code,
                nuts2.IstIndustrie,
                nuts2.IstGUD), (group, elements) => new Thermal_KWK_Nuts2_Dto(group.Energietraeger, group.Inbetriebnahmejahrfuenft, group.Stilllegungsjahrfuenft, elements.Select(item => item.Nettonennleistung).Sum(), group.IstIndustrie, elements.Select(item => item.KWK_ThermischeNutzleistung).Sum(), elements.Select(item => item.KWK_ElektrischeNutzleistung).Sum(), group.Nuts2_code, group.IstGUD, true, group.Inbetriebnahmejahrfuenft, group.Stilllegungsjahrfuenft)).ToList();
    }

    public class Thermal_KWK_All_Repository : Repository<Thermal_KWK_All_Dto>
    {
        public Thermal_KWK_All_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public List<Thermal_KWK_All_Dto> GetAllNamedForYear(int year, List<int> mastrIds)
            => this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).ToList().Where(item => mastrIds.Contains(item.MastrID)).ToList();
    }

    public class Thermal_Kond_DE_Repository : Repository<Thermal_Kond_DE_Dto>
    {
        public Thermal_Kond_DE_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public List<Thermal_Kond_DE_Dto> GetAllForYear(int year)
            => this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).ToList().GroupBy(nuts2 => (nuts2.Energietraeger,
                nuts2.Inbetriebnahmejahrfuenft,
                nuts2.Stilllegungsjahrfuenft,
                nuts2.IstIndustrie,
                nuts2.IstGUD), (group, elements) => new Thermal_Kond_DE_Dto(group.Energietraeger, group.Inbetriebnahmejahrfuenft, group.Stilllegungsjahrfuenft, elements.Select(item => item.Nettonennleistung).Sum(), group.IstIndustrie, group.IstGUD, false, group.Inbetriebnahmejahrfuenft, group.Stilllegungsjahrfuenft)).ToList();
    }

    public class Thermal_Kond_All_Repository : Repository<Thermal_Kond_All_Dto>
    {
        public Thermal_Kond_All_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public List<Thermal_Kond_All_Dto> GetAllNamedForYear(int year, List<int> mastrIds)
            => this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).ToList().Where(item => mastrIds.Contains(item.MastrID)).ToList();
    }

    public class WindOnshore_Nuts2_Repository : Repository<WindOnshore_Nuts2_Dto>
    {
        public WindOnshore_Nuts2_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public List<(string Nuts2, float PowerKW)> GetPowerForYear(int year)
            => this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).ToList().GroupBy(item => item.Nuts2_code, (group, values) => ( group, values.Select(val => val.Nettonennleistung).Sum())).ToList();
    }

    public class Hydro_RunOfRiver_DE_Repository : Repository<Hydro_RunOfRiver_DE_Dto>
    {
        public Hydro_RunOfRiver_DE_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public List<(bool IstEEG, float PowerKW)> GetPowerForYear(int year)
        {
            var dtos = this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).ToList();
            return dtos.GroupBy(dto => dto.IstEEG, (istEEG, list) => (istEEG, list.Select(item => item.Nettonennleistung).Sum())).ToList();
        }   
    }

        public class PV_DE_Repository : Repository<PV_DE_Dto>
    {
        public PV_DE_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public float GetPowerForYear(int year)
        {
            return this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).Select(item => item.Nettonennleistung).Sum();
        }
    }

    public class Biomass_DE_Repository : Repository<Biomass_DE_Dto>
    {
        public Biomass_DE_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public float GetPowerForYear(int year)
        {
            return this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year).Select(item => item.Nettonennleistung).Sum();
        }
    }

    public class Storage_DE_Repository : Repository<Storage_DE_Dto>
    {
        public Storage_DE_Repository() : base(new AssetsFromMaStR_Context())
        {

        }

        public float GetPowerForYear(string technology, int year)
        {
            return this.Get().Where(item => item.Inbetriebnahmejahr <= year && item.Stilllegungsjahr >= year && item.Technologie == technology).Select(item => item.Nettonennleistung).Sum();
        }
    }
}
