﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using winMoz.Data.Access.Contexts;
using winMoz.Assets;
using winMoz.Agents;
using winMoz.Assets.Plants;
using winMoz.Assets.Loads;
using winMoz.Assets;
using Microsoft.EntityFrameworkCore;
using EFCore.BulkExtensions;
using System.Data.Entity.Infrastructure;

namespace winMoz.Data.Access.Repositories
{
    /// <summary>
    /// MarketResultsRepository
    /// </summary>
    public class AssetRepository: IDisposable
    {
        SimContext _context;
        // TODO: Implement a caching system that saves cache to db when repo is disposed or "save" is called

        public AssetRepository(SimContext context) { _context = context; }


        private IQueryable<TAsset> GetBasicAsset<TAsset>(BalancingGroup balancingGroup, int ScenarioID) where TAsset: class, IAsset
        {
            return _context.Set<TAsset>()
                .Where(asset => ScenarioID == asset.ScenarioID)
                .Where(asset => balancingGroup == null ? (asset.BalancingGroupId > 0): asset.BalancingGroupId == balancingGroup.Id)
                .Include(el => el.Economics)
                .Include(el => el.Location);
        }
        public List<Plant_Thermal> GetThermal(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var plants =  GetBasicAsset<Plant_Thermal>(balancingGroup,ScenarioID)
                .Include(el => el.Units)
                .ThenInclude(unit => unit.AvailabiltySchedule)
                .Include(el => el.Units)
                .ThenInclude(unit => unit.ThermalData)
                .ToList();
            if (balancingGroup != null) plants.ForEach(asset => asset.SetBalancingGroup(balancingGroup));
            plants.ForEach(asset => {
                asset.SetTLP();
                foreach (Block unit in asset.Units)
                {
                    unit.SetBalancingGroup((BalancingGroup)asset.BalancingGroup);
                    if (unit.EfficiencyAtPnom == null || unit.EfficiencyAtPnom == 0)
                    {
                        unit.EfficiencyAtPnom = unit.ThermalData.Efficiency.AtPnom;
                        unit.ThermalData.Efficiency.SetCurve();
                    }
                    else
                    {
                        unit.ThermalData.Efficiency.SetEfficiencyAtPnom((float)unit.EfficiencyAtPnom);
                    }
                }
                });
            return plants;
        }
        public List<Plant_PV> GetPV(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var plants = GetBasicAsset<Plant_PV>(balancingGroup, ScenarioID)
                .ToList();
            if (balancingGroup != null) plants.ForEach(asset => asset.SetBalancingGroup(balancingGroup));
            return plants;
        }

        public List<Plant_Hydro> GetHydro(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var plants = GetBasicAsset<Plant_Hydro>(balancingGroup, ScenarioID)
                .ToList();
            if (balancingGroup != null) plants.ForEach(asset => asset.SetBalancingGroup(balancingGroup));
            return plants;
        }

        public List<Plant_Biomass> GetBiomass(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var plants = GetBasicAsset<Plant_Biomass>(balancingGroup, ScenarioID)
                .ToList();
            if (balancingGroup != null) plants.ForEach(asset => asset.SetBalancingGroup(balancingGroup));
            return plants;
        }
        public List<Plant_Storage> GetStorage(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var plants = GetBasicAsset<Plant_Storage>(balancingGroup, ScenarioID)
                .ToList();
            if (balancingGroup != null) plants.ForEach(asset => asset.SetBalancingGroup(balancingGroup));
            return plants;
        }
        public List<Load_SLP> GetLoad_SLP(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var loads = GetBasicAsset<Load_SLP>(balancingGroup, ScenarioID)
                .ToList();
            if (balancingGroup != null) loads.ForEach(asset => asset.SetBalancingGroup(balancingGroup));
            return loads;
        }

        public List<Load_Grid> GetLoad_Grid(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var loads = GetBasicAsset<Load_Grid>(balancingGroup, ScenarioID)
                .ToList();
            if (balancingGroup != null) loads.ForEach(asset => asset.SetBalancingGroup(balancingGroup));
            return loads;
        }

        public List<Plant_Wind> GetWind(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            var plants = GetBasicAsset<Plant_Wind>(balancingGroup, ScenarioID)
                .ToList();
            if (plants.Count() > 0)
            {
                if (balancingGroup != null) plants.ForEach(item => item.SetBalancingGroup(balancingGroup));

                var plantsNonAggregated = plants.Where(item => !item.IsAggregated).ToList();
                var plantsAggregated = plants.Where(item => item.IsAggregated).ToList();

                var modelIdsNonAggregated = plantsNonAggregated.Select(item => item.ModelId).GroupBy(item => item, (el1, el2) => el1).ToList();
                var modelIdsAggregated = plantsAggregated.Select(item => item.ModelId).GroupBy(item => item, (el1, el2) => el1).ToList();
                using (WindDtoRepository windRepo = new WindDtoRepository())
                {
                    var powerCurvesNonAggregated = windRepo.GetPowerAtWind(modelIdsNonAggregated);
                    var powerCurvesAggregated = windRepo.GetPowerAtWindAggregated(modelIdsAggregated);
                    plantsNonAggregated.ForEach(item => item.SetPowerCurve(powerCurvesNonAggregated[item.ModelId]));
                    plantsAggregated.ForEach(item => item.SetPowerCurve(powerCurvesAggregated[item.ModelId], isScaleIntegerated: true));
                }
            }
            return plants;
        }

        /// <summary>
        /// Funktion gibt alle Assets als Liste zurück. BG ist nur über die ID gematcht. Der BG muss also separat geladen werden, sofern keine BG angegeben ist.
        /// </summary>
        /// <returns></returns>
        public List<IAsset> GetAllAssets(BalancingGroup balancingGroup = null, int ScenarioID = 0)
        {
            List<IAsset> assets = new List<IAsset>();

            assets.AddRange(GetThermal(balancingGroup, ScenarioID));
            var units = assets.SelectMany(el => (el as Plant_Thermal).Units).ToList();
            assets.AddRange(units);
            assets.AddRange(GetPV(balancingGroup, ScenarioID));
            assets.AddRange(GetWind(balancingGroup, ScenarioID));
            //assets.AddRange(GetWindAggregate(balancingGroup, ScenarioID));
            assets.AddRange(GetStorage(balancingGroup, ScenarioID));
            assets.AddRange(GetLoad_SLP(balancingGroup, ScenarioID));
            assets.AddRange(GetLoad_Grid(balancingGroup, ScenarioID));
            assets.AddRange(GetBiomass(balancingGroup, ScenarioID));
            assets.AddRange(GetHydro(balancingGroup, ScenarioID));

            return assets;
        }
        /// <summary>
        /// Funktion gibt alle TSO zurück und erstellt gleichzeitig DSOs und BG. Assets werden nicht mitgeladen.
        /// </summary>
        /// <returns></returns>
        public List<TSO> GetTSO(int ScenarioID = 0)
        {
            return _context.Set<TSO>()
                .Where(tso => ScenarioID == tso.ScenarioID)
                .Include(tso => tso.dsos)
                .ThenInclude(dsos => dsos._BalancingGroups)
                .ToList();
        }

        public void DeleteTSO(IEnumerable<TSO> tsos)
        {
            _context.RemoveRange(tsos);
            _context.SaveChanges();
        }

        public void AddRange(IEnumerable<IAsset> assets)
        {
            List<Type> types = assets.Select(el => el.GetType()).Distinct().ToList();
            foreach (Type type in types)
            {
                List<IAsset> assetsType = assets.Where(asset => asset.GetType() == type).ToList();
                foreach (IAsset asset in assetsType)
                {
                    // Using Add adds the whole tree meaning also location, economics etc.
                    // Changing the state for this entity only adds this entity
                    _context.Entry(asset).State = EntityState.Added;
                }
            }
            _context.SaveChanges();
               
        }

        #region IDisposable support
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ConstantsRepository()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}