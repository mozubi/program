﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using System.Data;

namespace winMoz.Data.Access.Repositories
{
    public class WindDtoRepository : IDisposable
    {

        public IEnumerable<WindDto> GetAllNonAggregated()
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlants.ToList());
        }

        public IEnumerable<WindAggDto> GetAllAggregated()
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlantsAggregate.ToList());
        }

        public IEnumerable<WindDto> GetAllNonAggregatedForTSO(string tsoName)
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlants.Where(item => item.TSO == tsoName).ToList());
        }

        public IEnumerable<WindAggDto> GetAllAggregatedForTSO(string tsoName)
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlantsAggregate.Where(item => item.TSO == tsoName).ToList());
        }

        public WindDto FirstOrDefault(Expression<Func<WindDto, bool>> expression)
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlants.FirstOrDefault(expression));
        }
        public WindDto SingleOrDefault(Expression<Func<WindDto, bool>> expression)
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlants.SingleOrDefault(expression));
        }

        public WindAggDto FirstAggregatedOrDefault(Expression<Func<WindAggDto, bool>> expression)
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlantsAggregate.FirstOrDefault(expression));
        }
        public WindAggDto SingleAggregatedOrDefault(Expression<Func<WindAggDto, bool>> expression)
        {
            using (var ctx = new WindDtoContext())
                return SetPowerCurves(ctx.WindPlantsAggregate.SingleOrDefault(expression));
        }

        public IList<float> GetPowerAtWind(int wecModelId)
        {
            var tableName = "PowerCurves";
            return GetPowerAtWindFromTable(tableName, wecModelId);
        }

        public IList<float> GetPowerAtWindAggregated(int typeID)
        {
            var tableName = "PowerCurvesAggregate";
            return GetPowerAtWindFromTable(tableName, typeID);
        }

        private IList<float> GetPowerAtWindFromTable(string tableName, int modelID)
        {
            var output = new List<float>();

            using (var ctx = new WindDtoContext())
            using (var cmd = ctx.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = $"select * from {tableName}";
                ctx.Database.OpenConnection();
                using (var rdr = cmd.ExecuteReader())
                    while (rdr.Read())
                        output.Add(rdr.GetFloat(modelID));
            }

            return output;
        }

        private Dictionary<int, IList<float>> GetPowerAtWindFromTable(string tableName, List<int> modelIds)
        {
            var ids = modelIds.GroupBy(item => item, (el1, el2) => el1).ToList();
            var output = new Dictionary<int, IList<float>>();
            foreach (int id in ids) output.Add(id, new List<float>());

            using (var ctx = new WindDtoContext())
            using (var cmd = ctx.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = $"select * from {tableName}";
                ctx.Database.OpenConnection();
                using (var rdr = cmd.ExecuteReader())
                    while (rdr.Read())
                        foreach (int id in ids) output[id].Add(rdr.GetFloat(id));
            }
            return output;
        }

        public Dictionary<int, IList<float>> GetPowerAtWind(List<int> wecModelIds)
        {
            var tableName = "PowerCurves";
            return GetPowerAtWindFromTable(tableName, wecModelIds);
        }

        public Dictionary<int, IList<float>> GetPowerAtWindAggregated(List<int> typeIds)
        {
            var tableName = "PowerCurvesAggregate";
            return GetPowerAtWindFromTable(tableName, typeIds);
        }

        public Dictionary<string, IList<float>> GetPowerAtWind(List<string> wecModelIds)
        {
            var tableName = "PowerCurves";
            return GetPowerAtWindFromTable(tableName, wecModelIds);
        }

        public Dictionary<string, IList<float>> GetPowerAtWindAggregate(List<string> wecModelIds)
        {
            var tableName = "PowerCurves";
            return GetPowerAtWindFromTable(tableName, wecModelIds);
        }

        private Dictionary<string, IList<float>> GetPowerAtWindFromTable(string tableName, List<string> modelIds)
        {
            var names = modelIds.GroupBy(item => item, (el1, el2) => el1).ToList();
            var output = new Dictionary<string, IList<float>>();
            foreach (string name in names) output.Add(name, new List<float>());

            using (var ctx = new WindDtoContext())
            using (var cmd = ctx.Database.GetDbConnection().CreateCommand())
            {
                var columns = string.Join(", ", names.Select(item => "\"" + item.ToString() + "\"").ToList());
                cmd.CommandText = $"select {columns} from {tableName}";
                ctx.Database.OpenConnection();
                using (var rdr = cmd.ExecuteReader())
                    while (rdr.Read())
                        for (int i = 0; i < names.Count(); i++) output[names[i]].Add(rdr.GetFloat(i));
            }
            return output;
        }

        private WindDto SetPowerCurves(WindDto dto)
        {
            var powerCurve = GetPowerAtWind((int)dto.WECmodelId);
            dto.SetCurve(powerCurve);
            return dto;
        }

        private List<WindDto> SetPowerCurves(List<WindDto> dtos)
        {
            var powerCurve = GetPowerAtWind(dtos.Select(dto => (int)dto.WECmodelId).ToList());
            foreach (var dto in dtos) dto.SetCurve(powerCurve[(int)dto.WECmodelId]);
            return dtos;
        }

        private IEnumerable<WindDto> SetPowerCurves(IEnumerable<WindDto> listIn)
        {
            Parallel.ForEach(listIn, dto => SetPowerCurves(dto));
            return listIn;
        }

        private WindAggDto SetPowerCurves(WindAggDto dto)
        {
            var powerCurve = GetPowerAtWindAggregated((int)dto.WECmodelId);
            dto.SetCurve(powerCurve);
            return dto;
        }

        private List<WindAggDto> SetPowerCurves(List<WindAggDto> dtos)
        {
            var powerCurve = GetPowerAtWind(dtos.Select(dto => (int)dto.WECmodelId).ToList());
            foreach (var dto in dtos) dto.SetCurve(powerCurve[(int)dto.WECmodelId]);
            return dtos;
        }

        private IEnumerable<WindAggDto> SetPowerCurves(IEnumerable<WindAggDto> listIn)
        {
            Parallel.ForEach(listIn, dto => SetPowerCurves(dto));
            return listIn;
        }

        #region IDisposable implementation
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~WindDtoRepository()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
