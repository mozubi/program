﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Simulation;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access;
using winMoz.Markets.ControlReserve;

namespace winMoz.Data.Access.Repositories
{
    public class ScenarioData_Repository : Repository<Scenario>
    {
        private AssetRepository assetRepository;
        private EconomicsRepository economicsRepository;
        private LocationRepository locationRepository;
        private TynpRepository thermalDataRepository;
        private SlpRepository slpRepository;
        private MarketRepository marketRepository;

        public AssetRepository AssetRepository
        { get
            {
                if (this.assetRepository == null)
                {
                    this.assetRepository = new AssetRepository(context: (SimContext)_context);
                }
                return assetRepository;
            }
        }
        public EconomicsRepository EconomicsRepository
        {
            get
            {
                if (this.economicsRepository == null)
                {
                    this.economicsRepository = new EconomicsRepository(context: (SimContext)_context);
                }
                return economicsRepository;
            }
        }
        public LocationRepository LocationRepository
        {
            get
            {
                if (this.locationRepository == null)
                {
                    this.locationRepository = new LocationRepository(context: (SimContext)_context);
                }
                return locationRepository;
            }
        }
        public TynpRepository ThermalDataRepository
        {
            get
            {
                if (this.thermalDataRepository == null)
                {
                    this.thermalDataRepository = new TynpRepository(context: (SimContext)_context);
                }
                return thermalDataRepository;
            }
        }
        public SlpRepository SlpRepository
        {
            get
            {
                if (this.slpRepository == null)
                {
                    this.slpRepository = new SlpRepository(context: (SimContext)_context);
                }
                return slpRepository;
            }
        }
        public MarketRepository MarketRepository
        {
            get
            {
                if (this.marketRepository == null)
                {
                    this.marketRepository = new MarketRepository(context: (SimContext)_context);
                }
                return marketRepository;
            }
        }
        public ScenarioData_Repository(SimContext simContext): base(simContext)
        {

        }
        public ScenarioData_Repository() : base(new SimContext())
        {
            //DbHelper.ReCreateDb<ControlReserveDemandContext>();
            //AssetRepository = new AssetRepository(context: this._context as SimContext);
        }
        public override void Add(Scenario scenario)
        {
            base.Add(scenario);
            _context.AddRange(scenario.tsos);
            AssetRepository.AddRange(scenario.assets);
            _context.AddRange(scenario.Markets);
            _context.AddRange(scenario.controlReserveActivationVolumeHandlers);
            _context.AddRange(scenario.ControlReserveActivations);
            _context.AddRange(scenario.controlReserveTenderVolumeHandlers);
            _context.AddRange(scenario.mFRR_Attributes);
            _context.AddRange(scenario.ReBAP_Calculations);
            _context.SaveChanges();

        }
        public void Delete(int scenarioID)
        {
            var scenario = _dbSet.Find(scenarioID);
            if (scenario == null)
                return;
            int marketsID = scenario.Markets_ID;
            int assetsID = scenario.Assets_ID;
            if (!(_dbSet.Where(el => el.Total_ID != scenarioID).Select(el => el.Markets_ID).Contains(marketsID)))
            {
                var markets = MarketRepository.GetMarkets(marketsID, withIntitialization: false);
                var igcc = MarketRepository.GetIGCCs(marketsID, withInitialization: false);
                var saldohandler = MarketRepository.GetDeviationFromRZSaldoHandlers(marketsID, withInitialization: false);
                var tenderHandler = MarketRepository.GetControlReserveTenderVolumeHandlers(marketsID, withInitialization: false);
                var mFRRAttributes = ((SimContext)_context).MFRR_Attributes.Where(el => el.ScenarioID == marketsID);
                var activationHandler = MarketRepository.GetControlReserveActivationVolumeHandlers(marketsID, withInitialization: false);
                var activation = MarketRepository.GetControlReserveActivation(marketsID, withInitialization: false);

                MarketRepository.Delete(markets.OfType<ControlReserve_Capacity>());
                MarketRepository.Delete(markets.OfType<ControlReserve_Energy>());
                MarketRepository.Delete(markets);
                _context.RemoveRange(activation);
                _context.SaveChanges();
                _context.RemoveRange(activationHandler);
                _context.SaveChanges();
                _context.RemoveRange(tenderHandler);
                _context.SaveChanges();
                _context.RemoveRange(igcc);
                _context.SaveChanges();
                _context.RemoveRange(saldohandler);
                _context.SaveChanges();
                _context.RemoveRange(mFRRAttributes);
                _context.SaveChanges();
            }
            if (!(_dbSet.Where(el => el.Total_ID != scenarioID).Select(el => el.Assets_ID).Contains(assetsID)))
            {
                var tsos = AssetRepository.GetTSO(assetsID);
                AssetRepository.DeleteTSO(tsos);
            }
            _dbSet.Remove(scenario);
            _context.SaveChanges();
        }

        public Scenario GetFromID(int total_ID)
            => this.Get().FirstOrDefault(item => item.Total_ID == total_ID);

        public (DateTime Start, TimeSpan Duration) GetStartAndDurationFromID(int total_ID)
        {
            var ScenarioData = GetFromID(total_ID);
            return (ScenarioData.SimStartTime, ScenarioData.SimEndTime - ScenarioData.SimStartTime);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this._context.Dispose();
                    this.assetRepository?.Dispose();
                    this.economicsRepository?.Dispose();
                    this.locationRepository?.Dispose();
                    this.thermalDataRepository?.Dispose();
                    this.slpRepository?.Dispose();
                    this.marketRepository?.Dispose();
                }  

                disposedValue = true;
            }
        }
        #endregion
    }
}
