﻿using System;
using Microsoft.EntityFrameworkCore;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Contexts;
using System.Collections.Generic;
using System.Linq;

namespace winMoz.Data.Access.Repositories
{
    public class PlotAndOutput_Variables_Console_Repository : Repository<PlotAndOutput_Variables_Dto>
    {
        
        public PlotAndOutput_Variables_Console_Repository() : base(new PlotAndOutput_Variables_Console_Context())
        {
            //((Debug_Variables_Console_Context)_context).CacheDebugVariables();
        }

        public override IEnumerable<PlotAndOutput_Variables_Dto> Get()
        {
            if (((PlotAndOutput_Variables_Console_Context)_context).CachedPlotAndOutputVariablesConsole.Count() == 0)
                ((PlotAndOutput_Variables_Console_Context)_context).CachePlotAndOutputVariables();
            var results = ((PlotAndOutput_Variables_Console_Context)_context).CachedPlotAndOutputVariablesConsole;
            return results;
        }
    }
}
