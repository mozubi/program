﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using winMoz.Data.Access.Contexts;
using winMoz.Assets;
using winMoz.Agents;
using winMoz.Assets.Plants;
using winMoz.Assets.Loads;
using Microsoft.EntityFrameworkCore;
using EFCore.BulkExtensions;
using winMoz.Assets.Schedules;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories
{
    /// <summary>
    /// MarketResultsRepository
    /// </summary>
    public static class SumScheduleRepository
    {
        static IRepository<SumScheduleItem> repo = new DataBaseCachingDecorator<SumScheduleItem>(new Repository<SumScheduleItem>(new SumScheduleContext()), 10000000, saveToDb: Simulator.SaveStates.AreSumSchedulesSaved);

        #region Schedule
        public static void Add(IEnumerable<SumScheduleItem> results) => repo.Add(results);

        public static IEnumerable<SumScheduleItem> GetSchedules(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            var results = new SumScheduleContext().SumSchedules.Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList(); //.ToList().Where(item => item.TimeStamp.Ticks >= start.Ticks && item.TimeStamp.Ticks < EndOfDuration.Ticks).ToList();
            return results;
        }

        public static void CacheToDB() => repo.SaveChanges();

        #endregion
    }
}