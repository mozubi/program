﻿using System;
using Microsoft.EntityFrameworkCore;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Contexts;
using System.Collections.Generic;
using System.Linq;

namespace winMoz.Data.Access.Repositories
{
    public class PlotAndOutput_Variables_Plot_Repository : Repository<PlotAndOutput_Variables_Dto>
    {
        public PlotAndOutput_Variables_Plot_Repository() : base(new PlotAndOutput_Variables_Plot_Context())
        {
            ((PlotAndOutput_Variables_Plot_Context)_context).CachePlotAndOutputVariables();
        }

        public override IEnumerable<PlotAndOutput_Variables_Dto> Get()
            => ((PlotAndOutput_Variables_Plot_Context)_context).CachedPlotAndOutputVariablesPlot;
    }
}
