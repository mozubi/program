﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using winMoz.Data.Access.Contexts;
using winMoz.Assets;
using winMoz.Agents;
using winMoz.Assets.Plants;
using winMoz.Assets.Loads;
using winMoz.Assets;
using Microsoft.EntityFrameworkCore;
using EFCore.BulkExtensions;
using winMoz.Assets.Schedules;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories
{
    /// <summary>
    /// MarketResultsRepository
    /// </summary>
    public class ScheduleRepository: IDisposable
    {
        IRepository<ScheduleItem>  repo;
        // TODO: Implement a caching system that saves cache to db when repo is disposed or "save" is called
        public ScheduleRepository() 
        {
            repo = new DataBaseCachingDecorator<ScheduleItem>(new Repository<ScheduleItem>(new ScheduleContext()), 10000, saveToDb: Simulator.SaveStates.AreSchedulesSaved);
        }

        public void AddScheduleItemsOfAssetsUntil(IEnumerable<IAsset> assets, DateTime timeUntil)
        {
            if (Simulator.SaveStates.AreSchedulesSaved)
            {
                BulkConfig config = new BulkConfig() { SetOutputIdentity = true, PreserveInsertOrder = false };

                assets.ToList().ForEach(asset => asset.Schedule.Where(item => item.TimeStamp < timeUntil).ToList().ForEach(scheduleItem => { scheduleItem.SetAssetId(asset.Id); scheduleItem.SetType(asset.GetType().Name); }));

                var scheduleItems = assets.SelectMany(asset => asset.Schedule.Where(item => item.TimeStamp < timeUntil).ToList()).ToList();

                //List<ScheduleItem> NaNScheduleItems = scheduleItems.Where(item => item.GetType().GetProperties().Where(item2 => item2.PropertyType.Name == "float").Any(item2 => float.IsNaN((float)item2.GetValue(item)))).ToList();

                // insert tsos and update dsos
                this.repo.Add(scheduleItems);
            }
            assets.ToList().ForEach(asset => asset.CutScheduleUntil(timeUntil));
            var plantsThermal = assets.Where(item => item.IsThermal).Select(asset => (Plant_Thermal)asset).ToList();
            if (Simulator.SaveStates.AreSchedulesSaved)
            {
                plantsThermal.ForEach(asset => ((Plant_Thermal)asset).Units.ToList().ForEach(unit => unit.Schedule.ToList().ToList().ForEach(scheduleItem => { scheduleItem.SetAssetId(asset.Id); scheduleItem.SetType(asset.GetType().Name); scheduleItem.SetBlockId(unit.Id); })));

                var scheduleItemsUnits = assets.SelectMany(asset => asset.Schedule.Where(item => item.TimeStamp < timeUntil).ToList()).ToList();
                // insert tsos and update dsos
                this.repo.Add(scheduleItemsUnits);
            }
            plantsThermal.SelectMany(item => item.Units).ToList().ForEach(unit => unit.CutScheduleUntil(timeUntil));
        }

        public void CacheToDB()
        {
            this.repo.SaveChanges();
        }

        #region IDisposable support
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ConstantsRepository()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}