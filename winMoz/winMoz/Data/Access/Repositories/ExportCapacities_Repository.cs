﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.Elements;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;

namespace winMoz.Data.Access.Repositories
{
    public class ExportCapacities_Repository 
    {
        MCC_Context Context;
        int ExportCapacityID { get; set; }
        int CFunctionsID { get; set; }
        public ExportCapacities_Repository(int exportCapacityID, int cFunctionsID)
        {
            Context = new MCC_Context();
            //Context2 = new MCC_Context2();
            ExportCapacityID = exportCapacityID;
            CFunctionsID = cFunctionsID;
            try
            {
                Context.Export_Capacity_static.ToList();
                Context.C_Functions_Complete.ToList();
                //Context2.C_Functions_Complete.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table ExportCapacities in market coupling database could'nt be loaded");
            }
        }

        public List<(MarketAreaEnum Country_From, MarketAreaEnum Country_To, decimal Capacity, decimal Losses)> GetExportCapacities() 
            => Context.Export_Capacity_static.Where(item => item.ImExFixID == ExportCapacityID).ToList().Select(item => (item.ShortCode_From, item.ShortCode_To, (decimal)item.Capacity, 0m)).ToList();

        public List<(MarketAreaEnum Country_From, MarketAreaEnum Country_To, decimal Capacity, decimal wind_Prog_From)> GetExportCapacitiesCFunctions()
            => Context.C_Functions_Complete.Where(item => item.C_FuncID == CFunctionsID).ToList().Select(item => (item.ShortCode_From, item.ShortCode_To, (decimal)item.CapacityRel,(decimal)item.Wind_Prog_From)).ToList();

    }
}
