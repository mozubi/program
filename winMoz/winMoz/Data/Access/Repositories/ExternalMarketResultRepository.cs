﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Results;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories
{
    public static class ExternalMarketResultRepository //: IExternalMarketResultRepository
    {
        static IRepository<ExternalMarketResult> repo = new DataBaseCachingDecorator<ExternalMarketResult>(new Repository<ExternalMarketResult>(new ExternalMarketResultsContext()), 5000, saveToDb: Simulator.SaveStates.AreExternalMarketResultsSaved);
        //public ExternalMarketResultRepository()
        //{
        //}

        public static void Add(IEnumerable<ExternalMarketResult> externalMarketResults) => repo.Add(externalMarketResults);

        public static void CacheToDB() => repo.SaveChanges();

        public static List<ExternalMarketResult> Get(DateTime time) => repo.Get().Where(el => el.TimeStamp == time).ToList();

        public static List<ExternalMarketResult> Get(DateTime time, TimeSpan duration)
        {
            DateTime EndOfDuration = time.Add(duration);
            return repo.Get().Where(el => el.TimeStamp >= time && el.TimeStamp < EndOfDuration).ToList();
        }

    }
}
