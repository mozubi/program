﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories;

namespace winMoz.Data.Access.Repositories
{
    public class APMaxHistoricRepository : Repository<APMaxData>
    {
        public APMaxHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.APMax)).ToList();
        }
    }

    public class DAPricesHistoricRepository : Repository<DayAheadData>
    {
        public DAPricesHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Price)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Volume)).ToList();
        }

        public List<(DateTime TimeStamp, float Price, float Volume)> GetPricesAndVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Price, item.Volume)).ToList();
        }
    }

    public class IDPricesHistoricRepository : Repository<IntradayData>
    {
        public IDPricesHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Price)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Volume)).ToList();
        }

        public List<(DateTime TimeStamp, float Price, float Volume)> GetPricesAndVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Price, item.Volume)).ToList();
        }
    }

    public class MRLHistoricRepository : Repository<MRLData>
    {
        public MRLHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetPositiveCapacityPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Leistungspreis_Pos.HasValue ? item.Leistungspreis_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Price)> GetNegativeCapacityPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Leistungspreis_Neg.HasValue ? item.Leistungspreis_Neg.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetPositiveCapacityVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.VorgehMenge_Pos.HasValue ? item.VorgehMenge_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetNegativeCapacityVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.VorgehMenge_Neg.HasValue ? item.VorgehMenge_Neg.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Price)> GetPositiveActivationPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Arbeitspreis_Pos.HasValue ? item.Arbeitspreis_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Price)> GetNegativeActivationPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Arbeitspreis_Neg.HasValue ? item.Arbeitspreis_Neg.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetPositiveActivationVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.AbgerMenge_Pos.HasValue ? item.AbgerMenge_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetNegativeActivationVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.AbgerMenge_Neg.HasValue ? item.AbgerMenge_Neg.Value : 0)).ToList();
        }

    }

    public class NRVHistoricRepository : Repository<NRVData>
    {
        public NRVHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Balance)> GetBalances(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Saldo)).ToList();
        }
    }

    public class PVGenerationHistoricRepository : Repository<Generation>
    {
        public PVGenerationHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetGeneration(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Solar.HasValue ? item.Solar.Value : 0)).ToList();
        }
    }

    public class ReBAPHistoricRepository : Repository<RebapData>
    {
        public ReBAPHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Price)).ToList();
        }
    }

    public class SRLHistoricRepository : Repository<SRLData>
    {
        public SRLHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetPositiveCapacityPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Leistungspreis_Pos.HasValue ? item.Leistungspreis_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Price)> GetNegativeCapacityPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Leistungspreis_Neg.HasValue ? item.Leistungspreis_Neg.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetPositiveCapacityVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.VorgehMenge_Pos.HasValue ? item.VorgehMenge_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetNegativeCapacityVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.VorgehMenge_Neg.HasValue ? item.VorgehMenge_Neg.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Price)> GetPositiveActivationPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Arbeitspreis_Pos.HasValue ? item.Arbeitspreis_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Price)> GetNegativeActivationPrices(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Arbeitspreis_Neg.HasValue ? item.Arbeitspreis_Neg.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetPositiveActivationVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.AbgerMenge_Pos.HasValue ? item.AbgerMenge_Pos.Value : 0)).ToList();
        }

        public List<(DateTime TimeStamp, float Volume)> GetNegativeActivationVolumes(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.AbgerMenge_Neg.HasValue ? item.AbgerMenge_Neg.Value : 0)).ToList();
        }

    }

    public class WindGenerationHistoricRepository : Repository<Generation>
    {
        public WindGenerationHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetGeneration(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, (item.Wind_Onshore.HasValue ? item.Wind_Onshore.Value : 0) + (item.Wind_Offshore.HasValue ? item.Wind_Offshore.Value : 0))).ToList();
        }
    }

    public class GridLoadHistoricRepository : Repository<LoadData>
    {
        public GridLoadHistoricRepository() : base(new PredictionDataContext())
        {

        }
        public List<(DateTime TimeStamp, float Price)> GetLoad(DateTime start, TimeSpan duration)
        {
            DateTime EndOfDuration = start.Add(duration);
            return Get().Where(item => item.TimeStamp >= start && item.TimeStamp < EndOfDuration).ToList().Select(item => (item.TimeStamp, item.Volume.HasValue ? item.Volume.Value : 0)).ToList();
        }
    }
}
