﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets.Loads;
using winMoz.Data.Access.Contexts;

namespace winMoz.Data.Access.Repositories
{
    public class SlpRepository : IDisposable
    {
        SimContext context;

        public SlpRepository(SimContext context) { this.context = context; }

        #region SLP
        public IEnumerable<SLPValue> GetSLPProfileValues(SLP.Profile type) => context.SLPs.Where(el => el.Profile == type).ToList();

        public Lookup<SLP.Profile, SLPValue> GetSLP(params SLP.Profile[] profiles)
        {
            var slp = new List<(SLP.Profile, SLPValue)>();
            
            foreach (var profile in profiles)
                foreach (var value in GetSLPProfileValues(profile))
                    slp.Add((value.Profile, value));

            return (Lookup<SLP.Profile,SLPValue>)slp.ToLookup(el => el.Item1, it => it.Item2);
        }
        #endregion


        #region IDisposable support
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~SlpRepository()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
