﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Data.Access.Contexts;

namespace winMoz.Data.Access.Repositories
{
    public class EconomicsRepository : Repository<EconomicAttributes>
    {
        public EconomicsRepository(SimContext context) : base(context) { }

        public EconomicAttributes GetBySubType(string assetSubType) => _dbSet.AsNoTracking().ToList().FirstOrDefault(el => el.SubType.ToLowerInvariant() == assetSubType.ToLowerInvariant());
        public EconomicAttributes GetBySubType(Enum assetSubType) => this.GetBySubType(assetSubType.ToString());

        public List<EconomicAttributes> GetAll() => _dbSet.AsNoTracking().ToList();
    }
}
