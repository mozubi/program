﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Data.Access.Contexts;

namespace winMoz.Data.Access.Repositories
{
    public class LocationRepository : Repository<Location>
    {
       
        public int Count { get; private set; }

        public LocationRepository(SimContext context):base(context)
        {
            this.Count = _dbSet.Count();
        }

        internal Location GetNearest(int postcode)
        {
            var postcodes = GetPostcodes();
            var diff = postcodes.Select(el => Math.Abs(el - postcode)).ToList();
            var idxMin = diff.IndexOf(diff.Min());

            return _dbSet.ToList().ElementAt(idxMin);
        }

        internal Location GetFromNUTS2(string nuts2) => _dbSet.AsNoTracking().FirstOrDefault(el => el.NUTS2 == nuts2);

        internal Location GetFromNUTS3(string nuts3) => _dbSet.AsNoTracking().FirstOrDefault(el => el.NUTS3 == nuts3);

        internal List<Location> GetFromNUTS3s(List<string> nuts3s) => _dbSet.AsNoTracking().Where(el => nuts3s.Contains(el.NUTS3)).ToList();

        internal List<int> GetPostcodes() => _dbSet.AsNoTracking().Select(el => el.Postcode).ToList();

        internal Location GetFromPostcode(int postcode) => _dbSet.AsNoTracking().FirstOrDefault(el => el.Postcode == postcode) ?? GetNearest(postcode);

        internal List<Location> GetFromPostcodes(List<int> postcodes)
        {
            var Locations = _dbSet.AsNoTracking().Where(el => postcodes.Contains(el.Postcode)).ToList();
            var LocationPostcodes = Locations.Select(item => item.Postcode).ToList();
            var OtherPostcodes = postcodes.Where(item => !LocationPostcodes.Contains(item)).ToList();
            foreach(var postcode in OtherPostcodes) Locations.Add(GetNearest(postcode));
            return Locations;
        }

        internal Location GetRandom() => _dbSet.AsNoTracking().ToList()[new System.Random().Next(0, this.Count)];

        public List<string> GetAllNuts2() => _dbSet.AsNoTracking().Select(item => item.NUTS2).GroupBy(item => item, (el1, el2) => el1).ToList();

        internal List<Location> GetAll() => _dbSet.AsNoTracking().ToList();
    }
}
