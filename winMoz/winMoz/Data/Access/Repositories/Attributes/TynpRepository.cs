﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using winMoz.Data.Access.Contexts;
using winMoz.Assets;
using winMoz.Assets.Plants;
using winMoz.Information;
using System.Collections.Generic;

namespace winMoz.Data.Access.Repositories
{
    public class TynpRepository : Repository<Tynp>
    {
        /// <summary>
        /// Constructor uses default <see cref="TynpDtoContext"/>
        /// </summary>
        public TynpRepository() : base(new SimContext()) { }

        public TynpRepository(SimContext context) : base(context) { }

        public Tynp GetByCommodityAndAge(Commodities.Commodity commodity, Plant_Thermal.AgeTypes age)
        {
            //only commodity.Equals(el.Commodity) works, not the other way around
            Tynp data = _dbSet.AsNoTracking().ToList().Where(el => (commodity.Equals(el.Commodity)) && (age.Equals(el.AgeType))).FirstOrDefault();
            if (data == null)
            {
                winMoz.Simulation.Log.Warning("Commodity and Age Combination not found. Use first AgeType.");
                data = _dbSet.AsNoTracking().Where(el => commodity.Equals(el.Commodity)).ToList().LastOrDefault();
                if (data == null)
                {
                    throw new Exception("Commodity and Age Combination not found.");
                }
            }
            data.Efficiency.SetCurve();
            return data;
        }

            public Tynp GetByCommodity(Information.Commodities.Commodity commodity)
        {
            return Get().Where(el => (commodity == el.Commodity)).FirstOrDefault();
        }

        public override void Add(Tynp entity) => throw new NotImplementedException();

        public override void Update(Tynp entity) => throw new NotImplementedException();

        public override void Delete(Tynp entity) => throw new NotImplementedException();

        public override void SaveChanges() => throw new NotImplementedException();
    }
}
