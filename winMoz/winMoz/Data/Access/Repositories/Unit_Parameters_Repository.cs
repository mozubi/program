﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Markets.Elements;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories
{
    public class Unit_Parameters_Repository 
    {
        MCC_Context Context = new MCC_Context();
        Int64 GenParaID { get; set; }
        Int64 CapaID { get; set; }
        MarketAreaEnum MA_Code { get; set; }
        Int64 CapacitiesYear { get; set; }
        Int64 TYNDP_Forecast_Year { get; set; }
        Int64 TYNDP_Year { get; set; }
        EnumCollection_EOMMarkets.TYNDP_Scenario_Type TYNDP_Scenario { get; set; }
        Int64 TYNDP_Climate_Year { get; set; }

        public Unit_Parameters_Repository(Int64 genParaID, Int64 capaID, MarketAreaEnum ma_Code, Int64 tyndp_Forecast_Year, Int64 tyndp_Year, EnumCollection_EOMMarkets.TYNDP_Scenario_Type tyndp_Scenario, Int64 tyndp_Climate_Year)
        {
            GenParaID = genParaID;
            CapaID = capaID;
            MA_Code = ma_Code;
            TYNDP_Forecast_Year = tyndp_Forecast_Year;
            TYNDP_Year = tyndp_Year;
            TYNDP_Scenario = tyndp_Scenario;
            TYNDP_Climate_Year = tyndp_Climate_Year;
            Context = new MCC_Context();
            try
            {
                Context.Generation_Parameter_Thermal.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table Generation_Parameter_Thermal in market coupling database could'nt be loaded");
            }
            try
            {
                Context.Generation_Parameter_Constant.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table Generation_Parameter_Constant in market coupling database could'nt be loaded");
            }
            try
            {
                Context.Capacities.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table Capacities in market coupling database could'nt be loaded");
            }
            try
            {
                Context.VBH_VRE.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table VBH_VRE in market coupling database could'nt be loaded");
            }
        }

        public GenPara_Thermal_Dto GetThermalUnitParameters(EnumCollection_EOMMarkets.ThermalEnum generationType)
            => Context.Generation_Parameter_Thermal.Find((Int64) GenParaID, generationType);

        public float GetConstantUnitVBH(EnumCollection_EOMMarkets.ConstantEnum generationType)
            => Context.Generation_Parameter_Constant.Find((Int64)GenParaID, generationType).vbh;

        public Capacities_Dto GetCapacities()
        {
            var Capacities = Context.Capacities.Find(CapaID, MA_Code);
            if (Capacities.GetType().GetProperties().Where(prop => prop.PropertyType.Name == "float" && prop.Name != "ProductionType").All(value => (float)value.GetValue(Capacities) == -1))
                Capacities = Context.Capacities.Find(GetNextCapaID(CapaID), MA_Code);
            return Capacities;
        }

        public Int64 GetNextCapaID(Int64 CapaID) => CapaID - 100;

        public float GetVBH_VRE_TYNDP(EnumCollection_EOMMarkets.VREType vreType)
            => Context.VBH_VRE.Find(MA_Code, TYNDP_Forecast_Year, TYNDP_Year, TYNDP_Scenario, TYNDP_Climate_Year, vreType).VBH_WithSimultaneityFactor_h;
    }
}
