﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Helper;
using winMoz.Helper;
using winMoz.Data.Access.Contexts;
using winMoz.Information;
using winMoz.Markets.Elements;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories
{
    public class MCC_Load_Repository 
    {
        /// <summary>
        /// Generiert die Nachfrage anhand der Regeressionsgleichung
        /// </summary>
        //Coeffizienten für Regressionsgleichung
        private float[] QuarterCoeff = new float[4];
        private float[] DayCoeff = new float[7];
        private float[] HourCoeff = new float[24];
        private float Intercept;
        private float TempCoeff;
        private float MeanLoad;
        private MarketAreaEnum Country;
        public DayValues DayValues { get; set; }

        private MCC_Context Context;

        public MCC_Load_Repository(Int64 loadScaleID, MarketAreaEnum country, Int64 loadValID, Int64 loadYear)
        {
            Country = country;
            Context = new MCC_Context();
            try
            {
                Context.LoadScaleParameter.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table LoadScaleParameters in market coupling database could'nt be loaded");
            }
            try
            {
                Context.Load_Values.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table Load_Values in market coupling database could'nt be loaded");
            }
            DayValues = new DayValues();
            InitParameter(loadScaleID, Country, loadValID, loadYear);
        }

        private void InitParameter(Int64 loadScaleID, MarketAreaEnum Country, Int64 loadValID, Int64 loadYear)
        {
            //DataTable Coefficents = Data.Access.DataAccessOld.MCC_GetLoadScaleParameter(LoadScaleID,ShortCode);
            var LoadScale = Context.LoadScaleParameter.Find(loadScaleID, Country);
            for (int i = 0; i < 4; i++)
            {
                string Property = "wQuarter_" + i.ToString();
                QuarterCoeff[i] = (float)LoadScale.GetType().GetProperty(Property).GetValue(LoadScale);
                //QuarterCoeff[i] = Convert.Tofloat(Coefficents.Rows[0][String.Format(@"isQuarter_{0}", i)]);
            }
            for (int i = 0; i < 7; i++)
            {
                string Property = "wDay_" + i.ToString();
                DayCoeff[i] = (float)LoadScale.GetType().GetProperty(Property).GetValue(LoadScale);
                //DayCoeff[i] = Convert.Tofloat(Coefficents.Rows[0][String.Format(@"isDay_{0}", i)]);
            }
            for (int i = 0; i < 24; i++)
            {
                string Property = "wHour_" + i.ToString();
                HourCoeff[i] = (float)LoadScale.GetType().GetProperty(Property).GetValue(LoadScale);
                //HourCoeff[i] = Convert.Tofloat(Coefficents.Rows[0][String.Format(@"isHour_{0}", i)]);
            }

            var Load = Context.Load_Values.Find(loadValID, Country, loadYear);

            TempCoeff = LoadScale.Temperature;
            Intercept = LoadScale.Intercept;
            MeanLoad = LoadScale.AvgMean_hist;

            float Tmp = Load.AvgLoadMW;

            //float Tmp = Data.Access.DataAccessOld.MCC_GetAverageLoadValue(LoadValID, ShortCode, LoadYear);
            if (Tmp > MeanLoad) { MeanLoad = Tmp; }

            //Coefficents = null;
        }


        public float GetAvgLoad() => MeanLoad;

        public decimal GetMaxLoad()
            => (decimal)(MeanLoad * (Intercept + QuarterCoeff.Max() + DayCoeff.Max() + HourCoeff.Max()
                    + Math.Max(TempCoeff * 70, TempCoeff * -110)));

        public List<(DateTime timeStamp, decimal LoadValue)> GetLoadDiff(DateTime TS)
        {
            int AktQuarter = ((TS.Month + 2) / 3) - 1;
            int AktDay = DayValues.getDay(Country.ToString(), TS);
            List<(DateTime, decimal)> HourlyLoad = new List<(DateTime, decimal)>();

            var Temperaturen = Weather.GetTemperature(
                TS,
                new TimeSpan(24, 0, 0),
                Country.ToString() == "IT" || Country.ToString() == "SE" ? "DE" : Country.ToString())
                .ToList().Resample(step: Resampling.TimeStep.Hour).Select(el => el.Item2).ToList();

            DateTime timeStamp = TS;

            while (timeStamp < TS.AddDays(1))
            {
                decimal LoadValue = (decimal)(MeanLoad * (Intercept + QuarterCoeff[AktQuarter] + DayCoeff[AktDay] + HourCoeff[timeStamp.Hour]
                    + TempCoeff * Temperaturen[(int)Math.Truncate(timeStamp.Hour / 4.0)]) - MeanLoad);

                HourlyLoad.Add((timeStamp, LoadValue));

                timeStamp = timeStamp.AddHours(1);
            }

            return HourlyLoad;

        }
    }
}
