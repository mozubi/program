﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets;
using winMoz.Markets.ControlReserve;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;

namespace winMoz.Data.Access.Repositories
{
    public static class ControlReserveDemandRepository //: IControlReserveDemandRepository
    {
        #region Attributes
        #region Contexts
        /// <summary>
        /// Context zum Abruf historischer NRV-Saldi
        /// </summary>
        static IRepository<NRVData> _context_historic = new MemoryOnlyCachingDecorator<NRVData>(new Repository<NRVData>(new PredictionDataContext()), item => item.TimeStamp >= Simulator.Times.Start.AddMonths(-1) && item.TimeStamp <= Simulator.Times.End.AddMonths(1));
        /// <summary>
        /// Context zum Speichern der im Modell berechneten Regelreserve-Bedarfe, Abrufwerte und NRV-Saldi
        /// </summary>
        static class _context_simulation
        {
            static ControlReserveDemandContext _context = new ControlReserveDemandContext();
            public static IRepository<TenderVolume_Dto> tenderVolume = new DataBaseCachingDecorator<TenderVolume_Dto>(new Repository<TenderVolume_Dto>(_context), 2000, saveToDb: Simulator.SaveStates.AreControlReserveDemandSaved);
            public static IRepository<Balances_Dto> balances = new DataBaseCachingDecorator<Balances_Dto>(new Repository<Balances_Dto>(_context), 100000, saveToDb: Simulator.SaveStates.AreControlReserveDemandSaved);
            public static IRepository<ActivationVolume_Dto> activations = new DataBaseCachingDecorator<ActivationVolume_Dto>(new Repository<ActivationVolume_Dto>(_context), 2000, saveToDb: Simulator.SaveStates.AreControlReserveDemandSaved);
            public static IRepository<Limits_Dto> limits = new DataBaseCachingDecorator<Limits_Dto>(new Repository<Limits_Dto>(_context), 2000);
        }
        
        #endregion

        #region Variables for split in aFRR und mFRR
        public static MFRR_Attributes mFRR_Attributes = new ScenarioData_Repository().MarketRepository.Get_mFRRAttributes();  
        #endregion

        #endregion

        #region Save to DB
        public static void Save()
        {
            _context_simulation.tenderVolume.SaveChanges();
            _context_simulation.balances.SaveChanges();
            _context_simulation.activations.SaveChanges();
            _context_simulation.limits.SaveChanges();
        }
        #endregion

        #region Control Reserve Tender Volume

        public static bool IsTenderVolumeTimeFrameCached(DemandArgs args)
        {
            DateTime timeStamp = args.DeterminationDate;
            DateTime EndOfDuration = timeStamp.Add(args.Duration - args.StepDuration);
            var cachedResults = _context_simulation.tenderVolume.Get();
            return cachedResults.Count() == 0 ? false : cachedResults.First().TimeStamp <= timeStamp && cachedResults.Last().TimeStamp.AddMinutes(15) >= EndOfDuration;
        }

        public static List<(DateTime TimeStamp, int Demand)> GetTenderVolume(DemandArgs args)
            => _context_simulation.tenderVolume
            .Get()
            .Where(el => el.TimeStamp >= args.DeterminationDate && el.TimeStamp < args.DeterminationDate + args.Duration)
            .Where(item => item.FRR_Type == args.ControlReserveType)
            .Select(item => (item.TimeStamp, item.TenderVolume)).ToList();

        public static (DateTime TimeStamp, int Demand) GetTenderVolumeActivation(DemandArgs args)
            => _context_simulation.tenderVolume
             .Get().Where(el => el.TimeStamp >= new DateTime(args.DeterminationDate.Year, args.DeterminationDate.Month, args.DeterminationDate.Day) && el.TimeStamp < new DateTime(args.DeterminationDate.Year, args.DeterminationDate.Month, args.DeterminationDate.Day) + args.Duration).Where(item => item.FRR_Type == args.ControlReserveType).Where(item => item.TimeStamp <= args.DeterminationDate).Select(item => (item.TimeStamp, item.TenderVolume)).Last();


        public static void AddTenderVolume(DateTime time, Dictionary<ControlReserveShortCode.ShortCode, int> demands)
            => _context_simulation.tenderVolume.Add(demands.Select(item => new TenderVolume_Dto(time, item.Key, item.Value)).ToList());
        #endregion

        #region GCC-Balance or. Total Control Reserve Demand (FRR) and Synthetic Control Reserve Demand (aFRR, mFRR) (15 Minutes Average)

        //private static bool AreBalancesEmpty()
        //    => _context_simulation.AreBalancesEmpty();

        //Synthetischer Regelleistungsbedarf nach Consentec Gutachten im Auftrag der vier Übertragungsnetzbetreiber
        //"Verfahren zur dynamischen Bestimmung des Bedarfs für Sekundärregel- und Minutenreserve"
        //aus 2008 (zu finden auf regelleistung.net)

        public static void CacheHistoric(DateTime timeStamp, TimeSpan duration)
        {
            if (_context_simulation.balances.Get().Count() == 0 && mFRR_Attributes != null)
            {
                List<(DateTime TimeStamp, float Balance)> BalancesFRR = GetFromDB(timeStamp, duration);
                List<float> LastBalances = new List<float>() { BalancesFRR[BalancesFRR.Count() - 2].Balance, BalancesFRR.Last().Balance };
                (List<(DateTime TimeStamp, float Balance)> BalancesAFRR, List<(DateTime TimeStamp, float Balance)> BalancesMFRR) = Get_aFRR_and_mFRR_Balances(LastBalances, BalancesFRR);
                AddBalances(BalancesFRR, BalancesAFRR, BalancesMFRR);
            }
        }

        private static List<(DateTime, float)> GetFromDB(DateTime timeStamp, TimeSpan duration)
            => _context_historic.Get().Where(el => el.TimeStamp >= timeStamp && el.TimeStamp < timeStamp + duration).Select(item => (item.TimeStamp, item.Saldo)).ToList();

        /// <summary>
        /// Berechnung der Sekundärregelleistung aus der Gesamtregelleistung
        /// Dabei wird zunächst die Minutenreservebedarf für den Zeitraum von zwei Viertelstunden zuvor
        /// (bei Anfang der Liste jeweils den aktuelle Wert)
        /// Diese wird dann von der Sekundärregleleistung abgezogen um den Sekundärregleleistungsbedarf 
        /// der aktuellen Viertelstunde zu bestimmen.
        /// </summary>
        public static (List<(DateTime TimeStamp, float Value)> Balances_aFRR, List<(DateTime TimeStamp, float Value)> Balances_mFRR) Get_aFRR_and_mFRR_Balances(List<float> lastValues, List<(DateTime TimeStamp, float Balance)> balancesAddFrom) //LastValue -> Letzte zwei Werte für FRR
        {
            float balance, balanceHistoric, mFRR;
            var balancesOrdered = balancesAddFrom.OrderBy(item => item.TimeStamp).ToList();
            List<(DateTime, float)> balances_aFRR = new List<(DateTime, float)>();
            List<(DateTime, float)> balances_mFRR = new List<(DateTime, float)>();

            foreach ((DateTime TimeStamp, float Balance) item in balancesOrdered)
            {
                balance = item.Balance;
                balanceHistoric = lastValues.Count() > 0 ? lastValues.Take(1).FirstOrDefault() : balance; // Wenn keine Werte aus Vergangenheit bekannt, besteht völliges Wissen.
                if (lastValues.Count() > 1) lastValues.RemoveRange(0, 1);
                mFRR = mFRR_Attributes.mFRR_Factor * (balanceHistoric > mFRR_Attributes.mFRR_Threshold ? (balanceHistoric - mFRR_Attributes.mFRR_Threshold) : balanceHistoric < -mFRR_Attributes.mFRR_Threshold ? (balanceHistoric + mFRR_Attributes.mFRR_Threshold) : 0);
                balances_aFRR.Add((item.TimeStamp, balance - mFRR));
                balances_mFRR.Add((item.TimeStamp, mFRR));
                lastValues.Add(balance);
            }
            return (balances_aFRR, balances_mFRR);
        }


        public static List<(DateTime TimeStamp, float Balance)> GetBalances(DateTime determinationDate, TimeSpan duration, ControlReserveShortCode.ShortCode type = ControlReserveShortCode.ShortCode.FRR)
            => _context_simulation.balances.Get()
                .Where(el => el.TimeStamp >= determinationDate && el.TimeStamp < determinationDate + duration)
                .Where(balance => balance.FRR_Type == type)
                .AsEnumerable()
                .Select(item => (item.TimeStamp, item.Balance)).ToList();

        public static Dictionary<ControlReserveShortCode.ShortCode, Dictionary<DateTime, float>> GetBalancesHistoricAllTypes(DateTime timeUntil)
            => _context_simulation.balances.Get().Where(item => item.TimeStamp < timeUntil).ToList().GroupBy(item => item.FRR_Type, (type, group) => new { type, Balances = group.GroupBy(item => item.TimeStamp, (time, balances) => new { time, Balance = balances.First().Balance }).ToDictionary(item => item.time, item => item.Balance) }).ToDictionary(item => item.type, item => item.Balances);

        //public static bool IsBalancesTimeFrameCached(DateTime timeStamp, TimeSpan duration) => _context_simulation.IsTimeFrameCached<Balances_Dto>(timeStamp, duration);

        public static void AddBalances(List<(DateTime TimeStamp, float Balance)> balances)
        {
            if (balances.Count() > 0)
            {
                DateTime LastBalancesStart = balances.First().TimeStamp.Date.AddMinutes(-30);
                List<float> LastBalances = GetBalances(LastBalancesStart, TimeSpan.FromMinutes(30)).Select(item => item.Balance).ToList();
                (List<(DateTime TimeStamp, float Balance)> BalancesAFRR, List<(DateTime TimeStamp, float Balance)> BalancesMFRR) = Get_aFRR_and_mFRR_Balances(LastBalances, balances);
                AddBalances(balances, BalancesAFRR, BalancesMFRR);
            }
        }
        private static void AddBalances(List<(DateTime TimeStamp, float Balance)> balances_FRR, List<(DateTime TimeStamp, float Balance)> balances_aFRR, List<(DateTime TimeStamp, float Balance)> balances_mFRR)
        {
            List<Balances_Dto> Balances = new List<Balances_Dto>();
            Balances.AddRange(balances_FRR.Select(balance => new Balances_Dto(balance.TimeStamp, ControlReserveShortCode.ShortCode.FRR, balance.Balance)).ToList());
            Balances.AddRange(balances_aFRR.Select(balance => new Balances_Dto(balance.TimeStamp, ControlReserveShortCode.ShortCode.aFRR, balance.Balance)).ToList());
            Balances.AddRange(balances_mFRR.Select(balance => new Balances_Dto(balance.TimeStamp, ControlReserveShortCode.ShortCode.mFRR, balance.Balance)).ToList());
            Balances = Balances.OrderBy(balance => balance.TimeStamp).ToList();
            _context_simulation.balances.Add(Balances);
        }
        #endregion

        #region Control Reserve Activation Volume
        public static bool IsActivationVolumeTimeFrameCached(ActivationVolumeArgs args)
        {
            DateTime timeStamp = args.DeterminationDate;
            DateTime EndOfDuration = timeStamp.Add(args.Duration - args.StepDuration);
            var cachedResults = _context_simulation.activations.Get();
            return cachedResults.Count() == 0 ? false : cachedResults.First().TimeStamp <= timeStamp && cachedResults.Last().TimeStamp.AddMinutes(15) >= EndOfDuration;
        }


        public static List<(DateTime TimeStamp, int Volume)> GetActivationVolumes(ActivationVolumeArgs args)
            => _context_simulation.activations.Get()
                .Where(el => el.TimeStamp >= args.DeterminationDate && el.TimeStamp < args.DeterminationDate + args.Duration)
                .Where(item => item.FRR_Type == args.ControlReserveType).ToList().Select(item => (item.TimeStamp, item.ActivationVolume)).ToList();

        public static void AddActivationVolumes(DateTime time, Dictionary<ControlReserveShortCode.ShortCode, int> activationVolumes)
            => _context_simulation.activations.Add(activationVolumes.Select(item => new ActivationVolume_Dto(time, item.Key, item.Value)).ToList());

        #endregion

        #region ActivationVolume -> LimitsExceeded
        //public static bool IsLimitsListTimeFrameCached(DemandArgs args) => _context_simulation.IsTimeFrameCached<Balances_Dto>(args.DeterminationDate, args.Duration - args.StepDuration);


        //public static List<(DateTime TimeStamp, bool IsLimit)> GetActivationVolumeIsLimitList(DemandArgs args)
        //    => _context_simulation.Get<Limits_Dto>(args.DeterminationDate, args.Duration).Where(item => item.FRR_Type == args.ControlReserveType).ToList().Select(item => (item.TimeStamp, item.LimitExceeded)).ToList();

        public static void AddActivationVolumeIsLimitList(DateTime time, Dictionary<ControlReserveShortCode.ShortCode, bool> activationVolumeIsLimitList)
            => _context_simulation.limits.Add(activationVolumeIsLimitList.Select(item => new Limits_Dto(time, item.Key, item.Value)).ToList());

        #endregion

    }
}
