﻿using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace winMoz.Data.Access.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity>, IDisposable where TEntity : class
    {
        protected DbContext _context;
        protected DbSet<TEntity> _dbSet;

        public Repository(DbContext context)
        {
            this._context = context;
            this._dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get() => _dbSet.AsEnumerable();
       

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate) => _dbSet.FirstOrDefault(predicate);
        public virtual TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate) => _dbSet.SingleOrDefault(predicate);

        public virtual void Add(TEntity entity)
        {
            _dbSet.Add(entity);
            SaveChanges();
        }

        public virtual void Add(IEnumerable<TEntity> entities)
        {
            BulkInsert(entities);
            SaveChanges();
        }

        public virtual void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
            SaveChanges();
        }
        public virtual void Delete(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
            SaveChanges();
        }

        public virtual void Update(IEnumerable<TEntity> entities) 
        {
            _dbSet.UpdateRange(entities); 
            SaveChanges(); 
        }
        public virtual void Update(TEntity entity)
        {
            _dbSet.Update(entity);
            SaveChanges(); 
        }

        protected virtual void BulkInsert(IEnumerable<object> entities)
        {
            if (entities != null)
            {
                using (var transaction = this._context.Database.BeginTransaction())
                {
                    this._context.BulkInsert(entities.ToList(), new BulkConfig { SetOutputIdentity = true, PreserveInsertOrder = false });
                    transaction.Commit();
                }
            }
        }

        public virtual void SaveChanges() => _context.SaveChanges();

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this._context.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
