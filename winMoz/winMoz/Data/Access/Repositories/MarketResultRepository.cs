﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Markets.Bids;
using winMoz.Markets.Results;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories
{
    /// <summary>
    /// MarketResultsRepository
    /// </summary>
    public static class MarketResultRepository //: IDisposable
    {
        // TODO: Implement a caching system that saves cache to db when repo is disposed or "save" is called

        static MarketContext Context = new MarketContext();

        //private static MarketContext _context;
        public static IRepository<FuturesResult> FuturesResultsRepo = new DataBaseCachingDecorator<FuturesResult>(new Repository<FuturesResult>(Context), 10000, saveToDb: Simulator.SaveStates.AreMarketResultsSaved);
        public static IRepository<DayAheadResult> DayAheadResultsRepo = new DataBaseCachingDecorator<DayAheadResult>(new Repository<DayAheadResult>(Context), 1000, saveToDb: Simulator.SaveStates.AreMarketResultsSaved);
        public static IRepository<IntraDayResult> IntradayResultsRepo = new DataBaseCachingDecorator<IntraDayResult>(new Repository<IntraDayResult>(Context), 1000, saveToDb: Simulator.SaveStates.AreMarketResultsSaved);
        public static IRepository<ControlReserveResult> ControlReserveResultsRepo = new DataBaseCachingDecorator<ControlReserveResult>(new Repository<ControlReserveResult>(Context), 10000, saveToDb: Simulator.SaveStates.AreMarketResultsSaved);
        private static bool disposedValue;


        #region Results 
        public static void Add<TResult>(IEnumerable<TResult> results) where TResult : MarketResult
        {
            FuturesResultsRepo.Add(results.OfType<FuturesResult>());
            DayAheadResultsRepo.Add(results.OfType<DayAheadResult>());
            IntradayResultsRepo.Add(results.OfType<IntraDayResult>());
            ControlReserveResultsRepo.Add(results.OfType<ControlReserveResult>());
        }

        public static void CacheToDB()
        {
            FuturesResultsRepo.SaveChanges();
            DayAheadResultsRepo.SaveChanges();
            IntradayResultsRepo.SaveChanges();
            ControlReserveResultsRepo.SaveChanges();
        }

        public static IEnumerable<MarketResult> GetResultsFromDB(DateTime timeStamp, TimeSpan duration)
        {
            List<MarketResult> marketResults = new List<MarketResult>();
            using (var context = new MarketContext())
            {
                DateTime EndOfDuration = timeStamp.Add(duration);
                var DayAheadResults = context.DayAheadResults.Where(item => item.TimeStamp >= timeStamp && item.TimeStamp < EndOfDuration);
                if (DayAheadResults.Count() > 0) marketResults.AddRange(DayAheadResults);
                var IntradayResults = context.IntradayResults.Where(item => item.TimeStamp >= timeStamp && item.TimeStamp < EndOfDuration);
                if (IntradayResults.Count() > 0) marketResults.AddRange(IntradayResults);
                var FuturesResults = context.FuturesResults.Where(item => item.TimeStamp >= timeStamp && item.TimeStamp < EndOfDuration);
                if (FuturesResults.Count() > 0) marketResults.AddRange(FuturesResults);
                var ControlReserveResults = context.ControlReserveResults.Where(item => item.TimeStamp >= timeStamp && item.TimeStamp < EndOfDuration);
                if (ControlReserveResults.Count() > 0) marketResults.AddRange(ControlReserveResults);
            }
            return marketResults;
        }




        #endregion
    }
}