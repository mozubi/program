﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Contexts;

namespace winMoz.Data.Access.Repositories
{
    public class TYNDP2018_Scenario_Repository : Repository<TYNDP2018_Scenario_Dto>
    {
        public TYNDP2018_Scenario_Repository(TYNDP2018_Scenario_Context context) : base(context)
        {
        }

        public TYNDP2018_Scenario_Dto GetForCountry(string country)
            => this.Get().FirstOrDefault(item => item.Country == country);
    }

    public class Generation2025BEST_Repository : TYNDP2018_Scenario_Repository
    {
        public Generation2025BEST_Repository() : base(new Generation2025BEST_Context())
        {
        }
    }

    public class Generation2030DG_Repository : TYNDP2018_Scenario_Repository
    {
        public Generation2030DG_Repository() : base(new Generation2030DG_Context())
        {
        }
    }

    public class Generation2030EUCO_Repository : TYNDP2018_Scenario_Repository
    {
        public Generation2030EUCO_Repository() : base(new Generation2030EUCO_Context())
        {
        }
    }

    public class Generation2030ST_Repository : TYNDP2018_Scenario_Repository
    {
        public Generation2030ST_Repository() : base(new Generation2030ST_Context())
        {
        }
    }

    public class Generation2040DG_Repository : TYNDP2018_Scenario_Repository
    {
        public Generation2040DG_Repository() : base(new Generation2040DG_Context())
        {
        }
    }

    public class Generation2040GCA_Repository : TYNDP2018_Scenario_Repository
    {
        public Generation2040GCA_Repository() : base(new Generation2040GCA_Context())
        {
        }
    }

    public class Generation2040ST_Repository : TYNDP2018_Scenario_Repository
    {
        public Generation2040ST_Repository() : base(new Generation2040ST_Context())
        {
        }
    }

}
