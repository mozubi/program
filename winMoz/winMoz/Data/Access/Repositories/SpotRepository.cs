﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Markets.Results;

namespace winMoz.Data.Access.Repositories
{
    /// <summary>
    /// Combines MarketResults through simulation with historic data in case of overlapping time periods
    /// </summary>
    public static class SpotRepository
    {
        public static IEnumerable<(DateTime TimeStamp, float Price)> GetPrices<TMarketResult>(DateTime timeStamp, TimeSpan duration) where TMarketResult : MarketResult
        {
            var prices = new List<(DateTime TimeStamp, float Price)>();

            if (!MarketResultRepository.IsEmpty<TMarketResult>())
                prices.AddRange(MarketResultRepository.GetResults<TMarketResult>(timeStamp, duration).Where(item => !item.IsBlock).Select(el => (el.TimeStamp, el.Price)));


            if (prices.Count == CountTarget<TMarketResult>(duration)) return prices;

            var t1 = prices.FirstOrDefault().TimeStamp;
            var t2 = prices.LastOrDefault().TimeStamp;

            using (var repo = new PredictionDataRepository())
            {
                if (t1 == default | t2 == default)
                    return repo.GetPrices<TMarketResult>(timeStamp, duration);

                if (t1 > timeStamp && t2 == timeStamp + duration)
                    return repo.GetPrices<TMarketResult>(timeStamp, t1 - timeStamp).Concat(prices).ToList();

                if (t1 > timeStamp && t2 < timeStamp + duration)
                    return repo.GetPrices<TMarketResult>(timeStamp, t1 - timeStamp).Concat(prices).Concat(repo.GetPrices<TMarketResult>(t2, timeStamp + duration - t2)).ToList();
            }

            // TODO: fill missing with interpolated, bfill, ffill
            return prices;
        }

        private static int CountTarget<TMarketResult>(TimeSpan duration) where TMarketResult : MarketResult
        {
            if (typeof(TMarketResult) == typeof(DayAheadResult)) return (int)(duration.Ticks / TimeSpan.FromMinutes(60).Ticks);
            else if (typeof(TMarketResult) == typeof(IntraDayResult)) return (int)(duration.Ticks / TimeSpan.FromMinutes(15).Ticks);

            throw new NotImplementedException("Type not implemented.");
        }
    }
}
