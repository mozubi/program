﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.Bids;
using winMoz.Simulation;
using winMoz.Helper;
using EFCore.BulkExtensions;

namespace winMoz.Data.Access.Repositories
{
    public class BidRepository : Repository<Bid>
    {
        public BlockingCollection<Bid> Bids { get; private set; } = new BlockingCollection<Bid>();

        public int NumberOfDaysToLetBidsInBidList { get; private set; }

        public BidRepository(DbContext context, int numberOfDaysToLetBidsInBidList = 0) : base(context) { AppDomain.CurrentDomain.ProcessExit += (s, e) => { SaveToDb(); NumberOfDaysToLetBidsInBidList = numberOfDaysToLetBidsInBidList; };/*if (!unloaded) base.Add(Bids); unloaded = true; };*/}

        public BidRepository(int numberOfDaysToLetBidsInBidList = 0) : this(new BidContext()) { NumberOfDaysToLetBidsInBidList = numberOfDaysToLetBidsInBidList; }

        public List<Bid> GetAll()
        {
            return this._context.Set<Bid>().Include(bid => bid.Segments).ToList();
        }

        #region Interface implementations
        public override void Add(Bid entity) => this.Bids.Add(entity);

        public override void Add(IEnumerable<Bid> entities) => this.Bids.AddRange(entities);

        public void SaveToDb()
        {
            if (Simulator.Times.SimTime < Simulator.Times.End && Simulator.Times.SimTime != default)
            {
                DateTime TimeToSave = Simulator.Times.SimTime.AddDays(NumberOfDaysToLetBidsInBidList);
                try
                {
                    var BidsToSave = this.Bids.Where(item => item.TimeStamp <= TimeToSave).ToList();
                    var OtherBids = this.Bids.Where(item => item.TimeStamp > TimeToSave).ToList();
                    if (Simulator.SaveStates.AreBidsSaved)
                    {
                        base.BulkInsert(BidsToSave);
                        this.Bids.ToList().ForEach(bid => bid.Segments.ToList().ForEach(seg => seg.SetBidId(bid.BidId)));
                        var segmentsToSave = BidsToSave.ToList().SelectMany(bid => bid.Segments).ToList();
                        base.BulkInsert(segmentsToSave);
                    }
                    this.Bids = new BlockingCollection<Bid>();
                    foreach (var Bid in OtherBids) this.Bids.Add(Bid);
                }
                catch (Exception e)
                {
                    Log.Error(e.Message);
                    Log.Error("Error saving Bids to database");
                }
            }
            else
            {
                if (Simulator.Times.SimTime != default)
                {
                    DateTime TimeToSave = Simulator.Times.SimTime.AddDays(NumberOfDaysToLetBidsInBidList);
                    try
                    {
                        if (Simulator.SaveStates.AreBidsSaved)
                        {
                            base.BulkInsert(Bids);
                            this.Bids.ToList().ForEach(bid => bid.Segments.ToList().ForEach(seg => seg.SetBidId(bid.BidId)));
                            var segmentsToSave = Bids.ToList().SelectMany(bid => bid.Segments).ToList();
                            base.BulkInsert(segmentsToSave);
                        }
                        this.Bids = new BlockingCollection<Bid>();
                    }
                    catch (Exception e)
                    {
                        Log.Error(e.Message);
                        Log.Error("Error saving Bids to database");
                    }
                }
            }
        }

        #endregion
    }
}
