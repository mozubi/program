﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Contexts;
using winMoz.Markets.Elements;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Data.Access.DTO;
using System.Data.Entity;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories
{
    

    public class MCC_Scenario_Repository 
    {
        MCC_Context Context;

        public int TotalID { get; private set; }
        public MCC_Scenario_Repository(int totalID)
        {
            Context = new MCC_Context();
            try
            {
                Context.TotalScenario.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table TotalScenario in market coupling database could'nt be loaded");
            }
            try
            {
                Context.ScenarioCountries.ToList();
            }
            catch (Exception e)
            {
                Log.Error(e.GetType().ToString() + ": Table ScenarioCountries in market coupling database could'nt be loaded");
            }
            TotalID = totalID;
        }
        public Int64 GetImExFixID() => Context.TotalScenario.Find((Int64)TotalID).ImExFixID;
        public Int64 GetC_FuncID() => Context.TotalScenario.Find((Int64)TotalID).C_FuncID;
        public Int64 GetLoadScaleID() => Context.TotalScenario.Find((Int64)TotalID).LoadScaleID;
        public Int64 GetLoadValID() => Context.TotalScenario.Find((Int64)TotalID).LoadValID;
        public Int64 GetLoadValYear() => Context.TotalScenario.Find((Int64)TotalID).LoadValYearStatic;
        public Int64 GetCapaID() => Context.TotalScenario.Find((Int64)TotalID).CapaID;
        public Int64 GetGenParaID() => Context.TotalScenario.Find((Int64)TotalID).GenParaID;
        public Int64 GetTYNPD_Forecast_Year() => Context.TotalScenario.Find((Int64)TotalID).TYNDP_Forecast_Year;
        public Int64 GetTYNPD_Year() => Context.TotalScenario.Find((Int64)TotalID).TYNDP_Year;
        public EnumCollection_EOMMarkets.TYNDP_Scenario_Type GetTYNPD_Scenario() => Context.TotalScenario.Find((Int64)TotalID).TYNDP_Scenario;
        public Int64 GetCountryID() => Context.TotalScenario.Find((Int64)TotalID).CountryID;
        public Int64 GetTYNPD_Climate_Year() => Context.TotalScenario.Find((Int64)TotalID).TYNDP_Climate_Year;
        public List<MarketAreaEnum> GetScenarioCountries() => Context.ScenarioCountries.Where(prop => prop.ScenID == GetCountryID()).Select(prop => prop.ShortCode).ToList();

        public (Int64 LoadScaleID, Int64 LoadValID, Int64 LoadValYear, Int64 GenParaID, Int64 CapaID, Int64 TYNDP_Forecast_Year, Int64 TYNDP_Year, EnumCollection_EOMMarkets.TYNDP_Scenario_Type TYNDP_Scenario, Int64 TYNDP_Climate_Year) GetExternalMarketParameters() => (GetLoadScaleID(), GetLoadValID(), GetLoadValYear(), GetGenParaID(), GetCapaID(), GetTYNPD_Forecast_Year(), GetTYNPD_Year(), GetTYNPD_Scenario(), GetTYNPD_Climate_Year());
    }
}
