﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using winMoz.Helper;
using winMoz.Markets.Results;
using winMoz.Tools.Prediction.GridLoad;
using winMoz.Tools.Prediction.Prices.DayAhead;
using winMoz.Tools.Prediction.Prices.Intraday;
using winMoz.Simulation;

namespace winMoz.Data.Access.Repositories.Prediction
{
    /// <summary>
    /// Klasse zum Zugriff auf die Vorhersagedaten. Gleichzeitig 
    /// </summary>
    public class PredictionDataRepository : IDisposable
    {
        protected DbContext context;
        private IRepository<DayAheadData> _dayAheadDataRepo;
        private IRepository<IntradayData> _intradayDataRepo;
        private IRepository<LoadData> _loadDataRepo;
        private IRepository<REData> _reDataRepo;
        private IRepository<Generation> _generationRepo;

        public IRepository<DayAheadData> DayAheadDataRepo
        {
            get
            {
                if (this._dayAheadDataRepo == null)
                {
                    this._dayAheadDataRepo = new MemoryOnlyCachingDecorator<DayAheadData>(new Repository<DayAheadData>(context: context), item => item.TimeStamp.Year == Simulator.StartDataYear.Year);
                }
                return _dayAheadDataRepo;
            }
        }
        public IRepository<IntradayData> IntradayDataRepo
        {
            get
            {
                if (this._intradayDataRepo == null)
                {
                    this._intradayDataRepo = new MemoryOnlyCachingDecorator<IntradayData>(new Repository<IntradayData>(context: context), item => item.TimeStamp.Year == Simulator.StartDataYear.Year);
                }
                return _intradayDataRepo;
            }
        }
        public IRepository<LoadData> LoadDataRepo
        {
            get
            {
                if (this._loadDataRepo == null)
                {
                    this._loadDataRepo = new MemoryOnlyCachingDecorator<LoadData>(new Repository<LoadData>(context: context), item => item.TimeStamp.Year == Simulator.StartDataYear.Year);
                }
                return _loadDataRepo;
            }
        }
        public IRepository<REData> REDataRepo
        {
            get
            {
                if (this._reDataRepo == null)
                {
                    this._reDataRepo = new MemoryOnlyCachingDecorator<REData>(new Repository<REData>(context: context), item => item.TimeStamp.Year == Simulator.StartDataYear.Year);
                }
                return _reDataRepo;
            }
        }
        public IRepository<Generation> GenerationRepo
        {
            get
            {
                if (this._generationRepo == null)
                {
                    this._generationRepo = new MemoryOnlyCachingDecorator<Generation>(new Repository<Generation>(context: context), item => item.TimeStamp.Year == Simulator.StartDataYear.Year);
                }
                return _generationRepo;

            }
        }

        public PredictionDataRepository(DbContext context)
        {
            this.context = context;
        }

        public PredictionDataRepository()
        {
            context = new PredictionDataContext();
        }

        public IEnumerable<TData> Get<TData>(Expression<Func<TData, bool>> selector = default) where TData : PredictionData
        {
            IQueryable<TData> data;
            if (selector == default) data = context.Set<TData>();
            else data = context.Set<TData>().Where(selector);

            return data;
        }
        public void AddMarketResults(IEnumerable<MarketResult> results)
        {
            IEnumerable<DayAheadData> dayAheadValues = results.OfType<DayAheadResult>().Select(el => new DayAheadData(el.TimeStamp, el.Price, el.Volume));
            IEnumerable<IntradayData> intradays = results.OfType<IntraDayResult>().Select(el => new IntradayData(el.TimeStamp, el.Price, el.Volume));
            DayAheadDataRepo.Add(dayAheadValues);
            IntradayDataRepo.Add(intradays);
        }
        public void DeleteAllTrainingData(DateTime keepStart, DateTime keepEnd)
        {
            var DAdata = DayAheadDataRepo.Get().Where(el => el.TimeStamp.Date < keepStart.Date || el.TimeStamp.Date > keepEnd.Date ).ToList();
            foreach(var value in DAdata)
            {
                DayAheadDataRepo.Delete(value);
            }
            var IDdata = IntradayDataRepo.Get().Where(el => el.TimeStamp.Date < keepStart.Date || el.TimeStamp.Date > keepEnd.Date).ToList();
            foreach (var value in IDdata)
            {
                IntradayDataRepo.Delete(value);
            }
            var loadData = LoadDataRepo.Get().Where(el => el.TimeStamp.Date < keepStart.Date || el.TimeStamp.Date > keepEnd.Date).ToList();
            foreach (var value in loadData)
            {
                LoadDataRepo.Delete(value);
            }
            var reData = REDataRepo.Get().Where(el => el.TimeStamp.Date < keepStart.Date || el.TimeStamp.Date > keepEnd.Date).ToList();
            foreach (var value in reData)
            {
                REDataRepo.Delete(value);
            }
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _dayAheadDataRepo?.Dispose();
                    _loadDataRepo?.Dispose();
                    _reDataRepo?.Dispose();
                    context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~PredictionDataRepository()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
