﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;

namespace winMoz.Data.Access
{
    public class DataBaseCachingDecorator<TEntity> : IRepository<TEntity> where TEntity : class, IComparable<TEntity>
    {
        private readonly IRepository<TEntity> _decoratedRepo;
        private readonly MemoryCache _cache;
        private readonly int _numberOfElements;
        private readonly bool _saveElements;
        public DataBaseCachingDecorator(IRepository<TEntity> decoratedRepo, int numberOfElements = 100000, bool saveToDb = false)
        {
            _decoratedRepo = decoratedRepo;
            _cache = MemoryCache.Default;
            _numberOfElements = numberOfElements;
            _saveElements = saveToDb;
        }
        public IEnumerable<TEntity> Get()
        {
            SortedSet<TEntity> values = _cache.Get(typeof(TEntity).Name) as SortedSet<TEntity>;
            if (values == null)
            {
                values = new SortedSet<TEntity>(_decoratedRepo.Get().OrderBy(el => el).Take(_numberOfElements));
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.Priority = CacheItemPriority.NotRemovable;
                _cache.Set(typeof(TEntity).Name, values, cacheItemPolicy);
            }
            return values;
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Get().AsQueryable().SingleOrDefault(predicate);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Get().AsQueryable().FirstOrDefault(predicate);
        }

        public void Add(TEntity entity)
        {
            SortedSet<TEntity> values = _cache.Get(typeof(TEntity).Name) as SortedSet<TEntity>;
            if (values == null)
            {
                values = new SortedSet<TEntity>();
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.Priority = CacheItemPriority.NotRemovable;
                _cache.Set(typeof(TEntity).Name, values, cacheItemPolicy);
            }
            if (!values.Contains(entity)) values.Add(entity);
            if (values.Count >= _numberOfElements)
            {
                int count = values.Count;
                var valuesToSave = values.Take((int)count / 2);
                if (_saveElements == true)
                {
                    Save(valuesToSave);
                }
                values.RemoveWhere(el => valuesToSave.Contains(el));
            }
        }

        private void Save(IEnumerable<TEntity> entities)
        {
            _decoratedRepo.Add(entities);
        }

        public void Add(IEnumerable<TEntity> entity)
        {
            if (entity != null && entity.Count() > 0)
            {
                SortedSet<TEntity> values = _cache.Get(typeof(TEntity).Name) as SortedSet<TEntity>;
                if (values == null)
                {
                    values = new SortedSet<TEntity>();
                    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                    cacheItemPolicy.Priority = CacheItemPriority.NotRemovable;
                    _cache.Set(typeof(TEntity).Name, values, cacheItemPolicy);
                }
                values.UnionWith(entity);
                if (values.Count >= _numberOfElements)
                {
                    int count = values.Count;
                    var valuesToSave = values.Take((int)count / 2);
                    if (_saveElements == true)
                    {
                        Save(valuesToSave);
                    }
                    values = new SortedSet<TEntity>(values.Except(valuesToSave).OrderBy(el => el));
                    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                    cacheItemPolicy.Priority = CacheItemPriority.NotRemovable;
                    _cache.Set(typeof(TEntity).Name, values, cacheItemPolicy);
                }
            }
        }

        public void Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<TEntity> entities)
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _decoratedRepo.Dispose();
        }

        public void SaveChanges()
        {
            SortedSet<TEntity> values = _cache.Get(typeof(TEntity).Name) as SortedSet<TEntity>;
            _decoratedRepo.Add(values);
            _cache.Remove(typeof(TEntity).Name);
        }
    }
}
