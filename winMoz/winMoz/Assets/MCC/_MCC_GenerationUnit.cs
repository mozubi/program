﻿using System;
using System.Collections.Generic;
using winMoz.Markets.EoMMarkets.Elements;

namespace winMoz.Assets.MCC
{
    ///<summary>
    ///Klasse für Standardblöcke zur vereinfachten Konstruktion der (sonstigen) Marktgebiete
    ///
    /// Funktionsweise: Vereinfacht werden je Kraftwerksblock Gebotstupel erstellt:
    /// Gebote zum Optimalen Wirkungsgrad wurden (fiktiv) über Termin vermarktet und sind unlimitiertes Angebot
    /// Gebote über dem Wirkungsgradoptimum sind klassische flexible Angebote
    /// Gebote unter dem Wirkungsgradoptimum bis zur Mindestleistung sind flexible Nachfragegebote
    /// Bei KWK-Anlagen, wird anhand der KWK-Auslaustungskurve (externe Information, Szenario) die Min-Leistung entsprechend 
    /// nach oben korrigiert.        
    /// 
    /// </summary>
    public abstract class _MCC_GenerationUnit
    {

        /// <summary>
        /// Schalter zur ggf. notwendigen Neuberechnung der Volumina Winter/Sommer
        /// </summary>
        public byte IsSummer { get; set; }
        public EnumCollection_EOMMarkets.UnitType Type { get; set; }

        public abstract List<(decimal Price, decimal Volume)> GetBids(DateTime TS);

        public void CheckSummer(DateTime TS)
        {
            if (TS.Month > 3 && TS.Month < 10)
            { IsSummer = 1; }
            else { IsSummer = 0; }
        }

        public MCC_ThermalUnit.HalfYearType GetHalfYearType(DateTime TS)
        {
            if (TS.Month > 3 && TS.Month < 10)
                return MCC_ThermalUnit.HalfYearType.Summer;
            else
                return MCC_ThermalUnit.HalfYearType.Winter;
        }

    }
}
