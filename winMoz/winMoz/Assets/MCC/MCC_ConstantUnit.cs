﻿using System;
using System.Collections.Generic;
using winMoz.Markets.EoMMarkets.Elements;

namespace winMoz.Assets.MCC
{
    /// <summary>
    /// Klasse zur Erzeugung von Geboten für unflexible nicht-fossile Kraftwerke
    /// </summary>
    public class MCC_ConstantUnit : _MCC_GenerationUnit
    {
        /// <summary>
        /// Im mittel zur Verfügung stehende Leistung
        /// </summary>
        private float ActiveCapacity;

        /// <summary>
        /// Klasse zur Erzeugung von Geboten für nicht-variable nicht-fossile Kraftwerke
        /// </summary>
        /// <param name="activeCapacity"> Im mittel zur Verfügung stehende Leistung</param>
        public MCC_ConstantUnit(float activeCapacity)
        {
            ActiveCapacity = activeCapacity == -1 ? 0 : activeCapacity;
            Type = EnumCollection_EOMMarkets.UnitType.CONSTANT;
        }

        /// <summary>
        /// Gibt Gebot mit aktiver Leistung ab. Der Preis ist auf Grund fehlender Grenzkosten 0 €
        /// </summary>
        /// <param name="TS"></param>
        /// <returns></returns>
        public override List<(decimal Price, decimal Volume)> GetBids(DateTime TS)
        {
            List<(decimal, decimal)> Bids = new List<(decimal, decimal)>();
            Bids.Add(((decimal)0, (decimal)(ActiveCapacity)));
            return Bids;
        }
    }
}
