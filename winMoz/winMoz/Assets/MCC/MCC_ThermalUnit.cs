﻿using System;
using System.Collections.Generic;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Information;
using winMoz.Helper;
using winMoz.Data.Access.DTO;

namespace winMoz.Assets.MCC
{
    /// <summary>
    /// Klasse für externe thermische Kraftwerke
    /// </summary>
    public class MCC_ThermalUnit : _MCC_GenerationUnit
    {
        private float AvgEfficiency;
        private float NormStab;
        private float CarbonFactor;
        private float VariableCosts;
        private float RelPartFuture;
        private float MinimalUsage;
        private float OwnUsage;
        private decimal MakeBuyMarkDown;
        private float RelPartOutageSummer;
        private float RelPartOutageWinter;
        private float TotalCapacity;
        public winMoz.Information.Commodities.Commodity Commodity;


        /// <summary>
        /// Fixierte Wirkungsgrade anhand von normalisierten Zufallszahlen
        /// </summary>
        private float[] Efficiencies;
        /// <summary>
        /// Anzahl an Teilen, in die die Gesamtleistung je Typ zerlegt wird.
        /// </summary>
        private int PartsToDivide = 10;

        public enum MarketingType {
            MinVolume,    //MinLeistung = unlimitierter Verkauf
            MakeOrBuy_Purchase, //minWK bis RelFuture = MakeOrBuy Kauf
            MakeOrBuy_Sell //relfuture bis Max = Limitierter Verkauf zu MC
        }

        public enum HalfYearType { Summer, Winter }

        Dictionary<MarketingType, Dictionary<HalfYearType, decimal>> Volumes;

        ///// <summary>
        ///// Array für Volumen:
        ///// 0 - MinLeistung = unlimitierter Verkauf
        ///// minWK bis RelFuture = MakeOrBuy Kauf
        ///// relfuture bis Max = Limitierter Verkauf zu MC
        ///// </summary>
        //private decimal[,] Volumes = new decimal[3, 2];



        public MCC_ThermalUnit(GenPara_Thermal_Dto genPara,
            float totalCapacity,
            winMoz.Information.Commodities.Commodity commodity)
        {
            Volumes = new Dictionary<MarketingType, Dictionary<HalfYearType, decimal>>();
            Type = EnumCollection_EOMMarkets.UnitType.THERMAL;
            AvgEfficiency = genPara.wk;
            NormStab = genPara.normStab;
            CarbonFactor = genPara.carbon;
            VariableCosts = genPara.var_costs;
            RelPartFuture = genPara.relfuture;
            MinimalUsage = genPara.minWK;
            OwnUsage = genPara.own_usage;
            MakeBuyMarkDown = genPara.makeBuy;
            RelPartOutageSummer = 1 - genPara.outageSummer;
            RelPartOutageWinter = 1 - genPara.outageWinter;
            TotalCapacity = totalCapacity == -1 ? 0 : totalCapacity;
            Commodity = commodity;
            PartsToDivide = 10;
            InitVolumes();
            SetRndEfficiencies();
        }

        ///// <summary>
        ///// Gibt die Gebote für den Tag TS zurück
        ///// </summary>
        ///// <param name="TS"></param>
        ///// <returns></returns>
        //public override List<(decimal Price, decimal Volume)> GetBids(DateTime TS)
        //{
        //    float Fuel = Commodities.GetPrice(TS, Commodity);
        //    float Carbon = Commodities.GetPrice(TS, Commodities.Commodity.Carbon);
        //    float Fuelterm = Fuel + CarbonFactor * Carbon;
        //    List<(decimal Price, decimal Volume)> Results = new List<(decimal Price, decimal Volume)>();

        //    CheckSummer(TS);

        //    for (int i = 0; i < PartsToDivide; i++)
        //    {
        //        decimal MC = (decimal)((1 + OwnUsage) * (VariableCosts + Fuelterm / Efficiencies[i]));

        //        Results.Add((-99999999, Volumes[0, IsSummer]));
        //        Results.Add((MC * MakeBuyMarkDown, Volumes[1, IsSummer]));
        //        Results.Add((MC, Volumes[2, IsSummer]));
        //    }

        //    return Results;
        //}

        /// <summary>
        /// Gibt die Gebote für den Tag TS zurück
        /// </summary>
        /// <param name="TS"></param>
        /// <returns></returns>
        public override List<(decimal Price, decimal Volume)> GetBids(DateTime TS)
        {
            float Fuel = Commodities.GetPrice(TS, Commodity);
            float Carbon = Commodities.GetPrice(TS, Commodities.Commodity.Carbon);
            float Fuelterm = Fuel + CarbonFactor * Carbon;
            List<(decimal Price, decimal Volume)> Results = new List<(decimal Price, decimal Volume)>();

            var HalfYearType = GetHalfYearType(TS);

            for (int i = 0; i < PartsToDivide; i++)
            {
                decimal MC = (decimal)((1 + OwnUsage) * (VariableCosts + Fuelterm / Efficiencies[i]));

                Results.Add((-99999999, Volumes[MarketingType.MinVolume][HalfYearType]));
                Results.Add((MC * MakeBuyMarkDown, Volumes[MarketingType.MakeOrBuy_Purchase][HalfYearType]));
                Results.Add((MC, Volumes[MarketingType.MakeOrBuy_Sell][HalfYearType]));
            }

            return Results;
        }

        /// <summary>
        /// Setzt ein (immer gleiches) Standardseed anhand der Durchschnittseffizienz und den damit berechnenten Effizienzen
        /// </summary>
        public void SetRndEfficiencies()
        {
            Efficiencies = new float[PartsToDivide];
            int Seed = (int)(AvgEfficiency * 10000);
            for (int i = 0; i < PartsToDivide; i++)
            {
                decimal Efficiency = MathHelper.GetNormVerteilt((float)AvgEfficiency, (float)NormStab, Seed);
                Seed = (int)(Efficiency * 10000);
                Efficiencies[i] = (float)Efficiency;
            }
        }
        

        ///// <summary>
        ///// Initialisiert einmalig die Volumina für Sommer / Winter für die drei Stufen:
        ///// Unlimited, MakeOrBuy, AddtionalProduction
        ///// </summary>
        //private void InitVolumes()
        //{
        //    float Volume;

        //    float PartCapacity = TotalCapacity * Math.Pow(PartsToDivide, -1);

        //    //Wenn Last sonst zu hoch. Integration der Angebote aus dem Future Handel.
        //    Volume = MinimalUsage > 0 ? RelPartFuture * PartCapacity : 0; // MinimalUsage * PartCapacity; 

        //    Volumes[0, 0] = (decimal)(Volume * RelPartOutageSummer);
        //    Volumes[0, 1] = (decimal)(Volume * RelPartOutageWinter);

        //    Volume = RelPartFuture > MinimalUsage ? (RelPartFuture - MinimalUsage) * PartCapacity * -1 : 0; //Kauf: Make Or Buy

        //    Volumes[1, 0] = (decimal)(Volume * RelPartOutageSummer);
        //    Volumes[1, 1] = (decimal)(Volume * RelPartOutageWinter);

        //    Volume = RelPartFuture < 1 ? (1 - RelPartFuture) * PartCapacity : 0; //Kauf: Make Or Buy

        //    Volumes[2, 0] = (decimal)(Volume * RelPartOutageSummer);
        //    Volumes[2, 1] = (decimal)(Volume * RelPartOutageWinter);

        //}

        /// <summary>
        /// Initialisiert einmalig die Volumina für Sommer / Winter für die drei Stufen:
        /// Unlimited, MakeOrBuy, AddtionalProduction
        /// </summary>
        private void InitVolumes()
        {
            float Volume;

            float PartCapacity = TotalCapacity * (float)Math.Pow(PartsToDivide, -1);

            //Wenn Last sonst zu hoch. Integration der Angebote aus dem Future Handel.
            Volume = MinimalUsage > 0 ? RelPartFuture * PartCapacity : 0; // MinimalUsage * PartCapacity; 

            Volumes.Add(MarketingType.MinVolume, GetWinterAndSummerVolumes((decimal)Volume, (decimal)RelPartOutageSummer, (decimal)RelPartOutageWinter));

            Volume = RelPartFuture > MinimalUsage ? (RelPartFuture - MinimalUsage) * PartCapacity * -1 : 0; //Kauf: Make Or Buy

            Volumes.Add(MarketingType.MakeOrBuy_Purchase, GetWinterAndSummerVolumes((decimal)Volume, (decimal)RelPartOutageSummer, (decimal)RelPartOutageWinter));

            Volume = RelPartFuture < 1 ? (1 - RelPartFuture) * PartCapacity : 0; //Kauf: Make Or Buy

            Volumes.Add(MarketingType.MakeOrBuy_Sell, GetWinterAndSummerVolumes((decimal)Volume, (decimal)RelPartOutageSummer, (decimal)RelPartOutageWinter));

        }

        private Dictionary<HalfYearType, decimal> GetWinterAndSummerVolumes(decimal volume, decimal partOutageSummer, decimal partOutageWinter)
        {
            return new Dictionary<HalfYearType, decimal>
                {
                    { HalfYearType.Summer,  (decimal)(volume * partOutageSummer)},
                    { HalfYearType.Winter,  (decimal)(volume * partOutageWinter)}
                };
        }
    }
}
