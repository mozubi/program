﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;
using winMoz.Markets.EoMMarkets.Elements;

namespace winMoz.Assets.MCC
{
    /// <summary>
    /// Klasse für externe Windkraftwerke -> Eine Klasse für WindOnshore und WindOffshore
    /// </summary>
    public class MCC_WindUnit : _MCC_GenerationUnit
    {
        private float Capacity;
        private decimal VBH_Factor;
        private List<(float WindSpeed, float NormPower)> PowerCurve;
        public List<(DateTime TimeStamp, float Volume)> WindSpeedList;
        public List<(DateTime TimeStamp, float Volume)> WindSimulation;

        public MCC_WindUnit(float capacity, decimal vbh_TYNDP, int tyndp_Forecast_Year, List<(float WindSpeed, float NormPower)> powerCurve, EnumCollection_EOMMarkets.UnitType type, MarketAreaEnum country, List<(DateTime TimeStamp, float Volume)> windSimulation)
        {
            Capacity = capacity == -1 ? 0 : capacity; 
            PowerCurve = new List<(float, float)>();
            PowerCurve.AddRange(powerCurve);
            Type = type;
            VBH_Factor = GetVBHFactorForCapacity(vbh_TYNDP, tyndp_Forecast_Year, windSimulation);
        }

        public decimal GetVBHFactorForCapacity(decimal vbh_TYNDP, int tyndp_Forecast_year, List<(DateTime TimeStamp, float Volume)> windSimulation)
        {
            float TimeDifferenceFactor = EoM_Extensions.CalculateTimeDifferenceFactor(tyndp_Forecast_year, windSimulation.Select(wind => wind.TimeStamp));
            decimal vbhFactor = vbh_TYNDP == 0 ? 0 : (decimal)TimeDifferenceFactor * GetVBH(windSimulation) / vbh_TYNDP;
            return vbhFactor;
        }

        private decimal GetVBH(List<(DateTime TimeStamp, float Volume)> windSimulation) => (decimal)windSimulation.Select(item => GetPowerFromWindSpeed(item.Volume) / 4000.0).Sum();

        public override List<(decimal Price, decimal Volume)> GetBids(DateTime TS)
        {
            List<(decimal, decimal)> Bids = new List<(decimal, decimal)>();
            Bids.Add(((decimal)0, (decimal)Capacity));
            return Bids;
        }


        private float GetPowerFromWindSpeed(float windSpeed)
        {
            if (windSpeed != 0)
            {
                (int Tupel1, int Tupel2, bool BolInFirstOrLastBidTupel) = MeritOrder_FindBidTupels(PowerCurve.Select(item => item.WindSpeed).ToList(), windSpeed);
                if (BolInFirstOrLastBidTupel)
                    return PowerCurve[Tupel1].NormPower;
                else
                {
                    var PowerLower = PowerCurve[Tupel1];
                    var PowerUpper = PowerCurve[Tupel2];
                    return (PowerLower.NormPower + ((windSpeed - PowerLower.WindSpeed) / (PowerUpper.WindSpeed - PowerLower.WindSpeed)) * (PowerUpper.NormPower - PowerLower.NormPower));
                }
            }
            else
                return 0;
        }

        public static (int Tupel1, int Tupel2, bool BolInFirstOrLastBidTupel) MeritOrder_FindBidTupels(List<float> meritOrder, float value)
        {
            int a, b, c;

            a = 0;
            c = meritOrder.Count - 1;

            if (meritOrder[a] > value) return (Tupel1: 0, Tupel2: 0, BolInFirstOrLastBidTupel: true);
            if (meritOrder[c] < value) return (Tupel1: meritOrder.Count() - 1, Tupel2: meritOrder.Count() - 1, BolInFirstOrLastBidTupel: true);

            //vorher i + 1 statt i
            for (int i = 0; 2 + (1 - Math.Pow(2, i)) / (1 - 2) <= meritOrder.Count(); i++) //Ausfallbedingung durch geometrische Summenformel.
            {
                b = (a + c) / 2;
                if (meritOrder[b] > value)
                {
                    if (meritOrder[b - 1] > value)
                        //Menge ist zu hoch, setzte c = b
                        c = b;
                    else
                        //Preis liegt zwischen b und b-1
                        return (Tupel1: b - 1, Tupel2: b, BolInFirstOrLastBidTupel: false);
                }
                else
                {
                    if (meritOrder[b + 1] < value)
                        //Menge ist zu niedrig, setzte a = b
                        a = b;
                    else
                        //Preis liegt zwischen b und b +1
                        return (Tupel1: b, Tupel2: b + 1, BolInFirstOrLastBidTupel: false);
                }
            }
            return (Tupel1: a, Tupel2: c, BolInFirstOrLastBidTupel: false);
        }

        /// <summary>
        /// Gibt die Korrektur der Windstromproduktion nach aktuellen Wetterbedingungen für den Tag aus
        /// Das Wetter von Italien wird durch dem von Frankreich ersetzt und das Wetter von Schweden durch das von Dänemark
        /// TO-DO: Wetterdaten von Italien und Schweden suchen. 
        /// </summary>
        /// <param name="TS"></param>
        /// <param name="Country"></param>
        /// <returns></returns>
        public List<(DateTime TimeStamp, float Volume)> GetReducedVolume(DateTime TS, MarketAreaEnum Country)
        {
            WindSpeedList = Weather.GetWindspeed(TS, new TimeSpan(24, 0, 0), Country.ToString() == "IT" ? "FR" : Country.ToString() == "SE" ? "DK" : Country.ToString(), isExtended: false).ToList();
            var result1 = WindSpeedList.Select(item => (item.TimeStamp, Capacity - (float)VBH_Factor * GetPowerFromWindSpeed(item.Volume) * Capacity)).ToList();
            var result2 = result1.Resample(step: Resampling.TimeStep.Hour).ToList();
            return result2;
        }
    }
}
