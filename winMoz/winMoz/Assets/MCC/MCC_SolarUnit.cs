﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Markets.EoMMarkets.Elements;
using winMoz.Information;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Elements;
using winMoz.Helper;

namespace winMoz.Assets.MCC
{
    /// <summary>
    /// Klasse für externe Solarkraftwerke
    /// </summary>
    public class MCC_SolarUnit : _MCC_GenerationUnit
    {
        private float Capacity;
        private decimal VBH_Factor;
        private List<(DateTime TimeStamp, float Volume)> SolarRadiation;
        private MarketAreaEnum Country { get; set; }
        
        
        public MCC_SolarUnit(float capacity, decimal vbh_TYNDP, int tyndp_Forecast_Year, MarketAreaEnum country, List<(DateTime TimeStamp, float Volume)> solarSimulation)
        {
            Type = EnumCollection_EOMMarkets.UnitType.SOLAR;
            Capacity = capacity == -1 ? 0 : capacity;
            VBH_Factor = GetVBHFactorForCapacity(vbh_TYNDP, tyndp_Forecast_Year, solarSimulation);
        }

        public decimal GetVBHFactorForCapacity(decimal vbh_TYNDP, int tyndp_Forecast_year, List<(DateTime TimeStamp, float Volume)> solarSimulation)
        {
            float TimeDifferenceFactor = EoM_Extensions.CalculateTimeDifferenceFactor(tyndp_Forecast_year, solarSimulation.Select(solar => solar.TimeStamp));
            decimal vbhFactor = (decimal)TimeDifferenceFactor * GetVBH(solarSimulation) / vbh_TYNDP;
            return vbhFactor;
        }

        private decimal GetVBH(List<(DateTime TimeStamp, float Volume)> solarSimulation) => (decimal)solarSimulation.Select(item => item.Volume / 4000.0).Sum();

        /// <summary>
        /// Gibt die Gebot mit maximaler Solarstromproduktion für die Stunde aus
        /// </summary>
        public override List<(decimal Price, decimal Volume)> GetBids(DateTime TS)
        {
            List<(decimal, decimal)> Bids = new List<(decimal, decimal)>();
            Bids.Add(((decimal)0, (decimal)Capacity));
            return Bids;
        }

        /// <summary>
        /// Gibt die Korrektur der Solarstromproduktion nach aktuellen Wetterbedingungen für den Tag aus
        /// Das Wetter von Italien wird durch dem von Frankreich ersetzt und das Wetter von Schweden durch das von Dänemark
        /// TO-DO: Wetterdaten von Italien und Schweden suchen. 
        /// </summary>
        /// <param name="TS"></param>
        /// <param name="Country"></param>
        /// <returns></returns>
        public List<(DateTime TimeStamp, float Volume)> GetReducedVolume(DateTime TS, MarketAreaEnum Country)
        {
            var SolarRadiationList = Weather.GetSolar(TS, new TimeSpan(24, 0, 0), Country.ToString() == "IT" ? "FR" : Country.ToString() == "SE" ? "DK" : Country.ToString()).ToList()
                .Select(item => (item.TimeStamp, Capacity - (float)VBH_Factor * (item.RadDirHor + item.RadDiffHor) / 1000 * Capacity)).ToList();
            return SolarRadiationList;
        }
    }
}
