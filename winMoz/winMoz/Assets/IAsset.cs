﻿using System;
using System.Collections.Generic;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Markets;
using winMoz.Simulation;
using winMoz.Trading.Strategies;

namespace winMoz.Assets
{
    public interface IAsset : ISimObject
    {
        
        #region Booleans
        bool IsLoad { get; }
        bool IsPlant { get; }
        bool IsThermal { get; }
        bool IsRenewable { get; }
        bool IsFlexibility { get; }
        bool IsVariableRenewable { get; }
        #endregion
        #region Attributes
        float PowerMW { get;  }

        /// <summary>
        /// Daten zur Position des Assets
        /// </summary>
        Location Location { get;  }

        /// <summary>
        /// Postleitzahl als Datenbankschlüssel zur Position des Assets
        /// </summary>
        int LocationPostCode { get; }

        /// <summary>
        /// Ökonomische Anlageneigenschaften
        /// </summary>
        EconomicAttributes Economics { get; }

        /// <summary>
        /// Name der Ökonomischen Eigenschaften als Datenbankschlüssel
        /// </summary>
        string EconomicsSubType { get; }
        #endregion Attributes


        IBalancingGroup BalancingGroup { get; }
        int BalancingGroupId { get;}
        Schedule Schedule { get; }
        ControlReserveSchedule ControlReserveSchedule { get; }

        void CalcSchedule(DateTime timeStamp, TimeSpan duration);
        void SetBalancingGroup(BalancingGroup bg);
        List<(DateTime TimeStamp, List<(float volume, float cost)> )> GetCostCurvePerMWh(DateTime TimeStamp, TimeSpan duration, TimeSpan step);
        float GetCostForMWh(DateTime timeStamp, float PowerMWh);

        void SetNewAssumedCallProbability(ControlReserveShortCode.ShortCode shortCode, float asumedCallProbability);

        Dictionary<ControlReserveShortCode.ShortCode, float> GetAssumedCallProbability();

        void CutScheduleUntil(DateTime TimeStamp);

        void CutControlReserveScheduleUntil(DateTime TimeStamp);

        TradingStrategyType TradingStrategy { get; }

        void UpdateBalancingGroup();
    }
}