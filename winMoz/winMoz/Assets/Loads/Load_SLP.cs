﻿using System;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Assets.Schedules;

namespace winMoz.Assets.Loads
{
    /// <summary>
    /// Die Klasse implementiert ein Asset für die Netzlast mit Hilfe der <see cref="SLP"/> Klasse.
    /// </summary>
    public class Load_SLP : Asset
    {
        public enum SubTypes { NONE };
        public readonly SubTypes SubType;
        public SLP SLP { get; private set; }

        /// <summary>
        /// Jährliche Energieproduktion in GWh
        /// </summary>
        public float EnergyAnnualGWh { get; private set; }
        /// <summary>
        /// Art des Standardlastprofils
        /// </summary>
        public SLP.Profile ProfileType { get; private set; }

        public string Profile { get; private set; }

        #region Constructors
        public Load_SLP()
        {
            this.SubType = SubTypes.NONE;
        }

        public Load_SLP(BalancingGroup bg, int locationPostcode, int scenarioID = 0, params (SLP.Profile subType, float EnergyAnnualGWh)[] profiles) : base(bg, SubTypes.NONE, 0.0F, locationPostcode, scenarioID: scenarioID) // new AssetAttributes(profiles.Sum(el => el.EnergyAnnualGWh), location, new EconomicAttributes()))
        {
            this.SLP = new SLP(profiles);
            this.EnergyAnnualGWh = profiles.Sum(el => el.EnergyAnnualGWh);
            ProfileType = profiles.Select(el => el.subType).First();
            Profile = ProfileType.ToString();
        }
        #endregion

        //public Load_SLP(BalancingGroup bg, SLPAttributes attributes, int scenarioID = 0) : base(bg,  SubTypes.NONE, attributes, scenarioID: scenarioID)
        //{
        //    this.SLP = new SLP(attributes.ProfileType, attributes.EnergyAnnualGWh * 1000000);
        //}

        #region CalcSchedule Override
        /// <summary>
        /// Die Funktion errechnet den Fahrplan für einen gegebenen Zeitraum. Dazu nutzt sie die <see cref="SLP"/> Klasse. 
        /// </summary>
        /// <param name="startTime"> Zeitpunkt des ersten Fahrplaneintags, der berechnet wird. </param>
        /// <param name="duration"> Zeitraum über welchen der Fahrplan berechnet wird.</param>
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            this.Schedule.Extend(startTime, duration);
            this.Schedule.SetPower(this.SLP.Get(startTime, duration).Select(el => (el.TimeStamp, el.Power / 1000)).ToList());
        }
        #endregion
    }
}
