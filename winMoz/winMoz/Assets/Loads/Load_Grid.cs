﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Tools.Prediction;
using winMoz.Tools.Prediction.GridLoad;
using winMoz.Agents;
using winMoz.Trading.Strategies;

namespace winMoz.Assets.Loads
{
    /// <summary>
    /// Die Klasse implementiert ein Asset für die Netzlast mit Hilfe der <see cref="PredictorGridLoad"/> Klasse.
    /// </summary>
    public class Load_Grid : Asset
    {
        IPredictor<ForecastGridLoad, (DateTime TimeStamp, float Power)> loadPredictor = new PredictorGridLoad();
        public enum SubTypes { NONE }
        public readonly SubTypes SubType = SubTypes.NONE;

        /// <summary>
        /// Jährlicher Gesamtenergieverbrauch in MWh
        /// </summary>
        public float BaseLoadSumMWh { get; private set; }
        /// <summary>
        /// Benötigte Leistung in MW
        /// </summary>
        public float BaseLoadPowerMW { get; private set; }
        /// <summary>
        /// Skalierungsfaktor zum skalieren der Leistung zwischen 0-1
        /// </summary>
        public float ScaleFactor { get; private set; } = 1;

        #region Static Constructors
        public static Load_Grid FromEnergy(IBalancingGroup bg, float loadSumMWh, int locationPostcode = default, int scenarioID = default)
        {
            var attributes = new Load_Grid(bg, locationPostcode, scenarioID);
            attributes.ScaleWithEnergy(loadSumMWh);
            attributes.SetPower();
            return attributes;
        }

        public static Load_Grid FromPower(IBalancingGroup bg, float loadPowerMW, int locationPostcode = default, int scenarioID = default)
        {
            var attributes = new Load_Grid(bg, locationPostcode, scenarioID);
            attributes.ScaleWithPower(loadPowerMW);
            attributes.SetPower();
            return attributes;
        }

        public static Load_Grid FromScale(IBalancingGroup bg, float scaleFactor, int locationPostcode = default, int scenarioID = default)
        {
            var attributes = new Load_Grid(bg, locationPostcode, scenarioID);
            attributes.ScaleFactor = scaleFactor;
            attributes.SetPower();
            return attributes;
        }
        #endregion

        #region Constructors
        public Load_Grid()
        {
        }

        public Load_Grid(IBalancingGroup bg, int locationPostcode = 66115, int scenarioID = 0) : base(bg, subType: SubTypes.NONE, powerMW: 0.0F, locationPostcode: locationPostcode, scenarioID: scenarioID, tradingStrategy: TradingStrategyType.Unlimited)
        {
            SetBaseLoadSum();
            SetBaseLoadPower();
            SetPower();
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Funktion errechnet den jährlichen Energiebedarf durch Addition der <see cref="PredictorGridLoad"/> Ergebnisse und setzt die <see cref="BaseLoadSumMWh"/> Eigenschaft.
        /// </summary>
        public void SetBaseLoadSum()
        {
            DateTime Start = new DateTime(2015, 1, 1, 0, 0, 0);
            DateTime End = new DateTime(2015, 12, 31, 23, 59, 59);
            TimeSpan Duration = End - Start;

            var forecast = new List<ForecastGridLoad>();
            for (int qh = 0; qh < Duration.TotalHours * 4; qh++)
                forecast.Add(new ForecastGridLoad(Start.AddMinutes(15 * qh)));

            IPredictor<ForecastGridLoad, (DateTime TimeStamp, float Power)> loadPredictor = new PredictorGridLoad();
            var prediction = loadPredictor.Predict(forecast);
            BaseLoadSumMWh = prediction.Select(item => item.Power / 4).Sum();
        }
        /// <summary>
        /// Funktion setzt die Eigenschaft <see cref="Asset.PowerMW"/> basierend auf dem <see cref="ScaleFactor"/> und der <see cref="BaseLoadPowerMW"/>.
        /// </summary>
        public void SetPower() => SetPower(ScaleFactor * BaseLoadPowerMW);
        #endregion

        #region Private Functions
        private void SetBaseLoadPower() => BaseLoadPowerMW = BaseLoadSumMWh / 8760;

        private void ScaleWithEnergy(float loadSumMWh) => ScaleFactor = loadSumMWh / BaseLoadSumMWh;

        private void ScaleWithPower(float loadPowerMW) => ScaleFactor = loadPowerMW / BaseLoadPowerMW;
        #endregion

        #region CalcSchedule Override
        /// <summary>
        /// Die Funktion errechnet den Fahrplan für einen gegebenen Zeitraum. Dazu nutzt sie die <see cref="PredictorGridLoad"/> Klasse. 
        /// </summary>
        /// <param name="startTime"> Zeitpunkt des ersten Fahrplaneintags, der berechnet wird. </param>
        /// <param name="duration"> Zeitraum über welchen der Fahrplan berechnet wird.</param>
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            DateTime EndOfDuration = startTime.Add(duration);
            if (Schedule.Count() == 0 || !(Schedule.First().TimeStamp.Ticks <= startTime.Ticks) || !(Schedule.Last().TimeStamp.Ticks >= startTime.Add(duration).AddMinutes(-15).Ticks))
            {
                Schedule.Extend(startTime, duration);
                var forecast = new List<ForecastGridLoad>();
                for (int qh = 0; qh < duration.TotalHours * 4; qh++)
                    forecast.Add(new ForecastGridLoad(startTime.AddMinutes(15 * qh)));

                var prediction = loadPredictor.Predict(forecast);

                //foreach (var pred in prediction)
                this.Schedule.SetPower(prediction.Select(el => (el.TimeStamp, -this.ScaleFactor * el.Power)).ToList());
            }
        }
        #endregion
    }
}
