﻿using System;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Tools.Prediction.GridLosses;

namespace winMoz.Assets.Loads
{
    /// <summary>
    /// Die Klasse implementiert ein Asset für den Verlustbilanzkreis mit Hilfe der <see cref="PredictorGridLosses"/> Klasse.
    /// </summary>
    public class GridLosses : Asset
    {
        /// <summary>
        /// Standard Lastprofil
        /// </summary>
        SLP slp { get; set; }
        PredictorGridLosses predictor = new PredictorGridLosses();
        #region  Constructors
        public GridLosses(BalancingGroup bg, SLP slp) : base(bg, new EconomicAttributes("Losses"), Location.Random(), 0.0F, tradingStrategy: Trading.Strategies.TradingStrategyType.Unlimited)
        {
            this.slp = slp;
        }

        private GridLosses(BalancingGroup bg, Enum subType, float powerMW) : base(bg, subType, powerMW) { }

        //private GridLosses(BalancingGroup bg,  Enum subType, AssetAttributes attributes) : base(bg,  subType, attributes) { }

        //private GridLosses(BalancingGroup bg,  string subType, AssetAttributes attributes) : base(bg,  subType, attributes) { }

        private GridLosses(BalancingGroup bg, Enum subType, float powerMW, Location location) : base(bg, subType, powerMW, location) { }
        #endregion
        #region CalcSchedule Override
        /// <summary>
        /// Die Funktion errechnet den Fahrplan für einen gegebenen Zeitraum. Dazu nutzt sie die <see cref="PredictorGridLosses"/> Klasse. 
        /// </summary>
        /// <param name="startTime"> Zeitpunkt des ersten Fahrplaneintags, der berechnet wird. </param>
        /// <param name="duration"> Zeitraum über welchen der Fahrplan berechnet wird.</param>
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            var input = this.slp.Get(startTime, duration).Select(el => new ForecastGridLosses(el.TimeStamp, el.Power));
            var prediction = predictor.Predict(input);

            this.Schedule.Extend(startTime, duration);
            this.Schedule.SetPower(prediction.Select(el => (el.TimeStamp, el.GridLossPrediction)).ToList());
        }
        #endregion
    }
}
