﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;

namespace winMoz.Assets.Loads
{
    public class SLP 
    {
        public enum Profile { H0, G0, G1, G2, G3, G4, G5, G6, L0, L1, L2 }

        Lookup<Profile, SLPValue> ProfileValues { get; set; }

        public List<(Profile Type, float EnergyAnnualkWh)> Profiles { get; private set; }


        #region Constructors
        public SLP(params (Profile profile, float EnergyAnnualkWh)[] profiles)
        {
            this.Profiles = new List<(Profile, float)>(profiles);
            LoadProfiles();
        }
        public SLP(Profile profile, float EnergyAnnualkWh) : this((profile, EnergyAnnualkWh)) { }

        public SLP(string subType, float EnergyAnnualkWh) : this(((Profile)Enum.Parse(typeof(Profile), subType), EnergyAnnualkWh)) { }

        public SLP(params (string subType, float EnergyAnnualkWh)[] profiles) : this(profiles.Select(el => ((Profile)Enum.Parse(typeof(Profile), el.subType), el.EnergyAnnualkWh)).ToArray()) { }
        #endregion

        private void LoadProfiles()
        {
            using (var repo = new ScenarioData_Repository())
                this.ProfileValues = repo.SlpRepository.GetSLP(this.Profiles.Select(el => el.Type).ToArray());
        }

        #region SLP Calculation
        public List<(DateTime TimeStamp, float Power)> Get(DateTime startTime, TimeSpan duration)
        {
            // Vorzeichen von Power_Diff positiv bei Kauf - negativ bei Verkauf -> PowerPlanned einer Last ! negativ
            var slp = new List<(DateTime TimeStamp, float Power)>();
            for (DateTime t = startTime; t.CompareTo(startTime + duration) < 0; t = t.AddMinutes(15))
            {
                float power = 0;
                foreach (var profile in this.Profiles)
                {
                    var profileValues = this.ProfileValues[profile.Type].ToList();
                    power -= Calc_QHSLP(t, profile.EnergyAnnualkWh, profileValues);
                }
                slp.Add((t, power/1000));
            }
            return slp;
        }

        /// <summary>
        /// Methode berechnet und gibt einen Viertelstundenwert eines SLP-Kundens zurück in Watt
        /// </summary>
        /// <param name="load">SLP-Typ und Jahresenergieverbrauch des Profils</param>
        /// <param name="time">Zeitpunkt, für den der Viertelstundenwert berechnet werden soll</param>
        /// <param name="slp">Standardlastprofil der Kundengruppe</param>
        /// <returns>Skalierte und dynamisierte Viertelstundenleistung des Profils zum Zeitpunkt</returns>
        private float Calc_QHSLP(DateTime time, float energyAnnualkWh, List<SLPValue> slp)
        {
            var slpValue = slp.FirstOrDefault(pr => pr.Season == Season(time.DayOfYear) && pr.Day == DayOfWeek(time) && pr.QuarterHour == time.GetQuarterHourOfDay());
            var dynFactor = DynFactor(time.DayOfYear);
            var scale = Scaling(energyAnnualkWh);

            return ((slpValue.Power * dynFactor).Round(1) * scale);
        }
        #endregion

        #region Methoden zur Berechnung eines Viertelstundenwertes eines Standardlastprofils nach VDEW SLP-Verfahren
        /// <summary>
        /// Methode bestimmt die Jahreszeit zur Auswahl des relevanten Standardlastprofils (SLP)
        /// </summary>
        /// <param name="time">Zeitpunkt des Berechnungsschrittes als DateTime</param>
        /// <returns>SLP-Jahreszeit als String</returns>
        public string Season(int dayOfYear)
        {
            if (dayOfYear > 305 || dayOfYear < 79) return "Sommer"; // in den VDEW Lastprofilen ist die Saison vertauscht
            else if (dayOfYear > 135 && dayOfYear < 257)return "Winter";
            else return "Uebergangszeit";
        }

        /// <summary>
        /// Methode bestimmt den Wochentag zur Auswahl des relevanten Standardlastprofils (SLP)
        /// </summary>
        /// <param name="time">Zeitpunkt des Berechnungsschrittes als DateTime</param>
        /// <returns>SLP-Wochentag als String</returns>
        private string DayOfWeek(DateTime time)
        {
            if (time.IsWeekDay()) return "Werktag";
            else if (time.DayOfWeek == System.DayOfWeek.Saturday) return "Samstag";
            else return "Sonntag";
        }

        /// <summary>
        /// Methode berechnet den Dynamisierungsfaktor Ft für standardisiertes Lastprofil nach VDEW
        /// </summary>
        /// <param name="dayInYear">Zeitpunkt des Simulationsschrittes für den der Profilwert bestimmt werden soll als DateTime</param>
        /// <returns>Dynamisierungsfaktor Ft als float</returns>
        private float DynFactor(int dayInYear) => (float)Math.Round((-3.92F * 10).Pow(-10) * dayInYear.Pow(4) + (3.2F * 10).Pow(-7) * dayInYear.Pow(3) - (7.02F * 10).Pow(-5) * dayInYear.Pow(2) + (2.1F * 10).Pow(-3) * dayInYear + 1.24F, 4);

        /// <summary>
        /// Methode zur Berechnung des Skalierungsfaktors für SLP-Kunden - Berücksichtigung des Jahresverbrauchs der Kundengruppe
        /// </summary>
        /// <param name="energyAnnualkWh">Jahresenergieverbrauch in kWh als float</param>
        /// <returns>Skalierungsfaktor = Quotient des Jahresenergieverbrauchs des SLP-Kundens und 1000kWh </returns>
        private float Scaling(float energyAnnualkWh) => energyAnnualkWh / 1000;
        #endregion

    }

    public class SLPValue
    {
        public SLP.Profile Profile { get; private set; }
        public SLPValue() { }

        public string Season { get; private set; }
        public string Day { get; private set; }
        public short QuarterHour { get; private set; }
        public float Power { get; private set; }
    }
}