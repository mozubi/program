﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Tools.Prediction.DiffBk;

namespace winMoz.Assets.Loads
{
    /// <summary>
    /// Die Klasse implementiert ein Asset für den Differenzbilanzkreises mit Hilfe der <see cref="PredictorDifference"/> Klasse.
    /// </summary>
    public class Difference : Asset
    {
        public enum SubTypes { FINANCIAL }
        PredictorDifference predictor = new PredictorDifference();
        #region Constructors
        private Difference() { }

        public Difference(IBalancingGroup bg, float powerMW) : base(bg, new EconomicAttributes("FINANCIAL", lCOE: 40.0F), Location.Random(), powerMW * 8760)
        {
        }
        #endregion

        #region CalcSchedule Override
        /// <summary>
        /// Die Funktion errechnet den Fahrplan für einen gegebenen Zeitraum. Dazu nutzt sie die <see cref="PredictorDifference"/> Klasse. 
        /// </summary>
        /// <param name="startTime"> Zeitpunkt des ersten Fahrplaneintags, der berechnet wird. </param>
        /// <param name="duration"> Zeitraum über welchen der Fahrplan berechnet wird.</param>
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            if (duration.TotalHours > 1)
            {
                var historic = predictor.HistoricValues.Where(el => el.TimeStamp.DayOfYear == startTime.DayOfYear).Take((int)duration.TotalMinutes / 15).ToList();

                var forecast = Enumerable.Range(0, (int)duration.TotalMinutes / 15).Select(el => startTime.AddMinutes(el * 15)).Zip(historic, (t, h) => new ForecastDiff(t, h.DifferencePowerMW)).ToList();

                var prediction = predictor.Predict(forecast).Select(el => (el.TimeStamp, el.DifferencePrediction * this.PowerMW)).ToList();

                this.Schedule.Extend(startTime, duration);
                this.Schedule.SetPower(prediction);
            }
        }
        #endregion
    }
}
