﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Helper;
using winMoz.Trading.Strategies;

namespace winMoz.Assets.Plants
{
    /// <summary>
    /// Klasse zur Definition des Speichertyps - Erbt Eigenschaften der abstrakten Klasse Asset
    /// </summary>
    public class Plant_Storage : Asset
    {
        public float Capacity { get; private set; }
        /// <summary>
        /// Minimale Ladung des Speichers in MWh
        /// </summary>
        public float MinSoC => 0.1F * Capacity;
        /// <summary>
        /// Maximale Ladung des Speichers in MWh
        /// </summary>
        public float MaxSoC => Capacity;
        /// <summary>
        /// Viertelstündliche Leistungsänderung in MW
        /// </summary>
        public float P_Delta_QH => 0.98F * P_Charge_Max;
        /// <summary>
        /// Maximale Leistung beim Laden in MW
        /// </summary>
        public float P_Charge_Max => this.PowerMW;
        /// <summary>
        /// Maximale Leistung beim Entladen in MW
        /// </summary>
        public float P_Discharge_Max => this.PowerMW;
        /// <summary>
        /// Daten zur Effizienz des Speichers
        /// </summary>
        public Efficiency Efficiency { get; private set; } = new Efficiency(new List<(float, float)>() { (0.04F, 0.8725F), (0.10F, 0.929F), (0.20F, 0.951F), (0.5F, 0.958F), (1.0F, 0.955F) });//new Efficiency(.98, .92, .7);
        public enum SubTypes { BATTERY, PUMPED_WATER }
        public readonly SubTypes SubType;

        public List<(DateTime TimeStamp, float Level)> StateOfCharge { get; private set; } = new List<(DateTime TimeStamp, float Level)>();

        #region Constructors
        public Plant_Storage() { }

        public Plant_Storage(BalancingGroup bg, float powerMW, Location location, float capacity, SubTypes subType = SubTypes.BATTERY, int scenarioID = 0, TradingStrategyType tradingStrategy = null) : base(bg, subType, powerMW, location, scenarioID: scenarioID, tradingStrategy == null ? TradingStrategyType.Arbitrage : tradingStrategy ) 
        {
            this.Capacity = capacity;
            this.SubType = subType;
            this.Schedule.PowerSet += SetSoC;
        }
        #endregion

        /*public Plant_Storage(BalancingGroup bg, SubTypes subType, StorageAttributes attributes, int scenarioID = 0, TradingStrategyType tradingStrategy = null) : base(bg, subType.ToString(), attributes, scenarioID: scenarioID, tradingStrategy == null ? TradingStrategyType.Arbitrage : tradingStrategy)
        {
            this.SubType = subType;
            this.Schedule.PowerSet += SetSoC;
        }*/

        #region CalcSchedule Override
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
             if (Schedule.Count() == 0 || !(Schedule.First().TimeStamp.Ticks <= startTime.Ticks) || !(Schedule.Last().TimeStamp.Ticks >= startTime.Add(duration).AddMinutes(-15).Ticks))
            {
                this.Schedule.Extend(startTime, duration);
                this.ControlReserveSchedule.Extend(startTime, duration);
            }
        }
        #endregion

        #region Public Functions
        public override void InitializeSchedule()
        {
            base.InitializeSchedule();
            this.Schedule.PowerSet += SetSoC;
        }
        #endregion

        #region Private Functions
        private void SetSoC(DateTime startOfProcurement, DateTime endOfProcurement)
        {
            CalcStateOfCharge(startOfProcurement, endOfProcurement);
        }
        private void CalcStateOfCharge(DateTime startOfProcurement, DateTime endOfProcurement)
        {
            var ts = this.Schedule.Get(startOfProcurement, (endOfProcurement.AddMinutes(15) - startOfProcurement)).Select(el => (el.TimeStamp, el.Power)).ToList();
            float lastLevel = Math.Max(MinSoC, StateOfCharge.At(startOfProcurement).Item2);

            var soc = new List<(DateTime, float)>();
            for (int i = 0; i < ts.Count; i++)
            {
                float level = lastLevel - (ts[i].Power / 4);
                soc.Add((ts[i].TimeStamp, level));
                lastLevel = level;
            }

            this.StateOfCharge = this.StateOfCharge.Merge(TimeSeries.Method.Last,soc);
        }
        #endregion
    }
}

