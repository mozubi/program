﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Simulation;
using winMoz.Trading.Strategies;
using winMoz.Data.Access.DTO;

namespace winMoz.Assets.Plants
{
    /// <summary>
    /// Klasse zur Definition des Kraftwerkstyps Windkraftanlage-Onshore - Erbt Eigenschaften der abstrakten Klasse Asset
    /// --
    /// Datengrundlage für Windkraftanlagen: 
    /// Engelhorn & Müskens 2018 - How to estimate wind-turbine infeed with incomplete stock data [...], BTU Cottbus
    /// </summary>
    public class Plant_Wind : Asset
    {
        /// <summary>
        /// Rauigkeitslänge in m
        /// </summary>
        public float Roughness { get; private set; }
        /// <summary>
        /// Rotornabenhöhe in m
        /// </summary>
        public float HubHeight { get; private set; }
        /// <summary>
        /// Rotordurchmesser in m
        /// </summary>
        public float RotorDiameter { get; private set; }
        /// <summary>
        /// Nennleistung in Kilowatt
        /// </summary>
        public float PowerKW { get => PowerMW * 1000; }
        /// <summary>
        /// Windkraftanlagen-Modellname
        /// </summary>
        public string ModelName { get; private set; }
        /// <summary>
        /// Windkraftanlagen-ModellId
        /// </summary>
        public int ModelId { get; private set; }
        /// <summary>
        /// Höhe der Anlagenlokation [m üNN.]
        /// </summary>
        public float LocationHeight { get; private set; } = 0;
        /// <summary>
        /// Leistungskennlinie der Windkraftanlage aus Datenbank
        /// </summary>
        public WindPowerCurve PowerCurve { get; private set; }

        /// <summary>
        /// Unterscheidung nach aggregierten Anlagen und einfachen, weil Leistungskurven und Daten in unterschiedlichen Tabellen gespeichert sind.
        /// </summary>
        public bool IsAggregated { get; private set; }
        public enum SubTypes { wind_onshore_central, wind_onshore_south, wind_onshore_north, wind_offshore_mid, wind_offshore_short, wind_offshore_far }
        public readonly SubTypes SubType;

        /// <summary>
        /// Konstruktor für Klasse Windkraft-Onshore mit Übergabe der BalancingGroup-Id
        /// </summary>
        /// <param name="BkId">Id des zugehörigen BalancingGroupss</param>
        public Plant_Wind(IBalancingGroup bg, float powerMW, WindAggDto dto, Plant_Wind.SubTypes subType, int scenarioID, TradingStrategyType tradingStrategy) : base(bg, EconomicAttributes.Load(subType.ToString()), Location.FromPostcode((int)dto.Postcode), powerMW, scenarioID, tradingStrategy)
        {
            this.HubHeight = dto.HubHeight;
            this.LocationHeight = dto.LocationHeight;
            this.ModelName = dto.WECmodel;
            this.ModelId = (int)dto.WECmodelId;
            this.RotorDiameter = dto.RotorDiameter;
            this.PowerCurve = dto.PowerCurve;
            this.Roughness = dto.Roughness;
            this.IsAggregated = true;
        }


        private Plant_Wind()
        {

        }

        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            if (Schedule.Count() == 0 || !(Schedule.First().TimeStamp.Ticks <= startTime.Ticks) || !(Schedule.Last().TimeStamp.Ticks >= startTime.Add(duration).AddMinutes(-15).Ticks))
            {
                this.Schedule.Extend(startTime, duration);
            }
            if (duration <= TimeSpan.FromDays(1))
            {
                var forecast = GetForecast(startTime, duration);
                this.Schedule.SetPower(forecast);
                this.ControlReserveSchedule.Extend(startTime, duration);
            }

        }

        #region Fahrplanmethoden
        /// <summary>
        /// Methode berechnet die DayAhead-Erzeugungsprognose für die Kraftwerksinstanz auf Basis der Anlagenparameter, Zeitpunkt und Wetterdaten
        /// </summary>
        /// <param name="startTime">Zeitpunkt der Simulation</param>
        private List<(DateTime TimeStamp, float Power)> GetForecast(DateTime startTime, TimeSpan duration)
        {
            // Day-Ahead Erzeugungsprognose für die Windkraftanlage
            var forecast = new List<(DateTime TimeStamp, float Power)>();

            var windAtHubHeight = CalcWindAtHubHeight(Weather.GetWindspeedForecast(startTime, duration, this.BalancingGroup, this.Location.NUTS2).Select(el => el.WindSpeed10m).ToList());

            for (DateTime t = startTime; t.CompareTo((startTime + duration)) < 0; t = t.AddMinutes(15))
            {
                float windSpeed = (float)Math.Round(windAtHubHeight[t.GetQuarterHourSinceDate(startTime) - 1], 1);
                float power = this.PowerCurve.GetPowerOutput(windSpeed, this.PowerMW);

                forecast.Add((t, power));
            }
            return forecast;
        }

        public void SetPowerCurve(IList<float> power, bool isScaleIntegerated = false)
           => PowerCurve = new WindPowerCurve(power, isScaleIntegerated);

        /// <summary>
        /// Methode berechnet Windgeschwindigkeit in definierter Höhe in Abhängigkeit der Rauhigkeit und der Windgeschwindigkeit in 10m
        /// </summary>
        /// <param name="wind10m">Liste(float) der Windgeschwindigkeit in 10m Höhe</param>
        /// <returns>Liste(float) der Windgeschwindigkeit in Nabenhöhe</returns>
        private List<float> CalcWindAtHubHeight(List<float> wind10m)
        {
            var windAtHH = new List<float>();

            for (int i = 0; i < wind10m.Count; i++)
            {
                float wind = (float) (wind10m[i] *
                    (Math.Log((this.HubHeight / this.Roughness), Math.E)
                    / Math.Log((10.0 / this.Roughness), Math.E)));

                windAtHH.Add(wind);
            }

            return windAtHH;
        }
        #endregion
    }
}

