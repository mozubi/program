﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets;
using winMoz.Information;
using winMoz.Trading.Strategies;
using winMoz.Agents;

namespace winMoz.Assets.Plants
{
    /// <summary>
    /// Klasse zur Modellierung von Laufwasserkraftwerken. Laufwasserkraftwerke werden nur über Zeitreihen der vergangenen Produktion modelliert.
    /// </summary>
    public class Plant_Hydro : Asset
    {
        public enum SubTypes { HYDRO }
        public readonly SubTypes SubType;
        public bool IsEEG { get; private set; }
        private Plant_Hydro() { }
       
        public Plant_Hydro(BalancingGroup bg, Location location, float powerMW, bool isEEG, int scenarioID = 0, TradingStrategyType tradingStrategy = null) : base(bg, SubTypes.HYDRO, powerMW, location, scenarioID: scenarioID, tradingStrategy is null ? TradingStrategyType.HedgeVRE : tradingStrategy)
        {
            this.IsEEG = isEEG;
        }
        /// <summary>
        /// Funktion errechnet den Fahrplan für ein Laufwasserkraftwerk. Es wird nur ein globales Kraftwerk betrachtet, das die Produktion für ganz Deutschland modelliert.
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            if (Schedule.Count() == 0 || !(Schedule.First().TimeStamp.Ticks <= startTime.Ticks) || !(Schedule.Last().TimeStamp.Ticks >= startTime.Add(duration).AddMinutes(-15).Ticks))
            {
                this.Schedule.Extend(startTime, duration);
                this.ControlReserveSchedule.Extend(startTime, duration);
            }
            if (duration <= TimeSpan.FromDays(1))
            {
                var forecast = Weather.GetHydroForecast(startTime, duration, this.BalancingGroup);

                this.Schedule.SetPower(forecast.Select(hydro => (hydro.TimeStamp, hydro.Run_of_river * this.PowerMW)).ToList());

            }
        }
    }
}
