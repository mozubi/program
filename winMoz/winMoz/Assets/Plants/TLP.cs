﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Simulation;

namespace winMoz.Assets.Plants
{

    /// <summary>
    /// Thermisches Last-/Bedarsprofil Klasse <see cref="TLP"/> basierend auf Gas-SLP für thermische Kraftwerke/Portfolien
    /// </summary>
    /// <remarks>    
    /// https://www.bdew.de/media/documents/nach_Leitfaden_SLP.pdf
    /// </remarks>
    public class TLP 
    {
        /// <summary>
        /// Maximale Leistung des Lastprofils in MW
        /// </summary>
        private float PowerMWTLPmax;
        /// <summary>
        /// Kundenwert (Jahreswert) in MWh
        /// </summary>
        public float KW { get; private set; }
        /// <summary>
        /// Angeschlossene thermische Leistung in MW
        /// </summary>
        public float PowerMWTLP { get; private set; }
        /// <summary>
        /// Jahresvolllaststunden in h
        /// </summary>
        public int FullLoadHours { get; private set; }
        public IBalancingGroup BalancingGroup => Asset.BalancingGroup;
        public IAsset Asset { get; private set; }
        public Schedule Schedule { get; private set; }
        public Location Location { get; private set; }

        private TLP() { }

        /// <summary>
        /// Konstruktor für Thermisches Lastprofil <seealso cref="TLP"/>
        /// </summary>
        /// <param name="location">Lokalisierung</param>
        /// <param name="powerMWTLPmax">Angeschlossene Thermische Leistung in MW</param>
        /// <param name="FullLoadHours">Jahresvolllaststunden in h</param>
        private TLP(float PlantPowerMW, float powerMWTLPmax, int FullLoadHours, Location location)
        {
            this.PowerMWTLP = powerMWTLPmax;
            this.PowerMWTLPmax = powerMWTLPmax;
            this.FullLoadHours = FullLoadHours;
            this.Location = location;
            this.Schedule = new Schedule();
        }

        public TLP(float PlantPowerMW, float powerMWTLPmax, int FullLoadHours, IAsset asset) : this(PlantPowerMW, powerMWTLPmax, FullLoadHours, asset.Location)
        {
            this.Asset = asset;
            this.Schedule = new Schedule();
        }

        public TLP(float PowerMWTLPmax, int FullLoadHours, IAsset asset) : this(PlantPowerMW: PowerMWTLPmax * (float)FullLoadHours / 8760.0F, PowerMWTLPmax, FullLoadHours, asset.Location)
        {
            this.Asset = asset;
            this.Schedule = new Schedule();
        }


        #region Wochentagsfaktoren

        /// <summary>
        /// Methode gibt Wochentagsfaktor für Gas-SLP für bestimmtes Datum zurück
        /// </summary>
        /// <param name="TimeStamp">Datum, für das der Wochentagsfaktor bestimmt werden soll</param>
        /// <returns>Wochentagsfaktor Fwd als float</returns>
        private float GetFwd(DateTime TimeStamp)
        {
            float fMon = 1.03F;
            float fTue = 1.03F;
            float fWed = 1.02F;
            float fThu = 1.03F;
            float fFri = 1.01F;
            float fSat = 0.93F;
            float fSun = 0.94F;

            switch (TimeStamp.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return fSun;
                case DayOfWeek.Monday:
                    return fMon;
                case DayOfWeek.Tuesday:
                    return fTue;
                case DayOfWeek.Wednesday:
                    return fWed;
                case DayOfWeek.Thursday:
                    return fThu;
                case DayOfWeek.Friday:
                    return fFri;
                case DayOfWeek.Saturday:
                    return fSat;
                default:
                    return 0;
            }
        }
        #endregion

        #region SLP-Berechnungsmethoden
        /// <summary>
        /// Methode berechnet den thermischen Bedarfsfahrplan ab Startzeit für gewählte Dauer nach BDEW-Gas-SLP-Verfahren
        /// </summary>
        /// <param name="timeStamp">Startzeit als DateTime</param>
        /// <param name="duration">Dauer als TimeSpan</param>
        /// <returns>Thermischer Fahrplan (15min)</returns>
        public List<ScheduleItem> GetOrSetSchedule(DateTime timeStamp, TimeSpan duration)
        {
            if (!this.Schedule.IsValid(timeStamp, duration))
            {
                List<(DateTime TimeStamp, float demand)> demand = GetDemand(timeStamp, duration);
                this.Schedule.AddPower(demand.Select(el => (el.TimeStamp, el.demand)).ToList());
            }
            return this.Schedule.Get(timeStamp, duration);
        }

        /// <summary>
        /// Methode berechnet den thermischen Bedarfsfahrplan für Thermisches Lastprofil mit gegebenen Kundenwerten und gibt diesen als Fahrplan zurück
        /// </summary>
        /// <param name="timeStamp">Startzeitpunkt, für den der Fahrplan berechnet werden soll als <seealso cref="DateTime"/></param>
        /// <param name="duration">Dauer, für die der Fahrplan berechnet werden soll als <seealso cref="TimeSpan"/></param>
        /// <returns><see cref="Schedule"/> Fahrplan</returns>
        public List<(DateTime timeStamp, float demand)> GetDemand(DateTime timeStamp, TimeSpan duration)
        {
            var demand = new List<(DateTime timeStamp, float demand)>();

            if (this.KW == 0 && this.PowerMWTLP != 0) SetKW();

            float dailyDemand = 0;
            float quarterHourlyDemand = 0;

            var tempYear = Weather.GetTemperatureForecast(timeStamp, duration, this.BalancingGroup, this.Location.NUTS2);
            var qhFactor = new List<float>();

            for (DateTime t = timeStamp; t.CompareTo(timeStamp + duration) < 0; t = t.AddMinutes(15))
            {
                if ((t.AddMinutes(-15)).DayOfYear - t.DayOfYear != 0 || t == timeStamp)
                {
                    dailyDemand = CalcDailyDemand(t, tempYear);
                    var tEndDay = t.AddHours(24);
                    var qhFactor1 = GasSLPDailyFactors.GetGasSLPDailyFactors(t,
                       GetSLPTemperature(tempYear.Where(temp => temp.TimeStamp >= t && temp.TimeStamp < tEndDay).ToList()));
                    qhFactor = qhFactor1.ResampleLinear(96).Select(el => el / 4).ToList();
                }

                quarterHourlyDemand = Math.Min(PowerMWTLPmax, dailyDemand * qhFactor[t.GetQuarterHourOfDay() - 1]);
                demand.Add((timeStamp: t, quarterHourlyDemand));
            }

            return demand;
        }


        /// <summary>
        /// Methode berechnet die tägliche benötigte Wärmeenergie in Abhängigkeit der des Wochentags und der Umgebungstemperatur
        /// mithilfe der Sigmoid-Funktion und der angeschlossenen Leistung
        /// </summary>
        /// <param name="timeStamp">Zeitstempel zur Berechnung des Wärmebedarfs</param>
        /// <returns>Wärmebedarf in MWh</returns>
        private float CalcDailyDemand(DateTime timeStamp, List<(DateTime TimeStamp, float Temperature)> wth)
        {
            return this.KW * HSum.GetSigmoid(GetTallocated(timeStamp, wth)) * GetFwd(timeStamp);
        }

        /// <summary>
        /// Methode berechnet allozierte Temperatur aus für gewähltes Datum
        /// </summary>
        /// <param name="timeStamp">Datum als DateTime</param>
        /// <returns>Allozierte Temperatur</returns>
        private float GetTallocated(DateTime timeStamp, List<(DateTime TimeStamp, float Temperature)> wth)
        {
            float tD = GetTemperature(timeStamp, wth);
            float tD1 = GetTemperature(timeStamp.AddDays(-1), wth);
            float tD2 = GetTemperature(timeStamp.AddDays(-2), wth);
            float tD3 = GetTemperature(timeStamp.AddDays(-3), wth);

            return (tD + 0.5F * tD1 + 0.25F * tD2 + 0.125F * tD3) / (1 + 0.5F + 0.25F + 0.125F);
        }

        /// <summary>
        /// Methode berechnet Durchschnittstemperatur für ein gewähltes Datum und Temperaturverlauf
        /// </summary>
        /// <param name="timeStamp">Datum zur Berechnung der Durchschnittstemperatur</param>
        /// <param name="weatherForecast">Wettervorhersage/Temperaturverlauf</param>
        /// <returns>Durchschnittstemperatur des gew. Datums</returns>
        private float GetTemperature(DateTime timeStamp, List<(DateTime TimeStamp, float Temperature)> weatherForecast)
            => weatherForecast.SingleOrDefault(el => el.TimeStamp == timeStamp).Temperature;

        /// <summary>
        /// Methode bestimmt Temperaturbereich für ein bestimmtes Datum
        /// </summary>
        /// <param name="timeStamp">Datum als DateTime</param>
        /// <returns>Temperaturzone für SLP-Berechnung</returns>
        private int GetSLPTemperature(DateTime timeStamp)
        {
            float temp = Weather.GetTemperatureForecast(timeStamp, new TimeSpan(24, 0, 0), this.BalancingGroup, this.Location.NUTS2).ToList().Select(el => el.Temperature).Average();

            if (temp > 20) return 25;
            if (temp > 15) return 20;
            if (temp > 10) return 15;
            if (temp > 5) return 10;
            if (temp > 0) return 5;
            if (temp > -5) return 0;
            if (temp <= -5) return -5;

            throw new ArgumentOutOfRangeException("Fehlerhafte Temperatur");
        }

        /// <summary>
        /// Methode bestimmt Temperaturbereich für ein bestimmtes Datum
        /// </summary>
        /// <param name="timeStamp">Datum als DateTime</param>
        /// <returns>Temperaturzone für SLP-Berechnung</returns>
        private int GetSLPTemperature(List<(DateTime TimeStamp, float Temperature)> temperatureForecast)
        {
            float temp = temperatureForecast.Select(el => el.Temperature).Average();

            if (temp > 20) return 25;
            if (temp > 15) return 20;
            if (temp > 10) return 15;
            if (temp > 5) return 10;
            if (temp > 0) return 5;
            if (temp > -5) return 0;
            if (temp <= -5) return -5;

            throw new ArgumentOutOfRangeException("Fehlerhafte Temperatur");
        }

        /// <summary>
        /// Methode berechnet und setzt den Kundenwert (Jahresenergiebedarf)
        /// </summary>
        public void SetKW()
        {
            float hSum = HSum.GetHSum(Simulation.Simulator.Times.Start, this.Location.NUTS2);

            this.KW = (this.PowerMWTLP * this.FullLoadHours) / hSum;
        }
        #endregion
    }
}
