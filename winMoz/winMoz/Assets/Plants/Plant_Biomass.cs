﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets;
using winMoz.Agents;
using winMoz.Trading.Strategies;
using winMoz.Information;
using winMoz.Helper;
using winMoz.Simulation;

namespace winMoz.Assets.Plants
{
    /// <summary>
    /// Die Klasse implementiert ein Biomassekraftwerk. Es besteht aus einem flexiblen und einem unflexiblen Anteil sowie einem Anteil zur Wärmeerzeugung.
    /// </summary>
    public class Plant_Biomass : Asset
    {
        public enum SubTypes { BIOMASS }
        public readonly SubTypes SubType;
        /// <summary>
        /// Thermisches Lastprofil, dass diesem Asset zugeordnet ist.
        /// </summary>
        public TLP TLP { get; private set; }
        /// <summary>
        /// Wärmeerzeugung
        /// </summary>
        public float HeatVariablePower { get; private set; }
        /// <summary>
        /// Flexibple Leistung
        /// </summary>
        public float FlexiblePower { get; private set; }
        /// <summary>
        /// Unflexible Leistung
        /// </summary>
        public float InflexiblePower => PowerMW - HeatVariablePower - FlexiblePower;

        private (DateTime StartDay, List<int> Hours) HoursFlexiblePower;

        #region Constructors
        private Plant_Biomass() { }
        public Plant_Biomass(IBalancingGroup bg, float powerMW, float heatVariablePower, float flexiblePower, Location location, Plant_Biomass.SubTypes subType, TradingStrategyType tradingStrategy, int scenarioID = default) : this(bg, powerMW, heatVariablePower, flexiblePower, location, subType.ToString(), tradingStrategy, scenarioID) { }

        public Plant_Biomass(IBalancingGroup bg, float powerMW, float heatVariablePower, float flexiblePower, Location location, string subType, TradingStrategyType tradingStrategy, int scenarioID = default) : base(bg, EconomicAttributes.Load(subType), location,powerMW,scenarioID, tradingStrategy)
        {
            HeatVariablePower = heatVariablePower;
            FlexiblePower = flexiblePower;
            SetTLP();
        }
        #endregion

        #region Static Constructors
        public static Plant_Biomass FromRelativePower(IBalancingGroup bg, float powerMW, float heatVariablePowerRel, float flexiblePowerRel, Location location, Plant_Biomass.SubTypes subType, TradingStrategyType tradingStrategy, int scenarioID = default)
           => new Plant_Biomass(bg, powerMW, heatVariablePowerRel * powerMW, flexiblePowerRel * powerMW, location, subType, tradingStrategy, scenarioID);
        #endregion

        #region CalcSchedule Override
        /// <summary>
        /// Die Funktion errechnet den Fahrplan für einen gegebenen Zeitraum. 
        /// </summary>
        /// <param name="startTime"> Zeitpunkt des ersten Fahrplaneintags, der berechnet wird. </param>
        /// <param name="duration"> Zeitraum über welchen der Fahrplan berechnet wird.</param>
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            if (Schedule.Count() == 0 || !(Schedule.First().TimeStamp.Ticks <= startTime.Ticks) || !(Schedule.Last().TimeStamp.Ticks >= startTime.Add(duration).AddMinutes(-15).Ticks))
            {
                this.Schedule.Extend(startTime, duration);
                this.ControlReserveSchedule.Extend(startTime, duration);
            }
            if (duration <= TimeSpan.FromDays(1) && startTime.Add(duration).Ticks > Simulator.Times.SimTime.Ticks)
            {
                var forecast = GetForecast(startTime, duration);
                this.Schedule.SetPower(forecast);
            }
        }
        #endregion

        #region Public Functions
        public void SetTLP()
        {
            var maxThermalHeatVariablePower = this.HeatVariablePower * 0.5F;
            this.TLP = new TLP(maxThermalHeatVariablePower * 0.5F, maxThermalHeatVariablePower, 4000, this); //Effizeinz von thermischer in elektrische Leistung (Testannahme)
        }
        public List<(DateTime TimeStamp, float Power)> GetForecast(DateTime startTime, TimeSpan duration)
        {
            if (this.TLP == null)
                SetTLP();
            SetFlexiblePriceHours(startTime);
            var heatVariablePower = duration.TotalHours > 1 ? TLP.GetDemand(startTime, duration).Select(item => (item.timeStamp, item.demand / 0.5F)).ToList().Resample(Resampling.TimeStep.Hour).ToList() : TLP.GetDemand(startTime, duration).Select(item => (item.timeStamp, item.demand / 0.5F)).ToList().Resample(Resampling.TimeStep.QuarterHour).ToList(); //0.5 Wirkungsgrad thermisch zu elektrisch und geboten wird unlimitiert
            var schedule = heatVariablePower.Select(item => (item.TimeStamp, item.Item2 + (HoursFlexiblePower.Hours.Contains(item.TimeStamp.Hour) ? FlexiblePower + InflexiblePower : InflexiblePower))).ToList();

            return schedule.Resample(Resampling.TimeStep.QuarterHour, Resampling.Method.FFill).ToList();
        }
        #endregion

        #region Private Functions
        private void SetFlexiblePriceHours(DateTime time)
        {
            if (HoursFlexiblePower.StartDay != default || HoursFlexiblePower.StartDay != new DateTime(time.Year, time.Month, time.Day))
            {
                var PricePrediction = Info.HPFC.Get(new DateTime(time.Year, time.Month, time.Day), TimeSpan.FromDays(1), this.BalancingGroup).ToList().Resample(Resampling.TimeStep.Hour).ToList();
                HoursFlexiblePower = (new DateTime(time.Year, time.Month, time.Day), PricePrediction.OrderByDescending(item => item.Item2).Select(item => item.TimeStamp).Take(10).Select(item => item.Hour).ToList());
            }
        }
        #endregion
    }
}
