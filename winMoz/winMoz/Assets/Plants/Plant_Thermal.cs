﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Helper;
using winMoz.Portfolios.Optimization;
using winMoz.Trading.Strategies;
using winMoz.Data.Access.Repositories;
using winMoz.Trading.Optimization;
using winMoz.Simulation;

namespace winMoz.Assets.Plants
{
    /// <summary>
    /// Abgeleitete Klasse zur Definition des Kraftwerkstyps Thermisch - Erbt Eigenschaften der <seealso langword="abstract"/> Klasse <seealso cref="Asset"/>
    /// </summary>
    public class Plant_Thermal : Asset
    {
        //public AgeTypes AgeType => Attributes.AgeType;
        public enum SubTypes { GAS, OIL, COAL, LIGNITE, NUCLEAR, SEWAGELANDFILLGAS, BIOGAS }
        public readonly SubTypes SubType;
        public bool IsIndustry => false;

        public enum AgeTypes
        {
            DEFAULT, HARD_COAL_OLD1, HARD_COAL_OLD2, HARD_COAL_NEW, HARD_COAL_CCS, LIGNITE_OLD1, LIGNITE_OLD2, LIGNITE_NEW, LIGNITE_CCS, GAS_CONV_OLD1,
            GAS_CONV_OLD2, GAS_GT_OLD1, GAS_GT_OLD2, GAS_GT_NEW, GAS_GT_CCS, OCGT_OLD, OCGT_NEW, OIL_LIGHT, OIL_HEAVY_OLD1, OIL_HEAVY_OLD2, OIL_HEAVY_NEW, OLD, NEW
        }

        public static string GetStringFromAgeType(AgeTypes type)
        {
            switch (type)
            {
                case AgeTypes.OCGT_NEW: return "OCGT new";
                case AgeTypes.OCGT_OLD: return "OCGT old";
                case AgeTypes.NEW: return "new";
                case AgeTypes.OLD: return "old";
                case AgeTypes.DEFAULT: return "-";
                default: return Convert.ToString(type);
            }
        }

        public static AgeTypes GetAgeTypeFromString(string ageTypeString)
        {
            try
            {
                var type = (AgeTypes)Enum.Parse(typeof(AgeTypes), ageTypeString);
                return type;
            }
            catch (Exception ex)
            {
                switch (ageTypeString)
                {
                    case "OCGT new": return AgeTypes.OCGT_NEW;
                    case "OCGT old": return AgeTypes.OCGT_OLD;
                    case "new": return AgeTypes.NEW;
                    case "old": return AgeTypes.OLD;
                    default: return AgeTypes.DEFAULT;
                }
            }
        }
        public List<(DateTime TimeStamp, float Value)> CleanSpread(Plant_Thermal plant, DateTime TimeStamp, TimeSpan duration, bool hourly = false, bool monthly = false)
        {
            
                throw new NotImplementedException("Dummy Function so that the optimizers compile. Real version is found in thermal attributes");
           
        }

        /// <summary>
        /// Spitzenleistung des thermischen Bedarfs in MW
        /// </summary>
        public float P_TLP { get; private set; }

        public TLP TLP { get; private set; }
        public List<Block> Units { get; private set; } = new List<Block>();
        //private List<(DateTime TimeStamp, int[] Revision)> revisonSchedule;
        private Dictionary<int, List<(DateTime startRev, DateTime endRev)>> revisionSchedule => this.Units.Select(unit => (unit.Id, unit.revisionSchedule)).ToDictionary(sched => sched.Id, sched => sched.revisionSchedule);

        #region Konstruktor
        private Plant_Thermal() {}
        public Plant_Thermal(IBalancingGroup bg, Enum subType, float powerMW,  Location location = default, float P_TLP = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null) : base(bg, subType, powerMW, location, scenarioID, tradingStrategy)
        {
            this.P_TLP = P_TLP;
            this.SubType = (SubTypes)subType;
            SetTLP();

        }
        public Plant_Thermal(IBalancingGroup bg, Enum subType, float powerMW, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null): base(bg, subType, powerMW, locationPostcode, scenarioID, tradingStrategy)
        {
            this.P_TLP = P_TLP;
            this.SubType = (SubTypes)subType;
            SetTLP();
        }

        #endregion

        #region Methoden zur Fahrplanberechnung
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            if (Units.First().Schedule.Count() == 0 || !(Units.First().Schedule.First().TimeStamp.Ticks <= startTime.Ticks) || !(Units.First().Schedule.Last().TimeStamp.Ticks >= startTime.Add(duration).AddMinutes(-15).Ticks))
            {
                Schedule.Extend(startTime, duration);
                this.ControlReserveSchedule.Extend(startTime, duration);

                Units.ForEach(unit => unit.Schedule.Extend(startTime, duration));
                Units.ForEach(unit => unit.ControlReserveSchedule.Extend(startTime, duration));
            }

            //TODO: Dies sollte auch in die Optimierungsfunktion
            if (duration.Days > 1 && !this.IsIndustry)
            {
                CommitUnitsLongterm2(startTime, duration);
                return;
            }
            else
            {

                var newPower = this.Schedule.Get(startTime, duration).ToList().Select(el => ScheduleItem.Dummy(el)).ToList().Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                this.Schedule.SetPower(newPower);
                this.Units.Where(item => item is Block_Kond || (Simulator.Times.SimTime != default && startTime.Ticks < Simulator.Times.SimTime.Ticks)).ToList().ForEach(unit =>
                {
                    var newPowerUnit = unit.Schedule.Get(startTime, duration).ToList().Select(el => ScheduleItem.Dummy(el)).ToList().Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                    unit.Schedule.SetPower(newPowerUnit);

                });

            }

            //if (duration.Days <= 1)
            //    Optimization.DayAhead(this, startTime, duration);
        }

        public override void CutScheduleUntil(DateTime timeUntil)
        {
            base.CutScheduleUntil(timeUntil);
            this.Units.ForEach(unit =>
            {
                unit.CutScheduleUntilFromThermal(timeUntil);
            });
        }

        public override void CutControlReserveScheduleUntil(DateTime timeUntil)
        {
            base.CutControlReserveScheduleUntil(timeUntil);
            this.Units.ForEach(unit =>
            {
                unit.CutControlReserveScheduleUntilFromThermal(timeUntil);
            });
        }

        private void CommitUnitsLongterm2(DateTime startTime, TimeSpan duration)
        {
            if (!(this.TLP is null))
            {
                PlanThermalProduction(startTime, duration);
            }
            PlanElectricProduction(startTime, duration);
                         
            
                // 

        }
        private void PlanThermalProduction(DateTime startTime, TimeSpan duration)
        {
            // thermischen Bedarf des Kraftwerks bestimmen
            var thermalDemand = TLP.GetDemand(startTime, duration);
            // Blöcke nach thermischen Produktionskosten sortieren
            Dictionary<Block_CHP, float> costPerUnit = new Dictionary<Block_CHP, float>();
            foreach (Block_CHP unit in Units.OfType<Block_CHP>())
            {
                // eine Stunde reicht, zukünftige Preise werden mit dem aktuellen Preis für die jeweilige Commodity angenommen
                if (unit is Block_CHP)
                {
                    costPerUnit[unit] = unit.ProductionCostsPerMWh_th(startTime, TimeSpan.FromHours(1)).First().Item2;
                }
            }
            IEnumerable<Block_CHP> sortedCost = from entry in costPerUnit orderby entry.Value ascending select entry.Key;

            // Welcher Bedarf kann durch wie viel Blöcke gedeckt werden? 
            foreach (Block_CHP unit in sortedCost)
            {
                // Die Nachfrage ist gegeben in MWh augelöst auf 15 min, deshalb Leistung / 4 für die Energie
                // Produziere entweder Demand oder Was geht; Wenn demand nicht gedeckt werden kann mit minimaler KW Leistung dann produziere gar nicht. 
                var producedEnergy = thermalDemand.Select(el => (el.timeStamp, el.demand >= unit.P_th_min ? Math.Min(el.demand, unit.P_th_Kwk / 4) : 0)).ToList();
                thermalDemand = thermalDemand.Zip(producedEnergy, (demand, produced) => (demand.timeStamp, demand.demand - produced.Item2)).ToList();

                // save Power as electrical Power
                // TODO: Check if the cost is calculated correctly; cost calculation is problematic cause it considers only thermal cost and only at constant efficiency
                unit.Schedule.AddPower(producedEnergy.Select(el => (el.timeStamp, unit.ThermalToElectrical(el.Item2))).ToList());
            }
        }
        
        private void PlanElectricProduction(DateTime startTime, TimeSpan duration)
        {
            foreach (Block unit in Units)
            {
                if (!(unit is Block_CHP))
                {
                    List<(DateTime TimeStamp, float Cost)> cost = unit.ProductionCostsPerMWh_el(startTime, duration).ToList().Resample(Resampling.TimeStep.QuarterHour).ToList();
                    unit.Schedule.AddPower(cost.Select(el => (el.TimeStamp, unit.PowerMW)).ToList());
                }
            }
        }
        #endregion

      

        public Plant_Thermal CreateUnits(List<Block> units)
        {
            units.ForEach(unit => unit.SetPlant(this));

            this.Units = new List<Block>(units);
            return this;
        }
        
        internal void SetTLP()
        {
            if (this.P_TLP != 0)
                // zwei mal derselbe Input, da P_TLP einmal die maximale Leistung ist und einmal verwendet wird um die Vollasstunden zu berechnen
                // TODO: Konstruktor auf nur zwei Arugmente festlegen
                this.TLP = new TLP(this.P_TLP, this.P_TLP, (int)this.Economics.FullLoadHours, this);
        }
    }
}

