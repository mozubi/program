﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Helper;
using winMoz.Information;
using winMoz.Trading.Strategies;

namespace winMoz.Assets.Plants
{
    /// <summary>
    /// Klasse zur Definition des Kraftwerkstyps Photovoltaik - Erbt Eigenschaften der abstrakten Klasse Asset
    /// </summary>
    public class Plant_PV : Asset
    {
        public enum SubTypes { PV_ROOF, PV_AREA }
        public readonly SubTypes SubType;

        /// <summary>
        /// Modulneigung in Grad
        /// </summary>
        public float ModuleAngle { get; private set; }
        /// <summary>
        /// Modulazimut in Grad
        /// </summary>
        public float ModuleAzimut { get; private set; }
        /// <summary>
        /// Relativer Auslastungsfaktor des Wechselrichters ( Pdc/Pnenn )
        /// </summary>
        public float LoadFactor { get; private set; }

        public enum AzimutType { South, SouthWest, SouthEast, West, East, NorthWest, NorthEast, North }
        
        public float GetAzimutFromAzimutType(AzimutType azimutType)
        {
            switch (azimutType)
            {
                case AzimutType.North: return 0;
                case AzimutType.SouthWest: return 225;
                case AzimutType.SouthEast: return 135;
                case AzimutType.West: return 270;
                case AzimutType.East: return 90;
                case AzimutType.NorthWest: return 315;
                case AzimutType.NorthEast: return 45;
                default: return 180;
            }
        }
        #region Constructors
        public Plant_PV(BalancingGroup bg, float powerMW, Location location, float ModuleAngle, float ModuleAzimut, float LoadFactor, SubTypes subType = SubTypes.PV_ROOF, int scenarioID = 0, TradingStrategyType tradingStrategy = null) : base(bg,  subType,  powerMW, location, scenarioID: scenarioID, tradingStrategy is null ? TradingStrategyType.HedgeVRE : tradingStrategy)
        {
            this.SubType = subType;
            this.ModuleAngle = ModuleAngle;
            this.ModuleAzimut = ModuleAzimut;
            this.LoadFactor = LoadFactor;
        }
        

        private Plant_PV() { }
        #endregion
        #region CalcSchedule Override
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            if (Schedule.Count() == 0 || !(Schedule.First().TimeStamp.Ticks <= startTime.Ticks) || !(Schedule.Last().TimeStamp.Ticks >= startTime.Add(duration).AddMinutes(-15).Ticks))
            {
                this.Schedule.Extend(startTime, duration);
                this.ControlReserveSchedule.Extend(startTime, duration);

            }
            if (duration <= TimeSpan.FromDays(1))
            {
                var forecast = GetForecast(startTime, duration);
                this.Schedule.SetPower(forecast);
            }
        }
        #endregion
        #region Public Functions
        /// <summary>
        /// Methode berechnet die DayAhead-Erzeugungsprognose für die Kraftwerksinstanz
        /// auf Basis der Anlagenparameter, Zeitpunkt und Wetterdaten
        /// in Anlehnung an Schubert 2012
        /// </summary>
        /// <param name="startTime">Zeitpunkt der Simulation</param>
        public List<(DateTime TimeStamp, float Power)> GetForecast(DateTime startTime, TimeSpan duration)
        {
            var forecast_DA = new List<(DateTime TimeStamp, float Power)>();

            float tModule; // Modultemperatur
            float gModule; // Globalstrahlung auf Modul
            float etaWR = CalcEtaWR(); // Wechselrichterwirkungsgrad
            // Zusätzliche Systemverluste
            // [1]-Mismatching (in Modulwinkelstreuung bereits enthalten), [2]-Reflexionen, [3]-Verschmutzungen, [4]-Gleichstromverkabelung
            float etaSys = 1 - (0.00F + 0.025F + 0.02F + 0.002F);

            // Wetterdaten für den Folgetag -> werden bei Methodenaufruf und Listengenerierung direkt aus der Datenbank geladen
            var SolarWeatherDA = Weather.GetSolarForecast(startTime, duration, this.BalancingGroup, this.Location.NUTS2);
            int idx = 0;


            for (DateTime t = startTime; t.CompareTo((startTime + duration)) < 0; t = t.AddMinutes(15))
            {
                // Berechnung der Sonnenstandsdaten (Höhe, Azimut) zum Zeitpunkt t
                (float gammaSolar, float Azimut) solarData = CalcSolarAngle(t);

                var weatherAtTime = SolarWeatherDA.ElementAt(idx);

                // Berechnung der Strahlungsanteile (Reflektion, Diffuse- und Direkte Strahlung) zum Zeitpunkt
                float enReflModule = 0.5F * 0.2F * (weatherAtTime.RadDiffHor + weatherAtTime.RadDirHor) * (1 - this.ModuleAngle.CosD()); //0.2 = Albedo
                float enDiffModule = weatherAtTime.RadDiffHor * 0.5F * (1 + this.ModuleAngle.CosD());
                float enDirModule = Math.Max(0.9F, (solarData.Azimut - this.ModuleAzimut).CosD()) * weatherAtTime.RadDirHor;

                // Berechnung Globalstrahlung und Modultemperatur
                gModule = enDirModule * CalcShadowing(solarData.gammaSolar) + enDiffModule + enReflModule;
                tModule = weatherAtTime.T_env + GetTcoeff() * gModule;

                // Berechnung Modulwirkungsgrad auf Basis der Globalstrahlung und Modultemperatur
                float etaMod = CalcEtaMod(gModule, tModule);

                // Schreiben des aktuellen Gesamt-Anlagenwirkungsgrades zur späteren Berechnung der Stromgestehungskosten
                float Efficiency = etaSys * etaMod * etaWR;

                // Berechnung der Erzeugungsleistung zum Zeitpunkt auf Basis der Einstrahlung und des Wirkungsgrades
                float power = Math.Max(0, Efficiency * ((gModule / 1000) * (this.PowerMW * 1000)) / 1000);

                if (power > this.PowerMW)
                    throw new ArgumentException("Berechnungsfehler - PV Leistung");

                forecast_DA.Add((t, power));

                idx++;
            }

            return forecast_DA;
        }
        public void SetPVParameters(float moduleAngle = 32.5F, float moduleAzimut = 180.0F, float loadFactor = 1, int assetId = 0)
        {
            this.ModuleAngle = (float)MathHelper.GetNormVerteilt(moduleAngle, 5, assetId);
            this.ModuleAzimut = (float)MathHelper.GetNormVerteilt(moduleAzimut, 35, assetId);
            this.LoadFactor = (float)MathHelper.GetNormVerteilt(loadFactor, 0.15F, assetId);
        }
        #endregion

        #region Methoden zur Berechnung der Anlageneigenwerte
        /// <summary>
        /// Methode gibt den Temperatur-Koeffizienten für verschiedene Montagearten zurück
        /// für Freiflächen- und Dachanlagen nach Drews 2007, S.554  -> Schubert S.11
        /// </summary>
        /// <returns>Temperaturkoeffizent als float</returns>
        private float GetTcoeff()
        {
            if (SubType == SubTypes.PV_ROOF)
                return 0.036F;

            if (SubType == SubTypes.PV_AREA)
                return 0.02F;
            // TemperaturCoeff 

            // Wenn PV-Typ nicht definiert
            throw new ArgumentException("Wrong PV-Type");

        }

        /// <summary>
        /// Methode berechnet den Wechselrichterwirkungsgrad nach Schubert 2012
        /// </summary>
        /// <returns></returns>
        private float CalcEtaWR()
        {
            // Koeffizienten zur Ermittlung de WR-Wirkungsgrades (nach Macedo und Zilles 2007 S.344)
            (float A, float B, float C) Kwr = (0.0079F, 0.0411F, 0.05F);

            float etaWR = (float)(0.8F - (Kwr.A + Kwr.B * this.LoadFactor + Kwr.C * Math.Pow(this.LoadFactor, 2))) / this.LoadFactor;

            return
                etaWR;
        }

        /// <summary>
        /// Methode berechnet den Modul-Wirkungsgrad auf Basis der Einstrahlung und Temperatur nach Schubert 2012
        /// </summary>
        /// <param name="Gmod">Auf das Modul treffende Globalstrahlung</param>
        /// <param name="Tmod">Am Modul herrschende Umgebungstemperatur</param>
        /// <returns>(float)Wirkungsgrad</returns>
        private float CalcEtaMod(float Gmod, float Tmod)
        {
            // Koeffizienten zur Ermittlung des rel. Modulwirkungsgrades nach Huld 2010 (S.329) -> sa. Schubert 2012
            (float A, float B, float C, float D, float E, float F) Kmod =
                    (-0.017162F, -0.040289F, -0.004681F, 0.000148F, 0.000169F, 0.000005F);

            // Wenn Globalstrahlung == 0 dann überspringe Berechnung
            if (Gmod == 0)
                return 0;

            float etaMod =(float)(
                1 + Kmod.A * Math.Log((Gmod / 1000), Math.E)
                    + Kmod.B * Math.Pow(Math.Log((Gmod / 1000), Math.E), 2)
                    + (Kmod.C + Kmod.D * Math.Log((Gmod / 1000), Math.E)
                        + Kmod.E * Math.Pow(Math.Log((Gmod / 1000), Math.E), 2))
                    * (Tmod - 25)
                    + Kmod.F * Math.Pow((Tmod - 25), 2));

            return
                Math.Max(0, etaMod);
        }

        /// <summary>
        /// Methode berechnet einen Verschattungsfaktor auf Basis des Sonnenstandes nach Schubert 2012
        /// Bis zu einer Sonnenhöhe von 17° über dem Horizont beträgt der Skaliert der Faktor von 0.7 bis 1
        /// Ab einer Höhe von >17° beträgt die Verschattung 0, der Faktor wird 1
        /// </summary>
        /// <param name="solarAngle">Sonnenhöhe in Grad</param>
        /// <returns>(float) Verschattungsfaktor</returns>
        private float CalcShadowing(float solarAngle)
        {
            if (solarAngle > 17)
            {
                return 1;
            }
            else
            {
                return
                    (float)Math.Max(0.7, 0.7 + 0.3 * (solarAngle / 17));
            }
        }

        /// <summary>
        /// Methode berechnet den Sonnenstand zu einer gegebenen Zeit an einem bestimmten Ort definiert nach Lat-Lon
        /// Berchnung erfolgt in Anlehnung an 
        /// Duffie, John A.; Beckman, William A.: Solar energy thermal processes, Quaschning, Wikipedia
        /// </summary>
        /// <param name="time">(DateTime)-Zeitpunkt für den der Sonnenstand berechnet werden soll</param>
        /// <returns>Tupel Sonnenhöhe gammaSolar und Azimut in Grad</returns>
        private (float gammaSolar, float Azimut) CalcSolarAngle(DateTime time)
        {
            //Testdaten
            //time = new DateTime(2018, 01, 02, 1, 30, 0);
            //var Latitude = 51.5146960071935;
            //var Longitude = 7.47343106952101;

            float n = time.ToJulianDate() - 2451545.0F;
            float L = (280.460F + 0.9856474F * n) % 360;
            float g = (357.528F + 0.9856003F * n) % 360;

            // Ekliptikale Länge der Sonne
            float eklLength = ((float)(L.ToRadians() + 1.915F.ToRadians() * Math.Sin(g.ToRadians()) + 0.01997F.ToRadians() * Math.Sin((2 * g.ToRadians())))).ToDegrees();
            float epsilon = 23.439F - 0.0000004F * n;

            // Deklination der Sonne
            float delta = ((float)(Math.Asin(epsilon.SinD() * eklLength.SinD()))).ToDegrees();

            // Mittlere Ortszeit (= Wahre Ortszeit)
            float MOZ = time.Hour - 1 + (4 * this.Location.Longitude) / 60;

            // Berücksichtigung Sommerzeit/Winterzeit
            if (time.IsDaylightSavingTime())
                MOZ = time.Hour - 2 + (4 * this.Location.Longitude) / 60;

            //float WOZ = MOZ + ZGL // hier : WOZ = MOZ 
            float omega = (12.00F - MOZ) * 15;

            // Sonnenhöhenwinkel ohne Berücksichtigung der Refraktion
            float h = ((float)(Math.Asin(
                omega.CosD() * this.Location.Latitude.CosD() * delta.CosD() +
                this.Location.Latitude.SinD() * delta.SinD()
                ))).ToDegrees();

            float azimut = 0;

            // Berechnung Sonnenazimut für Uhrzeit vor und nach Zenitzeit
            if (MOZ <= 12)
            {
                azimut = 180 - ((float)(Math.Acos(
                    ((h.SinD() * this.Location.Latitude.SinD() - delta.SinD()) /
                    (h.CosD() * this.Location.Latitude.CosD()))
                    ))).ToDegrees();
            }
            else if (MOZ > 12)
            {
                azimut = 180 + ((float)(Math.Acos(
                    (h.SinD() * this.Location.Latitude.SinD() - delta.SinD()) /
                    h.CosD() * this.Location.Latitude.CosD()
                    ))).ToDegrees();
            }

            return (h, azimut);
        }
        #endregion
    }
}

