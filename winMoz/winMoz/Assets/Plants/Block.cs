﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets.Schedules;
using winMoz.Assets;
using winMoz.Information;
using winMoz.Agents;
using winMoz.Trading.Strategies;

namespace winMoz.Assets.Plants
{
    /// <summary>
    /// Abstrakte Klasse für einen Kraftwerksblock. 
    /// </summary>
    public abstract class Block : Asset
    {
        /// <summary>
        /// Fremdschlüsseleigenschaft von <see cref="Plant_Thermal"/>.
        /// </summary>
        public int PlantId { get; private set; }
        /// <summary>
        /// <see cref="Plant_Thermal"/> zu dem dieser Block gehört.
        /// </summary>
        public Plant_Thermal Plant { get; private set; }
        /// <summary>
        /// Wirkungsgrad bei Nennlast. Wenn angegeben wird die Wirkungsgradkurve aus ThermalData.Efficiency um die Differenz zwischen dem dort angegeben Wirkungsgrad und diesem Wert verschoben.
        /// </summary>
        public float? EfficiencyAtPnom { get; set; }
        /// <summary>
        /// Spezifische Daten zu verschiedenen Kraftwerkstypen
        /// </summary>
        public Tynp ThermalData { get; private set; }
        /// <summary>
        /// Fremdschlüssel für Kraftwerkstypen
        /// </summary>
        public int ThermalDataCategory { get; private set; }
        /// <summary>
        /// Verwendeter Brennstoff des Kraftwerks
        /// </summary>
        public Commodities.Commodity Commodity => ThermalData.Commodity;

        public List<AvailabiltyScheduleItem> AvailabiltySchedule { get; private set; } = new List<AvailabiltyScheduleItem>();
        public List<(DateTime startRev, DateTime endRev)> revisionSchedule => AvailabiltySchedule.Where(sched => sched.isPlanned).Select(sched => (sched.noPowerBegin, sched.noPowerEnd)).ToList();

        #region Constructors
        protected Block()
        {
            InitializeSchedule();
        }
        public Block(IBalancingGroup bg, Enum subType, float powerMW, Information.Commodities.Commodity commodity, Plant_Thermal.AgeTypes ageType, Plant_Thermal plant, float efficiency = default, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null) :
            this(bg, subType, powerMW, Tynp.Load(commodity, ageType), plant, efficiency, locationPostcode, scenarioID, tradingStrategy)
        { }
        public Block(IBalancingGroup bg, Enum subType, float powerMW, Tynp ThermalData, Plant_Thermal plant, float? efficiency = null, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null) : base(bg, subType, powerMW, locationPostcode, scenarioID, tradingStrategy)
        {
            this.Plant = plant;
            this.PlantId = plant.Id;

            this.ThermalData = ThermalData;
            this.ThermalDataCategory = ThermalData.Category;
            if (efficiency != null)
            {
                this.ThermalData.Efficiency.SetEfficiencyAtPnom(efficiency.Value);
                EfficiencyAtPnom = efficiency.Value;
            }
        }
        #endregion

        #region Public functions
        /// <summary>
        /// Funktion errechnet die Kosten pro MWh für einen Zeitpunkt und gewünschter Produktion. 
        /// </summary>
        /// <param name="timeStamp"> Zeitpunkt zu welchem die Kosten gewünscht sind.</param>
        /// <param name="PowerMWh"> Gewünschte produzierte Energie. </param>
        /// <returns></returns>
        public override float GetCostForMWh(DateTime timeStamp, float PowerMWh)
        {
            var costs = GetCurrentCostCurvePerMWh(timeStamp);
            if (costs.Select(item => item.MWh_el).Contains(PowerMWh))
                return costs.Single(item => item.MWh_el == PowerMWh).costPerMWh;
            else
            {
                var CostsLower = costs.LastOrDefault(item => item.MWh_el < PowerMWh);
                var CostsHigher = costs.FirstOrDefault(item => item.MWh_el > PowerMWh);
                if (CostsLower != default && CostsHigher != default)
                    return CostsLower.costPerMWh + (PowerMWh - CostsLower.MWh_el) * (CostsHigher.costPerMWh - CostsLower.costPerMWh) / (CostsHigher.MWh_el - CostsLower.MWh_el);
                else if (CostsLower == default)
                    return 0F;
                else
                    return costs.Last().costPerMWh;
            }
        }

        /// <summary>
        /// Funktion errechnet die Kostenkurve in Abhängigkeit der produzierten Energie. Commodity Preise werden als gleichbleibend für den gesamten Zeitraum angenommen.
        /// </summary>
        /// <param name="TimeStamp"> Startzeit für die Kostenkurve.</param>
        /// <param name="duration"> Zeitraum für welchen die Kostenkurve berechnet werden soll. </param>
        /// <param name="step"> Zeitliche Auflösung der Berechnung. </param>
        /// <returns></returns>
        public override List<(DateTime TimeStamp, List<(float volume, float cost)>)> GetCostCurvePerMWh(DateTime TimeStamp, TimeSpan duration, TimeSpan step)
        {
            var costs = GetCurrentCostCurvePerMWh(TimeStamp);
            return Enumerable.Range(0, (int)(duration.Ticks / step.Ticks)).Select(el => (TimeStamp.AddMinutes(el * step.TotalMinutes), costs)).ToList();
        }
        /// <summary>
        /// Funktion errechnet die Kostenkurve für einen gegebenen Zeitpunkt. Dazu nutzt sie CO2 Preise, Brennstoffpreise und gibt die Kurve in Abhängigkeit der Effizienz aus.
        /// </summary>
        /// <param name="timestamp"> Zeitpunkt für welchen die Kurve berechnet werden soll. </param>
        /// <returns></returns>
        public List<(float MWh_el, float costPerMWh)> GetCurrentCostCurvePerMWh(DateTime timestamp)
        {
            var coalPrice = Commodities.GetPrices(timestamp, TimeSpan.FromDays(1), Commodity).First();
            var carbonPrice = Commodities.GetPrices(timestamp, TimeSpan.FromDays(1), Information.Commodities.Commodity.Carbon).First();

            var costs = this.Costs(coalPrice, carbonPrice);
            List<float> powerCurves = this.ThermalData.Efficiency.PowerCurve;

            List<(float MWh_El, float costPerMWh)> marginalCost = powerCurves.Select(el => (el * this.PowerMW, costs / ThermalData.Efficiency.AtPower(el))).ToList();

            return marginalCost;
        }

        public void UpdatePlantId() => PlantId = Plant.Id;
        
        /// <summary>
        /// Funktion löscht alte Fahrplanelemente bis zum gegebenen Zeitpunkt.
        /// </summary>
        /// <param name="timeUntil"> Zeitpunkt, bis zu welchem die Elemente gelöscht werden sollen. </param>
        public override void CutScheduleUntil(DateTime timeUntil) {}

        /// <summary>
        /// Funktion löscht alte Fahrplanelemente bis zum gegebenen Zeitpunkt.
        /// </summary>
        /// <param name="timeUntil"> Zeitpunkt, bis zu welchem die Elemente gelöscht werden sollen. </param>
        public void CutScheduleUntilFromThermal(DateTime timeUntil)
        {
            var scheduleItems = this.Schedule.Get(timeUntil, TimeSpan.FromDays(10000));
            this.Schedule = new Schedule();
            this.Schedule.AddRange(scheduleItems);
        }

        /// <summary>
        /// Funktion löscht alte Fahrplanelemente bis zum gegebenen Zeitpunkt.
        /// </summary>
        /// <param name="timeUntil"> Zeitpunkt, bis zu welchem die Elemente gelöscht werden sollen. </param>
        public override void CutControlReserveScheduleUntil(DateTime timeUntil) { }

        /// <summary>
        /// Funktion löscht alte Fahrplanelemente bis zum gegebenen Zeitpunkt.
        /// </summary>
        /// <param name="timeUntil"> Zeitpunkt, bis zu welchem die Elemente gelöscht werden sollen. </param>
        public void CutControlReserveScheduleUntilFromThermal(DateTime timeUntil)
        {
            var scheduleItems = this.ControlReserveSchedule.Get(timeUntil, TimeSpan.FromDays(10000));
            this.ControlReserveSchedule = new ControlReserveSchedule();
            this.ControlReserveSchedule.AddRange(scheduleItems);
        }
        #endregion


        #region CalcSchedule Override
        /// <summary>
        /// Die Funktion errechnet keine Fahrpläne, da das über die <see cref="Plant_Thermal"/> Klasse ausgelöst wird, um mehrere Blöcke und den thermischen Bedarf zu berücksichtigen.
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="duration"></param>
        public override void CalcSchedule(DateTime startTime, TimeSpan duration)
        {
            // Hier passiert nichts, da der Schedule basierend auf den Anforderungen des thermischen Kraftwerks vermarktet wird.
        }
        #endregion

        #region Private Functions

        private void InitializeSchedule() => Schedule = new Schedule();
        /// <summary>
        /// Funktion errechnet die Produktionskosten in Abhängigkeit der aktuellen BRennstoffpreise und CO2 Preise. 
        /// </summary>
        /// <param name="startTime"> Start der Berechnung. </param>
        /// <param name="duration"> Zeitraum der Berechnung. </param>
        /// <param name="efficiency"> Effizienz des Kraftwerks. </param>
        /// <returns> Produktionskosten in täglicher Auflösung </returns>
        protected IEnumerable<(DateTime, float)> ProductionCostsPerMWh(DateTime startTime, TimeSpan duration, float efficiency)
        {
            //Ein Preis pro Tag, bei Terminmarkt wird immer erster Preis für gesamten Monat angenommen. 
            //throw new NotImplementedException("Function needs fixes");
            var prices = new List<(DateTime, float costs)>();
            var coalPrice = Commodities.GetPrices(startTime, duration, Commodity).First();
            var carbonPrice = Commodities.GetPrices(startTime, duration, Information.Commodities.Commodity.Carbon).First();

            for (DateTime t = startTime; t < startTime + duration; t = t.AddDays(1))
            {
                prices.Add((t, Costs(coalPrice, carbonPrice)/efficiency));
            }

            return prices;
        }

        /// <summary>
        /// Funktion errechnet die Produktionskosten für elektrische Energie in Abhängigkeit von CO2-Preisen, Brennstroffpreisen in einem gegebenen Zeitraum. 
        /// </summary>
        /// <param name="startTime"> Startzeitpunkt der Berechnung</param>
        /// <param name="duration"> Zeitraum der Berechnung </param>
        /// <returns> Produktionskosten in Tagesauflösung </returns>
        public virtual IEnumerable<(DateTime, float)> ProductionCostsPerMWh_el(DateTime startTime, TimeSpan duration)
        {
            return ProductionCostsPerMWh(startTime, duration, ThermalData.Efficiency.AtPnom);
        }

        /// <summary>
        /// Errechnet die Kosten pro MWh bei gegebenem Brennstoff und CO2 Preis
        /// </summary>
        /// <param name="fuelPrice"> Brennstoffpreis in €/MWh. </param>
        /// <param name="carbonPrice"> CO2 Preis in €/t. </param>
        /// <returns> Kosten für Energie Produktion in €/MWh. </returns>
        protected float Costs(float fuelPrice, float carbonPrice)
        {
            // fuelPrice in € / MWh_in ; carbonPrice in €/t ;
            // CO2 Emissions in kg / GJ_in => 1kg = 1/1000t, 1 GJ = 1000/3600 MWh => 0.0036 t/MWh_in ; efficiency in MWh_out / MWh_in 
            // VariableOM in €/MWh_in
            return fuelPrice + carbonPrice * ThermalData.CO2Emissions * 0.0036F + ThermalData.Costs.VariableOM;
        }
        #endregion

        #region Methoden für BlockAttribute

        public void SetPlant(Plant_Thermal plant)
        {
            this.Plant = plant;
        }
        #endregion
    }
    /// <summary>
    /// Klasse für einen Kraftwerksblock ohne Wärmeauskopplung.
    /// </summary>
    public class Block_Kond : Block
    {
        #region Constructors
        private Block_Kond():base()
        {
        }

        public Block_Kond(IBalancingGroup bg, Enum subType, float powerMW, Information.Commodities.Commodity commodity, Plant_Thermal.AgeTypes ageType, Plant_Thermal plant, float efficiency = default, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null) :
           this(bg, subType, powerMW, Tynp.Load(commodity, ageType), plant, efficiency, locationPostcode, scenarioID, tradingStrategy)
        { }
        public Block_Kond(IBalancingGroup bg, Enum subType, float powerMW, Tynp ThermalData, Plant_Thermal plant, float? efficiency = null, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null) : 
            base(bg, subType, powerMW, ThermalData, plant, efficiency, locationPostcode, scenarioID, tradingStrategy)
        {
        }
        #endregion
    }
    /// <summary>
    /// Klasse für einen Kraftwerksblock mit Wärmeauskopplung.
    /// </summary>
    public class Block_CHP : Block
    {
        /// <summary>
        /// Ausgekoppelte Wärmeleistung in MW
        /// </summary>
        public float P_th_Kwk { get; private set; }     // Ausgekoppelte Wärmeleistung in MW
        /// <summary>
        ///         Mögliche Leistungsänderung pro Viertelstunde in MW
        /// </summary>
        public float P_th_DeltaQh => 0.025F * this.P_th_Kwk * 15; // Stk: 1.5/4/6 %Pn/min 
        /// <summary>
        /// Thermische Effizienz bezogen auf die eingesetzte Energie
        /// </summary>
        private float eff_th => eff_ges - ThermalData.Efficiency.AtPnom;
        /// <summary>
        /// Verhältnis von P_th / P_elekt im Arbeitspunkt^, (heat-to-electricity-ratio)
        /// </summary>
        private float PthToPel => P_th_Kwk / PowerMW;
        /// <summary>
        /// Gesamteffizienz 
        /// </summary>
        public float eff_ges { get; private set; }
        public float P_th_min => P_th_Kwk * ThermalData.MinimumStableGeneration;

        #region Constructors
        private Block_CHP():base()
        { }
        public Block_CHP(IBalancingGroup bg, Enum subType, float powerMW, float PowerMWTh, float TotalEff, Information.Commodities.Commodity commodity, Plant_Thermal.AgeTypes ageType, Plant_Thermal plant, float efficiency = default, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null) :
            base(bg, subType, powerMW, Tynp.Load(commodity, ageType), plant, efficiency, locationPostcode, scenarioID, tradingStrategy)
        {
            this.P_th_Kwk = PowerMWTh;
            this.eff_ges = TotalEff;
        }
        public Block_CHP(IBalancingGroup bg, Enum subType, float powerMW, float PowerMWTh, float TotalEff, Tynp ThermalData, Plant_Thermal plant, float? efficiency = null, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null) :
            base(bg, subType, powerMW, ThermalData, plant, efficiency, locationPostcode, scenarioID, tradingStrategy)
        {
            this.P_th_Kwk = PowerMWTh;
            this.eff_ges = TotalEff;
        }
        #endregion


        #region Public Functions

        /// <summary>
        /// Function calculates the production cost for electrical energy in dependency of carbon prices, fuel prices and plant efficiency for the given timespan. 
        /// </summary>
        /// <param name="startTime"> Start of caluclation</param>
        /// <param name="duration"> Timeframe of calculation </param>
        /// <returns> Produkction cost in daily resolution </returns>
        public override IEnumerable<(DateTime, float)> ProductionCostsPerMWh_el(DateTime startTime, TimeSpan duration)
        {
            //return ProductionCostsPerMWh(startTime, duration, ThermalData.Efficiency.AtPnom).Select(item => (item.Item1, item.Item2 * ((this.eff_ges - this.eff_th) / this.eff_ges))).ToList();
            return ProductionCostsPerMWh(startTime, duration, ThermalData.Efficiency.AtPnom).Select(item => (item.Item1, item.Item2 * (0.6F - this.eff_th))).ToList();
        }


        /// <summary>
        /// Funktion errechnet die Kosten für thermische Energie für einen gebeenen Zeitraum.
        /// </summary>
        /// <param name="startTime"> Startpunkt der Berechnung </param>
        /// <param name="duration"> Zeitspanne der Berechnung </param>
        /// <returns> Produkction cost in daily resolution </returns>
        public IEnumerable<(DateTime, float)> ProductionCostsPerMWh_th(DateTime startTime, TimeSpan duration)
        {
            return ProductionCostsPerMWh(startTime, duration, eff_th);
        }
        /// <summary>
        /// Funktion gibt den elektrischen Output bei gegebenem thermischen Output.
        /// </summary>
        /// <param name="thermalPower"> Die thermische Leistung in MW zu welcher die elektrische ausgekoppelte Leistung benötigt wird. </param>
        /// <returns> Elektrischer Output in MW </returns>
        public float ThermalToElectrical(float thermalPower)
        {
            return thermalPower / PthToPel;
        }
        #endregion
    }
}


