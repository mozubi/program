﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access;
using winMoz.Markets;
using System.Collections.Concurrent;

namespace winMoz.Assets.Schedules
{
    public class ControlReserveScheduleItem
    {
        public DateTime TimeStamp { get; private set; }
        public int Id { get; private set; }
        private static int _id;
        public int AssetId { get; private set; }
        public int ThermalBlockId { get; private set; } = 1;
        public string AssetType { get; private set; }

        #region Power & Prices
        public bool IsCommited { get; private set; } = false;
        public float Costs { get; private set; }
        public float Revenues { get; private set; }
        public float PowerReservedOrActivatedPositive { get; private set; }
        public float PowerReservedOrActivatedNegative { get; private set; }
        #endregion

        #region Constructors

        public void SetAssetId(int assetId)
        {
            if (AssetId == default)
                AssetId = assetId;
        }

        public void SetBlockId(int blockId)
        {
            if (ThermalBlockId == default)
                ThermalBlockId = blockId;
        }

        public void SetType(Type assetType)
        {
            if (AssetType == default)
                AssetType = assetType.Name;
        }

        public void SetType(string assetType)
        {
            if (AssetType == default)
                AssetType = assetType;
        }

        private ControlReserveScheduleItem(DateTime timeStamp, ControlReserveScheduleItem copyItem)
        {
            this.Id = copyItem.Id;
            this.TimeStamp = timeStamp;
            this.PowerReservedOrActivatedPositive = copyItem.PowerReservedOrActivatedPositive;
            this.PowerReservedOrActivatedNegative = copyItem.PowerReservedOrActivatedNegative;
            this.Costs = copyItem.Costs;
            this.Revenues = copyItem.Revenues;
        }

        public ControlReserveScheduleItem(DateTime timeStamp)
        {
            this.TimeStamp = timeStamp;
        }

        private ControlReserveScheduleItem(DateTime timeStamp, int id) : this(timeStamp)
        {
            this.Id = id;
        }

        private static int GetId() => System.Threading.Interlocked.Increment(ref _id);
        #endregion

        /// <summary>
        /// Method adds two ScheduleItems keeps Id and Time from A
        /// </summary>
        /// <param name="itemIn"></param>
        public static ControlReserveScheduleItem operator +(ControlReserveScheduleItem itemA, ControlReserveScheduleItem itemB)
        {
            if (itemA.TimeStamp != itemB.TimeStamp) throw new InvalidOperationException("Falscher Zeitstempel: keine Zuweisung möglich!");
            itemA.Costs += itemB.Costs;
            itemA.Revenues += itemB.Revenues;
            itemA.PowerReservedOrActivatedPositive += itemB.PowerReservedOrActivatedPositive;
            itemA.PowerReservedOrActivatedNegative += itemB.PowerReservedOrActivatedNegative;

            return itemA;
        }


        /// <summary>
        /// Method adds two ScheduleItems keeps Id and Time from A
        /// </summary>
        /// <param name="items"></param>
        public static ControlReserveScheduleItem Sum(List<ControlReserveScheduleItem> items)
        {
            var itemA = items.First();
            if (items.Any(item => item.TimeStamp != itemA.TimeStamp)) throw new InvalidOperationException("Falscher Zeitstempel: keine Zuweisung möglich!");
            var SumCosts = items.Select(item => item.Costs).Sum();
            itemA.Costs = SumCosts;
            var SumRevenues = items.Select(item => item.Revenues).Sum();
            itemA.Revenues = SumRevenues;
            var SumPowerReservedPositive = items.Select(item => item.PowerReservedOrActivatedPositive).Sum();
            var SumPowerReservedNegative = items.Select(item => item.PowerReservedOrActivatedNegative).Sum();

            if (float.IsNaN(itemA.Costs)) Debugger.Break();
            return itemA;
            //return new ScheduleItem(itemA.TimeStamp, items.Select(item => item.Power).Sum(), SumPowerPriceProduct / SumPower);
        }


        public static ControlReserveScheduleItem Copy(ControlReserveScheduleItem item) => new ControlReserveScheduleItem(item.TimeStamp, item);
        public static ControlReserveScheduleItem Dummy(ControlReserveScheduleItem item) => new ControlReserveScheduleItem(item.TimeStamp, item) { Id = -1 };

        public void ReservePositiveCapacity(float powerReserved, float priceReserved)
        {
            if (powerReserved * priceReserved > 0) Revenues += powerReserved / 4.0F * priceReserved / 1000.0F;
            else Costs += -powerReserved / 4.0F * priceReserved / 1000.0F;
            this.PowerReservedOrActivatedPositive += powerReserved;

            if (float.IsNaN(this.Costs)) Debugger.Break();
        }

        public void ReserveNegativeCapacity(float powerReserved, float priceReserved)
        {
            if (powerReserved * priceReserved > 0) Revenues += powerReserved / 4.0F * priceReserved / 1000.0F;
            else Costs += -powerReserved / 4.0F * priceReserved / 1000.0F;
            this.PowerReservedOrActivatedNegative += powerReserved;

            if (float.IsNaN(this.Costs)) Debugger.Break();
        }

        public void ReservePositiveEnergy(float powerReserved)
            => this.PowerReservedOrActivatedPositive += powerReserved;

        public void ReserveNegativeEnergy(float powerReserved)
            => this.PowerReservedOrActivatedNegative += powerReserved;

        public void FreePositiveReservation(float powerFreed)
            => this.PowerReservedOrActivatedPositive -= powerFreed;

        public void FreeNegativeReservation(float powerFreed)
            => this.PowerReservedOrActivatedNegative -= powerFreed;

        public void ActivatePositive(float powerActivated, float priceActivated)
        {
            if (powerActivated * priceActivated > 0) Revenues += powerActivated / 4.0F * priceActivated / 1000.0F;
            else Costs += -powerActivated / 4.0F * priceActivated / 1000.0F;
            this.PowerReservedOrActivatedPositive += powerActivated;

            if (float.IsNaN(this.Costs)) Debugger.Break();
        }

        public void ActivateNegative(float powerActivated, float priceActivated)
        {
            if (powerActivated * priceActivated > 0) Revenues += powerActivated / 4.0F * priceActivated / 1000.0F;
            else Costs += -powerActivated / 4.0F * priceActivated / 1000.0F;
            this.PowerReservedOrActivatedNegative += powerActivated;

            if (float.IsNaN(this.Costs)) Debugger.Break();
        }

        public ControlReserveScheduleItem Commit()
        {
            this.IsCommited = true;
            return this;
        }
    }
}
