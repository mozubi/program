﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Data.Access;

namespace winMoz.Assets.Schedules
{
    public class ScheduleItem : IComparable<ScheduleItem>, IEquatable<ScheduleItem>
    {
        public DateTime TimeStamp { get; private set; }
        public int Id { get; private set; }
        private static int _id;
        public int ProcurementCounter { get; private set; }
        public int AssetId { get; private set; }
        public int ThermalBlockId { get; private set; } = 1;
        public string AssetType { get; protected set; }

        #region Power & Prices
        public bool IsProduction => Power >= 0;
        public bool IsConsumption => !IsProduction;
        public bool IsCommited { get; private set; } = false;
        public float Power { get; private set; }
        public float PowerProcured { get; private set; }
        public float PowerDiff => Power - PowerProcured; //Power - PowerReserved - PowerProcured; //IsProduction ? -PowerReserved -PowerProcured - Power : -Power - PowerProcured - PowerReserved;
        public float Costs { get; private set; }
        public float Revenues { get; private set; }
        #endregion

        #region Constructors
        public ScheduleItem(DateTime timeStamp) : this(timeStamp, GetId()) {  }

        public ScheduleItem(DateTime timeStamp, float power) : this(timeStamp)
        {
            Set(power);
        }

        public void SetAssetId(int assetId)
        {
            if (AssetId == default)
                AssetId = assetId;
        }

        public void SetBlockId(int blockId)
        {
            if (ThermalBlockId == default)
                ThermalBlockId = blockId;
        }

        public void SetType(Type assetType)
        {
            if (AssetType == default)
                AssetType = assetType.Name;
        }

        public void SetType(string assetType)
        {
            if (AssetType == default)
                AssetType = assetType;
        }

        protected ScheduleItem(DateTime timeStamp, ScheduleItem copyItem)
        {
            this.Id = copyItem.Id;
            this.TimeStamp = timeStamp;
            this.Power = copyItem.Power;
            this.PowerProcured = copyItem.PowerProcured;
            this.Costs = copyItem.Costs;
            this.Revenues = copyItem.Revenues;
        }

        private ScheduleItem(DateTime timeStamp, int id)
        {
            this.TimeStamp = timeStamp;
            this.Id = id;
        }

        private static int GetId() => System.Threading.Interlocked.Increment(ref _id);
        #endregion
        #region IEquatable
        public virtual bool Equals(ScheduleItem y)
        {
            if (y is null)
                return false;
            return this.TimeStamp.Ticks.Equals(y.TimeStamp.Ticks);
        }
        public override bool Equals(object obj) => this.Equals(obj as ScheduleItem);

        public override int GetHashCode()
        {
            return this.TimeStamp.Ticks.GetHashCode();
        }
        #endregion IEquatable


        /// <summary>
        /// Method adds two ScheduleItems keeps Id and Time from A
        /// </summary>
        /// <param name="itemIn"></param>
        public static ScheduleItem operator +(ScheduleItem itemA, ScheduleItem itemB)
        {
            if (itemA.TimeStamp != itemB.TimeStamp) throw new InvalidOperationException("Falscher Zeitstempel: keine Zuweisung möglich!");
            itemA.Power += itemB.Power;
            itemA.PowerProcured += itemB.PowerProcured;
            itemA.Costs += itemB.Costs;
            itemA.Revenues += itemB.Revenues;
            
            return itemA;
        }


        /// <summary>
        /// Method adds two ScheduleItems keeps Id and Time from A
        /// </summary>
        /// <param name="itemIn"></param>
        public static ScheduleItem Sum(List<ScheduleItem> items)
        {
            var itemA = items.First();
            if (items.Any(item => item.TimeStamp != itemA.TimeStamp)) throw new InvalidOperationException("Falscher Zeitstempel: keine Zuweisung möglich!");
            var SumPower = items.Select(item => item.Power).Sum();
            itemA.Power = SumPower;
            var SumPowerProcured = items.Select(item => item.PowerProcured).Sum();
            itemA.PowerProcured = SumPowerProcured;
            var SumCosts = items.Select(item => item.Costs).Sum();
            itemA.Costs = SumCosts;
            var SumRevenues = items.Select(item => item.Revenues).Sum();
            itemA.Revenues = SumRevenues;

            if (float.IsNaN(itemA.Costs)) Debugger.Break();
            return itemA;
            //return new ScheduleItem(itemA.TimeStamp, items.Select(item => item.Power).Sum(), SumPowerPriceProduct / SumPower);
        }

        public static (DateTime timeStamp, float power) SumProcured(List<ScheduleItem> items)
        {
            var itemA = items.First();
            if (!items.Any(item => item.TimeStamp != itemA.TimeStamp)) throw new InvalidOperationException("Falscher Zeitstempel: keine Zuweisung möglich!");
            var SumPower = items.Select(item => item.PowerProcured).Sum();
            return (itemA.TimeStamp, SumPower );
        }


        public static ScheduleItem Copy(ScheduleItem item) => new ScheduleItem(item.TimeStamp, item);
        public static ScheduleItem Dummy(ScheduleItem item) => new ScheduleItem(item.TimeStamp, item) { Id = -1 };

        public void Procure(float powerProcured, float priceProcured, bool fromReserved = false)
        {
            if (powerProcured * priceProcured > 0) Revenues += powerProcured / 4.0F * priceProcured / 1000.0F;
            else Costs += -powerProcured / 4.0F * priceProcured / 1000.0F;
            this.PowerProcured = this.PowerProcured + powerProcured;

            this.ProcurementCounter += 1;

            if (float.IsNaN(this.PowerDiff) || float.IsNaN(this.PowerProcured) || float.IsNaN(this.Costs)) Debugger.Break();
        }

       

        public void Set(float power)
        {
            if (!IsCommited)
            {
                this.Power = power;
            }
        }

        public ScheduleItem Commit()
        {
            this.IsCommited = true;
            return this;
        }

        private void AddWeighted(float oldPower, float oldPrice, float power2add, float price2add, out float powerSum, out float priceWeightedSum)
        {
            powerSum = oldPower + power2add;
            priceWeightedSum = GetWeightedAveragePrice(power2add, price2add, oldPower, oldPrice);
        }

        public static float GetWeightedAveragePrice(float powerFirst, float priceFirst, float powerSecond, float priceSecond)
        {
            var avgPrice = (float)Math.Round((Math.Abs(powerSecond * priceSecond) + Math.Abs((powerFirst * priceFirst))) / (Math.Abs(powerSecond) + Math.Abs(powerFirst)), 2);

            if (float.IsNaN(avgPrice)) return 0.0F;
            return avgPrice;
        }

        public virtual int CompareTo(ScheduleItem other)
        {
            int value = TimeStamp.CompareTo(other.TimeStamp);
            if (value == 0)
            {
                int innerValue = this.AssetType.CompareTo(other.AssetType);
                if (value == 0)
                {
                    return AssetId - other.AssetId;
                }
                else
                    return innerValue;
            }
            else
                return value;
        }
    }

    public class SumScheduleItem : ScheduleItem
    {
        public SumScheduleItem(DateTime timeStamp) : base(timeStamp) { }

        public SumScheduleItem(DateTime timeStamp, float power) : base(timeStamp, power) { }

        public SumScheduleItem(ScheduleItem item) : base(item.TimeStamp, item)
        {
            this.AssetType = item.AssetType;
        }

        public override bool Equals(ScheduleItem y)
        {
            return base.Equals(y) && this.AssetType.Equals(y);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() + this.AssetType.GetHashCode();
        }

        public override int CompareTo(ScheduleItem other)
        {
            int value = TimeStamp.CompareTo(other.TimeStamp);
            if (value == 0)
            {
                int innerValue = this.AssetType.CompareTo(other.AssetType);
                return innerValue;
            }
            else
                return value;
        }
    }
}
