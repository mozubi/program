﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;

namespace winMoz.Assets.Schedules
{
    public class Schedule : List<ScheduleItem>
    {
        public TimeSpan Duration => this.Select(el => el.TimeStamp).Duration();

        #region Events
        public delegate void ScheduleChange<ChangedItems>(ChangedItems items) where ChangedItems : IEnumerable<ScheduleItem>;
        public event ScheduleChange<IEnumerable<ScheduleItem>> ScheduleChanged;

        public delegate void ProcuringEventHandler<Start, End>(Start startOfProcurement, End endOfProcurement);
        public event ProcuringEventHandler<DateTime, DateTime> Procured;

        public delegate void PowerSetEventHandler<Start, End>(Start startTime, End endTime);
        public event PowerSetEventHandler<DateTime, DateTime> PowerSet;
        #endregion

        #region Constructors
        public Schedule() { }

        public Schedule(IEnumerable<ScheduleItem> collection) { this.AddRange(collection); ScheduleChanged?.Invoke(collection); }
        #endregion

        public void Plot() => winMoz.Tools.Viz.Plotter.Plot(this);

        #region Fahrplaneintragungen
        public void SetPower(List<(DateTime TimeStamp, float Power)> ts2set)
        {
            if (ts2set.Count() > 0)
            {
                if (!this.IsValid(ts2set.First().TimeStamp, ts2set.Duration()))
                    this.Extend(ts2set.First().TimeStamp, ts2set.Duration());
                this.Join(ts2set, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                    .ForEach(el => el.ScheduleItem.Set(el.tsItem.Power));

                this.PowerSet?.Invoke(ts2set.FirstOrDefault().TimeStamp, ts2set.LastOrDefault().TimeStamp);
            }
        }

        public void AddPower(List<(DateTime TimeStamp, float Power)> ts2set)
        {
            DateTime start = ts2set.First().TimeStamp;
            DateTime end = ts2set.Last().TimeStamp;
            TimeSpan Duration = end - start;
            if (!this.IsValid(start, Duration))
                this.Extend(start, Duration);

            this.SkipWhile(item => item.TimeStamp.Ticks < start.Ticks).ToList().Join(ts2set, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.Set(el.ScheduleItem.Power + el.tsItem.Power));

            this.PowerSet?.Invoke(ts2set.FirstOrDefault().TimeStamp, ts2set.LastOrDefault().TimeStamp);
        }

        public float GetWeightedPrice(float priceA, float powerA, float priceB, float powerB)
            => (powerA + powerB) > 0 ? (powerA * priceA + powerB * priceB) / (powerA + powerB) : 0;

        public void Procure(List<(DateTime TimeStamp, float PowerProcured, float PriceCleared)> ts2add, bool fromReserved = false)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.Procure(el.tsItem.PowerProcured, el.tsItem.PriceCleared, fromReserved));

            this.Procured?.Invoke(ts2add.First().TimeStamp, ts2add.Last().TimeStamp);
        }

        public void Extend(DateTime start, TimeSpan duration)
        {
            var sched = Enumerable.Range(0, (int)duration.TotalHours * 4).Select(qh => new ScheduleItem(start.AddMinutes(15 * qh)));
            var Except = sched.Reverse().TakeWhile(item => item.TimeStamp.Ticks >= start.Ticks).Except(this);
            this.AddRange(Except);
            this.Sort((x, y) => DateTime.Compare(x.TimeStamp, y.TimeStamp));
            this.ScheduleChanged?.Invoke(this.Get(start, duration));
        }



        public void InsertOrReplace(ScheduleItem item)
        {
            int idx2insert = 0;
            for (int i = 0; i < this.Count; i++)
            {
                idx2insert += 1;
                if (this[i].TimeStamp == item.TimeStamp)
                    break;
            }
            if (idx2insert == this.Count) this.Add(item);
            else this[idx2insert] = item;
        }
        #endregion

        #region Boolesche Fahrplan-Methoden
        /// <summary>
        /// Erweiterungsmethode überprüft Fahrplan auf Gültigkeit und Regelmäßigkeit in Zeitraum und gibt Bool zurück
        /// </summary>
        /// <param name="schedule">Fahrplan, der überprüft werden soll</param>
        /// <param name="startTime">Startzeitpunkt der Fahrplanüberprüfung</param>
        /// <param name="duration">Dauer der Fahrplanüberprüfung</param>
        /// <returns>Bool - Fahrplan gültig/ungültig</returns>
        public bool IsValid(DateTime startTime, TimeSpan duration)
        {
            if (this == null || this.Count == 0)
                return false;

            var timeStep = this[1].TimeStamp - this[0].TimeStamp;

            bool startIsFound = this.Exists(el => el.TimeStamp == startTime);
            bool endIsFound = this.Exists(el => el.TimeStamp == (startTime + duration - timeStep));
            bool isRegular = this.IsRegular();

            return startIsFound && endIsFound && isRegular;
        }

        /// <summary>
        /// Erweiterungsmethode für Fahrpläne überprüft Regelmäßigkeit des Fahrplans bezüglich zeitlicher Schrittweite
        /// </summary>
        /// <param name="schedule">Fahrplan, der überprüft werden soll</param>
        /// <returns>Bool - Fahrplan ist Regelmäßig true/false</returns>
        private bool CheckIsRegular(TimeSpan duration)
        {
            if (this.Count < 2) return false;

            for (int i = 1; i < this.Count; i++)
                if (this[i].TimeStamp - this[i - 1].TimeStamp != duration)
                    return false;

            return true;
        }

        public bool IsRegular() => this.CheckIsRegular(this[1].TimeStamp - this[0].TimeStamp);
        public bool IsRegularQh() => this.CheckIsRegular(TimeSpan.FromMinutes(15));
        #endregion

        #region Getters

        /// <summary>
        /// Erweiterungsmethode für Fahrpläne gibt Fahrplan für Zeitraum ab Startzeit zurück
        /// </summary>
        /// <param name="schedule">Fahrplan, aus dem ausgelesen werden soll</param>
        /// <param name="startTime">Startzeit, ab dem der Fahrplan ausgelesen werden soll</param>
        /// <param name="duration">Dauer, für die der Fahrplan ausgelesen werden soll</param>
        /// <returns>Ausgelesener Fahrplan</returns>
        public List<ScheduleItem> Get(DateTime startTime, TimeSpan duration)
        {
            DateTime EndOfDuration = Extensions.GetEndOfDuration(startTime, duration);
            // Where always returns empty list if nothing is found
            return this.Where(el => el.TimeStamp.Ticks >= startTime.Ticks && el.TimeStamp.Ticks < EndOfDuration.Ticks).ToList();
        }
        public ScheduleItem GetItem(DateTime TimeStamp) => this.SingleOrDefault(el => el.TimeStamp.CompareTo(TimeStamp) == 0);
        #endregion

        #region Export Methoden
        public void Export2CSV()
            => Helper.CSV.ToCSV(this.Select(x => (x.TimeStamp, x.Power, x.PowerProcured, x.PowerDiff, x.Costs, x.Revenues)).ToList(), new string[] { "TimeStamp", "Power", "PowerProcured", "PowerDiff", "Costs", "Revenues" });
        #endregion


        #region Interface implementations

        public ScheduleItem this[DateTime time] { get => GetItem(time); set => this[this.FindIndex(el => el.TimeStamp == time)] = value; }

      
        #endregion
    }
}
