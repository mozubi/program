﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets.Plants;

namespace winMoz.Assets.Schedules
{
    public class AvailabiltyScheduleItem 
    {
        public DateTime noPowerBegin { get; private set; }
        public DateTime noPowerEnd { get; private set; }
        public bool isPlanned { get; private set; }
        public int UnitId { get; private set; }
        public Block Unit { get; private set; }
    }
}
