﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Markets;

namespace winMoz.Assets.Schedules
{
    public class ControlReserveSchedule : List<ControlReserveScheduleItem>, IEqualityComparer<ControlReserveScheduleItem>
    {
        public TimeSpan Duration => this.Select(el => el.TimeStamp).Duration();

        #region Events
        public delegate void ScheduleChange<ChangedItems>(ChangedItems items) where ChangedItems : IEnumerable<ControlReserveScheduleItem>;
        public event ScheduleChange<IEnumerable<ControlReserveScheduleItem>> ScheduleChanged;

        public delegate void ReservationCapacityEventHandler<Start, End>(Start startOfProcurement, End endOfProcurement);
        public event ReservationCapacityEventHandler<DateTime, DateTime> ReservedCapacity;

        public delegate void ReservationEnergyEventHandler<Start, End>(Start startOfProcurement, End endOfProcurement);
        public event ReservationCapacityEventHandler<DateTime, DateTime> ReservedEnergy;

        public delegate void ActivationEventHandler<Start, End>(Start startOfProcurement, End endOfProcurement);
        public event ReservationCapacityEventHandler<DateTime, DateTime> Activated;

        public delegate void PowerSetEventHandler<Start, End>(Start startTime, End endTime);
        public event PowerSetEventHandler<DateTime, DateTime> PowerSet;
        #endregion

        #region Constructors
        public ControlReserveSchedule() { }

        public ControlReserveSchedule(IEnumerable<ControlReserveScheduleItem> collection) { this.AddRange(collection); ScheduleChanged?.Invoke(collection); }
        #endregion

        //public void Plot() => winMoz.Tools.Viz.Plotter.Plot(this);

        #region Fahrplaneintragungen

        public void ReservePositiveCapacity(List<(DateTime TimeStamp,float PowerReserved, float PriceCleared)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.ReservePositiveCapacity(el.tsItem.PowerReserved, el.tsItem.PriceCleared));
        }

        public void ReserveNegativeCapacity(List<(DateTime TimeStamp, float PowerReserved, float PriceCleared)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.ReserveNegativeCapacity(el.tsItem.PowerReserved, el.tsItem.PriceCleared));
        }

        public void ReservePositiveEnergy(List<(DateTime TimeStamp, float PowerReserved)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.ReservePositiveEnergy(el.tsItem.PowerReserved));
        }

        public void ReserveNegativeEnergy(List<(DateTime TimeStamp, float PowerReserved)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.ReserveNegativeEnergy(el.tsItem.PowerReserved));
        }

        public void FreePositiveReservation(List<(DateTime TimeStamp, float PowerReserved)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.FreePositiveReservation(el.tsItem.PowerReserved));
        }

        public void FreeNegativeReservation(List<(DateTime TimeStamp, float PowerReserved)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.FreeNegativeReservation(el.tsItem.PowerReserved));
        }

        public void ActivatePositive(List<(DateTime TimeStamp, float PowerReserved, float PriceCleared)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.ActivatePositive(el.tsItem.PowerReserved, el.tsItem.PriceCleared));
        }

        public void ActivateNegative(List<(DateTime TimeStamp, float PowerReserved, float PriceCleared)> ts2add)
        {
            this.Join(ts2add, sch => sch.TimeStamp.Ticks, ts => ts.TimeStamp.Ticks, (schedItem, tsItem) => (ScheduleItem: schedItem, tsItem: tsItem)).ToList()
                .ForEach(el => el.ScheduleItem.ActivateNegative(el.tsItem.PowerReserved, el.tsItem.PriceCleared));
        }

        public void Extend(DateTime start, TimeSpan duration)
        {
            var sched = Enumerable.Range(0, (int)duration.TotalHours * 4).Select(qh => new ControlReserveScheduleItem(start.AddMinutes(15 * qh)));
            var Except = sched.Except(this, this);
            this.AddRange(Except);
            this.Sort((x, y) => DateTime.Compare(x.TimeStamp, y.TimeStamp));
            this.ScheduleChanged?.Invoke(this.Get(start, duration));
        }

        #region IEqualityComparer
        public bool Equals(ControlReserveScheduleItem x, ControlReserveScheduleItem y)
        {
            return x.TimeStamp.Ticks.Equals(y.TimeStamp.Ticks);
        }

        public int GetHashCode(ControlReserveScheduleItem item)
        {
            return item.TimeStamp.Ticks.GetHashCode();
        }
        #endregion IEqualityComparer

        public void InsertOrReplace(ControlReserveScheduleItem item)
        {
            int idx2insert = 0;
            for (int i = 0; i < this.Count; i++)
            {
                idx2insert += 1;
                if (this[i].TimeStamp == item.TimeStamp)
                    break;
            }
            if (idx2insert == this.Count) this.Add(item);
            else this[idx2insert] = item;
        }
        #endregion

        #region Boolesche Fahrplan-Methoden
        /// <summary>
        /// Erweiterungsmethode überprüft Fahrplan auf Gültigkeit und Regelmäßigkeit in Zeitraum und gibt Bool zurück
        /// </summary>
        /// <param name="schedule">Fahrplan, der überprüft werden soll</param>
        /// <param name="startTime">Startzeitpunkt der Fahrplanüberprüfung</param>
        /// <param name="duration">Dauer der Fahrplanüberprüfung</param>
        /// <returns>Bool - Fahrplan gültig/ungültig</returns>
        public bool IsValid(DateTime startTime, TimeSpan duration)
        {
            if (this == null || this.Count == 0)
                return false;

            var timeStep = this[1].TimeStamp - this[0].TimeStamp;

            bool startIsFound = this.Exists(el => el.TimeStamp == startTime);
            bool endIsFound = this.Exists(el => el.TimeStamp == (startTime + duration - timeStep));
            bool isRegular = this.IsRegular();

            return startIsFound && endIsFound && isRegular;
        }

        /// <summary>
        /// Erweiterungsmethode für Fahrpläne überprüft Regelmäßigkeit des Fahrplans bezüglich zeitlicher Schrittweite
        /// </summary>
        /// <param name="schedule">Fahrplan, der überprüft werden soll</param>
        /// <returns>Bool - Fahrplan ist Regelmäßig true/false</returns>
        private bool CheckIsRegular(TimeSpan duration)
        {
            if (this.Count < 2) return false;

            for (int i = 1; i < this.Count; i++)
                if (this[i].TimeStamp - this[i - 1].TimeStamp != duration)
                    return false;

            return true;
        }

        public bool IsRegular() => this.CheckIsRegular(this[1].TimeStamp - this[0].TimeStamp);
        public bool IsRegularQh() => this.CheckIsRegular(TimeSpan.FromMinutes(15));
        #endregion

        #region Getters

        /// <summary>
        /// Erweiterungsmethode für Fahrpläne gibt Fahrplan für Zeitraum ab Startzeit zurück
        /// </summary>
        /// <param name="schedule">Fahrplan, aus dem ausgelesen werden soll</param>
        /// <param name="startTime">Startzeit, ab dem der Fahrplan ausgelesen werden soll</param>
        /// <param name="duration">Dauer, für die der Fahrplan ausgelesen werden soll</param>
        /// <returns>Ausgelesener Fahrplan</returns>
        public List<ControlReserveScheduleItem> Get(DateTime startTime, TimeSpan duration)
        {
            DateTime EndOfDuration = Extensions.GetEndOfDuration(startTime, duration);
            // Where always returns empty list if nothing is found
            return this.Where(el => el.TimeStamp.CompareTo(startTime) >= 0 && el.TimeStamp.CompareTo(EndOfDuration) < 0).ToList();
        }
        public ControlReserveScheduleItem GetItem(DateTime TimeStamp) => this.SingleOrDefault(el => el.TimeStamp.CompareTo(TimeStamp) == 0);
        #endregion

        #region Export Methoden
        public void Export2CSV()
        {
            Helper.CSV.ToCSV(this.Select(x => (x.TimeStamp, x.PowerReservedOrActivatedPositive, x.PowerReservedOrActivatedNegative, x.Costs, x.Revenues)).ToList(), new string[] { "TimeStamp", "PowerReservedOrActivatedPositive", "PowerReservedOrActivatedNegative", "Costs", "Revenues" });
        }
        #endregion


        #region Interface implementations

        public ControlReserveScheduleItem this[DateTime time] { get => GetItem(time); set => this[this.FindIndex(el => el.TimeStamp == time)] = value; }


        #endregion
    }
}
