﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Tools.Viz;
using winMoz.Helper;

namespace winMoz.Assets.Schedules
{
    public static class ScheduleExtensions
    {

        #region Plotting Implementierung
        public static void Plot(this Schedule schedule, bool plotPrices = false) => Plotter.Plot(schedule.ToList(), plotPrices);
        public static void Plot(this Schedule schedule, List<float> series, string nameA = "SeriesA", bool plotPrices = false) => Plotter.Plot(schedule.ToList(), series, nameA, plotPrices);
        public static void Plot(this Schedule schedule, List<float> seriesA, List<float> seriesB, string nameA = "SeriesA", string nameB = "SeriesB", bool plotPrices = false)
            => Plotter.Plot(schedule.ToList(), seriesA, seriesB, nameA, nameB, plotPrices);
        public static void Plot(this Schedule schedule, List<float> seriesA, List<float> seriesB, List<float> seriesC, string nameA = "SeriesA", string nameB = "SeriesB", string nameC = "SeriesC", bool plotPrices = false)
            => Plotter.Plot(schedule.ToList(), seriesA, seriesB, seriesC, nameA, nameB, nameC, plotPrices);
        #endregion



        #region Methods für mehrere Assets
        public static Schedule GetSumSchedule(this IEnumerable<IAsset> assets, DateTime startTime = default, TimeSpan duration = default)
        {
            if (assets.Count() == 0)
            {
                Schedule schedule = new Schedule();
                schedule.Extend(startTime, duration);
                return schedule;
            }
            else if (assets.Count() == 1)
                return new Schedule(assets.First().Schedule.Get(startTime, duration).Select(el => ScheduleItem.Dummy(el)).ToList());
            //Wenn die Anzahl der Kraftwerke groß ist, ist dieser Algorithmus schneller.
            else if (assets.Count() > 200)
                return GetSumScheduleManyAssets(assets, startTime, duration);

            if (startTime == default) startTime = assets.Select(asset => asset.Schedule.ToList().First().TimeStamp).Min();
            if (duration == default) duration = assets.Select(asset => asset.Schedule.ToList().Last().TimeStamp).Max() - startTime;

            var assetSchedules = assets.Select(asset => asset.Schedule.Get(startTime, duration));

            return new Schedule(assetSchedules.First().Select(el => ScheduleItem.Dummy(el)).ToList().Sum(assetSchedules.Skip(1)));
        }

        public static Schedule GetSumScheduleManyAssets(this IEnumerable<IAsset> assets, DateTime startTime = default, TimeSpan duration = default)
        {
            if (startTime == default) startTime = assets.Select(asset => asset.Schedule.ToList().First().TimeStamp).Min();
            if (duration == default) duration = assets.Select(asset => asset.Schedule.ToList().Last().TimeStamp).Max() - startTime;
            var assetSchedules = assets.Select(asset => asset.Schedule.Get(startTime, duration));
            return new Schedule(assetSchedules.ToList().Transpose().ToList().Select(item => ScheduleItem.Sum(item.Select(el => ScheduleItem.Dummy(el)).ToList())));
        }

        public static Schedule Sum(this List<ScheduleItem> mainSchedule, IEnumerable<IEnumerable<ScheduleItem>> schedules, bool createDummies = true) => mainSchedule.Sum(createDummies, schedules.ToArray());

        public static Schedule Sum(this List<ScheduleItem> mainSchedule, bool createDummies = true, params IEnumerable<ScheduleItem>[] schedules)
        {
            foreach (var schedule in schedules)
                if (createDummies)
                    mainSchedule = mainSchedule.Join(schedule, sum => sum.TimeStamp.Ticks, sched => sched.TimeStamp.Ticks, (a, b) => ScheduleItem.Dummy(a + b)).ToList();
                else
                    mainSchedule = mainSchedule.Join(schedule, sum => sum.TimeStamp.Ticks, sched => sched.TimeStamp.Ticks, (a, b) => a + b).ToList();

            return new Schedule(mainSchedule);
        }
        #endregion
    }
}
