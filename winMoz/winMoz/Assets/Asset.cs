﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Assets.Plants;
using winMoz.Assets.Loads;
using winMoz.Assets.Schedules;
using winMoz.Simulation;
using winMoz.Trading.Strategies;
using System.Collections.Generic;
using winMoz.Markets;
using System.Linq;

namespace winMoz.Assets
{
    /// <summary>
    /// Abstrakte Asset Klasse, die als Basis für alle Assets gilt. 
    /// </summary>
    public abstract class Asset : SimObject, IAsset
    {
        #region Attributes
        /// <summary>
        /// Anlagennennleistung in MW
        /// </summary>
        public float PowerMW { get; private set; }

        /// <summary>
        /// Daten zur Position des Assets
        /// </summary>
        public Location Location { get; private set; }

        /// <summary>
        /// Postleitzahl als Datenbankschlüssel zur Position des Assets
        /// </summary>
        public int LocationPostCode { get; private set; }

        /// <summary>
        /// Ökonomische Anlageneigenschaften
        /// </summary>
        public EconomicAttributes Economics { get; private set; }

        /// <summary>
        /// Name der Ökonomischen Eigenschaften als Datenbankschlüssel
        /// </summary>
        public string EconomicsSubType { get; private set; }

        /// <summary>
        /// Risikoaversion
        /// </summary>
        public float RiskAversion { get; private set; }

        /// <summary>
        /// Erwartete Abrufwahrscheinlichkeit im Regelreservemarkt nach Art der Regelleistung als String (wird für BulkInsert gebraucht)
        /// </summary>
        public string AsumedCallProbability { get; private set; }

        /// <summary>
        /// Erwartete Abrufwahrscheinlichkeit im Regelreservemarkt nach Art der Regelleistung
        /// </summary>
        public Dictionary<ControlReserveShortCode.ShortCode, float> _AsumedCallProbability { get; private set; }



        public void SetPower(float powerMW) => this.PowerMW = powerMW;

        public void SetLocation(Location location) => this.Location = location;
        #endregion
 
        public int BalancingGroupId { get; private set; }
        [NotMapped]
        public IBalancingGroup BalancingGroup { get; private set; }
        public Schedule Schedule { get; protected set; }

        public ControlReserveSchedule ControlReserveSchedule { get; protected set; }


        public TradingStrategyType TradingStrategy { get; private set; }

        #region Types & Booleans

        public bool IsPlant => !IsLoad;
        public bool IsLoad => this is Load_SLP | this is Load_RLM;
        public bool IsThermal => this is Plant_Thermal;
        public bool IsRenewable => this is Plant_PV | this is Plant_Wind /*| Type == Types.HYDRO*/ | this is Plant_Thermal && (this as Plant_Thermal).SubType == Plant_Thermal.SubTypes.BIOGAS;
        public bool IsFlexibility => this is Plant_Storage;
        public bool IsVariableRenewable => this is Plant_PV | this is Plant_Wind;
        #endregion
        public Asset() {
            _AsumedCallProbability = _AsumedCallProbability == default ? new Dictionary<ControlReserveShortCode.ShortCode, float>()
            {
                { ControlReserveShortCode.ShortCode.p_aFRR, 0.05F },
                { ControlReserveShortCode.ShortCode.n_aFRR, 0.05F },
                { ControlReserveShortCode.ShortCode.p_mFRR, 0.05F },
                { ControlReserveShortCode.ShortCode.n_mFRR, 0.05F }
            } : AsumedCallProbability.Split(';').Select(item => new { Key = (ControlReserveShortCode.ShortCode)Enum.Parse(typeof(ControlReserveShortCode.ShortCode), item.Split(':')[0]), Value = (float)Convert.ToSingle(item.Split(':')[1]) }).ToDictionary(item => item.Key, item => item.Value);
            InitializeSchedule(); }
        public Asset(int scenarioID): base(scenarioID) { InitializeSchedule(); }
        public Asset(IBalancingGroup bg, Enum subType, float powerMW, Location location, int scenarioID = 0, TradingStrategyType tradingStrategy = null, float riskAversion = 0.8F, Dictionary<ControlReserveShortCode.ShortCode, float> asumedCallProbability = null) : this(bg, EconomicAttributes.Load(subType), location, powerMW, scenarioID, tradingStrategy, riskAversion, asumedCallProbability) { }
        public Asset(IBalancingGroup bg,  Enum subType, float powerMW, int locationPostcode = default, int scenarioID = 0, TradingStrategyType tradingStrategy = null, float riskAversion = 0.8F, Dictionary<ControlReserveShortCode.ShortCode, float> asumedCallProbability = null) : this(bg, EconomicAttributes.Load(subType), locationPostcode == default ? Location.Random() : Location.FromPostcode(locationPostcode) , powerMW, scenarioID, tradingStrategy, riskAversion, asumedCallProbability) { }
        public Asset(IBalancingGroup bg,  EconomicAttributes economics , Location location, float powerMW,  int scenarioID = 0, TradingStrategyType tradingStrategy = null, float riskAversion = 0.8F, Dictionary<ControlReserveShortCode.ShortCode, float> asumedCallProbability = null) : this(scenarioID)
        {
            this.BalancingGroup = bg;
            this.BalancingGroupId = bg.Id;

            this.Economics = economics;
            this.EconomicsSubType = economics.SubType;

            this.Location = location;
            this.LocationPostCode = location.Postcode;
            RiskAversion = riskAversion;
            _AsumedCallProbability = asumedCallProbability == null ? new Dictionary<ControlReserveShortCode.ShortCode, float>()
            {
                { ControlReserveShortCode.ShortCode.p_aFRR, 0.05F },
                { ControlReserveShortCode.ShortCode.n_aFRR, 0.05F },
                { ControlReserveShortCode.ShortCode.p_mFRR, 0.05F },
                { ControlReserveShortCode.ShortCode.n_mFRR, 0.05F }
            } : asumedCallProbability;

            this.PowerMW = powerMW;
          
            this.TradingStrategy = tradingStrategy is null ? TradingStrategyType.Hedge : tradingStrategy;

            bg.Register(this);
        }

        public void SetTradingStrategy(TradingStrategyType tradingStrategy) => this.TradingStrategy = tradingStrategy;

        //public void UpdateAttributes() => AttributesId = Attributes.Id;
        public void SetNewAssumedCallProbability(ControlReserveShortCode.ShortCode shortCode, float asumedCallProbability)
        {
            this._AsumedCallProbability[shortCode] = 0.5F * (this._AsumedCallProbability[shortCode] + asumedCallProbability);
        }

        public Dictionary<ControlReserveShortCode.ShortCode, float> GetAssumedCallProbability()
        {
            return this._AsumedCallProbability;
        }

        public void SetBalancingGroup(BalancingGroup bg)
        {
            if (BalancingGroup == null && bg.Id == BalancingGroupId) BalancingGroup = bg;
        }

        public void UpdateBalancingGroup() => BalancingGroupId = BalancingGroup.Id;

        /*public void SetAttributesGroup(TAttributes attributes)
        {
            if (Attributes == null && attributes.Id == AttributesId) Attributes = attributes;
        }*/

        public virtual void InitializeSchedule()
        {
            this.Schedule = new Schedule();
            this.ControlReserveSchedule = new ControlReserveSchedule();
        }

        protected virtual void CallProbability()
        {
           
        }

        public virtual List<(DateTime TimeStamp, List<(float volume, float cost)>)> GetCostCurvePerMWh(DateTime TimeStamp, TimeSpan duration, TimeSpan step)
        {
            throw new NotImplementedException();
        }
        public virtual float GetCostForMWh(DateTime timeStamp, float Power)
        {
            throw new NotImplementedException();
        }

        public virtual void CutScheduleUntil(DateTime timeUntil)
        {
            var scheduleItems = this.Schedule.Get(timeUntil, TimeSpan.FromDays(10000));
            this.Schedule = new Schedule();
            this.Schedule.AddRange(scheduleItems);
        }

        public virtual void CutControlReserveScheduleUntil(DateTime timeUntil)
        {
            var scheduleItems = this.ControlReserveSchedule.Get(timeUntil, TimeSpan.FromDays(10000));
            this.ControlReserveSchedule = new ControlReserveSchedule();
            this.ControlReserveSchedule.AddRange(scheduleItems);
        }

        /// <summary>
        /// <seealso langword="abstract"/> Methode zur konkreten Berechnung des Einsatzfahrplans ab gewähltem Datum für gewählte Dauer
        /// </summary>
        /// <param name="startTime">Startzeitpunkt der Fahrplanberechnung</param>
        /// <param name="duration">Dauer für die der Fahrplan berechnet werden soll</param>
        /// <returns>Fahrplan ab Startzeit für Dauer</returns>
        public virtual void CalcSchedule(DateTime startTime, TimeSpan duration)
        {

        }
    }
}
