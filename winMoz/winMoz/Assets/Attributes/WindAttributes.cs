﻿
using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets.Plants;
using winMoz.Data.Access.DTO;
using winMoz.Helper;

namespace winMoz.Assets
{

    /// <summary>
    /// Eigenschaftenklasse für Windkraftanlagen
    /// </summary>
    public class WindPowerCurve : System.Collections.ObjectModel.Collection<(float WindSpeed, float NormPowerMW)>
    {
        public int WECModelId { get; private set; }
        public string WECModelName { get; private set; }

        public WindPowerCurve(IList<float> power, bool isScaleIntegrated = false)
        {
            var powCurve = new List<(float WindSpeed, float NormPowerMW)>();

            for (int i = 0; i < power.Count; i++)
                powCurve.Add(((0.5F * i), power[i]));

            SetNormalizedPowerCurve(powCurve, isScaleIntegrated);
        }

        private void SetNormalizedPowerCurve(IList<(float WindSpeed, float PowerMW)> list, bool isScaleIntegrated = false)
        {
            var pCintWind = Extensions.ResampleLinear(list.Select(el => el.WindSpeed).ToList(), list.Count * 5);
            var pCintPow = Extensions.ResampleLinear(list.Select(el => el.PowerMW).ToList(), list.Count * 5);

            foreach (var tpl in pCintWind.Zip(pCintPow, (a, b) => (a, b)))
                this.Add(tpl);

            // Normalize
            float normPower = isScaleIntegrated ? 1 : this.Select(el => el.NormPowerMW).Max();
            for (int i = 0; i < this.Count; i++)
                this[i] = ((float)Math.Round(this[i].WindSpeed, 1), (float)Math.Round(this[i].NormPowerMW / normPower, 4));
        }

        internal float GetPowerOutput(float windSpeed, float powerMW)
        {
            return this.Where(el => el.WindSpeed == windSpeed).Select(el => el.NormPowerMW).SingleOrDefault() * powerMW;
        }

    }
}









