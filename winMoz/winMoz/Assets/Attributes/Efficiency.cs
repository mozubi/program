﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using winMoz.Assets.Plants;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;
using MathNet.Numerics.Interpolation;

namespace winMoz.Assets
{
    public class Efficiency 
    {
        private Efficiency() 
        {
        }
            /// <summary>
            /// Konstruktor für die Effizienzklasse
            /// </summary>
            /// <param name="atPnom"> Effizienz im Arbeitspunkt </param>
            /// <param name="atPmin"> Effizienz bei minimaler Leistung </param>
            /// <param name="pMin"> Minimale Leistung im Verhältnis zur Leistung im Arbeitspunkt </param>
            /// <param name="pMax"> Maximale Leistung im Verhältnis zur Leistung im Arbeitspunkt</param>
            public Efficiency(float atPnom, float atPmin, float MinimumStableGeneration)
        {
            this.AtPnom = atPnom;
            this.AtPmin = atPmin;
            this.MinimimumStableGeneration = MinimumStableGeneration;
            SetCurve(MinimumStableGeneration, 1.1F);
        }
        public Efficiency(List<(float, float)> EfficiencyCurve)
        {
            this.AtPnom = EfficiencyCurve.Where(el => (el.Item1 == 1)).Select(el => el.Item2).First();
            this.AtPmin = EfficiencyCurve.First().Item2;
            SetCurve(EfficiencyCurve);
        }

        /// <summary>
        /// Wirkungsgrad der Anlage bei Nennlast zwischen 0-1
        /// </summary>
        public float AtPnom { get; private set; } = 1;
        /// <summary>
        /// Wirkungsgrad der Anlage bei minimaler Produktion zwischen 0-1
        /// </summary>
        public float AtPmin { get; private set; } = 1;
        /// <summary>
        /// Minimal mögliche Produktion relativ zur Nennlast zwischen 0-1
        /// </summary>
        public float MinimimumStableGeneration { get; private set; } = 0;
        /// <summary>
        /// Effizienzkurve mit erstem Wert als Effizienz bei minimaler Produktion, vorletztem Wert als Effizienz bei Nennlast und letztem Wert als Effizienz in 10% Überlast. Alle Werte zwischen 0-1
        /// </summary>
        [NotMapped]
        public List<float> Curve { get; private set; }
        [NotMapped]
        public List<float> PowerCurve { get; private set; }
        /// <summary>
        /// Interpolationsobjekt der Effizienzkurve. Damit kann die genaue Effizienz bei einem gegebenem Output berechnet werden.
        /// </summary>
        [NotMapped]
        private IInterpolation powerToEfficiency;
        /// <summary>
        /// Methode gibt die Effizienz bei gegebenem Power Output.
        /// </summary>
        /// <param name="power"> Der Poweroutput wird prozentual zur Nennlast angegeben</param>
        /// <returns></returns>
        public float AtPower(float power) => (float)powerToEfficiency.Interpolate(power);

        #region Private Methods
        /// <summary>
        /// Methode erstellt die Effizienzkurve. Dazu wird mit Hilfe eines quadratischen Polynoms zwischen Effizienz bei minimaler Leistung und Effizienz im Arbeitspunkt interpoliert. Außerdem wird angenommen, dass im Arbeitspunkt der Wirkungsgrad am höchsten ist.
        /// </summary>
        /// <param name="pMin"> Minimale Leistung im Verhältnis zur Leistung im Arbeitspunkt </param>
        /// <param name="pMax"> Maximale Leistung im Verhältnis zur Leistung im Arbeitspunkt </param>
        /// <returns></returns>
        private void SetCurve(float pMin, float pMax)
        {
            // set up coefficient matrix for quadratic interpolation
            // last line assumes that the highest point is at pNom
            var A = Matrix<float>.Build.DenseOfArray(new float[,]
            {
                { pMin*pMin, pMin, 1},
                { 1, 1, 1},
                { 2, 1, 0 }

            });
            var b = Vector<float>.Build.DenseOfArray(new float[]
            {
                AtPmin,
                AtPnom,
                0
            });
            var coeff = A.Solve(b);

            // create the x values for the efficiency curve
            this.PowerCurve = Generate.LinearSpaced(10, pMin, 1).ToList().ConvertAll(el => (float)el);
            this.PowerCurve.Add(pMax);
            this.Curve = PowerCurve.Select(el => coeff[0] * el * el + coeff[1] * el + coeff[2]).ToList();
            this.powerToEfficiency = CubicSpline.InterpolateAkimaSorted(this.PowerCurve.ConvertAll(el => (double)el).ToArray(), this.Curve.ConvertAll(el => (double)el).ToArray());

            //this.Curve = new List<float>() { 0.1 * this.AtPnom, 0.2 * this.AtPnom, 0.3 * this.AtPnom, 0.5 * this.AtPnom, 0.937 * this.AtPnom, 0.946 * this.AtPnom, 0.955 * this.AtPnom, 0.964 * this.AtPnom, 0.973 * this.AtPnom, 0.982 * this.AtPnom, 0.991 * this.AtPnom, 1 * this.AtPnom, 0.991 * this.AtPnom };
        }
        /// <summary>
        /// Funktion nimmt Datenpunkte einer Effizienzkurve auf, um daraus Splines zu berechnen, die dann die Effizienzkurve in gleichmäßigem Abstand ausgibt.
        /// </summary>
        /// <param name="EfficiencyCurve"></param>
        private void SetCurve(List<(float, float)> EfficiencyCurve)
        {
             IInterpolation interpolation = CubicSpline.InterpolateAkimaSorted(EfficiencyCurve.Select(el => (double)el.Item1).ToArray(), EfficiencyCurve.Select(el => (double)el.Item2).ToArray());
            var x = Generate.LinearSpaced(10, EfficiencyCurve.First().Item1, 1);

            this.Curve = x.Select(el => (float)interpolation.Interpolate((double)el)).ToList();

        }
        #endregion

        #region Public Methods
        public void SetEfficiencyAtPnom(float efficiency)
        {
            float efficiencyLoss = AtPnom - AtPmin;
            AtPnom = efficiency;
            AtPmin = AtPnom - efficiencyLoss;
            SetCurve();
        }
        public void SetCurve() => SetCurve(MinimimumStableGeneration, 1.1F);

        internal static Efficiency GetEfficiency(Plant_Thermal asset, bool Default = false)
        {
            // Verteilungen für Werte einbauen!
            //if (!Default)
            //    return Helper.Helper_XML.LoadThermalEfficiencies(Config.XmlFiles.Constants, asset.SubType, asset.AgeType).SetCurve();

            throw new NotImplementedException();
        }

        public List<float> GetPowerRange(float PowerInput)
        {
            //powerRange.Add(0);
            var powerRange = new List<float>(this.PowerCurve);
           
            for (int i = 0; i < powerRange.Count(); i++)
            {
                //PowerMW is electrical Power
                powerRange[i] *= PowerInput;
            }
            return powerRange;
        }




        public List<float> GetPowerRange(Plant_Storage plant)
        {
            List<float> powerRange = new List<float>();

            powerRange.Add(0);

            for (int i = 1; i < plant.Efficiency.Curve.Count; i++)
                powerRange.Add((i + 1) * plant.PowerMW / plant.Efficiency.Curve.Count);

            return powerRange;
        }

        public List<List<List<float>>> GetVariableUnitGenCosts(Plant_Thermal plant, DateTime startTime, TimeSpan duration, List<float> productionCosts) 
            => new List<List<List<float>>>(plant.Units.Select(unit => new List<List<float>>(Enumerable.Range(0, (int)duration.TotalHours - 1).Select(t => this.Curve.Select(j => productionCosts[t] / j).ToList()).ToList())).ToList());

        #endregion
    }
}
