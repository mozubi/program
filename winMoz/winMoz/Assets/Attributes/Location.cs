﻿using winMoz.Data.Access.Repositories;
using System.Collections.Generic;

namespace winMoz.Assets
{
    public class Location
    {
        /// <summary>
        /// Postleitahl
        /// </summary>
        public int Postcode { get; private set; }
        /// <summary>
        /// NUTS1 Ebene
        /// </summary>
        public string NUTS1 { get; private set; }
        /// <summary>
        /// NUTS2 Ebene
        /// </summary>
        public string NUTS2 { get; private set; }
        /// <summary>
        /// NUTS3 Ebene
        /// </summary>
        public string NUTS3 { get; private set; }
        /// <summary>
        /// Stadt oder Landkreis
        /// </summary>
        public string Municipality { get; private set; }
        /// <summary>
        /// Breitengrad in Grad
        /// </summary>
        public float Latitude { get; private set; }
        /// <summary>
        /// Längengrad in Grad
        /// </summary>
        public float Longitude { get; private set; }

        private Location() { }
        public Location(int Postcode, string nuts1, string nuts2, string nuts3, string municipality, float latitude, float longitude) 
        {
            this.Postcode = Postcode;
            this.NUTS1 = nuts1;
            this.NUTS2 = nuts2;
            this.NUTS3 = nuts3;
            this.Municipality = municipality;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
        public static Location FromNUTS2(string nuts2)
        {
            using (var repo = new ScenarioData_Repository())
                return repo.LocationRepository.GetFromNUTS2(nuts2);
        }

        public static Location FromNUTS3(string nuts3)
        {
            using (var repo = new ScenarioData_Repository())
                return repo.LocationRepository.GetFromNUTS3(nuts3);
        }

        public static List<Location> FromNUTS3s(List<string> nuts3s)
        {
            using (var repo = new ScenarioData_Repository())
                return repo.LocationRepository.GetFromNUTS3s(nuts3s);
        }

        public static Location FromPostcode(int postcode)
        {
            using (var repo = new ScenarioData_Repository())
                return repo.LocationRepository.GetFromPostcode(postcode);
        }

        public static List<Location> FromPostCodes(List<int> postcodes)
        {
            using (var repo = new ScenarioData_Repository())
                return repo.LocationRepository.GetFromPostcodes(postcodes);
        }


        public static Location Random()
        {
            using (var repo = new ScenarioData_Repository())
                return repo.LocationRepository.GetRandom();
        }

        public static List<string> GetAllNuts2()
        {
            using (var repo = new ScenarioData_Repository())
                return repo.LocationRepository.GetAllNuts2();
        }
    }
}
