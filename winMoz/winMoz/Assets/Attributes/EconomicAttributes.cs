﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using winMoz.Data.Access.Repositories;

namespace winMoz.Assets
{
    /// <summary>
    /// Ökonomische Eigenschaften für Assets
    /// </summary>
    public class EconomicAttributes 
    {
        public string SubType { get; private set; }

        /// <summary>
        /// Fixe Stromgestehungskosten in €/kW
        /// </summary>
        public float LCOE { get; private set; } = 10.0F;
        /// <summary>
        /// Investitions-Fixkosten in €/kW
        /// </summary>
        public float Capex { get; private set; }
        /// <summary>
        /// Fixe Betriebskosten in €/kWa 
        /// </summary>
        public float OpexFix { get; private set; }

        /// <summary>
        /// Variable Betriebskosten in €/kW
        /// </summary>
        public float OpexVar { get; private set; }

        /// <summary>
        /// Vollaststunden einer Anlage pro Jahr in h
        /// </summary>
        public float FullLoadHours { get; private set; }
        /// <summary>
        /// Lebensdauer einer Anlage in y
        /// </summary>
        public float LifeTime { get; private set; }

        public EconomicAttributes() { }

        public EconomicAttributes(string subType, float lCOE = 0, float capex = 0, float opexFix = 0, float opexVar = 0, float fullLoadHours = 0, float lifeTime = 0)
        {
            SubType = subType ?? throw new ArgumentNullException(nameof(subType));
            LCOE = lCOE;
            Capex = capex;
            OpexFix = opexFix;
            OpexVar = opexVar;
            //Efficiency = efficiency;
            FullLoadHours = fullLoadHours;
            LifeTime = lifeTime;
        }

        public static EconomicAttributes Load(Enum subType) => Load(subType.ToString());

        public static EconomicAttributes Load(string subType)
        {
            using (var repo = new ScenarioData_Repository())
                return repo.EconomicsRepository.GetBySubType(subType);
        }

        #region
        // Konstantin 2013
        //public static float Calc_LCOE(float CAPEX, float OPEXfix,float OPEXvar, float powerKW, int lifeTime, int loadFactor, float discRate )
        //{
        //    float lcoe = 0;

        //    float opCostAnnualVAR = (OPEXvar * powerKW * loadFactor);
        //    float opCostAnnualFIX = (OPEXfix * powerKW);

        //    float opCostAnnual = opCostAnnualFIX+opCostAnnualVAR;
        //    float invest = CAPEX * powerKW;

        //    float costAnnualDisc = 0;
        //    float energyPerYear = powerKW * loadFactor;
        //    float energyTotalDisc = 0;

        //    for (int t = 1; t <= lifeTime; t++)
        //    {
        //        costAnnualDisc += (opCostAnnual) / (Math.Pow((1 + discRate), t));
        //        energyTotalDisc += (energyPerYear) / (Math.Pow((1 + discRate), t));
        //    }

        //    float costTotal = CAPEX + costAnnualDisc;

        //    lcoe = costTotal / energyTotalDisc;

        //    return
        //        lcoe/1000;
        //}

        /// <summary>
        /// Berechnung der Stromgestehungskosten nach Geldermann 2014 - Erweitert um CO2-EB-Kosten - Validiert mit Daten aus arrhenius-Studie-KKW-Main 2009
        /// </summary>
        /// <param name="powerKW">Leistung in kW</param>
        /// <param name="efficiency">Wirkungsgrad</param>
        /// <param name="CAPEX">Investitionskosten in €/kW</param>
        /// <param name="fuelPrice">Brennstoffkosten in €/kWh</param>
        /// <param name="CO2price">CO2-Zertifikatspreis in €/tCO2</param>
        /// <param name="lifeTime">Ökon. Lebensdauer der Anlage</param>
        /// <param name="fullLoadHours">Volllaststunden der Anlage in h/a</param>
        /// <param name="annuity">Annuität als Fixwert in %/a</param>
        /// <returns>Stromgestehungskosten in EUR/MWh</returns
        public static float Calc_PCG(float powerMW, float efficiency, float CAPEX, float fuelPrice, float CO2price, float CO2emiFactor, float lifeTime, float fullLoadHours, float annuity)
        {
            float powerKW = powerMW * 1000;
            //24.12.18 - ÜBERARBEITEN -> gibt fehlerhafte Werte zurück für PV!!! erneut validieren
            if (efficiency == 0)
                return 0;

            float pcg = 0;
            float operationalAvailabilty = fullLoadHours; // hours/a
            float energyAnnual = powerKW * operationalAvailabilty; // kWh/a

            float ki = CAPEX;
            float Ki = ki * annuity * powerKW; // €/a

            float fA = (energyAnnual) / (powerKW * operationalAvailabilty); // rel. Auslastung
            float fV = operationalAvailabilty / 8760; // rel. Verfügbarkeit
            float fB = 1; // Faktor für Handhabungskosten BS

            float kb = efficiency * fB * fuelPrice * powerKW; // €/hour - Handhabungskosten Brennstoff

            float timeEff = fA * fV * 8760;  // Effektive Betriebszeit
            float Kb = kb * timeEff; // €/a - Brennstoffkosten

            float kc = CO2emiFactor; // tCO2/GJ nach DHSt-Liste Standard-Emissionsfaktoren
            float kcAnnual = kc * (energyAnnual * 3.6F / efficiency); // CO2-Emissionen in t/a
            float Kc = kcAnnual / 1000 * CO2price;

            float E = powerKW * timeEff;

            float K = Ki + Kb + Kc; // €/a

            pcg = (K / E) * 1000; // €/MWh

            return
                pcg;

        }
        #endregion

    }
}