using System;
using winMoz.Assets.Plants;
using winMoz.Data.Access.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using winMoz.Information;

namespace winMoz.Assets
{
    public class Tynp 
    {
        public int Category { get; private set; }
        public Tynp()
        {
            Costs = new Costs();
        }

        /// <summary>
        /// Verwendeter Brennstoff des Kraftwerks 
        /// </summary>
        public Commodities.Commodity Commodity { get; private set; }
        /// <summary>
        /// Alterstyp des Kraftwerks
        /// </summary>
        public Plant_Thermal.AgeTypes AgeType { get; private set; }
        /// <summary>
        /// Daten zur Kraftwerkseffizienz
        /// </summary>
        public Efficiency Efficiency { get; private set; }
        /// <summary>
        /// CO2 Emissionen des Kraftwerks in kg/GJ
        /// </summary>
        public float CO2Emissions { get; private set; }
        /// <summary>
        /// Daten zu variablen Betriebskosten des Kraftwerks in €/MW
        /// </summary>
        public Costs Costs { get; private set; }
        /// <summary>
        /// Daten zu Übergangszeiten des Kraftwerks
        /// </summary>
        public Time TransitionTime { get; private set; }
        /// <summary>
        /// Daten zu geplanten und ungeplanten Kraftwerksausfällen
        /// </summary>
        public Unavailabilities Unavailability { get; private set; }
        /// <summary>
        /// Minimal mögliche Auslastung relativ zur Nennlast zwischen 0-1
        /// </summary>
        public float MinimumStableGeneration => Efficiency.MinimimumStableGeneration;
        
        public static Tynp Load(Commodities.Commodity commodity, Plant_Thermal.AgeTypes age)
        {
            using (var rep = new TynpRepository())
            {
                return rep.GetByCommodityAndAge(commodity, age);
            }
        }
    }

    #region TYNP Classes
    [Owned]
    public class Costs 
    {
        public Costs()
        { }

        public Costs(float variableOM, float startUp_Fix_Cold, float startUp_Fix_Warm, float startUp_Fix_Hot, float fuelConsumption_Cold, float fuelConsumption_Warm, float fuelConsumption_Hot)
        {
            this.VariableOM = variableOM;
            this.StartUp_Fix_Cold = startUp_Fix_Cold;
            this.StartUp_Fix_Warm = startUp_Fix_Warm;
            this.StartUp_Fix_Hot = startUp_Fix_Hot;
            this.FuelConsumption_Cold = fuelConsumption_Cold;
            this.FuelConsumption_Warm = fuelConsumption_Warm;
            this.FuelConsumption_Hot = fuelConsumption_Hot;
        }
        /// <summary>
        /// Variabale Betriebs- und Wartunkgsosten in €/MWh
        /// </summary>
        public float VariableOM { get; private set; }
        /// <summary>
        /// Fixe Startkosten bei einem Kaltstart in €/MW
        /// </summary>
        public float StartUp_Fix_Cold { get; private set; }
        /// <summary>
        /// Fixe Startkosten bei einem Warmstart in €/MW
        /// </summary>
        public float StartUp_Fix_Warm { get; private set; }
        /// <summary>
        /// Fixe Startkosten bei einem Heißstart in €/MW
        /// </summary>
        public float StartUp_Fix_Hot { get; private set; }
        /// <summary>
        /// Brennstoffverbrauch bei einem Kaltstart in GJ/MW
        /// </summary>
        public float FuelConsumption_Cold { get; private set; } // Fuel Consumption!! UMRECHNEN!
       /// <summary>
       /// Brennstoffverbrauch bei einem Warmstart in GJ/MW
       /// </summary>
        public float FuelConsumption_Warm { get; private set; }
        /// <summary>
        /// Brennstoffverbrauch bei einem Heißstart in GJ/MW
        /// </summary>
        public float FuelConsumption_Hot { get; private set; }
    }
    [Owned]
    public class Time 
    {
        public Time()
        {
        }

        public Time(TimeSpan MinHoursOn, TimeSpan MinHoursOff, TimeSpan transition_HotCold, TimeSpan transition_HotWarm)
        {
            this.MinHoursOn = MinHoursOn;
            this.MinHoursOff = MinHoursOff;
            this.HotCold = transition_HotCold;
            this.HotWarm = transition_HotWarm;
        }
        /// <summary>
        /// Minimale Betriebszeit pro Tag in h
        /// </summary>
        public TimeSpan MinHoursOn { get; private set; }
        /// <summary>
        /// Minimale Ruhezeit pro Tag in h
        /// </summary>
        public TimeSpan MinHoursOff { get; private set; }
        /// <summary>
        /// Übergangszeit von heiß zu warm in h
        /// </summary>
        public TimeSpan HotWarm { get; private set; }
        /// <summary>
        /// Übergangszeit von heiß zu kalt in h
        /// </summary>
        public TimeSpan HotCold { get; private set; }

    }
    [Owned]
    public class Unavailabilities 
    {
        public Unavailabilities()
        {
        }

        public Unavailabilities(float forced_AnnualRate, float forced_MeanTimeToRepair, float planned_AnnualDays, float planned_WinterRate)
        {
            this.Forced_AnnualRate = forced_AnnualRate;
            this.Forced_MeanTimeToRepair = forced_MeanTimeToRepair;
            this.Planned_AnnualDays = planned_AnnualDays;
            this.Planned_WinterRate = planned_WinterRate;
        }
        /// <summary>
        /// Ungeplante jährliche Ausfallzeit zwischen 0-1
        /// </summary>
        public float Forced_AnnualRate { get; private set; }
        /// <summary>
        /// Mittlere Reparaturzeit in d
        /// </summary>
        public float Forced_MeanTimeToRepair { get; private set; }
        /// <summary>
        /// Geplante jährliche Ausfallzeit zwischen 0-1
        /// </summary>
        public float Planned_AnnualDays { get; private set; }
        /// <summary>
        /// Winterrate zwischen 0-1
        /// </summary>
        public float Planned_WinterRate { get; private set; }

        // Verteilungen für Werte einbauen!
    }
    #endregion
}
