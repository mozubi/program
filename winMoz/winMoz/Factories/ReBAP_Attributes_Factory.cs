﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.Elements;
using winMoz.Simulation;

namespace winMoz.Factories
{
    public static class ReBAP_Attributes_Factory
    {
        public static SimEventAttributes GetDefault(DateTime start)
        {
            return new SimEventAttributes(start, start.AddDays(2).AddMinutes(15), start.AddDays(1), new TimeSpanSave(0, 0, 1, 0, 0), new TimeSpanSave(0, 0, 0, 0, 15));
        }
    }
}
