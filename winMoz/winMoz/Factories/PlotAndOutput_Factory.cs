﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;
using winMoz.Markets.Elements;
using winMoz.Data.Access;

namespace winMoz.Factories
{
    public static class PlotAndOutput_Factory
    {
        public static void ClearRepository(string outputType)
        {
            switch (outputType)
            {
                case "Plot":
                    {
                        DbHelper.ReCreateDb<PlotAndOutput_Variables_Plot_Context>();
                        break;
                    }
                case "Console":
                    {
                        DbHelper.ReCreateDb<PlotAndOutput_Variables_Console_Context>();
                        break;
                    }
                case "CSV":
                    {
                        DbHelper.ReCreateDb<PlotAndOutput_Variables_CSV_Context>();
                        break;
                    }
            }

        }

        public static void AddBid(string outputType, DateTime start, DateTime end, string market, bool withStep, bool withBlock, bool differByPlantTypeAndSubType, bool onlyOnePlantType, bool onlyOnePlant, bool onlyOneControlReserveType, bool onlyCapacityBids, string plant_Type = null, string subType = null, int plant_Id = 0, string controlReserveType = null)
        {
            var Dto = GetDTOBid(start, end, market, withStep, withBlock, differByPlantTypeAndSubType, onlyOnePlantType, onlyOnePlant, onlyOneControlReserveType, onlyCapacityBids, plant_Type, subType, plant_Id, controlReserveType);
            AddToRepo(outputType, Dto);
        }

        public static PlotAndOutput_Variables_Dto GetDTOBid(DateTime start, DateTime end, string market, bool withStep, bool withBlock, bool differByPlantTypeAndSubType, bool onlyOnePlantType, bool onlyOnePlant, bool onlyOneControlReserveType, bool onlyCapacityBids, string plant_Type = null, string subType = null, int plant_Id = 0, string controlReserveType = null)
        {
            PlotAndOutput_Element Element = new PlotAndOutput_Element_Bid(market, withStep, withBlock, differByPlantTypeAndSubType, onlyOnePlantType, onlyOnePlant, onlyOneControlReserveType, onlyCapacityBids, plant_Type, subType, plant_Id, controlReserveType);
            PlotAndOutput_Times Times = new PlotAndOutput_Times_Durations(start, end);
            PlotAndOutput_Variables_Dto dto = new PlotAndOutput_Variables_Dto(Element, Times);
            return dto;
        }

        public static void AddMeritOrder(string outputType, DateTime start, DateTime end, string simEvent, bool onlyOneControlReserveType, string controlReserveType = null)
        {
            var Dto = GetDTOMeritOrder(start, end, simEvent, onlyOneControlReserveType, controlReserveType);
            AddToRepo(outputType, Dto);
        }

        public static PlotAndOutput_Variables_Dto GetDTOMeritOrder(DateTime start, DateTime end, string simEvent, bool onlyOneControlReserveType, string controlReserveType = null)
        {
            PlotAndOutput_Element Element = new PlotAndOutput_Element_MeritOrder(simEvent, onlyOneControlReserveType, controlReserveType);
            PlotAndOutput_Times Times = new PlotAndOutput_Times_Durations(start, end);
            PlotAndOutput_Variables_Dto dto = new PlotAndOutput_Variables_Dto(Element, Times);
            return dto;
        }

        public static void AddCoupledMeritOrder(string outputType, DateTime start, DateTime end, bool marketAreaSpecified, MarketAreaEnum marketArea = null)
        {
            var Dto = GetDTOCoupledMeritOrder(start, end, marketAreaSpecified, marketArea);
            AddToRepo(outputType, Dto);
        }

        public static PlotAndOutput_Variables_Dto GetDTOCoupledMeritOrder(DateTime start, DateTime end, bool marketAreaSpecified, MarketAreaEnum marketArea = null)
        {
            PlotAndOutput_Element Element = new PlotAndOutput_Element_CoupledMeritOrder(marketAreaSpecified, marketArea);
            PlotAndOutput_Times Times = new PlotAndOutput_Times_Durations(start, end);
            PlotAndOutput_Variables_Dto dto = new PlotAndOutput_Variables_Dto(Element, Times);
            return dto;
        }

        public static void AddCoupledSchedule(string outputType, DateTime start, DateTime end, bool differByPlantTypeAndSubType, bool onlyOnePlantType, bool onlyOnePlant, bool withPrices, bool isOutputSimTimeSet, string plant_Type = null, string subType = null, int plant_Id = 0, DateTime outputSimTime = default)
        {
            var Dto = GetDTOSchedule(start, end, differByPlantTypeAndSubType, onlyOnePlantType, onlyOnePlant, withPrices, isOutputSimTimeSet, plant_Type, subType, plant_Id, outputSimTime);
            AddToRepo(outputType, Dto);
        }

        public static PlotAndOutput_Variables_Dto GetDTOSchedule(DateTime start, DateTime end, bool differByPlantTypeAndSubType, bool onlyOnePlantType, bool onlyOnePlant, bool withPrices, bool isOutputSimTimeSet, string plant_Type = null, string subType = null, int plant_Id = 0, DateTime outputSimTime = default)
        {
            PlotAndOutput_Element Element = new PlotAndOutput_Element_Schedule(differByPlantTypeAndSubType, onlyOnePlantType, onlyOnePlant, withPrices, isOutputSimTimeSet, plant_Type, subType, plant_Id, outputSimTime);
            PlotAndOutput_Times Times = new PlotAndOutput_Times_Durations(start, end);
            PlotAndOutput_Variables_Dto dto = new PlotAndOutput_Variables_Dto(Element, Times);
            return dto;
        }

        public static void AddMarketResults(string outputType, DateTime start, DateTime end, bool isMarketTypeSpecified, bool differByMarketType, bool volumeVersusPrice, bool timeVersusVolume, bool timeVersusPrice, bool timeVersusVolumeAndPrice, string marketResultType = null)
        {
            var Dto = GetDTOMarketResults(start, end, isMarketTypeSpecified, differByMarketType, volumeVersusPrice, timeVersusVolume, timeVersusPrice, timeVersusVolumeAndPrice, marketResultType);
            AddToRepo(outputType, Dto);
        }

        public static PlotAndOutput_Variables_Dto GetDTOMarketResults(DateTime start, DateTime end, bool isMarketTypeSpecified, bool differByMarketType, bool volumeVersusPrice, bool timeVersusVolume, bool timeVersusPrice, bool timeVersusVolumeAndPrice, string marketResultType = null)
        {
            PlotAndOutput_Element Element = new PlotAndOutput_Element_MarketResults(isMarketTypeSpecified, differByMarketType, volumeVersusPrice, timeVersusVolume, timeVersusPrice, timeVersusVolumeAndPrice, marketResultType);
            PlotAndOutput_Times Times = new PlotAndOutput_Times_Durations(start, end);
            PlotAndOutput_Variables_Dto dto = new PlotAndOutput_Variables_Dto(Element, Times);
            return dto;
        }

        public static void AddToRepo(string outputType, PlotAndOutput_Variables_Dto dto)
        {
            switch (outputType)
            {
                case "Plot":
                    {
                        PlotAndOutput_Variables_Plot_Repository repoPlot = new PlotAndOutput_Variables_Plot_Repository();
                        repoPlot.Add(dto);
                        break;
                    }
                case "Console":
                    {
                        PlotAndOutput_Variables_Console_Repository repoConsole = new PlotAndOutput_Variables_Console_Repository();
                        repoConsole.Add(dto);
                        break;
                    }
                case "CSV":
                    {
                        PlotAndOutput_Variables_CSV_Repository repoPlot = new PlotAndOutput_Variables_CSV_Repository();
                        repoPlot.Add(dto);
                        break;
                    }
            }
        }

    }
}
