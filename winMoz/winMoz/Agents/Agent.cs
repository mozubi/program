﻿using System;
using winMoz.Simulation;

namespace winMoz.Agents
{
    public abstract class Agent : SimObjectWithPrices, IAgent
    {
        #region Agent types & role
        public enum Types { TSO, DSO, BG }
        private readonly Types typeId;

        /// <summary>
        /// Typbeschreibung des Agenten
        /// </summary>
        public string Type { get => Enum.GetName(typeof(Types), this.typeId); }
        #endregion

        protected Agent() { }

        protected internal Agent(Types type, int scenarioID = 0) : base(scenarioID)
        {
            this.typeId = type;
        }

    }
}
