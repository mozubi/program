﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using winMoz.Assets;
using winMoz.Assets.Schedules;
using winMoz.Markets;
using winMoz.Portfolios;
using winMoz.Markets.Elements;

namespace winMoz.Agents
{
    public class BalancingGroup : Agent, IBalancingGroup
    {
        public DSO DSO { get; private set; }
        [NotMapped]
        public Trader Trader { get; private set; } = new Trader();
        //public IReadOnlyCollection<IAsset> Assets => _Assets.ToList();
        //private HashSet<IAsset> _Assets = new HashSet<IAsset>();
        public List<IAsset> Assets { get; private set; } = new List<IAsset>();
        public int DSOId { get; private set; }
        bool gateClosedSubscribed = false;

        #region Constructors

        private BalancingGroup() : base(Types.BG) { }
        public BalancingGroup(DSO dso, int scenarioID = 0) : base(Types.BG, scenarioID)
        {
            this.DSO = dso;
            DSO.Register(this);
            SubscribeToGateClosed();
            DSOId = dso.Id;           
        }
        #endregion

        public void SetDSO(DSO dso)
        {
            if (DSO == null && dso.Id == DSOId) DSO = dso;
        }
        public void SubscribeToGateClosed()
        {
            if (!gateClosedSubscribed)
            {
                DSO.TSO.GateClosed += SubmitBalanceSchedule;
                gateClosedSubscribed = true;
            }
        }

         public List<(DateTime TimeStamp, float Input, float Output, float Diff)> SubmitBalanceSchedule(DateTime timeStamp, TimeSpan duration)
         {
            this.Assets.ToList().ForEach(asset => asset.CalcSchedule(timeStamp, duration));

            // if there are only bids associated to assets, this represents the whole balance. if there are financial-only bids, these need to be taken into account
            //return this.Assets.GetSumSchedule(timeStamp, duration).Select(el => (el.TimeStamp, Math.Max(0, el.Power) + Math.Max(0, el.PowerProcured), Math.Min(0, el.Power) + Math.Min(0, el.PowerProcured))).ToList();
            return this.Assets.GetSumSchedule(timeStamp, duration).Select(el => (el.TimeStamp, Math.Max(0, el.Power), Math.Min(0, el.Power), el.PowerDiff)).ToList();
        }

        public BalancingGroup(DSO dso, params IAsset[] assets) : this(dso)
        {
            Register(assets);
        }


        public void Register(IAsset asset)
        {
            this.Trader.Add(asset);

            
            //throw error if something gets added twice
            if (this.Assets.Contains(asset))
                throw new Exception();
            else
            {
                this.Assets.Add(asset);
            }
        }

        public void UpdateDSO() => DSOId = DSO.Id;

        public void Register(IEnumerable<IAsset> assets) => assets.ToList().ForEach(asset => this.Register(asset));

        //public void Register(IEnumerable<IAsset> assets)
        //{
        //    this.Trader.Add(assets.ToArray());

        //    foreach (var asset in assets)
        //        if (!this._Assets.Add(asset))
        //            throw new Exception();
        //}

        public void SubscribeToMarket(params Market[] markets) => Trader.SubscribeToMarket(markets);

    }
}

