﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace winMoz.Agents
{
    public class TSO : Agent
    {
        BlockingCollection<IEnumerable<(DateTime TimeStamp, float Input, float Output, float Diff)>> BalancingGroupTimeSeries = new BlockingCollection<IEnumerable<(DateTime TimeStamp, float Input, float Output, float Diff)>>();

        public delegate IEnumerable<(DateTime TimeStamp, float Input, float Output, float Diff)> GateClosure(DateTime timeStamp, TimeSpan duration);
        public event GateClosure GateClosed;

        public List<DSO> dsos { get; private set; } = new List<DSO>();

        protected TSO() { }
        public TSO(int scenarioID = 0) : base(Agent.Types.TSO, scenarioID)
        {
        }

        /// <summary>
        /// Methode sammelt die Summenfahrpläne aller Bilanzkreise über den Zeitraum eines Tages ein.
        /// </summary>
        /// <param name="timeStamp">Zeitpunkt ab welchem die Summenfahpläne eingesammelt werden.</param>
        public void CloseGate(DateTime timeStamp)
        {
            var subs = GateClosed?.GetInvocationList();
            //empty old data; depending on what is needed later it can be changed
            BalancingGroupTimeSeries = new BlockingCollection<IEnumerable<(DateTime TimeStamp, float Input, float Output, float Diff)>>();
            Parallel.ForEach(subs, sub => BalancingGroupTimeSeries.Add((IEnumerable<(DateTime TimeStamp, float Input, float Output, float Diff)>)sub.DynamicInvoke(timeStamp.AddDays(-1), TimeSpan.FromDays(1))));
        }

        /// <summary>
        /// Funktion gibt eine Liste über das Regelzonensaldo zurück. Dabei sind nur die Werte enthalten, die durch das letzte CloseGate gespeichert wurden.
        /// </summary>
        /// <returns>Liste mit Zeitstempeln und passendem Regelzonensaldo über den Zeitraum eines Tages</returns>
        public List<(DateTime timeStamp, float saldo, float saldoFromDiff)> getSaldo()
        {
            //Initialisierung
            List<(DateTime timeStamp, float saldo, float saldoFromDiff)> saldolist = BalancingGroupTimeSeries.First().Select(el => (el.TimeStamp, 0.0F, 0.0F)).ToList();
            foreach (var bg in BalancingGroupTimeSeries)
            {
                saldolist = saldolist.Zip(bg, (saldo, bgel) => (saldo.timeStamp, saldo.saldo + bgel.Input +bgel.Output, saldo.saldoFromDiff + bgel.Diff)).ToList();
            }
            return saldolist.Select(item => (item.timeStamp, -item.saldo, -item.saldoFromDiff)).ToList();
        }
    }
}
