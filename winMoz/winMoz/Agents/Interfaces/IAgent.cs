﻿using winMoz.Simulation;

namespace winMoz.Agents
{
    public interface IAgent : ISimObject, ISimObjectWithPrices
    {
        string Type { get; }
    }
}
