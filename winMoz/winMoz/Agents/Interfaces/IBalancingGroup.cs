﻿using System;
using System.Collections.Generic;
using winMoz.Assets;

namespace winMoz.Agents
{
    public interface IBalancingGroup : IAgent
    {
        List<(DateTime TimeStamp, float Input, float Output, float Diff)> SubmitBalanceSchedule(DateTime timeStamp, TimeSpan duration);

        List<IAsset> Assets { get; }

        void Register(IAsset asset);

    }
}
