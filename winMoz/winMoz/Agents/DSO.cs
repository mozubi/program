﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using winMoz.Assets.Loads;
using winMoz.Markets;
using winMoz.Portfolios;
using winMoz.Trading.Strategies;
using winMoz.Markets.Elements;
using winMoz.Markets.EoMMarkets;

namespace winMoz.Agents
{
    public class DSO : Agent
    {
        public int TSOId { get; private set; }
        public TSO TSO { get; private set; }
        // HashSet stellt sicher, dass keine BalancingGroup doppelt hinzugefügt werden kann.
        public List<BalancingGroup> _BalancingGroups { get; private set; } = new List<BalancingGroup>();
        public IReadOnlyList<IBalancingGroup> BalancingGroups => _BalancingGroups.ToList();
        public BalancingGroup TimeSeries { get; private set; }

        [NotMapped]
        public SLP SLP { get; private set; }

        private DSO() : base(Types.DSO)
        {

        }

        public DSO(TSO tso, int scenarioID = 0) : base(Types.DSO, scenarioID)
        {
            this.TSO = tso;
            this.TSO.dsos.Add(this);
            this.SLP = new SLP((SLP.Profile.H0, 2000000), (SLP.Profile.G0, 3000000));
            BuildBalancingGroups();
            TSOId = tso.Id;
        }

        public void SetTSO(TSO tso)
        {
            if (TSO == null)
            {
                TSO = tso;
                this.SLP = new SLP((SLP.Profile.H0, 2000000), (SLP.Profile.G0, 3000000));
                BuildBalancingGroups();
            }
        }
        public void UpdateTSO() => TSOId = TSO.Id;

        public void BuildBalancingGroups()
        {
            this.TimeSeries = new BalancingGroup(this);
            this.SLP = new SLP((SLP.Profile.H0, 2000000), (SLP.Profile.G0, 3000000));
            var losses = new GridLosses(this.TimeSeries, this.SLP);
            this.TimeSeries.Trader.Add(new Unlimited(new[] { losses }));//, new Type[] { typeof(IntradayMarket) }));

            //var diff = new Difference(this.TimeSeries, Assets.Asset<Assets.AssetAttributes>.Types.FINANCIAL, this.SLP.Profiles.Sum(profile => profile.EnergyAnnualkWh / 1000));
            //var diff = new Difference(this.TimeSeries, this.SLP.Profiles.Sum(profile => profile.EnergyAnnualkWh / 1000));
            //this.TimeSeries.Register(diff);
            //this.TimeSeries.Trader.Add(new Hedge(new[] { diff }));
        }

        public void Register(IBalancingGroup bg)
        {
            if (TimeSeries != null) this._BalancingGroups.Add(bg as BalancingGroup);
        }

        public void SubscribeToMarket(params Market[] markets) => new[] {this.TimeSeries  }.ToList().ForEach(bg => bg.SubscribeToMarket(markets));


    }
}
