﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets;
using winMoz.Helper;

namespace winMoz.Information
{
    public static class HSum
    {
        static HSumStorage storage = new HSumStorage();

        /// <summary>
        /// Methode gibt HSum einer bestimmten Commodity zu einem Zeitpunkt zurück
        /// </summary>
        public static float GetHSum(DateTime timeStamp, string nuts2)
        {
            int Year = timeStamp.Year;
            return storage.GetHSum(Year, nuts2);
        }

        public static float GetSigmoid(float theta) => storage.GetSigmoid(theta);
    }

    internal class HSumStorage
    {
        #region Sigmoid-Koeffizienten nach TUM-Profil (D14)
        float sA = 3.1850191F;
        float sB = -37.4124155F;
        float sC = 6.1723179F;
        float sD = 0.0761096F;
        float tht0 = 40.0F;
        #endregion
        public Dictionary<string, float> LocationAndHSum { get; private set; } = new Dictionary<string, float>();

        private int CachedYear;

        /// <summary>
        /// Methode berechnet den Sigmoid-Wert für die Berechnung des SLP-Tageswertes für bestimmte Temperatur nach TUM Sigmoidfunktion
        /// </summary>
        /// <param name="theta">Temperatur</param>
        /// <returns>Sigmoidwert</returns>
        internal float GetSigmoid(float theta) => (float)(this.sA / (1 + Math.Pow((this.sB / (theta - this.tht0)), Math.Round(this.sC)))) + this.sD;

        private void CacheHSums(int year)
        {
            DateTime start = new DateTime(year, 1, 1);

            int days = start.GetDaysOfYear();

            var temperatures = new List<(DateTime TimeStamp, float Temperature)>();

            var locations = Location.GetAllNuts2();

            var locationAndTemperature = locations.Select(location => new { location, TemperaturList = Weather.GetTemperature(new DateTime(year, 1, 1), TimeSpan.FromDays(days), location).ToList().Resample(Resampling.TimeStep.Day).ToList().Select(item => item.Item2).ToList() }).ToList();

            LocationAndHSum = locationAndTemperature.Select(location => new { location.location, Hsum = location.TemperaturList.Select(temp => GetSigmoid(temp)).Sum() }).ToDictionary(sigm => sigm.location, sigm => sigm.Hsum);

            CachedYear = year;
        }

        public float GetHSum(int year, string nuts2)
        {
            if (CachedYear == default || CachedYear != year)
                CacheHSums(year);
            return LocationAndHSum[nuts2];
        }
    }
}
