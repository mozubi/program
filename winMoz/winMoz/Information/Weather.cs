﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using winMoz.Agents;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Simulation;
using winMoz.Tools.Prediction;

namespace winMoz.Information
{
    /// <summary>
    /// Wetterklasse - Enthält alle Eigenschaften und Methoden zur Abfrage und Verwertung von Wetterdaten
    /// </summary>
    public static class Weather
    {
        static WxContext wxCtx = new WxContext(Config.ConnStrings.Weather);
        const float INSTALLED_HYDRO_POWER = 3.87F * 10e3F; //installierte Leistung 2021

        public static void CacheWeather(DateTime from, DateTime to) => wxCtx.CacheWxData(from, (to - from));

        #region Public-Methods
        public static List<(DateTime TimeStamp, float Run_of_river)> GetHydroForecast(DateTime time, TimeSpan duration, ISeedable seedable = null, bool forecast = true, bool isExtended = false)
        {
            List <(DateTime TimeStamp, float Run_of_river)> hydroActual, hydroForecast = new List<(DateTime TimeStamp, float Run_of_river)>();
            using (var repo = new PredictionDataRepository())
            {
                hydroActual = repo
                    .GenerationRepo
                    .Get()
                    .Where(el => (el.TimeStamp >= time && el.TimeStamp < time + duration))
                    .Select(item => (item.TimeStamp, item.Hydro_Run_of_river_and_poundage.GetValueOrDefault(0) / INSTALLED_HYDRO_POWER))
                    .ToList();
            }
            if (forecast)
            {
                hydroForecast = Forecast.GetForecast(hydroActual, forecastType: Forecast.Type.Run_Of_River, seed: seedable.GetSeed())
                    .Select(el => (el.TimeStamp, Math.Max(0, el.value)))
                    .ToList();
            }
            else
            {
                hydroForecast = hydroActual;
            }


            return hydroForecast;

        }
        public static List<(DateTime TimeStamp, float T_env, float RadDirHor, float RadDiffHor)> GetSolarForecast(DateTime time, TimeSpan duration, int seed = default, string nuts2 = "DE", bool forecast = true, bool useWeatherYear = true) => GetSolar(time, duration, seed, nuts2, forecast, useWeatherYear: useWeatherYear);
        public static List<(DateTime TimeStamp, float T_env, float RadDirHor, float RadDiffHor)> GetSolar(DateTime time, TimeSpan duration, string nuts2 = "DE", bool isExtended = false, bool useWeatherYear = true) => GetSolar(time, duration, nuts2: nuts2, forecast: false, isExtended : isExtended, useWeatherYear: useWeatherYear);
        public static List<(DateTime TimeStamp, float T_env, float RadDirHor, float RadDiffHor)> GetSolarForecast(DateTime time, TimeSpan duration, ISeedable seedable = null, string nuts2 = "DE", bool forecast = true, bool isExtended = true, bool useWeatherYear = true) => GetSolar(time, duration, seedable.GetSeed(), nuts2, forecast, isExtended: isExtended, useWeatherYear: useWeatherYear);

        private static List<(DateTime TimeStamp, float T_env, float RadDirHor, float RadDiffHor)> GetSolar(DateTime time, TimeSpan duration, int seed = default, string nuts2 = "DE", bool forecast = true, bool isExtended = false, bool useWeatherYear = true)
        {
            List<(DateTime TimeStamp, float tEnv, float radDirHor, float radDiffHor)> weatherActual, weatherForecast = new List<(DateTime TimeStamp, float tEnv, float radDirHor, float radDiffHor)>();
            List<(DateTime TimeStamp, float value)> tEnv, radDirHor, radDiffHor = new List<(DateTime TimeStamp, float value)>();

            weatherActual = wxCtx.Get(time, isExtended ? duration + TimeSpan.FromHours(Math.Max(duration.TotalHours, 5)) : duration, nuts2).Select(el => (el.TimeStamp, el.Temperature, el.Radiation_direct_horizontal, el.Radiation_diffuse_horizontal)).ToList();

            var Last = weatherActual.Where(item => item.TimeStamp.Month == 2 && item.TimeStamp.Day > 20);

            if (forecast)
            {
                tEnv = Forecast.GetForecast(weatherActual.Select(el => (el.TimeStamp, el.tEnv)).ToList(), Forecast.Type.Temperature, seed);
                radDirHor = Forecast.GetForecast(weatherActual.Select(el => (el.TimeStamp, el.radDirHor)).ToList(), Forecast.Type.SolarRadDir, seed).Select(el => (el.TimeStamp, Math.Max(0, el.value))).ToList();
                radDiffHor = Forecast.GetForecast(weatherActual.Select(el => (el.TimeStamp, el.radDiffHor)).ToList(), Forecast.Type.SolarRadDiff, seed).Select(el => (el.TimeStamp, Math.Max(0, el.value))).ToList();
            }
            else
            {
                tEnv = weatherActual.Select(el => (el.TimeStamp, el.tEnv)).ToList();
                radDirHor = weatherActual.Select(el => (el.TimeStamp, el.radDirHor)).ToList().Select(el => (el.TimeStamp, Math.Max(0, el.radDirHor))).ToList();
                radDiffHor = weatherActual.Select(el => (el.TimeStamp, el.radDiffHor)).ToList().Select(el => (el.TimeStamp, Math.Max(0, el.radDiffHor))).ToList();
            }

            for (int i = 0; i < tEnv.Count; i++)
                weatherForecast.Add((weatherActual[i].TimeStamp, tEnv[i].value, radDirHor[i].value, radDiffHor[i].value));

            return weatherForecast.Where(el => el.TimeStamp < time + duration).ToList();
        }

        public static List<(DateTime TimeStamp, float WindSpeed10m)> GetWindspeedForecast(DateTime time, TimeSpan duration, int seed = default, string nuts2 = "DE", bool forecast = true, bool useWeatherYear = true) => GetWindspeed(time, duration, seed, nuts2, forecast, useWeatherYear: useWeatherYear);
        public static List<(DateTime TimeStamp, float WindSpeed10m)> GetWindspeed(DateTime time, TimeSpan duration, string nuts2 = "DE", bool isExtended = false, bool useWeatherYear = true) => GetWindspeed(time, duration, nuts2: nuts2, forecast: false, isExtended: isExtended, useWeatherYear: useWeatherYear);

        public static List<(DateTime TimeStamp, float WindSpeed10m)> GetWindspeedForecast(DateTime time, TimeSpan duration, ISeedable seedable, string nuts2 = "DE", bool forecast = true, bool useWeatherYear = true) => GetWindspeed(time, duration, seedable.GetSeed(), nuts2, forecast, useWeatherYear: useWeatherYear);
        /// <summary>
        /// Methode lädt Wind-Wettereintrag für gegebene Postleitzahl und Viertelstunden-Index(Datum) aus Datenbank und gibt ihn als List<float> zurück
        /// </summary>
        /// <param name="t_DA">DateTime ( 0 Uhr des Folgetages )</param>
        /// <param name="Postcode">Integer 5-stellige Postleitzahl</param>
        /// <returns>Windgeschwindigkeit in 10m Höhe-float-Liste</returns>
        private static List<(DateTime TimeStamp, float WindSpeed10m)> GetWindspeed(DateTime time, TimeSpan duration, int seed = default, string nuts2 = "DE", bool forecast = true, bool isExtended = false, bool useWeatherYear = true)
        {
            var DurationAboveMinimum = isExtended ? duration.Add(TimeSpan.FromHours(Math.Max(duration.TotalHours, 5))) : duration;
            var windActual = wxCtx.Get(time, DurationAboveMinimum, nuts2).Select(el => (el.TimeStamp, el.Windspeed_10m)).ToList();

            if (forecast)
                return Forecast.GetForecast(windActual, Forecast.Type.Wind, seed).ToList();

            // if Forecast-Date is in the past, return actual values
            return windActual.Select(el => (el.TimeStamp, Math.Max(0, el.Windspeed_10m))).ToList();
        }

        public static List<(DateTime TimeStamp, float Temperature)> GetTemperatureForecast(DateTime time, TimeSpan duration, int seed = default, string nuts2 = "DE", bool forecast = true, bool useWeatherYear = true) => GetTemperature(time, duration, seed, nuts2, forecast, useWeatherYear: useWeatherYear);
        public static List<(DateTime TimeStamp, float Temperature)> GetTemperature(DateTime time, TimeSpan duration, string nuts2 = "DE", bool isExtended = false, bool useWeatherYear = true) => GetTemperature(time, duration, nuts2: nuts2, forecast: false, isExtended: isExtended, useWeatherYear: useWeatherYear);
        public static List<(DateTime TimeStamp, float Temperature)> GetTemperatureForecast(DateTime time, TimeSpan duration, ISeedable seedable, string nuts2 = "DE", bool forecast = true, bool useWeatherYear = true) => GetTemperature(time, duration, seedable.GetSeed(), nuts2, forecast, useWeatherYear: useWeatherYear);

        /// <summary>
        /// Methode lädt Temperaturdaten ab Startdatum für angegebene Dauer
        /// </summary>
        /// <param name="time">Startzeit als DateTime</param>
        /// <param name="duration">Dauer als TimeSpan</param>
        /// <returns>Liste mit Zeit-Temperatur-Wertepaaren als Tupel</returns>
        private static List<(DateTime TimeStamp, float Temperature)> GetTemperature(DateTime time, TimeSpan duration, int seed = default, string nuts2 = "DE", bool forecast = true, bool isExtended = false, bool useWeatherYear = true)
        {
            var temperatureActual = wxCtx.Get(time, isExtended ? duration + TimeSpan.FromHours(Math.Max(duration.TotalHours, 5)) : duration, nuts2).ToList().Select(el => (el.TimeStamp, el.Temperature)).ToList();

            if (forecast)
                return Forecast.GetForecast(temperatureActual, Forecast.Type.Temperature, seed).Where(el => el.TimeStamp < time + duration).ToList();

            return temperatureActual;
        }
        #endregion
    }
}
