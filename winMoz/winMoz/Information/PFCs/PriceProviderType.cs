﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Tools.Helper;
using winMoz.Information.PFCs;

namespace winMoz.Information.PFCs
{
    public class PriceProviderType : Enumeration
    {
        public Type Provider { get; private set; }
        public PriceProviderType(string value, int id, Type type) : base(value, id)
        {
            if (type.GetInterfaces().Select(item => item.Name).Any(item => item.Contains("IPFCProvider"))) Provider = type;
            else throw new Exception($"IPFCProvider has to be Assignable from Type {type}.");
        }

        public static bool HasUniqueValues()
            => HasUniqueValues<PriceProviderType>();

        public static bool HasUniqueIds()
            => HasUniqueIds<PriceProviderType>();

        public static bool HasUniqueProvider()
            => GetAll().GroupBy(prop => prop.Provider, (el1, el2) => el2.Count() == 1).All(el => el);

        public static IEnumerable<PriceProviderType> GetAll()
            => GetAll<PriceProviderType>();

        public static PriceProviderType FromString(string value)
            => FromString<PriceProviderType>(value);

        #region Values
        public static PriceProviderType HPFC = new PriceProviderType("HPFC", 0, typeof(HPFCProvider));
        public static PriceProviderType QHPFC = new PriceProviderType("QHPFC", 1, typeof(QHPFCProvider));
        public static PriceProviderType MPFC = new PriceProviderType("MPFC", 1, typeof(MPFCProvider));
        #endregion
    }
}
