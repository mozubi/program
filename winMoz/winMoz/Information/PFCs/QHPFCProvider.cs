﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Tools.Prediction.Prices.Intraday;

namespace winMoz.Information.PFCs
{
    public sealed class QHPFCProvider : IPFCProvider<ForecastPriceID, IEnumerable<(DateTime TimeStamp, float PricePrediction)>>
    {
        static int triesForHistoricData = 0;
        PredictorPriceID predictor = new PredictorPriceID();

        public IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, ISeedable seedable = null)
            => Get(timeStamp, duration, seedable.GetSeed());

        public IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, int seed = default)
        {
            //if (duration > TimeSpan.FromHours(1)) System.Diagnostics.Debugger.Break(); // TODO: Fix error in id-prices
            if (duration == default) duration = new TimeSpan(0, 1, 0, 0);

            timeStamp = new DateTime(timeStamp.Year, timeStamp.Month, timeStamp.Day, timeStamp.Hour, 0, 0);

            List<ForecastPriceID> predictionInput = new List<ForecastPriceID>();

            if (duration > TimeSpan.FromHours(1))
            {
                var EndOfDuration = timeStamp.Add(duration);
                for(DateTime date = timeStamp; date < EndOfDuration; date = date.AddHours(1))
                    predictionInput.AddRange(GetPredictionInput(date, TimeSpan.FromHours(1), seed).ToList());
            }
            else
                predictionInput = GetPredictionInput(timeStamp, duration, seed).ToList();

            return predictor.Predict(predictionInput);
        }

        public IEnumerable<ForecastPriceID> GetPredictionInput(DateTime timeStamp, TimeSpan duration, int seed = default)
        {
            // Get historic DA Prices for duration + last 3 hours
            //+1 because we have 11 hourly prices in a timespan from 00:00 to 10:00
            var repo = new PredictionDataRepository();
            var histDAPrices = Info.HPFC.Get(timeStamp, duration, seed).Select(el => (el.TimeStamp, el.PricePrediction)).ToList();
            // Get historic ID Prices for duration + last 4 qhours
            //+1 because we have 11 hourly prices in a timespan from 00:00 to 10:00
            List<(DateTime TimeStamp, float RE)>  RE = repo.REDataRepo.Get().Where(el => timeStamp.Date == el.TimeStamp.Date).Select(item => (item.TimeStamp, item.PV_Ante + item.WI_Ante)).ToList();
            var REAverage = RE.Resample(Resampling.TimeStep.Hour, Resampling.Method.Average).ToList()
                    .Select(item => new { item.TimeStamp, RE_Av = item.Item2}).ToList();
            var forecastInput = new List<ForecastPriceID>();

            for( var t = timeStamp; t < timeStamp + duration; t = t.AddHours(1))
            {
                float DA = histDAPrices.First(el => el.TimeStamp.Date == t.Date && el.TimeStamp.Hour == t.Hour).PricePrediction;
                var REav = REAverage.First(el => el.TimeStamp.Date == t.Date && el.TimeStamp.Hour == t.Hour).RE_Av;
                var REGrad = RE.Where(el => el.TimeStamp.Date == t.Date && el.TimeStamp.Hour == t.Hour).Select(el =>  (el.TimeStamp, el.RE - REav));
                foreach (var grad in REGrad)
                {
                    forecastInput.Add(new ForecastPriceID(grad.TimeStamp, DA, grad.Item2));
                }
            }

            return forecastInput;
        }

        public void UpdatePredictor(IEnumerable<ForecastPriceID> input2train)
        {
            throw new NotImplementedException();
        }

        public void RebuildModel() => predictor.Train();
    }
}
