﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Helper;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Tools.Prediction.Prices.DayAhead;

namespace winMoz.Information.PFCs
{
    public class HPFCProvider : IPFCProvider<ForecastPriceDA, IEnumerable<(DateTime TimeStamp, float PricePrediction)>>
    {
        PredictorPriceDA predictor = new PredictorPriceDA();

        #region Overrides & Implementation of Interface
        public IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, ISeedable seedable = default) => this.Get(timeStamp, duration, seedable.GetSeed());

        public IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, int seed = default)
        {
            DateTime originalTime = new DateTime(timeStamp.Ticks);

            #region Cleanup time and duration
            if (duration == default)
                duration = new TimeSpan(1, 0, 0, 0);
            if (timeStamp.Hour != 0 || timeStamp.Minute != 0 || timeStamp.Second != 0)
                timeStamp = new DateTime(timeStamp.Year, timeStamp.Month, timeStamp.Day, 0, 0, 0);
            duration = duration.Add(originalTime - timeStamp);
            #endregion

            var prediction = new List<(DateTime TimeStamp, float PricePrediction)>();
            for (int d = 0; d < duration.TotalDays; d++)
            {
                var predictionInput = GetPredictionInput(timeStamp.AddDays(d), TimeSpan.FromDays(1), seed);
                prediction.AddRange(predictor.Predict(predictionInput));
            }

            return prediction;
        }

        /// <summary>
        /// Methode erzeugt benötigten DayAhead-Forecast (Inputparameter) für das Prognosemodell
        /// </summary>
        /// <param name="timeStamp">T0 des Folgetags</param>
        /// <returns>Inputparameter (Zeit, letzter Preis, Windprognose, PV-Prognose)</returns>
        public IEnumerable<ForecastPriceDA> GetPredictionInput(DateTime timeStamp, TimeSpan duration, int seed)
        {
            //var forecastInput = new List<ForecastPriceDA>();

            //var lastDAResults = SpotRepository.GetPrices<DayAheadResult>(timeStamp.AddDays(-1), new TimeSpan(1, 0, 0, 0)).ToList().Resample(Resampling.TimeStep.Hour).Select(el => el.Item2).ToList();

            //// Multiplier = DEBUG - Müssen evaluiert werden
            //// TODO: Normalization schafft ungewünschte Ergebnisse bei Prognose über einen längeren Zeitraum, da auf den Tag normalisiert wird
            //var wind = Weather.GetWindspeedForecast(timeStamp, duration, seed).ToList().Resample(Resampling.TimeStep.Hour).Select(el => el.Item2).ToList().Normalize(16, 40000);
            //var solar = Weather.GetSolarForecast(timeStamp, duration, seed).Select(el => (el.TimeStamp, el.RadDiffHor)).ToList().Resample(Resampling.TimeStep.Hour).Select(el => el.Item2).ToList().Normalize(384, 20000);
            //int hour = 0;
            //for (DateTime t = timeStamp; t.CompareTo((timeStamp + duration)) < 0; t = t.AddHours(1))
            
            IEnumerable<ForecastPriceDA> forecastInput;
            using (var repo = new PredictionDataRepository())
            {
                List<winMoz.Data.Access.DTO.DayAheadData> histDA = GetDayAheadData(timeStamp, repo);
                var price23H = histDA.FirstOrDefault(el => el.TimeStamp.Hour == 23).Price;
                var RE_Data = repo.REDataRepo.Get().Where(el => timeStamp.Date == el.TimeStamp.Date).Select(item => (item.TimeStamp, item.PV_Ante, item.WI_Ante)).ToList()
                    .Resample(Resampling.TimeStep.Hour, Resampling.Method.Average).ToList()
                    .Select(item => new { item.TimeStamp, PV_Ante = item.Item2, WI_Ante = item.Item3 }).ToList();
                var load_Data = repo.LoadDataRepo.Get().Where(el => timeStamp.Date == el.TimeStamp.Date).Select(item => (item.TimeStamp, item.Volume.GetValueOrDefault(0))).ToList()
                    .Resample(Resampling.TimeStep.Hour, Resampling.Method.Average).ToList()
                    .Select(item => new { item.TimeStamp, Volume = item.Item2 }).ToList();

                forecastInput =  histDA.Zip(RE_Data, (DA, RE) => (DA, RE))
                    .Zip(load_Data, (DA_RE, LOAD) =>
                    new ForecastPriceDA(DA_RE.RE.TimeStamp, DA_RE.DA.Price, price23H, DA_RE.RE.PV_Ante, DA_RE.RE.WI_Ante, LOAD.Volume));
            }
            return forecastInput;
        }

        public List<winMoz.Data.Access.DTO.DayAheadData> GetDayAheadData(DateTime timeStamp, PredictionDataRepository repo)
        {
            var Shift = new DateTime(Simulator.StartDataYear.Year, timeStamp.Month, timeStamp.Day, timeStamp.Hour, timeStamp.Minute, 0) - timeStamp;
            List<winMoz.Data.Access.DTO.DayAheadData> histDA = new List<Data.Access.DTO.DayAheadData>();
            if (timeStamp >= Simulator.Times.Start.AddDays(2))
                histDA = repo.DayAheadDataRepo.Get().Where(el => timeStamp.AddDays(-1).Date == el.TimeStamp.Date).ToList();
            else
            {
                histDA = repo.DayAheadDataRepo.Get().Where(el => timeStamp.Add(Shift) == el.TimeStamp.Date.AddDays(-1)).ToList();
                histDA.ForEach(item => item.TimeStamp.AddMinutes(-Shift.TotalMinutes));
            }
            return histDA;
        }

        public void UpdatePredictor(IEnumerable<ForecastPriceDA> input2train) => throw new NotImplementedException();

        public void RebuildModel() => predictor.Train();
        #endregion

        #region Public-Methods
        public (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) GetBlocks(DateTime timeStamp, ISeedable seedable = null)
        {
            var time = new DateTime(timeStamp.Year, timeStamp.Month, timeStamp.Day, 0, 0, 0);
            return CalcBlockPrices(Get(time, new TimeSpan(1, 0, 0, 0), seedable));
        }

        public static (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) CalcBlockPrices(IEnumerable<(DateTime TimeStamp, float Price)> spotForecast)
        {
            return (TimeStamp: spotForecast.FirstOrDefault().TimeStamp,
                      BasePrice: spotForecast.Select(x => x.Price).Average(),
                      PeakPrice: spotForecast.Where(el => el.TimeStamp.IsPeakTime()).Select(x => x.Price).Average(),
                   OffPeakPrice: spotForecast.Where(el => el.TimeStamp.IsOffPeakTime()).Select(x => x.Price).Average());
        }

        #endregion

    }
}
