﻿using System;
using System.Collections.Generic;
using winMoz.Helper;
using winMoz.Simulation;

namespace winMoz.Information.PFCs
{
    /// <summary>
    /// Klasse zur Berechnung und Verwaltung von Price-Forward-Curves und anderer Strompreisprognosen
    /// </summary>
    [Obsolete]
    public class CommodityPricesOld
    {


        #region Public-Methods
        public float GetCommodityPrice(DateTime TimeStamp, string commodityName, string type = "")
        {
            return XML.GetCommodityPrice(Config.XmlFiles.CommodityPrices, TimeStamp, commodityName, type);
        }

        public List<(DateTime TimeStamp, float Price)> GetCommodityPrice(DateTime TimeStamp, TimeSpan duration, Commodities.Commodity commodity)
        {
            var prices = new List<(DateTime TimeStamp, float Price)>();

            for (DateTime t = TimeStamp; t.CompareTo(TimeStamp + duration) < 0; t = t.AddDays(1))
            {
                prices.Add((t, GetCommodityPrice(t, Commodities.GetName(commodity))));
            }

            return prices;
        }
        #endregion


    }
}
