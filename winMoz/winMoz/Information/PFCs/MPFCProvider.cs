﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Simulation;
using winMoz.Tools.Prediction;
using winMoz.Tools.Prediction.Prices.DayAhead;

namespace winMoz.Information.PFCs
{
    public sealed class MPFCProvider : IPFCProvider<ForecastPriceDA, (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice)>
    {
        readonly IPFCProvider<ForecastPriceDA, IEnumerable<(DateTime TimeStamp, float PricePrediction)>> Provider;
        List<(DateTime TimeStamp, float PricePrediction)> history = new List<(DateTime TimeStamp, float PricePrediction)>();
        List<(DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice)> results = new List<(DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice)>();

        #region Overrides & Implementation of Base
        public MPFCProvider(IPFCProvider<ForecastPriceDA, IEnumerable<(DateTime TimeStamp, float PricePrediction)>> provider)
        {
            this.Provider = provider;
        }

        public (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) Get(DateTime timeStamp, TimeSpan duration = default, ISeedable seedable = null)
            => Get(timeStamp, duration, seedable.GetSeed());
        public (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) Get(DateTime timeStamp, TimeSpan duration = default, int seed = default)
        {
            var time = timeStamp.ToStartOfMonth();

            if (HistoryIsValid(time))
                return GetResults4Agent(time, seed);

            Update(time, seed);

            return GetResults4Agent(time, seed);
        }

        public void UpdatePredictor(IEnumerable<ForecastPriceDA> input2train) => Provider.UpdatePredictor(input2train);

        public IEnumerable<ForecastPriceDA> GetPredictionInput(DateTime timeStamp, TimeSpan duration, int seed = default) => Provider.GetPredictionInput(timeStamp, duration, seed);
        #endregion

        private (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) GetResults4Agent(DateTime timeStamp, ISeedable seedable)
            => GetResults4Agent(timeStamp, seedable.GetSeed());
        private (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) GetResults4Agent(DateTime timeStamp, int seed)
        {
            var resultsWithError = Forecast.GetForecast(GetResultsFromHistory(timeStamp).ToList(), Forecast.Type.MonthlyPrices, seed);

            return HPFCProvider.CalcBlockPrices(resultsWithError);
        }

        private IEnumerable<(DateTime TimeStamp, float PricePrediction)> GetResultsFromHistory(DateTime timeStamp)
        {
            return history.Where(el => el.TimeStamp >= timeStamp && el.TimeStamp < timeStamp + timeStamp.GetDurationOfMonth());
        }

        private bool HistoryIsValid(DateTime timeStamp) => results.Any(el => el.TimeStamp == timeStamp);

        private void Update(DateTime time, ISeedable seedable) => Update(time, seedable.GetSeed());

        private void Update(DateTime time, int seed)
        {
            var duration = time.GetDurationOfMonth();

            var prediction = Provider.Get(time, duration, seed);

            this.history = this.history.Concat(prediction).ToList();
        }

        public void RebuildModel()
        {
            throw new NotImplementedException();
        }
    }
}
