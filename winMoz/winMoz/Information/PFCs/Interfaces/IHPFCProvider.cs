﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Agents;
using winMoz.Data.Access.Repositories;
using winMoz.Helper;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Tools.Prediction.Prices.DayAhead;

namespace winMoz.Information.PFCs
{
    public interface IHPFCProvider : IPFCProvider<ForecastPriceDA, IEnumerable<(DateTime TimeStamp, float PricePrediction)>>
    {

        #region Overrides & Implementation of Interface
        IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, ISeedable seedable = default);

        IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, int seed = default);

        #endregion

        #region Public-Methods
        (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) GetBlocks(DateTime timeStamp, ISeedable seedable = null);
        #endregion

    }
}
