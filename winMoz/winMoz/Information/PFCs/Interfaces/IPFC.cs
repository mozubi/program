﻿using System;

namespace winMoz.Information.PFCs
{
    public interface IPFC<TPFCOutput>
    {
        TPFCOutput Get(DateTime timeStamp, TimeSpan duration, int seed = default);
    }
}