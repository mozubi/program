﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Helper;
using winMoz.Simulation;
using winMoz.Tools.Prediction;
using winMoz.Tools.Prediction.Prices.DayAhead;

namespace winMoz.Information.PFCs
{
    public interface IMPFCProvider : IPFCProvider<ForecastPriceDA, (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice)>
    {
        List<(DateTime TimeStamp, float PricePrediction)> history { get; }
        List<(DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice)> results { get; }

        #region Overrides & Implementation of Base

        (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) Get(DateTime timeStamp, TimeSpan duration = default, ISeedable seedable = null);
        (DateTime TimeStamp, float BasePrice, float PeakPrice, float OffPeakPrice) Get(DateTime timeStamp, TimeSpan duration = default, int seed = default);

        #endregion
    }
}
