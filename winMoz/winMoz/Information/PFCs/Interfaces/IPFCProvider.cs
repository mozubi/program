﻿using System;
using System.Collections.Generic;
using winMoz.Agents;

namespace winMoz.Information.PFCs
{
    public interface IPFCProvider<PredictionInput, PFCOutput> :  IPFC<PFCOutput>
    {
        
        IEnumerable<PredictionInput> GetPredictionInput(DateTime timeStamp, TimeSpan duration, int seed = default);
        void UpdatePredictor(IEnumerable<PredictionInput> input2train);

        void RebuildModel();
    }
}
