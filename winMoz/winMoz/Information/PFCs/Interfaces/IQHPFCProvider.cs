﻿using System;
using System.Collections.Generic;
using System.Linq;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Results;
using winMoz.Simulation;
using winMoz.Tools.Prediction.Prices.Intraday;

namespace winMoz.Information.PFCs
{
    public interface IQHPFCProvider : IPFCProvider<ForecastPriceID, IEnumerable<(DateTime TimeStamp, float PricePrediction)>>
    {
        IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, ISeedable seedable = null);

        IEnumerable<(DateTime TimeStamp, float PricePrediction)> Get(DateTime timeStamp, TimeSpan duration = default, int seed = default);
    }
}
