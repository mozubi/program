﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Helper;
using System.Xml.Linq;
using System.Globalization;
using winMoz.Simulation;

namespace winMoz.Information
{
    public static class GasSLPDailyFactors
    {
        static GasSLPDailyFactorsStorage storage = new GasSLPDailyFactorsStorage();

        public static List<float> GetGasSLPDailyFactors(DateTime timeStamp, int temperature) => storage.GetGasSLPDailyFactors(timeStamp, temperature);
    }

    internal class GasSLPDailyFactorsStorage
    {
        Dictionary<(string Day, int Temperature), List<float>> CachedGasSLPDailyFactors = new Dictionary<(string Day, int Temperature), List<float>>();
        public void CacheGasSLPDailyFactors()
        {
            XDocument XmlDoc = Config.XmlFiles.GasSLP;
            Dictionary<(string Day, int Temperature), List<float>> GasSLPDailyFactorsLocal = new Dictionary<(string Day, int Temperature), List<float>>();

            float weightComm = 0.75F;
            float weightHh = 0.25F;

            List<string> DaysOfWeek = new List<string>() { "Working", "Saturday", "Sunday" };

            foreach (var day in DaysOfWeek)
            {
                for (int temperature = -5; temperature <= 25; temperature = temperature + 5)
                {
                    List<float> factor = new List<float>();
                    for (int h = 1; h <= 24; h++)
                    {
                        var query = (from hourFactor in XmlDoc.Descendants("SLPHourFactor")
                                     where hourFactor.Attribute("Day").Value.Equals(day.ToString())
                                     where hourFactor.Attribute("Temperature").Value.Equals(temperature.ToString())
                                     where hourFactor.Attribute("Hour").Value.Equals(h.ToString())
                                     select new
                                     {
                                         hour = hourFactor.Attribute("Hour").Value,
                                         FactorHH = Convert.ToSingle(hourFactor.Attribute("FactorHH").Value, CultureInfo.InvariantCulture),
                                         FactorComm = Convert.ToSingle(hourFactor.Attribute("FactorComm").Value, CultureInfo.InvariantCulture)
                                     }).SingleOrDefault();

                        factor.Add((query.FactorComm * weightComm + query.FactorHH * weightHh) / (weightHh + weightComm));
                    }
                    GasSLPDailyFactorsLocal.Add((day, temperature), factor);
                }
            }
            CachedGasSLPDailyFactors = GasSLPDailyFactorsLocal;
        }

        public List<float> GetGasSLPDailyFactors(DateTime timeStamp, int temperature)
        {
            if (CachedGasSLPDailyFactors.Count() == 0)
                CacheGasSLPDailyFactors();
            string day = timeStamp.DayOfWeek.ToString();
            if (timeStamp.IsWeekDay())
                day = "Working";
            return CachedGasSLPDailyFactors[(day, temperature)];
        }
    }
}
