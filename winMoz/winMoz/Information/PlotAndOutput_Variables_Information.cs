﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;

namespace winMoz.Information
{
    public static class PlotAndOutput_Variables_Information
    {
        static PlotAndOutput_Variables_Console_Context ContextConsole = new PlotAndOutput_Variables_Console_Context();
        static PlotAndOutput_Variables_Plot_Context ContextPlot = new PlotAndOutput_Variables_Plot_Context();
        static PlotAndOutput_Variables_CSV_Context ContextCSV = new PlotAndOutput_Variables_CSV_Context();
        public static List<PlotAndOutput_Variables_Dto> GetConsole()
        {
            List<PlotAndOutput_Variables_Dto> cachedPrices = new List<PlotAndOutput_Variables_Dto>();
            var cachedVariables = ContextConsole.Get().ToList();
            return cachedVariables;
        }

        public static List<PlotAndOutput_Variables_Dto> GetPlot()
        {
            List<PlotAndOutput_Variables_Dto> cachedPrices = new List<PlotAndOutput_Variables_Dto>();
            var cachedVariables = ContextPlot.Get().ToList();
            return cachedVariables;
        }

        public static List<PlotAndOutput_Variables_Dto> GetCSV()
        {
            List<PlotAndOutput_Variables_Dto> cachedPrices = new List<PlotAndOutput_Variables_Dto>();
            var cachedVariables = ContextCSV.Get().ToList();
            return cachedVariables;
        }
    }
}
