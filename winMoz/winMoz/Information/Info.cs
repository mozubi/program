﻿using winMoz.Information.PFCs;

namespace winMoz.Information
{
    /// <summary>
    /// Info-Klasse aggregiert alle globalen Informationen der Simulation
    /// </summary>
    public static class Info
    {
        /// <summary>
        /// Price-Forward-Curves (Strompreisprognosen)
        /// </summary>
        public static HPFCProvider HPFC = new HPFCProvider();
        public static QHPFCProvider QHPFC = new QHPFCProvider();
        public static MPFCProvider MPFC = new MPFCProvider(HPFC);


    }
}
