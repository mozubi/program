﻿using System;
using System.Collections.Generic;
using System.Data;
using winMoz.Simulation;
//using winMoz.Assets.Types;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using System.Linq;
using thermalSubtypes = winMoz.Assets.Plants.Plant_Thermal.SubTypes;

namespace winMoz.Information
{
    public static class Commodities
    {
        /// <summary>
        /// Commodity-Typen
        /// </summary>
        public enum Commodity {DEFAULT, Hard_Coal, Lignite, Natural_Gas, Carbon, Uranium, Oil }


        static CommoditiesContext Context = new CommoditiesContext();
        static Int64 CommodityID { get; set; } = 100000;

        public static string GetName(Commodity typ)
        {
            return Enum.GetName(typeof(Commodity), typ).ToLower();
        }

        public class CommodityPricesCoupled : Dictionary<Commodity, float>
        {
            public CommodityPricesCoupled(CommodityPrices_Dto dto)
            {
                this.Add(Commodity.Hard_Coal, dto.hard_coal);
                this.Add(Commodity.Lignite, dto.lignite);
                this.Add(Commodity.Natural_Gas, dto.natural_gas);
                this.Add(Commodity.Carbon, dto.carbon);
                this.Add(Commodity.Uranium, dto.uranium);
                this.Add(Commodity.Oil, dto.oil);
            }
        }
        /// <summary>
        /// Methode gibt Preis einer bestimmten Commodity zu einem Zeitpunkt zurück
        /// </summary>
        public static float GetPrice(DateTime timeStamp, Commodity commodity)
        {
            Int64 DayINT = GetDayINT(timeStamp);
            return new CommodityPricesCoupled(Context.CommodityPrices.Find(CommodityID, DayINT))[commodity];
        }

        public static Int64 GetDayINT(DateTime timeStamp)
            => timeStamp.Year * 10000 + timeStamp.Month * 100 + timeStamp.Day;
        /// <summary>
        /// Methode gibt Preise einer bestimmten Commodity ab einem bestimmten Zeitpunkt für eine angegebene Dauer zurück
        /// </summary>
        /// <param name="timeStamp">Startzeitpunkt</param>
        /// <param name="duration">Dauer für die Preise ausgelesen werden sollen</param>
        /// <param name="commodity">Commodity-Typ</param>
        /// <returns>Liste mit Preisen</returns>
        public static List<float> GetPrices(DateTime timeStamp, TimeSpan duration, Commodity commodity)
        {
            List<CommodityPrices_Dto> cachedPrices = new List<CommodityPrices_Dto>();
            var Prices = new List<float>();
            var EndOfDuration = timeStamp.Add(duration);
            var StartDayInt = GetDayINT(timeStamp);
            var EndDayInt = GetDayINT(EndOfDuration);
            if (StartDayInt != EndDayInt)
            {
                cachedPrices = Context.CachedPrices.Where(item => item.DayINT >= StartDayInt && item.DayINT < EndDayInt && item.CommodityID == CommodityID).ToList();
                if (cachedPrices.Count() > 0)
                {
                    return cachedPrices.Select(item => GetPriceFromComodity(item, commodity)).ToList();
                }
                else
                {
                    TimeSpan DurationYear = timeStamp.AddYears(1) - timeStamp;
                    CacheCommodities(timeStamp, DurationYear);
                    return Context.CachedPrices.Where(item => item.DayINT >= StartDayInt && item.DayINT < EndDayInt && item.CommodityID == CommodityID).ToList().Select(item => GetPriceFromComodity(item, commodity)).ToList();
                }
            }
            else
            {
                cachedPrices = Context.CachedPrices.Where(item => item.DayINT == StartDayInt && item.CommodityID == CommodityID).ToList();
                if (cachedPrices.Count() > 0)
                {
                    return cachedPrices.Select(item => GetPriceFromComodity(item, commodity)).ToList();
                }
                else
                {
                    TimeSpan DurationMonth = timeStamp.AddMonths(1) - timeStamp;
                    CacheCommodities(timeStamp, DurationMonth);
                    return Context.CachedPrices.Where(item => item.DayINT == StartDayInt && item.CommodityID == CommodityID).ToList().Select(item => GetPriceFromComodity(item, commodity)).ToList();
                }
            }
        }

        public static List<(DateTime TimeStamp, float Price)> GetPricesWithTime(DateTime timeStamp, TimeSpan duration, Commodity commodity)
        {
            List<CommodityPrices_Dto> cachedPrices = new List<CommodityPrices_Dto>();
            var Prices = new List<float>();
            var EndOfDuration = timeStamp.Add(duration);
            var StartDayInt = GetDayINT(timeStamp);
            var EndDayInt = GetDayINT(EndOfDuration);
            if (StartDayInt != EndDayInt)
            {
                cachedPrices = Context.CachedPrices.Where(item => item.DayINT >= StartDayInt && item.DayINT < EndDayInt && item.CommodityID == CommodityID).ToList();
                if (cachedPrices.Count() > 0)
                {
                    return cachedPrices.Select(item =>(item.TS_normCET, GetPriceFromComodity(item, commodity))).ToList();
                }
                else
                {
                    TimeSpan DurationMonth = timeStamp.AddMonths(1) - timeStamp;
                    CacheCommodities(timeStamp, DurationMonth);
                    return Context.CachedPrices.Where(item => item.DayINT >= StartDayInt && item.DayINT < EndDayInt && item.CommodityID == CommodityID).ToList().Select(item => (item.TS_normCET, GetPriceFromComodity(item, commodity))).ToList();
                }
            }
            else
            {
                cachedPrices = Context.CachedPrices.Where(item => item.DayINT == StartDayInt && item.CommodityID == CommodityID).ToList();
                if (cachedPrices.Count() > 0)
                {
                    return cachedPrices.Select(item => (item.TS_normCET, GetPriceFromComodity(item, commodity))).ToList();
                }
                else
                {
                    TimeSpan DurationMonth = timeStamp.AddMonths(1) - timeStamp;
                    CacheCommodities(timeStamp, DurationMonth);
                    return Context.CachedPrices.Where(item => item.DayINT == StartDayInt && item.CommodityID == CommodityID).ToList().Select(item => (item.TS_normCET, GetPriceFromComodity(item, commodity))).ToList();
                }
            }
        }

        public static void CacheCommodities(DateTime timeStamp, TimeSpan duration) => Context.CachePrices(timeStamp, duration);

        public static float GetPriceFromComodity(CommodityPrices_Dto dto, Commodity commodity)
        {
            switch (commodity)
            {
                case Commodity.Hard_Coal: return dto.hard_coal;
                case Commodity.Lignite: return dto.lignite;
                case Commodity.Natural_Gas: return dto.natural_gas;
                case Commodity.Carbon: return dto.carbon;
                case Commodity.Uranium: return dto.uranium;
                default: return dto.oil;
            }
        }

        public static Commodity GetCommodityForAsset(thermalSubtypes subType)
        {
            switch (subType)
            {
                case thermalSubtypes.GAS:
                case thermalSubtypes.SEWAGELANDFILLGAS:
                    return Commodity.Natural_Gas;
                case thermalSubtypes.OIL:
                    return Commodity.Oil;
                case thermalSubtypes.COAL:
                    return Commodity.Hard_Coal;
                case thermalSubtypes.LIGNITE:
                    return Commodity.Lignite;
                case thermalSubtypes.NUCLEAR:
                    return Commodity.Uranium;
                default:
                    return Commodity.Carbon;
            }
        }

        public static bool HasChanged(DateTime timeStamp, CommodityPricesCoupled pricesNow)
            => new CommodityPricesCoupled(Context.CommodityPrices.FirstOrDefault(item => item.DayINT == GetDayINT(timeStamp))) == pricesNow;

        public static bool HasChanged(DateTime now, DateTime last)
            => new CommodityPricesCoupled(Context.CommodityPrices.FirstOrDefault(item => item.DayINT == GetDayINT(now))) == new CommodityPricesCoupled(Context.CommodityPrices.FirstOrDefault(item => item.DayINT == GetDayINT(last)));
    }
}
