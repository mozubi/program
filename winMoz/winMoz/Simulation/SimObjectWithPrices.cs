﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Information.PFCs;
using System.Collections.Concurrent;

namespace winMoz.Simulation
{
    public abstract class SimObjectWithPrices : SeedableSimObject
    {
        protected ConcurrentDictionary<PriceProviderType, (DateTime EventTime, DateTime DeterminationTime, TimeSpan Duration, List<(DateTime TimeStamp, float PricePrediction)> Prediction)> PricePredictions { get; private set; }

        private Dictionary<PriceProviderType, object> PriceProviders { get; set; }

        protected SimObjectWithPrices() { }
        public SimObjectWithPrices(int scenarioID) : base(scenarioID)
        {
            PricePredictions = new ConcurrentDictionary<PriceProviderType, (DateTime EventTime, DateTime DeterminationTime, TimeSpan Duration, List<(DateTime TimeStamp, float PricePrediction)> Prediction)>();
            PriceProviders = new Dictionary<PriceProviderType, object>();
        }

        public IEnumerable<(DateTime TimeStamp, float PricePrediction)> GetPriceCurve(PriceProviderType providerType, DateTime timeStamp, TimeSpan duration = default)
        {
            if (!PricePredictions.ContainsKey(providerType)) CreatePriceCurve(timeStamp, duration, providerType);
            if (Simulator.Times.SimTime > PricePredictions[providerType].EventTime || PricePredictions[providerType].DeterminationTime != timeStamp || PricePredictions[providerType].Duration != duration) UpdatePriceCurve(timeStamp, duration, providerType);
            if(PricePredictions[providerType].Prediction.Count() > 4)
            {

            }
            return PricePredictions[providerType].Prediction;
        }

        private void UpdatePriceCurve(DateTime timeStamp, TimeSpan duration, PriceProviderType providerType)
        {
            if (!PriceProviders.ContainsKey(providerType))
            {
                var PricePredictor = Activator.CreateInstance(providerType.Provider);
                PriceProviders.Add(providerType, PricePredictor);
            }
            PricePredictions[providerType] = (Simulator.Times.SimTime, timeStamp, duration, ((IEnumerable<(DateTime, float)>)PriceProviders[providerType].GetType().GetMethods().LastOrDefault(item => item.Name == "Get").Invoke(PriceProviders[providerType], new object[3] { timeStamp, duration, this.GetSeed() })).ToList());
        }

        private void CreatePriceCurve(DateTime timeStamp, TimeSpan duration, PriceProviderType providerType)
        {
            if (!PriceProviders.ContainsKey(providerType))
            {
                var PricePredictor = Activator.CreateInstance(providerType.Provider);
                PriceProviders.Add(providerType, PricePredictor);
            }
            PricePredictions.GetOrAdd(providerType, (Simulator.Times.SimTime, timeStamp, duration, ((IEnumerable<(DateTime, float)>)PriceProviders[providerType].GetType().GetMethods().LastOrDefault(item => item.Name == "Get").Invoke(PriceProviders[providerType], new object[3] { timeStamp, duration, this.GetSeed() })).ToList()));
        }
    }
}
