﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Simulation
{
    public class TimeSpanSave
    {
        public int Years;
        public int Months;
        public int Days;
        public int Hours;
        public int Minutes;

        public TimeSpanSave(int years, int months, int days, int hours, int minutes)
        {
            Years = years;
            Months = months;
            Days = days;
            Hours = hours;
            Minutes = minutes;
        }

        /// <summary>
        /// Konstruktor mit Array 0 -> Jahre, 5 -> Minuten
        /// </summary>
        /// <param name="dateValues"></param>
        public TimeSpanSave(params int[] dateValues)
        {
            Years = dateValues[0];
            Months = dateValues[1];
            Days = dateValues[2];
            Hours = dateValues[3];
            Minutes = dateValues[4];
        }
    }
}
