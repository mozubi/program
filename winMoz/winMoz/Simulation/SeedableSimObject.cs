﻿using System.Collections.Generic;
using winMoz.Helper;

namespace winMoz.Simulation
{
    public abstract class SeedableSimObject : SimObject, ISeedable, ISeedableSimObject
    {
        private List<int> seedValues;

        protected SeedableSimObject() { }
        public SeedableSimObject(int scenarioID) : base(scenarioID) { }

        /// <summary>
        /// Methode zieht den nächsten Setzwert aus der Setzwerttabelle des SimObjects und entfernt diesen anschließend
        /// </summary>
        /// <returns>Einzigartiger Setzwert als <see cref="int"/></returns>
        public int GetSeed()
        {
            if (seedValues == null || seedValues.Count == 0) seedValues = MathHelper.GetRandomSeeds(this.Id, 35040);

            int seed = seedValues[0];
            seedValues.RemoveAt(0);

            return seed;
        }
    }
}
