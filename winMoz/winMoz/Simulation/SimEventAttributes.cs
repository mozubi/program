﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Simulation;
using winMoz.Helper;

namespace winMoz.Simulation
{
    /// <summary>
    /// Eigenschaftsklasse für Simeventeigenschaften
    /// </summary>ISimEvent<ISimEventAttributes>
    public class SimEventAttributes : ISimEventAttributes
    {
        //public int Id { get; private set; }
        public DateTime StartEventTime { get; set; }
        public TimeSpanSave StartEventTimeDurationFromStart { get; set; }
        public DateTime StartDeterminationTime { get; set; }
        public TimeSpanSave StartDeterminationTimeDurationFromStart { get; set; }
        public TimeSpanSave DeterminationDuration { get; set; }
        public TimeSpanSave DeterminationStep { get; set; }

        protected SimEventAttributes() { }
        public SimEventAttributes(DateTime start, DateTime startEventTime, DateTime startDeterminationTime, TimeSpanSave determinationDuration, TimeSpanSave determinationStep) //, TimeSpan auctionStep)
        {
            StartEventTime = startEventTime;
            StartEventTimeDurationFromStart = Extensions.GetDiff(StartEventTime, start);
            StartDeterminationTime = startDeterminationTime;
            StartDeterminationTimeDurationFromStart = Extensions.GetDiff(StartDeterminationTime, start);
            DeterminationDuration = determinationDuration;
            DeterminationStep = determinationStep;
        }

        public SimEventAttributes(TimeSpanSave startEventTimeDurationFromStart, TimeSpanSave startDeterminationTimeDurationFromStart, TimeSpanSave determinationDuration, TimeSpanSave determinationStep) //, TimeSpan auctionStep)
        {
            StartEventTime = Simulator.Times.Start.Add(startEventTimeDurationFromStart);
            StartEventTimeDurationFromStart = startEventTimeDurationFromStart;
            StartDeterminationTime = Simulator.Times.Start.Add(startDeterminationTimeDurationFromStart);
            StartDeterminationTimeDurationFromStart = startDeterminationTimeDurationFromStart;
            DeterminationDuration = determinationDuration;
            DeterminationStep = determinationStep;
        }

        public ISimEventAttributes Default()
        {
            throw new System.NotImplementedException();
        }

        //public bool Validate()
        //{
        //    if (StartEventTime < StartDeterminationTime)
        //    {
        //        Log.Error("Derminationtime of an Activation schould be bevor Eventtime.");
        //        return false;
        //    }
        //    else if (ActivationDuration < TimeSpan.FromMinutes(5))
        //    {
        //        Log.Error("Duration schould not be smaller than 5 Minutes.");
        //        return false;
        //    }
        //    else if (ActivationStep < TimeSpan.FromMinutes(5))
        //    {
        //        Log.Error("Step schould not be smaller than 5 Minutes.");
        //        return false;
        //    }
        //    return true;
        //}
    }
}
