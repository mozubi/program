﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;
using winMoz.Simulation;

namespace winMoz.Simulation
{
    public interface ISimEvent : ISimObject
    {
        (DateTime TimeStamp, TimeSpan Duration, TimeSpan Step) EventTimeframe { get; }

        bool IsEventTime(DateTime time);
    }
}
