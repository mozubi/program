﻿using System.Threading;

namespace winMoz.Simulation
{
    public interface ISimObject
    {
        int ScenarioID { get; }
        /// <summary>
        /// Einzigartige ID des Agenten
        /// </summary>
        int Id { get; }
    }
}
