﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Information.PFCs;
using System.Collections.Concurrent;

namespace winMoz.Simulation
{
    public interface ISimObjectWithPrices : ISeedableSimObject
    {
        IEnumerable<(DateTime TimeStamp, float PricePrediction)> GetPriceCurve(PriceProviderType providerType, DateTime timeStamp, TimeSpan duration = default);
    }
}
