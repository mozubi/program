﻿using System.Collections.Generic;
using winMoz.Helper;

namespace winMoz.Simulation
{
    public interface ISeedableSimObject : ISimObject, ISeedable
    {
        /// <summary>
        /// Methode zieht den nächsten Setzwert aus der Setzwerttabelle des SimObjects und entfernt diesen anschließend
        /// </summary>
        /// <returns>Einzigartiger Setzwert als <see cref="int"/></returns>
        int GetSeed();
    }

    public interface ISeedable
    {
        int GetSeed();
    }
}
