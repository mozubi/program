﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Simulation;

namespace winMoz.Simulation
{
    public interface ISimEventAttributes
    {
        DateTime StartEventTime { get; set; } 
        TimeSpanSave StartEventTimeDurationFromStart { get; set; }
        DateTime StartDeterminationTime { get; set; }
        TimeSpanSave StartDeterminationTimeDurationFromStart { get; set; }
        TimeSpanSave DeterminationDuration { get; set; }
        TimeSpanSave DeterminationStep { get; set; }

        ISimEventAttributes Default();

        //TimeSpan AuctionStep { get; set; }

        //bool Validate();
    }
}
