﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.DTO;
using winMoz.Simulation;
using winMoz.Data.Access.Repositories;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Factories;
using winMoz.Assets.Loads;
using winMoz.Assets.Plants;
using winMoz.Information;
using winMoz.Trading.Strategies;
using winMoz.Data.Access.Contexts;
using System.Data.SQLite;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets;
using winMoz.Markets.Elements;
using winMoz.Markets.ControlReserve;


namespace winMoz.Simulation
{
    public class Scenario_Builder
    {
        DSO DSO;
        BalancingGroup BG;
        Dictionary<Plant_Thermal.SubTypes, BalancingGroup> BGs;
        List<int> MaStRIds = new List<int>();
        public int YearBase { get; private set; }
        public int YearScenario { get; private set; }

        private TYNDP2018_ScenarioCalculation ScenCalc;
        private winMoz.Simulation.Scenario ScenData;
        private bool IsDefaultBuild = false;


        public Scenario_Builder() { Config Cfg = new Config(); }


        public void DeleteScenario(int total_ID)
        {
            using (var repoScenario = new ScenarioData_Repository())
            {
                repoScenario.Delete(total_ID);
            }
        }

        private void SetTableAutoId(string tableName, int id)
        {
            string qry = string.Format($@"UPDATE sqlite_sequence SET seq = {id} WHERE name = '{tableName}'");
            var Command = new SQLiteCommand(qry);
            var Connection = new SQLiteConnection(Config.ConnStrings.Sim);
            Connection.Open();
            Command.Connection = Connection;
            Command.ExecuteNonQuery();
        }
        private void SetTableAutoIdOnHighestId(string tableName)
        {
            int id = GetHighestId(tableName);
            SetTableAutoId(tableName, id);
        }

        private int GetHighestId(string tableName)
        {
            string qry = string.Format($@"SELECT MAX([Id]) FROM [{tableName}]");
            var Command = new SQLiteCommand(qry);
            var Connection = new SQLiteConnection(Config.ConnStrings.Sim);
            Connection.Open();
            Command.Connection = Connection;
            var output = Command.ExecuteScalar();
            var HighestId = (output is DBNull) ? 0 : Convert.ToInt32(output);
            return HighestId;
        }

        public void BuildScenario()
        {
            IsDefaultBuild = true;
            BuildScenario(
                start: new DateTime(2014, 12, 31),
                end: new DateTime(2015, 12, 31, 23, 59, 59),
                maStRIds: new List<int>(),
                total_ID: 100000,
                scenarioName: "Default",
                markets_ID: 0,
                assets_ID: 0,
                weatherYear: 2015,
                startDataYear: 2015,
                localMACode: MarketAreaEnum.DE,
                scenario: TYNDP2018_ScenarioCalculation.ScenarioEnum.DEFAULT);
            IsDefaultBuild = false;
        }

        public void BuildScenario(DateTime start, DateTime end, List<int> maStRIds, int total_ID, string scenarioName, int markets_ID, int assets_ID, int weatherYear, int startDataYear, MarketAreaEnum localMACode, TYNDP2018_ScenarioCalculation.ScenarioEnum scenario = TYNDP2018_ScenarioCalculation.ScenarioEnum.DEFAULT)
        {
            BuildScenarioData(
                start: start,
                end: end,
                maStRIds: maStRIds,
                total_ID: total_ID,
                scenarioName: scenarioName,
                markets_ID: markets_ID,
                assets_ID: assets_ID,
                weatherYear: weatherYear,
                startDataYear: startDataYear,
                localMACode: localMACode,
                scenario: scenario);
            List<Scenario> currentScenarios;
            using (ScenarioData_Repository repo = new ScenarioData_Repository())
            {
                currentScenarios = repo.Get().ToList();
            }
            if (IsDefaultBuild || !currentScenarios.Select(item => item.Markets_ID).ToList().Contains(ScenData.Markets_ID))
            {
                    BuildMarkets();
                }
                if (IsDefaultBuild || !currentScenarios.Select(item => item.Assets_ID).ToList().Contains(ScenData.Assets_ID))
                {
                    Build_DSOs();
                    Build_BalancingGroups();
                    Build_Assets();
                }
            using (ScenarioData_Repository repo = new ScenarioData_Repository())
            {
                repo.Add(ScenData);
            }
        }

        private void BuildScenarioData(DateTime start, DateTime end, List<int> maStRIds, int total_ID, string scenarioName, int markets_ID, int assets_ID, int weatherYear, int startDataYear, MarketAreaEnum localMACode, TYNDP2018_ScenarioCalculation.ScenarioEnum scenario = TYNDP2018_ScenarioCalculation.ScenarioEnum.DEFAULT)
        {
            var ScenarioData = new ScenarioData(
                total_ID: total_ID,
                scenarioName: scenarioName,
                markets_ID: markets_ID,
                assets_ID: assets_ID,
                simStartTime: start,
                simEndTime: end,
                localMACode: localMACode,
                assetsYear: start.Year,
                weatherYear: weatherYear,
                startDataYear: startDataYear,
                additionalAssetsMaStR: maStRIds,
                tyndp2018Scenario: scenario);
            YearScenario = start.Year;
            YearBase = Math.Min(2020, YearScenario);

            ScenCalc = new TYNDP2018_ScenarioCalculation(scenario);

            ScenData = new Scenario();
            ScenData.AddScenarioData(ScenarioData);
        }

        public void BuildMarkets()
        {
            //Markets
            FuturesMarket Futures = new FuturesMarket(new FuturesAttributes(ScenData.SimStartTime, ScenData.LocalMACode), ScenData.Markets_ID);
            DayAheadMarket DayAhead = new DayAheadMarket(new DayAheadAttributes(ScenData.SimStartTime, ScenData.LocalMACode), ScenData.Markets_ID);
            IntradayMarket Intraday = new IntradayMarket(new IntradayAttributes(ScenData.SimStartTime, ScenData.LocalMACode), ScenData.Markets_ID);
            ControlReserve_Activation ControlReserve_Activation = new ControlReserve_Activation(new ActivationAttributes(ScenData.SimStartTime), scenarioID: ScenData.Markets_ID);
            ControlReserve_Energy ControlReserve_Energy = new ControlReserve_Energy(new ControlReserve_Energy_Attributes(ScenData.SimStartTime, ScenData.LocalMACode), Intraday, ControlReserve_Activation, ScenData.Markets_ID);
            ControlReserve_Capacity ControlReserve_Capacity = new ControlReserve_Capacity(new ControlReserve_Capacity_Attributes(ScenData.SimStartTime, ScenData.LocalMACode), ControlReserve_Energy, ScenData.Markets_ID);

            ScenData.Markets.AddRange(new List<Market>() { Futures, DayAhead, Intraday, ControlReserve_Capacity });

            //With Markets connected Objects
            IGCC_Calculation IGCCCalculation = new IGCC_Calculation(scenarioID: ScenData.Markets_ID);
            MFRR_Attributes mFRR_Attributes = new MFRR_Attributes(mfrr_Threshold: 600, mfrr_Factor: 0.7f, ScenData.Markets_ID);
            ControlReserveTenderVolumeHandler TenderVolumeHandler = new ControlReserveTenderVolumeHandler(new ControlReserveTenderVolumeAttributes(errorLevel_FRR: 0.03F / 100, errorLevel_aFRR: 0.2F / 100), new List<Market> { ControlReserve_Capacity, ControlReserve_Energy }, ScenData.Markets_ID);
            DeviationFromRZSaldo_Handler DeviationHandler = new DeviationFromRZSaldo_Handler(scenarioID: ScenData.Markets_ID);
            ControlReserveActivationVolumeHandler ActivationVolumeHandler = new ControlReserveActivationVolumeHandler(new List<ControlReserve_Activation> { ControlReserve_Activation }, IGCCCalculation, DeviationHandler, ScenData.Markets_ID);
            ReBAP_Calculation reBAP_Calculation = new ReBAP_Calculation(ReBAP_Attributes_Factory.GetDefault(ScenData.SimStartTime), IGCCCalculation, scenarioID: ScenData.Markets_ID);
            ScenData.ControlReserveActivations.Add(ControlReserve_Activation);
            ScenData.ReBAP_Calculations.Add(reBAP_Calculation);
            ScenData.controlReserveActivationVolumeHandlers.Add(ActivationVolumeHandler);
            ScenData.controlReserveTenderVolumeHandlers.Add(TenderVolumeHandler);
            ScenData.mFRR_Attributes.Add(mFRR_Attributes);
        }

        public void Build_DSOs()
        {
            DSO = new DSO(new TSO(ScenData.Assets_ID), ScenData.Assets_ID);
        }

        public void Build_BalancingGroups()
        {
            BGs = new Dictionary<Plant_Thermal.SubTypes, BalancingGroup>();
            foreach (int item in Enum.GetValues(typeof(Plant_Thermal.SubTypes)))
                BGs.Add((Plant_Thermal.SubTypes)item, new BalancingGroup(new DSO(new TSO(ScenData.Assets_ID), ScenData.Assets_ID), ScenData.Assets_ID));
            foreach (BalancingGroup bg in BGs.Values)
            {
                bg.Trader.ExcludedMarkets = new List<Type>() { typeof(ControlReserve_Capacity), typeof(ControlReserve_Energy) };
                //repoAssets.Add(bg);
            }
            BG = new BalancingGroup(DSO, ScenData.Assets_ID);
            //repoAssets.Add(BG);
            BG.Trader.ExcludedMarkets = new List<Type>() { typeof(ControlReserve_Capacity), typeof(ControlReserve_Energy) };
            ScenData.tsos.AddRange(BGs.Values.Select(el => el.DSO.TSO).Distinct());
            ScenData.tsos.Add(BG.DSO.TSO);
        }

        public void Build_Assets()
        {
            Build_ThermalPlants();
            Build_WindOnshore();
            Build_HydroRunOfRiver();
            Build_PV();
            Build_Biomass();
            Build_HydroPumpedStorage();
            Build_Load();
        }

        public void Build_ThermalPlants()
        {
            //List<Plant_Thermal> Plants = GetKWK();
            //Plants.AddRange(GetNotKWK());
            List<Plant_Thermal> Plants = GetThermalKond();
            //repoAssets.AddRange(Plants);
            List<Plant_Thermal> Plants_kwk = GetThermalKWK();
            //repoAssets.AddRange(Plants_kwk);
        }

        private List<Plant_Thermal> GetThermalKWK()
        {
            Thermal_KWK_Nuts2_Repository repoNuts2 = new Thermal_KWK_Nuts2_Repository();
            var dataNuts2 = repoNuts2.GetAllForYear(YearScenario);
            Thermal_KWK_All_Repository repoAll = new Thermal_KWK_All_Repository();
            var dataAll = repoAll.GetAllNamedForYear(YearBase, MaStRIds);
            dataAll.ForEach(all => dataNuts2.FirstOrDefault(nuts2 =>
                nuts2.Energietraeger == all.Energietraeger &&
                nuts2.Inbetriebnahmejahrfuenft == all.Inbetriebnahmejahrfuenft &&
                nuts2.Stilllegungsjahrfuenft == all.Stilllegungsjahrfuenft &&
                nuts2.Nuts2_code == all.Nuts2_code &&
                nuts2.IstIndustrie == all.IstIndustrie &&
                nuts2.IstGUD == all.IstGUD).ReducePower(all.Nettonennleistung, all.KWK_ThermischeNutzleistung, all.KWK_ElektrischeNutzleistung));
            var Plants = GetThermalKWKNuts2(dataNuts2);
            Plants.AddRange(GetThermalKWKAll(dataAll));
            return Plants;
        }

        private List<Plant_Thermal> GetThermalKond()
        {
            Thermal_Kond_DE_Repository repoNuts2 = new Thermal_Kond_DE_Repository();
            var dataNuts2 = repoNuts2.GetAllForYear(YearScenario);
            Thermal_Kond_All_Repository repoAll = new Thermal_Kond_All_Repository();
            var dataAll = repoAll.GetAllNamedForYear(YearBase, MaStRIds);
            dataAll.ForEach(all => dataNuts2.FirstOrDefault(nuts2 =>
                nuts2.Energietraeger == all.Energietraeger &&
                nuts2.Inbetriebnahmejahrfuenft == all.Inbetriebnahmejahrfuenft &&
                nuts2.Stilllegungsjahrfuenft == all.Stilllegungsjahrfuenft &&
                nuts2.IstIndustrie == all.IstIndustrie &&
                nuts2.IstGUD == all.IstGUD).ReducePower(all.Nettonennleistung));
            var Plants = GetThermalKondNuts2(dataNuts2);
            Plants.AddRange(GetThermalKondAll(dataAll));
            return Plants;
        }

        private List<Plant_Thermal> GetThermalKWKNuts2(List<Thermal_KWK_Nuts2_Dto> data)
        {
            var Kraftwerke = data.Select(item => new
            {
                Data = item,
                Type = GetSubType(item.Energietraeger),
                Asset = new Plant_Thermal(BGs[GetSubType(item.Energietraeger)],
                    GetSubType(item.Energietraeger),
                    item.KWK_ElektrischeNutzleistung / 1000.0F,
                    location: Location.FromNUTS2(item.Nuts2_code),
                    P_TLP: item.KWK_ThermischeNutzleistung / 1000.0F,
                    scenarioID: ScenData.Assets_ID,
                    tradingStrategy: TradingStrategyType.Nothing
                    )
            }).ToList();
            //Erzeugen einer Unit pro Kraftwerk
            Kraftwerke.ForEach(asset =>
                asset.Asset.CreateUnits(new List<Block>() { new Block_CHP(
                  asset.Asset.BalancingGroup,
                  subType: asset.Asset.SubType,
                  powerMW: asset.Asset.PowerMW,
                  PowerMWTh: asset.Data.KWK_ThermischeNutzleistung / 1000.0F,
                  TotalEff: 0.8F,
                  commodity: GetCommodity(asset.Data.Energietraeger),
                  ageType: GetAgeTypeByCommodityAndEfficiency(GetCommodity(asset.Data.Energietraeger), GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr), asset.Data.IstGUD),
                  plant: asset.Asset,
                  efficiency: GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr),
                  asset.Asset.LocationPostCode,
                  asset.Asset.ScenarioID,
                  TradingStrategyType.Hedge) }
                ));
            //IEnumerable<(Kraftwerke.KraftwerkeKWK_Dto item, winMoz.Asset_Dto)> assetDtosNeu = kraftwerke.Select(item => (item, new winMoz.Asset_Dto(unitPower: new List<float> { (item.Nettonennleistung / 1000.0) })));

            //Erzeugen einer Unit pro Kraftwerk
            return Kraftwerke.Select(item => item.Asset).ToList();
        }

        private List<Plant_Thermal> GetThermalKWKAll(List<Thermal_KWK_All_Dto> data)
        {
            var Kraftwerke = data.Select(item => new
            {
                Data = item,
                Type = GetSubType(item.Energietraeger),
                Asset = new Plant_Thermal(BGs[GetSubType(item.Energietraeger)],
                   GetSubType(item.Energietraeger),
                   item.KWK_ElektrischeNutzleistung / 1000.0F,
                   location: Location.FromPostcode(item.Plz),
                   P_TLP: item.KWK_ThermischeNutzleistung,
                   scenarioID: ScenData.Assets_ID,
                   tradingStrategy: TradingStrategyType.Nothing
                   )
            }).ToList();
            //Erzeugen einer Unit pro Kraftwerk
            Kraftwerke.ForEach(asset =>
               asset.Asset.CreateUnits(new List<Block>() { new Block_CHP(
                  asset.Asset.BalancingGroup,
                  subType: asset.Asset.SubType,
                  powerMW: asset.Asset.PowerMW,
                  PowerMWTh: asset.Data.KWK_ThermischeNutzleistung,
                  TotalEff: 0.8F,
                  commodity: GetCommodity(asset.Data.Energietraeger),
                  ageType: GetAgeTypeByCommodityAndEfficiency(GetCommodity(asset.Data.Energietraeger), GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr), asset.Data.IstGUD),
                  plant: asset.Asset,
                  efficiency: GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr),
                  asset.Asset.LocationPostCode,
                  asset.Asset.ScenarioID,
                  TradingStrategyType.Hedge) }
                ));
            return Kraftwerke.Select(item => item.Asset).ToList();
        }

        private List<Plant_Thermal> GetThermalKondNuts2(List<Thermal_Kond_DE_Dto> data)
        {
            var Kraftwerke = data.Select(item => new
            {
                Data = item,
                Type = GetSubType(item.Energietraeger),
                Asset = new Plant_Thermal(BGs[GetSubType(item.Energietraeger)],
                    GetSubType(item.Energietraeger),
                    item.Nettonennleistung / 1000.0F,
                    locationPostcode: 66115,
                    scenarioID: ScenData.Assets_ID,
                    tradingStrategy: TradingStrategyType.Nothing
                    )
            }).ToList();
            //Erzeugen einer Unit pro Kraftwerk
            Kraftwerke.ForEach(asset =>
                asset.Asset.CreateUnits(new List<Block>() { new Block_Kond(
                   asset.Asset.BalancingGroup,
                   subType : asset.Asset.SubType,
                   powerMW: asset.Asset.PowerMW,
                   commodity: GetCommodity(asset.Data.Energietraeger),
                   ageType: GetAgeTypeByCommodityAndEfficiency(GetCommodity(asset.Data.Energietraeger), GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr), asset.Data.IstGUD),
                   plant: asset.Asset,
                   efficiency: GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr),
                   asset.Asset.LocationPostCode,
                   asset.Asset.ScenarioID,
                   TradingStrategyType.MakeOrBuy)
                }));
            return Kraftwerke.Select(item => item.Asset).ToList();
        }

        private List<Plant_Thermal> GetThermalKondAll(List<Thermal_Kond_All_Dto> data)
        {
            var Kraftwerke = data.Select(item => new
            {
                Data = item,
                Type = GetSubType(item.Energietraeger),
                Asset = new Plant_Thermal(BGs[GetSubType(item.Energietraeger)],
                    GetSubType(item.Energietraeger),
                    item.Nettonennleistung / 1000.0F,
                    locationPostcode: item.Plz,
                    scenarioID: ScenData.Assets_ID,
                    tradingStrategy: TradingStrategyType.Nothing
                    )
            }).ToList();
            //Erzeugen einer Unit pro Kraftwerk
            Kraftwerke.ForEach(asset =>
                asset.Asset.CreateUnits(new List<Block>() { new Block_Kond(
                   asset.Asset.BalancingGroup,
                   subType : asset.Asset.SubType,
                   powerMW: asset.Asset.PowerMW,
                   commodity: GetCommodity(asset.Data.Energietraeger),
                   ageType: GetAgeTypeByCommodityAndEfficiency(GetCommodity(asset.Data.Energietraeger), GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr), asset.Data.IstGUD),
                   plant: asset.Asset,
                   efficiency: GetDefaultWK(GetIntTyp(asset.Data.Energietraeger, asset.Data.IstGUD), asset.Data.Inbetriebnahmejahr),
                   asset.Asset.LocationPostCode,
                   asset.Asset.ScenarioID,
                   TradingStrategyType.MakeOrBuy)
                }));

            return Kraftwerke.Select(item => item.Asset).ToList();
        }


        public void Build_Load()
        {
            //Load
            Load_Grid GridLoad = Load_Grid.FromScale(BG, 1, scenarioID: ScenData.Assets_ID);
            //repoAssets.AddRange(new List<Load_Grid>() { GridLoad });
        }



        public void Build_WindOnshore()
        {
            //WindAggregate
            var repo = new WindDtoRepository();
            var WindAggregateDtos = repo.GetAllAggregated().ToList();
            var repoCorrected = new WindOnshore_Nuts2_Repository();
            var WindAggregateDtosCorrected = repoCorrected.GetPowerForYear(YearScenario);
            if (YearScenario > YearBase)
            {
                float WindBasePowerMW = WindAggregateDtosCorrected.Select(item => item.PowerKW / 1000.0F).Sum();
                float WindScenarioPowerMW = ScenCalc.GetWindOnshore(YearScenario, WindBasePowerMW);
                float Scale = WindScenarioPowerMW / WindBasePowerMW;
                WindAggregateDtosCorrected = WindAggregateDtosCorrected.Select(item => (item.Nuts2, item.PowerKW * Scale)).ToList();
            }
            var WindAggregateDtosWithCorrection = WindAggregateDtos.Join(WindAggregateDtosCorrected, item => Location.FromPostcode((int)item.Postcode).NUTS2, item => item.Nuts2, (before, corrected) => (before, corrected)).ToList();
            WindAggregateDtosWithCorrection.ForEach(item => item.before.PowerKW = item.corrected.PowerKW);
            List<Plant_Wind> WindPlantsAggregate = WindAggregateDtosWithCorrection.Select(item => item.before).ToList()
                .Select(dto => new Plant_Wind(BG, dto.PowerKW / 1000.0F, dto, dto.WECmodelId == 1 ? Plant_Wind.SubTypes.wind_onshore_north : Plant_Wind.SubTypes.wind_onshore_south, ScenData.Assets_ID, TradingStrategyType.HedgeVRE)).ToList();
            //repoAssets.AddRange(WindPlantsAggregate);
        }

        public void Build_PV()
        {
            //PVAggregate
            var repo = new Repository<PVDto>(new PVDtoContext());
            var PVAggregateDtos = repo.Get().ToList();
            var repoCorrected = new PV_DE_Repository();
            var PVAggregateDtosCorrectedPower = repoCorrected.GetPowerForYear(YearScenario);
            var PV2015CorrectedPower = repoCorrected.GetPowerForYear(2015);
            var PV2015AggregateDtosPower = PVAggregateDtos.Select(item => item.PowerMW).Sum();
            float ScaleDtos = PV2015AggregateDtosPower / PV2015CorrectedPower;
            if (YearScenario > YearBase)
            {
                float PVBasePowerMW = PVAggregateDtosCorrectedPower;
                float PVScenarioPowerMW = ScenCalc.GetPV(YearScenario, PVBasePowerMW);
                float Scale = PVScenarioPowerMW / PVBasePowerMW;
                PVAggregateDtosCorrectedPower = Scale * PVAggregateDtosCorrectedPower;
            }
            ScaleDtos = ScaleDtos * PVAggregateDtosCorrectedPower / PV2015AggregateDtosPower;
            PVAggregateDtos.ForEach(item => item.PowerMW = item.PowerMW * ScaleDtos);
            List<Plant_PV> PVPlantsAggregate = PVAggregateDtos.Select(dto => new Plant_PV(BG, dto.PowerMW, Location.FromPostcode(dto.PostCode), dto.ModuleAngle, dto.ModuleAzimut, dto.LoadFactor, Plant_PV.SubTypes.PV_AREA)).ToList();
            //repoAssets.AddRange(PVPlantsAggregate);
        }

        public void Build_Biomass() //8 GW Biogas, 1 GW Altholz
        {
            //Biomass
            var repoCorrected = new Biomass_DE_Repository();
            var BiomassAggregatePowerMW = repoCorrected.GetPowerForYear(YearScenario) / 1000.0F;
            var repoExtraPower = new BiomassExtraPower_Repository();
            float BiomassExtraPower = 0;
            if (YearScenario >= repoExtraPower.GetFirstYear())
                BiomassExtraPower = repoExtraPower.GetForYear(YearScenario);
            if (YearScenario > YearBase)
            {
                float BiomassBasePowerMW = BiomassAggregatePowerMW;
                float BiomassScenarioPowerMW = ScenCalc.GetBiomass(YearScenario, BiomassBasePowerMW);
                float Scale = BiomassScenarioPowerMW / BiomassBasePowerMW;
                BiomassAggregatePowerMW = Scale * BiomassAggregatePowerMW;
            }
            Plant_Biomass Biomass = Plant_Biomass.FromRelativePower(BG, BiomassAggregatePowerMW - BiomassExtraPower, 0, BiomassExtraPower, Location.FromPostcode(66115), Plant_Biomass.SubTypes.BIOMASS, TradingStrategyType.HedgeVRE, ScenData.Assets_ID);
            //repoAssets.AddRange(new List<Plant_Biomass>() { Biomass });
        }

        public void Build_HydroPumpedStorage()
        {
            //Hydro_Pumped_Storage
            var repoCorrected = new Storage_DE_Repository();
            var HydroPumpedStorageAggregatePowerMW = repoCorrected.GetPowerForYear("Pumpspeicher", YearScenario) / 1000.0F;
            if (YearScenario > YearBase)
            {
                float HydroPumpedStorageBasePowerMW = HydroPumpedStorageAggregatePowerMW;
                float HydroPumpedStorageScenarioPowerMW = ScenCalc.GetHydroPumpStorage(YearScenario, HydroPumpedStorageBasePowerMW);
                float Scale = HydroPumpedStorageScenarioPowerMW / HydroPumpedStorageBasePowerMW;
                HydroPumpedStorageAggregatePowerMW = Scale * HydroPumpedStorageAggregatePowerMW;
            }
            Plant_Storage HydroPumpedStorage = new Plant_Storage(BG, HydroPumpedStorageAggregatePowerMW, Location.FromPostcode(66115), HydroPumpedStorageAggregatePowerMW * 10, Plant_Storage.SubTypes.PUMPED_WATER, ScenData.Assets_ID, TradingStrategyType.Arbitrage);
            //repoAssets.AddRange(new List<Plant_Storage>() { HydroPumpedStorage });
        }

        public void Build_HydroRunOfRiver()
        {
            //Hydro Run of River
            var repo = new Hydro_RunOfRiver_DE_Repository();
            var data = repo.GetPowerForYear(YearScenario);
            if (YearScenario > YearBase)
            {
                float HydroBasePowerMW = data.Select(item => item.PowerKW / 1000.0F).Sum();
                float HydroScenarioPowerMW = ScenCalc.GetHydroRunOfRiver(YearScenario, HydroBasePowerMW);
                float Scale = HydroScenarioPowerMW / HydroBasePowerMW;
                data = data.Select(item => (item.IstEEG, item.PowerKW * Scale)).ToList();
            }
            List<Plant_Hydro> Plants_Hydro = data.Select(item => new Plant_Hydro(BG, Location.FromPostcode(66115), item.PowerKW / 1000, item.IstEEG, ScenData.Assets_ID)).ToList();
            //repoAssets.AddRange(Plants_Hydro);
        }

        public Plant_Thermal.AgeTypes GetAgeTypeByCommodityAndEfficiency(Commodities.Commodity commodity, float efficiency, bool isGT)
        {
            List<Tynp> tynps;
            using (var repo = new ScenarioData_Repository())
            {
                tynps = repo.ThermalDataRepository.Get().ToList();
            }
            if (isGT)
            {

            }
            List<Tynp> tynpsCommodity = tynps.Where(el => el.Commodity == commodity && !el.AgeType.ToString().Contains("CCS") && !el.AgeType.ToString().Contains("OCGT") && ((commodity == Commodities.Commodity.Natural_Gas && isGT) ? el.AgeType.ToString().Contains("GT") : !el.AgeType.ToString().Contains("GT"))).ToList();
            if (tynpsCommodity.Count() != 0)
            {
                var ageType = tynpsCommodity.OrderBy(item => Math.Pow(item.Efficiency.AtPnom - efficiency, 2)).First().AgeType;
                return ageType;
            }
            else
                throw new Exception("Commodityn not found");
        }

        public Plant_Thermal.SubTypes GetSubType(string typeString)
        {
            switch (typeString)
            {
                case "Steinkohle": return Plant_Thermal.SubTypes.COAL;
                case "Braunkohle": return Plant_Thermal.SubTypes.LIGNITE;
                case "Kernenergie": return Plant_Thermal.SubTypes.NUCLEAR;
                case "Mineralölprodukte": return Plant_Thermal.SubTypes.OIL;
                case "Erdgas": return Plant_Thermal.SubTypes.GAS;
                default: return Plant_Thermal.SubTypes.BIOGAS;
            }
        }

        public Commodities.Commodity GetCommodity(string typeString)
        {
            switch (typeString)
            {
                case "Steinkohle": return Commodities.Commodity.Hard_Coal;
                case "Braunkohle": return Commodities.Commodity.Lignite;
                case "Kernenergie": return Commodities.Commodity.Uranium;
                case "Mineralölprodukte": return Commodities.Commodity.Oil;
                case "Erdgas": return Commodities.Commodity.Natural_Gas;
                default: return Commodities.Commodity.DEFAULT;
            }
        }

        public int GetIntTyp(string typeString, bool isGUD)
        {
            switch (typeString)
            {
                case "Steinkohle": return 1;
                case "Braunkohle": return 2;
                case "Kernenergie": return 7;
                case "Mineralölprodukte": return 5;
                case "Erdgas": if (isGUD) return 3; else return 4;
                default: return 0;
            }
        }

        public float GetDefaultWK(int intTyp, int intJahr)
        {

            float dblWK = 0;

            if (intJahr < 1960) intJahr = 1960;

            switch (intTyp)
            {
                case 1: //Steinkohle

                    dblWK = -0.2514F * (2020 - intJahr) + 48.8F;
                    break;

                case 2: // 'Braunkohle

                    dblWK = -0.2114F * (2020 - intJahr) + 45.733F;
                    break;

                case 3: // 'Gas-GuD

                    dblWK = -0.4486F * (2020 - intJahr) + 62.8667F;
                    break;

                case 4: // 'Gas GT

                    dblWK = -0.4486F * (2020 - intJahr) + 48.667F;
                    break;

                case 5: // 'Öl GT

                    dblWK = -0.4486F * (2020 - intJahr) + 48.667F;
                    break;
                case 6:

                    dblWK = 100;
                    break;
                case 7: // 'Kernkraft
                    dblWK = 33;
                    break;
                default:
                    dblWK = 40;
                    break;

            }

            return dblWK / 100;

        }
    }
}
