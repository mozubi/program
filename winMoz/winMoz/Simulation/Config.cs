﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using winMoz.Data.Access;
using winMoz.Data.Access.Contexts;
using winMoz.Helper;

namespace winMoz.Simulation
{
    public sealed class Config
    {
        public static string ProjectPath { get; private set; } = Directory.GetParent(System.Reflection.Assembly.GetCallingAssembly().Location).Parent.Parent.Parent.FullName;


        public struct ConnStrings
        {
            public static string Sim = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_sim.db";
            public static string Schedule = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_schedule.db";
            public static string SumSchedule = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_sumSchedule.db";
            public static string ExternalMarketResults = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_ExternalMarketResults.db";
            public static string Assets = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_assets.db";
            public static string Weather = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_wx.db";
            public static string WindPlants = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_windplants.db";
            public static string MarketResults = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_marketResults.db";
            public static string Bids = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_bids.db";
            public static string Logging = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_logging.db";
            public static string PredictionData = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_prognosedaten.db";
            public static string MCCData = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_MCC.db";
            public static string CommodititiesData = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_Commodities.db";
            public static string Prediction_IZES = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_PredictionData_IZES.db";
            public static string MaStaReg = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_MaStaReg.db";
            public static string SimObjects = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_simObjects.db";
            public static string MaStR_Reduced = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_MaStR_Reduced.db";
            public static string ControlReserveDemand = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_CR_Demand.db";
            public static string ResultPower = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_resultPower.db";
            public static string PlotAndOutputVariablesPlot = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_plotAndOutputVariablesPlot.db";
            public static string PlotAndOutputVariablesConsole = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_plotAndOutputVariablesConsole.db";
            public static string PlotAndOutputVariablesCSV = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_plotAndOutputVariablesCSV.db";
            public static string AssetsFromMaStR = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_AssetsFromMaStR.db";
            public static string TYNP2018Scenario = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_TYNDP2018_Scenario.db";
            public static string PVPlants = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQLite\mzdb_pvplants.db";
            public static string BiomassExtraPower = $@"Data Source={ProjectPath}\winMoz\Data\Files\SQlite\mzdb_Biomass_Extra_Power.db";
        }

        public struct DBOptions
        {
            public static DbContextOptions simContextOptions; 
        }


        /// <summary>
        /// Standard-Konstruktor für die Config-Klasse - Lädt Konfigurations-XML und Pfade
        /// </summary>
        public Config(string projectPath = default)
        {
            if (projectPath != default) ProjectPath = projectPath;

            Initialize_DBOptions();
            Initialize_Databases();
            Load_XML_Files();
            SetCultureInfo();
        }
        public void Initialize_DBOptions()
        {
            DBOptions.simContextOptions = new DbContextOptionsBuilder()
                .UseSqlite(Config.ConnStrings.Sim)
                .UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll)
                .Options;
        }

        public void Initialize_Databases()
        {
            //DbHelper.ReCreateDb<PlotAndOutput_Variables_Console_Context>();
            //DbHelper.ReCreateDb<PlotAndOutput_Variables_CSV_Context>();
            //DbHelper.ReCreateDb<PlotAndOutput_Variables_Plot_Context>();
            DbHelper.ReCreateDb<MarketContext>();
            DbHelper.ReCreateDb<BidContext>();
            DbHelper.ReCreateDb<LoggingContext>();
            //DbHelper.ReCreateDb<AssetContext>();
            DbHelper.ReCreateDb<ResultPowerContext>();
            DbHelper.ReCreateDb<ScheduleContext>();
            DbHelper.ReCreateDb<SumScheduleContext>();
            DbHelper.ReCreateDb<ExternalMarketResultsContext>();
            DbHelper.ReCreateDb<ControlReserveDemandContext>(); //TODO: Wenn in winMoz überführt muss Kommentar entfernt werden.
        }

        #region XML-Pfade und Dokumente
        public struct XMLPaths
        {
            public static string CommodityPrices { get; set; }
            public static string GasSLP { get; set; }
            public static string Agents { get; set; }
        }

        internal struct XmlFiles
        {
            public static XDocument CommodityPrices;
            public static XDocument GasSLP;
            public static XDocument Agents;
        }
        #endregion

        private static void Load_XML_Files()
        {
            var assembly = typeof(Config).Assembly;
            List<string> resourceNames = assembly.GetManifestResourceNames().ToList();
            XMLPaths.CommodityPrices = resourceNames.Where(name => name.Contains("CommodityPrices.xml")).SingleOrDefault(); // Pfad zur XML-Constants Datei
            XMLPaths.GasSLP = resourceNames.Where(name => name.Contains("GasSLPDistribution.xml")).SingleOrDefault(); // Pfad zur XML-Standardlastprofil Datei
            XMLPaths.Agents = resourceNames.Where(name => name.Contains("Agents.xml")).SingleOrDefault(); // Pfad zur XML-AgentenDaten Datei

            XmlFiles.CommodityPrices = XML.Load_Xml_File(XMLPaths.CommodityPrices, assembly);
            XmlFiles.GasSLP = XML.Load_Xml_File(XMLPaths.GasSLP, assembly);
            XmlFiles.Agents = XML.Load_Xml_File(XMLPaths.Agents, assembly);
        }

        /// <summary>
        /// Zusammenfassende Methode zum Laden aller Konfigurationseinstellungen
        /// (Sim- und WorldVariables)
        /// </summary>
        private static void SetCultureInfo()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        }

        #region Python-Specific
        public static void AddPythonToPATH()
        {
            Thread t = new Thread(new ThreadStart(GetAndWritePythonPath));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
        }

        private static void GetAndWritePythonPath()
        {
            string file = GetFile();
            var scope = EnvironmentVariableTarget.User;
            var oldValue = Environment.GetEnvironmentVariable("PATH", scope);
            var newValue = oldValue + ";" + file;
            Environment.SetEnvironmentVariable("PATH", newValue, scope);
        }

        private static string GetFile()
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.Description = $@"Please navigate to the folder where your python.exe is located. Default: {Environment.SpecialFolder.LocalApplicationData.ToString()}\Programs\Python\..";
            folderBrowser.RootFolder = Environment.SpecialFolder.Desktop;
            folderBrowser.ShowNewFolderButton = false;
            if (folderBrowser.ShowDialog() == DialogResult.OK)
                if (folderBrowser.SelectedPath.ToLowerInvariant().Contains("python"))
                    return folderBrowser.SelectedPath;

            throw new FileNotFoundException("Error: Wrong filename.");

        }
        #endregion
    }
}
