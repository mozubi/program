﻿using System.Threading;
using winMoz.Information;

namespace winMoz.Simulation
{
    /// <summary>
    /// Simobject ist die Oberklasse für alle Objekte in der Simulation. 
    /// </summary>
    public abstract class SimObject : ISimObject
    {
        /// <summary>
        /// ScnearioID gibt die Verknüpfung zum Szenario wieder
        /// </summary>
        public int ScenarioID { get; private set; }
        /// <summary>
        /// Einzigartige ID des Agenten
        /// </summary>
        public int Id { get; private set; }

        #region Count & Seed
        private int simObjectId = 1;
        public static int Count { get; private set; } = 1;

        protected SimObject() {}
        protected SimObject(int scenarioID = 0) 
        {
            //this.Id = simObjectId;
            Interlocked.Increment(ref simObjectId);
            Count = simObjectId;
            ScenarioID = scenarioID;
        }
        #endregion
    }
}
