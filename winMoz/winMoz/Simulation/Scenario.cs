﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Agents;
using winMoz.Assets;
using winMoz.Data.Access.Repositories;
using winMoz.Markets;
using winMoz.Markets.Elements;
using winMoz.Markets.ControlReserve;
using winMoz.Simulation;
using winMoz.Markets.EoMMarkets;
using winMoz.Assets.Plants;
using winMoz.Assets.Schedules;
using winMoz.Data.Access.Repositories.Prediction;
using winMoz.Data.Access.DTO;
using winMoz.Assets.Loads;

namespace winMoz.Simulation
{
    public class Scenario
    {
        public int Total_ID { get; private set; }
        public string ScenarioName { get; private set; }
        public List<TSO> tsos { get; private set; } = new List<TSO>();
        public List<DSO> dsos => tsos.SelectMany(el => el.dsos).ToList();
        List<IBalancingGroup> bgs => dsos.SelectMany(el => el.BalancingGroups).ToList();

        public List<Market> Markets { get; private set; } = new List<Market>();

        public List<ControlReserve_Activation> ControlReserveActivations { get; private set; } = new List<ControlReserve_Activation>();

        public List<ReBAP_Calculation> ReBAP_Calculations { get; private set; } = new List<ReBAP_Calculation>();
        public List<ControlReserveTenderVolumeHandler> controlReserveTenderVolumeHandlers { get; private set; } = new List<ControlReserveTenderVolumeHandler>();
        public List<ControlReserveActivationVolumeHandler> controlReserveActivationVolumeHandlers { get; private set; } = new List<ControlReserveActivationVolumeHandler>();
        public List<MFRR_Attributes> mFRR_Attributes { get; private set; } = new List<MFRR_Attributes>();
        public int Assets_ID { get; private set; }
        public int Markets_ID { get; private set; }
        public DateTime SimStartTime { get; private set; }
        public DateTime SimEndTime { get; private set; }
        public MarketAreaEnum LocalMACode { get; private set; }
        public int AssetsYear { get; private set; }
        public int WeatherYear { get; private set; }
        public int StartDataYear { get; private set; }
        public List<int> AdditionalAssetsMaStR { get; private set; }
        public TYNDP2018_ScenarioCalculation.ScenarioEnum TYNDP2018Scenario { get; private set; }
        MarketHandler marketHandler;

        public List<IAsset> assets => bgs.SelectMany(el => el.Assets).ToList();
        // create TSO
        // create DSO
        // create BG
        // create Asset
        // create Markets
        // 
        public Scenario()
        { }

        public void AddScenarioData(ScenarioData scenData)
        {
            Total_ID = scenData.Total_ID;
            ScenarioName = scenData.ScenarioName;
            Assets_ID = scenData.Assets_ID;
            Markets_ID = scenData.Markets_ID;
            SimStartTime = scenData.SimStartTime;
            SimEndTime = scenData.SimEndTime;
            LocalMACode = scenData.LocalMACode;
            AssetsYear = scenData.AssetsYear;
            WeatherYear = scenData.WeatherYear;
            StartDataYear = scenData.StartDataYear;
            AdditionalAssetsMaStR = scenData.AdditionalAssetsMaStR;
            TYNDP2018Scenario = scenData.TYNDP2018Scenario;
        }
        static public Scenario Load(int Total_ID)
        {
            Scenario scen;
            using (var repo = new ScenarioData_Repository())
            {
                scen = repo.GetFromID(Total_ID);
                scen.Markets = repo.MarketRepository.GetMarkets(scen.Markets_ID);
                var controlReserveEnergies = scen.Markets.Where(item => item is ControlReserve_Energy).ToList();
                scen.ControlReserveActivations = controlReserveEnergies.Select(item => ((ControlReserve_Energy)item).Activation).ToList();
                var controlReserveIntraday = controlReserveEnergies.Select(item => ((ControlReserve_Energy)item).Intraday).ToList();
                scen.marketHandler = new MarketHandler(scen.Markets.ToArray());
                var ControlReserveDemandHandlers = repo.MarketRepository.GetControlReserveTenderVolumeHandlers(scen.Markets_ID);
                if (ControlReserveDemandHandlers.Count() > 0)
                    ControlReserveDemandHandlers.ForEach(handler => handler.SetMarkets(scen.Markets));
                var ControlReserveActivationVolumeHandlers = repo.MarketRepository.GetControlReserveActivationVolumeHandlers(scen.Markets_ID);
                List<IGCC_Calculation> IGCCs = new List<IGCC_Calculation>();
                if (ControlReserveActivationVolumeHandlers.Count() > 0)
                {
                    ControlReserveActivationVolumeHandlers.ForEach(handler => handler.SetActivations(scen.ControlReserveActivations));
                    IGCCs = ControlReserveActivationVolumeHandlers.Select(handler => handler.IGCC).ToList();
                }
                scen.ReBAP_Calculations = repo.MarketRepository.GetReBAP_Calculations(scen.Markets_ID);
                if (scen.ReBAP_Calculations.Count() > 0)
                {
                    scen.ReBAP_Calculations.ForEach(handler => handler.SetIGCC(IGCCs));
                    if (scen.ReBAP_Calculations.Any(item => item.IGCC == null))
                    {
                        IGCCs = repo.MarketRepository.GetIGCCs(scen.Markets_ID);
                        scen.ReBAP_Calculations.ForEach(handler => handler.SetIGCC(IGCCs));
                    }
                }

                Log.Info($" Start initialization of Assets");

                scen.tsos = repo.AssetRepository.GetTSO(scen.Assets_ID);
                var assets = repo.AssetRepository.GetAllAssets(ScenarioID: scen.Assets_ID);
                foreach (var tso in scen.tsos)
                    foreach (var dso in tso.dsos)
                    {
                        foreach (var bg in dso._BalancingGroups)
                        {
                            bg.Register(assets.Where(asset => asset.BalancingGroupId == bg.Id));
                            assets.ForEach(asset => asset.SetBalancingGroup(bg));
                            bg.SubscribeToGateClosed();
                            bg.SubscribeToMarket(scen.marketHandler.Markets.Select(market => (Market)market).ToArray());
                        }
                        dso.BuildBalancingGroups();
                        dso.TimeSeries.SubscribeToMarket(scen.marketHandler.Markets.Where(item => item is IntradayMarket).Select(market => (Market)market).ToArray());
                    }
                Log.Info($" Assets initialized");
            }
            return scen;
        }
        public Scenario(ScenarioData scenData)
        {
            Total_ID = scenData.Total_ID;
            ScenarioName = scenData.ScenarioName;
            Assets_ID = scenData.Assets_ID;
            Markets_ID = scenData.Markets_ID;
            SimStartTime = scenData.SimStartTime;
            SimEndTime = scenData.SimEndTime;
            LocalMACode = scenData.LocalMACode;
            AssetsYear = scenData.AssetsYear;
            WeatherYear = scenData.WeatherYear;
            AdditionalAssetsMaStR = scenData.AdditionalAssetsMaStR;
            TYNDP2018Scenario = scenData.TYNDP2018Scenario;
            using (var repo = new ScenarioData_Repository())
            {
                Markets = repo.MarketRepository.GetMarkets(scenData.Markets_ID);
                var controlReserveEnergies = Markets.Where(item => item is ControlReserve_Energy).ToList();
                ControlReserveActivations = controlReserveEnergies.Select(item => ((ControlReserve_Energy)item).Activation).ToList();
                var controlReserveIntraday = controlReserveEnergies.Select(item => ((ControlReserve_Energy)item).Intraday).ToList();
                marketHandler = new MarketHandler(Markets.ToArray());
                var ControlReserveDemandHandlers = repo.MarketRepository.GetControlReserveTenderVolumeHandlers(scenData.Markets_ID);
                if (ControlReserveDemandHandlers.Count() > 0)
                    ControlReserveDemandHandlers.ForEach(handler => handler.SetMarkets(Markets));
                var ControlReserveActivationVolumeHandlers = repo.MarketRepository.GetControlReserveActivationVolumeHandlers(scenData.Markets_ID);
                List<IGCC_Calculation> IGCCs = new List<IGCC_Calculation>();
                if (ControlReserveActivationVolumeHandlers.Count() > 0)
                {
                    ControlReserveActivationVolumeHandlers.ForEach(handler => handler.SetActivations(ControlReserveActivations));
                    IGCCs = ControlReserveActivationVolumeHandlers.Select(handler => handler.IGCC).ToList();
                }
                ReBAP_Calculations = repo.MarketRepository.GetReBAP_Calculations(scenData.Markets_ID);
                if (ReBAP_Calculations.Count() > 0)
                {
                    ReBAP_Calculations.ForEach(handler => handler.SetIGCC(IGCCs));
                    if (ReBAP_Calculations.Any(item => item.IGCC == null))
                    {
                        IGCCs = repo.MarketRepository.GetIGCCs(scenData.Markets_ID);
                        ReBAP_Calculations.ForEach(handler => handler.SetIGCC(IGCCs));
                    }
                }
            }
            Log.Info($" Start initialization of Assets");
            using (var repo = new ScenarioData_Repository())
            {
                tsos = repo.AssetRepository.GetTSO(scenData.Assets_ID);
                var assets = repo.AssetRepository.GetAllAssets(ScenarioID: scenData.Assets_ID);
                foreach (var tso in tsos)
                    foreach (var dso in tso.dsos)
                        foreach (var bg in dso._BalancingGroups)
                        {
                            bg.Register(assets.Where(asset => asset.BalancingGroupId == bg.Id));
                            assets.ForEach(asset => asset.SetBalancingGroup(bg));
                            bg.SubscribeToGateClosed();
                            bg.SubscribeToMarket(marketHandler.Markets.Select(market => (Market)market).ToArray());
                        }
            }
            Log.Info($" Assets initialized");
        }

        public Scenario(List<TSO> _tsos)
        {
            tsos = _tsos;
        }

        public void Step(DateTime time)
        {
            marketHandler.RunMarkets(time);

            if (time == SimStartTime.AddMonths(1))
            {
                // retrain the QHPFC model after 1 month
                Information.Info.QHPFC.RebuildModel();
            }
            // empty cache after initial run of one day
            if (time == SimStartTime.AddDays(2))
            {
                using (var repo = new PredictionDataRepository())
                {
                    repo.DeleteAllTrainingData(SimStartTime, time);
                }
            }
            if (time.Hour == 0 && time.Minute == 0 && (time - Simulator.Times.Start).TotalDays > 1)
            {
                // Calculate DayAhead Data for Renewable ENergies and Load
                foreach (var plant in this.assets.OfType<Plant_PV>())
                {
                    plant.CalcSchedule(time.AddDays(1), TimeSpan.FromDays(1));
                }
                foreach (var plant in this.assets.OfType<Plant_Wind>())
                {
                    plant.CalcSchedule(time.AddDays(1), TimeSpan.FromDays(1));
                }
                using (var repo = new PredictionDataRepository())
                {
                    var pvData = this.assets.OfType<Plant_PV>().GetSumSchedule(time.AddDays(1), TimeSpan.FromDays(1));
                    var wiData = this.assets.OfType<Plant_Wind>().GetSumSchedule(time.AddDays(1), TimeSpan.FromDays(1));
                    var loadData = this.assets.OfType<Load_Grid>().GetSumSchedule(time.AddDays(1), TimeSpan.FromDays(1)).Select(el => new LoadData(el.TimeStamp, -el.Power));
                    var reData = pvData.Join(wiData, pv => pv.TimeStamp, wi => wi.TimeStamp, (pv, wi) => new REData(pv.TimeStamp, pv.Power, wi.Power ));
                    repo.REDataRepo.Add(reData);
                    repo.LoadDataRepo.Add(loadData);
                }
                var saldo = new List<(DateTime TimeStamp, float Saldo, float SaldoFromDiff)>();
                foreach (TSO tso in tsos)
                {
                    tso.CloseGate(time);
                    var saldo_ = tso.getSaldo();
                    saldo = saldo.Count() == 0 ? saldo_ : saldo
                        .Zip(saldo_,
                        (fullsaldo, tsosaldo)
                            => (tsosaldo.timeStamp, fullsaldo.Item2 + tsosaldo.saldo, fullsaldo.SaldoFromDiff + tsosaldo.saldoFromDiff)).ToList();
                }
                if (saldo.Count() > 0) // && !ControlReserveDemandRepository.IsBalancesTimeFrameCached(saldo.First().TimeStamp, saldo.Last().TimeStamp - saldo.First().TimeStamp))
                    ControlReserveDemandRepository.AddBalances(saldo.Select(item => (item.TimeStamp, item.SaldoFromDiff)).ToList());
            }
            this.ControlReserveActivations.ForEach(item => item.HandleActivation(time));
            this.ReBAP_Calculations.ForEach(item => item.Handle_reBAP_Calculation(time));
            var MinAssumedCallProbalility = assets.SelectMany(item => item.GetAssumedCallProbability()).GroupBy(item => item.Key, (group, elements) => (group, elements.Select(item => item.Value).Min())).ToList();
        }
    }
}
