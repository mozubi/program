﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets.Elements;

namespace winMoz.Simulation
{
    public class ScenarioData
    {
        public int Total_ID { get; private set; }
        public string ScenarioName { get; private set; }
        public int Markets_ID { get; private set; }
        public int Assets_ID { get; private set; }
        public DateTime SimStartTime { get; private set; }
        public DateTime SimEndTime { get; private set; }
        public MarketAreaEnum LocalMACode { get; private set; }
        public int AssetsYear { get; private set; }
        public int WeatherYear { get; private set; }
        public int StartDataYear { get; private set; }
        public List<int> AdditionalAssetsMaStR { get; private set; }
        public TYNDP2018_ScenarioCalculation.ScenarioEnum TYNDP2018Scenario { get; private set; }
        private ScenarioData() { }
        public ScenarioData(int total_ID, string scenarioName, int markets_ID, int assets_ID, DateTime simStartTime, DateTime simEndTime, MarketAreaEnum localMACode, int assetsYear, int weatherYear, int startDataYear, List<int> additionalAssetsMaStR, TYNDP2018_ScenarioCalculation.ScenarioEnum tyndp2018Scenario)
        {
            Total_ID = total_ID;
            ScenarioName = scenarioName;
            Markets_ID = markets_ID;
            Assets_ID = assets_ID;
            SimStartTime = simStartTime;
            SimEndTime = simEndTime;
            LocalMACode = localMACode;
            AssetsYear = assetsYear;
            WeatherYear = weatherYear;
            StartDataYear = startDataYear;
            AdditionalAssetsMaStR = additionalAssetsMaStR;
            TYNDP2018Scenario = tyndp2018Scenario;
        }
    }
}
