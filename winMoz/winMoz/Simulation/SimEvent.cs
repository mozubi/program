﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Helper;

namespace winMoz.Simulation
{
    public abstract class SimEvent : SimObject, ISimEvent
    {
        protected DateTime EventTime { get; set; }
        public (DateTime TimeStamp, TimeSpan Duration, TimeSpan Step) EventTimeframe { get; protected set; }
        private TimeSpanSave DeterminationDuration;
        private TimeSpanSave DeterminationStep;

        public SimEvent() { }
        public SimEvent(SimEventAttributes attributes, int scenarioID = 0) : base(scenarioID)
        {
            InitializeEventTimes(attributes);
        }

        public bool IsEventTime(DateTime time) => time == EventTime;

        public virtual void InitializeEventTimes(SimEventAttributes attributes)
        {
            //Aktivierung
            this.DeterminationDuration = attributes.DeterminationDuration;
            this.DeterminationStep = attributes.DeterminationStep;
            this.EventTime = attributes.StartEventTime != default ? attributes.StartEventTime : Simulator.Times.Start.Add(attributes.StartEventTimeDurationFromStart); // new DateTime(start.Year, start.Month, start.Day, 0, 0, 0).AddDays(2);
            var NewDeterminationDuration = attributes.StartDeterminationTime.Add(attributes.DeterminationDuration) - attributes.StartDeterminationTime;
            var NewDeterminationStep = attributes.StartDeterminationTime.Add(attributes.DeterminationStep) - attributes.StartDeterminationTime;
            this.EventTimeframe = (
                attributes.StartDeterminationTime != default ? attributes.StartDeterminationTime : Simulator.Times.Start.Add(attributes.StartDeterminationTimeDurationFromStart),
                 NewDeterminationDuration, NewDeterminationStep); // (determinationTime, new TimeSpan(24, 0, 0), new TimeSpan(0, 15, 0));
        }

        protected virtual void UpdateEventTiming()
        {
            this.EventTime += this.EventTimeframe.Duration;
            var NewDeterminationTimeStamp = this.EventTimeframe.TimeStamp.Add(this.EventTimeframe.Duration);
            var NewDeterminationDuration = NewDeterminationTimeStamp.Add(DeterminationDuration) - NewDeterminationTimeStamp;
            var NewDeterminationStep = NewDeterminationTimeStamp.Add(DeterminationStep) - NewDeterminationTimeStamp;
            this.EventTimeframe = (NewDeterminationTimeStamp, NewDeterminationDuration, NewDeterminationStep);
        }

    }
}
