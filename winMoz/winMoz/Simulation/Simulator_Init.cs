﻿using System;
using winMoz.Helper;
using winMoz.Data.Access.Repositories;
using System.Linq;
using winMoz.Assets.Schedules;
using winMoz.Assets.Plants;
using winMoz.Tools.Viz;
using winMoz.Data.Access.Repositories.Prediction;

namespace winMoz.Simulation
{
    /// <summary>
    /// Simulator-Klasse
    /// </summary>
    public class Simulator
    {
        Scenario Scen;
        ScenarioData ScenData;
        int Total_ID;

        DateTime NextSavePointSchedules;
        public bool IsInitialized { get; private set; } = false;

        /// <summary>
        /// Simulationszeiten
        /// </summary>
        public sealed class Times
        {
            /// <summary> Startzeit der Simulation</summary>
            public static DateTime Start { get; private set; } = new DateTime(2015, 1, 1, 0, 0, 0);
            /// <summary> Stopzeit der Simulation</summary>
            public static DateTime End { get; internal set; } = new DateTime(2015, 12, 31, 23, 45, 0);
            ///<summary>Aktueller Zeitpunkt der Simulation</summary>
            public static DateTime SimTime { get; private set; }

            internal static void SetStartTime(DateTime TimeStamp)
            {
                Start = TimeStamp;
                SimTime = Start;
            }

            public static void IncrementTime(TimeSpan Duration) => SimTime = SimTime.Add(Duration);

            public static TimeSpan SimDuration() => End - Start;
        }

        /// <summary>
        /// Simulationszeiten
        /// </summary>
        public sealed class SaveStates
        {
            public static bool AreBidsSaved { get; private set; }
            public static bool AreSchedulesSaved { get; internal set; }
            public static bool AreSumSchedulesSaved { get; internal set; }
            public static bool AreMarketResultsSaved { get; internal set; }
            public static bool AreExternalMarketResultsSaved{ get; internal set; }
            public static bool AreControlReserveDemandSaved { get; internal set; }

            public static void SaveBids() => AreBidsSaved = true;
            public static void SaveSchedules() => AreSchedulesSaved = true;
            public static void SaveSumSchedules() => AreSumSchedulesSaved = true;
            public static void SaveMarketResults() => AreMarketResultsSaved = true;
            public static void SaveExternalMarketResults() => AreExternalMarketResultsSaved = true;
            public static void SaveControlReserveDemand() => AreControlReserveDemandSaved = true;
        }

        /// <summary>
        /// Simulationszeiten
        /// </summary>
        public sealed class WeatherYear
        {
            public static int Year { get; private set; }

            public static TimeSpan Shift { get; private set; }

            public static void SetWeatherYear(int year, DateTime startTime)
            {
                Year = year;
                if (!(startTime.Month == 2 && startTime.Day == 29))
                    Shift = new DateTime(year, startTime.Month, startTime.Day) - startTime;
                else
                    Shift = new DateTime(year, startTime.Month, startTime.Day - 1) - startTime;
                var test = startTime.Add(Shift);
            }
        }

        /// <summary>
        /// Simulationszeiten
        /// </summary>
        public sealed class StartDataYear
        {
            public static int Year { get; private set; }

            public static TimeSpan Shift { get; private set; }

            public static void SetStartDataYear(int year, DateTime startTime)
            {
                Year = year;
                if (!(startTime.Month == 2 && startTime.Day == 29))
                    Shift = new DateTime(year, startTime.Month, startTime.Day) - startTime;
                else
                    Shift = new DateTime(year, startTime.Month, startTime.Day - 1) - startTime;
                var test = startTime.Add(Shift);
            }
        }

        public Simulator(int total_ID)
        {
            (DateTime start, TimeSpan duration) = new ScenarioData_Repository().GetStartAndDurationFromID(total_ID);
            InitializeTimes(start, duration);
            InitializeSim(total_ID);
        }

        public virtual void InitializeSim(int Total_ID)
        {
            Config cfg = new Config();
            TimeSpan DurationYear = Simulator.Times.Start.AddYears(1) - Simulator.Times.Start;
            winMoz.Information.Commodities.CacheCommodities(Simulator.Times.Start, DurationYear);
            // Load a scenario
            //scenario = new Scenario(Times.Start);
            this.IsInitialized = true;
            NextSavePointSchedules = Simulator.Times.Start.AddDays(1).AddMonths(1);
            using (var repo = new ScenarioData_Repository())
            {
                //ScenData = repo.GetFromID(Total_ID);
                Scen = Scenario.Load(Total_ID); // new Scenario(ScenData);
            }
            Log.Info("Simulation initialized");
        }

        private void InitializeTimes(DateTime simStartTime, TimeSpan simDuration)
        {
            Times.SetStartTime(simStartTime);
            Times.End = simStartTime + simDuration;
        }

        public void Run()
        {

            Log.Info("Simulation started");
            while (Times.End.CompareTo(Times.SimTime) >= 0)
            {
                if (Times.SimTime.Day == 1 && Times.SimTime.IsFullHour(0))
                    Log.Info($"===============  {Times.SimTime.Date}  ===============");

                // Simulate a Scenario
                StepScenario();
                PlotAndOutputSchedules();
                Times.IncrementTime(TimeSpan.FromMinutes(15));
                SaveDuringSim();
            }
            SaveAtEnd();
            PlotSumSchedule(Times.Start, Times.End - Times.Start);
            PlotAndOutputSchedules();
            PlotAndOutputMarketResults();
            Log.Info("Complete...");
            Log.Info("Shut.me.down");
            //Console.Clear();
        }

        private void PlotAndOutputSchedules()
        {
            PlotAndOutput.Plot_Schedules(this.Scen.assets);
            PlotAndOutput.OutputConsole_Schedules(this.Scen.assets);
            PlotAndOutput.OutputCSV_Schedules(this.Scen.assets);
        }

        private void PlotAndOutputMarketResults()
        {
            PlotAndOutput.Plot_MarketResults();
            PlotAndOutput.OutputConsole_MarketResults();
            PlotAndOutput.OutputCSV_MarketResults();
        }

        protected virtual void SaveAtEnd()
        {
            if (Simulator.SaveStates.AreMarketResultsSaved)
            {
                Log.Info("Save MarketResults to DB");
                MarketResultRepository.CacheToDB();
            }
            if (Simulator.SaveStates.AreSumSchedulesSaved)
            {
                Log.Info("Save SumSchedules to DB");
                var LastSavePointSchedules = NextSavePointSchedules.AddMonths(-1).AddDays(-1);
                SaveSumSchedules(LastSavePointSchedules, Simulator.Times.End - LastSavePointSchedules, cacheToDB: true);
            }
            if (Simulator.SaveStates.AreSchedulesSaved)
            {
                Log.Info("Save Schedules to DB");
                SaveSchedules(Simulator.Times.End, cacheToDB: true);
            }
            if (Simulator.SaveStates.AreControlReserveDemandSaved)
            {
                Log.Info("Save ControlReserve-Demand to DB");
                ControlReserveDemandRepository.Save();
            }
            if (Simulator.SaveStates.AreExternalMarketResultsSaved)
            {
                Log.Info("Save ExternalMarketResults to DB");
                //ExternalMarketResultRepository repo = new ExternalMarketResultRepository();
                ExternalMarketResultRepository.CacheToDB();
            }

            if (Simulator.SaveStates.AreBidsSaved)
            {
                Log.Info("Save ExternalMarketResults to DB");
                //ExternalMarketResultRepository repo = new ExternalMarketResultRepository();
                Scen.Markets.ForEach(market => market.SaveBidsToDb());
            }

        }

        protected virtual void SaveDuringSim()
        {
            if (Simulator.Times.SimTime.Ticks >= NextSavePointSchedules.Ticks)
            {
                DateTime StartSave = NextSavePointSchedules.AddMonths(-1).AddDays(-1);
                DateTime EndSave = NextSavePointSchedules.AddDays(-1);
                TimeSpan DurationSave = EndSave - StartSave;
                SaveSumSchedules(StartSave, DurationSave, Simulator.SaveStates.AreSumSchedulesSaved);
                SaveSchedules(EndSave, Simulator.SaveStates.AreSchedulesSaved);
                NextSavePointSchedules = NextSavePointSchedules.AddMonths(1);
            }
        }

        public virtual void StepScenario()
        {
            Scen.Step(Simulator.Times.SimTime);
        }
        
        public virtual void PlotSumSchedule(DateTime start, TimeSpan duration)
        {
        }
        private void SaveSumSchedules(DateTime start, TimeSpan duration, bool cacheToDB = false)
        {
            if (cacheToDB)
            {
                Log.Info("Save SumSchedules to DB");
                var DummySchedule = new Schedule();
                DummySchedule.Extend(start, duration);
                var Duration = duration;
                var EndDuration = start.Add(Duration);
                var AssetsPV = this.Scen.assets.Where(item => item.GetType().Name == "Plant_PV").ToList(); ;
                var SumSchedulePV = AssetsPV.Count() > 0 ? AssetsPV.GetSumSchedule(start, Duration) : DummySchedule;
                SumSchedulePV.ToList().ForEach(item => item.SetType("PV"));
                var PowerPV = SumSchedulePV.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsWind = this.Scen.assets.Where(item => item.GetType().Name == "Plant_Wind" || item.GetType().Name == "Plant_Wind_Aggregate");
                var SumScheduleWind = AssetsWind.Count() > 0 ? AssetsWind.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleWind.ToList().ForEach(item => item.SetType("Wind"));
                var PowerWind = SumScheduleWind.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsOil = this.Scen.assets.Where(item => item is Block_CHP || item is Block_Kond && (item as Block).Commodity == winMoz.Information.Commodities.Commodity.Oil);
                var SumScheduleOil = AssetsOil.Count() > 0 ? AssetsOil.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleOil.ToList().ForEach(item => item.SetType("Oil"));
                var PowerOil = SumScheduleOil.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsGas = this.Scen.assets.Where(item => item is Block_CHP || item is Block_Kond && (item as Block).Commodity == winMoz.Information.Commodities.Commodity.Natural_Gas);
                var SumScheduleGas = AssetsGas.Count() > 0 ? AssetsGas.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleGas.ToList().ForEach(item => item.SetType("Gas"));
                var PowerGas = SumScheduleGas.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsNuclear = this.Scen.assets.Where(item => item is Block_CHP || item is Block_Kond && (item as Block).Commodity == winMoz.Information.Commodities.Commodity.Uranium);
                var SumScheduleNuclear = AssetsNuclear.Count() > 0 ? AssetsNuclear.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleNuclear.ToList().ForEach(item => item.SetType("Nuclear"));
                var PowerNuclear = SumScheduleNuclear.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsHardCoal = this.Scen.assets.Where(item => item is Block_CHP || item is Block_Kond && (item as Block).Commodity == winMoz.Information.Commodities.Commodity.Hard_Coal);
                var SumScheduleHardCoal = AssetsHardCoal.Count() > 0 ? AssetsHardCoal.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleHardCoal.ToList().ForEach(item => item.SetType("Hard Coal"));
                var PowerHardCoal = SumScheduleHardCoal.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsLignite = this.Scen.assets.Where(item => item is Block_CHP || item is Block_Kond && (item as Block).Commodity == winMoz.Information.Commodities.Commodity.Lignite);
                var SumScheduleLignite = AssetsLignite.Count() > 0 ? AssetsLignite.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleLignite.ToList().ForEach(item => item.SetType("Lignite"));
                var PowerLignite = SumScheduleLignite.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsThermal = this.Scen.assets.Where(item => item is Block_CHP || item is Block_Kond);
                var SumScheduleThermal = AssetsThermal.Count() > 0 ? AssetsThermal.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleThermal.ToList().ForEach(item => item.SetType("Thermal_Total"));
                var PowerThermal = SumScheduleThermal.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsStorage = this.Scen.assets.Where(item => item.GetType().Name == "Plant_Storage");
                var SumScheduleStorage = AssetsStorage.Count() > 0 ? AssetsStorage.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleStorage.ToList().ForEach(item => item.SetType("Storage"));
                var PowerStorage = SumScheduleStorage.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsGridLoad = this.Scen.assets.Where(item => item.GetType().Name == "Load_Grid");
                var SumScheduleGridLoad = AssetsGridLoad.Count() > 0 ? AssetsGridLoad.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleGridLoad.ToList().ForEach(item => item.SetType("Grid Load"));
                var PowerLoad = SumScheduleGridLoad.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsBiomass = this.Scen.assets.Where(item => item.GetType().Name == "Plant_Biomass");
                var SumScheduleBiomass = AssetsBiomass.Count() > 0 ? AssetsBiomass.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleBiomass.ToList().ForEach(item => item.SetType("Biomass"));
                var PowerBiomass = SumScheduleBiomass.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();
                var AssetsHydro = this.Scen.assets.Where(item => item.GetType().Name == "Plant_Hydro");
                var SumScheduleHydro = AssetsBiomass.Count() > 0 ? AssetsBiomass.GetSumSchedule(start, Duration) : DummySchedule;
                SumScheduleBiomass.ToList().ForEach(item => item.SetType("Hydro"));
                var PowerHydro = SumScheduleBiomass.Select(item => (item.TimeStamp, item.PowerProcured)).ToList();

                var ElementsCountPerTimeStamp = SumSchedulePV.GroupBy(item => item.TimeStamp, (group, elements) => (group, elements.Count())).ToList();

                var schedulesPV = SumScheduleRepository.GetSchedules(start, Duration).Where(item => item.AssetType.ToString() == "PV").ToList();
                var ElementsCountPerTimeStampRepo = schedulesPV.GroupBy(item => item.TimeStamp, (group, elements) => (group, elements.Count())).ToList();

                SumScheduleRepository.Add(SumSchedulePV.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleWind.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleOil.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleGas.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleNuclear.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleHardCoal.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleLignite.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleStorage.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleGridLoad.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.Add(SumScheduleBiomass.Select(item => new SumScheduleItem(item)).ToList());
                SumScheduleRepository.CacheToDB();
            }
        }
        private void SaveSchedules(DateTime timeUntil, bool cacheToDB = false)
        {
            ScheduleRepository repo = new ScheduleRepository();
            if (cacheToDB)
            {
                Log.Info("Save Schedules to DB");
                repo.AddScheduleItemsOfAssetsUntil(Scen.assets, timeUntil);
                repo.CacheToDB();
            }
            Scen.assets.ForEach(asset =>
            {
                asset.CutScheduleUntil(timeUntil);
                asset.CutControlReserveScheduleUntil(timeUntil);
            });
            Scen.dsos.ForEach(dso => dso.TimeSeries.Assets.ForEach(asset =>
            {
                asset.CutScheduleUntil(timeUntil);
                asset.CutControlReserveScheduleUntil(timeUntil);
            }));
        }
    }
}
