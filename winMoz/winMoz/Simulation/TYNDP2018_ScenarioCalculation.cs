﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Repositories;

namespace winMoz.Simulation
{
    public class TYNDP2018_ScenarioCalculation
    {
        private ScenarioEnum Scenario;

        private Dictionary<int, TYNDP2018_Scenario_Repository> Repos = new Dictionary<int, TYNDP2018_Scenario_Repository>();

        public enum ScenarioEnum
        {
            DEFAULT, BEST, DG, EUCO, ST, GCA
        }

        public TYNDP2018_ScenarioCalculation(ScenarioEnum scenario)
        {
            Scenario = scenario;
            InitialiseRepos();
        }

        public void InitialiseRepos()
        {
            switch (Scenario)
            {
                case ScenarioEnum.BEST:
                    {
                        Repos.Add(2025, new Generation2025BEST_Repository());
                        break;
                    }
                case ScenarioEnum.DG:
                    {
                        Repos.Add(2025, new Generation2025BEST_Repository());
                        Repos.Add(2030, new Generation2030DG_Repository());
                        Repos.Add(2040, new Generation2040DG_Repository());
                        break;
                    }
                case ScenarioEnum.EUCO:
                    {
                        Repos.Add(2025, new Generation2025BEST_Repository());
                        Repos.Add(2030, new Generation2030EUCO_Repository());
                        break;
                    }
                case ScenarioEnum.ST:
                    {
                        Repos.Add(2025, new Generation2025BEST_Repository());
                        Repos.Add(2030, new Generation2030ST_Repository());
                        Repos.Add(2040, new Generation2040ST_Repository());
                        break;
                    }
                case ScenarioEnum.GCA:
                    {
                        Repos.Add(2025, new Generation2025BEST_Repository());
                        Repos.Add(2030, new Generation2030ST_Repository());
                        Repos.Add(2040, new Generation2040GCA_Repository());
                        break;
                    }
            }
        }

        public float GetWindOnshore(int year, float value2020)
        {
            var WindScenarioValues = Repos.Select(item => (item.Key, item.Value.GetForCountry("DE").Wind_Onshore)).ToList();
            WindScenarioValues = WindScenarioValues.Prepend((2020, value2020)).ToList();
            return Interpolate(year, WindScenarioValues);
        }

        public float GetHydroRunOfRiver(int year, float value2020)
        {
            var HydroScenarioValues = Repos.Select(item => (item.Key, item.Value.GetForCountry("DE").Hydro_run)).ToList();
            HydroScenarioValues = HydroScenarioValues.Prepend((2020, value2020)).ToList();
            return Interpolate(year, HydroScenarioValues);
        }

 public float GetPV(int year, float value2020)
        {
            var PVScenarioValues = Repos.Select(item => (item.Key, item.Value.GetForCountry("DE").Solar_PV)).ToList();
            PVScenarioValues = PVScenarioValues.Prepend((2020, value2020)).ToList();
            return Interpolate(year, PVScenarioValues);
        }

        public float GetBiomass(int year, float value2020)
        {
            var BiomassScenarioValues = Repos.Select(item => (item.Key, item.Value.GetForCountry("DE").Biofuels)).ToList();
            BiomassScenarioValues = BiomassScenarioValues.Prepend((2020, value2020)).ToList();
            return Interpolate(year, BiomassScenarioValues);
        }

        public float GetHydroPumpStorage(int year, float value2020)
        {
            var HydroPumpedStorageScenarioValues = Repos.Select(item => (item.Key, item.Value.GetForCountry("DE").Hydro_pump)).ToList();
            HydroPumpedStorageScenarioValues = HydroPumpedStorageScenarioValues.Prepend((2020, value2020)).ToList();
            return Interpolate(year, HydroPumpedStorageScenarioValues);
        }
        
        private float Interpolate(int year, List<(int Year, float Value)> values)
        {
            var valuesOrdered = values.OrderBy(item => item.Year).ToList().TakeWhile(item => item.Year < year + 5).ToList();
            int count = valuesOrdered.Count();
            return Interpolate(valuesOrdered[count - 2].Year, valuesOrdered[count - 1].Year, valuesOrdered[count - 2].Value, valuesOrdered[count - 1].Value, year);
        }

        private float Interpolate(float x1, float x2, float y1, float y2, float x)
            => y1 + (y2 - y1) / (x2 - x1) * (x - x1);

    }
}
