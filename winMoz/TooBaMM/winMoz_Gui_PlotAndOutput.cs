﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;
using System.Diagnostics;
using winMoz.Simulation;
using winMoz.Data.Access.DTO;
using winMoz.Markets.Elements;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.Repositories;

namespace winMoz_Gui
{
    public partial class winMoz_Gui_PlotAndOutput : Form
    {
        winMoz_Gui_PlotAndOutput_Elements gui;

        List<PlotAndOutput_Variables_Dto> PlotList = new List<PlotAndOutput_Variables_Dto>();
        List<PlotAndOutput_Variables_Dto> OutputConsoleList = new List<PlotAndOutput_Variables_Dto>();
        List<PlotAndOutput_Variables_Dto> OutputCSVList = new List<PlotAndOutput_Variables_Dto>();

        List<PlotAndOutput_Variables_Dto> PlotListSaved = new List<PlotAndOutput_Variables_Dto>();
        List<PlotAndOutput_Variables_Dto> OutputConsoleListSaved = new List<PlotAndOutput_Variables_Dto>();
        List<PlotAndOutput_Variables_Dto> OutputCSVListSaved = new List<PlotAndOutput_Variables_Dto>();

        public winMoz_Gui_PlotAndOutput()
        {
            InitializeComponent();
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
            gui = new winMoz_Gui_PlotAndOutput_Elements();
            this.Width = 1050;
            this.Height = 550;
        }

        private void AddPlot_Click(object sender, EventArgs e)
        {
            gui.Text = "Choose Plot";
            gui.ShowDialog();
            if (gui.ChoosedElement != "" && gui.IsSelected)
                CreatePlotAndOutput("Plot");
        }

        private void AddOutputConsole_Click(object sender, EventArgs e)
        {
            gui.Text = "Choose Output Console";
            gui.ShowDialog();
            if (gui.ChoosedElement != "" && gui.IsSelected)
                CreatePlotAndOutput("OutputConsole");
        }

        private void AddOutputCSV_Click(object sender, EventArgs e)
        {
            gui.Text = "Choose Output CSV";
            gui.ShowDialog();
            if (gui.ChoosedElement != "" && gui.IsSelected)
                CreatePlotAndOutput("OutputCSV");

        }

        private void CreatePlotAndOutput(string Type)
        {
            switch (gui.ChoosedElement)
            {
                case "Bids":
                    {
                        var Times = GetTimes();
                        var Element = GetBidElement();
                        var dto = new PlotAndOutput_Variables_Dto(Element, Times);
                        if (Type == "Plot")
                        {
                            PlotList.Add(dto);
                            AddBidToListBox(Plot_ListBox, Times, Element);
                        }
                        else if (Type == "OutputConsole")
                        {
                            OutputConsoleList.Add(dto);
                            AddBidToListBox(OutputConsole_ListBox, Times, Element);
                        }
                        else if (Type == "OutputCSV")
                        {
                            OutputCSVList.Add(dto);
                            AddBidToListBox(OutputCSV_ListBox, Times, Element);
                        }
                        break;
                    }
                case "MeritOrders":
                    {
                        var Times = GetTimes();
                        var Element = GetMeritOrderElement();
                        var dto = new PlotAndOutput_Variables_Dto(Element, Times);
                        if (Type == "Plot")
                        {
                            PlotList.Add(dto);
                            AddMeritOrderToListBox(Plot_ListBox, Times, Element);
                        }
                        else if (Type == "OutputConsole")
                        {
                            OutputConsoleList.Add(dto);
                            AddMeritOrderToListBox(OutputConsole_ListBox, Times, Element);
                        }
                        else if (Type == "OutputCSV")
                        {
                            OutputCSVList.Add(dto);
                            AddMeritOrderToListBox(OutputCSV_ListBox, Times, Element);
                        }
                        break;
                    }
                case "CoupledMeritOrders":
                    {
                        var Times = GetTimes();
                        var Element = GetCoupledMeritOrderElement();
                        var dto = new PlotAndOutput_Variables_Dto(Element, Times);
                        if (Type == "Plot")
                        {
                            PlotList.Add(dto);
                            AddCoupledMeritOrderToListBox(Plot_ListBox, Times, Element);
                        }
                        else if (Type == "OutputConsole")
                        {
                            OutputConsoleList.Add(dto);
                            AddCoupledMeritOrderToListBox(OutputConsole_ListBox, Times, Element);
                        }
                        else if (Type == "OutputCSV")
                        {
                            OutputCSVList.Add(dto);
                            AddCoupledMeritOrderToListBox(OutputCSV_ListBox, Times, Element);
                        }
                        break;
                    }
                case "Schedules":
                    {
                        var Times = GetTimes();
                        var Element = GetScheduleElement();
                        var dto = new PlotAndOutput_Variables_Dto(Element, Times);
                        if (Type == "Plot")
                        {
                            PlotList.Add(dto);
                            AddScheduleToListBox(Plot_ListBox, Times, Element);
                        }
                        else if (Type == "OutputConsole")
                        {
                            OutputConsoleList.Add(dto);
                            AddScheduleToListBox(OutputConsole_ListBox, Times, Element);
                        }
                        else if (Type == "OutputCSV")
                        {
                            OutputCSVList.Add(dto);
                            AddScheduleToListBox(OutputCSV_ListBox, Times, Element);
                        }
                        break;
                    }
                case "MarketResults":
                    {
                        var Times = GetTimes();
                        var Element = GetMarketResultElement();
                        var dto = new PlotAndOutput_Variables_Dto(Element, Times);
                        if (Type == "Plot")
                        {
                            PlotList.Add(dto);
                            AddMarketResultToListBox(Plot_ListBox, Times, Element);
                        }
                        else if (Type == "OutputConsole")
                        {
                            OutputConsoleList.Add(dto);
                            AddMarketResultToListBox(OutputConsole_ListBox, Times, Element);
                        }
                        else if (Type == "OutputCSV")
                        {
                            OutputCSVList.Add(dto);
                            AddMarketResultToListBox(OutputCSV_ListBox, Times, Element);
                        }
                        break;
                    }
            }

        }
        private PlotAndOutput_Times_Durations GetTimes() => new PlotAndOutput_Times_Durations(gui.StartDate, gui.EndDate);

        private PlotAndOutput_Element_Bid GetBidElement()
        {
            return new PlotAndOutput_Element_Bid(
                market: gui.FormElements["Market"].Element.Text,
                withStep: ((CheckBox)gui.FormElements["WithStep"].Element).Checked,
                withBlock: ((CheckBox)gui.FormElements["WithBlock"].Element).Checked,
                differByPlantTypeAndSubType: ((RadioButton)gui.FormElements["DifferByPlantTypeAndSubType"].Element).Checked,
                onlyOnePlantType: ((RadioButton)gui.FormElements["OnlyOnePlantType"].Element).Checked,
                onlyOnePlant: ((RadioButton)gui.FormElements["OnlyOnePlant"].Element).Checked,
                onlyOneControlReserveType: ((RadioButton)gui.FormElements["OnlyOneControlReserveType"].Element).Checked,
                onlyCapacityBids: ((RadioButton)gui.FormElements["OnlyCapacityBids"].Element).Checked,
                plant_Type: gui.FormElements["Plant_Type"].Element.Text,
                subType: gui.FormElements["SubType"].Element.Text,
                plant_Id: (int)Convert.ToInt64(((NumericUpDown)gui.FormElements["Plant_Id"].Element).Value),
                controlReserveType: gui.FormElements["ControlReserveType"].Element.Text);
        }

        private PlotAndOutput_Element_MeritOrder GetMeritOrderElement()
        {
            return new PlotAndOutput_Element_MeritOrder(
                simEvent: gui.FormElements["SimEvent"].Element.Text,
                onlyOneControlReserveType: ((RadioButton)gui.FormElements["OnlyOneControlReserveType"].Element).Checked,
                controlReserveType: gui.FormElements["ControlReserveType"].Element.Text);
                
        }

        private PlotAndOutput_Element_CoupledMeritOrder GetCoupledMeritOrderElement()
        {
            return new PlotAndOutput_Element_CoupledMeritOrder(
                marketAreaSpecified: !((CheckBox)gui.FormElements["MarketAreaSpecified"].Element).Checked,
                marketArea: MarketAreaEnum.FromString(gui.FormElements["MarketArea"].Element.Text));
        }

        private PlotAndOutput_Element_Schedule GetScheduleElement()
        {
            return new PlotAndOutput_Element_Schedule(
                differByPlantTypeAndSubType: ((RadioButton)gui.FormElements["DifferByPlantTypeAndSubType"].Element).Checked,
                onlyOnePlantType: ((RadioButton)gui.FormElements["OnlyOnePlantType"].Element).Checked,
                onlyOnePlant: ((RadioButton)gui.FormElements["OnlyOnePlant"].Element).Checked,
                withPrices: ((CheckBox)gui.FormElements["WithPrices"].Element).Checked,
                isOutputSimTimeSet: ((RadioButton)gui.FormElements["IsOutputSimTimeSet"].Element).Checked,
                plant_Type: gui.FormElements["Plant_Type"].Element.Text,
                subType: gui.FormElements["SubType"].Element.Text,
                plant_Id: (int)((NumericUpDown)gui.FormElements["Plant_Id"].Element).Value,
                outputSimTime: ((RadioButton)gui.FormElements["IsOutputSimTimeSet"].Element).Checked ? Convert.ToDateTime(gui.FormElements["OutputSimTime"].Element.Text) : default);
        }

        private PlotAndOutput_Element_MarketResults GetMarketResultElement()
        {
            return new PlotAndOutput_Element_MarketResults(
                isMarketTypeSpecified: ((CheckBox)gui.FormElements["IsMarketTypeSpecified"].Element).Checked,
                differByMarketType: ((CheckBox)gui.FormElements["DifferByMarketType"].Element).Checked,
                volumeVersusPrice: ((RadioButton)gui.FormElements["VolumeVersusPrice"].Element).Checked,
                timeVersusVolume: ((RadioButton)gui.FormElements["TimeVersusVolume"].Element).Checked,
                timeVersusPrice: ((RadioButton)gui.FormElements["TimeVersusPrice"].Element).Checked,
                timeVersusVolumeAndPrice: ((RadioButton)gui.FormElements["TimeVersusVolumeAndPrice"].Element).Checked,
                marketResultType: gui.FormElements["MarketResultType"].Element.Text
                );
        }

        private void AddVariableToListBox(ListBox listBox, PlotAndOutput_Variables_Dto dto)
        {
            switch (dto.Element.GetType().Name)
            {
                case "PlotAndOutput_Element_Bid":
                    {
                        AddBidToListBox(listBox, (PlotAndOutput_Times_Durations)dto.Times, (PlotAndOutput_Element_Bid)dto.Element);
                        break;
                    }
                case "PlotAndOutput_Element_MeritOrder":
                    {
                        AddMeritOrderToListBox(listBox, (PlotAndOutput_Times_Durations)dto.Times, (PlotAndOutput_Element_MeritOrder)dto.Element);
                        break;
                    }
                case "PlotAndOutput_Element_CoupledMeritOrder":
                    {
                        AddCoupledMeritOrderToListBox(listBox, (PlotAndOutput_Times_Durations)dto.Times, (PlotAndOutput_Element_CoupledMeritOrder)dto.Element);
                        break;
                    }
                case "PlotAndOutput_Element_Schedule":
                    {
                        AddScheduleToListBox(listBox, (PlotAndOutput_Times_Durations)dto.Times, (PlotAndOutput_Element_Schedule)dto.Element);
                        break;
                    }
                case "PlotAndOutput_Element_MarketResults":
                    {
                        AddMarketResultToListBox(listBox, (PlotAndOutput_Times_Durations)dto.Times, (PlotAndOutput_Element_MarketResults)dto.Element);
                        break;
                    }
            }
        }

        private void AddBidToListBox(ListBox listBox, PlotAndOutput_Times_Durations times, PlotAndOutput_Element_Bid bid)
        {
            string text = "Bid: From "
                + times.Start + " To " + times.End
                + " Market: " + bid.Market
                + " WithStep: " + bid.WithStep
                + " WithBlock: " + bid.WithBlock
                + " DifferByPlantTypeAndSubType: " + bid.DifferByPlantTypeAndSubType
                + " OnlyOnePlantType: " + bid.OnlyOnePlantType
                + " OnlyOnePlant: " + bid.OnlyOnePlant
                + " OnlyOneControlReserveType: " + bid.OnlyOneControlReserveType
                + " OnlyCapacityBids: " + bid.OnlyCapacityBids
                + ((bid.OnlyOnePlantType || bid.OnlyOnePlant) ? (" Plant_Type: " + bid.Plant_Type) : "")
                + (bid.OnlyOnePlantType ? (" SubType: " + bid.SubType) : "")
                + (bid.OnlyOnePlant ? (" Plant_Id: " + bid.Plant_Id) : "")
                + (bid.OnlyOneControlReserveType ? (" ControlReserveType: " + bid.ControlReserveType) : "");
            listBox.Items.Add(text);
        }

        private void AddMeritOrderToListBox(ListBox listBox, PlotAndOutput_Times_Durations times, PlotAndOutput_Element_MeritOrder meritOrder)
        {
            string text = "MeritOrder: From "
                + times.Start + " To " + times.End
                + " SimEvent: " + meritOrder.SimEvent
                + " OnlyOneControlReserveType: " + meritOrder.OnlyOneControlReserveType
                + (meritOrder.OnlyOneControlReserveType ? (" ControlReserveType: " + meritOrder.ControlReserveType) : "");
            listBox.Items.Add(text);
        }

        private void AddCoupledMeritOrderToListBox(ListBox listBox, PlotAndOutput_Times_Durations times, PlotAndOutput_Element_CoupledMeritOrder coupledMeritOrder)
        {
            string text = "CoupledMeritOrder: From "
                + times.Start + " To " + times.End
                + " MarketAreaSpecified: " + coupledMeritOrder.MarketAreaSpecified
                + (coupledMeritOrder.MarketAreaSpecified ? (" MarketArea: " + coupledMeritOrder.MarketArea.ToString()) : "");
            listBox.Items.Add(text);
        }

        private void AddScheduleToListBox(ListBox listBox, PlotAndOutput_Times_Durations times, PlotAndOutput_Element_Schedule schedule)
        {
            string text = "Schedule: From "
                + times.Start + " To " + times.End
                + " DifferByPlantTypeAndSubType: " + schedule.DifferByPlantTypeAndSubType
                + " OnlyOnePlantType: " + schedule.OnlyOnePlantType
                + " OnlyOnePlant: " + schedule.OnlyOnePlant
                + ((schedule.OnlyOnePlantType || schedule.OnlyOnePlant) ? (" Plant_Type: " + schedule.Plant_Type) : "")
                + (schedule.OnlyOnePlantType ? (" SubType: " + schedule.SubType) : "")
                + (schedule.OnlyOnePlant ? (" Plant_Id: " + schedule.Plant_Id) : "");
            listBox.Items.Add(text);
        }

        private void AddMarketResultToListBox(ListBox listBox, PlotAndOutput_Times_Durations times, PlotAndOutput_Element_MarketResults marketResult)
        {
            string text = "MarketResult: From "
                + times.Start + " To " + times.End
                + " IsMarketTypeSpecified: " + marketResult.IsMarketTypeSpecified
                + " DifferByMarketType: " + marketResult.DifferByMarketType
                + " VolumeVersusPrice: " + marketResult.VolumeVersusPrice
                + " TimeVersusVolume: " + marketResult.TimeVersusVolume
                + " TimeVersusPrice: " + marketResult.TimeVersusPrice
                + " TimeVersusVolumeAndPrice: " + marketResult.TimeVersusVolumeAndPrice
                + " MarketResultType: " + marketResult.MarketResultType;
            listBox.Items.Add(text);
        }

        private void Save_Click(object sender, EventArgs e)
        {
            DbHelper.ReCreateDb<PlotAndOutput_Variables_Console_Context>();
            DbHelper.ReCreateDb<PlotAndOutput_Variables_CSV_Context>();
            DbHelper.ReCreateDb<PlotAndOutput_Variables_Plot_Context>();
            PlotAndOutput_Variables_Plot_Repository repoPlot = new PlotAndOutput_Variables_Plot_Repository();
            PlotAndOutput_Variables_Console_Repository repoConsole = new PlotAndOutput_Variables_Console_Repository();
            PlotAndOutput_Variables_CSV_Repository repoCSV = new PlotAndOutput_Variables_CSV_Repository();
            foreach (var plot in PlotList) repoPlot.Add(plot);
            foreach (var outputConsole in OutputConsoleList) repoConsole.Add(outputConsole);
            foreach (var outputCSV in OutputCSVList) repoCSV.Add(outputCSV);
            PlotListSaved = new List<PlotAndOutput_Variables_Dto>(PlotList);
            OutputConsoleListSaved = new List<PlotAndOutput_Variables_Dto>(OutputConsoleList);
            OutputCSVListSaved = new List<PlotAndOutput_Variables_Dto>(OutputCSVList);
            this.Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PlotList = new List<PlotAndOutput_Variables_Dto>(PlotListSaved);
            OutputConsoleList = new List<PlotAndOutput_Variables_Dto>(OutputConsoleListSaved);
            OutputCSVList = new List<PlotAndOutput_Variables_Dto>(OutputCSVListSaved);
            this.Plot_ListBox.Items.Clear();
            this.OutputConsole_ListBox.Items.Clear();
            this.OutputCSV_ListBox.Items.Clear();
            foreach (var plot in PlotList) AddVariableToListBox(this.Plot_ListBox, plot);
            foreach (var outputConsole in OutputConsoleList) AddVariableToListBox(this.Plot_ListBox, outputConsole);
            foreach (var outputCSV in OutputCSVList) AddVariableToListBox(this.Plot_ListBox, outputCSV);
            this.Close();
        }

        private void DeletePlot_Click(object sender, EventArgs e)
        {
            this.PlotList.RemoveAt(this.Plot_ListBox.SelectedIndex);
            this.Plot_ListBox.Items.RemoveAt(this.Plot_ListBox.SelectedIndex);
        }

        private void DeletOutputConsole_Click(object sender, EventArgs e)
        {
            this.OutputConsoleList.RemoveAt(this.OutputConsole_ListBox.SelectedIndex);
            this.OutputConsole_ListBox.Items.RemoveAt(this.OutputConsole_ListBox.SelectedIndex);
        }

        private void DeletOutputCSV_Click(object sender, EventArgs e)
        {
            this.OutputCSVList.RemoveAt(this.OutputCSV_ListBox.SelectedIndex);
            this.OutputCSV_ListBox.Items.RemoveAt(this.OutputCSV_ListBox.SelectedIndex);
        }
    }
}
