﻿namespace winMoz_Gui
{
    partial class winMoz_Gui_NewMarketObject
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.MarketObject_ComboBox = new System.Windows.Forms.ComboBox();
            this.MarketObject = new System.Windows.Forms.Label();
            this.Attributes_Box = new System.Windows.Forms.GroupBox();
            this.Default = new System.Windows.Forms.Button();
            this.Attributes_Panel = new System.Windows.Forms.Panel();
            this.Attributes_Box.SuspendLayout();
            this.SuspendLayout();
            // 
            // Add
            // 
            this.Add.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Add.Font = new System.Drawing.Font("Arial", 12F);
            this.Add.Location = new System.Drawing.Point(33, 14);
            this.Add.Margin = new System.Windows.Forms.Padding(6);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(595, 52);
            this.Add.TabIndex = 13;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = false;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Delete
            // 
            this.Delete.BackColor = System.Drawing.Color.LightCoral;
            this.Delete.Font = new System.Drawing.Font("Arial", 12F);
            this.Delete.Location = new System.Drawing.Point(640, 14);
            this.Delete.Margin = new System.Windows.Forms.Padding(6);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(542, 52);
            this.Delete.TabIndex = 14;
            this.Delete.Text = "Cancel";
            this.Delete.UseVisualStyleBackColor = false;
            // 
            // MarketObject_ComboBox
            // 
            this.MarketObject_ComboBox.FormattingEnabled = true;
            this.MarketObject_ComboBox.Items.AddRange(new object[] {
            "FuturesMarket",
            "DayAheadMarket",
            "IntradayMarket",
            "ControlReserve_Capacity",
            "ControlReserve_Energy",
            "ControlReserve_Energy_Scenario1",
            "ControlReserve_Activation",
            "ControlReserveTenderVolumeHandler",
            "Scenario1_ControlReserveTenderVolumeHandler",
            "ControlReserveActivationVolumeHandler",
            "IGCC_Calculation",
            "ReBAP_Calculation",
            "DeviationFromRZSaldo_Handler"});
            this.MarketObject_ComboBox.Location = new System.Drawing.Point(420, 93);
            this.MarketObject_ComboBox.Name = "MarketObject_ComboBox";
            this.MarketObject_ComboBox.Size = new System.Drawing.Size(551, 33);
            this.MarketObject_ComboBox.TabIndex = 15;
            this.MarketObject_ComboBox.SelectedIndexChanged += new System.EventHandler(this.MarketObject_ComboBox_SelectedIndexChanged);
            // 
            // MarketObject
            // 
            this.MarketObject.AutoSize = true;
            this.MarketObject.Location = new System.Drawing.Point(28, 101);
            this.MarketObject.Name = "MarketObject";
            this.MarketObject.Size = new System.Drawing.Size(146, 25);
            this.MarketObject.TabIndex = 16;
            this.MarketObject.Text = "Market Object";
            // 
            // Attributes_Box
            // 
            this.Attributes_Box.Controls.Add(this.Attributes_Panel);
            this.Attributes_Box.Location = new System.Drawing.Point(33, 154);
            this.Attributes_Box.Name = "Attributes_Box";
            this.Attributes_Box.Size = new System.Drawing.Size(1167, 549);
            this.Attributes_Box.TabIndex = 17;
            this.Attributes_Box.TabStop = false;
            this.Attributes_Box.Text = "Attributes";
            // 
            // Default
            // 
            this.Default.BackColor = System.Drawing.Color.Silver;
            this.Default.Font = new System.Drawing.Font("Arial", 12F);
            this.Default.Location = new System.Drawing.Point(980, 86);
            this.Default.Margin = new System.Windows.Forms.Padding(6);
            this.Default.Name = "Default";
            this.Default.Size = new System.Drawing.Size(202, 48);
            this.Default.TabIndex = 18;
            this.Default.Text = "Default";
            this.Default.UseVisualStyleBackColor = false;
            this.Default.Click += new System.EventHandler(this.Default_Click);
            // 
            // Attributes_Panel
            // 
            this.Attributes_Panel.AutoScroll = true;
            this.Attributes_Panel.Location = new System.Drawing.Point(6, 32);
            this.Attributes_Panel.Name = "Attributes_Panel";
            this.Attributes_Panel.Size = new System.Drawing.Size(1155, 511);
            this.Attributes_Panel.TabIndex = 0;
            //
            // winMoz_Gui_NewMarketObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1204, 725);
            this.Controls.Add(this.Default);
            this.Controls.Add(this.Attributes_Box);
            this.Controls.Add(this.MarketObject);
            this.Controls.Add(this.MarketObject_ComboBox);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Add);
            this.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.Name = "winMoz_Gui_NewMarketObject";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "Mozubi Scenario Builder";
            this.Load += new System.EventHandler(this.winMoz_Gui_Load);
            this.Attributes_Box.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.ComboBox MarketObject_ComboBox;
        private System.Windows.Forms.Label MarketObject;
        private System.Windows.Forms.GroupBox Attributes_Box;
        private System.Windows.Forms.Button Default;
        private System.Windows.Forms.Panel Attributes_Panel;
    }
}

