﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace winMoz_Gui
{
    public partial class winMoz_Gui_AddMaStRId : Form
    {
        public bool HasToBeAdded { get; private set; }
        public int MaStRId_Value { get; private set; }

        public List<int> MaStRIds { get; set; }

        public winMoz_Gui_AddMaStRId()
        {
            HasToBeAdded = false;
            InitializeComponent();
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
        }

        private void MaStRId_TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (MaStRId_TextBox.Text == "" || MaStRIds.Contains((int)Convert.ToInt64(MaStRId_TextBox.Text)))
                {
                    HasToBeAdded = false;
                    this.Add.Enabled = false;
                }
                else
                {
                    this.Add.Enabled = true;
                }
            }
            catch
            {
                this.Add.Enabled = false;
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            HasToBeAdded = true;
            MaStRId_Value = (int)Convert.ToInt64(this.MaStRId_TextBox.Text);
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            HasToBeAdded = false;
            this.Close();
        }
    }
}
