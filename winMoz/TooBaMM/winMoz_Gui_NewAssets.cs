﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace winMoz_Gui
{
    public partial class winMoz_Gui_NewAssets : Form
    {
        public bool HasToBeAdded { get; private set; }
        public List<int> Assets_IDs { get; set; }
        public int Assets_ID_Value { get; set; }
        public int MaStRId_Value { get; private set; }

        public List<int> MaStRIds { get; set; }
        winMoz_Gui_AddMaStRId gui_AddMaStRId;

        public winMoz_Gui_NewAssets()
        {
            HasToBeAdded = false;
            InitializeComponent();
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
            gui_AddMaStRId = new winMoz_Gui_AddMaStRId();
            gui_AddMaStRId.FormClosed += AddMaStRIds_Close;
            this.Assets_ID_TextBox.Text = this.Assets_ID_Value.ToString();
        }

        private void MaStRId_TextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            HasToBeAdded = true;
            Assets_ID_Value = (int)Convert.ToInt64(this.Assets_ID_TextBox.Text);
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            HasToBeAdded = false;
            this.Close();
        }

        private void AddAdditionalAsset_Click(object sender, EventArgs e)
        {
            gui_AddMaStRId.MaStRIds = this.MaStRIds;
            gui_AddMaStRId.ShowDialog();
        }

        private void AddMaStRIds_Close(object sender, EventArgs e)
        {
            if (gui_AddMaStRId.HasToBeAdded)
            {
                this.AdditionalAssets_ListBox.Items.Add(gui_AddMaStRId.MaStRId_Value.ToString());
                this.MaStRIds.Add(gui_AddMaStRId.MaStRId_Value);
            }
        }

        private void Assets_ID_TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Assets_ID_TextBox.Text == "" || Assets_IDs.Contains((int)Convert.ToInt64(Assets_ID_TextBox.Text)))
                {
                    HasToBeAdded = false;
                    this.AddAdditionalAsset.Enabled = false;
                    this.DeleteAdditionalAsset.Enabled = false;
                    this.Save.Enabled = false;
                }
                else
                {
                    this.AddAdditionalAsset.Enabled = true;
                    this.DeleteAdditionalAsset.Enabled = true;
                    this.Save.Enabled = true;
                }
            }
            catch
            {
                this.AddAdditionalAsset.Enabled = false;
                this.DeleteAdditionalAsset.Enabled = false;
                this.Save.Enabled = false;
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            this.HasToBeAdded = true;
            this.Assets_ID_Value = (int)Convert.ToInt64(this.Assets_ID_TextBox.Text);
            this.Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.HasToBeAdded = false;
            this.Close();
        }
    }
}
