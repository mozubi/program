﻿namespace winMoz_Gui
{
    partial class winMoz_Gui_PlotAndOutput
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Save = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.PlotsBox = new System.Windows.Forms.GroupBox();
            this.DeletePlot = new System.Windows.Forms.Button();
            this.AddPlot = new System.Windows.Forms.Button();
            this.Plot_ListBox = new System.Windows.Forms.ListBox();
            this.OutputConsoleBox = new System.Windows.Forms.GroupBox();
            this.DeletOutputConsole = new System.Windows.Forms.Button();
            this.AddOutputConsole = new System.Windows.Forms.Button();
            this.OutputConsole_ListBox = new System.Windows.Forms.ListBox();
            this.OutputCSVBox = new System.Windows.Forms.GroupBox();
            this.DeletOutputCSV = new System.Windows.Forms.Button();
            this.AddOutputCSV = new System.Windows.Forms.Button();
            this.OutputCSV_ListBox = new System.Windows.Forms.ListBox();
            this.PlotsBox.SuspendLayout();
            this.OutputConsoleBox.SuspendLayout();
            this.OutputCSVBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Save
            // 
            this.Save.BackColor = System.Drawing.Color.LawnGreen;
            this.Save.Font = new System.Drawing.Font("Arial", 12F);
            this.Save.Location = new System.Drawing.Point(1222, 42);
            this.Save.Margin = new System.Windows.Forms.Padding(6);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(272, 68);
            this.Save.TabIndex = 0;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = false;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Cancel
            // 
            this.Cancel.BackColor = System.Drawing.Color.Silver;
            this.Cancel.Font = new System.Drawing.Font("Arial", 12F);
            this.Cancel.Location = new System.Drawing.Point(1222, 132);
            this.Cancel.Margin = new System.Windows.Forms.Padding(6);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(272, 64);
            this.Cancel.TabIndex = 2;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = false;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // PlotsBox
            // 
            this.PlotsBox.Controls.Add(this.DeletePlot);
            this.PlotsBox.Controls.Add(this.AddPlot);
            this.PlotsBox.Controls.Add(this.Plot_ListBox);
            this.PlotsBox.Location = new System.Drawing.Point(20, 18);
            this.PlotsBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PlotsBox.Name = "PlotsBox";
            this.PlotsBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PlotsBox.Size = new System.Drawing.Size(1170, 220);
            this.PlotsBox.TabIndex = 5;
            this.PlotsBox.TabStop = false;
            this.PlotsBox.Text = "Plots";
            // 
            // DeletePlot
            // 
            this.DeletePlot.BackColor = System.Drawing.Color.PaleVioletRed;
            this.DeletePlot.Font = new System.Drawing.Font("Arial", 12F);
            this.DeletePlot.Location = new System.Drawing.Point(9, 104);
            this.DeletePlot.Margin = new System.Windows.Forms.Padding(6);
            this.DeletePlot.Name = "DeletePlot";
            this.DeletePlot.Size = new System.Drawing.Size(110, 42);
            this.DeletePlot.TabIndex = 8;
            this.DeletePlot.Text = "Delete";
            this.DeletePlot.UseVisualStyleBackColor = false;
            this.DeletePlot.Click += new System.EventHandler(this.DeletePlot_Click);
            // 
            // AddPlot
            // 
            this.AddPlot.BackColor = System.Drawing.Color.SkyBlue;
            this.AddPlot.Font = new System.Drawing.Font("Arial", 12F);
            this.AddPlot.Location = new System.Drawing.Point(9, 48);
            this.AddPlot.Margin = new System.Windows.Forms.Padding(6);
            this.AddPlot.Name = "AddPlot";
            this.AddPlot.Size = new System.Drawing.Size(110, 44);
            this.AddPlot.TabIndex = 2;
            this.AddPlot.Text = "Add";
            this.AddPlot.UseVisualStyleBackColor = false;
            this.AddPlot.Click += new System.EventHandler(this.AddPlot_Click);
            // 
            // Plot_ListBox
            // 
            this.Plot_ListBox.FormattingEnabled = true;
            this.Plot_ListBox.HorizontalScrollbar = true;
            this.Plot_ListBox.ItemHeight = 25;
            this.Plot_ListBox.Location = new System.Drawing.Point(141, 52);
            this.Plot_ListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Plot_ListBox.Name = "Plot_ListBox";
            this.Plot_ListBox.Size = new System.Drawing.Size(1012, 154);
            this.Plot_ListBox.TabIndex = 3;
            // 
            // OutputConsoleBox
            // 
            this.OutputConsoleBox.Controls.Add(this.DeletOutputConsole);
            this.OutputConsoleBox.Controls.Add(this.AddOutputConsole);
            this.OutputConsoleBox.Controls.Add(this.OutputConsole_ListBox);
            this.OutputConsoleBox.Location = new System.Drawing.Point(20, 262);
            this.OutputConsoleBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OutputConsoleBox.Name = "OutputConsoleBox";
            this.OutputConsoleBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OutputConsoleBox.Size = new System.Drawing.Size(1170, 220);
            this.OutputConsoleBox.TabIndex = 9;
            this.OutputConsoleBox.TabStop = false;
            this.OutputConsoleBox.Text = "Output Console";
            // 
            // DeletOutputConsole
            // 
            this.DeletOutputConsole.BackColor = System.Drawing.Color.PaleVioletRed;
            this.DeletOutputConsole.Font = new System.Drawing.Font("Arial", 12F);
            this.DeletOutputConsole.Location = new System.Drawing.Point(9, 104);
            this.DeletOutputConsole.Margin = new System.Windows.Forms.Padding(6);
            this.DeletOutputConsole.Name = "DeletOutputConsole";
            this.DeletOutputConsole.Size = new System.Drawing.Size(110, 42);
            this.DeletOutputConsole.TabIndex = 8;
            this.DeletOutputConsole.Text = "Delete";
            this.DeletOutputConsole.UseVisualStyleBackColor = false;
            this.DeletOutputConsole.Click += new System.EventHandler(this.DeletOutputConsole_Click);
            // 
            // AddOutputConsole
            // 
            this.AddOutputConsole.BackColor = System.Drawing.Color.SkyBlue;
            this.AddOutputConsole.Font = new System.Drawing.Font("Arial", 12F);
            this.AddOutputConsole.Location = new System.Drawing.Point(9, 48);
            this.AddOutputConsole.Margin = new System.Windows.Forms.Padding(6);
            this.AddOutputConsole.Name = "AddOutputConsole";
            this.AddOutputConsole.Size = new System.Drawing.Size(110, 44);
            this.AddOutputConsole.TabIndex = 2;
            this.AddOutputConsole.Text = "Add";
            this.AddOutputConsole.UseVisualStyleBackColor = false;
            this.AddOutputConsole.Click += new System.EventHandler(this.AddOutputConsole_Click);
            // 
            // OutputConsole_ListBox
            // 
            this.OutputConsole_ListBox.FormattingEnabled = true;
            this.OutputConsole_ListBox.HorizontalScrollbar = true;
            this.OutputConsole_ListBox.ItemHeight = 25;
            this.OutputConsole_ListBox.Location = new System.Drawing.Point(141, 52);
            this.OutputConsole_ListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OutputConsole_ListBox.Name = "OutputConsole_ListBox";
            this.OutputConsole_ListBox.Size = new System.Drawing.Size(1012, 154);
            this.OutputConsole_ListBox.TabIndex = 3;
            // 
            // OutputCSVBox
            // 
            this.OutputCSVBox.Controls.Add(this.DeletOutputCSV);
            this.OutputCSVBox.Controls.Add(this.AddOutputCSV);
            this.OutputCSVBox.Controls.Add(this.OutputCSV_ListBox);
            this.OutputCSVBox.Location = new System.Drawing.Point(23, 506);
            this.OutputCSVBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OutputCSVBox.Name = "OutputCSVBox";
            this.OutputCSVBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OutputCSVBox.Size = new System.Drawing.Size(1170, 220);
            this.OutputCSVBox.TabIndex = 10;
            this.OutputCSVBox.TabStop = false;
            this.OutputCSVBox.Text = "Output CSV";
            // 
            // DeletOutputCSV
            // 
            this.DeletOutputCSV.BackColor = System.Drawing.Color.PaleVioletRed;
            this.DeletOutputCSV.Font = new System.Drawing.Font("Arial", 12F);
            this.DeletOutputCSV.Location = new System.Drawing.Point(9, 104);
            this.DeletOutputCSV.Margin = new System.Windows.Forms.Padding(6);
            this.DeletOutputCSV.Name = "DeletOutputCSV";
            this.DeletOutputCSV.Size = new System.Drawing.Size(110, 42);
            this.DeletOutputCSV.TabIndex = 8;
            this.DeletOutputCSV.Text = "Delete";
            this.DeletOutputCSV.UseVisualStyleBackColor = false;
            this.DeletOutputCSV.Click += new System.EventHandler(this.DeletOutputCSV_Click);
            // 
            // AddOutputCSV
            // 
            this.AddOutputCSV.BackColor = System.Drawing.Color.SkyBlue;
            this.AddOutputCSV.Font = new System.Drawing.Font("Arial", 12F);
            this.AddOutputCSV.Location = new System.Drawing.Point(9, 48);
            this.AddOutputCSV.Margin = new System.Windows.Forms.Padding(6);
            this.AddOutputCSV.Name = "AddOutputCSV";
            this.AddOutputCSV.Size = new System.Drawing.Size(110, 44);
            this.AddOutputCSV.TabIndex = 2;
            this.AddOutputCSV.Text = "Add";
            this.AddOutputCSV.UseVisualStyleBackColor = false;
            this.AddOutputCSV.Click += new System.EventHandler(this.AddOutputCSV_Click);
            // 
            // OutputCSV_ListBox
            // 
            this.OutputCSV_ListBox.FormattingEnabled = true;
            this.OutputCSV_ListBox.HorizontalScrollbar = true;
            this.OutputCSV_ListBox.ItemHeight = 25;
            this.OutputCSV_ListBox.Location = new System.Drawing.Point(141, 52);
            this.OutputCSV_ListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OutputCSV_ListBox.Name = "OutputCSV_ListBox";
            this.OutputCSV_ListBox.Size = new System.Drawing.Size(1012, 154);
            this.OutputCSV_ListBox.TabIndex = 3;
            // 
            // winMoz_Gui_PlotAndOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1746, 766);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OutputCSVBox);
            this.Controls.Add(this.OutputConsoleBox);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.PlotsBox);
            this.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.Name = "winMoz_Gui_PlotAndOutput";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "PlotAndOutput-Tool";
            this.Load += new System.EventHandler(this.winMoz_Gui_Load);
            this.PlotsBox.ResumeLayout(false);
            this.OutputConsoleBox.ResumeLayout(false);
            this.OutputCSVBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.GroupBox PlotsBox;
        private System.Windows.Forms.Button DeletePlot;
        private System.Windows.Forms.Button AddPlot;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.ListBox Plot_ListBox;
        private System.Windows.Forms.GroupBox OutputConsoleBox;
        private System.Windows.Forms.Button DeletOutputConsole;
        private System.Windows.Forms.Button AddOutputConsole;
        private System.Windows.Forms.ListBox OutputConsole_ListBox;
        private System.Windows.Forms.GroupBox OutputCSVBox;
        private System.Windows.Forms.Button DeletOutputCSV;
        private System.Windows.Forms.Button AddOutputCSV;
        private System.Windows.Forms.ListBox OutputCSV_ListBox;
    }
}

