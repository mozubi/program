﻿namespace winMoz_Gui
{
    partial class winMoz_Gui_MarketObjects
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.MarketsBox = new System.Windows.Forms.GroupBox();
            this.Market_ID_TextBox = new System.Windows.Forms.TextBox();
            this.MarketObjects = new System.Windows.Forms.Label();
            this.DeleteMarketObject = new System.Windows.Forms.Button();
            this.MarketObjects_ListBox = new System.Windows.Forms.ListBox();
            this.AddMarketObject = new System.Windows.Forms.Button();
            this.Market_ID = new System.Windows.Forms.Label();
            this.Delete = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.MarketsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MarketsBox
            // 
            this.MarketsBox.Controls.Add(this.Market_ID_TextBox);
            this.MarketsBox.Controls.Add(this.MarketObjects);
            this.MarketsBox.Controls.Add(this.DeleteMarketObject);
            this.MarketsBox.Controls.Add(this.MarketObjects_ListBox);
            this.MarketsBox.Controls.Add(this.AddMarketObject);
            this.MarketsBox.Controls.Add(this.Market_ID);
            this.MarketsBox.Location = new System.Drawing.Point(26, 94);
            this.MarketsBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarketsBox.Name = "MarketsBox";
            this.MarketsBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarketsBox.Size = new System.Drawing.Size(620, 344);
            this.MarketsBox.TabIndex = 5;
            this.MarketsBox.TabStop = false;
            this.MarketsBox.Text = "Markets";
            // 
            // Market_ID_TextBox
            // 
            this.Market_ID_TextBox.Location = new System.Drawing.Point(139, 55);
            this.Market_ID_TextBox.Name = "Market_ID_TextBox";
            this.Market_ID_TextBox.Size = new System.Drawing.Size(439, 33);
            this.Market_ID_TextBox.TabIndex = 12;
            this.Market_ID_TextBox.TextChanged += new System.EventHandler(this.Market_ID_TextBox_TextChanged);
            // 
            // MarketObjects
            // 
            this.MarketObjects.AutoSize = true;
            this.MarketObjects.Font = new System.Drawing.Font("Arial", 12F);
            this.MarketObjects.Location = new System.Drawing.Point(6, 121);
            this.MarketObjects.Name = "MarketObjects";
            this.MarketObjects.Size = new System.Drawing.Size(96, 54);
            this.MarketObjects.TabIndex = 6;
            this.MarketObjects.Text = "Market \r\nobjects:";
            // 
            // DeleteMarketObject
            // 
            this.DeleteMarketObject.BackColor = System.Drawing.Color.PaleVioletRed;
            this.DeleteMarketObject.Enabled = false;
            this.DeleteMarketObject.Font = new System.Drawing.Font("Arial", 12F);
            this.DeleteMarketObject.Location = new System.Drawing.Point(429, 272);
            this.DeleteMarketObject.Margin = new System.Windows.Forms.Padding(6);
            this.DeleteMarketObject.Name = "DeleteMarketObject";
            this.DeleteMarketObject.Size = new System.Drawing.Size(149, 45);
            this.DeleteMarketObject.TabIndex = 8;
            this.DeleteMarketObject.Text = "Delete";
            this.DeleteMarketObject.UseVisualStyleBackColor = false;
            this.DeleteMarketObject.Click += new System.EventHandler(this.DeleteMarketObject_Click);
            // 
            // MarketObjects_ListBox
            // 
            this.MarketObjects_ListBox.FormattingEnabled = true;
            this.MarketObjects_ListBox.HorizontalScrollbar = true;
            this.MarketObjects_ListBox.ItemHeight = 25;
            this.MarketObjects_ListBox.Location = new System.Drawing.Point(139, 121);
            this.MarketObjects_ListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarketObjects_ListBox.Name = "MarketObjects_ListBox";
            this.MarketObjects_ListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.MarketObjects_ListBox.Size = new System.Drawing.Size(439, 129);
            this.MarketObjects_ListBox.TabIndex = 3;
            // 
            // AddMarketObject
            // 
            this.AddMarketObject.BackColor = System.Drawing.Color.SkyBlue;
            this.AddMarketObject.Font = new System.Drawing.Font("Arial", 12F);
            this.AddMarketObject.Location = new System.Drawing.Point(139, 272);
            this.AddMarketObject.Margin = new System.Windows.Forms.Padding(6);
            this.AddMarketObject.Name = "AddMarketObject";
            this.AddMarketObject.Size = new System.Drawing.Size(152, 44);
            this.AddMarketObject.TabIndex = 2;
            this.AddMarketObject.Text = "Add";
            this.AddMarketObject.UseVisualStyleBackColor = false;
            this.AddMarketObject.Click += new System.EventHandler(this.AddMarketObject_Click);
            // 
            // Market_ID
            // 
            this.Market_ID.AutoSize = true;
            this.Market_ID.Font = new System.Drawing.Font("Arial", 12F);
            this.Market_ID.Location = new System.Drawing.Point(6, 57);
            this.Market_ID.Name = "Market_ID";
            this.Market_ID.Size = new System.Drawing.Size(127, 27);
            this.Market_ID.TabIndex = 4;
            this.Market_ID.Text = "Market_ID:";
            // 
            // Delete
            // 
            this.Delete.BackColor = System.Drawing.Color.LightCoral;
            this.Delete.Font = new System.Drawing.Font("Arial", 12F);
            this.Delete.Location = new System.Drawing.Point(360, 22);
            this.Delete.Margin = new System.Windows.Forms.Padding(6);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(284, 52);
            this.Delete.TabIndex = 16;
            this.Delete.Text = "Cancel";
            this.Delete.UseVisualStyleBackColor = false;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Add
            // 
            this.Add.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Add.Font = new System.Drawing.Font("Arial", 12F);
            this.Add.Location = new System.Drawing.Point(26, 22);
            this.Add.Margin = new System.Windows.Forms.Padding(6);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(322, 52);
            this.Add.TabIndex = 15;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = false;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // winMoz_Gui_MarketObjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(662, 451);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.MarketsBox);
            this.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.Name = "winMoz_Gui_MarketObjects";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "Mozubi Scenario Builder";
            this.Load += new System.EventHandler(this.winMoz_Gui_Load);
            this.MarketsBox.ResumeLayout(false);
            this.MarketsBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox MarketsBox;
        private System.Windows.Forms.Label MarketObjects;
        private System.Windows.Forms.ListBox MarketObjects_ListBox;
        private System.Windows.Forms.Label Market_ID;
        private System.Windows.Forms.Button DeleteMarketObject;
        private System.Windows.Forms.Button AddMarketObject;
        private System.Windows.Forms.TextBox Market_ID_TextBox;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Add;
    }
}

