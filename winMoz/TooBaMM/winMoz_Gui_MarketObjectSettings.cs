﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using winMoz.Simulation;
using winMoz.Markets;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.Elements;
using winMoz.Markets.Bids;
using winMoz.Markets.EoMMarkets;

namespace winMoz_Gui
{
    public partial class winMoz_Gui_MarketObjectSettings : Form
    {
        public SimObject MarketObjectForSettings;

        public Dictionary<string, (Label Label, Control Element)> FormElements = new Dictionary<string, (Label, Control)>();

        public winMoz_Gui_MarketObjectSettings()
        {
            InitializeComponent();
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
            this.Text = "Settings of " + MarketObjectForSettings.GetType().Name + " ID:" + MarketObjectForSettings.Id;
            switch (MarketObjectForSettings.GetType().Name)
            {
                case "FuturesMarket":
                    {
                        CreateFuturesMarketElements((FuturesMarket)MarketObjectForSettings);
                        break;
                    }
                case "DayAheadMarket":
                    {
                        CreateDayAheadMarketElements((DayAheadMarket)MarketObjectForSettings);
                        break;
                    }
                case "IntradayMarket":
                    {
                        CreateIntradayMarketElements((IntradayMarket)MarketObjectForSettings);
                        break;
                    }
                case "ControlReserve_Capacity":
                    {
                        CreateControlReserve_CapacityElements((ControlReserve_Capacity)MarketObjectForSettings);
                        break;
                    }
                case "ControlReserve_Energy":
                    {
                        CreateControlReserve_EnergyElements((ControlReserve_Energy)MarketObjectForSettings);
                        break;
                    }
                case "ControlReserve_Energy_Scenario1":
                    {
                        CreateControlReserve_EnergyElements((ControlReserve_Energy)MarketObjectForSettings);
                        break;
                    }
                case "ControlReserve_Activation":
                    {
                        CreateControlReserve_ActivationElements((ControlReserve_Activation)MarketObjectForSettings);
                        break;
                    }
                case "ControlReserveTenderVolumeHandler":
                    {
                        CreateControlReserveTenderVolumeHandlerElements((ControlReserveTenderVolumeHandler)MarketObjectForSettings);
                        break;
                    }
                case "Scenario1_ControlReserveTenderVolumeHandler":
                    {
                        CreateControlReserveTenderVolumeHandlerElements((ControlReserveTenderVolumeHandler)MarketObjectForSettings);
                        break;
                    }
                case "ControlReserveActivationVolumeHandler":
                    {
                        CreateControlReserveActivationVolumeHandlerElements((ControlReserveActivationVolumeHandler)MarketObjectForSettings);
                        break;
                    }
                case "IGCC_Calculation":
                    {
                        CreateIGCC_CalculationElements();
                        break;
                    }
                case "ReBAP_Calculation":
                    {
                        CreateReBAP_CalculationElements((ReBAP_Calculation)MarketObjectForSettings);
                        break;
                    }
                case "DeviationFromRZSaldo_Handler":
                    {
                        CreateDeviationFromRZSaldo_HandlerElements();
                        break;
                    }
            }
        }

        private void CreateFuturesMarketElements(FuturesMarket futures)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketAttributes(0, typeof(FuturesMarket), futures.Attributes);
        }

        private void CreateDayAheadMarketElements(DayAheadMarket dayAhead)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketWithBlocksAttributes(0, typeof(DayAheadMarket), (DayAheadAttributes)dayAhead.Attributes);
            FormElements.Add("WithMarketCoupling", (CreateElementLabel("WithMarketCoupling:", nextNumberYPos), CreateElementCheckBox(nextNumberYPos, (DayAheadAttributes)dayAhead.Attributes != null ? ((DayAheadAttributes)dayAhead.Attributes).WithMarketCoupling : false)));
            FormElements.Add("MCC_ID", (CreateElementLabel("MCC_ID:", nextNumberYPos + 1), CreateElementTextBox(nextNumberYPos + 1, (DayAheadAttributes)dayAhead.Attributes != null ? ((DayAheadAttributes)dayAhead.Attributes).MCC_ID.ToString() : "")));
        }

        private void CreateIntradayMarketElements(IntradayMarket intraday)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketWithBlocksAttributes(0, typeof(IntradayMarket), (IntradayAttributes)intraday.Attributes);
        }

        private void CreateControlReserve_CapacityElements(ControlReserve_Capacity controlReserveCapacity)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketAttributes(0, typeof(ControlReserve_Capacity), (ControlReserve_Capacity_Attributes)controlReserveCapacity.Attributes);
            FormElements.Add("EnergyMarket", (CreateElementLabel("EnergyMarket:", nextNumberYPos), CreateElementTextBox(nextNumberYPos, controlReserveCapacity.EnergyMarket.GetType().Name + " ID:" + controlReserveCapacity.EnergyMarket.Id.ToString())));
        }

        private void CreateControlReserve_EnergyElements(ControlReserve_Energy controlReserveEnergy)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketAttributes(0, typeof(ControlReserve_Energy), (ControlReserve_Energy_Attributes)controlReserveEnergy.Attributes);
            FormElements.Add("OnlyBidsFromCapacityAllowed", (CreateElementLabel("OnlyBidsFromCapacity:", nextNumberYPos), CreateElementCheckBox(nextNumberYPos, value: (ControlReserve_Energy_Attributes)controlReserveEnergy.Attributes != null ? ((ControlReserve_Energy_Attributes)controlReserveEnergy.Attributes).OnlyBidsFromCapacityAllowed : false)));
            FormElements.Add("IntradayMarket", (CreateElementLabel("IntradayMarket:", nextNumberYPos + 1), CreateElementTextBox(nextNumberYPos + 1, controlReserveEnergy.Intraday.GetType().Name + " ID:" + controlReserveEnergy.Intraday.Id.ToString())));
            FormElements.Add("Activation", (CreateElementLabel("Activation:", nextNumberYPos + 2), CreateElementTextBox(nextNumberYPos + 2, controlReserveEnergy.Activation.GetType().Name + " ID:" + controlReserveEnergy.Activation.Id.ToString())));
        }

        private void CreateControlReserve_ActivationElements(ControlReserve_Activation activation)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddActivationAttributes(0, activation.Attributes);
        }

        private void CreateControlReserveTenderVolumeHandlerElements(ControlReserveTenderVolumeHandler controlReserveTenderVolumeHandler)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("Markets", (CreateElementLabel("Markets:", 0), CreateElementListBox(0, controlReserveTenderVolumeHandler.Markets.Select(item => item.GetType().Name + " ID:" + item.Id.ToString()).ToArray())));
            FormElements.Add("ErrorLevel_FRR", (CreateElementLabel("ErrorLevel_FRR:", 2), CreateElementNumericUpDown(2, min: 0.00001m, max: 100m, dec: 5, value: controlReserveTenderVolumeHandler.Attributes != null ? (decimal)controlReserveTenderVolumeHandler.Attributes.ErrorLevel_FRR : 0.00001m)));
            FormElements.Add("ErrorLevel_aFRR", (CreateElementLabel("ErrorLevel_aFRR:", 3), CreateElementNumericUpDown(3, min: 0.00001m, max: 100m, dec: 5, value: controlReserveTenderVolumeHandler.Attributes != null ? (decimal)controlReserveTenderVolumeHandler.Attributes.ErrorLevel_aFRR : 0.00001m)));
        }

        private void CreateControlReserveActivationVolumeHandlerElements(ControlReserveActivationVolumeHandler controlReserveActivationVolumeHandler)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("Activations", (CreateElementLabel("Activation:", 0), CreateElementListBox(0, controlReserveActivationVolumeHandler.Activations.Select(item => item.GetType().Name + " ID:" + item.Id.ToString()).ToArray())));
            FormElements.Add("IGCC_Calculation", (CreateElementLabel("IGCC_Calculation:", 2), CreateElementTextBox(2, controlReserveActivationVolumeHandler.IGCC.GetType().Name + " ID:" + controlReserveActivationVolumeHandler.IGCC.Id.ToString())));
            FormElements.Add("DeviationFromRZSaldoHandler", (CreateElementLabel("DeviationFromRZSaldoHandler:", 3), CreateElementTextBox(3, controlReserveActivationVolumeHandler.DeviationFromRZSaldo.GetType().Name + " ID:" + controlReserveActivationVolumeHandler.DeviationFromRZSaldo.Id.ToString())));
        }

        private void CreateIGCC_CalculationElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            //this.Attributes_Box.Height = 30;
            //this.Height = 151 + 30;
        }

        private void CreateReBAP_CalculationElements(ReBAP_Calculation reBAP)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddEventAttributes(0, reBAP.Attributes);
            FormElements.Add("IGCC_Calculation", (CreateElementLabel("IGCC_Calculation:", nextNumberYPos), CreateElementTextBox(nextNumberYPos, reBAP.IGCC.GetType().Name + " ID:" + reBAP.IGCC.Id.ToString())));
        }

        private void CreateDeviationFromRZSaldo_HandlerElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
        }

        private int AddMarketWithBlocksAttributes(int numberYPos, Type marketType, MarketWithBlocksAttributes attributes = null)
        {
            var nextNumberYPos = AddMarketAttributes(numberYPos, marketType, attributes);
            FormElements.Add("BlockTypes", (CreateElementLabel("BlockTypes:", nextNumberYPos), CreateElementListBox(nextNumberYPos, attributes._BlockTypes.Select(item => item.ToString()).ToArray())));
            return nextNumberYPos + 2;
        }

        private string[] GetBlockTypes(Type type)
        {
            switch (type.ToString())
            {
                case "DayAhead":
                    {
                        var Test = ShortCode.DayAhead.GetBlockCodes().ToArray();
                        return ShortCode.DayAhead.GetBlockCodes().ToArray();
                    }
                case "Intraday": return ShortCode.IntraDay.GetBlockCodes().ToArray();
                default:
                    {
                        var BlockTypes = new List<string>(ShortCode.DayAhead.GetBlockCodes());
                        BlockTypes.AddRange(ShortCode.IntraDay.GetBlockCodes());
                        return BlockTypes.ToArray();
                    }
            }
        }

        private int AddActivationAttributes(int numberYPos, ActivationAttributes attributes = null)
        {
            int nextNumberYPos = AddEventAttributes(numberYPos);
            FormElements.Add("AuctionStep", (CreateElementLabel("AuctionStep:", nextNumberYPos), CreateElementTextBox(nextNumberYPos, attributes != null ? attributes.AuctionStep.ToString() : "")));
            FormElements.Add("Upper_Price_Bound", (CreateElementLabel("Upper_Price_Bound:", nextNumberYPos + 1), CreateElementNumericUpDown(nextNumberYPos + 1, dec: 2, value: attributes != null ? (decimal)attributes.Upper_Price_Bound : 0)));
            FormElements.Add("Lower_Price_Bound", (CreateElementLabel("Lower_Price_Bound:", nextNumberYPos + 2), CreateElementNumericUpDown(nextNumberYPos + 2, dec: 2, value: attributes != null ? (decimal)attributes.Lower_Price_Bound : 0)));
            return nextNumberYPos + 3;
        }

        private int AddMarketAttributes(int numberYPos, Type marketType, MarketAttributes attributes = null)
        {
            int nextNumberYPos = AddEventAttributes(numberYPos, attributes != null ? attributes.EventAttributes : null);
            FormElements.Add("Upper_Price_Bound", (CreateElementLabel("Upper_Price_Bound:", nextNumberYPos), CreateElementNumericUpDown(nextNumberYPos, dec: 2, value: attributes != null ? (decimal)attributes.Upper_Price_Bound : 0)));
            FormElements.Add("Lower_Price_Bound", (CreateElementLabel("Lower_Price_Bound:", nextNumberYPos + 1), CreateElementNumericUpDown(nextNumberYPos + 1, dec: 2, value: attributes != null ? (decimal)attributes.Lower_Price_Bound : 0)));
            FormElements.Add("LocalMACode", (CreateElementLabel("LocalMACode:", nextNumberYPos + 2), CreateElementTextBox(nextNumberYPos + 2, attributes != null ? attributes.LocalMACode.ToString() : "")));
            FormElements.Add("MarketType", (CreateElementLabel("MarketType:", nextNumberYPos + 3), CreateElementTextBox(nextNumberYPos + 3, marketType.ToString(), enabled: false)));
            FormElements.Add("AllowedBidType", (CreateElementLabel("AllowedBidType:", nextNumberYPos + 4), CreateElementTextBox(nextNumberYPos + 4, attributes != null ? attributes.AllowedBidType.ToString() : "")));
            FormElements.Add("NumberOfDaysToLetBidsInBidList", (CreateElementLabel("NumberOfDaysToLetBidsInBidList:", nextNumberYPos + 5), CreateElementNumericUpDown(nextNumberYPos + 5, dec: 0, value: attributes != null ? attributes.NumberOfDaysToLetBidsInBidList : 0)));
            return nextNumberYPos + 6;
        }

        private string[] GetAllowedBidTypes(Type type)
        {
            switch (type.ToString())
            {
                case "Futures": return new List<string>() { "winMoz.Markets.Bids.Bid_DayAhead" }.ToArray();
                case "DayAhead": return new List<string>() { "winMoz.Markets.Bids.Bid_DayAhead" }.ToArray();
                case "Intraday": return new List<string>() { "winMoz.Markets.Bids.Bid_Intraday" }.ToArray();
                case "ControlReserve_Capacity": return new List<string>() { "winMoz.Markets.Bids.Bid_ControlReserve_Capacity" }.ToArray();
                case "ControlReserve_Energy": return new List<string>() { "winMoz.Markets.Bids.Bid_ControlReserve_Energy" }.ToArray();
                default: return new List<string>() { "winMoz.Markets.Bids.Bid_DayAhead", "winMoz.Markets.Bids.Bid_DayAhead", "winMoz.Markets.Bids.Bid_Intraday", "winMoz.Markets.Bids.Bid_ControlReserve_Capacity", "winMoz.Markets.Bids.Bid_ControlReserve_Energy" }.ToArray();
            }
        }

        private int AddEventAttributes(int numberYPos, SimEventAttributes attributes = null)
        {
            FormElements.Add("StartEventTime", (CreateElementLabel("StartEventTime:", numberYPos), CreateElementDurationBox(numberYPos, attributes != null ? attributes.StartEventTimeDurationFromStart : null)));
            FormElements.Add("StartDeterminationTime", (CreateElementLabel("StartDeterminationTime:", numberYPos + 3), CreateElementDurationBox(numberYPos + 3, attributes != null ? attributes.StartDeterminationTimeDurationFromStart : null)));
            FormElements.Add("DeterminationDuration", (CreateElementLabel("DeterminationDuration in min:", numberYPos + 6), CreateElementDurationBox(numberYPos + 6, attributes != null ? attributes.DeterminationDuration : null)));
            FormElements.Add("DeterminationStep", (CreateElementLabel("DeterminationStep in min:", numberYPos + 9), CreateElementDurationBox(numberYPos + 9, attributes != null ? attributes.DeterminationStep : null)));
            return numberYPos + 12;
        }

        private Label CreateElementLabel(string text, int numberYPos, int width = default)
        {
            Label lbl = new Label();
            this.Attributes_Panel.Controls.Add(lbl);
            lbl.Location = new Point(x: 28, y: 30 + numberYPos * 30);
            lbl.Text = text;
            lbl.Width = width == default ? 264 : width;
            return lbl;
        }
        private GroupBox CreateElementDurationBox(int numberYPos, TimeSpanSave duration = null)
        {
            GroupBox gb = new GroupBox();
            this.Attributes_Panel.Controls.Add(gb);
            gb.Location = new Point(x: 350, y: 10 + numberYPos * 30);
            gb.Text = "";
            var lblYears = CreateElementLabel("Years", 0, 50);
            gb.Controls.Add(lblYears);
            lblYears.Location = new Point(x: 30, y: 20);
            var nudYears = CreateElementNumericUpDown(0, min: -100, max: 100, value: duration != null ? duration.Years : 0, width: 50);
            gb.Controls.Add(nudYears);
            nudYears.Location = new Point(x: 30, y: 50);
            var lblMonth = CreateElementLabel("Month", 0, 50);
            gb.Controls.Add(lblMonth);
            lblMonth.Location = new Point(x: 100, y: 20);
            var nudMonth = CreateElementNumericUpDown(0, min: -100, max: 100, value: duration != null ? duration.Months : 0, width: 50);
            gb.Controls.Add(nudMonth);
            nudMonth.Location = new Point(x: 100, y: 50);
            var lblDays = CreateElementLabel("Days", 0, 50);
            gb.Controls.Add(lblDays);
            lblDays.Location = new Point(x: 170, y: 20);
            var nudDays = CreateElementNumericUpDown(0, min: -100, max: 100, value: duration != null ? duration.Days : 0, width: 50);
            gb.Controls.Add(nudDays);
            nudDays.Location = new Point(x: 170, y: 50);
            var lblHours = CreateElementLabel("Hours", 0, 50);
            gb.Controls.Add(lblHours);
            lblHours.Location = new Point(x: 240, y: 20);
            var nudHours = CreateElementNumericUpDown(0, min: -100, max: 100, value: duration != null ? duration.Hours : 0, width: 50);
            gb.Controls.Add(nudHours);
            nudHours.Location = new Point(x: 240, y: 50);
            var lblMinutes = CreateElementLabel("Minutes", 0, 50);
            gb.Controls.Add(lblMinutes);
            lblMinutes.Location = new Point(x: 310, y: 20);
            var nudMinutes = CreateElementNumericUpDown(0, min: -100, max: 100, value: duration != null ? duration.Minutes : 0, width: 50);
            gb.Controls.Add(nudMinutes);
            nudMinutes.Location = new Point(x: 310, y: 50);
            gb.Size = new Size(width: 390, height: 90);
            return gb;
        }

        private TextBox CreateElementTextBox(int numberYPos, string text = "", bool enabled = false)
        {
            TextBox tb = new TextBox();
            this.Attributes_Panel.Controls.Add(tb);
            tb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            tb.Size = new Size(width: 390, height: 35);
            tb.Text = text;
            tb.Enabled = enabled;
            return tb;
        }

        private CheckBox CreateElementCheckBox(int numberYPos, bool value = false)
        {
            CheckBox chb = new CheckBox();
            this.Attributes_Panel.Controls.Add(chb);
            chb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            chb.Text = "";
            chb.Checked = value;
            chb.Enabled = false;
            return chb;
        }

        private NumericUpDown CreateElementNumericUpDown(int numberYPos, decimal min = (-1000000m), decimal max = 1000000m, int dec = 0, decimal value = 0, int width = default)
        {
            NumericUpDown nud = new NumericUpDown();
            this.Attributes_Panel.Controls.Add(nud);
            nud.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            nud.Minimum = min;
            nud.Maximum = max;
            nud.Value = value;
            if (width != default) nud.Width = width;
            nud.DecimalPlaces = dec;
            nud.Enabled = false;
            return nud;
        }

        private ComboBox CreateElementComboBox(int numberYPos, string text, params string[] elements)
        {
            ComboBox cb = new ComboBox();
            this.Attributes_Panel.Controls.Add(cb);
            cb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            cb.Text = text;
            cb.Items.AddRange(elements);
            cb.Size = new Size(width: 390, height: 35);
            cb.Enabled = false;
            return cb;
        }

        private ListBox CreateElementListBox(int numberYPos, params string[] elements)
        {
            ListBox lb = new ListBox();
            this.Attributes_Panel.Controls.Add(lb);
            lb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            lb.Text = "";
            lb.Items.AddRange(elements);
            lb.Size = new Size(width: 390, height: 70);
            lb.Enabled = true;
            return lb;
        }
    }
}
