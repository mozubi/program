﻿namespace winMoz_Gui
{
    partial class winMoz_Gui
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Start = new System.Windows.Forms.Button();
            this.ScenarioBox = new System.Windows.Forms.GroupBox();
            this.DeleteScenario = new System.Windows.Forms.Button();
            this.PlotAndOutputTool = new System.Windows.Forms.Button();
            this.SaveSumSchedules = new System.Windows.Forms.CheckBox();
            this.SaveControlReserveDemand = new System.Windows.Forms.CheckBox();
            this.SaveExternalMarketResults = new System.Windows.Forms.CheckBox();
            this.SaveMarketResults = new System.Windows.Forms.CheckBox();
            this.SaveSchedules = new System.Windows.Forms.CheckBox();
            this.SaveBids = new System.Windows.Forms.CheckBox();
            this.AddScenario = new System.Windows.Forms.Button();
            this.NewScenarioBox = new System.Windows.Forms.GroupBox();
            this.StartDataYear = new System.Windows.Forms.Label();
            this.Total_ID_NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.EndDate_DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.StartDate_DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.TYNDPScenario = new System.Windows.Forms.Label();
            this.TYNDPScenario_ComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ScenarioName_TextBox = new System.Windows.Forms.TextBox();
            this.LocalMarket_TextBox = new System.Windows.Forms.TextBox();
            this.LocalMarket = new System.Windows.Forms.Label();
            this.EndDate = new System.Windows.Forms.Label();
            this.StartDate = new System.Windows.Forms.Label();
            this.AssetsBox = new System.Windows.Forms.GroupBox();
            this.CancelNewAssets = new System.Windows.Forms.Button();
            this.Asset_ID_ComboBox = new System.Windows.Forms.ComboBox();
            this.NewAssets = new System.Windows.Forms.Button();
            this.AdditionalAssets_ListBox = new System.Windows.Forms.ListBox();
            this.AdditionalAssets = new System.Windows.Forms.Label();
            this.Asset_ID = new System.Windows.Forms.Label();
            this.MarketsBox = new System.Windows.Forms.GroupBox();
            this.CancelMarketObjects = new System.Windows.Forms.Button();
            this.ShowSettingsMarketObject = new System.Windows.Forms.Button();
            this.NewMarkets = new System.Windows.Forms.Button();
            this.Market_ID_ComboBox = new System.Windows.Forms.ComboBox();
            this.MarketObjects = new System.Windows.Forms.Label();
            this.MarketObjects_ListBox = new System.Windows.Forms.ListBox();
            this.Market_ID = new System.Windows.Forms.Label();
            this.Total_ID = new System.Windows.Forms.Label();
            this.Scenarios_ComboBox = new System.Windows.Forms.ComboBox();
            this.WeatherYear_ComboBox = new System.Windows.Forms.ComboBox();
            this.StartDataYear_ComboBox = new System.Windows.Forms.ComboBox();
            this.ScenarioBox.SuspendLayout();
            this.NewScenarioBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total_ID_NumericUpDown)).BeginInit();
            this.AssetsBox.SuspendLayout();
            this.MarketsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Start
            // 
            this.Start.BackColor = System.Drawing.Color.GreenYellow;
            this.Start.Enabled = false;
            this.Start.Font = new System.Drawing.Font("Arial", 12F);
            this.Start.Location = new System.Drawing.Point(1415, 33);
            this.Start.Margin = new System.Windows.Forms.Padding(6);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(272, 84);
            this.Start.TabIndex = 0;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = false;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // ScenarioBox
            // 
            this.ScenarioBox.Controls.Add(this.DeleteScenario);
            this.ScenarioBox.Controls.Add(this.PlotAndOutputTool);
            this.ScenarioBox.Controls.Add(this.SaveSumSchedules);
            this.ScenarioBox.Controls.Add(this.SaveControlReserveDemand);
            this.ScenarioBox.Controls.Add(this.SaveExternalMarketResults);
            this.ScenarioBox.Controls.Add(this.SaveMarketResults);
            this.ScenarioBox.Controls.Add(this.SaveSchedules);
            this.ScenarioBox.Controls.Add(this.SaveBids);
            this.ScenarioBox.Controls.Add(this.AddScenario);
            this.ScenarioBox.Controls.Add(this.NewScenarioBox);
            this.ScenarioBox.Controls.Add(this.Start);
            this.ScenarioBox.Controls.Add(this.Scenarios_ComboBox);
            this.ScenarioBox.Font = new System.Drawing.Font("Arial", 12F);
            this.ScenarioBox.Location = new System.Drawing.Point(14, 15);
            this.ScenarioBox.Margin = new System.Windows.Forms.Padding(4);
            this.ScenarioBox.Name = "ScenarioBox";
            this.ScenarioBox.Padding = new System.Windows.Forms.Padding(6);
            this.ScenarioBox.Size = new System.Drawing.Size(1721, 926);
            this.ScenarioBox.TabIndex = 1;
            this.ScenarioBox.TabStop = false;
            this.ScenarioBox.Text = "Scenarios";
            // 
            // DeleteScenario
            // 
            this.DeleteScenario.BackColor = System.Drawing.Color.LightCoral;
            this.DeleteScenario.Enabled = false;
            this.DeleteScenario.Font = new System.Drawing.Font("Arial", 12F);
            this.DeleteScenario.Location = new System.Drawing.Point(1415, 185);
            this.DeleteScenario.Margin = new System.Windows.Forms.Padding(6);
            this.DeleteScenario.Name = "DeleteScenario";
            this.DeleteScenario.Size = new System.Drawing.Size(272, 52);
            this.DeleteScenario.TabIndex = 10;
            this.DeleteScenario.Text = "Delete scenario";
            this.DeleteScenario.UseVisualStyleBackColor = false;
            this.DeleteScenario.Click += new System.EventHandler(this.DeleteScenario_Click);
            // 
            // PlotAndOutputTool
            // 
            this.PlotAndOutputTool.BackColor = System.Drawing.Color.Thistle;
            this.PlotAndOutputTool.Font = new System.Drawing.Font("Arial", 12F);
            this.PlotAndOutputTool.Location = new System.Drawing.Point(1415, 240);
            this.PlotAndOutputTool.Margin = new System.Windows.Forms.Padding(6);
            this.PlotAndOutputTool.Name = "PlotAndOutputTool";
            this.PlotAndOutputTool.Size = new System.Drawing.Size(272, 44);
            this.PlotAndOutputTool.TabIndex = 9;
            this.PlotAndOutputTool.Text = "PlotAndOutput-Tool";
            this.PlotAndOutputTool.UseVisualStyleBackColor = false;
            this.PlotAndOutputTool.Click += new System.EventHandler(this.PlotAndOutputTool_Click);
            // 
            // SaveSumSchedules
            // 
            this.SaveSumSchedules.AutoSize = true;
            this.SaveSumSchedules.Location = new System.Drawing.Point(483, 227);
            this.SaveSumSchedules.Name = "SaveSumSchedules";
            this.SaveSumSchedules.Size = new System.Drawing.Size(254, 31);
            this.SaveSumSchedules.TabIndex = 8;
            this.SaveSumSchedules.Text = "Save sumschedules";
            this.SaveSumSchedules.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SaveSumSchedules.UseVisualStyleBackColor = true;
            this.SaveSumSchedules.CheckedChanged += new System.EventHandler(this.SaveSumSchedules_CheckedChanged);
            // 
            // SaveControlReserveDemand
            // 
            this.SaveControlReserveDemand.AutoSize = true;
            this.SaveControlReserveDemand.Location = new System.Drawing.Point(483, 190);
            this.SaveControlReserveDemand.Name = "SaveControlReserveDemand";
            this.SaveControlReserveDemand.Size = new System.Drawing.Size(351, 31);
            this.SaveControlReserveDemand.TabIndex = 7;
            this.SaveControlReserveDemand.Text = "Save control reserve demand";
            this.SaveControlReserveDemand.UseVisualStyleBackColor = true;
            this.SaveControlReserveDemand.CheckedChanged += new System.EventHandler(this.SaveControlReserveDemand_CheckedChanged);
            // 
            // SaveExternalMarketResults
            // 
            this.SaveExternalMarketResults.AutoSize = true;
            this.SaveExternalMarketResults.Location = new System.Drawing.Point(483, 151);
            this.SaveExternalMarketResults.Name = "SaveExternalMarketResults";
            this.SaveExternalMarketResults.Size = new System.Drawing.Size(342, 31);
            this.SaveExternalMarketResults.TabIndex = 6;
            this.SaveExternalMarketResults.Text = "Save external market results";
            this.SaveExternalMarketResults.UseVisualStyleBackColor = true;
            this.SaveExternalMarketResults.CheckedChanged += new System.EventHandler(this.SaveExternalMarketResults_CheckedChanged);
            // 
            // SaveMarketResults
            // 
            this.SaveMarketResults.AutoSize = true;
            this.SaveMarketResults.Location = new System.Drawing.Point(483, 114);
            this.SaveMarketResults.Name = "SaveMarketResults";
            this.SaveMarketResults.Size = new System.Drawing.Size(250, 31);
            this.SaveMarketResults.TabIndex = 5;
            this.SaveMarketResults.Text = "Save market results";
            this.SaveMarketResults.UseVisualStyleBackColor = true;
            this.SaveMarketResults.CheckedChanged += new System.EventHandler(this.SaveMarketResults_CheckedChanged);
            // 
            // SaveSchedules
            // 
            this.SaveSchedules.AutoSize = true;
            this.SaveSchedules.Location = new System.Drawing.Point(483, 80);
            this.SaveSchedules.Name = "SaveSchedules";
            this.SaveSchedules.Size = new System.Drawing.Size(208, 31);
            this.SaveSchedules.TabIndex = 4;
            this.SaveSchedules.Text = "Save schedules";
            this.SaveSchedules.UseVisualStyleBackColor = true;
            this.SaveSchedules.CheckedChanged += new System.EventHandler(this.SaveSchedules_CheckedChanged);
            // 
            // SaveBids
            // 
            this.SaveBids.AutoSize = true;
            this.SaveBids.Location = new System.Drawing.Point(483, 43);
            this.SaveBids.Name = "SaveBids";
            this.SaveBids.Size = new System.Drawing.Size(143, 31);
            this.SaveBids.TabIndex = 3;
            this.SaveBids.Text = "Save bids";
            this.SaveBids.UseVisualStyleBackColor = true;
            this.SaveBids.CheckedChanged += new System.EventHandler(this.SaveBids_CheckedChanged);
            // 
            // AddScenario
            // 
            this.AddScenario.BackColor = System.Drawing.Color.LightSkyBlue;
            this.AddScenario.Font = new System.Drawing.Font("Arial", 12F);
            this.AddScenario.Location = new System.Drawing.Point(1415, 133);
            this.AddScenario.Margin = new System.Windows.Forms.Padding(6);
            this.AddScenario.Name = "AddScenario";
            this.AddScenario.Size = new System.Drawing.Size(272, 52);
            this.AddScenario.TabIndex = 2;
            this.AddScenario.Text = "Add scenario";
            this.AddScenario.UseVisualStyleBackColor = false;
            this.AddScenario.Click += new System.EventHandler(this.AddScenario_Click);
            // 
            // NewScenarioBox
            // 
            this.NewScenarioBox.Controls.Add(this.StartDataYear_ComboBox);
            this.NewScenarioBox.Controls.Add(this.WeatherYear_ComboBox);
            this.NewScenarioBox.Controls.Add(this.StartDataYear);
            this.NewScenarioBox.Controls.Add(this.Total_ID_NumericUpDown);
            this.NewScenarioBox.Controls.Add(this.EndDate_DateTimePicker);
            this.NewScenarioBox.Controls.Add(this.StartDate_DateTimePicker);
            this.NewScenarioBox.Controls.Add(this.TYNDPScenario);
            this.NewScenarioBox.Controls.Add(this.TYNDPScenario_ComboBox);
            this.NewScenarioBox.Controls.Add(this.label2);
            this.NewScenarioBox.Controls.Add(this.label1);
            this.NewScenarioBox.Controls.Add(this.ScenarioName_TextBox);
            this.NewScenarioBox.Controls.Add(this.LocalMarket_TextBox);
            this.NewScenarioBox.Controls.Add(this.LocalMarket);
            this.NewScenarioBox.Controls.Add(this.EndDate);
            this.NewScenarioBox.Controls.Add(this.StartDate);
            this.NewScenarioBox.Controls.Add(this.AssetsBox);
            this.NewScenarioBox.Controls.Add(this.MarketsBox);
            this.NewScenarioBox.Controls.Add(this.Total_ID);
            this.NewScenarioBox.Font = new System.Drawing.Font("Arial", 12F);
            this.NewScenarioBox.Location = new System.Drawing.Point(9, 287);
            this.NewScenarioBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NewScenarioBox.Name = "NewScenarioBox";
            this.NewScenarioBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NewScenarioBox.Size = new System.Drawing.Size(1692, 629);
            this.NewScenarioBox.TabIndex = 1;
            this.NewScenarioBox.TabStop = false;
            this.NewScenarioBox.Text = "Scenario";
            // 
            // StartDataYear
            // 
            this.StartDataYear.AutoSize = true;
            this.StartDataYear.Font = new System.Drawing.Font("Arial", 12F);
            this.StartDataYear.Location = new System.Drawing.Point(0, 491);
            this.StartDataYear.Name = "StartDataYear";
            this.StartDataYear.Size = new System.Drawing.Size(124, 54);
            this.StartDataYear.TabIndex = 21;
            this.StartDataYear.Text = "Start data \r\nyear:";
            // 
            // Total_ID_NumericUpDown
            // 
            this.Total_ID_NumericUpDown.Location = new System.Drawing.Point(135, 56);
            this.Total_ID_NumericUpDown.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.Total_ID_NumericUpDown.Name = "Total_ID_NumericUpDown";
            this.Total_ID_NumericUpDown.Size = new System.Drawing.Size(421, 35);
            this.Total_ID_NumericUpDown.TabIndex = 19;
            // 
            // EndDate_DateTimePicker
            // 
            this.EndDate_DateTimePicker.Enabled = false;
            this.EndDate_DateTimePicker.Location = new System.Drawing.Point(136, 261);
            this.EndDate_DateTimePicker.Name = "EndDate_DateTimePicker";
            this.EndDate_DateTimePicker.Size = new System.Drawing.Size(420, 35);
            this.EndDate_DateTimePicker.TabIndex = 18;
            // 
            // StartDate_DateTimePicker
            // 
            this.StartDate_DateTimePicker.Enabled = false;
            this.StartDate_DateTimePicker.Location = new System.Drawing.Point(136, 191);
            this.StartDate_DateTimePicker.Name = "StartDate_DateTimePicker";
            this.StartDate_DateTimePicker.Size = new System.Drawing.Size(420, 35);
            this.StartDate_DateTimePicker.TabIndex = 11;
            // 
            // TYNDPScenario
            // 
            this.TYNDPScenario.AutoSize = true;
            this.TYNDPScenario.Font = new System.Drawing.Font("Arial", 12F);
            this.TYNDPScenario.Location = new System.Drawing.Point(6, 554);
            this.TYNDPScenario.Name = "TYNDPScenario";
            this.TYNDPScenario.Size = new System.Drawing.Size(113, 54);
            this.TYNDPScenario.TabIndex = 17;
            this.TYNDPScenario.Text = "TYNDP \r\nScenario:";
            // 
            // TYNDPScenario_ComboBox
            // 
            this.TYNDPScenario_ComboBox.Enabled = false;
            this.TYNDPScenario_ComboBox.Font = new System.Drawing.Font("Arial", 12F);
            this.TYNDPScenario_ComboBox.FormattingEnabled = true;
            this.TYNDPScenario_ComboBox.Items.AddRange(new object[] {
            "BEST",
            "DG",
            "EUCO",
            "ST",
            "GCA"});
            this.TYNDPScenario_ComboBox.Location = new System.Drawing.Point(136, 551);
            this.TYNDPScenario_ComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TYNDPScenario_ComboBox.Name = "TYNDPScenario_ComboBox";
            this.TYNDPScenario_ComboBox.Size = new System.Drawing.Size(421, 35);
            this.TYNDPScenario_ComboBox.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F);
            this.label2.Location = new System.Drawing.Point(6, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 54);
            this.label2.TabIndex = 16;
            this.label2.Text = "Scenario \r\nname:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F);
            this.label1.Location = new System.Drawing.Point(6, 394);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 81);
            this.label1.TabIndex = 15;
            this.label1.Text = "Weather \r\nyear\r\n(<=2016):";
            // 
            // ScenarioName_TextBox
            // 
            this.ScenarioName_TextBox.Enabled = false;
            this.ScenarioName_TextBox.Location = new System.Drawing.Point(135, 126);
            this.ScenarioName_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ScenarioName_TextBox.Name = "ScenarioName_TextBox";
            this.ScenarioName_TextBox.Size = new System.Drawing.Size(421, 35);
            this.ScenarioName_TextBox.TabIndex = 14;
            // 
            // LocalMarket_TextBox
            // 
            this.LocalMarket_TextBox.Enabled = false;
            this.LocalMarket_TextBox.Location = new System.Drawing.Point(135, 330);
            this.LocalMarket_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LocalMarket_TextBox.Name = "LocalMarket_TextBox";
            this.LocalMarket_TextBox.Size = new System.Drawing.Size(421, 35);
            this.LocalMarket_TextBox.TabIndex = 12;
            // 
            // LocalMarket
            // 
            this.LocalMarket.AutoSize = true;
            this.LocalMarket.Font = new System.Drawing.Font("Arial", 12F);
            this.LocalMarket.Location = new System.Drawing.Point(6, 331);
            this.LocalMarket.Name = "LocalMarket";
            this.LocalMarket.Size = new System.Drawing.Size(91, 54);
            this.LocalMarket.TabIndex = 11;
            this.LocalMarket.Text = "Local \r\nMarket:";
            // 
            // EndDate
            // 
            this.EndDate.AutoSize = true;
            this.EndDate.Font = new System.Drawing.Font("Arial", 12F);
            this.EndDate.Location = new System.Drawing.Point(6, 266);
            this.EndDate.Name = "EndDate";
            this.EndDate.Size = new System.Drawing.Size(117, 27);
            this.EndDate.TabIndex = 10;
            this.EndDate.Text = "End date:";
            // 
            // StartDate
            // 
            this.StartDate.AutoSize = true;
            this.StartDate.Font = new System.Drawing.Font("Arial", 12F);
            this.StartDate.Location = new System.Drawing.Point(6, 199);
            this.StartDate.Name = "StartDate";
            this.StartDate.Size = new System.Drawing.Size(124, 27);
            this.StartDate.TabIndex = 8;
            this.StartDate.Text = "Start date:";
            // 
            // AssetsBox
            // 
            this.AssetsBox.Controls.Add(this.CancelNewAssets);
            this.AssetsBox.Controls.Add(this.Asset_ID_ComboBox);
            this.AssetsBox.Controls.Add(this.NewAssets);
            this.AssetsBox.Controls.Add(this.AdditionalAssets_ListBox);
            this.AssetsBox.Controls.Add(this.AdditionalAssets);
            this.AssetsBox.Controls.Add(this.Asset_ID);
            this.AssetsBox.Location = new System.Drawing.Point(1213, 43);
            this.AssetsBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AssetsBox.Name = "AssetsBox";
            this.AssetsBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AssetsBox.Size = new System.Drawing.Size(459, 578);
            this.AssetsBox.TabIndex = 6;
            this.AssetsBox.TabStop = false;
            this.AssetsBox.Text = "Assets";
            // 
            // CancelNewAssets
            // 
            this.CancelNewAssets.BackColor = System.Drawing.Color.PaleVioletRed;
            this.CancelNewAssets.Enabled = false;
            this.CancelNewAssets.Font = new System.Drawing.Font("Arial", 12F);
            this.CancelNewAssets.Location = new System.Drawing.Point(320, 76);
            this.CancelNewAssets.Margin = new System.Windows.Forms.Padding(6);
            this.CancelNewAssets.Name = "CancelNewAssets";
            this.CancelNewAssets.Size = new System.Drawing.Size(110, 45);
            this.CancelNewAssets.TabIndex = 14;
            this.CancelNewAssets.Text = "Cancel";
            this.CancelNewAssets.UseVisualStyleBackColor = false;
            this.CancelNewAssets.Click += new System.EventHandler(this.CancelNewMarket_Click);
            // 
            // Asset_ID_ComboBox
            // 
            this.Asset_ID_ComboBox.Enabled = false;
            this.Asset_ID_ComboBox.Font = new System.Drawing.Font("Arial", 12F);
            this.Asset_ID_ComboBox.FormattingEnabled = true;
            this.Asset_ID_ComboBox.Location = new System.Drawing.Point(144, 60);
            this.Asset_ID_ComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Asset_ID_ComboBox.Name = "Asset_ID_ComboBox";
            this.Asset_ID_ComboBox.Size = new System.Drawing.Size(167, 35);
            this.Asset_ID_ComboBox.TabIndex = 13;
            // 
            // NewAssets
            // 
            this.NewAssets.BackColor = System.Drawing.Color.SkyBlue;
            this.NewAssets.Enabled = false;
            this.NewAssets.Font = new System.Drawing.Font("Arial", 12F);
            this.NewAssets.Location = new System.Drawing.Point(320, 28);
            this.NewAssets.Margin = new System.Windows.Forms.Padding(6);
            this.NewAssets.Name = "NewAssets";
            this.NewAssets.Size = new System.Drawing.Size(110, 44);
            this.NewAssets.TabIndex = 12;
            this.NewAssets.Text = "New";
            this.NewAssets.UseVisualStyleBackColor = false;
            this.NewAssets.Click += new System.EventHandler(this.NewAssets_Click);
            // 
            // AdditionalAssets_ListBox
            // 
            this.AdditionalAssets_ListBox.Enabled = false;
            this.AdditionalAssets_ListBox.FormattingEnabled = true;
            this.AdditionalAssets_ListBox.ItemHeight = 27;
            this.AdditionalAssets_ListBox.Location = new System.Drawing.Point(144, 164);
            this.AdditionalAssets_ListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AdditionalAssets_ListBox.Name = "AdditionalAssets_ListBox";
            this.AdditionalAssets_ListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.AdditionalAssets_ListBox.Size = new System.Drawing.Size(286, 382);
            this.AdditionalAssets_ListBox.TabIndex = 8;
            // 
            // AdditionalAssets
            // 
            this.AdditionalAssets.AutoSize = true;
            this.AdditionalAssets.Font = new System.Drawing.Font("Arial", 12F);
            this.AdditionalAssets.Location = new System.Drawing.Point(14, 164);
            this.AdditionalAssets.Name = "AdditionalAssets";
            this.AdditionalAssets.Size = new System.Drawing.Size(127, 81);
            this.AdditionalAssets.TabIndex = 9;
            this.AdditionalAssets.Text = "Additional\r\nassets\r\n(MaStRId):";
            // 
            // Asset_ID
            // 
            this.Asset_ID.AutoSize = true;
            this.Asset_ID.Font = new System.Drawing.Font("Arial", 12F);
            this.Asset_ID.Location = new System.Drawing.Point(14, 60);
            this.Asset_ID.Name = "Asset_ID";
            this.Asset_ID.Size = new System.Drawing.Size(114, 27);
            this.Asset_ID.TabIndex = 4;
            this.Asset_ID.Text = "Asset_ID:";
            // 
            // MarketsBox
            // 
            this.MarketsBox.Controls.Add(this.CancelMarketObjects);
            this.MarketsBox.Controls.Add(this.ShowSettingsMarketObject);
            this.MarketsBox.Controls.Add(this.NewMarkets);
            this.MarketsBox.Controls.Add(this.Market_ID_ComboBox);
            this.MarketsBox.Controls.Add(this.MarketObjects);
            this.MarketsBox.Controls.Add(this.MarketObjects_ListBox);
            this.MarketsBox.Controls.Add(this.Market_ID);
            this.MarketsBox.Location = new System.Drawing.Point(582, 43);
            this.MarketsBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarketsBox.Name = "MarketsBox";
            this.MarketsBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarketsBox.Size = new System.Drawing.Size(617, 578);
            this.MarketsBox.TabIndex = 5;
            this.MarketsBox.TabStop = false;
            this.MarketsBox.Text = "Markets";
            // 
            // CancelMarketObjects
            // 
            this.CancelMarketObjects.BackColor = System.Drawing.Color.PaleVioletRed;
            this.CancelMarketObjects.Enabled = false;
            this.CancelMarketObjects.Font = new System.Drawing.Font("Arial", 12F);
            this.CancelMarketObjects.Location = new System.Drawing.Point(490, 72);
            this.CancelMarketObjects.Margin = new System.Windows.Forms.Padding(6);
            this.CancelMarketObjects.Name = "CancelMarketObjects";
            this.CancelMarketObjects.Size = new System.Drawing.Size(110, 45);
            this.CancelMarketObjects.TabIndex = 12;
            this.CancelMarketObjects.Text = "Cancel";
            this.CancelMarketObjects.UseVisualStyleBackColor = false;
            this.CancelMarketObjects.Click += new System.EventHandler(this.CancelMarketObjects_Click);
            // 
            // ShowSettingsMarketObject
            // 
            this.ShowSettingsMarketObject.BackColor = System.Drawing.Color.DarkGray;
            this.ShowSettingsMarketObject.Enabled = false;
            this.ShowSettingsMarketObject.Font = new System.Drawing.Font("Arial", 12F);
            this.ShowSettingsMarketObject.Location = new System.Drawing.Point(139, 521);
            this.ShowSettingsMarketObject.Margin = new System.Windows.Forms.Padding(6);
            this.ShowSettingsMarketObject.Name = "ShowSettingsMarketObject";
            this.ShowSettingsMarketObject.Size = new System.Drawing.Size(461, 44);
            this.ShowSettingsMarketObject.TabIndex = 11;
            this.ShowSettingsMarketObject.Text = "Show Settings";
            this.ShowSettingsMarketObject.UseVisualStyleBackColor = false;
            this.ShowSettingsMarketObject.Click += new System.EventHandler(this.ShowSettingsMarketObject_Click);
            // 
            // NewMarkets
            // 
            this.NewMarkets.BackColor = System.Drawing.Color.SkyBlue;
            this.NewMarkets.Enabled = false;
            this.NewMarkets.Font = new System.Drawing.Font("Arial", 12F);
            this.NewMarkets.Location = new System.Drawing.Point(490, 25);
            this.NewMarkets.Margin = new System.Windows.Forms.Padding(6);
            this.NewMarkets.Name = "NewMarkets";
            this.NewMarkets.Size = new System.Drawing.Size(110, 44);
            this.NewMarkets.TabIndex = 10;
            this.NewMarkets.Text = "New";
            this.NewMarkets.UseVisualStyleBackColor = false;
            this.NewMarkets.Click += new System.EventHandler(this.NewMarkets_Click);
            // 
            // Market_ID_ComboBox
            // 
            this.Market_ID_ComboBox.Enabled = false;
            this.Market_ID_ComboBox.Font = new System.Drawing.Font("Arial", 12F);
            this.Market_ID_ComboBox.FormattingEnabled = true;
            this.Market_ID_ComboBox.Location = new System.Drawing.Point(139, 49);
            this.Market_ID_ComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Market_ID_ComboBox.Name = "Market_ID_ComboBox";
            this.Market_ID_ComboBox.Size = new System.Drawing.Size(342, 35);
            this.Market_ID_ComboBox.TabIndex = 9;
            // 
            // MarketObjects
            // 
            this.MarketObjects.AutoSize = true;
            this.MarketObjects.Font = new System.Drawing.Font("Arial", 12F);
            this.MarketObjects.Location = new System.Drawing.Point(6, 121);
            this.MarketObjects.Name = "MarketObjects";
            this.MarketObjects.Size = new System.Drawing.Size(96, 54);
            this.MarketObjects.TabIndex = 6;
            this.MarketObjects.Text = "Market \r\nobjects:";
            // 
            // MarketObjects_ListBox
            // 
            this.MarketObjects_ListBox.FormattingEnabled = true;
            this.MarketObjects_ListBox.HorizontalScrollbar = true;
            this.MarketObjects_ListBox.ItemHeight = 27;
            this.MarketObjects_ListBox.Location = new System.Drawing.Point(139, 121);
            this.MarketObjects_ListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarketObjects_ListBox.Name = "MarketObjects_ListBox";
            this.MarketObjects_ListBox.Size = new System.Drawing.Size(461, 382);
            this.MarketObjects_ListBox.TabIndex = 3;
            // 
            // Market_ID
            // 
            this.Market_ID.AutoSize = true;
            this.Market_ID.Font = new System.Drawing.Font("Arial", 12F);
            this.Market_ID.Location = new System.Drawing.Point(6, 57);
            this.Market_ID.Name = "Market_ID";
            this.Market_ID.Size = new System.Drawing.Size(127, 27);
            this.Market_ID.TabIndex = 4;
            this.Market_ID.Text = "Market_ID:";
            // 
            // Total_ID
            // 
            this.Total_ID.AutoSize = true;
            this.Total_ID.Font = new System.Drawing.Font("Arial", 12F);
            this.Total_ID.Location = new System.Drawing.Point(6, 58);
            this.Total_ID.Name = "Total_ID";
            this.Total_ID.Size = new System.Drawing.Size(105, 27);
            this.Total_ID.TabIndex = 2;
            this.Total_ID.Text = "Total_ID:";
            // 
            // Scenarios_ComboBox
            // 
            this.Scenarios_ComboBox.Font = new System.Drawing.Font("Arial", 12F);
            this.Scenarios_ComboBox.FormattingEnabled = true;
            this.Scenarios_ComboBox.Location = new System.Drawing.Point(25, 96);
            this.Scenarios_ComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Scenarios_ComboBox.Name = "Scenarios_ComboBox";
            this.Scenarios_ComboBox.Size = new System.Drawing.Size(320, 35);
            this.Scenarios_ComboBox.TabIndex = 0;
            this.Scenarios_ComboBox.SelectedIndexChanged += new System.EventHandler(this.Scenarios_ComboBox_SelectedIndexChanged);
            // 
            // WeatherYear_ComboBox
            // 
            this.WeatherYear_ComboBox.Enabled = false;
            this.WeatherYear_ComboBox.Font = new System.Drawing.Font("Arial", 12F);
            this.WeatherYear_ComboBox.FormattingEnabled = true;
            this.WeatherYear_ComboBox.Location = new System.Drawing.Point(135, 394);
            this.WeatherYear_ComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.WeatherYear_ComboBox.Name = "WeatherYear_ComboBox";
            this.WeatherYear_ComboBox.Size = new System.Drawing.Size(422, 35);
            this.WeatherYear_ComboBox.TabIndex = 23;
            // 
            // StartDataYear_ComboBox
            // 
            this.StartDataYear_ComboBox.Enabled = false;
            this.StartDataYear_ComboBox.Font = new System.Drawing.Font("Arial", 12F);
            this.StartDataYear_ComboBox.FormattingEnabled = true;
            this.StartDataYear_ComboBox.Location = new System.Drawing.Point(136, 488);
            this.StartDataYear_ComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.StartDataYear_ComboBox.Name = "StartDataYear_ComboBox";
            this.StartDataYear_ComboBox.Size = new System.Drawing.Size(422, 35);
            this.StartDataYear_ComboBox.TabIndex = 24;
            // 
            // winMoz_Gui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1746, 957);
            this.Controls.Add(this.ScenarioBox);
            this.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.Name = "winMoz_Gui";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "Mozubi Scenario Builder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.winMoz_Gui_FormClosing);
            this.Load += new System.EventHandler(this.winMoz_Gui_Load);
            this.ScenarioBox.ResumeLayout(false);
            this.ScenarioBox.PerformLayout();
            this.NewScenarioBox.ResumeLayout(false);
            this.NewScenarioBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total_ID_NumericUpDown)).EndInit();
            this.AssetsBox.ResumeLayout(false);
            this.AssetsBox.PerformLayout();
            this.MarketsBox.ResumeLayout(false);
            this.MarketsBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.GroupBox ScenarioBox;
        private System.Windows.Forms.ComboBox Scenarios_ComboBox;
        private System.Windows.Forms.GroupBox NewScenarioBox;
        private System.Windows.Forms.Label Total_ID;
        private System.Windows.Forms.GroupBox AssetsBox;
        private System.Windows.Forms.Label Asset_ID;
        private System.Windows.Forms.GroupBox MarketsBox;
        private System.Windows.Forms.Label MarketObjects;
        private System.Windows.Forms.ListBox MarketObjects_ListBox;
        private System.Windows.Forms.Label Market_ID;
        private System.Windows.Forms.ListBox AdditionalAssets_ListBox;
        private System.Windows.Forms.Label AdditionalAssets;
        private System.Windows.Forms.Label EndDate;
        private System.Windows.Forms.Label StartDate;
        private System.Windows.Forms.Button AddScenario;
        private System.Windows.Forms.TextBox LocalMarket_TextBox;
        private System.Windows.Forms.Label LocalMarket;
        private System.Windows.Forms.CheckBox SaveExternalMarketResults;
        private System.Windows.Forms.CheckBox SaveMarketResults;
        private System.Windows.Forms.CheckBox SaveSchedules;
        private System.Windows.Forms.CheckBox SaveBids;
        private System.Windows.Forms.CheckBox SaveControlReserveDemand;
        private System.Windows.Forms.CheckBox SaveSumSchedules;
        private System.Windows.Forms.Button PlotAndOutputTool;
        private System.Windows.Forms.Button DeleteScenario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ScenarioName_TextBox;
        private System.Windows.Forms.Label TYNDPScenario;
        private System.Windows.Forms.ComboBox TYNDPScenario_ComboBox;
        private System.Windows.Forms.Button NewAssets;
        private System.Windows.Forms.Button NewMarkets;
        private System.Windows.Forms.ComboBox Market_ID_ComboBox;
        private System.Windows.Forms.ComboBox Asset_ID_ComboBox;
        private System.Windows.Forms.Button ShowSettingsMarketObject;
        private System.Windows.Forms.Button CancelMarketObjects;
        private System.Windows.Forms.Button CancelNewAssets;
        private System.Windows.Forms.NumericUpDown Total_ID_NumericUpDown;
        private System.Windows.Forms.DateTimePicker EndDate_DateTimePicker;
        private System.Windows.Forms.DateTimePicker StartDate_DateTimePicker;
        private System.Windows.Forms.Label StartDataYear;
        private System.Windows.Forms.ComboBox WeatherYear_ComboBox;
        private System.Windows.Forms.ComboBox StartDataYear_ComboBox;
    }
}

