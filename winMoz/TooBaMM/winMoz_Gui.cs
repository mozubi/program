﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access;
using winMoz.Simulation;
using winMoz.Data.Access.Contexts;
using winMoz.Information;
using winMoz.Markets.Elements;
using winMoz.Markets;
using winMoz.Markets.ControlReserve;

namespace winMoz_Gui
{
    public partial class winMoz_Gui : Form
    {
        winMoz_Gui_PlotAndOutput gui_PlotAndOutput;
        winMoz_Gui_MarketObjects gui_MarketObjects;
        winMoz_Gui_MarketObjectSettings gui_MarketObjectSettings;
        winMoz_Gui_NewAssets gui_NewAssets;


        List<SimObject> MarketObjectsList;
        bool IsScenarioSaved = false;
        bool IsNewMarketObjects = false;
        List<int> Assets_IDs = new List<int>();
        List<int> Markets_IDs = new List<int>();
        List<int> MaStRIds = new List<int>();

        public winMoz_Gui()
        {
            InitializeComponent();
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
            new Config();
            ScenarioData_Repository repoScenario = new ScenarioData_Repository();
            var ScenarioIds = repoScenario.Get().Select(item => item.Total_ID.ToString()).ToArray();
            this.Scenarios_ComboBox.Items.AddRange(ScenarioIds);
            DbHelper.ReCreateDb<PlotAndOutput_Variables_Console_Context>();
            DbHelper.ReCreateDb<PlotAndOutput_Variables_CSV_Context>();
            DbHelper.ReCreateDb<PlotAndOutput_Variables_Plot_Context>();
            gui_PlotAndOutput = new winMoz_Gui_PlotAndOutput();
            gui_MarketObjects = new winMoz_Gui_MarketObjects();
            gui_MarketObjectSettings = new winMoz_Gui_MarketObjectSettings();
            gui_MarketObjects.FormClosed += AddMarketObjects_Close;
            gui_NewAssets = new winMoz_Gui_NewAssets();
            gui_NewAssets.FormClosed += NewAssets_Close;
            this.Width = this.ScenarioBox.Width + 40;
            this.Height = this.ScenarioBox.Height + 60;
            this.Location = new System.Drawing.Point(10, 10);
            repoScenario.Dispose();
            this.WeatherYear_ComboBox.Items.AddRange(Enumerable.Range(1980, 37).Select(item => Convert.ToString(item)).ToArray());
            this.StartDataYear_ComboBox.Items.AddRange(Enumerable.Range(2015, 1).Select(item => Convert.ToString(item)).ToArray());
        }

        private void Start_Click(object sender, EventArgs e)
        {
            this.Hide();
            Simulator.WeatherYear.SetWeatherYear((int)Convert.ToInt64(this.WeatherYear_ComboBox.Text), this.StartDate_DateTimePicker.Value);
            Simulator.StartDataYear.SetStartDataYear((int)Convert.ToInt64(this.StartDataYear_ComboBox.Text), this.StartDate_DateTimePicker.Value);
            Simulator Sim = new Simulator((int)Convert.ToInt64(this.Total_ID_NumericUpDown.Text));
            Sim.Run();
        }

        private void Scenarios_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Scenarios_ComboBox.SelectedIndex != -1)
            {
                this.Start.Enabled = true;
                this.IsScenarioSaved = true;
                this.ScenarioName_TextBox.Enabled = false;
                this.StartDate_DateTimePicker.Enabled = false;
                this.EndDate_DateTimePicker.Enabled = false;
                this.LocalMarket_TextBox.Enabled = false;
                this.WeatherYear_ComboBox.Enabled = false;
                this.StartDataYear_ComboBox.Enabled = false;
                this.TYNDPScenario_ComboBox.Enabled = false;
                this.Market_ID_ComboBox.Enabled = false;
                this.Asset_ID_ComboBox.Enabled = false;
                this.AdditionalAssets_ListBox.SelectionMode = SelectionMode.None;
                ScenarioData_Repository repoScenario = new ScenarioData_Repository();
                var Scenario = repoScenario.GetFromID((int)Convert.ToInt64(this.Scenarios_ComboBox.Text));
                this.Total_ID_NumericUpDown.Value = (decimal)Scenario.Total_ID;
                this.ScenarioName_TextBox.Text = Scenario.ScenarioName;
                this.StartDate_DateTimePicker.Value = Scenario.SimStartTime;
                this.EndDate_DateTimePicker.Value = Scenario.SimEndTime;
                var Market_IDs = repoScenario.Get().ToList().Select(item => item.Markets_ID).GroupBy(item => item, (group, elements) => group.ToString()).ToArray();
                this.Market_ID_ComboBox.Text = Scenario.Markets_ID.ToString();
                this.Market_ID_ComboBox.Items.AddRange(Market_IDs);
                Assets_IDs = repoScenario.Get().ToList().Select(item => item.Assets_ID).GroupBy(item => item, (group, elements) => group).ToList();
                this.Asset_ID_ComboBox.Text = Scenario.Assets_ID.ToString();
                this.Asset_ID_ComboBox.Items.AddRange(Assets_IDs.Select(item => item.ToString()).ToArray());
                this.LocalMarket_TextBox.Text = Scenario.LocalMACode.ToString();
                this.WeatherYear_ComboBox.Text = Convert.ToString(Scenario.WeatherYear);
                this.StartDataYear_ComboBox.Text = Convert.ToString(Scenario.StartDataYear);
                SetTYNDP2018Scenario(Scenario.TYNDP2018Scenario);
                //Scenario.
                this.MarketObjectsList = ReadMarketObjects(Scenario.Markets_ID);
                this.MarketObjects_ListBox.Items.Clear();
                InsertMarketObjects(this.MarketObjectsList);
                this.ShowSettingsMarketObject.Enabled = true;

                this.MaStRIds = Scenario.AdditionalAssetsMaStR;
                this.AdditionalAssets_ListBox.Items.AddRange(this.MaStRIds.Select(item => item.ToString()).ToArray());
                if (Scenarios_ComboBox.Text != "100000" && Scenarios_ComboBox.Text != "") //Default-Szenario nicht löschbar
                    this.DeleteScenario.Enabled = true;
            }
            else
            {
                this.Start.Enabled = false;
                this.DeleteScenario.Enabled = false;
            }
        }


        private void SetTYNDP2018Scenario(TYNDP2018_ScenarioCalculation.ScenarioEnum tyndp2018Scenario)
        {
            switch (tyndp2018Scenario)
            {
                case TYNDP2018_ScenarioCalculation.ScenarioEnum.BEST:
                    {
                        this.TYNDPScenario_ComboBox.SelectedIndex = 0;
                        break;
                    }
                case TYNDP2018_ScenarioCalculation.ScenarioEnum.DG:
                    {
                        this.TYNDPScenario_ComboBox.SelectedIndex = 1;
                        break;
                    }
                case TYNDP2018_ScenarioCalculation.ScenarioEnum.EUCO:
                    {
                        this.TYNDPScenario_ComboBox.SelectedIndex = 2;
                        break;
                    }
                case TYNDP2018_ScenarioCalculation.ScenarioEnum.ST:
                    {
                        this.TYNDPScenario_ComboBox.SelectedIndex = 3;
                        break;
                    }
                case TYNDP2018_ScenarioCalculation.ScenarioEnum.GCA:
                    {
                        this.TYNDPScenario_ComboBox.SelectedIndex = 4;
                        break;
                    }
                default:
                    {
                        this.TYNDPScenario_ComboBox.SelectedIndex = 0;
                        break;
                    }
            }
        }

        private void SaveBids_CheckedChanged(object sender, EventArgs e)
        {
            Simulator.SaveStates.SaveBids();
        }

        private void SaveSchedules_CheckedChanged(object sender, EventArgs e)
        {
            Simulator.SaveStates.SaveSchedules();
        }

        private void SaveMarketResults_CheckedChanged(object sender, EventArgs e)
        {
            Simulator.SaveStates.SaveMarketResults();
        }

        private void SaveExternalMarketResults_CheckedChanged(object sender, EventArgs e)
        {
            Simulator.SaveStates.SaveExternalMarketResults();
        }

        private void SaveControlReserveDemand_CheckedChanged(object sender, EventArgs e)
        {
            Simulator.SaveStates.SaveControlReserveDemand();
        }

        private void SaveSumSchedules_CheckedChanged(object sender, EventArgs e)
        {
            Simulator.SaveStates.SaveSumSchedules();
        }

        private void PlotAndOutputTool_Click(object sender, EventArgs e)
        {
            gui_PlotAndOutput.ShowDialog();
        }

        private void AddScenario_Click(object sender, EventArgs e)
        {
            if (this.AddScenario.Text == "Add scenario")
            {
                this.IsScenarioSaved = false;
                this.Scenarios_ComboBox.SelectedIndex = -1;
                this.AddScenario.Text = "Save";
                this.DeleteScenario.Text = "Cancel";
                this.DeleteScenario.Enabled = true;
                this.Start.Enabled = false;
                this.PlotAndOutputTool.Enabled = false;
                this.Total_ID_NumericUpDown.Enabled = true;
                ScenarioData_Repository repo = new ScenarioData_Repository();
                var MaxTotalId = repo.Get().ToList().Select(item => item.Total_ID).Max() + 1;
                this.Total_ID_NumericUpDown.Value = (decimal)MaxTotalId;
                this.Scenarios_ComboBox.Text = MaxTotalId.ToString();
                this.Scenarios_ComboBox.Enabled = false;
                this.ScenarioName_TextBox.Enabled = true;
                this.StartDate_DateTimePicker.Enabled = true;
                this.EndDate_DateTimePicker.Enabled = true;
                this.LocalMarket_TextBox.Enabled = true;
                this.WeatherYear_ComboBox.Enabled = true;
                this.StartDataYear_ComboBox.Enabled = true;
                this.TYNDPScenario_ComboBox.Enabled = true;
                this.Market_ID_ComboBox.Enabled = true;
                this.Market_ID_ComboBox.Items.Clear();
                var Market_IDs = repo.Get().Select(item => item.Markets_ID).GroupBy(item => item, (group, elements) => group).Select(item => item.ToString()).ToArray();
                this.Market_ID_ComboBox.Items.AddRange(Market_IDs);
                this.NewMarkets.Enabled = true;
                this.Asset_ID_ComboBox.Enabled = true;
                this.Asset_ID_ComboBox.Items.Clear();
                var Assets_IDs = repo.Get().Select(item => item.Assets_ID).GroupBy(item => item, (group, elements) => group).Select(item => item.ToString()).ToArray();
                this.Asset_ID_ComboBox.Items.AddRange(Assets_IDs);
                this.NewAssets.Enabled = true;
            }
            else
            {
                try
                {
                    DateTime start = this.StartDate_DateTimePicker.Value;
                    DateTime end = this.EndDate_DateTimePicker.Value;
                    List<int> maStRIds = this.MaStRIds;
                    int total_ID = (int)this.Total_ID_NumericUpDown.Value;
                    string scenarioName = this.ScenarioName_TextBox.Text;
                    int markets_ID = (int)Convert.ToInt64(this.Market_ID_ComboBox.Text);
                    int assets_ID = (int)Convert.ToInt64(this.Asset_ID_ComboBox.Text);
                    int weatherYear = (int)Convert.ToInt64(this.WeatherYear_ComboBox.Text);
                    int startDataYear = (int)Convert.ToInt64(this.StartDataYear_ComboBox.Text);
                    MarketAreaEnum localMACode = MarketAreaEnum.FromString(this.LocalMarket_TextBox.Text);
                    TYNDP2018_ScenarioCalculation.ScenarioEnum scenario = (TYNDP2018_ScenarioCalculation.ScenarioEnum)Enum.Parse(typeof(TYNDP2018_ScenarioCalculation.ScenarioEnum), this.Scenarios_ComboBox.Text);
                    Scenario_Builder builder = new Scenario_Builder();
                    builder.BuildScenario(
                        start: this.StartDate_DateTimePicker.Value,
                        end: this.EndDate_DateTimePicker.Value,
                        maStRIds: this.MaStRIds,
                        total_ID: (int)this.Total_ID_NumericUpDown.Value,
                        scenarioName: this.ScenarioName_TextBox.Text,
                        markets_ID: (int)Convert.ToInt64(this.Market_ID_ComboBox.Text),
                        assets_ID: (int)Convert.ToInt64(this.Asset_ID_ComboBox.Text),
                        weatherYear: weatherYear,
                        startDataYear: startDataYear,
                        localMACode: MarketAreaEnum.FromString(this.LocalMarket_TextBox.Text),
                        scenario: (TYNDP2018_ScenarioCalculation.ScenarioEnum)Enum.Parse(typeof(TYNDP2018_ScenarioCalculation.ScenarioEnum), this.Scenarios_ComboBox.Text));
                    this.IsScenarioSaved = false;
                    this.AddScenario.Text = "Add scenario";
                    this.DeleteScenario.Text = "Delete scenario";
                    this.Start.Enabled = true;
                    this.PlotAndOutputTool.Enabled = true;
                    this.Scenarios_ComboBox.Enabled = true;
                    this.ScenarioName_TextBox.Enabled = false;
                    this.StartDate_DateTimePicker.Enabled = false;
                    this.EndDate_DateTimePicker.Enabled = false;
                    this.LocalMarket_TextBox.Enabled = false;
                    this.WeatherYear_ComboBox.Enabled = false;
                    this.StartDataYear_ComboBox.Enabled = false;
                    this.TYNDPScenario_ComboBox.Enabled = false;
                    this.Market_ID_ComboBox.Enabled = false;
                    this.NewMarkets.Enabled = false;
                    this.Asset_ID_ComboBox.Enabled = false;
                    this.NewAssets.Enabled = false;
                }
                catch
                {

                }
            }
        }

        private void DeleteScenario_Click(object sender, EventArgs e)
        {
            if (this.DeleteScenario.Text == "Delete scenario")
            {
                if (this.Total_ID_NumericUpDown.Value != 100000)
                {
                    this.IsScenarioSaved = false;
                    Scenario_Builder ScenarioBuilder = new Scenario_Builder();
                    ScenarioBuilder.DeleteScenario((int)this.Total_ID_NumericUpDown.Value);
                    this.DeleteScenario.Enabled = false;
                    this.Scenarios_ComboBox.SelectedIndex = -1;
                    this.Scenarios_ComboBox.Text = "";
                    this.Total_ID_NumericUpDown.Value = default;
                    this.Total_ID_NumericUpDown.Enabled = false;
                    this.ScenarioName_TextBox.Text = "";
                    this.StartDate_DateTimePicker.Value = this.StartDate_DateTimePicker.MinDate;
                    this.EndDate_DateTimePicker.Value = this.EndDate_DateTimePicker.MinDate;
                    this.LocalMarket_TextBox.Text = "";
                    this.WeatherYear_ComboBox.SelectedIndex = -1;
                    this.StartDataYear_ComboBox.SelectedIndex = -1;
                    this.TYNDPScenario_ComboBox.SelectedIndex = -1;
                    this.Market_ID_ComboBox.Text = "";
                    this.MarketObjects_ListBox.Items.Clear();
                    this.Asset_ID_ComboBox.Text = "";
                    this.AdditionalAssets_ListBox.Items.Clear();
                }
            }
            else
            {
                this.IsScenarioSaved = false;
                this.AddScenario.Text = "Add scenario";
                this.DeleteScenario.Text = "Delete scenario";
                this.DeleteScenario.Enabled = false;
                this.Start.Enabled = true;
                this.PlotAndOutputTool.Enabled = true;
                this.Scenarios_ComboBox.Enabled = true;
                this.Total_ID_NumericUpDown.Enabled = false;
                this.ScenarioName_TextBox.Enabled = false;
                this.StartDate_DateTimePicker.Enabled = false;
                this.EndDate_DateTimePicker.Enabled = false;
                this.LocalMarket_TextBox.Enabled = false;
                this.WeatherYear_ComboBox.Enabled = false;
                this.StartDataYear_ComboBox.Enabled = false;
                this.TYNDPScenario_ComboBox.Enabled = false;
                this.Market_ID_ComboBox.Enabled = false;
                this.Market_ID_ComboBox.Items.Clear();
                this.NewMarkets.Enabled = false;
                this.Asset_ID_ComboBox.Enabled = false;
                this.Asset_ID_ComboBox.Items.Clear();
                this.NewAssets.Enabled = false;
                this.Scenarios_ComboBox.SelectedIndex = -1;
                this.Scenarios_ComboBox.Text = "";
                this.Total_ID_NumericUpDown.Value = default;
                this.ScenarioName_TextBox.Text = "";
                this.StartDate_DateTimePicker.Value = this.StartDate_DateTimePicker.MinDate;
                this.EndDate_DateTimePicker.Value = this.EndDate_DateTimePicker.MinDate;
                this.LocalMarket_TextBox.Text = "";
                this.WeatherYear_ComboBox.SelectedIndex = -1;
                this.StartDataYear_ComboBox.SelectedIndex = -1;
                this.TYNDPScenario_ComboBox.SelectedIndex = -1;
                this.Market_ID_ComboBox.Text = "";
                this.MarketObjects_ListBox.Items.Clear();
                this.Asset_ID_ComboBox.Text = "";
                this.Start.Enabled = false;
            }
        }

        private void NewMarkets_Click(object sender, EventArgs e)
        {
            this.IsNewMarketObjects = true;
            this.Market_ID_ComboBox.SelectedIndex = -1;
            ScenarioData_Repository repo = new ScenarioData_Repository();
            var MaxMarketsId = repo.Get().ToList().Select(item => item.Markets_ID).Max() + 1;
            var MarketIds = repo.Get().ToList().Select(item => item.Markets_ID).ToList();

            gui_MarketObjects.MarketIds = MarketIds;
            gui_MarketObjects.Market_ID_Value = MaxMarketsId;
            this.Market_ID_ComboBox.Enabled = false;
            this.CancelMarketObjects.Enabled = true;
            try
            {
                gui_MarketObjects.StartDate = this.StartDate_DateTimePicker.Value = default;
            }
            catch
            {
                gui_MarketObjects.StartDate = default;
                gui_MarketObjects.ShowDialog();
            }
            repo.Dispose();
            gui_MarketObjects.ShowDialog();
        }

        private List<SimObject> ReadMarketObjects(int markets_ID)
        {
            List<SimObject> marketObjects = new List<SimObject>();
            using (var repo = new ScenarioData_Repository())
            {
                var markets = repo.MarketRepository.GetMarkets(markets_ID, withIntitialization: false);
                marketObjects.AddRange(markets);
                var crTenderVolumeHandlers = repo.MarketRepository.GetControlReserveTenderVolumeHandlers(markets_ID, withInitialization: false);
                marketObjects.AddRange(crTenderVolumeHandlers);
                var crActivationVolumeHandlers = repo.MarketRepository.GetControlReserveActivationVolumeHandlers(markets_ID, withInitialization: false);
                marketObjects.AddRange(crActivationVolumeHandlers);
                var IGCC_Calculations = repo.MarketRepository.GetIGCCs(markets_ID, withInitialization: false);
                marketObjects.AddRange(IGCC_Calculations);
                var ReBAP_Calculations = repo.MarketRepository.GetReBAP_Calculations(markets_ID, withInitialization: false);
                marketObjects.AddRange(ReBAP_Calculations);
                var DeviationFromRZSaldoHandlers = repo.MarketRepository.GetDeviationFromRZSaldoHandlers(markets_ID, withInitialization: false);
                marketObjects.AddRange(DeviationFromRZSaldoHandlers);
            }
            return marketObjects;
        }

        public void InsertMarketObjects(List<SimObject> marketObjects)
        {
            //Markets
            var markets = marketObjects.Where(obj => obj is Market).ToList();
            if (markets.Count() > 0) this.MarketObjects_ListBox.Items.Add("Markets");
            var marketsText = markets.Select(market => market.GetType().Name.ToString() + " ID: " + market.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(marketsText);
            //ControlReserve_Activation
            var cr_Activation = marketObjects.Where(obj => obj is ControlReserve_Activation).ToList();
            if (cr_Activation.Count() > 0) this.MarketObjects_ListBox.Items.Add("ControlReserve_Activation");
            var cr_ActivationText = cr_Activation.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(cr_ActivationText);
            //ControlReserveTenderVolumeHandler
            var crTenderVolumeHandlers = marketObjects.Where(obj => obj is ControlReserveTenderVolumeHandler).ToList();
            if (crTenderVolumeHandlers.Count() > 0) this.MarketObjects_ListBox.Items.Add("ControlReserveTenderVolumeHandler");
            var crTenderVolumeHandlersText = crTenderVolumeHandlers.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(crTenderVolumeHandlersText);
            //ControlReserveActivationVolumeHandler
            var crActivationVolumeHandlers = marketObjects.Where(obj => obj is ControlReserveActivationVolumeHandler).ToList();
            if (crActivationVolumeHandlers.Count() > 0) this.MarketObjects_ListBox.Items.Add("ControlReserveActivationVolumeHandler");
            var crActivationVolumeHandlersText = crActivationVolumeHandlers.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(crActivationVolumeHandlersText);
            //IGCC_Calculation
            var IGCC_Calculations = marketObjects.Where(obj => obj is IGCC_Calculation).ToList();
            if (IGCC_Calculations.Count() > 0) this.MarketObjects_ListBox.Items.Add("IGCC_Calculation");
            var IGCC_CalculationsText = IGCC_Calculations.Select(igcc => igcc.GetType().Name.ToString() + " ID: " + igcc.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(IGCC_CalculationsText);
            //ReBAP_Calculation
            var ReBAP_Calculations = marketObjects.Where(obj => obj is ReBAP_Calculation).ToList();
            if (ReBAP_Calculations.Count() > 0) this.MarketObjects_ListBox.Items.Add("ReBAP_Calculation");
            var ReBAP_CalculationsText = ReBAP_Calculations.Select(reBAP => reBAP.GetType().Name.ToString() + " ID: " + reBAP.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(ReBAP_CalculationsText);
            //DeviationFromRZSaldo_Handler
            var DeviationFromRZSaldoHandlers = marketObjects.Where(obj => obj is DeviationFromRZSaldo_Handler).ToList();
            if (DeviationFromRZSaldoHandlers.Count() > 0) this.MarketObjects_ListBox.Items.Add("DeviationFromRZSaldoHandler");
            var DeviationFromRZSaldoHandlersText = DeviationFromRZSaldoHandlers.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(DeviationFromRZSaldoHandlersText);
        }

        private void NewAssets_Click(object sender, EventArgs e)
        {
            this.NewAssets.Enabled = false;
            this.CancelNewAssets.Enabled = true;
            this.Market_ID_ComboBox.SelectedIndex = -1;
            ScenarioData_Repository repo = new ScenarioData_Repository();
            var MaxAssetsId = repo.Get().ToList().Select(item => item.Assets_ID).Max() + 1;
            this.Asset_ID_ComboBox.Text = MaxAssetsId.ToString();
            gui_NewAssets.Assets_ID_Value = MaxAssetsId;
            gui_NewAssets.Assets_IDs = this.Assets_IDs;
            gui_NewAssets.MaStRIds = this.MaStRIds;
            repo.Dispose();
            gui_NewAssets.ShowDialog();
        }

        private void AddMarketObjects_Close(object sender, EventArgs e)
        {
            if (gui_MarketObjects.marketObjectsHaveToBeAdded)
                this.Market_ID_ComboBox.Text = Convert.ToString(gui_MarketObjects.Market_ID_Value); // .Market_ID_Value.ToString();

            this.MarketObjectsList = ReadMarketObjects((int)Convert.ToInt64(this.Market_ID_ComboBox.Text));
            this.MarketObjects_ListBox.Items.Clear();
            InsertMarketObjects(this.MarketObjectsList);
        }
        

        private void winMoz_Gui_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.IsScenarioSaved && this.IsNewMarketObjects)
            {
                //MarketIZESRepository repo = new MarketIZESRepository();
                using (var repo = new ScenarioData_Repository())
                {
                    repo.MarketRepository.DeleteAllFromScenarioID((int)Convert.ToInt64(this.Market_ID_ComboBox.Text));
                }
            }
        }

        private void CancelMarketObjects_Click(object sender, EventArgs e)
        {
            this.Market_ID_ComboBox.Enabled = true;
            //MarketIZESRepository repo = new MarketIZESRepository();
            using (var repo = new ScenarioData_Repository())
            {
                repo.MarketRepository.DeleteAllFromScenarioID((int)Convert.ToInt64(this.Market_ID_ComboBox.Text));
            }
            this.NewMarkets.Enabled = true;
            gui_MarketObjects.MarketObjectsList.Clear();
            this.MarketObjects_ListBox.Items.Clear();
            this.Market_ID_ComboBox.Text = "";
            this.IsNewMarketObjects = false;
        }

        private void ShowSettingsMarketObject_Click(object sender, EventArgs e)
        {
            try
            {
                //MarketIZESRepository repo = new MarketIZESRepository();
                (string Type, int Id) = GetIdAndType(this.MarketObjects_ListBox.SelectedItem.ToString());
                var objectForSettings = this.MarketObjectsList.SingleOrDefault(obj => obj.GetType().Name == Type && obj.Id == Id);
                var repo = new ScenarioData_Repository();
                var initializedObjectForSettings = repo.MarketRepository.GetIntitializedVersionOf(objectForSettings);
                repo.Dispose();
                gui_MarketObjectSettings.MarketObjectForSettings = initializedObjectForSettings;
                gui_MarketObjectSettings.ShowDialog();
            }
            catch
            {

            }
        }

        private (string Type, int Id) GetIdAndType(string listBoxLine)
        {
            var elements = listBoxLine.Split(' ', ':');
            var Type = elements[0];
            var Id = (int)Convert.ToInt64(elements[3]);
            return (Type, Id);
        }



        private void DeleteAdditionalAsset_Click(object sender, EventArgs e)
        {
            this.MaStRIds.RemoveAt(this.AdditionalAssets_ListBox.SelectedIndex);
            this.AdditionalAssets_ListBox.Items.RemoveAt(this.AdditionalAssets_ListBox.SelectedIndex);
        }

        private void CancelNewMarket_Click(object sender, EventArgs e)
        {
            this.CancelNewAssets.Enabled = false;
            this.NewAssets.Enabled = true;
            this.Asset_ID_ComboBox.SelectedIndex = -1;
            this.Asset_ID_ComboBox.Text = "";
            this.AdditionalAssets_ListBox.Items.Clear();
        }

        private void NewAssets_Close(object sender, EventArgs e)
        {
            if (gui_NewAssets.HasToBeAdded)
            {
                this.MaStRIds = gui_NewAssets.MaStRIds;
                foreach (int id in this.MaStRIds) this.AdditionalAssets_ListBox.Items.Add(id);
            }
        }
    }
}
