﻿namespace winMoz_Gui
{
    partial class winMoz_Gui_AddMaStRId
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.MaStRId_TextBox = new System.Windows.Forms.TextBox();
            this.MaStRId = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Add
            // 
            this.Add.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Add.Font = new System.Drawing.Font("Arial", 12F);
            this.Add.Location = new System.Drawing.Point(48, 106);
            this.Add.Margin = new System.Windows.Forms.Padding(6);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(237, 52);
            this.Add.TabIndex = 13;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = false;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Delete
            // 
            this.Delete.BackColor = System.Drawing.Color.LightCoral;
            this.Delete.Font = new System.Drawing.Font("Arial", 12F);
            this.Delete.Location = new System.Drawing.Point(297, 106);
            this.Delete.Margin = new System.Windows.Forms.Padding(6);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(227, 52);
            this.Delete.TabIndex = 14;
            this.Delete.Text = "Cancel";
            this.Delete.UseVisualStyleBackColor = false;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // MaStRId_TextBox
            // 
            this.MaStRId_TextBox.Location = new System.Drawing.Point(197, 47);
            this.MaStRId_TextBox.Name = "MaStRId_TextBox";
            this.MaStRId_TextBox.Size = new System.Drawing.Size(327, 33);
            this.MaStRId_TextBox.TabIndex = 19;
            this.MaStRId_TextBox.TextChanged += new System.EventHandler(this.MaStRId_TextBox_TextChanged);
            // 
            // MaStRId
            // 
            this.MaStRId.AutoSize = true;
            this.MaStRId.Location = new System.Drawing.Point(53, 50);
            this.MaStRId.Name = "MaStRId";
            this.MaStRId.Size = new System.Drawing.Size(104, 25);
            this.MaStRId.TabIndex = 20;
            this.MaStRId.Text = "MaStRId:";
            // 
            // winMoz_Gui_AddMaStRId
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(581, 206);
            this.Controls.Add(this.MaStRId);
            this.Controls.Add(this.MaStRId_TextBox);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Add);
            this.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.Name = "winMoz_Gui_AddMaStRId";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "Mozubi Scenario Builder";
            this.Load += new System.EventHandler(this.winMoz_Gui_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.TextBox MaStRId_TextBox;
        private System.Windows.Forms.Label MaStRId;
    }
}

