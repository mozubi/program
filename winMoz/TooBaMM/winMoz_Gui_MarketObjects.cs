﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;
using System.Diagnostics;
using winMoz.Simulation;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.DTO;
using winMoz.Markets;
using winMoz.Markets.ControlReserve;
using winMoz.Information;

namespace winMoz_Gui
{
    public partial class winMoz_Gui_MarketObjects : Form
    {
        winMoz_Gui_NewMarketObject gui;

        public DateTime StartDate;
        public int Market_ID_Value = 0;

        public List<SimObject> MarketObjectsList = new List<SimObject>();

        public List<int> MarketIds;


        public bool marketObjectsHaveToBeAdded = false;

        public winMoz_Gui_MarketObjects()
        {
            InitializeComponent();
            gui = new winMoz_Gui_NewMarketObject();
            gui.FormClosing += Gui_Close;
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
            this.Market_ID_TextBox.Text = Market_ID_Value.ToString();
            this.MarketObjects_ListBox.Items.Clear();
            InsertMarketObjects(this.MarketObjectsList);
        }

        public void InsertMarketObjects(List<SimObject> marketObjects)
        {
            //Markets
            var markets = marketObjects.Where(obj => obj is Market).ToList();
            if (markets.Count() > 0) this.MarketObjects_ListBox.Items.Add("Markets");
            var marketsText = markets.Select(market => market.GetType().Name.ToString() + " ID: " + market.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(marketsText);
            //ControlReserve_Activation
            var cr_Activation = marketObjects.Where(obj => obj is ControlReserve_Activation).ToList();
            if (cr_Activation.Count() > 0) this.MarketObjects_ListBox.Items.Add("ControlReserve_Activation");
            var cr_ActivationText = cr_Activation.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(cr_ActivationText);
            //ControlReserveTenderVolumeHandler
            var crTenderVolumeHandlers = marketObjects.Where(obj => obj is ControlReserveTenderVolumeHandler).ToList();
            if (crTenderVolumeHandlers.Count() > 0) this.MarketObjects_ListBox.Items.Add("ControlReserveTenderVolumeHandler");
            var crTenderVolumeHandlersText = crTenderVolumeHandlers.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(crTenderVolumeHandlersText);
            //ControlReserveActivationVolumeHandler
            var crActivationVolumeHandlers = marketObjects.Where(obj => obj is ControlReserveActivationVolumeHandler).ToList();
            if (crActivationVolumeHandlers.Count() > 0) this.MarketObjects_ListBox.Items.Add("ControlReserveActivationVolumeHandler");
            var crActivationVolumeHandlersText = crActivationVolumeHandlers.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(crActivationVolumeHandlersText);
            //IGCC_Calculation
            var IGCC_Calculations = marketObjects.Where(obj => obj is IGCC_Calculation).ToList();
            if (IGCC_Calculations.Count() > 0) this.MarketObjects_ListBox.Items.Add("IGCC_Calculation");
            var IGCC_CalculationsText = IGCC_Calculations.Select(igcc => igcc.GetType().Name.ToString() + " ID: " + igcc.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(IGCC_CalculationsText);
            //ReBAP_Calculation
            var ReBAP_Calculations = marketObjects.Where(obj => obj is ReBAP_Calculation).ToList();
            if (ReBAP_Calculations.Count() > 0) this.MarketObjects_ListBox.Items.Add("ReBAP_Calculation");
            var ReBAP_CalculationsText = ReBAP_Calculations.Select(reBAP => reBAP.GetType().Name.ToString() + " ID: " + reBAP.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(ReBAP_CalculationsText);
            //DeviationFromRZSaldo_Handler
            var DeviationFromRZSaldoHandlers = marketObjects.Where(obj => obj is DeviationFromRZSaldo_Handler).ToList();
            if (DeviationFromRZSaldoHandlers.Count() > 0) this.MarketObjects_ListBox.Items.Add("DeviationFromRZSaldoHandler");
            var DeviationFromRZSaldoHandlersText = DeviationFromRZSaldoHandlers.Select(handler => handler.GetType().Name.ToString() + " ID: " + handler.Id).ToArray();
            this.MarketObjects_ListBox.Items.AddRange(DeviationFromRZSaldoHandlersText);
        }

        private void Gui_Close(object sender, EventArgs e)
        {
            if (gui.HasToBeAdded)
            {
                this.MarketObjectsList.Add(gui.MarketObjectToAdd);
                this.MarketObjects_ListBox.Items.Clear();
                InsertMarketObjects(MarketObjectsList.Select(item => item).ToList());
            }
            if (this.MarketObjects_ListBox.Items.Count > 0)
                this.Market_ID_TextBox.Enabled = false;
            else
                this.Market_ID_TextBox.Enabled = true;
            this.Close();
        }

        private void AddMarketObject_Click(object sender, EventArgs e)
        {
            gui.MarketObjectsList = this.MarketObjectsList;
            gui.MarketsId = (int)Convert.ToInt64(this.Market_ID_TextBox.Text);
            gui.StartDate = StartDate;
            gui.ShowDialog();
        }

        private void Market_ID_TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int MarketsIdNew = (int)Convert.ToInt64(this.Market_ID_TextBox.Text);
                if (!MarketIds.Contains(MarketsIdNew))
                {
                    this.MarketObjects_ListBox.Enabled = true;
                    this.MarketObjects_ListBox.SelectionMode = SelectionMode.One;
                    this.AddMarketObject.Enabled = true;
                    this.DeleteMarketObject.Enabled = true;
                }
                else
                {
                    this.MarketObjects_ListBox.Enabled = false;
                    this.MarketObjects_ListBox.SelectionMode = SelectionMode.None;
                    this.AddMarketObject.Enabled = false;
                    this.DeleteMarketObject.Enabled = false;
                }
            }
            catch
            {
                this.MarketObjects_ListBox.Enabled = false;
                this.MarketObjects_ListBox.SelectionMode = SelectionMode.None;
                this.AddMarketObject.Enabled = false;
                this.DeleteMarketObject.Enabled = false;
            }
        }

        private void DeleteMarketObject_Click(object sender, EventArgs e)
        {
            try
            {
                (string Type, int Id) = GetIdAndType(this.MarketObjects_ListBox.SelectedItem.ToString());
                var objectToDelete = this.MarketObjectsList.SingleOrDefault(obj => obj.GetType().Name == Type && obj.Id == Id);
                using (var repo = new ScenarioData_Repository())
                {
                    repo.MarketRepository.Delete(objectToDelete);
                }
                this.MarketObjectsList.Remove(objectToDelete);
                this.MarketObjects_ListBox.Items.RemoveAt(this.MarketObjects_ListBox.SelectedIndex);
            }
            catch
            {

            }
            this.MarketObjects_ListBox.Items.Clear();
            InsertMarketObjects(MarketObjectsList);
            if (this.MarketObjects_ListBox.Items.Count > 0)
                this.Market_ID_TextBox.Enabled = false;
            else
                this.Market_ID_TextBox.Enabled = true;
        }

        private (string Type, int Id) GetIdAndType(string listBoxLine)
        {
            var elements = listBoxLine.Split(' ', ':');
            var Type = elements[0];
            var Id = (int)Convert.ToInt64(elements[3]);
            return (Type, Id);
        }

        private void Add_Click(object sender, EventArgs e)
        {
            this.marketObjectsHaveToBeAdded = true;
            Market_ID_Value = (int)Convert.ToInt64(Market_ID_TextBox.Text);
            this.Close();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            using (var repo = new ScenarioData_Repository())
            {
                repo.MarketRepository.DeleteAllFromScenarioID((int)Convert.ToInt64(Market_ID_TextBox.Text));
            }
            this.marketObjectsHaveToBeAdded = false;
            this.Close();
        }
    }
}
