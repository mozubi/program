﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;
using System.Diagnostics;

namespace winMoz_Gui
{
    public partial class winMoz_Gui_PlotAndOutput_Elements : Form
    {
        public Dictionary<string, (Label Label, Control Element)> FormElements = new Dictionary<string, (Label, Control)>();

        public string ChoosedElement => this.ElementType_ComboBox.SelectedItem != null ? this.ElementType_ComboBox.SelectedItem.ToString() : "";

        public DateTime StartDate => Convert.ToDateTime(this.Start_TextBox.Text);
        public DateTime EndDate => Convert.ToDateTime(this.End_TextBox.Text);

        public bool IsSelected = false;

        public winMoz_Gui_PlotAndOutput_Elements()
        {
            InitializeComponent();
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
            IsSelected = false;
            this.Height = 400;
        }

        private void ElementType_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.ElementType_ComboBox.SelectedItem.ToString())
            {
                case "Bids":
                    {
                        this.TimesBox.Visible = true;
                        this.ElementBox.Visible = true;
                        CreateBidElements();
                        break;
                    }
                case "MeritOrders":
                    {
                        this.TimesBox.Visible = true;
                        this.ElementBox.Visible = true;
                        CreateMeritOrderElements();
                        break;
                    }
                case "CoupledMeritOrders":
                    {
                        this.TimesBox.Visible = true;
                        this.ElementBox.Visible = true;
                        CreateCoupledMeritOrderElements();
                        break;
                    }
                case "Schedules":
                    {
                        this.TimesBox.Visible = true;
                        this.ElementBox.Visible = true;
                        CreateScheduleElements();
                        break;
                    }
                case "MarketResults":
                    {
                        this.TimesBox.Visible = true;
                        this.ElementBox.Visible = true;
                        CreateMarketResultElements();
                        break;
                    }
            }
        }

        private void CreateBidElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("Market", (CreateElementLabel("Market:", 0), CreateElementTextBox(0)));
            FormElements.Add("WithStep", (CreateElementLabel("WithStep:", 1), CreateElementCheckBox(1)));
            FormElements.Add("WithBlock", (CreateElementLabel("WithBlock:", 2), CreateElementCheckBox(2)));
            FormElements.Add("Plant_Type", (CreateElementLabel("Plant_Type:", 3), CreateElementTextBox(3)));
            FormElements.Add("SubType", (CreateElementLabel("SubType:", 4), CreateElementTextBox(4)));
            FormElements.Add("Plant_Id", (CreateElementLabel("Plant_Id:", 5), CreateElementNumericUpDown(5)));
            FormElements.Add("ControlReserveType", (CreateElementLabel("ControlReserveType:", 6), CreateElementTextBox(6)));
            FormElements.Add("DifferByPlantTypeAndSubType", (CreateElementLabel("DifferByPlantTypeAndSubType:", 7), CreateElementRadioButton(7)));
            FormElements.Add("OnlyOnePlantType", (CreateElementLabel("OnlyOnePlantType:", 8), CreateElementRadioButton(8)));
            FormElements.Add("OnlyOnePlant", (CreateElementLabel("OnlyOnePlant:", 9), CreateElementRadioButton(9)));
            FormElements.Add("OnlyOneControlReserveType", (CreateElementLabel("OnlyOneControlReserveType:", 10), CreateElementRadioButton(10)));
            FormElements.Add("OnlyCapacityBids", (CreateElementLabel("OnlyCapacityBids:", 11), CreateElementRadioButton(11)));
            this.ElementBox.Height = 13 * 30;
        }

        private void CreateMeritOrderElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("SimEvent", (CreateElementLabel("SimEvent:", 0), CreateElementTextBox(0)));
            FormElements.Add("ControlReserveType", (CreateElementLabel("ControlReserveType:", 1), CreateElementTextBox(1)));
            FormElements.Add("OnlyOneControlReserveType", (CreateElementLabel("OnlyOneControlReserveType:", 2), CreateElementRadioButton(2)));
            this.ElementBox.Height = 4 * 30;
        }

        private void CreateCoupledMeritOrderElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("MarketArea", (CreateElementLabel("Market:", 0), CreateElementTextBox(0)));
            FormElements.Add("MarketAreaSpecified", (CreateElementLabel("MarketAreaSpecified:", 1), CreateElementCheckBox(1)));
            this.ElementBox.Height = 4 * 30;
        }

        private void CreateScheduleElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("Plant_Type", (CreateElementLabel("Plant_Type:", 0), CreateElementTextBox(0)));
            FormElements.Add("Plant_Id", (CreateElementLabel("Plant_Id:", 1), CreateElementNumericUpDown(1)));
            FormElements.Add("SubType", (CreateElementLabel("SubType:", 2), CreateElementTextBox(2)));
            FormElements.Add("DifferByPlantTypeAndSubType", (CreateElementLabel("DifferByPlantTypeAndSubType:", 3), CreateElementRadioButton(3)));
            FormElements.Add("OnlyOnePlantType", (CreateElementLabel("OnlyOnePlantType:", 4), CreateElementRadioButton(4)));
            FormElements.Add("OnlyOnePlant", (CreateElementLabel("OnlyOnePlant:", 5), CreateElementRadioButton(5)));
            FormElements.Add("WithPrices", (CreateElementLabel("WithPrices:", 6), CreateElementCheckBox(6)));
            FormElements.Add("IsOutputSimTimeSet", (CreateElementLabel("IsOutputSimTimeSet:", 7), CreateElementRadioButton(7)));
            FormElements.Add("OutputSimTime", (CreateElementLabel("OutputSimTime:", 8), CreateElementTextBox(8)));
            this.ElementBox.Height = 10 * 30;
        }

        private void CreateMarketResultElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("MarketResultType", (CreateElementLabel("MarketResultType:", 0), CreateElementTextBox(0)));
            FormElements.Add("IsMarketTypeSpecified", (CreateElementLabel("IsMarketTypeSpecified:", 1), CreateElementCheckBox(1)));
            FormElements.Add("DifferByMarketType", (CreateElementLabel("DifferByMarketType:", 2), CreateElementCheckBox(2)));
            FormElements.Add("VolumeVersusPrice", (CreateElementLabel("VolumeVersusPrice:", 3), CreateElementRadioButton(3)));
            FormElements.Add("TimeVersusVolume", (CreateElementLabel("TimeVersusVolume:", 4), CreateElementRadioButton(4)));
            FormElements.Add("TimeVersusPrice", (CreateElementLabel("TimeVersusPrice:", 5), CreateElementRadioButton(5)));
            FormElements.Add("TimeVersusVolumeAndPrice", (CreateElementLabel("TimeVersusVolumeAndPrice:", 6), CreateElementRadioButton(6)));
            this.ElementBox.Height = 8 * 30;
        }

        private Label CreateElementLabel(string text, int numberYPos)
        {
            Label lbl = new Label();
            this.ElementBox.Controls.Add(lbl);
            lbl.Location = new Point(x: 22, y: 30 + numberYPos * 30);
            lbl.Text = text;
            lbl.Width = 264;
            return lbl;
        }

        private TextBox CreateElementTextBox(int numberYPos)
        {
            TextBox tb = new TextBox();
            this.ElementBox.Controls.Add(tb);
            tb.Location = new Point(x: 322, y: 30 + numberYPos * 30);
            tb.Size = new Size(width: 377, height: 35);
            return tb;
        }

        private CheckBox CreateElementCheckBox(int numberYPos)
        {
            CheckBox chb = new CheckBox();
            this.ElementBox.Controls.Add(chb);
            chb.Location = new Point(x: 322, y: 30 + numberYPos * 30);
            chb.Text = "";
            return chb;
        }

        private NumericUpDown CreateElementNumericUpDown(int numberYPos)
        {
            NumericUpDown nud = new NumericUpDown();
            this.ElementBox.Controls.Add(nud);
            nud.Location = new Point(x: 322, y: 30 + numberYPos * 30);
            nud.Minimum = 1;
            return nud;
        }

        private RadioButton CreateElementRadioButton(int numberYPos)
        {
            RadioButton rb = new RadioButton();
            this.ElementBox.Controls.Add(rb);
            rb.Location = new Point(x: 322, y: 30 + numberYPos * 30);
            rb.Text = "";
            return rb;
        }

        private void Select_Click(object sender, EventArgs e)
        {
            IsSelected = true;
            this.Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            IsSelected = false;
            this.Close();
        }
    }
}
