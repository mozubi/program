﻿namespace winMoz_Gui
{
    partial class winMoz_Gui_NewAssets
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancel = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.AssetsBox = new System.Windows.Forms.GroupBox();
            this.Asset_ID = new System.Windows.Forms.Label();
            this.AdditionalAssets = new System.Windows.Forms.Label();
            this.AdditionalAssets_ListBox = new System.Windows.Forms.ListBox();
            this.AddAdditionalAsset = new System.Windows.Forms.Button();
            this.DeleteAdditionalAsset = new System.Windows.Forms.Button();
            this.Assets_ID_TextBox = new System.Windows.Forms.TextBox();
            this.AssetsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.BackColor = System.Drawing.Color.PaleVioletRed;
            this.Cancel.Font = new System.Drawing.Font("Arial", 12F);
            this.Cancel.Location = new System.Drawing.Point(279, 28);
            this.Cancel.Margin = new System.Windows.Forms.Padding(6);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(262, 43);
            this.Cancel.TabIndex = 23;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = false;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Save
            // 
            this.Save.BackColor = System.Drawing.Color.SkyBlue;
            this.Save.Enabled = false;
            this.Save.Font = new System.Drawing.Font("Arial", 12F);
            this.Save.Location = new System.Drawing.Point(21, 27);
            this.Save.Margin = new System.Windows.Forms.Padding(6);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(257, 44);
            this.Save.TabIndex = 24;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = false;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // AssetsBox
            // 
            this.AssetsBox.Controls.Add(this.Assets_ID_TextBox);
            this.AssetsBox.Controls.Add(this.Asset_ID);
            this.AssetsBox.Controls.Add(this.DeleteAdditionalAsset);
            this.AssetsBox.Controls.Add(this.AdditionalAssets);
            this.AssetsBox.Controls.Add(this.AddAdditionalAsset);
            this.AssetsBox.Controls.Add(this.AdditionalAssets_ListBox);
            this.AssetsBox.Location = new System.Drawing.Point(21, 89);
            this.AssetsBox.Name = "AssetsBox";
            this.AssetsBox.Size = new System.Drawing.Size(520, 522);
            this.AssetsBox.TabIndex = 25;
            this.AssetsBox.TabStop = false;
            this.AssetsBox.Text = "Assets";
            // 
            // Asset_ID
            // 
            this.Asset_ID.AutoSize = true;
            this.Asset_ID.Font = new System.Drawing.Font("Arial", 12F);
            this.Asset_ID.Location = new System.Drawing.Point(24, 74);
            this.Asset_ID.Name = "Asset_ID";
            this.Asset_ID.Size = new System.Drawing.Size(114, 27);
            this.Asset_ID.TabIndex = 15;
            this.Asset_ID.Text = "Asset_ID:";
            // 
            // AdditionalAssets
            // 
            this.AdditionalAssets.AutoSize = true;
            this.AdditionalAssets.Font = new System.Drawing.Font("Arial", 12F);
            this.AdditionalAssets.Location = new System.Drawing.Point(24, 132);
            this.AdditionalAssets.Name = "AdditionalAssets";
            this.AdditionalAssets.Size = new System.Drawing.Size(127, 81);
            this.AdditionalAssets.TabIndex = 21;
            this.AdditionalAssets.Text = "Additional\r\nassets\r\n(MaStRId):";
            // 
            // AdditionalAssets_ListBox
            // 
            this.AdditionalAssets_ListBox.FormattingEnabled = true;
            this.AdditionalAssets_ListBox.ItemHeight = 25;
            this.AdditionalAssets_ListBox.Location = new System.Drawing.Point(167, 132);
            this.AdditionalAssets_ListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AdditionalAssets_ListBox.Name = "AdditionalAssets_ListBox";
            this.AdditionalAssets_ListBox.Size = new System.Drawing.Size(286, 279);
            this.AdditionalAssets_ListBox.TabIndex = 17;
            // 
            // AddAdditionalAsset
            // 
            this.AddAdditionalAsset.BackColor = System.Drawing.Color.SkyBlue;
            this.AddAdditionalAsset.Enabled = false;
            this.AddAdditionalAsset.Font = new System.Drawing.Font("Arial", 12F);
            this.AddAdditionalAsset.Location = new System.Drawing.Point(167, 445);
            this.AddAdditionalAsset.Margin = new System.Windows.Forms.Padding(6);
            this.AddAdditionalAsset.Name = "AddAdditionalAsset";
            this.AddAdditionalAsset.Size = new System.Drawing.Size(143, 44);
            this.AddAdditionalAsset.TabIndex = 20;
            this.AddAdditionalAsset.Text = "Add";
            this.AddAdditionalAsset.UseVisualStyleBackColor = false;
            this.AddAdditionalAsset.Click += new System.EventHandler(this.AddAdditionalAsset_Click);
            // 
            // DeleteAdditionalAsset
            // 
            this.DeleteAdditionalAsset.BackColor = System.Drawing.Color.PaleVioletRed;
            this.DeleteAdditionalAsset.Font = new System.Drawing.Font("Arial", 12F);
            this.DeleteAdditionalAsset.Location = new System.Drawing.Point(315, 446);
            this.DeleteAdditionalAsset.Margin = new System.Windows.Forms.Padding(6);
            this.DeleteAdditionalAsset.Name = "DeleteAdditionalAsset";
            this.DeleteAdditionalAsset.Size = new System.Drawing.Size(142, 43);
            this.DeleteAdditionalAsset.TabIndex = 19;
            this.DeleteAdditionalAsset.Text = "Delete";
            this.DeleteAdditionalAsset.UseVisualStyleBackColor = false;
            // 
            // Assets_ID_TextBox
            // 
            this.Assets_ID_TextBox.Location = new System.Drawing.Point(163, 72);
            this.Assets_ID_TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Assets_ID_TextBox.Name = "Assets_ID_TextBox";
            this.Assets_ID_TextBox.Size = new System.Drawing.Size(290, 33);
            this.Assets_ID_TextBox.TabIndex = 22;
            this.Assets_ID_TextBox.TextChanged += new System.EventHandler(this.Assets_ID_TextBox_TextChanged);
            // 
            // winMoz_Gui_NewAssets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(558, 626);
            this.Controls.Add(this.AssetsBox);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Save);
            this.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.Name = "winMoz_Gui_NewAssets";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "Mozubi Scenario Builder";
            this.Load += new System.EventHandler(this.winMoz_Gui_Load);
            this.AssetsBox.ResumeLayout(false);
            this.AssetsBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.GroupBox AssetsBox;
        private System.Windows.Forms.TextBox Assets_ID_TextBox;
        private System.Windows.Forms.Label Asset_ID;
        private System.Windows.Forms.Button DeleteAdditionalAsset;
        private System.Windows.Forms.Label AdditionalAssets;
        private System.Windows.Forms.Button AddAdditionalAsset;
        private System.Windows.Forms.ListBox AdditionalAssets_ListBox;
    }
}

