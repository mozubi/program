﻿namespace winMoz_Gui
{
    partial class winMoz_Gui_PlotAndOutput_Elements
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.ElementType = new System.Windows.Forms.Label();
            this.Start_TextBox = new System.Windows.Forms.TextBox();
            this.Start = new System.Windows.Forms.Label();
            this.End_TextBox = new System.Windows.Forms.TextBox();
            this.TimesBox = new System.Windows.Forms.GroupBox();
            this.End = new System.Windows.Forms.Label();
            this.ElementType_ComboBox = new System.Windows.Forms.ComboBox();
            this.ElementBox = new System.Windows.Forms.GroupBox();
            this.Select = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TimesBox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ElementType
            // 
            this.ElementType.AutoSize = true;
            this.ElementType.Location = new System.Drawing.Point(17, 39);
            this.ElementType.Name = "ElementType";
            this.ElementType.Size = new System.Drawing.Size(159, 29);
            this.ElementType.TabIndex = 0;
            this.ElementType.Text = "Element type:";
            // 
            // Start_TextBox
            // 
            this.Start_TextBox.Location = new System.Drawing.Point(316, 45);
            this.Start_TextBox.Name = "Start_TextBox";
            this.Start_TextBox.Size = new System.Drawing.Size(377, 35);
            this.Start_TextBox.TabIndex = 1;
            // 
            // Start
            // 
            this.Start.AutoSize = true;
            this.Start.Location = new System.Drawing.Point(22, 51);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(68, 29);
            this.Start.TabIndex = 2;
            this.Start.Text = "Start:";
            // 
            // End_TextBox
            // 
            this.End_TextBox.Location = new System.Drawing.Point(316, 95);
            this.End_TextBox.Name = "End_TextBox";
            this.End_TextBox.Size = new System.Drawing.Size(377, 35);
            this.End_TextBox.TabIndex = 3;
            // 
            // TimesBox
            // 
            this.TimesBox.Controls.Add(this.End);
            this.TimesBox.Controls.Add(this.End_TextBox);
            this.TimesBox.Controls.Add(this.Start);
            this.TimesBox.Controls.Add(this.Start_TextBox);
            this.TimesBox.Location = new System.Drawing.Point(15, 20);
            this.TimesBox.Name = "TimesBox";
            this.TimesBox.Size = new System.Drawing.Size(744, 150);
            this.TimesBox.TabIndex = 4;
            this.TimesBox.TabStop = false;
            this.TimesBox.Text = "Times";
            this.TimesBox.Visible = false;
            // 
            // End
            // 
            this.End.AutoSize = true;
            this.End.Location = new System.Drawing.Point(22, 98);
            this.End.Name = "End";
            this.End.Size = new System.Drawing.Size(62, 29);
            this.End.TabIndex = 4;
            this.End.Text = "End:";
            // 
            // ElementType_ComboBox
            // 
            this.ElementType_ComboBox.FormattingEnabled = true;
            this.ElementType_ComboBox.Items.AddRange(new object[] {
            "Bids",
            "MeritOrders",
            "CoupledMeritOrders",
            "Schedules",
            "MarketResults"});
            this.ElementType_ComboBox.Location = new System.Drawing.Point(199, 36);
            this.ElementType_ComboBox.Name = "ElementType_ComboBox";
            this.ElementType_ComboBox.Size = new System.Drawing.Size(345, 37);
            this.ElementType_ComboBox.TabIndex = 5;
            this.ElementType_ComboBox.SelectedIndexChanged += new System.EventHandler(this.ElementType_ComboBox_SelectedIndexChanged);
            // 
            // ElementBox
            // 
            this.ElementBox.Location = new System.Drawing.Point(15, 189);
            this.ElementBox.Name = "ElementBox";
            this.ElementBox.Size = new System.Drawing.Size(744, 200);
            this.ElementBox.TabIndex = 5;
            this.ElementBox.TabStop = false;
            this.ElementBox.Text = "Elements";
            this.ElementBox.Visible = false;
            // 
            // Select
            // 
            this.Select.BackColor = System.Drawing.Color.LightGreen;
            this.Select.Location = new System.Drawing.Point(590, 9);
            this.Select.Name = "Select";
            this.Select.Size = new System.Drawing.Size(162, 40);
            this.Select.TabIndex = 6;
            this.Select.Text = "Select";
            this.Select.UseVisualStyleBackColor = false;
            this.Select.Click += new System.EventHandler(this.Select_Click);
            // 
            // Cancel
            // 
            this.Cancel.BackColor = System.Drawing.Color.LightCoral;
            this.Cancel.Location = new System.Drawing.Point(590, 55);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(162, 40);
            this.Cancel.TabIndex = 7;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = false;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.TimesBox);
            this.panel1.Controls.Add(this.ElementBox);
            this.panel1.Location = new System.Drawing.Point(22, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 400);
            this.panel1.TabIndex = 8;
            // 
            // winMoz_Gui_PlotAndOutput_Elements
            // 
            this.ClientSize = new System.Drawing.Size(842, 528);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Select);
            this.Controls.Add(this.ElementType_ComboBox);
            this.Controls.Add(this.ElementType);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "winMoz_Gui_PlotAndOutput_Elements";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.TimesBox.ResumeLayout(false);
            this.TimesBox.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.GroupBox PlotsBox;
        private System.Windows.Forms.Label Plots;
        private System.Windows.Forms.Button DeletePlot;
        private System.Windows.Forms.Button AddPlot;
        private System.Windows.Forms.ListBox Plots_ListBox;
        private System.Windows.Forms.GroupBox OutputConsoleBox;
        private System.Windows.Forms.Button DeletOutputConsole;
        private System.Windows.Forms.Label OutputConsole;
        private System.Windows.Forms.Button AddOutputConsole;
        private System.Windows.Forms.ListBox OutputConsole_ListBox;
        private System.Windows.Forms.GroupBox OutputCSVBox;
        private System.Windows.Forms.Button DeletOutputCSV;
        private System.Windows.Forms.Label OutputCSV;
        private System.Windows.Forms.Button AddOutputCSV;
        private System.Windows.Forms.ListBox OutputCSV_ListBox;
        private System.Windows.Forms.Label ElementType;
        private System.Windows.Forms.TextBox Start_TextBox;
        private System.Windows.Forms.Label Start;
        private System.Windows.Forms.TextBox End_TextBox;
        private System.Windows.Forms.GroupBox TimesBox;
        private System.Windows.Forms.Label End;
        private System.Windows.Forms.ComboBox ElementType_ComboBox;
        private System.Windows.Forms.GroupBox ElementBox;
        private System.Windows.Forms.Button Select;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Panel panel1;
    }
}

