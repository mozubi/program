﻿namespace winMoz_Gui
{
    partial class winMoz_Gui_MarketObjectSettings
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Attributes_Box = new System.Windows.Forms.GroupBox();
            this.Attributes_Panel = new System.Windows.Forms.Panel();
            this.Attributes_Box.SuspendLayout();
            this.SuspendLayout();
            // 
            // Attributes_Box
            // 
            this.Attributes_Box.Controls.Add(this.Attributes_Panel);
            this.Attributes_Box.Location = new System.Drawing.Point(33, 32);
            this.Attributes_Box.Name = "Attributes_Box";
            this.Attributes_Box.Size = new System.Drawing.Size(1210, 671);
            this.Attributes_Box.TabIndex = 17;
            this.Attributes_Box.TabStop = false;
            this.Attributes_Box.Text = "Attributes";
            // 
            // Attributes_Panel
            // 
            this.Attributes_Panel.AutoScroll = true;
            this.Attributes_Panel.Location = new System.Drawing.Point(6, 32);
            this.Attributes_Panel.Name = "Attributes_Panel";
            this.Attributes_Panel.Size = new System.Drawing.Size(1198, 633);
            this.Attributes_Panel.TabIndex = 0;
            // 
            // winMoz_Gui_MarketObjectSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1260, 725);
            this.Controls.Add(this.Attributes_Box);
            this.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.Name = "winMoz_Gui_MarketObjectSettings";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "Mozubi Scenario Builder";
            this.Load += new System.EventHandler(this.winMoz_Gui_Load);
            this.Attributes_Box.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox Attributes_Box;
        private System.Windows.Forms.Panel Attributes_Panel;
    }
}

