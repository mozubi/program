﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;
using System.Diagnostics;
using winMoz.Data.Access.DTO;
using winMoz.Markets;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.Elements;
using winMoz.Factories;
using winMoz.Markets.Bids;
using winMoz.Markets.EoMMarkets;
using winMoz.Information;

namespace winMoz_Gui
{
    public partial class winMoz_Gui_NewMarketObject : Form
    {
        public DateTime StartDate;

        public Dictionary<string, (Label Label, Control Element)> FormElements = new Dictionary<string, (Label, Control)>();

        public List<SimObject> MarketObjectsList;

        public SimObject MarketObjectToAdd;

        public int MarketsId;

        public bool HasToBeAdded { get; private set; }

        public winMoz_Gui_NewMarketObject()
        {
            InitializeComponent();
        }

        private void winMoz_Gui_Load(object sender, EventArgs e)
        {
        }

        private void MarketObject_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (MarketObject_ComboBox.SelectedItem)
            {
                case "FuturesMarket":
                    {
                        CreateFuturesMarketElements();
                        break;
                    }
                case "DayAheadMarket":
                    {
                        CreateDayAheadMarketElements();
                        break;
                    }
                case "IntradayMarket":
                    {
                        CreateIntradayMarketElements();
                        break;
                    }
                case "ControlReserve_Capacity":
                    {
                        CreateControlReserve_CapacityElements();
                        break;
                    }
                case "ControlReserve_Energy":
                    {
                        CreateControlReserve_EnergyElements();
                        break;
                    }
                case "ControlReserve_Energy_Scenario1":
                    {
                        CreateControlReserve_EnergyElements();
                        break;
                    }
                case "ControlReserve_Activation":
                    {
                        CreateControlReserve_ActivationElements();
                        break;
                    }
                case "ControlReserveTenderVolumeHandler":
                    {
                        CreateControlReserveTenderVolumeHandlerElements();
                        break;
                    }
                case "Scenario1_ControlReserveTenderVolumeHandler":
                    {
                        CreateControlReserveTenderVolumeHandlerElements();
                        break;
                    }
                case "ControlReserveActivationVolumeHandler":
                    {
                        CreateControlReserveActivationVolumeHandlerElements();
                        break;
                    }
                case "IGCC_Calculation":
                    {
                        CreateIGCC_CalculationElements();
                        break;
                    }
                case "ReBAP_Calculation":
                    {
                        CreateReBAP_CalculationElements();
                        break;
                    }
                case "DeviationFromRZSaldo_Handler":
                    {
                        CreateDeviationFromRZSaldo_HandlerElements();
                        break;
                    }
            }
        }

        private void CreateFuturesMarketElements(MarketAttributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketAttributes(0, typeof(FuturesMarket), attributes);
        }

        

        private void CreateDayAheadMarketElements(DayAheadAttributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketWithBlocksAttributes(0, typeof(DayAheadMarket), attributes);
            FormElements.Add("WithMarketCoupling", (CreateElementLabel("WithMarketCoupling:", nextNumberYPos), CreateElementCheckBox(nextNumberYPos, attributes != null ? attributes.WithMarketCoupling : false)));
            FormElements.Add("MCC_ID", (CreateElementLabel("MCC_ID:", nextNumberYPos + 1), CreateElementComboBox(nextNumberYPos + 1, attributes != null ? attributes.MCC_ID.ToString() : "", new MCC_Context().TotalScenario.Select(item => item.TotalID.ToString()).ToArray())));
        }


        private void CreateIntradayMarketElements(IntradayAttributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketWithBlocksAttributes(0, typeof(IntradayMarket), attributes);
        }

        private void CreateControlReserve_CapacityElements(ControlReserve_Capacity_Attributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketAttributes(0, typeof(ControlReserve_Capacity), attributes);
            FormElements.Add("EnergyMarket", (CreateElementLabel("EnergyMarket:", nextNumberYPos), CreateElementComboBox(nextNumberYPos, MarketObjectsList.Where(el => el is ControlReserve_Energy).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
        }

        private void CreateControlReserve_EnergyElements(ControlReserve_Energy_Attributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddMarketAttributes(0, typeof(ControlReserve_Energy), attributes);
            FormElements.Add("OnlyBidsFromCapacityAllowed", (CreateElementLabel("OnlyBidsFromCapacity:", nextNumberYPos), CreateElementCheckBox(nextNumberYPos, value: attributes != null ? attributes.OnlyBidsFromCapacityAllowed : false)));
            FormElements.Add("IntradayMarket", (CreateElementLabel("IntradayMarket:", nextNumberYPos + 1), CreateElementComboBox(nextNumberYPos + 1, MarketObjectsList.Where(el => el is IntradayMarket).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
            FormElements.Add("Activation", (CreateElementLabel("Activation:", nextNumberYPos + 2), CreateElementComboBox(nextNumberYPos + 2, MarketObjectsList.Where(el => el is ControlReserve_Activation).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
        }

        private void CreateControlReserve_ActivationElements(ActivationAttributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddActivationAttributes(0, attributes);
        }

        private void CreateControlReserveTenderVolumeHandlerElements(ControlReserveTenderVolumeAttributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("Markets", (CreateElementLabel("Markets:", 0), CreateElementsMultiElementsBox(0, MarketObjectsList.Where(el => el is Market).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
            FormElements.Add("ErrorLevel_FRR", (CreateElementLabel("ErrorLevel_FRR:", 5), CreateElementNumericUpDown(5, min: 0.00001m, max: 100m, dec: 5, value: attributes != null ? (decimal)attributes.ErrorLevel_FRR : 0.00001m)));
            FormElements.Add("ErrorLevel_aFRR", (CreateElementLabel("ErrorLevel_aFRR:", 6), CreateElementNumericUpDown(6, min: 0.00001m, max: 100m, dec: 5, value: attributes != null ? (decimal)attributes.ErrorLevel_aFRR : 0.00001m)));
        }

        private void CreateControlReserveActivationVolumeHandlerElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            FormElements.Add("Activations", (CreateElementLabel("Activation:", 0), CreateElementsMultiElementsBox(0, MarketObjectsList.Where(el => el is ControlReserve_Activation).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
            FormElements.Add("IGCC_Calculation", (CreateElementLabel("IGCC_Calculation:", 5), CreateElementComboBox(5, MarketObjectsList.Where(el => el is IGCC_Calculation).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
            FormElements.Add("DeviationFromRZSaldoHandler", (CreateElementLabel("DeviationFromRZSaldoHandler:", 6), CreateElementComboBox(6, MarketObjectsList.Where(el => el is DeviationFromRZSaldo_Handler).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
        }

        private void CreateIGCC_CalculationElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
        }

        private void CreateReBAP_CalculationElements(SimEventAttributes attributes = null)
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
            var nextNumberYPos = AddEventAttributes(0, attributes);
            FormElements.Add("IGCC_Calculation", (CreateElementLabel("IGCC_Calculation:", nextNumberYPos), CreateElementComboBox(nextNumberYPos, MarketObjectsList.Where(el => el is IGCC_Calculation).ToList().Select(item => item.GetType().Name + " ID:" + ((SimObject)item).Id.ToString()).ToArray())));
        }

        private void CreateDeviationFromRZSaldo_HandlerElements()
        {
            FormElements.Values.ToList().ForEach(item => { item.Label.Dispose(); item.Element.Dispose(); });
            FormElements.Clear();
        }

        private int AddMarketWithBlocksAttributes(int numberYPos, Type marketType, MarketWithBlocksAttributes attributes = null)
        {
            var nextNumberYPos = AddMarketAttributes(numberYPos, marketType, attributes);
            FormElements.Add("BlockTypes", (CreateElementLabel("BlockTypes:", nextNumberYPos), CreateElementsMultiElementsBox(nextNumberYPos, elementsListBox: attributes != null ? attributes._BlockTypes.Select(item => item.ToString()).ToList() : null, GetBlockTypes(marketType))));
            return nextNumberYPos + 5;
        }

        private string[] GetBlockTypes(Type type)
        {
            switch (type.ToString())
            {
                case "DayAhead":
                    {
                        var Test = ShortCode.DayAhead.GetBlockCodes().ToArray();
                        return ShortCode.DayAhead.GetBlockCodes().ToArray();
                    }
                case "Intraday": return ShortCode.IntraDay.GetBlockCodes().ToArray();
                default:
                    {
                        var BlockTypes = new List<string>(ShortCode.DayAhead.GetBlockCodes());
                        BlockTypes.AddRange(ShortCode.IntraDay.GetBlockCodes());
                        return BlockTypes.ToArray();
                    }
            }
        }

        private int AddActivationAttributes(int numberYPos, ActivationAttributes attributes = null)
        {
            int nextNumberYPos = AddEventAttributes(numberYPos);
            FormElements.Add("AuctionStep", (CreateElementLabel("AuctionStep:", nextNumberYPos), CreateElementTextBox(nextNumberYPos, attributes != null ? attributes.AuctionStep.ToString() : "")));
            FormElements.Add("Upper_Price_Bound", (CreateElementLabel("Upper_Price_Bound:", nextNumberYPos + 1), CreateElementNumericUpDown(nextNumberYPos + 1, dec: 2, value: attributes != null ? (decimal)attributes.Upper_Price_Bound : 0)));
            FormElements.Add("Lower_Price_Bound", (CreateElementLabel("Lower_Price_Bound:", nextNumberYPos + 2), CreateElementNumericUpDown(nextNumberYPos + 2, dec: 2, value: attributes != null ? (decimal)attributes.Lower_Price_Bound : 0)));
            return nextNumberYPos + 3;
        }

        private int AddMarketAttributes(int numberYPos, Type marketType, MarketAttributes attributes = null)
        {
            int nextNumberYPos = AddEventAttributes(numberYPos, attributes != null ? attributes.EventAttributes : null);
            FormElements.Add("Upper_Price_Bound", (CreateElementLabel("Upper_Price_Bound:", nextNumberYPos), CreateElementNumericUpDown(nextNumberYPos, dec: 2, value: attributes != null ? (decimal)attributes.Upper_Price_Bound : 0)));
            FormElements.Add("Lower_Price_Bound", (CreateElementLabel("Lower_Price_Bound:", nextNumberYPos + 1), CreateElementNumericUpDown(nextNumberYPos + 1, dec: 2, value: attributes != null ? (decimal)attributes.Lower_Price_Bound : 0)));
            FormElements.Add("LocalMACode", (CreateElementLabel("LocalMACode:", nextNumberYPos + 2), CreateElementComboBox(nextNumberYPos + 2, attributes != null ? attributes.LocalMACode.ToString() : "", MarketAreaEnum.GetAll().Select(item => item.Value.ToString()).ToArray())));
            FormElements.Add("MarketType", (CreateElementLabel("MarketType:", nextNumberYPos + 3), CreateElementTextBox(nextNumberYPos + 3, marketType.ToString(), enabled : false)));
            FormElements.Add("AllowedBidType", (CreateElementLabel("AllowedBidType:", nextNumberYPos + 4), CreateElementComboBox(nextNumberYPos + 4, attributes != null ? attributes.AllowedBidType.ToString() : "", GetAllowedBidTypes(marketType))));
            FormElements.Add("NumberOfDaysToLetBidsInBidList", (CreateElementLabel("NumberOfDaysToLetBidsInBidList:", nextNumberYPos + 5), CreateElementNumericUpDown(nextNumberYPos + 5, dec: 0, value: attributes != null ? attributes.NumberOfDaysToLetBidsInBidList : 0)));
            return nextNumberYPos + 6;
        }

        private string[] GetAllowedBidTypes(Type type)
        {
            switch (type.ToString())
            {
                case "Futures": return new List<string>() { "winMoz.Markets.Bids.Bid_Future" }.ToArray();
                case "DayAhead": return new List<string>() { "winMoz.Markets.Bids.Bid_DayAhead" }.ToArray();
                case "Intraday": return new List<string>() { "winMoz.Markets.Bids.Bid_Intraday" }.ToArray();
                case "ControlReserve_Capacity": return new List<string>() { "winMoz.Markets.Bids.Bid_ControlReserve_Capacity" }.ToArray();
                case "ControlReserve_Energy": return new List<string>() { "winMoz.Markets.Bids.Bid_ControlReserve_Energy" }.ToArray();
                default: return new List<string>() { "winMoz.Markets.Bids.Bid_DayAhead", "winMoz.Markets.Bids.Bid_DayAhead", "winMoz.Markets.Bids.Bid_Intraday", "winMoz.Markets.Bids.Bid_ControlReserve_Capacity", "winMoz.Markets.Bids.Bid_ControlReserve_Energy" }.ToArray();
            }
        }

        private int AddEventAttributes(int numberYPos, SimEventAttributes attributes = null)
        {
            FormElements.Add("StartEventTime", (CreateElementLabel("StartEventTime (Duration from start):", numberYPos), CreateElementDurationBox(numberYPos, attributes != null ? attributes.StartEventTimeDurationFromStart : null)));
            FormElements.Add("StartDeterminationTime", (CreateElementLabel("StartDeterminationTime (Duration from start):", numberYPos + 3), CreateElementDurationBox(numberYPos + 3, attributes != null ? attributes.StartDeterminationTimeDurationFromStart : null)));
            FormElements.Add("DeterminationDuration", (CreateElementLabel("DeterminationDuration:", numberYPos + 6), CreateElementDurationBox(numberYPos + 6, attributes != null ? attributes.DeterminationDuration : null)));
            FormElements.Add("DeterminationStep", (CreateElementLabel("DeterminationStep:", numberYPos + 9), CreateElementDurationBox(numberYPos + 9, attributes != null ? attributes.DeterminationStep : null)));
            return numberYPos + 12;
        }

        private Label CreateElementLabel(string text, int numberYPos, int width = default)
        {
            Label lbl = new Label();
            this.Attributes_Panel.Controls.Add(lbl);
            lbl.Location = new Point(x: 28, y: 30 + numberYPos * 30);
            lbl.Text = text;
            lbl.Width = width == default ? 314 : width;
            return lbl;
        }

        private TextBox CreateElementTextBox(int numberYPos, string text = "", bool enabled = true)
        {
            TextBox tb = new TextBox();
            this.Attributes_Panel.Controls.Add(tb);
            tb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            tb.Size = new Size(width: 377, height: 35);
            tb.Text = text;
            tb.Enabled = enabled;
            return tb;
        }

        private CheckBox CreateElementCheckBox(int numberYPos, bool value = false)
        {
            CheckBox chb = new CheckBox();
            this.Attributes_Panel.Controls.Add(chb);
            chb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            chb.Text = "";
            chb.Checked = value;
            return chb;
        }

        private NumericUpDown CreateElementNumericUpDown(int numberYPos, decimal min = (-1000000m), decimal max = 1000000m, int dec = 0, decimal value = 0, int width = default)
        {
            NumericUpDown nud = new NumericUpDown();
            this.Attributes_Panel.Controls.Add(nud);
            nud.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            nud.Minimum = min;
            nud.Maximum = max;
            nud.Value = value;
            if (width != default) nud.Width = width;
            nud.DecimalPlaces = dec;
            return nud;
        }

        private RadioButton CreateElementRadioButton(int numberYPos)
        {
            RadioButton rb = new RadioButton();
            this.Attributes_Panel.Controls.Add(rb);
            rb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            rb.Text = "";
            return rb;
        }

        private ComboBox CreateElementComboBox(int numberYPos, params string[] elements) => CreateElementComboBox(numberYPos, "", elements);

        private ComboBox CreateElementComboBox(int numberYPos, string text, params string[] elements)
        {
            ComboBox cb = new ComboBox();
            this.Attributes_Panel.Controls.Add(cb);
            cb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            cb.Text = text;
            cb.Items.AddRange(elements);
            cb.Size = new Size(width: 300, height: 35);
            return cb;
        }

        private ListBox CreateElementListBox(int numberYPos, params string[] elements)
        {
            ListBox lb = new ListBox();
            this.Attributes_Panel.Controls.Add(lb);
            lb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            lb.Text = "";
            lb.Items.AddRange(elements);
            lb.Size = new Size(width: 200, height: 70);
            return lb;
        }
        private GroupBox CreateElementDurationBox(int numberYPos, TimeSpanSave duration = null)
        {
            GroupBox gb = new GroupBox();
            this.Attributes_Panel.Controls.Add(gb);
            gb.Location = new Point(x: 350, y: 10 + numberYPos * 30);
            gb.Text = "";
            var lblYears = CreateElementLabel("Years", 0, 50);
            gb.Controls.Add(lblYears);
            lblYears.Location = new Point(x: 30, y: 20);
            var nudYears = CreateElementNumericUpDown(duration != null ? duration.Years : 0, min: -100, max: 100, value: 0, width: 50);
            gb.Controls.Add(nudYears);
            nudYears.Location = new Point(x: 30, y: 50);
            var lblMonth = CreateElementLabel("Month", 0, 50);
            gb.Controls.Add(lblMonth);
            lblMonth.Location = new Point(x: 100, y: 20);
            var nudMonth = CreateElementNumericUpDown(duration != null ? duration.Months : 0, min: -100, max: 100, value: 0, width: 50);
            gb.Controls.Add(nudMonth);
            nudMonth.Location = new Point(x: 100, y: 50);
            var lblDays = CreateElementLabel("Days", 0, 50);
            gb.Controls.Add(lblDays);
            lblDays.Location = new Point(x: 170, y: 20);
            var nudDays = CreateElementNumericUpDown(duration != null ? duration.Days : 0, min: -100, max: 100, value: 0, width: 50);
            gb.Controls.Add(nudDays);
            nudDays.Location = new Point(x: 170, y: 50);
            var lblHours = CreateElementLabel("Hours", 0, 50);
            gb.Controls.Add(lblHours);
            lblHours.Location = new Point(x: 240, y: 20);
            var nudHours = CreateElementNumericUpDown(duration != null ? duration.Hours : 0, min: -100, max: 100, value: 0, width: 50);
            gb.Controls.Add(nudHours);
            nudHours.Location = new Point(x: 240, y: 50);
            var lblMinutes = CreateElementLabel("Minutes", 0, 50);
            gb.Controls.Add(lblMinutes);
            lblMinutes.Location = new Point(x: 310, y: 20);
            var nudMinutes = CreateElementNumericUpDown(duration != null ? duration.Minutes : 0, min: -100, max: 100, value: 0, width: 50);
            gb.Controls.Add(nudMinutes);
            nudMinutes.Location = new Point(x: 310, y: 50);
            gb.Size = new Size(width: 380, height: 90);
            return gb;
        }

        private GroupBox CreateElementsMultiElementsBox(int numberYPos, params string[] elements) => CreateElementsMultiElementsBox(numberYPos, null, elements);

        private GroupBox CreateElementsMultiElementsBox(int numberYPos, List<string> elementsListBox, params string[] elements)
        {
            GroupBox gb = new GroupBox();
            this.Attributes_Panel.Controls.Add(gb);
            gb.Location = new Point(x: 350, y: 30 + numberYPos * 30);
            gb.Text = "";
            var cb = CreateElementComboBox(0, elements);
            gb.Controls.Add(cb);
            cb.Location = new Point(x: 30, y: 30);
            var lb = CreateElementListBox(0, elementsListBox != null ? elementsListBox.ToArray() : new List<string>().ToArray());
            gb.Controls.Add(lb);
            lb.Location = new Point(x: 30, y: 60);
            AddButton Add = new AddButton(cb, lb);
            gb.Controls.Add(Add);
            Add.Location = new Point(x: 250, y: 60);
            DeleteButton Delete = new DeleteButton(lb);
            gb.Controls.Add(Delete);
            Delete.Location = new Point(x: 250, y: 90);
            gb.Size = new Size(width: 350, height: 135);
            return gb;
        }

        private class AddButton : Button
        {
            public ComboBox CB { get; set; }

            public ListBox LB { get; set; }
            public AddButton(ComboBox cb, ListBox lb) : base()
            {
                CB = cb;
                LB = lb;
                this.Text = "Add";
                this.BackColor = Color.LightSkyBlue;
                this.Click += AddElements;
            }

            private void AddElements(object sender, EventArgs e)
            {
                if (CB.Text != "")
                    LB.Items.Add(CB.Text);
            }
        }

        private class DeleteButton : Button
        {
            public ListBox LB { get; set; }
            public DeleteButton(ListBox lb) : base()
            {
                LB = lb;
                this.Text = "Delete";
                this.BackColor = Color.LightCoral;
                this.Click += AddElements;
            }

            private void AddElements(object sender, EventArgs e)
            {
                if (LB.SelectedIndex != -1)
                    LB.Items.RemoveAt(LB.SelectedIndex);
            }
        }

        private void Default_Click(object sender, EventArgs e)
        {
            switch (MarketObject_ComboBox.SelectedItem)
            {
                case "FuturesMarket":
                    {
                        CreateFuturesMarketElements(new FuturesAttributes(StartDate, MarketAreaEnum.Default()));
                        break;
                    }
                case "DayAheadMarket":
                    {
                        CreateDayAheadMarketElements(new DayAheadAttributes(StartDate, MarketAreaEnum.Default()));
                        break;
                    }
                case "IntradayMarket":
                    {
                        CreateIntradayMarketElements(new IntradayAttributes(StartDate, MarketAreaEnum.Default()));
                        break;
                    }
                case "ControlReserve_Capacity":
                    {
                        CreateControlReserve_CapacityElements(new ControlReserve_Capacity_Attributes(StartDate, MarketAreaEnum.Default()));
                        break;
                    }
                case "ControlReserve_Energy":
                    {
                        CreateControlReserve_EnergyElements(new ControlReserve_Energy_Attributes(StartDate, MarketAreaEnum.Default()));
                        break;
                    }
                case "ControlReserve_Energy_Scenario1":
                    {
                        CreateControlReserve_EnergyElements(new ControlReserve_Energy_Attributes(StartDate, MarketAreaEnum.Default()));
                        break;
                    }
                case "ControlReserve_Activation":
                    {
                        CreateControlReserve_ActivationElements(new ActivationAttributes(StartDate));
                        break;
                    }
                case "ControlReserveTenderVolumeHandler":
                    {
                        CreateControlReserveTenderVolumeHandlerElements(new ControlReserveTenderVolumeAttributes());
                        break;
                    }
                case "Scenario1_ControlReserveTenderVolumeHandler":
                    {
                        CreateControlReserveTenderVolumeHandlerElements(new ControlReserveTenderVolumeAttributes());
                        break;
                    }
                case "ControlReserveActivationVolumeHandler":
                    {
                        CreateControlReserveActivationVolumeHandlerElements();
                        break;
                    }
                case "IGCC_Calculation":
                    {
                        CreateIGCC_CalculationElements();
                        break;
                    }
                case "ReBAP_Calculation":
                    {
                        CreateReBAP_CalculationElements(ReBAP_Attributes_Factory.GetDefault(StartDate));
                        break;
                    }
                case "DeviationFromRZSaldo_Handler":
                    {
                        CreateDeviationFromRZSaldo_HandlerElements();
                        break;
                    }
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (MarketObject_ComboBox.SelectedIndex != -1)
            {
                switch (MarketObject_ComboBox.SelectedItem)
                {
                    case "FuturesMarket":
                        {
                            MarketObjectToAdd = GetFuturesMarket();
                            break;
                        }
                    case "DayAheadMarket":
                        {
                            MarketObjectToAdd = GetDayAheadMarket();
                            break;
                        }
                    case "IntradayMarket":
                        {
                            MarketObjectToAdd = GetIntradayMarket();
                            break;
                        }
                    case "ControlReserve_Capacity":
                        {
                            MarketObjectToAdd = GetControlReserve_Capacity();
                            break;
                        }
                    case "ControlReserve_Energy":
                        {
                            MarketObjectToAdd = GetControlReserve_Energy();
                            break;
                        }
                    case "ControlReserve_Energy_Scenario1":
                        {
                            MarketObjectToAdd = GetControlReserve_Energy_Scenario1();
                            break;
                        }
                    case "ControlReserve_Activation":
                        {
                            MarketObjectToAdd = GetControlReserve_Activation();
                            break;
                        }
                    case "ControlReserveTenderVolumeHandler":
                        {
                            MarketObjectToAdd = GetControlReserveTenderVolumeHandler();
                            break;
                        }
                    case "Scenario1_ControlReserveTenderVolumeHandler":
                        {
                            MarketObjectToAdd = GetScenario1_ControlReserveTenderVolumeHandler();
                            break;
                        }
                    case "ControlReserveActivationVolumeHandler":
                        {
                            MarketObjectToAdd = GetControlReserveActivationVolumeHandler();
                            break;
                        }
                    case "IGCC_Calculation":
                        {
                            MarketObjectToAdd = GetIGCC_Calculation();
                            break;
                        }
                    case "ReBAP_Calculation":
                        {
                            MarketObjectToAdd = GetReBAP_Calculation();
                            break;
                        }
                    case "DeviationFromRZSaldo_Handler":
                        {
                            MarketObjectToAdd = GetDeviationFromRZSaldo_Handler();
                            break;
                        }
                }
                using (var repo = new ScenarioData_Repository())
                {
                    repo.MarketRepository.Add(MarketObjectToAdd);
                }
                HasToBeAdded = true;
                this.Close();
            }
        }

        private FuturesMarket GetFuturesMarket()
        {
            return new FuturesMarket(
                attributes: GetMarketAttributes<FuturesAttributes>(),
                scenarioID: MarketsId
                );

        }

        private DayAheadMarket GetDayAheadMarket()
        {
            return new DayAheadMarket(
                attributes: GetMarketAttributes<DayAheadAttributes>(),
                scenarioID: MarketsId
                );

        }

        private IntradayMarket GetIntradayMarket()
        {
            return new IntradayMarket(
                attributes: GetMarketAttributes<IntradayAttributes>(),
                scenarioID: MarketsId
                );
        }

        private ControlReserve_Capacity GetControlReserve_Capacity()
        {
            var EnergyMarketID = (int)Convert.ToInt64(((ComboBox)FormElements["EnergMarket"].Element).Text.Split(':')[1]);
            var EnergyMarket = (ControlReserve_Energy)MarketObjectsList.Where(item => item is ControlReserve_Energy).ToList().SingleOrDefault(item => ((ControlReserve_Energy)item).Id == EnergyMarketID);
            return new ControlReserve_Capacity(
                attributes: GetMarketAttributes<ControlReserve_Capacity_Attributes>(),
                energyMarket: EnergyMarket,
                scenarioID: MarketsId
                );
        }

        private ControlReserve_Energy GetControlReserve_Energy()
        {
            var IntradayID = (int)Convert.ToInt64(((ComboBox)FormElements["Intraday"].Element).Text.Split(':')[1]);
            var Intraday = (IntradayMarket)MarketObjectsList.Where(item => item is IntradayMarket).ToList().SingleOrDefault(item => ((IntradayMarket)item).Id == IntradayID);
            var ActivationID = (int)Convert.ToInt64(((ComboBox)FormElements["Activation"].Element).Text.Split(':')[1]);
            var Activation = (ControlReserve_Activation)MarketObjectsList.Where(item => item is ControlReserve_Activation).ToList().SingleOrDefault(item => ((ControlReserve_Activation)item).Id == IntradayID);
            return new ControlReserve_Energy(
                attributes: GetMarketAttributes<ControlReserve_Energy_Attributes>(),
                intraday: Intraday,
                activation: Activation,
                scenarioID: MarketsId
                );
        }

        private ControlReserve_Energy_Scenario1 GetControlReserve_Energy_Scenario1()
        {
            ControlReserve_Energy ControlReserve_Energy = GetControlReserve_Energy();
            return new ControlReserve_Energy_Scenario1(
                attributes: (ControlReserve_Energy_Attributes)ControlReserve_Energy.Attributes,
                intraday: ControlReserve_Energy.Intraday,
                activation: ControlReserve_Energy.Activation,
                scenarioID: MarketsId
                );
        }

        private ControlReserve_Activation GetControlReserve_Activation()
        {
            return new ControlReserve_Activation(
                attributes: GetActivationAttributes(),
                scenarioID: MarketsId
                );
        }

        private ControlReserveTenderVolumeHandler GetControlReserveTenderVolumeHandler()
        {
            List<int> MarketIDs = new List<int>();
            foreach (Control control in ((GroupBox)FormElements["Markets"].Element).Controls)
            {
                try
                {
                    foreach (var item in ((ListBox)control).Items)
                        MarketIDs.Add((int)Convert.ToInt64(item.ToString().Split(':')[1]));
                }
                catch
                {

                }
            }
            List<Market> Markets = new List<Market>();
            foreach(int marketID in MarketIDs) Markets.Add((Market)MarketObjectsList.Where(item => item is Market).SingleOrDefault(item => ((Market)item).Id == marketID));
            return new ControlReserveTenderVolumeHandler(
                attributes: GetControlReserveTenderVolumeAttributes(),
                markets: Markets,
                scenarioID: MarketsId
                );
        }

        private Scenario1_ControlReserveTenderVolumeHandler GetScenario1_ControlReserveTenderVolumeHandler()
        {
            var ControlReserveTenderVolumeHandler = GetControlReserveTenderVolumeHandler();
            return new Scenario1_ControlReserveTenderVolumeHandler(
                attributes: ControlReserveTenderVolumeHandler.Attributes,
                markets: ControlReserveTenderVolumeHandler.Markets,
                scenarioID: MarketsId
                );
        }

        private ControlReserveActivationVolumeHandler GetControlReserveActivationVolumeHandler()
        {
            List<int> ActivationIDs = new List<int>();
            foreach (Control control in ((GroupBox)FormElements["Markets"].Element).Controls)
            {
                try
                {
                    foreach (var item in ((ListBox)control).Items)
                        ActivationIDs.Add((int)Convert.ToInt64(item.ToString().Split(':')[1]));
                }
                catch
                {

                }
            }
            List<ControlReserve_Activation> Activations = new List<ControlReserve_Activation>();
            foreach (int activationID in ActivationIDs) Activations.Add((ControlReserve_Activation)MarketObjectsList.Where(item => item is ControlReserve_Activation).SingleOrDefault(item => ((Market)item).Id == activationID));
            var IGCC_ID = (int)Convert.ToInt64(((ComboBox)FormElements["IGCC"].Element).Text.Split(':')[1]);
            var IGCC = (IGCC_Calculation)MarketObjectsList.Where(item => item is IGCC_Calculation).SingleOrDefault(item => ((IGCC_Calculation)item).Id == IGCC_ID);
            var DeviationFromRZSaldoID = (int)Convert.ToInt64(((ComboBox)FormElements["DeviationFromRZSaldo"].Element).Text.Split(':')[1]);
            var DeviationFromRZSaldo = (DeviationFromRZSaldo_Handler)MarketObjectsList.Where(item => item is DeviationFromRZSaldo_Handler).SingleOrDefault(item => ((DeviationFromRZSaldo_Handler)item).Id == DeviationFromRZSaldoID);
            return new ControlReserveActivationVolumeHandler(
                activations: Activations,
                igcc: IGCC,
                deviationFromRZSaldo: DeviationFromRZSaldo,
                scenarioID: MarketsId
                );
        }

        private IGCC_Calculation GetIGCC_Calculation()
        {
            return new IGCC_Calculation(
                scenarioID: MarketsId
                );
        }

        private ReBAP_Calculation GetReBAP_Calculation()
        {
            var IGCC_ID = (int)Convert.ToInt64(((ComboBox)FormElements["IGCC"].Element).Text.Split(':')[1]);
            var IGCC = (IGCC_Calculation)MarketObjectsList.Where(item => item is IGCC_Calculation).SingleOrDefault(item => ((IGCC_Calculation)item).Id == IGCC_ID);
            return new ReBAP_Calculation(
                attributes: GetEventAttributes(),
                igcc: IGCC,
                scenarioID: MarketsId
                );
        }

        private DeviationFromRZSaldo_Handler GetDeviationFromRZSaldo_Handler()
        {
            return new DeviationFromRZSaldo_Handler(
                scenarioID: MarketsId
                );
        }

        private TAttributes GetMarketAttributes<TAttributes>() where TAttributes : MarketAttributes
        {
            try
            {
                var EventAttributes = GetEventAttributes();
                var LocalMACode = MarketAreaEnum.FromString(((ComboBox)FormElements["LocalMACode"].Element).Text);
                var marketType = Type.GetType(((TextBox)FormElements["Type"].Element).Text);
                var AllowedBidType = ((ComboBox)FormElements["AllowedBidType"].Element).Text;
                var NumberOfDaysToLetBidsInBidList = (int)Convert.ToInt64(((NumericUpDown)FormElements["NumberOfDaysToLetBidsInBidList"].Element).Value);
                var Upper_Price_Bound = (float)((NumericUpDown)FormElements["Upper_Price_Bound"].Element).Value;
                var Lower_Price_Bound = (float)((NumericUpDown)FormElements["Lower_Price_Bound"].Element).Value;

                Type attributesType = typeof(TAttributes);

                MarketAttributes Attributes = null;

                if (attributesType == typeof(FuturesAttributes))
                {
                    Attributes = new FuturesAttributes(EventAttributes,
                            localMACode: LocalMACode,
                            allowedBidType: AllowedBidType,
                            numberOfDaysToLetBidsInBidList: NumberOfDaysToLetBidsInBidList,
                            Upper_Price_Bound,
                            Lower_Price_Bound
                            );
                }
                else if (attributesType.IsSubclassOf(typeof(MarketWithBlocksAttributes)))
                {
                    List<ShortCode> BlockTypes = new List<ShortCode>();
                    foreach (Control control in ((GroupBox)FormElements["Type"].Element).Controls)
                    {
                        foreach (var item in ((ListBox)control).Items)
                            BlockTypes.Add(ShortCode.FromString(item.ToString()));
                    }
                    if (attributesType == typeof(DayAheadAttributes))
                    {
                        Attributes = new DayAheadAttributes(EventAttributes,
                            localMACode: LocalMACode,
                            allowedBidType: AllowedBidType,
                            numberOfDaysToLetBidsInBidList: NumberOfDaysToLetBidsInBidList,
                            blockTypes: BlockTypes,
                            withMarketCoupling: ((CheckBox)FormElements["WithMarketCoupling"].Element).Checked,
                            mcc_ID: (int)Convert.ToInt64(((ComboBox)FormElements["MCC_ID"].Element).Text),
                            Upper_Price_Bound,
                            Lower_Price_Bound
                            );
                    }
                    else
                    {
                        Attributes = new IntradayAttributes(EventAttributes,
                            localMACode: LocalMACode,
                            allowedBidType: AllowedBidType,
                            numberOfDaysToLetBidsInBidList: NumberOfDaysToLetBidsInBidList,
                            blockTypes: BlockTypes,
                            Upper_Price_Bound,
                            Lower_Price_Bound
                            );
                    }
                }
                else if (attributesType == typeof(ControlReserve_Capacity_Attributes))
                {
                    Attributes = new ControlReserve_Capacity_Attributes(EventAttributes,
                            localMACode: LocalMACode,
                            allowedBidType: AllowedBidType,
                            numberOfDaysToLetBidsInBidList: NumberOfDaysToLetBidsInBidList,
                            Upper_Price_Bound,
                            Lower_Price_Bound
                            );
                }
                else if (attributesType == typeof(ControlReserve_Energy_Attributes))
                {
                    Attributes = new ControlReserve_Energy_Attributes(EventAttributes,
                            localMACode: LocalMACode,
                            allowedBidType: AllowedBidType,
                            numberOfDaysToLetBidsInBidList: NumberOfDaysToLetBidsInBidList,
                            onlyBidsFromCapacityAllowed: ((CheckBox)FormElements["OnlyBidsFromCapacityAllowed"].Element).Checked,
                            Upper_Price_Bound,
                            Lower_Price_Bound
                            );
                }
                return Attributes as TAttributes; 
                }


            catch
            {
                return null;
            }

        }
       
        

        

       

        private ActivationAttributes GetActivationAttributes()
        {
            try
            {
                return new ActivationAttributes(eventAttributes: GetEventAttributes(),
                            auctionStep: TimeSpan.FromMinutes((int)Convert.ToInt64(((TextBox)FormElements["AuctionStep"].Element).Text)),
                            (float)((NumericUpDown)FormElements["Upper_Price_Bound"].Element).Value,
                            (float)((NumericUpDown)FormElements["Lower_Price_Bound"].Element).Value
                            );
            }
            catch
            {
                return null;
            }
        }

        private ControlReserveTenderVolumeAttributes GetControlReserveTenderVolumeAttributes()
        {
            try
            {
                return new ControlReserveTenderVolumeAttributes(
                            errorLevel_FRR: Convert.ToSingle(((TextBox)FormElements["ErrorLevel_FRR"].Element).Text),
                            errorLevel_aFRR: Convert.ToSingle(((TextBox)FormElements["ErrorLevel_aFRR"].Element).Text)
                            );
            }
            catch
            {
                return null;
            }
        }

        private SimEventAttributes GetEventAttributes()
        {
            try
            {
                return new SimEventAttributes(
                             startEventTimeDurationFromStart: GetTimeSpanSaveFromControl((GroupBox)FormElements["StartEventTime"].Element),
                            startDeterminationTimeDurationFromStart: GetTimeSpanSaveFromControl((GroupBox)FormElements["StartDeterminationTime"].Element),
                            determinationDuration: GetTimeSpanSaveFromControl((GroupBox)FormElements["DeterminationDuration"].Element),
                            determinationStep: GetTimeSpanSaveFromControl((GroupBox)FormElements["DeterminationStep"].Element)
                            );
            }
            catch
            {
                return null;
            }
        }
        
        private TimeSpanSave GetTimeSpanSaveFromControl(GroupBox durationBox)
        {
            Control.ControlCollection Controls = durationBox.Controls;
            List<int> dateValues = new List<int>();
            foreach(var control in Controls)
            {
                if (control is NumericUpDown)
                    dateValues.Add((int)((NumericUpDown)control).Value);
            }
            return new TimeSpanSave(dateValues.ToArray());
        }

    }
}
