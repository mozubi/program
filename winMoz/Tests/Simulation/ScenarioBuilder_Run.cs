﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using winMoz.Data.Access;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;

namespace Tests.Simulation
{
    public class ScenarioBuilder_Run
    {
        [Test]
        public void RunScenarioBuilder()
        {
            Scenario_Builder scenario_ = new Scenario_Builder();
            scenario_.DeleteScenario(100000);
            //DbHelper.ReCreateDb<SimContext>();
            scenario_.BuildScenario();

        }
    }
}
