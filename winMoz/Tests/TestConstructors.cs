using NUnit.Framework;
using winMoz.Assets;

namespace Tests
{
    static public class TestConstructors
    {
        public static Location GetTestLocation()
        {
            return new Location(14195, "DE3", "DE30", "DE300", "Berlin", 52.4584693908691F, 13.2833271026611F); 
        }
    }
}