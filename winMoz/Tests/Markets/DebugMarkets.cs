﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Helper;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.ControlReserve;
using winMoz.Markets.Elements;
using winMoz.Markets.EoMMarkets;

namespace Tests.Markets
{
    class DebugIntraday: winMoz.Markets.EoMMarkets.IntradayMarket
    {

      /*  protected override void ExecuteAuction(out List<winMoz.Markets.Results.MarketResult> results)
        {
            
            var bids = DebugMarketObjectsGenerator.DebugAddRandomBidsID(GetAuctionArgs(), 100);
            foreach (var bid in bids) AddBidToMarket(bid.Price, bid.Volume, bid.QuarterHour.GetQuarterHourOfHour());

            base.ExecuteAuction(out results);

        }



    }
    class DebugControlReserve_Energy: ControlReserve_Energy
    {
        public DebugControlReserve_Energy(ControlReserve_Energy_Attributes attributes, IntradayMarket intraday, ControlReserve_Activation activation, int scenarioID = 0) : base(attributes, intraday, activation, scenarioID)
        {

        }
        private void GenerateDebugBids(AuctionArgs args)
           => this.Bids.AddRange(DebugMarketObjectsGenerator.DebugGenerateBidsControlReserveEnergyMarket(args, this));
        protected override void ExecuteAuction(out List<winMoz.Markets.Results.MarketResult> results)
        {

            GenerateDebugBids(GetAuctionArgs());
            SetBidsAuction();

            base.ExecuteAuction(out results);

        }

    }

    class DebugControlReserve_Capacity : ControlReserve_Capacity
    {
        public DebugControlReserve_Capacity(ControlReserve_Capacity_Attributes attributes, ControlReserve_Energy energyMarket, int scenarioID = 0) : base(attributes, energyMarket, scenarioID)
        {

        }
        private void GenerateDebugBids(AuctionArgs args)
            => this.Bids.AddRange(DebugMarketObjectsGenerator.DebugGenerateBidsControlReserveCapacityMarket(this, args));

        protected override void ExecuteAuction(out List<winMoz.Markets.Results.MarketResult> results)
        {

            GenerateDebugBids(GetAuctionArgs());
            SetBidsAuction();

            base.ExecuteAuction(out results);

        }*/
    }
}
