﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Data.Access.Contexts;
using winMoz.Simulation;

namespace Tests
{
    public class SimContextSetup : IDisposable
    {
        private readonly DbConnection _connection;

        public SimContextSetup()
        {
            DbContextOptions ContextOptions = new DbContextOptionsBuilder()
                .UseSqlite(CreateInMemoryDatabase())
                .Options;
            Config.DBOptions.simContextOptions = ContextOptions;
            _connection = RelationalOptionsExtension.Extract(ContextOptions).Connection;
            new SimContext().Database.EnsureCreated();
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }

        public void Dispose() => _connection.Dispose();

    }
}
