﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Helper;
using winMoz.Markets.Bids;
using winMoz.Simulation;
using winMoz.Assets;
using winMoz.Data.Access.Contexts;
using winMoz.Data.Access.Repositories;
using winMoz.Markets.Results;
using winMoz.Information.PFCs;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets;
using winMoz.Markets.Elements;
using winMoz.Assets.Plants;
using winMoz.Agents;

namespace winMoz.Helper
{
    public static class DebugMarketObjectsGenerator
    {
        /*public static List<(decimal Price, decimal Volume, DateTime Hour)> DebugAddRandomBidsDA(AuctionArgs args, int Anzahl = 100)
        {
            List<(decimal Price, decimal Volume, DateTime Hour)> Bids = new List<(decimal Price, decimal Volume, DateTime Hour)>();
            DateTime EndDuration = args.TimeStamp.Add(args.Duration);
            Random r = new Random();
            int i, k;
            decimal randomPrice, randomVolume;

            //Werte für Testgebote
            int[] avgPriceMG = new int[24] { 15, 17, 18, 19, 21, 24, 26, 29, 35, 38, 40, 37, 36, 30, 25, 24, 28, 32, 45, 49, 46, 33, 32, 30 };
            int[] stabMG = new int[24] { 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20 };
            int iSeed;

            k = 0;
            iSeed = 1000; // Environment.TickCount;
            //Gebote je Stunde erzeugen
            for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
            {
                //Angebot erzeugen
                for (i = 0; i < Anzahl; i++)
                {
                    randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[k] + 10, stabMG[k], iSeed);
                    randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10;

                    //randomVolume = r.Next(50, 400) / 10;
                    Bids.Add((randomPrice, randomVolume, time));
                    iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
                }

                //Nachfrage erzeugen                
                for (i = 0; i < Anzahl; i++)
                {
                    randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[k], stabMG[k], iSeed);
                    randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10 * -1;
                    //randomVolume = r.Next(50, 400) / 10 * -1;
                    Bids.Add((randomPrice, randomVolume, time));
                    iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
                }
                k += 1;
            }
            return Bids;
        }

        public static List<(ShortCode BlockID, decimal Price, decimal Volume)> DebugAddRandomBlockBidsDA(int Anzahl = 10)
        {
            List<(ShortCode BlockID, decimal Price, decimal Volume)> BlockBids = new List<(ShortCode BlockID, decimal Price, decimal Volume)>();
            Random r = new Random();
            int i;
            decimal randomPrice, randomVolume;

            //Werte für Testgebote
            int[] avgPriceMG = new int[24] { 15, 17, 18, 19, 21, 24, 26, 29, 35, 38, 40, 37, 36, 30, 25, 24, 28, 32, 45, 49, 46, 33, 32, 30 };
            int[] stabMG = new int[24] { 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20 };
            int iSeed;

            iSeed = 1000; // Environment.TickCount;
            //Gebote für Offpeak
            ShortCode BlockID = ShortCode.DayAhead.OffPeak1;
            //Angebot erzeugen
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] + 6, stabMG[0], iSeed);

                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10 * (-1);

                //randomVolume = r.Next(50, 400) / 10;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }

            //Nachfrage erzeugen                
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] - 4, stabMG[0], iSeed);
                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10;
                //randomVolume = r.Next(50, 400) / 10 * -1;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }

            BlockID = ShortCode.DayAhead.OffPeak2;
            //Angebot erzeugen
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] + 6, stabMG[0], iSeed);

                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10 * (-1);

                //randomVolume = r.Next(50, 400) / 10;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }

            //Nachfrage erzeugen                
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] - 4, stabMG[0], iSeed);
                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10;
                //randomVolume = r.Next(50, 400) / 10 * -1;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }

            //Gebote für Peak
            BlockID = ShortCode.DayAhead.Peak;
            //Angebot erzeugen
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] + 8, stabMG[0], iSeed);

                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10 * (-1);
                //randomVolume = r.Next(50, 400) / 10;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }

            //Nachfrage erzeugen                
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] - 2, stabMG[0], iSeed);
                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10;
                //randomVolume = r.Next(50, 400) / 10 * -1;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }

            //Gebote für Base
            BlockID = ShortCode.DayAhead.Base;
            //Angebot erzeugen
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] + 4, stabMG[0], iSeed);
                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10 * (-1);
                //randomVolume = r.Next(50, 400) / 10;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }

            //Nachfrage erzeugen                
            for (i = 0; i < Anzahl; i++)
            {
                randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[0] - 6, stabMG[0], iSeed);
                randomVolume = MathHelper.GetNormVerteilt(250, 100, iSeed) / 10;
                //randomVolume = r.Next(50, 400) / 10 * -1;
                BlockBids.Add((BlockID, randomPrice, randomVolume));
                iSeed = decimal.ToInt32(randomPrice * randomVolume); // + Environment.TickCount;
            }
            return BlockBids;

        }

        public static List<(decimal Price, decimal Volume, DateTime QuarterHour)> DebugAddRandomBidsID(AuctionArgs args, int Anzahl = 100)
        {
            List<(decimal Price, decimal Volume, DateTime Hour)> Bids = new List<(decimal Price, decimal Volume, DateTime Hour)>();
            DateTime EndDuration = args.TimeStamp.Add(args.Duration);
            Random r = new Random();
            int i, k;
            decimal randomPrice, randomVolume;

            //Werte für Testgebote
            int[] avgPriceMG = new int[4] { 35, 38, 42, 25 };
            int[] stabMG = new int[4] { 20, 20, 20, 20 };
            int iSeed;

            k = 0;
            iSeed = Environment.TickCount;
            //Gebote je Stunde erzeugen
            for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
            {
                //Angebot erzeugen
                for (i = 0; i < Anzahl; i++)
                {
                    randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[k] + 10, stabMG[k], iSeed);
                    randomVolume = r.Next(50, 400) / 10;
                    Bids.Add((randomPrice, randomVolume, time));
                    iSeed = decimal.ToInt32(randomPrice * randomVolume) + Environment.TickCount;
                }

                //Nachfrage erzeugen                
                for (i = 0; i < Anzahl; i++)
                {
                    randomPrice = MathHelper.GetNormVerteilt(avgPriceMG[k], stabMG[k], iSeed);
                    randomVolume = r.Next(50, 400) / 10 * -1;
                    Bids.Add((randomPrice, randomVolume, time));
                    iSeed = decimal.ToInt32(randomPrice * randomVolume) + Environment.TickCount;
                }
                k += 1;
            }
            return Bids;
        }

        public static List<Bid> DebugGenerateBidsControlReserveCapacityMarket(Market calling, AuctionArgs args)
        {
            DateTime EndDuration = args.TimeStamp.Add(args.Duration);
            List<Bid> Bids = new List<Bid>();
            Random rnd = new Random(1000);
            List<Bid_Segment> BidSegments = new List<Bid_Segment>();
            //Gebote für positive Sekundärregelleistung
            int randomInt = rnd.Next(10, 20);
            int step = 0;
            Tests.Agents.TestBalancingGroup bg = new Tests.Agents.TestBalancingGroup();
            Tests.Assets.Test.TestAsset testAsset = new Tests.Assets.Test.TestAsset(bg as Bal, Tests.Assets.Test.TestAsset.TestAssetType.TestLoad, new Assets.Test.Attributes.TestAssetAttributes(1000, new winMoz.Assets.Location(), Assets.Test.Attributes.TestAssetAttributes.Profile.StandardLoad, (100, 1000), new List<TimeSpan>() { TimeSpan.FromHours(10) }, 10)); 
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Capacity Bid = new Bid_ControlReserve_Capacity(new List<IAsset> { testAsset }, calling);
                step = 1;
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int price = rnd.Next(1, 300);
                    int priceEnergy = rnd.Next(1, 200);
                    int volume = rnd.Next(100, 1000);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_aFRR, time, args.Step), time, volume, priceEnergy);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.p_aFRR, time, args.Step), time, volume, price);
                    
                    step++;
                }
                Bids.Add(Bid);
            }
            //Gebote für negative Sekundärregelleistung

            randomInt = rnd.Next(10, 20);
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Capacity Bid = new Bid_ControlReserve_Capacity(new List<IAsset> { testAsset }, calling);
                step = 1;
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int price = rnd.Next(-10, -1);
                    int priceEnergy = rnd.Next(-200, -1);
                    int volume = rnd.Next(-1000, -100);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_aFRR, time, args.Step), time, volume, priceEnergy);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.n_aFRR, time, args.Step), time, volume, price); 
                    step++;
                }
                Bids.Add(Bid);
            }
            //Gebote für positive Minutenreserveleistung

            randomInt = rnd.Next(10, 20);
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Capacity Bid = new Bid_ControlReserve_Capacity(new List<IAsset> { testAsset }, calling);
                step = 1;
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int price = rnd.Next(1, 300);
                    int priceEnergy = rnd.Next(1, 200);
                    int volume = rnd.Next(100, 1000);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_mFRR, time, args.Step), time, volume, priceEnergy);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.p_mFRR, time, args.Step), time, volume, price);
                    step++;
                }

                Bids.Add(Bid);
            }
            //Gebote für negative Minutenreserveleistung

            randomInt = rnd.Next(10, 20);
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Capacity Bid = new Bid_ControlReserve_Capacity(new List<IAsset> { testAsset }, calling);
                step = 1;
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int price = rnd.Next(-300, -1);
                    int priceEnergy = rnd.Next(-200, -1);
                    int volume = rnd.Next(-1000, -100);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_mFRR, time, args.Step), time, volume, priceEnergy);
                    Bid.AddSegment(calling, ShortCode.ControlReserve.CapacityFromEnum(ControlReserveShortCode.ShortCode.n_mFRR, time, args.Step), time, volume, price);
                    step++;
                }
                Bids.Add(Bid);
            }
            return Bids;
        }

        public static List<Bid> DebugGenerateBidsControlReserveEnergyMarket(AuctionArgs args, Market calling)
        {
            DateTime EndDuration = args.TimeStamp.Add(args.Duration);
            List<Bid> Bids = new List<Bid>();
            Random rnd = new Random(1000);
            //Gebote für positive Sekundärregelleistung
            int randomInt = rnd.Next(10, 20);
            int step = 1;
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Energy Bid = new Bid_ControlReserve_Energy();
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int priceEnergy = rnd.Next(1, 200);
                    int volume = rnd.Next(100, 1000);
                    Bid.AddSegment(ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_aFRR, time, args.Step), time, volume, priceEnergy);
                    step++;
                }

                Bids.Add(Bid);
            }
            //Gebote für negative Sekundärregelleistung

            randomInt = rnd.Next(10, 20);
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Energy Bid = new Bid_ControlReserve_Energy();
                step = 1;
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int priceEnergy = rnd.Next(-200, -1);
                    int volume = rnd.Next(-1000, -100);
                    Bid.AddSegment(ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_aFRR, time, args.Step), time, volume, priceEnergy);
                    step++;
                }
                Bids.Add(Bid);
            }
            //Gebote für positive Minutenreserveleistung

            randomInt = rnd.Next(10, 20);
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Energy Bid = new Bid_ControlReserve_Energy();
                step = 1;
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int priceEnergy = rnd.Next(1, 200);
                    int volume = rnd.Next(100, 1000);
                    Bid.AddSegment(ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.p_mFRR, time, args.Step), time, volume, priceEnergy);
                    step++;
                }

                Bids.Add(Bid);
            }
            //Gebote für negative Sekundärregelleistung

            randomInt = rnd.Next(10, 20);
            for (int i = 0; i <= randomInt; i++)
            {
                Bid_ControlReserve_Energy Bid = new Bid_ControlReserve_Energy();
                step = 1;
                for (DateTime time = args.TimeStamp; time < EndDuration; time = time.Add(args.Step))
                {
                    int priceEnergy = rnd.Next(-200, -1);
                    int volume = rnd.Next(-1000, -100);
                    Bid.AddSegment(ShortCode.ControlReserve.EnergyFromEnum(ControlReserveShortCode.ShortCode.n_mFRR, time, args.Step), time, volume, priceEnergy);
                    step++;
                }
                Bids.Add(Bid);
            }
            return Bids;
            
        }*/
    }
}
