﻿using System;
using System.Collections.Generic;
using System.IO;
using winMoz.Markets.EoMMarkets;
using winMoz.Markets.Elements;

namespace winMoz.Helper
{
    public static class Helper_DebugLog_MCC
    {
        public static string ProjectPath { get; private set; } = Directory.GetParent(System.Reflection.Assembly.GetCallingAssembly().Location).Parent.Parent.Parent.FullName;
        public static string DebugFilesFolder = $@"{ProjectPath}\DebugFilesTmp\";
 
        public static string PrintDebugLogBefore(
            int iDebug, MA_Auction MAExport, MA_Auction MAImport, MarketAreaEnum sIDIm,
            decimal VolumeExchangeRaw, decimal VolumeExchangeNetting, decimal losses, ImExMC Export, List<ImExMC> LstExport)
        {
            string sDebugLog = iDebug + ";";
            sDebugLog += MAExport.MA_Code.ToString() + ";";
            sDebugLog += sIDIm.ToString() + ";";
            sDebugLog += MAExport.MaxIm + ";";
            sDebugLog += MAExport.MaxEx + ";";
            sDebugLog += MAImport.MaxIm + ";";
            sDebugLog += MAImport.MaxEx + ";";
            sDebugLog += MAExport.V_Result + ";";
            sDebugLog += MAExport.P_Result + ";";
            sDebugLog += MAImport.V_Result + ";";
            sDebugLog += MAImport.P_Result + ";";
            sDebugLog += VolumeExchangeNetting + ";";
            sDebugLog += VolumeExchangeRaw + ";";
            sDebugLog += VolumeExchangeRaw * (1 - losses) + ";";

            return sDebugLog;
        }

        public static string PrintDebugLogAfter(string sDebugLog,
            int iDebug, MA_Auction MAExport, MA_Auction MAImport, MarketAreaEnum sIDIm,
            decimal VolumeExchangeRaw, decimal VolumeExchangeNetting, decimal losses, ImExMC Export, List<ImExMC> LstExport)
        {
            //Für das Exportland
            decimal VolumeExchange = VolumeExchangeNetting * (1 - losses) + VolumeExchangeRaw;
            //MAExport.V_Result += VolumeExchange;
            //MAExport.CalcPrice(MAExport.V_Result);
            //MAExport.MaxEx -= VolumeExchange;
            //MAExport.MaxIm += VolumeExchange;

            sDebugLog += VolumeExchange + ";";
            sDebugLog += MAExport.V_Result + ";";
            sDebugLog += MAExport.P_Result + ";";

            //Für das Importland
            VolumeExchange = VolumeExchangeNetting + VolumeExchangeRaw * (1 - losses);
            //MAImport.V_Result -= VolumeExchange;
            //MAImport.CalcPrice(MAExport.V_Result);
            //MAImport.MaxIm -= VolumeExchange;
            //MAImport.MaxEx += VolumeExchange;


            sDebugLog += VolumeExchange + ";";
            sDebugLog += MAImport.V_Result + ";";
            sDebugLog += MAImport.P_Result + ";";

            //Korrigiere Import-Export-Kapazitäten
            //Export.CapacityNetting -= VolumeExchangeNetting;
            //Export.CapacityRaw -= VolumeExchangeRaw;

            //Erhöhe Nettingkapazität für das Gegengeschäft
            //Export.CapacityNetting += VolumeExchangeRaw * (1 - losses);
            //Export.CapacityRaw += VolumeExchangeNetting / (1 - losses);

            int iCancel = 0;
            //Abbruchbedingung, falls Menge 0 dann ALG ende
            //if (VolumeExchange < 0.00001m) {  iCancel = -1; }

            sDebugLog += MAExport.MaxIm + ";";
            sDebugLog += MAExport.MaxEx + ";";

            if (iCancel < 0)
            {
                sDebugLog += "ABBRUCH Menge<0;";
                sDebugLog += "ABBRUCH Menge<0;";
            }
            else
            {
                sDebugLog += MAImport.MaxIm + ";";
                sDebugLog += MAImport.MaxEx + ";";

            }


            //##################Debugging ########################

            foreach (ImExMC tmp in LstExport)
            {
                sDebugLog += tmp.MA_Export.MA_Code.ToString() + ";";
                sDebugLog += tmp.MA_Import.MA_Code.ToString() + ";";
                sDebugLog += tmp.CapacityRaw + ";";
                sDebugLog += tmp.CapacityNetting + ";";
            }
            if (iDebug > 10000) { iCancel = -1000; }

            return sDebugLog;
        }

        public static void DebugAusgabe(List<ImExMC> LstExport, Dictionary<MarketAreaEnum, MA_Auction> LstMarketAreas, List<string> LstDebugLog, DateTime TS)
        {
            string sFilePath, sTupel;

            sFilePath = DebugFilesFolder + "DBug_MC_Markets_S" + TS.ToString("HH") + "_" + DateTime.Now.ToString("yyMMddHHmmss") + ".csv";

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(sFilePath))
            {
                sTupel = "ID;maxIm;maxEx;V_Start;P_Start;V_Result;P_Result;";
                file.WriteLine(sTupel);

                foreach (MA_Auction tmp in LstMarketAreas.Values)
                {
                    sTupel = tmp.MA_Code + ";" + tmp.MaxIm + ";" + tmp.MaxEx + ";" + tmp.V_Start + ";" + tmp.P_Start + ";" + tmp.V_Result + ";" + tmp.P_Result + ";";
                    file.WriteLine(sTupel);
                }
            }

            sFilePath = DebugFilesFolder + "DBug_MC_Capa_" + DateTime.Now.ToString("yyMMddHHmmss") + ".csv";

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(sFilePath))
            {
                sTupel = "Step;MA_Export;MA_Import;Ex_MaxIm_before;Export_MaxEx_before;Import_MaxIm_before;Import_MaxEx_before;Export_V_Result_before;Export_P_Result_before;Import_V_Result_before;Import_P_Result_before;ExchangeNettingExp;ExchangeRawExp;Loss;VolumeExport;Export_V_Result_after;Export_P_Result_after;VolumeImport;Import_V_Result_after;Import_P_Result_after;Ex_MaxIm_bevore;Export_MaxEx_bevore;Import_MaxIm_bevore;Import_MaxEx_bevore";


                foreach (ImExMC tmp in LstExport)
                {
                    sTupel += "Origin;";
                    sTupel += "Target;";
                    sTupel += "RawCapacity;";
                    sTupel += "NettingCapacity;";
                }

                file.WriteLine(sTupel);

                foreach (string tmp in LstDebugLog)
                {
                    file.WriteLine(tmp);
                }
            }

        }

    }
}
