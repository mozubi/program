﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Trading;
using winMoz.Markets.Bids;
using winMoz.Assets;
using winMoz.Markets;
using winMoz.Markets.Elements;
using Tests.Assets.Test;
using winMoz.Helper;
using Tests.Trading.Strategies;
using winMoz.Markets.EoMMarkets;

namespace Tests.Trading
{
    public class TestTrader : ITestTrader
    {
        // Strategien bewerten und adaptieren
        List<TestStrategy> Strategies = new List<TestStrategy>();
        IEnumerable<Bid> Bids => this.Strategies.SelectMany(strategy => strategy.Bids);
        IEnumerable<IAsset> Assets => this.Strategies.SelectMany(el => el.Assets).Distinct();
        public List<Type> ExcludedMarkets { get; } = new List<Type>() { typeof(FuturesMarket), typeof(DayAheadMarket), typeof(IntradayMarket) };
        #region Constructors
        public TestTrader() { }
        public TestTrader(params IAsset[] assets) : this(assets.ToList()) { }
        public TestTrader(IEnumerable<IAsset> assets) { BuildStrategies(assets); }
        public TestTrader(params TestStrategy[] strategies) { this.Strategies.AddRange(strategies); }
        #endregion

        #region Markets

        public void SubscribeToMarket(params Market[] markets)
        {
            markets.ToList().ForEach(market => market.Auction += Trade);
            foreach (Market market in markets) IncludeMarketType(market);
            this.Strategies.ForEach(strategy => strategy.ChangeExcludedMarkets(ExcludedMarkets.ToArray()));
        }

        private void IncludeMarketType(Market market)
        {
            if (market.IsFuturesMarket()) ExcludedMarkets.Remove(typeof(FuturesMarket));
            else if (market.IsDayAheadMarket()) ExcludedMarkets.Remove(typeof(DayAheadMarket));
            else if (market.IsIntradayMarket()) ExcludedMarkets.Remove(typeof(IntradayMarket));
        }

        /// <summary>
        /// Methode löst Handelsaktivitäten der Agenten aus
        /// </summary>
        /// <param name="callingMarket">Auslösendes Marktevent</param>
        /// <param name="auctionArgs">Übergeben Auktionsargumente</param>
        private void Trade(IMarket callingMarket, AuctionArgs auctionArgs)
        {
            //var commodities = winMoz.Information.Commodities.GetPricesOfAllCommodities(auctionArgs.TimeStamp, auctionArgs.Duration);
            this.Assets.ToList().ForEach(asset => asset.CalcSchedule(auctionArgs.TimeStamp, auctionArgs.Duration));
            this.Strategies.ForEach(strategy => strategy.Execute((Market)callingMarket));
        }
        #endregion

        #region Strategies
        private void BuildStrategies(IEnumerable<IAsset> assets)
        {
            this.Strategies.Add(new TestStrategy(assets.Where(asset => asset.GetType() == typeof(TestAsset)).Select(asset => (TestAsset)asset).ToList(), ExcludedMarkets.ToArray()));
        }

        public void Add(params TestStrategy[] strategies) => this.Strategies.AddRange(strategies);
        public void Add(params IAsset[] assets) => BuildStrategies(assets);
        #endregion
    }
}
