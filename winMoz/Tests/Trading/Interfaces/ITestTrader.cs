﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Trading;
using winMoz.Markets.Bids;
using winMoz.Assets;
using winMoz.Markets;
using winMoz.Markets.Elements;
using Tests.Assets.Test;
using Tests.Trading.Strategies;
using winMoz.Helper;

namespace Tests.Trading
{
    public interface ITestTrader
    {
        List<Type> ExcludedMarkets { get; }

        #region Markets

        void SubscribeToMarket(params Market[] markets);

        #endregion

        #region Strategies
        void Add(params TestStrategy[] strategies);
        void Add(params IAsset[] assets);
        #endregion
    }
}
