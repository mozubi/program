﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Markets;
using winMoz.Assets;
using winMoz.Portfolios;
using winMoz.Markets.Bids;
using winMoz.Helper;
using winMoz.Assets.Schedules;
using Tests.Assets.Test;

namespace Tests.Trading.Strategies
{
    public class TestStrategy : TradingStrategy
    {
        public TestStrategy(IEnumerable<TestAsset> assets, params Type[] excludedMarkets) : base(assets, excludedMarkets)
        {
        }

        protected override void CreateBids(Market callingMarket)
        {
            var Schedule = Assets.Where(asset => ((Asset)asset).GetType().Name == "TestAsset").GetSumSchedule(callingMarket.EventTimeframe.TimeStamp, callingMarket.EventTimeframe.Duration);
            var Assets1 = Assets.Where(asset => ((Asset)asset).GetType().Name == "TestAsset");
            var bids = new List<Bid>();
            DateTime EndDuration = callingMarket.EventTimeframe.TimeStamp.Add(callingMarket.EventTimeframe.Duration);
            if (callingMarket.IsDayAheadMarket())
                bids.Add(Bid_DayAhead.Hour(this.Assets.Where(asset => (asset.GetType().Name == "TestAsset")).ToList(), callingMarket, Schedule.Select(el => (el.TimeStamp, el.PowerDiff, 0.2F)).ToList()));

            if (callingMarket.IsIntradayMarket())
                bids.Add(Bid_Intraday.QuarterHour(this.Assets.Where(asset => ((Asset)asset).GetType().Name == "TestAsset").ToList(), callingMarket, Schedule.Select(el => (el.TimeStamp, el.PowerDiff, 0.2F)).ToList()));
            bids.ForEach(bid => bid.Segments.ForEach(item => item.SetUnlimitedIfTooHigh(callingMarket)));
            foreach (var Bid in bids) AddBid(Bid);
        }
    }
}
