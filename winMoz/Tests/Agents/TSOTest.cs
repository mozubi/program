﻿using NUnit.Framework;
using System;
using winMoz.Trading.Strategies;
using winMoz.Assets.Plants;
using winMoz.Agents;
using winMoz.Markets;
using winMoz.Simulation;
using System.Collections.Generic;
using System.Linq;
using winMoz.Assets;
using winMoz.Markets.Elements;
using winMoz.Markets.EoMMarkets;

namespace Tests.Agents
{
    public class TSOTest
    {
        private SimContextSetup _simContextSetup;
        [SetUp]
        public void Setup()
        {
            new Config();
            // 
            _simContextSetup = new SimContextSetup();
            
        }

        [Test]
        public void GetSaldo_Returns_FullDay()
        {
            var tso = new TSO();
            var dso = new DSO(tso);
            var bg = new BalancingGroup(dso);
            var timeStamp = Simulator.Times.Start;
            var market = new DayAheadMarket(new DayAheadAttributes(timeStamp, MarketAreaEnum.DE));
            //Leistung der Thermal Plant nicht eindeutig. In diesem Fall wird thermische Leistung der Unit verwendet, also 80.
            var thermal = new Plant_Thermal(bg, Plant_Thermal.SubTypes.COAL, 10.0F, Location.Random());
            thermal.CreateUnits(new List<Block>() { new Block_Kond(bg, Plant_Thermal.SubTypes.COAL, 80, winMoz.Information.Commodities.Commodity.Hard_Coal, Plant_Thermal.AgeTypes.HARD_COAL_NEW, thermal) });
            //thermal.CalcSchedule(timeStamp.AddDays(1), new TimeSpan(24, 0, 0));
            bg.Assets.ToList().ForEach(el => el.CalcSchedule(timeStamp.AddDays(1), new TimeSpan(24, 0, 0)));
            dso.TimeSeries.Assets.ToList().ForEach(el => el.CalcSchedule(timeStamp.AddDays(1), new TimeSpan(24, 0, 0)));

            tso.CloseGate(timeStamp.AddDays(1));
            Assert.IsTrue(tso.getSaldo().Count() == 96, "GetSaldo hat keine 96 Werte zurückgegeben.");
        }

        public void Save_And_Load_TSO()
        {
            var tso = new TSO();
            var dso = new DSO(tso);
            var bg = new BalancingGroup(dso);
            var timeStamp = Simulator.Times.Start;
            var market = new DayAheadMarket(new DayAheadAttributes(timeStamp, MarketAreaEnum.DE));
            //Leistung der Thermal Plant nicht eindeutig. In diesem Fall wird thermische Leistung der Unit verwendet, also 80.
            var thermal = new Plant_Thermal(bg, Plant_Thermal.SubTypes.COAL, 10.0F, Location.Random());
            thermal.CreateUnits(new List<Block>() { new Block_Kond(bg, Plant_Thermal.SubTypes.COAL, 80, winMoz.Information.Commodities.Commodity.Hard_Coal, Plant_Thermal.AgeTypes.HARD_COAL_NEW, thermal) });

        }
    }
}