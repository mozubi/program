﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Agents;
using Tests.Trading;
using winMoz.Assets;
using winMoz.Assets.Schedules;

namespace Tests.Agents
{
    public class TestBalancingGroup : Agent
    {
        public int Id { get; private set; }
        public IReadOnlyCollection<IAsset> Assets => _Assets.ToList();
        public TestTrader Trader { get; private set; } = new TestTrader();
        private HashSet<IAsset> _Assets = new HashSet<IAsset>();

        public TestBalancingGroup() : base(Types.BG)
        {
        }

        public List<(DateTime TimeStamp, float Input, float Output)> SubmitBalanceSchedule(DateTime timeStamp, TimeSpan duration)
        {
            // if there are only bids associated to assets, this represents the whole balance. if there are financial-only bids, these need to be taken into account
            return this.Assets.GetSumSchedule(timeStamp, duration).Select(el => (el.TimeStamp, Math.Max(0, el.Power) + Math.Max(0, el.PowerProcured), Math.Min(0, el.Power) + Math.Min(0, el.PowerProcured))).ToList();
        }

        public void Register(IAsset asset)
        {
            this.Trader.Add(asset);
            if (!this._Assets.Add(asset))
                throw new Exception();
        }
    }
}
