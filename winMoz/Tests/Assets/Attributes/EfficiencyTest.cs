﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using winMoz.Assets;
using System.Linq;

namespace UnitTests.Assets.Attributes
{
    [TestFixture]
    public class EfficiencyTest
    {
        [Test]
        public void SetEfficiencyAtPnomMovesCurve()
        {
            float offs = 0.17F;
            Efficiency eff = new Efficiency( 0.58F,0.35F,  0.5F);
            Efficiency eff2 = new Efficiency(0.58F, 0.35F, 0.5F);
            eff2.SetEfficiencyAtPnom(0.58F - offs);

            for (int i = 0; i< eff.Curve.Count; i++)
            {
                Assert.AreEqual(eff.Curve[i] - offs, eff2.Curve[i], 0.0001F);
            }
        }
        [Test]
        public void TestPowerAndEfficiencyCurve()
        {
            float PowerInput = 5.0F;
            //easy check for quadratic interpolation, function has (0,0) and (2,0) with maximum at 1
            // f(x) = x*(x-2) = -x^2 + 2x
            Efficiency eff = new Efficiency(1.0F, 0.0F, 0.0F);
            var PowerCurve = new List<float>() {0.0F, 1.0F/9.0F, 2.0F/9.0F, 3.0F / 9.0F, 4.0F / 9.0F, 5.0F / 9.0F, 6.0F / 9.0F, 7.0F / 9.0F, 8.0F / 9.0F, 1.0F, 1.1F };
            var EffCurve = PowerCurve.Select(el => -(el * el) + 2 * el).ToList();

            List<float> fullPower = eff.GetPowerRange(PowerInput);

            List<float> fullPowerExpected = new List<float>(PowerCurve.Count);

            for (int i = 0; i < PowerCurve.Count; i++)
            {
                Assert.AreEqual(PowerCurve[i], eff.PowerCurve[i], 0.0001F);
                Assert.AreEqual(EffCurve[i], eff.Curve[i], 0.0001F);

                Assert.AreEqual(PowerInput * PowerCurve[i] , fullPower[i], 0.0001F);
            }

        }
    }
}
