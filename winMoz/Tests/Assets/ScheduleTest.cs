﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets.Schedules;
namespace Tests.Assets
{
    class ScheduleTest
    {
        [Test]
        public void CheckWhereReturnsEmptyList()
        {
            Schedule schedule = new Schedule();

            var power = Enumerable.Range(0, 24).Select(el => (new DateTime(2015, 1, 1) + TimeSpan.FromHours(el), 15.0F));
            schedule.AddPower(power.ToList());

            var res = schedule.Where(el => el.TimeStamp >= new DateTime(2015, 2, 1)).ToList();
            Assert.IsNotNull(res);
            Assert.IsEmpty(res);
        }
    }
}
