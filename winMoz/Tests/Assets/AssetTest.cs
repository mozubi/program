﻿using NUnit.Framework;
using System;
using winMoz.Assets.Plants;
using winMoz.Assets.Loads;
using winMoz.Simulation;
using winMoz.Agents;
using System.Collections.Generic;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Contexts;
using winMoz.Assets;
using System.Linq;
using winMoz.Factories;

namespace Tests.Assets
{
    class AssetTest
    {
        TSO tso;
        DSO dso;
        BalancingGroup bg;
        List<IAsset> assetsToSave = new List<IAsset>();
        List<IAsset> assetsToLoad = new List<IAsset>();

        List<string> typesToTest =
            new List<string>()
            {
                "Plant_Thermal",
                "Plant_PV",
                "Plant_Storage",
                "Plant_Wind",
                "Load_SLP"
            };
        [SetUp]
        public void TestInitialize()
        {
            new Config();
            tso = new TSO();
            dso = new DSO(tso);
            bg = new BalancingGroup(dso);
        }

        [Test]
        public void Save_And_Load_Plants()
        {
            /*var repo = new AssetRepository();
            var factory = new AssetFactory();
            foreach (var type in typesToTest)
            {
                assetsToSave.Add(factory.GetDefault(type, bg));
            }

            repo.AddRange(assetsToSave);
            //repo.Add(plant2ToSave);


            assetsToLoad = repo.GetAllAssets(bg);


            Assert.IsTrue(assetsToSave.Count == assetsToLoad.Count);

            assetsToLoad = assetsToLoad.OrderBy(asset => asset._type).ToList();
            assetsToSave = assetsToSave.OrderBy(asset => asset._type).ToList();
            List<(IAsset save, IAsset load)> assetsSaveAndLoad = assetsToSave.Zip(assetsToLoad, (save, load) => (save, load)).ToList();
            foreach ((IAsset saved, IAsset loaded) in assetsSaveAndLoad)
            {
                if (saved is Plant_Thermal)
                {
                    Assert.IsTrue((saved as Plant_Thermal).AgeType == (loaded as Plant_Thermal).AgeType);
                    Assert.IsTrue((saved as Plant_Thermal).Attributes.ThermalData.Efficiency.AtPnom == (loaded as Plant_Thermal).Attributes.ThermalData.Efficiency.AtPnom);
                    Assert.IsTrue((saved as Plant_Thermal).Units.Count == (loaded as Plant_Thermal).Units.Count);
                    Assert.IsTrue((saved as Plant_Thermal).Attributes.AgeType == (loaded as Plant_Thermal).Attributes.AgeType);
                    Assert.IsFalse((loaded as Plant_Thermal).Attributes.ThermalData.Efficiency.Curve is null);
                }

                Assert.IsTrue(saved.BalancingGroup == loaded.BalancingGroup);
                Assert.IsTrue(saved.Attributes.Location.Postcode == loaded.Attributes.Location.Postcode);
                if (!(saved is Load_SLP)) Assert.IsTrue(saved.Attributes.Economics.Capex == loaded.Attributes.Economics.Capex);
                //check if same balancing group is loaded, both null
                Assert.IsTrue(saved.BalancingGroup.Id == loaded.BalancingGroupId);

            }*/

        }
    }
}
