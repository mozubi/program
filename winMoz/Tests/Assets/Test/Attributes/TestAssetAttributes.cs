﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winMoz.Assets.Test.Attributes
{
    public class TestAssetAttributes 
    {
        public enum Profile {StandardLoad, StandardPlant, HalfDayLoad, HalfDayPlant}

        public float[] ProfileList;

        public (float Min, float Max) Borders;

        public List<TimeSpan> Durations;

        public float Price;
        public TestAssetAttributes(float powerMW, Location location, Profile profile, (float Min, float Max) borders, List<TimeSpan> durations, float price = 999999)
        {
            Borders = borders;
            Durations = new List<TimeSpan>();
            if (durations.Count() >= GetProfileLength(profile))
            {
                for (int i = 0; i < GetProfileLength(profile); i++)
                    Durations.Add(durations[i]);
            }
            else
            {
                for (int i = 0; i < durations.Count(); i++)
                    Durations.Add(durations[i]);
                for (int i = durations.Count(); i < GetProfileLength(profile); i++)
                    Durations.Add(TimeSpan.FromHours(0));
            }
            ProfileList = GetProfile(profile);
            Price = price;
        }

        private int GetProfileLength(Profile profile)
        {
            switch (profile)
            {
                case Profile.StandardLoad: return 7;
                case Profile.HalfDayLoad: return 2;
                case Profile.HalfDayPlant: return 2;
                default: return 7;
            }
        }

        private float[] GetProfile(Profile profile)
        {
            switch (profile)
            {
                case Profile.StandardLoad: return new float[] { 0, 0.4F, 0.6F, 1, 0.8F, 0.2F, 0.1F };
                case Profile.HalfDayLoad: return new float[] { 0, 1 };
                case Profile.HalfDayPlant: return new float[] { 1, 0 };
                default: return new float[] { 0, 0.2F, 0.3F, 0.8F, 0.9F, 0.6F, 0.1F };
            }
        }
    }
}
