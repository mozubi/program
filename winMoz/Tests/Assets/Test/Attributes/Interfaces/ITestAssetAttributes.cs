﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Assets.Test.Attributes
{
    public interface ITestAssetAttributes
    {
        float[] ProfileList { get; }

        (float Min, float Max) Borders { get; }

        List<TimeSpan> Durations { get;  }

        float Price { get; }
    }
}
