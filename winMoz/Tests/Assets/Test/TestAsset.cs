﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winMoz.Assets;
using winMoz.Agents;
using winMoz.Assets.Test.Attributes;
using winMoz.Assets.Schedules;

namespace Tests.Assets.Test
{
    public class TestAsset : Asset
    {
        public enum TestAssetType { TestLoadUnlimited, TestLoad, TestPlantUnlimited, TestPlant }

        private string _subType;
        private ScheduleCreator ScheduleC;

        private class ScheduleCreator
        {
            (float Min, float Max) Borders;
            private TimeSpan[] Durations;
            private float[] Profile;
            private int IndexProfileStep;
            private DateTime TimeStamp;
            private TimeSpan ActualDurationStepPart;
            private float Price;
            private bool IsLoad;
            public ScheduleCreator((float Min, float Max) borders, TimeSpan[] durations, float[] profile, float price, bool isLoad)
            {
                Borders = borders;
                Durations = durations;
                Profile = profile;
                IndexProfileStep = 0;
                ActualDurationStepPart = TimeSpan.FromHours(0);
                IsLoad = isLoad;
                Price = price;
            }

            public Schedule GetSchedule(Schedule schedule, DateTime time, TimeSpan duration, TimeSpan stepDuration)
            {
                if (TimeStamp == default)
                    TimeStamp = time;
                var EndDuration = time.Add(duration);
                var scheduleBase = schedule.Where(el => el.TimeStamp > time && el.TimeStamp < EndDuration).ToList();
                if (!schedule.IsValid(time, duration))
                {
                    DateTime NewStartTime = scheduleBase.Count() == 0 ? time : scheduleBase.Select(item => item.TimeStamp).Max();
                    scheduleBase.AddRange(CreateSchedule(NewStartTime, duration - (NewStartTime - time), stepDuration));
                }
                else
                    return (Schedule)new List<ScheduleItem>();
                return (Schedule)scheduleBase;
            }

            private Schedule CreateSchedule(DateTime determinationTime, TimeSpan duration, TimeSpan stepDuration)
            {
                List<ScheduleItem> Schedule = new List<ScheduleItem>();
                DateTime EndDuration = determinationTime.Add(duration);
                if (EndDuration < determinationTime)
                {
                    DateTime newTime = determinationTime;
                    determinationTime = EndDuration;
                    EndDuration = newTime;
                }
                for (DateTime time = determinationTime; time < EndDuration; time = time.Add(stepDuration))
                {
                    ChangeTimeStampTo(time);
                    var Power = GetPower();
                    Schedule.Add(new ScheduleItem(time, Power));
                }
                return (Schedule)Schedule;
            }

            private void ChangeTimeStampTo(DateTime time)
            {
                var TimeStampAt = time;
                var DurationProfileTotalMinutes = Durations.Select(dur => dur.TotalMinutes).Sum();
                TimeStamp.AddMinutes(Math.Truncate((TimeStampAt - TimeStamp).TotalMinutes / DurationProfileTotalMinutes) * DurationProfileTotalMinutes);
                while (TimeStampAt != default && TimeStamp != TimeStampAt)
                {
                    if (TimeStamp > TimeStampAt)
                    {
                        if (ActualDurationStepPart.TotalMinutes == 0)
                        {
                            ReduceIndex();
                            ActualDurationStepPart = Durations[IndexProfileStep];
                        }
                        if (ActualDurationStepPart >= TimeStamp - TimeStampAt)
                        {
                            ActualDurationStepPart -= TimeStamp - TimeStampAt;
                            TimeStamp = TimeStampAt;
                        }
                        else
                        {
                            TimeStamp -= ActualDurationStepPart;
                            ActualDurationStepPart = TimeSpan.FromHours(0);
                        }
                    }
                    else if (TimeStamp < TimeStampAt)
                    {
                        if (Durations[IndexProfileStep] - ActualDurationStepPart >= TimeStampAt - TimeStamp)
                        {
                            ActualDurationStepPart += TimeStampAt - TimeStamp;
                            TimeStamp = TimeStampAt;
                        }
                        else
                        {
                            TimeStamp += Durations[IndexProfileStep] - ActualDurationStepPart;
                            ActualDurationStepPart = Durations[IndexProfileStep];
                        }
                        if (ActualDurationStepPart == Durations[IndexProfileStep])
                        {
                            IncreaseIndex();
                            ActualDurationStepPart = TimeSpan.FromHours(0);
                        }
                    }
                }
            }

            private void ReduceIndex()
                => IndexProfileStep = IndexProfileStep == 0 ? Profile.Count() - 1 : IndexProfileStep - 1;

            private void IncreaseIndex()
                => IndexProfileStep = IndexProfileStep == Profile.Count() - 1 ? 0 : IndexProfileStep + 1;

            private float GetPower()
                => (IsLoad ? (-1) : 1) * ( Borders.Min + Profile[IndexProfileStep] * (Borders.Max - Borders.Min));
        }

        public TestAsset(IBalancingGroup bg, TestAssetType type, TestAssetAttributes attributes)
        {
            this._subType = type.ToString();
            if (this._subType == TestAssetType.TestLoadUnlimited.ToString())
                ScheduleC = new ScheduleCreator(attributes.Borders, attributes.Durations.ToArray(), attributes.ProfileList, 999999, isLoad: true);
            else if (this._subType == TestAssetType.TestPlantUnlimited.ToString())
                ScheduleC = new ScheduleCreator(attributes.Borders, attributes.Durations.ToArray(), attributes.ProfileList, -999999, isLoad: false);
            else if (this._subType == TestAssetType.TestLoad.ToString())
                ScheduleC = new ScheduleCreator(attributes.Borders, attributes.Durations.ToArray(), attributes.ProfileList, attributes.Price, isLoad: true);
            else
                ScheduleC = new ScheduleCreator(attributes.Borders, attributes.Durations.ToArray(), attributes.ProfileList, attributes.Price, isLoad: false);
        }

        public override void CalcSchedule(DateTime timeStamp, TimeSpan duration)
        {
            if (!((Schedule)this.Schedule.OrderBy(item => item.TimeStamp).ToList()).IsValid(timeStamp, duration))
            {
                var schedule = ScheduleC.GetSchedule(this.Schedule, timeStamp, duration, TimeSpan.FromMinutes(15)).ToList();
                this.Schedule.AddRange(schedule);
                this.Schedule = (Schedule)Schedule.OrderBy(item => item.TimeStamp).ToList();
            }
        }
    }
}
