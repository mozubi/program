﻿using NUnit.Framework;
using System;
using winMoz.Assets.Plants;
using winMoz.Assets.Loads;
using winMoz.Simulation;
using winMoz.Agents;
using System.Collections.Generic;
using winMoz.Data.Access.Repositories;
using winMoz.Data.Access.Contexts;
using winMoz.Assets;
using System.Linq;
using winMoz.Factories;
using winMoz.Assets.Schedules;

namespace Tests.Assets
{
    class Plant_ThermalTest
    {
        TSO tso;
        DSO dso;
        BalancingGroup bg;
        Plant_Thermal plant;
        DateTime REV_SCHEDULE_START = new DateTime(2015, 1, 1);
        DateTime REV_SCHEDULE_END = new DateTime(2015, 1, 8, 0, 0, 0);
        [SetUp]
        public void TestInitialize()
        {
            new Config();
            using (var repo = new ScenarioData_Repository())
            {
                tso = repo.AssetRepository.GetTSO(ScenarioID: 0).First();
                dso = tso.dsos.First();
                bg = dso.BalancingGroups.First() as BalancingGroup;
                plant = repo.AssetRepository.GetThermal(balancingGroup: bg, ScenarioID: 0).First();
                plant.SetBalancingGroup(bg);
            }
        }
        [Test]
        public void TLPTest()
        {
            var curve = plant.TLP.GetOrSetSchedule(new DateTime(2015, 1, 1), TimeSpan.FromDays(1));

            foreach (ScheduleItem item in curve)
              {
                Assert.IsTrue(item.Power > 0);
            }

        }
        [Test]
        public void CalculateCorrectProductionCostPerEfficiency()
        {
            var volumeresults = new List<float>() { 0.989F, 1.134666667F, 1.280333333F, 1.426F, 1.571666667F, 1.717333333F, 1.863F, 2.008666667F, 2.154333333F, 2.3F, 2.53F };
            var priceresults = new List<float>(){ 32.82072727F, 32.51058143F, 32.24175083F, 32.01233498F, 31.82074864F, 31.66569356F, 31.54613592F, 31.46128886F, 31.41059936F, 31.39373913F, 31.43580667F};
            var fullresult = volumeresults.Zip(priceresults, (vol, price) => (vol, price)).ToList();
           
            var CostPerMW = plant.Units[0].GetCurrentCostCurvePerMWh(new DateTime(2015, 1, 1));


            for (int i = 0; i < CostPerMW.Count; i++)
                {
                    Assert.AreEqual(fullresult[i].vol, CostPerMW[i].MWh_el, 0.0001);
                    Assert.AreEqual(fullresult[i].price , CostPerMW[i].costPerMWh, 0.0001);
                }


        }
        [Test]
        public void LoadThermalPlantWithAllAttributesSet()
        {

            Assert.IsNotNull(plant.TLP);
            Assert.AreEqual(1, plant.BalancingGroupId);
            Assert.AreEqual(4.3, plant.PowerMW);
            //Locations
            Assert.AreEqual(1067, plant.LocationPostCode);
            Assert.AreEqual("DED", plant.Location.NUTS1);
            Assert.AreEqual("DED2", plant.Location.NUTS2);
            Assert.AreEqual("DED21", plant.Location.NUTS3);
            Assert.IsNull(plant.Location.Municipality);
            Assert.AreEqual(51.1218070983887, plant.Location.Latitude);
            Assert.AreEqual(10.4006948471069, plant.Location.Longitude);
            //Economics
            Assert.AreEqual("coal", plant.EconomicsSubType);
            Assert.AreEqual(0.08, plant.Economics.LCOE);
            Assert.AreEqual(1300, plant.Economics.Capex);
            Assert.AreEqual(32.0, plant.Economics.OpexFix);
            Assert.AreEqual(0.005, plant.Economics.OpexVar);
            Assert.AreEqual(6000, plant.Economics.FullLoadHours);
            Assert.AreEqual(40.0, plant.Economics.LifeTime);

            //
            Assert.AreEqual(2.0, plant.P_TLP);
            // TYNP has it's own testclass
            Assert.AreEqual(winMoz.Trading.Strategies.TradingStrategyType.MakeOrBuy, plant.TradingStrategy);


            //Unit properties
            Block testunit = plant.Units.First();
            Block_CHP testunit_chp = plant.Units[1] as Block_CHP;

            Assert.AreEqual(testunit.ThermalData.Efficiency.AtPnom, testunit.EfficiencyAtPnom);
            Assert.AreEqual(testunit_chp.ThermalData.Efficiency.AtPnom, testunit_chp.EfficiencyAtPnom);
            Assert.AreEqual(4, testunit.ThermalDataCategory);
            Assert.IsNotNull(testunit.ThermalData);
            Assert.AreEqual(4, testunit_chp.ThermalDataCategory);
            Assert.IsNotNull(testunit_chp.ThermalData);
            Assert.AreEqual(0.43, testunit.ThermalData.MinimumStableGeneration);
            Assert.AreEqual(0.43, testunit_chp.ThermalData.MinimumStableGeneration);

            Assert.AreEqual(testunit.Id, testunit.AvailabiltySchedule[0].UnitId);
            Assert.IsTrue(testunit.AvailabiltySchedule[0].isPlanned);
            Assert.AreEqual(REV_SCHEDULE_START, testunit.AvailabiltySchedule[0].noPowerBegin);
            Assert.AreEqual(REV_SCHEDULE_END, testunit.AvailabiltySchedule[0].noPowerEnd);

            Assert.AreEqual(2.3, testunit.PowerMW);
            Assert.AreEqual(2.0, testunit_chp.P_th_Kwk);
            Assert.AreEqual(testunit_chp.P_th_Kwk*testunit_chp.ThermalData.MinimumStableGeneration, testunit_chp.P_th_min);
            Assert.AreEqual(0.8, testunit_chp.eff_ges);

        }
        [Test]
        public void CalcScheduleWithRevisionOneUnit()
        {
            DateTime startTime = new DateTime(2015, 1, 1);
            TimeSpan duration = TimeSpan.FromDays(8);
            DateTime endTime = startTime + duration;
            plant.CalcSchedule(startTime, duration );
            var powerSchedule = plant.Schedule.Where(sched => sched.TimeStamp >= startTime && sched.TimeStamp < endTime).Select(sched => (sched.TimeStamp, sched.Power)).ToList();
            foreach ((DateTime timestamp, float power) in powerSchedule)
            {
                if (timestamp >= REV_SCHEDULE_START && timestamp <= REV_SCHEDULE_END)
                {
                    Assert.AreEqual(0.0, power);
                }
                else
                {
                    Assert.AreNotEqual(0.0, power);
                }
            }
        }
        [Test]
        public void CalcScheduleWithThermalDemand()
        {
            DateTime startTime = new DateTime(2015, 1, 1);
            TimeSpan duration = TimeSpan.FromHours(24);
            plant.CalcSchedule(startTime, duration);
        }
    }
}
