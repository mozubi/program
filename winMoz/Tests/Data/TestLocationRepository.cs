﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using winMoz.Assets;
using winMoz.Data.Access.Repositories;

namespace Tests.Data
{
    [TestFixture]
    public class TestLocationRepository
    {
        private SimContextSetup _simContextSetup;
        [SetUp]
        public void Setup()
        {
            _simContextSetup = new SimContextSetup();
        }
        [Test]
        public void Location_Add_Succesful()
        {
            Location loc = TestConstructors.GetTestLocation();
            using (var repo = new ScenarioData_Repository())
            {                
                repo.LocationRepository.Add(loc);

                int numberOfItems = repo.LocationRepository.Get().Count();

                Assert.AreEqual(1, numberOfItems);
            }
        }
    }
}
