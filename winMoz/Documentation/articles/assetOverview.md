# Überblick über alle Assets

Dieses Dokument gibt einen Überblick über die Assets und wie die Fahrplanerstellung in [CalcSchedule](../api/winMoz.Assets.Asset.yml#winMoz.Assets.Asset.CalcSchedule(System.DateTime,System.TimeSpan)) umgesetzt ist.
Die grundlegende Idee der Assets: Zur Fahrplanerstellung müssen Annahmen über die Zukunft getroffen werden. Im Falle von unflexiblen Lasten sind Vorhersagen über den zukünfiten Energiebedarf benötigt. Im einfachsten Fall werden Standardlastprofile verwendet, bei verbesserter Datenlage können auch komplexere Prognosen verwendet werden.
Gleiches gilt für fluktuierende erneuerbare Erzeuger. Hier bietet sich eine Vorhersage in Abhängigkeit des Wetters an. Für thermische Kraftwerke muss aufgrund ihrer Flexibiltät jedoch eine andere Herangehensweise gewählt werden. Sie versuchen ihre Gewinne zu maximieren. 

| Asset                                                                 | CalcSchedule Function                                                                                                                                                                                                                                                  |
|-----------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Loads.Difference](../api/winMoz.Assets.Loads.Difference.yml)         | Erstellt den Fahrplan mit Hilfe der [PredictorDifference](../api/winMoz.Tools.Prediction.DiffBk.PredictorDifference.yml) Klasse.                                                                                                                                       |
| [Loads.GridLosses](../api/winMoz.Assets.Loads.GridLosses.yml)         | Erstellt den Fahrplan mit Hilfe der [PredictorGridLosses](../api/winMoz.Tools.Prediction.GridLosses.PredictorGridLosses.yml) Klasse.                                                                                                                                   |
| [Loads.Load_Grid](../api/winMoz.Assets.Loads.Load_Grid.yml)           | Erstellt den Fahrplan mit Hilfe der [PredictorGridLoad](../api/winMoz.Tools.Prediction.GridLoad.PredictorGridLoad.yml) Klasse.                                                                                                                                         |
| [Loads.Load_SLP](../api/winMoz.Assets.Loads.Load_SLP.yml)             | Erstellt den Fahrplan mit Hilfe der [SLP](../api/winMoz.Assets.Loads.SLP.yml) Klasse.                                                                                                                                                                                  |
| [Plants.Block](../api/winMoz.Assets.Plants.Block.yml)                 | Errechnet keinen Fahrplan. Der Fahrplan wird in [Plant_Thermal](../api/winMoz.Assets.Plants.Plant_Thermal.yml) erstellt.                                                                                                                                               |
| [Plants.Plant_Biomass](../api/winMoz.Assets.Plants.Plant_Biomass.yml) | Erstellt den Fahrplan unter Berücksichtigung des Wärmebedarfs gegeben durch die [TLP](../api/winMoz.Assets.Plants.TLP.yml) Klasse.                                                                                                                                     |
| [Plants.Plant_Hydro](../api/winMoz.Assets.Plants.Plant_Hydro.yml)     | Erstellt den Fahrplan mit Hilfe historischer Zeitreihen der Laufwasserproduktion.                                                                                                                                                                                      |
| [Plants.Plant_PV](../api/winMoz.Assets.Plants.Plant_PV.yml)           | Erstellt den Fahrplan mit Hilfe der Wettervorhersage und der gegebenen technischen Details.                                                                                                                                                                            |
| [Plants.Plant_Storage](../api/winMoz.Assets.Plants.Plant_Storage.yml) | Erstellt keinen fixen Fahrplan.                                                                                                                                                                                                                                        |
| [Plants.Plant_Thermal](../api/winMoz.Assets.Plants.Plant_Thermal.yml) | Erstellt einen Fahrplan für jeden seiner [Blocks](../api/winMoz.Assets.Plants.Block.yml)  unter Berücksichtigung des thermischen Bedarfs gegeben durch die [TLP](../api/winMoz.Assets.Plants.TLP.yml) Klasse falls der Block vom Typ Block_CHP ist. Falls nicht wird er im Betriebspunkt betrieben. |
| [Plants.Plant_Wind](../api/winMoz.Assets.Plants.Plant_Wind.yml)       | Erstellt den Fahrplan mit Hilfe der Wettervorhersage und der gegebenen technischen Details.                                                                                                                                                                            |

# Modellierung einzelner Assets


## Thermische Kraftwerke

Die wesentlichen Kennzahlen zur Modellierung der thermischen Kraftwerke werden im folgenden mit hinreichender Begründung bezüglich der Wahl ihrer Werte beschrieben. Kennwerte der thermischen Kraftwerke und Quellen, Wie werden sie genutzt? 

| Name                                   | Beschreibung                                                                                                                           | Datengrundlage                       |
|----------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------| 
| CO2Emissions                           | CO2 Emissionen des Kraftwerks in kg/GJ_in. GJ_in beschreibt die Energie, die durch Verbrennung des Brennstoffs geliefert wird.         | [TYNDP](https://tyndp.entsoe.eu/maps-data/) | 
| VariableOM                             | Variable Betriebs und Wartungskosten in €/MWh_in. MWh_in beschreibt die Energie, die durch Verbrennung des Brennstoffs geliefert wird. | [TYNDP](https://tyndp.entsoe.eu/maps-data/) |
| MinimumStableGeneration                | Minimale mögliche Erzeugung in % der nominalen Erzeugung.                                                                              | [TYNDP](https://tyndp.entsoe.eu/maps-data/) |
| EfficiencyAtPnom                       | Wirkungsgrad des Kraftwerks im Arbeitspunkt.                                                                                           | [TYNDP](https://tyndp.entsoe.eu/maps-data/) |
| EfficiencyLoss                         | Wirkungsgradverlust bei minimaler Auslastung. EficiencyAtPmin = EfficiencyAtPnom - EfficiencyLoss                                      | (1) (2)                            |

Mit Hilfe der Daten aus der Tabelle können die Kraftwerke und vor allem Handelstrategien noch nicht hinreichend abgebildet werden. Mit gegebenen CO2- und Brennstoffpreisen können nur die Produktionskosten des Kraftwerks bei minimaler Auslastung sowie im Arbeitspunkt bestimmt werden. Es fehlen Zwischenwerte, um geeignete Effizeinzkurven zu bestimmen. Dazu werden verschiedene Annahmen getroffen:
 1. Die Effizienzkurven lassen sich durch ein quadratisches Poylnom interpolieren.
 2. Der maximale Wirkungsgrad ist im Arbeitspunkt.
 3. Die maximale Auslastung liegt 10% über dem Arbeitspunkt.

Daraus kann eine vollständige Effizienzkurve errechnet werden, die wie in der folgenden Abbildung aussieht: 

![Abbildung einer Effizienzkurve in Abhängigkeit der Kraftwerksauslastung.](../images/EfficiencyCurve.png "Abbildung einer Effizienzkurve in Abhängigkeit der Kraftwerksauslastung.")

*Abbildung einer Effizienzkurve in Abhängigkeit der Kraftwerksauslastung.*

Durch die Effizienzkurve können schließlich Produktionskosten für verschiedene Auslastungen des Kraftwerks errechnet werden:

 *Produktionskosten = Emissionskosten + Brennstoffkosten + Variable Betriebs- und Wartungskosten*

Eine entsprechende Kurve findet sich in der folgenden Abbildung:

![Abbildung der Produktionskosten in Abhängigkeit der Kraftwerksauslastung.](../images/ProductionCost.png "Abbildung der Produktionskosten in Abhängigkeit der Kraftwerksauslastung.")

Die Berechnungen werden für jeden Kraftwerksblock durchgeführt. Durch die leistungsabhängigen Wirkungsgrade können gestaffelte Gebote erstellt werden. 

Falls ein Kraftwerk einen Wärmebedarf decken muss und gleichzeitig einen KWK-Block besitzt, wird dieser wärmegeführt betrieben. Es wird zuerst der thermische Bedarf bestimmt. Auf Basis dieser Berechnung werden kostenabhängig die KWK-Blöcke betrieben, um den Bedarf zu decken. Der thermische Bedarf berechnet sich nach dem BDEW Leitfaden (QUELLE), wobei zunächst der Tagesbedarf an Energie bestimmt wird, um anschließend den Bedarf auf die einzelnen Stunden aufzuteilen. Die Deckung des Bedarfs erfolgt über die einzelnen KWK Blöcke. Der Algorithmus sieht wie folgt aus:
 - Bestimmung des thermischen Bedarfs.
 - Sortierung der KWK Anlagen nach Produktionskosten für thermische Energie.
 - Deckung des Bedarfs beginnend bei der günstigsten Anlage unter Berücksichtigung der minimal möglichen Auslastung.
 - Annahme: Restlicher Bedarf wird über reine Wärmeproduktion geregelt und hat dementsprechend keine Auswirkungen auf die elektrische Produktion.
 - Berechnung der elekrtischen Energie, die bei der Wärmeproduktion entsteht

Dazu sind weitere Daten notwendig. FÜr KWK Anlagen ist typischerweise ein Gesamtwirkungsgrad angegeben, der neben der elekrtischen Leistung auch die thermische Ausgangsleistung mit einbezieht. Deshalb wird neben dem elektrischen Wirkungsgrad auch der thermische Wirkungsgrad benötigt mit dessen Hilfe die thermische ausgekoppelte Leistung bestimmt werden kann. Zur Vereinfachung wird dieser Wirkungsgrad als konstant angenommen.  

(1) Schröder, Andreas; Kunz, Friedrich; Meiss, Jan; Mendelevitch, Roman; Hirschhausen, Christian von (2013): Current and prospective costs of electricity generation until 2050. DIW Data Documentation. Berlin (68). Online verfügbar unter http://hdl.handle.net/10419/80348, zuletzt geprüft am 26.11.2021.

(2) Ummels, Bart (2009): Wind Integration. Power System Operation with Large-Scale Wind Power in Liberalised Environments. Dissertation. Technische Universiteit Delft, Delft. Electrical Engineering, Mathematics and Computer Science. Online verfügbar unter http://resolver.tudelft.nl/uuid:96f1acce-b921-4ffd-b624-6796977e00c9, zuletzt geprüft am 26.11.2021.

 ## Laufwasserkraftwerke

Laufwasserkraftwerke werden über externe Zeitreihen von Laufwasserkraftwerken modelliert. Dazu gibt es zwei Gründe: 
1. Wasserkraft hat in Deutschland nur einen Anteil von etwa 4% an der gesamten Energieproduktion (energy-charts.info) und es wird in Zukunft keinen großen Zuwachs geben, weshalb eine genaue Modellierung nicht notwendig ist.
2. Die Laufwassererzeugung ist von mehreren Faktoren abhängig. Maßgeblich ist zum einen der lokale Niederschlag und zum anderen Schmelzwasser. 

Im folgenden wird vor allem Punkt zwei genauer erklärt, um die Schwierigkeit der Modellierung weiter zu verdeutlichen. Die Abbildung zeigt im obersten Graph die Laufwasserproduktion in Deutschland (Daten von https://transparency.entsoe.eu/). Darunter findet sich zunächst der Niederschlag, dann der Schneefall und schließlich die vorhandene Schneemasse in Deutschland für den gleichen Zeitraum (Daten von https://www.renewables.ninja/). 
![Abbildung der Laufwasserproduktion und verschiedener Einflussfaktoren.](../images/ProductionInfluence.png "Abbildung der Laufwasserproduktion und verschiedener Einflussfaktoren.")

Die Laufwasserproduktion unterliegt einer Saisonalität mit tendenziell höherer Produktion in der Mitte des Jahres. Aus den Niederschlagswerten lässt sich das Verhalten nicht erkennen. Im Jahr 2016 mit der höchsten Energieerzeugung ist kein höherer Niederschlag erkennbar als in den anderen Jahren. In 2017 hingegen gibt es bei hohem Niederschlag keinen großen Ausschlag in der Erzeugung. Die niedrigere Erzeugung im Winter korreliert mit Schneefall, also niedrigen Temperaturen. Aber auch hier lassen sich keine direkten Abhängigkeiten herauslesen.
Offensichtlich lassen sich anhand dieser Daten keine Vorhersagen zur Laufwasserproduktion treffen. Das Problem ist die fehlende Granularität, da die hier vorhandenen Werte über Gesamtdeutschland gemittelt sind. Wie in der folgenden Abbildung 
(Quelle: energy-charts.info) zu sehen sind die Laufwasserkraftwerke hauptsächlich im Süden Deutschlands. 

![Verteilung der Laufwasserkraftwerke in Deutschland.](../images/Laufwasser.png "Verteilung der Laufwasserkraftwerke in Deutschland.")

Damit lässt sich beispielsweise die hohe Produktion im Jahr 2016 erklären. Generell war es ein niederschlagsarmes Jahr im Durchschnitt. Allerdings gab es in der Mitte des Jahres vor allem im SÜden Deutschlands lokale heftige Unwetter mit Überschwemmungen (vgl. (1)). Für weitere Tests der Vorhersagen, sind lokal aufgelöste Wetterdaten notwendig. Da Laufwasser jedoch nur einen geringen Anteil an der deutschen Erzeugung ausmacht, wird an dieser Stelle auf eine genauere Modellierung verzichtet und stattdessen die Produktionszeitreihe verwendet. 

Da dadurch jedoch keine Vorhersagen getroffen werden muss der Vorhersagefehler simuliert werden.
Eine einfache Annahme ist, dass Vorhersagen zur Produktion auf Basis vergangener Werte berechnet werden. Z.B. wird die durchschnittliche DayAhead Produktion durch die Durchschnittswerte des Vortages vorhergesagt, d.h. es wird ein AR Modell aufgebaut und dessen Prognosefehler bestimmt. Für die nach installierter Leistung skalierte Zeitreihe ergeben sich mit dieser Methode die folgenden RMSE:
- eine Stunde Vorhersage: 0.00036
- zwei Stunden Vorhersage: 0.0005
- sechs Stunden Vorhersage: 0.001
- 24 Stunden Vorhersage: 0.0022
- Wochenvorhersage: 0.0036
- Monatsvorhersage: 0.0053

Zum Vergleich: Die Standardabweichung der Laufwasserproduktion ist 0.009872. Somit sind alle Vorhersagen besser als eine Vorhersage durch den Mittelwert.

 (1) Wetter und Klima - Deutscher Wetterdienst - Presse - Deutschlandwetter im Jahr 2016 (2021). Online verfügbar unter https://www.dwd.de/DE/presse/pressemitteilungen/DE/2016/20161229_deutschlandwetter_jahr2016_news.html, zuletzt aktualisiert am 20.07.2021, zuletzt geprüft am 20.07.2021.

 ## PV Anlagen
Um nicht alle PV Anlagen innerhalb Deutschlands zu simulieren werden sie räumlich aggregiert. Grundlage bildet das Marktstammdatenregister, aus welchem die Anlagen innerhalb Deutschlands ausgelesen werden. Dieser erste Überblick ergibt eine Aufteilung der installierten Leistung nach NUTS2 Zone, Azimut (Ost, Süd, West) und Neigungswinkel (0°-20°, 20°-40°, 40°-60°). Ziel der Untersuchung soll sein, mit möglichst wenig Anlagen einen erhöhten Anteil an Ost- bzw Westanlagen gegenüber Südanlagen darzustellen. Deshalb werden im ersten Schritt neun Anlagen modelliert wie in folgender Tabelle zu sehen. Die Prozentzahlen zeigen den Anteil an der installierten Leistung für jede Kategorie.

|                             |     Summe von OstMW    |     Summe von SüdMW    |     Summe von WestMW    |
|-----------------------------|------------------------|------------------------|-------------------------|
|     < 20 Grad               |     6,5%               |     21,9%              |     7,2%                |
|     20 - 40 Grad            |     4,3%               |     43,9%              |     6,1%                |
|     40 - 60 Grad            |     1,2%               |     7,1%               |     1,8%                |

Wie zu erkennen ist der Neiguungswinkel nicht eindeutig bestimmt. Dieser wird nun durch eine Optimierung für die jeweiligen Bereiche gesucht. Zielfunktion ist die tatsächliche Energieerzeugung durch PV Anlagen verglichen mit dem Gesamtausgang der simulierten Anlagen mit Wettereingangsdaten. Das Ergebnis liefert folgende Winkel:


|                             |     Summe von OstMW    |     Summe von SüdMW    |     Summe von WestMW    |
|-----------------------------|------------------------|------------------------|-------------------------|
|     < 20 Grad               |     0°                 |     20°                |     20°                 |
|     20 - 40 Grad            |     40°                |     40°                |     40°                 |
|     40 - 60 Grad            |     60°                |     60°                |     60°                 |

Wie in den nächsten Abbildungen zu sehen, hat der Neigungswinkel keinen zu großen Einfluss auf die Gesamterzeugung. Im folgenden wird der Fehler des Kpazitätsfaktors dargestellt. Der Kapazitätsfaktor beschreibt das Verhältnis von Ausgangsleistung zu installierter Leistung. Wie in der folgenden Abbildung zu sehen unterscheiden sich die Faktoren kaum zwischen den Initialwerten (10°, 30°, 50°) sowie den optimierten Werten aus voriger Tabelle.

![Kapazitätsfaktor für Initialwerte und Optimalwerte der Neigung.](../images/capacityFactor1.png "Kapazitätsfaktor für Initialwerte und Optimalwerte der Neigung.")

Um zuerst den Fehler allgemein zu verifzieren werden die Ergebnisse von (1) betrachtet. Dort werden die Fehler der Kapazitätsfaktoren für verschiedene Anlagen ermittelt. Diese sind vergleichbar mit den hier erreichten Fehlern. 
Da die Neigung nur wenig Einfluss auf die Erzeugung hat werden weitere Einflüsse untersucht. Deshalb ist in der folgenden Abbildung zusätzlich der Fehler angzeigt, wenn die installierte Leistung für die einzelnen Anlagen zufällig gewählt wird. Wie zu erkennen wird unter dieser Voraussetzung die Erzeugung wesentlich schlechter errechnet. Das führt zu dem Schluss, dass vor allem das richtige Verhältnis der installierten Leistung pro Ausrichtung entscheident für die Modellierung ist.

![Kapazitätsfaktor für Initialwerte, Optimalwerte der Neigung und zufällige Leistungsverteilung.](../images/capacityFactor2.png "Kapazitätsfaktor für Initialwerte, Optimalwerte der Neigung und zufällige Leistungsverteilung.")

(1) Pfenninger, Stefan; Staffell, Iain (2016): Long-term patterns of European PV output using 30 years of validated hourly reanalysis and satellite data. In: Energy 114, S. 1251–1265. DOI: 10.1016/j.energy.2016.08.060.
 