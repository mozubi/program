
# Schreiben von Dokumentation

Neben der automatisch generierten Dokumentation aus XML Tags können auch zusätzliche Dateien geschrieben werden. Die Schritte werden im folgenden erklärt. 
1. Gehe zum Ordner Documentation/articels und wähle die *.md Datei aus die du editieren möchtest. Falls eine neue Datei erstellt wird, wird sie in diesem Ordner erstellt und in der toc.yml im selben Ordner registriert. Bearbeite die Datei, wie du möchtest
2. Lade docfx unter [https://dotnet.github.io/docfx/] herunter. 
3. Führe die docfx.exe in der Kommandozeile aus:
```bash
 docfx REPO_PATH\winMoz\Documentation\docfx.json 
 ```
4. Commite alle Änderungen und pushe zu gitlab. 
5. Die Änderungen sollten nach kurzer Wartezeit auf den gitlab pages auftauchen.
