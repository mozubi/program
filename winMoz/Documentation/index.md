# **MozuBi - Modellierung zukünftiger Bilanzkreisbewirtschaftung**              ![](https://mozubi.net/wp-5f437-content/uploads/2019/07/mozubi2.2_transp-300x86.png)


Das Projekt **MozuBi** „*Modellierung zukünftiger Bilanzkreisbewirtschaftung*“ beschäftigt sich mit dem Bilanzkreiswesen als maßgebliches Instrument zur Minimierung von Abweichungen zwischen Erzeugung und Verbrauch. Angesichts der Herausforderungen durch die gegenwärtigen Transformationen im Energiesystem hat die Thematik der Bilanzkreistreue energiepolitisch eine neue Aktualität erhalten. Deshalb modelliert und bewertet das Institut für Hochspannungstechnik und Elektrische Energieanlagen ([**elenia**](https://elenia.tu-bs.de/)) gemeinsam mit dem Institut für ZukunftsEnergie- und Stoffstromsysteme ([**IZES gGmbH**](http://www.izes.de/)) als Projektpartner umfängliche Änderungen im Bilanzkreiswesen unter verschiedenen Anreizstrukturen zur Bilanzkreistreue. Darüber hinaus werden die Rückwirkungen auf die Energie(teil)märkte analysiert.

Weitere Informationen unter www.mozubi.net

![](https://mozubi.net/wp-5f437-content/uploads/2019/07/bmwi_office_farbe_de_wbz-150x150.jpg)
FKZ: 03ET4051B
## Quick Start Notes:
1. Unter Api Documentation befindet sich die Dokumentation der einzelnen Klassen und Funktionen
2. Unter Tutorials sind verschiedene Tutorials beschrieben, um den Einstieg zu erleichertn. 
